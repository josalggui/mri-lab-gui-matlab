% Creates the Rotation matrix for a Y-Z orientation
% Here Y is the readout and Z is the phase

function Quaternion = YZ_orientation ()

% Y is readout
% Z is phase
% Use the quaterion notation and the scalar product is the first term

% q1 = cos(theta/2)
% q2 = ex sin(theta/2)
% q3 = ey sin(theta/2)
% q4 = ez sin(theta/2)
Qcalc = @(theta,Evec) [cos(theta/2) Evec(1)*sin(theta/2) Evec(2)*sin(theta/2) Evec(3)*sin(theta/2)];

% first rotate the object by -90 about the Z-axis
q1 = Qcalc(-pi/2, [0 0 1]);

% second flip about the y axis
q2 = Qcalc(pi, [1 0 0]);

% multiply quaterions together
Quaternion = quatmultiply2(q1, q2);

end
