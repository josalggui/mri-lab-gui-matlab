function [ obj, instr, choice_varargin ] = ProgramInstruction( choice, instr, time_duration, choice_varargin, obj )
% Function to program instructions to the board WITHOUT having to redo them over and over again

% INPUTS
%   choice: the string which corresponds to an instruction
%   instr: the starting index of the instruction set
%   time_duration: the amount of time for the instruction group
%   choice_varargin: the variables for input for the instruction
%   obj: the object that holds all the data

% OUTPUTS
%   obj: the obj with the programmed data
%   instr: the final instr to be used for the next programmed set
%   choice_varargin: cleared variable for reprogramming

% Programmed options include:

%  SETBLANKING:
%    choice_varargin{1} = [ AMPs ; DACs ]
%    choice_varargin{2} = phase reset
%  SETGRADS:
%    choice_varargin{1} = [ AMPs ; DACs ]
%    choice_varargin{2} = phase reset
%  SETGRADSRISE:
%    choice_varargin{1} = [ AMPs ; SHIMS ; DACs ]
%    choice_varargin{2} = phase reset
%    choice_varargin{3} = rise_time
%  SETGRADSFALL:
%    choice_varargin{1} = [ AMPs ; SHIMS ; DACs ]
%    choice_varargin{2} = phase reset
%    choice_varargin{3} = rise_time
%  SETGRADSTRAP:
%    choice_varargin{1} = [ AMPs ; DACs ]
%    choice_varargin{2} = phase reset
%    choice_varargin{3} = rise_time
%  SETGRADSTRAPMATCH:
%    choice_varargin{1} = [ AMPs ; SHIMS ; DACs ]
%    choice_varargin{2} = phase reset
%    choice_varargin{3} = rise_time
%  RFPulse:
%    choice_varargin{1} = shape;
%    choice_varargin{2} = phase register;
%    choice_varargin{3} = envelope frequency register;
%    choice_varargin{4} = carrier frequency register;
%  RXs:
%    choice_varargin{1} = [ AMPs ; DACs ]
%    choice_vararing{2} = rf freq register
%  RESET:
%    choice_varargin{1} = []

switch choice
    % SET Blanking
    case 'SETBLANKING'
        ninstr = numel( choice_varargin{1}(1,:) );
        obj.AMPs( instr : instr + ninstr -1 )      = choice_varargin{1}(1,:);
        obj.DACs( instr : instr + ninstr -1 )      = choice_varargin{1}(2,:);
        obj.WRITEs( instr : instr + ninstr -1 )    = ones(1,ninstr);
        obj.UPDATEs( instr : instr + ninstr -1 )   = ones(1,ninstr);
        obj.CLEARs( instr : instr + ninstr -1 )    = zeros(1,ninstr);
        obj.FREQs( instr : instr + ninstr -1 )     = zeros(1,ninstr);
        obj.TXs( instr : instr + ninstr -1 )       = zeros(1,ninstr);
        obj.PHASEs( instr : instr + ninstr -1 )    = zeros(1,ninstr);
        obj.PhResets( instr : instr + ninstr -1 )  = [ zeros(1,ninstr-1), choice_varargin{2} ];
        obj.RXs( instr : instr + ninstr -1 )       = zeros(1,ninstr);
        obj.ENVELOPEs( instr : instr + ninstr -1 ) = repmat(7,1,ninstr);
        obj.FLAGs( instr : instr + ninstr -1 )     = ones(1,ninstr);
        obj.OPCODEs( instr : instr + ninstr -1 )   = zeros(1,ninstr);
        obj.DELAYs( instr : instr + ninstr -1 )    = [ repmat(0.1,1,ninstr-1), time_duration ]; %time in us
        
        % Set the appropriate number increase to the instructions
        instr = instr + ninstr;
        
    % SET GRADIENTS
    case 'SETGRADS'
        ninstr = numel( choice_varargin{1}(1,:) );
        obj.AMPs( instr : instr + ninstr -1 )      = choice_varargin{1}(1,:);
        obj.DACs( instr : instr + ninstr -1 )      = choice_varargin{1}(2,:);
        obj.WRITEs( instr : instr + ninstr -1 )    = ones(1,ninstr);
        obj.UPDATEs( instr : instr + ninstr -1 )   = ones(1,ninstr);
        obj.CLEARs( instr : instr + ninstr -1 )    = zeros(1,ninstr);
        obj.FREQs( instr : instr + ninstr -1 )     = zeros(1,ninstr);
        obj.TXs( instr : instr + ninstr -1 )       = zeros(1,ninstr);
        obj.PHASEs( instr : instr + ninstr -1 )    = zeros(1,ninstr);
        obj.PhResets( instr : instr + ninstr -1 )  = [ zeros(1,ninstr-1), choice_varargin{2} ];
        obj.RXs( instr : instr + ninstr -1 )       = zeros(1,ninstr);
        obj.ENVELOPEs( instr : instr + ninstr -1 ) = repmat(7,1,ninstr);
        obj.FLAGs( instr : instr + ninstr -1 )     = zeros(1,ninstr);
        obj.OPCODEs( instr : instr + ninstr -1 )   = zeros(1,ninstr);
        obj.DELAYs( instr : instr + ninstr -1 )    = [ repmat(0.1,1,ninstr-1), time_duration ]; %time in us
        
        % Set the appropriate number increase to the instructions
        instr = instr + ninstr;
        
    % SET GRADIENTS WITH COIL RISE TRACKING
    case 'SETGRADSRISE'
        ninstr = numel( choice_varargin{1}(1,:) );
        timeoffset = [ 0.2713, 0.5855-0.2713, 1-0.5855];
        ampoffset  = [ 0.3336 , 0.6490 , 1 ];
        
        % First set the appropriate gradient strengths during the rise and flat sequence
        obj.AMPs( instr : instr + 3*ninstr -1 )      = [ ampoffset(1) * ( choice_varargin{1}(1,:) - choice_varargin{1}(2,:) ) + choice_varargin{1}(2,:) , ...
                                                         ampoffset(2) * ( choice_varargin{1}(1,:) - choice_varargin{1}(2,:) ) + choice_varargin{1}(2,:) , ...
                                                         ampoffset(3) * ( choice_varargin{1}(1,:) - choice_varargin{1}(2,:) ) + choice_varargin{1}(2,:) ];
        obj.DACs( instr : instr + 3*ninstr -1 )      = [ choice_varargin{1}(3,:) , choice_varargin{1}(3,:) , choice_varargin{1}(3,:) ];
        obj.WRITEs( instr : instr + 3*ninstr -1 )    = ones(1,ninstr*3);
        obj.UPDATEs( instr : instr + 3*ninstr -1 )   = ones(1,ninstr*3);
        obj.CLEARs( instr : instr + 3*ninstr -1 )    = zeros(1,ninstr*3);
        obj.FREQs( instr : instr + 3*ninstr -1 )     = zeros(1,ninstr*3);
        obj.TXs( instr : instr + 3*ninstr -1 )       = zeros(1,ninstr*3);
        obj.PHASEs( instr : instr + 3*ninstr -1 )    = zeros(1,ninstr*3);
        obj.PhResets( instr : instr + 3*ninstr -1 )  = [ zeros(1,ninstr*3-1), choice_varargin{2} ];
        obj.RXs( instr : instr + 3*ninstr -1 )       = zeros(1,ninstr*3);
        obj.ENVELOPEs( instr : instr + 3*ninstr -1 ) = repmat(7,1,ninstr*3);
        obj.FLAGs( instr : instr + 3*ninstr -1 )     = zeros(1,ninstr*3);
        obj.OPCODEs( instr : instr + 3*ninstr -1 )   = zeros(1,ninstr*3);
        obj.DELAYs( instr : instr + 3*ninstr -1 )    = max( 0.1, [ repmat(0.1,1,ninstr-1), timeoffset(1) * choice_varargin{3} , ...
                                                       repmat(0.1,1,ninstr-1), timeoffset(2) * choice_varargin{3} , ...
                                                       repmat(0.1,1,ninstr-1), timeoffset(3) * choice_varargin{3} + time_duration ] );
        
            % Set the appropriate number increase to the instructions
            instr = instr + 3 * ninstr;
            
    % SET GRADIENTS WITH COIL FALL TRACKING
    case 'SETGRADSFALL'
        ninstr = numel( choice_varargin{1}(1,:) );
        timeoffset = [ 0.2713, 0.5855-0.2713, 1-0.5855];
        ampoffset  = [ 0.3336 , 0.6490 , 1 ];
        
        % First set the appropriate gradient strengths during the rise and flat sequence
        obj.AMPs( instr : instr + 3*ninstr -1 )      = [ ampoffset(1) * ( choice_varargin{1}(2,:) - choice_varargin{1}(1,:) ) + choice_varargin{1}(1,:) , ...
                                                         ampoffset(2) * ( choice_varargin{1}(2,:) - choice_varargin{1}(1,:) ) + choice_varargin{1}(1,:) , ...
                                                         ampoffset(3) * ( choice_varargin{1}(2,:) - choice_varargin{1}(1,:) ) + choice_varargin{1}(1,:) ];
        obj.DACs( instr : instr + 3*ninstr -1 )      = [ choice_varargin{1}(3,:) , choice_varargin{1}(3,:) , choice_varargin{1}(3,:) ];
        obj.WRITEs( instr : instr + 3*ninstr -1 )    = ones(1,ninstr*3);
        obj.UPDATEs( instr : instr + 3*ninstr -1 )   = ones(1,ninstr*3);
        obj.CLEARs( instr : instr + 3*ninstr -1 )    = zeros(1,ninstr*3);
        obj.FREQs( instr : instr + 3*ninstr -1 )     = zeros(1,ninstr*3);
        obj.TXs( instr : instr + 3*ninstr -1 )       = zeros(1,ninstr*3);
        obj.PHASEs( instr : instr + 3*ninstr -1 )    = zeros(1,ninstr*3);
        obj.PhResets( instr : instr + 3*ninstr -1 )  = [ zeros(1,ninstr*3-1), choice_varargin{2} ];
        obj.RXs( instr : instr + 3*ninstr -1 )       = zeros(1,ninstr*3);
        obj.ENVELOPEs( instr : instr + 3*ninstr -1 ) = repmat(7,1,ninstr*3);
        obj.FLAGs( instr : instr + 3*ninstr -1 )     = zeros(1,ninstr*3);
        obj.OPCODEs( instr : instr + 3*ninstr -1 )   = zeros(1,ninstr*3);
        obj.DELAYs( instr : instr + 3*ninstr -1 )    = max( 0.1, [ repmat(0.1,1,ninstr-1), timeoffset(1) * choice_varargin{3} , ...
                                                       repmat(0.1,1,ninstr-1), timeoffset(2) * choice_varargin{3} , ...
                                                       repmat(0.1,1,ninstr-1), timeoffset(3) * choice_varargin{3} + time_duration ] );
        
            % Set the appropriate number increase to the instructions
            instr = instr + 3 * ninstr;
            
    % SET GRADIENTS WITH A TRAPEZOIDAL SHAPE
    case 'SETGRADSTRAP'
        ninstr = numel( choice_varargin{1}(1,:) );
        [shimS,shimP,shimR]=SelectAxes(obj);
        
        % First set the appropriate gradient strengths
        obj.AMPs( instr : instr + ninstr -1 )      = choice_varargin{1}(1,:);
        obj.DACs( instr : instr + ninstr -1 )      = choice_varargin{1}(2,:);
        obj.WRITEs( instr : instr + ninstr -1 )    = ones(1,ninstr);
        obj.UPDATEs( instr : instr + ninstr -1 )   = ones(1,ninstr);
        obj.CLEARs( instr : instr + ninstr -1 )    = zeros(1,ninstr);
        obj.FREQs( instr : instr + ninstr -1 )     = zeros(1,ninstr);
        obj.TXs( instr : instr + ninstr -1 )       = zeros(1,ninstr);
        obj.PHASEs( instr : instr + ninstr -1 )    = zeros(1,ninstr);
        obj.PhResets( instr : instr + ninstr -1 )  = zeros(1,ninstr);
        obj.RXs( instr : instr + ninstr -1 )       = zeros(1,ninstr);
        obj.ENVELOPEs( instr : instr + ninstr -1 ) = repmat(7,1,ninstr);
        obj.FLAGs( instr : instr + ninstr -1 )     = zeros(1,ninstr);
        obj.OPCODEs( instr : instr + ninstr -1 )   = zeros(1,ninstr);
        obj.DELAYs( instr : instr + ninstr -1 )    = [ repmat(0.1,1,ninstr-1), choice_varargin{3} + time_duration ]; %time in us
        
            % Set the appropriate number increase to the instructions
            instr = instr + ninstr;
        
        % Then add in the gradient fall
        ninstr = 3;
        obj.AMPs( instr : instr + ninstr -1 )      = [ shimS shimP shimR ];
        obj.DACs( instr : instr + ninstr -1 )      = [ obj.selectSlice , obj.selectPhase, obj.selectReadout ];
        obj.WRITEs( instr : instr + ninstr -1 )    = ones(1,ninstr);
        obj.UPDATEs( instr : instr + ninstr -1 )   = ones(1,ninstr);
        obj.CLEARs( instr : instr + ninstr -1 )    = zeros(1,ninstr);
        obj.FREQs( instr : instr + ninstr -1 )     = zeros(1,ninstr);
        obj.TXs( instr : instr + ninstr -1 )       = zeros(1,ninstr);
        obj.PHASEs( instr : instr + ninstr -1 )    = zeros(1,ninstr);
        obj.PhResets( instr : instr + ninstr -1 )  = [ zeros(1,ninstr-1), choice_varargin{2} ];
        obj.RXs( instr : instr + ninstr -1 )       = zeros(1,ninstr);
        obj.ENVELOPEs( instr : instr + ninstr -1 ) = repmat(7,1,ninstr);
        obj.FLAGs( instr : instr + ninstr -1 )     = zeros(1,ninstr);
        obj.OPCODEs( instr : instr + ninstr -1 )   = zeros(1,ninstr);
        obj.DELAYs( instr : instr + ninstr -1 )    = [ repmat(0.1,1,ninstr-1), choice_varargin{3} ]; %time in us
        
        % Set the appropriate number increase to the instructions
        instr = instr + ninstr;
        
    % SET GRADIENTS WITH A TRAPEZOIDAL SHAPE WITH PRESCRIBED TIME POINTS TO BETTER MATCH GROWTH
    case 'SETGRADSTRAPMATCH'
        ninstr = numel( choice_varargin{1}(1,:) );
        timeoffset = [ 0.2713, 0.5855-0.2713, 1-0.5855];
        ampoffset  = [ 0.3336, 0.6490, 1 ];
        
        % First set the appropriate gradient strengths during the rise and flat sequence
        obj.AMPs( instr : instr + 3*ninstr -1 )      = [ ampoffset(1) * ( choice_varargin{1}(1,:) - choice_varargin{1}(2,:) ) + choice_varargin{1}(2,:) , ...
                                                         ampoffset(2) * ( choice_varargin{1}(1,:) - choice_varargin{1}(2,:) ) + choice_varargin{1}(2,:) , ...
                                                         ampoffset(3) * ( choice_varargin{1}(1,:) - choice_varargin{1}(2,:) ) + choice_varargin{1}(2,:) ];
        obj.DACs( instr : instr + 3*ninstr -1 )      = [ choice_varargin{1}(3,:) , choice_varargin{1}(3,:) , choice_varargin{1}(3,:) ];
        obj.WRITEs( instr : instr + 3*ninstr -1 )    = ones(1,ninstr*3);
        obj.UPDATEs( instr : instr + 3*ninstr -1 )   = ones(1,ninstr*3);
        obj.CLEARs( instr : instr + 3*ninstr -1 )    = zeros(1,ninstr*3);
        obj.FREQs( instr : instr + 3*ninstr -1 )     = zeros(1,ninstr*3);
        obj.TXs( instr : instr + 3*ninstr -1 )       = zeros(1,ninstr*3);
        obj.PHASEs( instr : instr + 3*ninstr -1 )    = zeros(1,ninstr*3);
        obj.PhResets( instr : instr + 3*ninstr -1 )  = zeros(1,ninstr*3);
        obj.RXs( instr : instr + 3*ninstr -1 )       = zeros(1,ninstr*3);
        obj.ENVELOPEs( instr : instr + 3*ninstr -1 ) = repmat(7,1,ninstr*3);
        obj.FLAGs( instr : instr + 3*ninstr -1 )     = zeros(1,ninstr*3);
        obj.OPCODEs( instr : instr + 3*ninstr -1 )   = zeros(1,ninstr*3);
        obj.DELAYs( instr : instr + 3*ninstr -1 )    = max( 0.1, [ repmat(0.1,1,ninstr-1), timeoffset(1) * choice_varargin{3} , ...
                                                       repmat(0.1,1,ninstr-1), timeoffset(2) * choice_varargin{3} , ...
                                                       repmat(0.1,1,ninstr-1), timeoffset(3) * choice_varargin{3} + time_duration ] );
        
            % Set the appropriate number increase to the instructions
            instr = instr + 3 * ninstr;
        
        % Then add in the gradient fall
        obj.AMPs( instr : instr + 3*ninstr -1 )      = [ ampoffset(1) * ( choice_varargin{1}(2,:) - choice_varargin{1}(1,:) ) + choice_varargin{1}(1,:) , ...
                                                         ampoffset(2) * ( choice_varargin{1}(2,:) - choice_varargin{1}(1,:) ) + choice_varargin{1}(1,:) , ...
                                                         ampoffset(3) * ( choice_varargin{1}(2,:) - choice_varargin{1}(1,:) ) + choice_varargin{1}(1,:) ];
        obj.DACs( instr : instr + 3*ninstr -1 )      = [ choice_varargin{1}(3,:) , choice_varargin{1}(3,:) , choice_varargin{1}(3,:) ];
        obj.WRITEs( instr : instr + 3*ninstr -1 )    = ones(1,ninstr*3);
        obj.UPDATEs( instr : instr + 3*ninstr -1 )   = ones(1,ninstr*3);
        obj.CLEARs( instr : instr + 3*ninstr -1 )    = zeros(1,ninstr*3);
        obj.FREQs( instr : instr + 3*ninstr -1 )     = zeros(1,ninstr*3);
        obj.TXs( instr : instr + 3*ninstr -1 )       = zeros(1,ninstr*3);
        obj.PHASEs( instr : instr + 3*ninstr -1 )    = [ zeros(1,ninstr*3-1), choice_varargin{2} ];
        obj.PhResets( instr : instr + 3*ninstr -1 )  = zeros(1,ninstr*3);
        obj.RXs( instr : instr + 3*ninstr -1 )       = zeros(1,ninstr*3);
        obj.ENVELOPEs( instr : instr + 3*ninstr -1 ) = repmat(7,1,ninstr*3);
        obj.FLAGs( instr : instr + 3*ninstr -1 )     = zeros(1,ninstr*3);
        obj.OPCODEs( instr : instr + 3*ninstr -1 )   = zeros(1,ninstr*3);
        obj.DELAYs( instr : instr + 3*ninstr -1 )    = max( 0.1, [ repmat(0.1,1,ninstr-1), timeoffset(1) * choice_varargin{3} , ...
                                                       repmat(0.1,1,ninstr-1), timeoffset(2) * choice_varargin{3} , ...
                                                       repmat(0.1,1,ninstr-1), timeoffset(3) * choice_varargin{3} ] );
        
            % Set the appropriate number increase to the instructions
            instr = instr + 3 * ninstr;
        
    % SET RFPulse
    case 'RFPulse'
        obj.AMPs( instr )      = 0.0;
        obj.DACs( instr )      = 3;
        obj.WRITEs( instr )    = 0;
        obj.UPDATEs( instr )   = 0;
        obj.CLEARs( instr )    = 0;
        obj.FREQs( instr )     = choice_varargin{4};
        obj.TXs( instr )       = 1;
        obj.PHASEs( instr )    = choice_varargin{2};
        obj.PhResets( instr )  = 0;
        obj.RXs( instr )       = 0;
        if choice_varargin{1}
            obj.ENVELOPEs( instr ) = choice_varargin{3};
        else
            obj.ENVELOPEs( instr ) = 7;
        end
        obj.FLAGs( instr )     = 1;
        obj.OPCODEs( instr )   = 0;
        obj.DELAYs( instr )    = time_duration; %time in us
        
        % Set the appropriate number increase to the instructions
        instr = instr + 1;
        
    % SET RXs
    case 'RXs'
        ninstr = numel( choice_varargin{1}(1,:) );
        obj.AMPs( instr : instr + ninstr -1 )      = choice_varargin{1}(1,:);
        obj.DACs( instr : instr + ninstr -1 )      = choice_varargin{1}(2,:);
        obj.WRITEs( instr : instr + ninstr -1 )    = ones(1,ninstr);
        obj.UPDATEs( instr : instr + ninstr -1 )   = ones(1,ninstr);
        obj.CLEARs( instr : instr + ninstr -1 )    = zeros(1,ninstr);
        obj.FREQs( instr : instr + ninstr -1 )     = [ zeros(1,ninstr-1), choice_varargin{2} ];
        obj.TXs( instr : instr + ninstr -1 )       = zeros(1,ninstr);
        obj.PHASEs( instr : instr + ninstr -1 )    = zeros(1,ninstr);
        obj.PhResets( instr : instr + ninstr -1 )  = zeros(1,ninstr);
        obj.RXs( instr : instr + ninstr -1 )       = [ zeros(1,ninstr-1), 1 ];
        obj.ENVELOPEs( instr : instr + ninstr -1 ) = repmat(7,1,ninstr);
        obj.FLAGs( instr : instr + ninstr -1 )     = zeros(1,ninstr);
        obj.OPCODEs( instr : instr + ninstr -1 )   = zeros(1,ninstr);
        obj.DELAYs( instr : instr + ninstr -1 )    = [ repmat(0.1,1,ninstr-1), time_duration ]; %time in us
        
        % Set the appropriate number increase to the instructions
        instr = instr + ninstr;
        
    case 'RESET'
        obj.AMPs( instr )      = 0.0;
        obj.DACs( instr )      = 3;
        obj.WRITEs( instr )    = 1;
        obj.UPDATEs( instr )   = 1;
        obj.CLEARs( instr )    = 1;
        obj.FREQs( instr )     = 0;
        obj.TXs( instr )       = 0;
        obj.PHASEs( instr )    = 0;
        obj.PhResets( instr )  = 0;
        obj.RXs( instr )       = 0;
        obj.ENVELOPEs( instr ) = 7;
        obj.FLAGs( instr )     = 0;
        obj.OPCODEs( instr )   = 0;
        obj.DELAYs( instr )    = time_duration; %time in us
        
        % Set the appropriate number increase to the instructions
        instr = instr + 1;
        
end % end switch for which instruction group

% CLEAR the choice_varargin variable
choice_varargin = {};
end % end program instruction