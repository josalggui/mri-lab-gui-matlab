function unwrapped = PhaseUnwrap( wrapped, np, dp, normal )

unwrapped = wrapped;
nb = numel(wrapped)/np;

for j = 1:nb
    switch normal
        case 0
            for i = 2:np
                if wrapped((j-1)*np+i)-wrapped((j-1)*np+i-1) < dp
                    unwrapped((j-1)*np+i:j*np) = unwrapped((j-1)*np+i:j*np) + 2*pi;
                end
            end
        case 1
            unwrapped((j-1)*np+1:(j-1)*np) = unwrap(wrapped((j-1)*np+1:(j-1)*np)); 
    end
end