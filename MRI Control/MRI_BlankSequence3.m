classdef MRI_BlankSequence3 < handle
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    %  Elena Diaz Caballero
    
    properties
        figSeq; % to store the handles for the SequenceDiagram figure
        cData;
        fidData;
        fftData1D;
        timeVector;
        freqVector;
        ProgramDir = 'C:\SpinCore\MRI_SpinAPI\examples';
        path_file;         
        ProgramName;
        SequenceName;
        OutputFilename;
        OutputFilename_CF;
        InputParameters;
        InputParNames;
        InputParValues;
        InputParValuesMin;
        InputParValuesMax;
        InputParUnits;
        InputParHidden; %this parameters are not shown in the input table
        OutputParameters;
        OutputParNames;
        OutputParValues;
        OutputParUnits;
        RF_Shape = 0;
        tx_phase = 0;
        blankingBit = 0;
        adcOffset = 47; % SPINCORE 2019.09.12
        SliceFreqOffset = 0;
        slice_en = 0;
        phase_en = 0;
        readout_en = 0;
        spoiler_en = 0;
        shimS_en = 0;
        shimP_en = 0;
        shimR_en = 0;
        RunCal = false;
        calibration_en = false;
        CalCheck = false;
        FIDPhase = 0;
        NAverages = 10;
        averaging = 0;
        autoPhasing = 0;
        AbortRun = false;
        ReferenceCF = 0;
        MaxCF_shift=20*MyUnits.kHz;
        AMPs =[];
        AMPs_string;
        DACs=[];
        DACs_string;
        WRITEs=[];
        WRITEs_string;
        UPDATEs=[];
        UPDATEs_string;
        CLEARs=[];
        CLEARs_string;
        FREQs=[];
        FREQs_string;
        TXs=[];
        TXs_string;
        PHASEs=[];
        PHASEs_string;
        PhResets=[];
        PhResets_string;
        RXs=[];
        RXs_string;
        ENVELOPEs=[];
        ENVELOPEs_string;
        FLAGs=[];
        FLAGs_string;
        OPCODEs=[];
        OPCODEs_string;
        DELAYs=[];
        DELAYs_string;
        maxInstructions = 2000;
        nInstructions;
        nline_reads;
        Kspace_readout;
        KSpace_phase;
        selectReadout;
        selectPhase;
        selectSlice;
        TE = 0;
        TR = 0;
        TTotal = 0;
        calibrationactive = 0;
        %% Unused
        %kwid= 3;
        %overg= 6;
        %DCF = [];
    end
    
    methods
        function obj = MRI_BlankSequence3
            obj.InputParameters = {...
                'SHIMSLICE','SHIMPHASE','SHIMREADOUT','NSCANS',...
                'NPOINTS','NSLICES','FREQUENCY','SPECTRALWIDTH',...
                'RFAMPLITUDE','PULSETIME','BLANKINGDELAY','TRANSIENTTIME','COILRISETIME',...
                'PHASE','READOUT','SLICE',...
                'SPOILERAMP', 'SPOILERTIME'...
                };
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT'}; %these pars will not be shown in a par table
            obj.InputParNames = containers.Map(obj.InputParameters,...
                {...
                'Slice Shimming','Phase Shimming','Readout Shimming','Number of Scans',...
                'Number of Points', 'Number of Slices', 'Frequency', 'SpectralWidth',...
                'RF Amplitude','Pulse Time','Blanking Delay','Transient Time', 'Coil Rise Time',...
                'Phase Axis','Readout Axis', 'Slice Axis',...
                'Spoiler Amplitude', 'Spoiler Time'...
                });
            obj.InputParValues = containers.Map(obj.InputParameters,...
                {...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,NaN,...
                '','','',...
                NaN,NaN...
                });
            obj.InputParValuesMin = containers.Map(obj.InputParameters,...
                {...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,NaN,...
                '','','',...
                NaN,NaN...
                });
            obj.InputParValuesMax = containers.Map(obj.InputParameters,...
                {...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,NaN,...
               '','','',...
                NaN,NaN...
                });
            obj.InputParUnits = containers.Map(obj.InputParameters,...
                {...
                '','','','',...
                '','','MHz','kHz',...
                '','us','ms','us','us',...
                '','','',...
                '','us'...
                });
            obj.OutputParameters = {
                'ADCDECIMATION','ACTUALSPECTRALWIDTH','ACQUISITIONTIME','LASTFREQUENCY','SNRMEANVALUE'    
                };
            obj.OutputParNames = containers.Map(obj.OutputParameters,...
                {...
                'ADC Frequency Decimation', 'Actual Spectral Width', 'Acquisition Time','Last RF Frequency','SNR Mean Value'...    
                });
            obj.OutputParValues = containers.Map(obj.OutputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN...   
                });
            obj.OutputParUnits = containers.Map(obj.OutputParameters,...
                {...
                '','MHz','ms','MHz',''...  
                });
        end
        function SetDefaultParameters(obj)
            [pathstr, name, ext] = fileparts(mfilename('fullpath'));
            obj.path_file = fullfile(pathstr,'FileDir'); % this is the path of the folder where all the generated files
                                                     % will be written and read from
            obj.OutputFilename = fullfile(obj.path_file,'FID_temp');
            obj.OutputFilename_CF = fullfile(obj.path_file,'FID_temp_CF');            
            obj.SetParameter('SHIMSLICE',0);
            obj.SetParameter('SHIMPHASE',0);
            obj.SetParameter('SHIMREADOUT',0);
            obj.SetParameter('NPOINTS',600);
            obj.SetParameter('NSCANS',1);
            obj.SetParameter('NSLICES',1);
            obj.SetParameter('FREQUENCY',14.5*MyUnits.MHz);
            obj.SetParameter('SPECTRALWIDTH',600*MyUnits.kHz);
            obj.SetParameter('RFAMPLITUDE',0.3);
            obj.SetParameter('PULSETIME',12*MyUnits.us);
            obj.SetParameter('BLANKINGDELAY',0.06*MyUnits.ms);
            obj.SetParameter('TRANSIENTTIME',0.1*MyUnits.us);
            obj.SetParameter('COILRISETIME',0.1*MyUnits.us);
            obj.SetParameter('PHASE','x');
            obj.SetParameter('READOUT','y');
            obj.SetParameter('SLICE','z');
            obj.SetParameter('SPOILERTIME',0.1*MyUnits.us);
            obj.SetParameter('SPOILERAMP',0.1);
        end
        function SetParameter(obj,par,value)
%             if ischar(value); value = str2num(value);end
            if (isempty(value) || isnan(value)); return; end
            if any(strcmp(obj.InputParameters,par))
                %               fprintf('Parameter %s is set to %f\n',par,value);
                if value < obj.InputParValuesMin(par)
                    obj.InputParValues(par) = obj.InputParValuesMin(par);
                elseif value > obj.InputParValuesMax(par)
                    obj.InputParValues(par) = obj.InputParValuesMax(par);
                else
                    obj.InputParValues(par) = value;
                end
            elseif any(strcmp(obj.OutputParameters,par))
                obj.OutputParValues(par) = value;
            else
                fprintf('Parameter %s is not found\n',par);
            end
        end
        function SetParameterMin(obj,par,value)
            if any(strcmp(obj.InputParameters,par))
                obj.InputParValuesMin(par) = value;
            else
                fprintf('Input Parameter %s is not found\n',par);
            end
        end
        function SetScanParameterValue(obj,par,value)
            obj.SetParameter(par,value)
        end
        function SetParameterMax(obj,par,value)
            if any(strcmp(obj.InputParameters,par))
                obj.InputParValuesMax(par) = value;
            else
                fprintf('Input Parameter %s is not found\n',par);
            end
        end
        function SetParameters(obj,ParValuesMap)
            for k=1:ParValuesMap.length()
                SetParameter(ParValuesMap.keys(k),ParValuesMap.values(k));
            end
        end
        function value = GetParameter(obj,par)
            if any(strcmp(obj.InputParameters,par))
                value = obj.InputParValues(par);
            elseif any(strcmp(obj.OutputParameters,par))
                value = obj.OutputParValues(par);
            else
                value = NaN;
            end
        end
        function value = GetParameterUnit(obj,par)
            if any(strcmp(obj.InputParameters,par))
                value = obj.InputParUnits(par);
            elseif any(strcmp(obj.OutputParameters,par))
                value = obj.OutputParUnits(par);
            else
                value = '';
            end
        end
        function value = GetParameterName(obj,par)
            if any(strcmp(obj.InputParameters,par))
                value = obj.InputParNames(par);
            elseif any(strcmp(obj.OutputParameters,par))
                value = obj.OutputParNames(par);
            else
                value = '';
            end
        end
        function [pars,names,values,units] = GetInputParameters(obj)
            pars = obj.InputParameters;
            names = containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());%Matlab trick to get a new copy of container
            values = containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());
            units = containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());
            for k=1:length(obj.InputParHidden)
                par = obj.InputParHidden{k};
                pars(strcmp(pars,par)) = [];
            end
            names.remove(obj.InputParHidden);
            values.remove(obj.InputParHidden);
            units.remove(obj.InputParHidden);
            
        end
        function [pars,names,values,units] = GetOutputParameters(obj)
            pars = obj.OutputParameters;
            names = obj.OutputParNames;
            values = obj.OutputParValues;
            units = obj.OutputParUnits;
        end
        function sw = ExistsInputParameter(obj,par)
            sw = any(strcmp(obj.InputParameters,par));
        end
        function SetFIDPhase(obj,phase)
            obj.FIDPhase = phase;
        end
        function SetNAverages(obj,n)
            obj.NAverages = n;
        end
        function EnableAveraging(obj,sw)
            obj.averaging = sw;
        end
        function EnableAutoPhase(obj,sw)
            obj.autoPhasing = sw;
        end
        function SetRFShape(obj,shape_type)
            obj.RF_Shape = shape_type;
        end
        function SetShimS(obj,v)
            obj.SetParameter('SHIMSLICE',v);
        end
        function SetShimP(obj,v)
            obj.SetParameter('SHIMPHASE',v);
        end
        function SetShimR(obj,v)
            obj.SetParameter('SHIMREADOUT',v);
        end
        function v = GetShimS(obj)
            v = obj.GetParameter('SHIMSLICE');
        end
        function v = GetShimP(obj)
            v = obj.GetParameter('SHIMPHASE');
        end
        function v = GetShimR(obj)
            v = obj.GetParameter('SHIMREADOUT');
        end
        function EnableSliceGradient(obj,sw)
            obj.slice_en = sw;
        end
        function EnablePhaseGradient(obj,sw)
            obj.phase_en = sw;
        end
        function EnableReadoutGradient(obj,sw)
            obj.readout_en = sw;
        end
        function EnableSpoilerGradient(obj,sw)
            obj.spoiler_en = sw;
        end
        function EnableShimming(obj,sw)
            obj.EnableShimmingSlice(sw);
            obj.EnableShimmingPhase(sw);
            obj.EnableShimmingReadout(sw);
        end
        function EnableShimmingSlice(obj,sw)
            obj.shimS_en = sw;
        end
        function EnableShimmingPhase(obj,sw)
            obj.shimP_en = sw;
        end
        function EnableShimmingReadout(obj,sw)
            obj.shimR_en = sw;
        end
        function EnableCalibration(obj,sw)
%             obj.RunCal = sw;
            obj.calibration_en = sw;
            obj.CalCheck = sw;
        end
        function CalibrateImage(obj,sw)
            obj.calibration_en = sw;
        end

        function [fidData,fftData] = GetData1D(obj)
            fidData = obj.fidData;
            fftData = obj.fftData1D;
        end
        function SaveFID(obj,fname)
            saveFile = fopen(fullfile(obj.path_file,fname),'w');
            for ii = 1:length(obj.timeVector)
                fprintf(saveFile,'%f %f %f\n',obj.timeVector(ii)/MyUnits.us,real(obj.fidData(ii)),imag(obj.fidData(ii)));
            end
            fclose(saveFile);
        end
        function Abort(obj)
            obj.AbortRun = true;
        end
        function [shimS,shimP,shimR]=SelectAxes(obj) %set the appropriate axes as were decided
            if (obj.GetParameter('READOUT')==obj.GetParameter('PHASE'))
              ed = errordlg('Readout and Phase gradient cannot be applied to the same axis','ConfigFileError','modal');
              uiwait(ed);
              return;
            end
            switch num2str(obj.GetParameter('READOUT'))
                case 'x'
                    obj.selectReadout = 2; shimR = obj.GetParameter('SHIMREADOUT');
                case 'y'
                    obj.selectReadout = 0; shimR = obj.GetParameter('SHIMPHASE');
                case 'z'
                    obj.selectReadout = 1; shimR = obj.GetParameter('SHIMSLICE');
                otherwise
                    obj.selectReadout = 2; shimR = obj.GetParameter('SHIMREADOUT');
            end
            switch num2str(obj.GetParameter('PHASE'))
                case 'x'
                    obj.selectPhase = 2; shimP = obj.GetParameter('SHIMREADOUT');
                case 'y'
                    obj.selectPhase = 0; shimP = obj.GetParameter('SHIMPHASE');
                case 'z'
                    obj.selectPhase = 1; shimP = obj.GetParameter('SHIMSLICE');
                otherwise
                    obj.selectPhase = 0; shimP = obj.GetParameter('SHIMPHASE');
            end
            obj.selectSlice=3-(obj.selectPhase+obj.selectReadout); 
            switch obj.selectSlice;
                case 0
                    shimS = obj.GetParameter('SHIMPHASE');
                case 2
                    shimS = obj.GetParameter('SHIMREADOUT');
                case 1
                    shimS = obj.GetParameter('SHIMSLICE');
                otherwise
                    shimS = obj.GetParameter('SHIMSLICE');
            end
            
            if (obj.shimS_en==0)
                shimS = 0;
            end
            if (obj.shimP_en==0)
                shimP = 0;
            end
            if (obj.shimR_en==0)
                shimR = 0;
            end
        end
        function CenterFrequency(obj,fname_CF)
            global MRIDATA
            % GET CENTER FREQUENCY
            MRIDATA.TempCF = [];
            
             
            [status,result] = system(fullfile(obj.path_file,fname_CF));
            
            
            if status == 0 && ~isempty(result)
                C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                res = C{:};
                obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
            end
            
            DataCF = load(sprintf('%s.txt',obj.OutputFilename_CF));
            cDataCF = complex(DataCF(:,1),DataCF(:,2));
            
            MRIDATA.TempCF = cDataCF;
            
            cDataCF = mean( reshape(cDataCF,obj.GetParameter('NPOINTS'),length(cDataCF)/obj.GetParameter('NPOINTS')), 2);
            fftData1D = flipud(fftshift(fft(cDataCF)));
            acq_time = obj.GetParameter('ACQUISITIONTIME');
            freqVector = (ceil(-length(fftData1D)/2):ceil(length(fftData1D)/2)-1)/acq_time;
            idx = find(fftData1D==max(fftData1D),1,'first');
            deltaFreq = freqVector(idx);

            obj.SetParameter('FREQUENCY',obj.GetParameter('FREQUENCY')+deltaFreq);   
            
            if abs(obj.ReferenceCF - obj.GetParameter('FREQUENCY')) >= obj.MaxCF_shift
                load gong.mat;
                sound(y, Fs);
                obj.ReferenceCF = obj.GetParameter('FREQUENCY');
                clear Fs y
                h_warn = warndlg(char(strcat('Please tune coil to: [MHz]', num2str( obj.GetParameter('FREQUENCY')/MyUnits.MHz))),'!!! Waiting to Continue !!!');
                uiwait(h_warn);
            end
        end
        function WriteBatchFile(obj,fname)
            % WRITE BATCH FILE AND RUN            
            fid = fopen(fullfile(obj.path_file,fname), 'Wt');
            pars = obj.InputParameters;
            values = obj.InputParValues;
            fprintf( fid, '@echo off\n');
            fprintf( fid, 'SET Debug=%d\n',0);
            if strcmp(fname(end-6:end-4),'_CF')
                    fprintf( fid, 'SET outputFilename=%s\n',obj.OutputFilename_CF);
            else
                    fprintf( fid, 'SET outputFilename=%s\n',obj.OutputFilename);
            end
            fprintf( fid, 'REM ======================\n');
            fprintf( fid, 'REM Acquisition Parameters\n' );
            fprintf( fid, 'REM ======================\n');
            fprintf( fid, 'SET nPoints=%u\n', values('NPOINTS') );
            fprintf( fid, 'SET nScans=%u\n', values('NSCANS') );
            fprintf( fid, 'SET nline_reads=%u\n', obj.nline_reads);
            fprintf( fid, 'SET spectrometerFrequency_MHz=%f\n', values('FREQUENCY')/MyUnits.MHz); 
            fprintf( fid, 'SET sliceFreqOffset_MHz=%f\n',obj.SliceFreqOffset/MyUnits.MHz);            
            fprintf( fid, 'SET spectralWidth_kHz=%f\n',values('SPECTRALWIDTH')/MyUnits.kHz);
                        
%             fprintf( fid, 'SET linebroadening_value=%f\n',values('LINEBROADENING'));
            
            fprintf( fid, 'REM ===================\n');
            fprintf( fid, 'REM RF Pulse Parameters\n');
            fprintf( fid, 'REM ===================\n'  );
            fprintf( fid, 'SET RF_Shape=%u\n', obj.RF_Shape);
            fprintf( fid, 'SET amplitude=%f\n', values('RFAMPLITUDE'));
            fprintf( fid, 'SET pulseTime_us=%f\n', values('PULSETIME')/MyUnits.us);
            
            % Now put in the vectors of the amplitude selections
            fprintf( fid, 'REM ===================\n');
            fprintf( fid, 'REM Gradient Pulse Schema\n');
            fprintf( fid, 'REM ===================\n'  );
%            fprintf( fid, 'SET nPulses=%u', values('NPULSES'));
            fprintf( fid, 'SET nInstructions=%u\n', obj.nInstructions);
            fprintf( fid, 'SET AMPs=%s\n', obj.AMPs_string);
            fprintf( fid, 'SET DACs=%s\n', obj.DACs_string);
            fprintf( fid, 'SET WRITEs=%s\n', obj.WRITEs_string);
            fprintf( fid, 'SET UPDATEs=%s\n', obj.UPDATEs_string);
            fprintf( fid, 'SET CLEARs=%s\n', obj.CLEARs_string);
            fprintf( fid, 'SET FREQs=%s\n', obj.FREQs_string);
            fprintf( fid, 'SET RXs=%s\n', obj.RXs_string);
            fprintf( fid, 'SET TXs=%s\n', obj.TXs_string);
            fprintf( fid, 'SET PHASEs=%s\n', obj.PHASEs_string);
            fprintf( fid, 'SET PhResets=%s\n', obj.PhResets_string);
            fprintf( fid, 'SET DELAYs=%s\n', obj.DELAYs_string);
            fprintf( fid, 'SET ENVELOPEs=%s\n', obj.ENVELOPEs_string);
            
            fprintf( fid, 'REM ===================\n');
            fprintf( fid, 'REM Flags and configuration\n');
            fprintf( fid, 'REM ===================\n'  );
            fprintf( fid, 'SET FLAGs=%s\n', obj.FLAGs_string);
            fprintf( fid, 'SET OPCODEs=%s\n', obj.OPCODEs_string);
            fprintf( fid, 'SET adcOffset=%u\n', obj.adcOffset);
            
            fprintf( fid, 'echo. | "%s"',fullfile(obj.ProgramDir,obj.ProgramName)); %we need echo. to exit batch file in case of errors (it emulates typing ENTER)
            fclose(fid);
        end
        function [nSlices,nPhases,readout,slice,phase,spoiler,SSaux] = ConfigureGradients(obj, mode)
            % mode =  "0", normal execution of the sequence,
            %         "1", central frequency adjustment sequence,
            %         "2", calibration of the k-space,  RO slice + encoding gradient ON
            %         "3",                              RO slice + encoding gradient OFF
            %         "4",                              phase slice + encoding gradient ON
            %         "5",                              phase slice + encoding gradient OFF
            
            nSlices = obj.GetParameter('NSLICES');
            
%             if (abs(obj.EndPhase - obj.GetParameter('STARTPHASE')) - abs(obj.GetParameter('ENDPHASE') - obj.GetParameter('STARTPHASE'))) > 1e-6 %last batch may be smaller
%                 nPhases = mod(obj.GetParameter('NPHASES'),obj.PhasesPerBatch);
%                 obj.EndPhase = obj.GetParameter('ENDPHASE');
%             else
%                 nPhases = min(obj.GetParameter('NPHASES'),obj.PhasesPerBatch);
%             end
            nPhases = 1;
            
            SSaux=obj.selectSlice;
            spoiler =obj.spoiler_en;
            
            if mode == 1  % central frequency adjustment sequence
                
                nSlices = 1;
                nPhases = 1;
                readout =0; % None of the gradients should be enabled if we are setting the central frequency (CF)
                slice =0;
                phase =0;
                spoiler =0;
                
            elseif mode ==0  % normal execution of the sequence
                
                readout =obj.readout_en;
                slice =obj.slice_en;
                phase =obj.phase_en;
                spoiler =obj.spoiler_en;
                
            elseif mode == 2 % calibration of the k-space,  RO slice + encoding gradient ON
                
                SSaux=obj.selectReadout; % switching the slice select gradient to the corresponding gradient axis
                readout = 1;
                phase   = 0; % encoding gradients in all other axes are switched off
                slice   = 0;
                
            elseif mode == 3 %  RO slice + encoding gradient OFF
                
                SSaux=obj.selectReadout; % switching the slice select gradient to the corresponding gradient axis
                readout = 0;          % all spatial encoding gradients are switched off
                phase   = 0;
                slice   = 0;
                
            elseif mode == 4 % phase slice + encoding gradient ON
                
                SSaux=obj.selectPhase; % switching the slice select gradient to the corresponding gradient axis
                phase   = 1;
                readout = 0; % encoding gradients in all other axes are switched off
                slice   = 0;
                
            elseif mode == 5 % phase slice + encoding gradient OFF
                
                SSaux=obj.selectPhase; % switching the slice select gradient to the corresponding gradient axis
                phase   = 0;        % all spatial encoding gradients are switched off
                readout = 0;
                slice   = 0;
                
            end
            
        end
        function CreateSequence( obj, mode )
            % Function rewritten inside each particular sequence
            
            obj.StartSlice = obj.GetParameter('STARTSLICE');
            obj.EndSlice = obj.GetParameter('ENDSLICE');
            
        end
        function [cDataBatch_Cal]=getCalibrationData(obj,mode,fname_Cal,cDataBatch_Cal)
            obj.CreateSequence(mode);
            obj.Sequence2String;
            obj.WriteBatchFile(fname_Cal);
            [status,result] = system(fullfile(obj.path_file,fname_Cal));
            DataBatch_Cal = load(sprintf('%s.txt',obj.OutputFilename));
            cDataBatch_Cal = [ cDataBatch_Cal ; complex(DataBatch_Cal(:,1),DataBatch_Cal(:,2))];
        end
        
        function Sequence2String(obj)
            if length(obj.AMPs)<1
                return;
            end
            
            obj.AMPs_string = num2str(obj.AMPs(:).','%10.3f,');
            obj.DACs_string = num2str(obj.DACs(:).','%d,');
            obj.WRITEs_string = num2str(obj.WRITEs(:).','%d,');
            obj.UPDATEs_string = num2str(obj.UPDATEs(:).','%d,');
            obj.CLEARs_string = num2str(obj.CLEARs(:).','%d,');
            obj.FREQs_string = num2str(obj.FREQs(:).','%d,');
            obj.RXs_string = num2str(obj.RXs(:).','%d,');
            obj.TXs_string = num2str(obj.TXs(:).','%d,');
            obj.PHASEs_string = num2str(obj.PHASEs(:).','%d,');
            obj.PhResets_string = num2str(obj.PhResets(:).','%d,');
            obj.ENVELOPEs_string = num2str(obj.ENVELOPEs(:).','%d,');
            obj.FLAGs_string = num2str(obj.FLAGs(:).','%d,');
            obj.OPCODEs_string = num2str(obj.OPCODEs(:).','%d,');
            obj.DELAYs_string = num2str(obj.DELAYs(:).','%10.3f,');
            
            obj.AMPs_string(end) = []; 
            obj.DACs_string(end) = [];
            obj.WRITEs_string(end) = [];
            obj.UPDATEs_string(end) = [];
            obj.CLEARs_string(end) = [];
            obj.FREQs_string(end) = [];
            obj.RXs_string(end) = [];
            obj.TXs_string(end) = [];
            obj.PHASEs_string(end) = [];
            obj.PhResets_string(end) = [];
            obj.ENVELOPEs_string(end) = [];
            obj.FLAGs_string(end) = [];
            obj.OPCODEs_string(end) = [];
            obj.DELAYs_string(end) = [];
            
%             obj.AMPs_string     = num2str(obj.AMPs(1));
%             obj.DACs_string     = num2str(obj.DACs(1)); 
%             obj.WRITEs_string   = num2str(obj.WRITEs(1));  
%             obj.UPDATEs_string  = num2str(obj.UPDATEs(1));   
%             obj.CLEARs_string   = num2str(obj.CLEARs(1)); 
%             obj.FREQs_string    = num2str(obj.FREQs(1));   
%             obj.RXs_string      = num2str(obj.RXs(1)); 
%             obj.TXs_string      = num2str(obj.TXs(1));
%             obj.PHASEs_string   = num2str(obj.PHASEs(1));
%             obj.PhResets_string = num2str(obj.PhResets(1));  
%             obj.ENVELOPEs_string= num2str(obj.ENVELOPEs(1)); 
%             obj.FLAGs_string    = num2str(obj.FLAGs(1)); 
%             obj.OPCODEs_string 	= num2str(obj.OPCODEs(1));  
%             obj.DELAYs_string   = num2str(obj.DELAYs(1)); 
%              for k = 2:length(obj.AMPs)
%                   obj.AMPs_string     = char(strcat( obj.AMPs_string, ',', num2str(obj.AMPs(k))));
%                   obj.DACs_string     = char(strcat( obj.DACs_string, ',', num2str(obj.DACs(k)))); 
%                   obj.WRITEs_string   = char(strcat( obj.WRITEs_string, ',', num2str(obj.WRITEs(k)))); 
%                   obj.UPDATEs_string  = char(strcat( obj.UPDATEs_string, ',', num2str(obj.UPDATEs(k))));  
%                   obj.CLEARs_string   = char(strcat( obj.CLEARs_string, ',', num2str(obj.CLEARs(k)))); 
%                   obj.FREQs_string    = char(strcat( obj.FREQs_string, ',', num2str(obj.FREQs(k))));  
%                   obj.RXs_string      = char(strcat( obj.RXs_string, ',', num2str(obj.RXs(k)))); 
%                   obj.TXs_string      = char(strcat( obj.TXs_string, ',', num2str(obj.TXs(k))));  
%                   obj.PHASEs_string   = char(strcat( obj.PHASEs_string, ',', num2str(obj.PHASEs(k))));
%                   obj.PhResets_string = char(strcat( obj.PhResets_string, ',', num2str(obj.PhResets(k)))); 
%                   obj.ENVELOPEs_string= char(strcat( obj.ENVELOPEs_string, ',', num2str(obj.ENVELOPEs(k)))); 
%                   obj.FLAGs_string    = char(strcat( obj.FLAGs_string, ',', num2str(obj.FLAGs(k)))); 
%                   obj.OPCODEs_string 	= char(strcat( obj.OPCODEs_string, ',', num2str(obj.OPCODEs(k))));  
%                   obj.DELAYs_string   = char(strcat( obj.DELAYs_string, ',', num2str(obj.DELAYs(k)))); 
%             end
        end
        function UpdateTETR( obj )
            obj.TE = 0;
            obj.TR = 0;
            obj.TTotal = 0;
        end
        
        function SetFigureHandle(obj,h)
            obj.figSeq = h(1);
        end
        
        function plot_diagram(obj,ninstr_ini, ninstr_fin)
            
            % To plot the diagram of the sequence from the instruction
            % ninstr_ini to ninstr_fin
            
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            
            ninstr_repr=ninstr_fin-ninstr_ini+1; %number of represented instructions
            [blanking_bit,RF_out,slice_grad,phase_grad,readout_grad,acquisition,time]=deal(zeros(2*ninstr_repr,1));
            
            n=0; %Initializing the counter used to fill the vectors
            for ninstr=ninstr_ini:ninstr_fin
                n=n+1;
                if (obj.WRITEs(ninstr)==1 && obj.UPDATEs(ninstr)==1)
                    if (obj.DACs(ninstr)== SS)
                        slice_grad(2*n-1)= obj.AMPs(ninstr);
                        if (n>1)
                            phase_grad(2*n-1)= phase_grad(2*n-3); % The other gradients don't change
                            readout_grad(2*n-1)= readout_grad(2*n-3);
                        end
                    elseif (obj.DACs(ninstr)== PP)
                        phase_grad(2*n-1)= obj.AMPs(ninstr);
                        if (n>1)
                            slice_grad(2*n-1)= slice_grad(2*n-3); % The other gradients don't change
                            readout_grad(2*n-1)= readout_grad(2*n-3);
                        end
                    elseif (obj.DACs(ninstr)== RR)
                        readout_grad(2*n-1)= obj.AMPs(ninstr);
                        if (n>1)
                            phase_grad(2*n-1)= phase_grad(2*n-3); % The other gradients don't change
                            slice_grad(2*n-1)= slice_grad(2*n-3);
                        end
                    end
                end
                
                if (obj.WRITEs(ninstr)==0 && obj.CLEARs(ninstr)==0)
                    if (n>1)
                            phase_grad(2*n-1)= phase_grad(2*n-3); % All the gradients remain the same
                            readout_grad(2*n-1)= readout_grad(2*n-3);
                            slice_grad(2*n-1)= slice_grad(2*n-3);
                    end
                end
%                 if (obj.WRITEs(ninstr)==1)
%                 end
                
                if (obj.TXs(ninstr) == 1)
                    RF_out(2*n-1)=1;
                    slice_grad(2*n-1)= slice_grad(2*n-3);
                end
                if (obj.RXs(ninstr) == 1)
                    acquisition(2*n-1)=1;
                end
                if (obj.FLAGs(ninstr) == 1)
                    blanking_bit(2*n-1)=1;
                end
                if (obj.DACs(ninstr) == 3 && obj.CLEARs(ninstr) == 1)
                    slice_grad(2*n-1)= 0;
                    phase_grad(2*n-1)= 0;
                    readout_grad(2*n-1)= 0;
                end
                
                time(2*n)= time(2*n-1)+obj.DELAYs(ninstr);
                if (2*n+1 < 2*ninstr_repr)
                    time(2*n+1)=time(2*n)+0.1;
                end
            end
            
            blanking_bit(2:2:end) = blanking_bit(1:2:end-1);
            RF_out(2:2:end) = RF_out(1:2:end-1);
            slice_grad(2:2:end) = slice_grad(1:2:end-1);
            phase_grad(2:2:end) = phase_grad(1:2:end-1);
            readout_grad(2:2:end) = readout_grad(1:2:end-1);
            acquisition(2:2:end) = acquisition(1:2:end-1);
            
            figure(obj.figSeq);
            set(obj.figSeq,'defaultLineLineWidth',3,'defaultAxesFontUnits','normalized','defaultAxesFontsize', 0.22, 'defaultAxesYlim',[-1 1])
            set(obj.figSeq,'numbertitle','off','name','Sequence Diagram')
            subplot(6,1,1)
            plot(time,blanking_bit)
            ylabel({'Blanking'; 'bit'})
            grid
            
            subplot(6,1,2)
            plot(time,RF_out)
            ylabel('RF out')
            grid
            
            subplot(6,1,3)
            plot(time,slice_grad)
            ylabel({'Slice'; 'gradient'})
            grid
            
            subplot(6,1,4)
            plot(time,phase_grad)
            ylabel({'Phase'; 'gradient'})
            grid
            
            subplot(6,1,5)
            plot(time,readout_grad)
            ylabel({'Readout'; 'gradient'})
            grid
            
            subplot(6,1,6)
            plot(time,acquisition)
            ylabel('Acquisition')
            xlabel('Time (\mus)')
            grid
            
        end
    end
    
end

