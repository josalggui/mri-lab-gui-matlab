classdef MyUnits
    %UNTITLED4 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
        %length
        m = 1;
        cm = unitsratio('m','cm');
        mm = unitsratio('m','mm');
        um = unitsratio('m','micron');
        
        %time
        s = 1;
        ms = 1e-3*MyUnits.s;
        us = 1e-6*MyUnits.s;
        ns = 1e-9*MyUnits.s;
        min = 60*MyUnits.s;
        hour = 60*60*MyUnits.s;
        day = 60*60*24*MyUnits.s;
        year = 31557600*MyUnits.s;
        
        %mag. field
        T = 1;
        mT = 1e-3*MyUnits.T;
        uT = 1e-6*MyUnits.T;
        G = 1e-4*MyUnits.T;
        Gauss = MyUnits.G;
        
        %Current
        A = 1;
        mA = 1e-3*MyUnits.A;
        
        %Freq
        Hz = 1;
        kHz = 1e3;
        MHz = 1e6;
        
        %energy
        J = 1;
        mJ = 1e-3*MyUnits.J;
        
        %angle
        rad = 1;
        deg = unitsratio('rad','deg')
        
        % slewrate
        Tms = 1;
        mTcms = 10*MyUnits.Tms;
        Tmus = (1E6)*MyUnits.Tms;
    end
    
    methods(Static)
        function newval = ConverFromStandardUnits(value,unit)
            if ~isempty(unit) && sum(ischar(value)==0) && ~sum(isfinite(value)==0)
                switch unit
                    case 'MHz'
                        newval = value./MyUnits.MHz;
                    case 'kHz'
                        newval = value./MyUnits.kHz;
                    case 'ns'
                        newval = value./MyUnits.ns;
                    case 'us'
                        newval = value./MyUnits.us;
                    case 'ms'
                        newval = value./MyUnits.ms;
                    case 'deg'
                        newval = value./MyUnits.deg;
                    case 'mTcms'
                        newval = value./MyUnits.mTcms;
                    case 'Tmus'
                        newval = value./MyUnits.Tmus;
                    case 'um'
                        newval = value./MyUnits.um;
                    case 'mm'
                        newval = value./MyUnits.mm;
                    otherwise
                        newval = value;
                end
            else
                newval = value;
            end
        end
        function newval = ConverToStandardUnits(value,unit)
            if ~isempty(unit) && ~ischar(value) && isfinite(value)
                switch unit
                    case 'MHz'
                        newval = value.*MyUnits.MHz;
                    case 'kHz'
                        newval = value.*MyUnits.kHz;
                    case 'ns'
                        newval = value.*MyUnits.ns;
                    case 'us'
                        newval = value.*MyUnits.us;
                    case 'ms'
                        newval = value.*MyUnits.ms;
                    case 'deg'
                        newval = value.*MyUnits.deg;
                    case 'mTcms'
                        newval = value.*MyUnits.mTcms;
                    case 'Tmus'
                        newval = value.*MyUnits.Tmus; 
                    case 'um'
                        newval = value.*MyUnits.um;
                    case 'mm'
                        newval = value.*MyUnits.mm;
                    otherwise
                        newval = value;
                end
            else
                newval = value;
            end
        end
    end
    
end

