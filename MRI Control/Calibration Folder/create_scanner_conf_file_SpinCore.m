% Script in order to create the the configuration file for a specific
% scanner.

% Example for SpinCore scanner

scanner = [];
scanner.name = 'SpinCore';
scanner.adcOffset = 53;

scanner.freq = 14.2; %MHz
scanner.defaultRFampl = 0.8;
scanner.max_ampl_RF = 1; 

scanner.BlankingDelay = 15; %us
scanner.deadTime = 15; %us

scanner.GradientConstants = [5.45 5.45 4.23]; %for calibration
scanner.defaultShim = [-0.0001 0.0005 0.0006]; %[RO PH SL]
scanner.nSteps = 3; %for gradient ramps
scanner.grad_delay = 0; %us
scanner.maxGrad = 0.65;
scanner.no_shim = 1; %in order no to have the shimming ON during the repetition delay

load('rawData-BW-Calibration_SpinCore.mat');
scanner.bwCal =  rawData; %struct with 2 fields: inputs [nPoints,bwMin,bwMax,steps] & outputs [fidList,fidMean,fidStd]

save scanner_conf_file_SpinCore.mat scanner