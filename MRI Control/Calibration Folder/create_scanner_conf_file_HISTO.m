% Script in order to create the the configuration file for a specific
% scanner.

% Example for Dental scanner

scanner = [];
scanner.name = 'HISTO';
scanner.adcOffset = 0;

scanner.freq = 42.58; %MHz
scanner.defaultRFampl = 0.1;
scanner.max_ampl_RF = 0.4; 

scanner.BlankingDelay = 8; %us
scanner.deadTime = 15; %us

scanner.GradientConstants = [2.9 2.4 1.2]; %for calibration
scanner.defaultShim = [-2e-4 2e-4 13e-4]; %[RO PH SL]
scanner.nSteps = 5; %for gradient ramps
scanner.grad_delay = 55; %us
scanner.maxGrad = 0.25;         % Maximun gradient for 100% duty cycle
scanner.no_shim = 0; %in order no to have the shimming ON during the repetition delay

load('rawData-BW-Calibration.mat');
scanner.bwCal =  rawData; %struct with 2 fields: inputs [nPoints,bwMin,bwMax,steps] & outputs [fidList,fidMean,fidStd]

save scanner_conf_file.mat scanner