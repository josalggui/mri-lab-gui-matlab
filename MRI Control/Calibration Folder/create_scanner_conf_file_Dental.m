% Script in order to create the the configuration file for a specific
% scanner.

% Example for Dental scanner

scanner = [];
scanner.name = 'Dental';
scanner.adcOffset = 1;

scanner.freq = 11.09; %MHz
scanner.defaultRFampl = 0.05;
scanner.max_ampl_RF = 0.08; 

scanner.BlankingDelay = 8; %us
scanner.deadTime = 70; %us

scanner.GradientConstants = [0.62 0.52 0.64]; %for calibration
scanner.defaultShim = [-0.0007 -0.0003 0]; %[RO PH SL]
scanner.nSteps = 5; %for gradient ramps
scanner.grad_delay = 55; %us
scanner.maxGrad = 0.25;
scanner.no_shim = 0; %in order no to have the shimming ON during the repetition delay

load('rawData-BW-Calibration_Dental.mat');
scanner.bwCal =  rawData; %struct with 2 fields: inputs [nPoints,bwMin,bwMax,steps] & outputs [fidList,fidMean,fidStd]

save scanner_conf_file_Dental.mat scanner