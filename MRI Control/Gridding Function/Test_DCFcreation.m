% creating the DCF from a set of kspace integers and gridding points


clear all
clc

% set the number of gridding points and the gridding point vector
nkp = 20;
x = linspace(-0.5,0.5,nkp);
y = linspace(-0.5,0.5,nkp);
[tempkx, tempky] = meshgrid(x,y);
kx = tempkx(:);
ky = tempky(:);

clear tempkx tempky x y;

% set the trajectory vector
nKp = 10;
% theta = linspace(0,pi-pi/16,16);
% radii = linspace(-0.5,0.5,nKp);
% tempKx = repmat(radii,numel(theta),1).*repmat(cos(theta)',1,numel(radii));
% tempKy = repmat(radii,numel(theta),1).*repmat(sin(theta)',1,numel(radii));
% Kx = tempKx(:);
% Ky = tempKy(:);

% try a rectangular grid
X = linspace(-0.45,0.45,nKp);
Y = linspace(-0.45,0.45,nKp);
[tempKx, tempKy] = meshgrid(X,Y);
clear X Y
X = linspace(-.15,.15,20);
Y = linspace(-.15,.15,20);
[tempKx2, tempKy2] = meshgrid(X,Y);

Kx = [tempKx(:) ; tempKx2(:)];
Ky = [tempKy(:) ; tempKy2(:)];


clear tempKx tempKy X Y;

% check trajectories and points
figure;
plot(kx,ky,'bo');hold on; plot(Kx,Ky,'ro')

% compute the T matrix
xd = repmat( Kx, 1, numel(kx) ) - repmat( kx', numel(Kx), 1);
yd = repmat( Ky, 1, numel(ky) ) - repmat( ky', numel(Ky), 1);
kd = sqrt( xd.^2 + yd.^2 ); 
T = sinc( kd );

clear xd yd kd

% compute the D vector
dt = T * pinv((T')*T) * (T');
db = T * (T');
d = diag( dt ) ./ diag( db );

clear dt db

% plot the DCF
figure;
plot3(Kx,Ky,(d),'bo')

