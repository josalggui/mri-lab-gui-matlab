clear all;
clc;

load spiralexampledata
% plot(kspacelocations);

kwid = 3;
overg = 6;
klength = 2000;

[kern, kbu] = calckbkernel(kwid,overg,klength);
D = calcdcflut(kspacelocations,256,kern);
figure;subplot(1,2,1);plot3(real(kspacelocations),imag(kspacelocations),D','bo');hold on;
plot3(real(kspacelocations),imag(kspacelocations),dcf,'ro')


[gdat] = gridkb(kspacelocations,spiraldata,D',256,kwid,overg);
im = fftshift(fft2(fftshift(gdat)));
im = abs(im)/500;
cmap = [0:255].'*[1 1 1] / 256;
colormap(cmap);
subplot(1,2,2);
image(uint8(im));
colormap(cmap);
axis square;

