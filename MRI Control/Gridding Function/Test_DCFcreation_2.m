% creating the DCF from a set of kspace integers and gridding points


clear all
clc

grid = 50;

% set the trajectory vector
nKp = 60;
theta = linspace(0,pi-pi/(nKp-3),nKp-3);
radii = linspace(-0.45,0.45,nKp);
tempKx = repmat(radii,numel(theta),1).*repmat(cos(theta)',1,numel(radii));
tempKy = repmat(radii,numel(theta),1).*repmat(sin(theta)',1,numel(radii));
Kx = tempKx(:);
Ky = tempKy(:);


% % try a rectangular grid
% X = linspace(-0.45,0.45,nKp);
% Y = linspace(-0.45,0.45,nKp);
% [tempKx, tempKy] = meshgrid(X,Y);
% clear X Y
% X = linspace(-.15,.15,20);
% Y = linspace(-.15,.15,20);
% [tempKx2, tempKy2] = meshgrid(X,Y);
% 
% Kx = [tempKx(:) ; tempKx2(:)];
% Ky = [tempKy(:) ; tempKy2(:)];


clear tempKx tempKy X Y;

% % check trajectories and points
figure;
subplot(1,2,1);plot(Kx,Ky,'ro')
kspacelocations = complex(Kx,Ky);

% coompute the DCF
kwid = 3;
overg = 6;
klength = 100;

[kern, kbu] = calckbkernel(kwid,overg,klength);
D = calcdcflut(kspacelocations,grid,kern);
subplot(1,2,2);plot3(real(kspacelocations),imag(kspacelocations),D','bo')


