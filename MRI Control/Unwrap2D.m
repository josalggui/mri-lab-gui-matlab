function [im_unwrapped, im_phase_quality] = Unwrap2D(IM)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
im_mask=ones(size(IM));                     %Mask (if applicable)
%%

im_mag=abs(IM);                             %Magnitude image
im_phase=angle(IM);                         %Phase image
im_unwrapped=zeros(size(IM));               %Zero starting matrix for unwrapped phase
adjoin=zeros(size(IM));                     %Zero starting matrix for adjoin matrix
unwrapped_binary=zeros(size(IM));           %Binary image to mark unwrapped pixels

%% Calculate phase quality map
im_phase_quality=PhaseDerivativeVariance(im_phase);   

%uiwait(msgbox('Select known true phase reference phase point. Black = high quality phase; white = low quality phase.','Phase reference point','modal'));
%[xpoint,ypoint] = ginput(1);                %Select starting point for the guided floodfill algorithm
xpoint = size(IM,1)/2;
ypoint =size(IM,2)/2;
%% Unwrap
colref=round(xpoint); rowref=round(ypoint);
im_unwrapped(rowref,colref)=im_phase(rowref,colref);                        %Save the unwrapped values
unwrapped_binary(rowref,colref,1)=1;
if im_mask(rowref-1, colref, 1)==1 adjoin(rowref-1, colref, 1)=1; end       %Mark the pixels adjoining the selected point
if im_mask(rowref+1, colref, 1)==1 adjoin(rowref+1, colref, 1)=1; end
if im_mask(rowref, colref-1, 1)==1 adjoin(rowref, colref-1, 1)=1; end
if im_mask(rowref, colref+1, 1)==1 adjoin(rowref, colref+1, 1)=1; end
im_unwrapped=GuidedFloodFill(im_phase, im_unwrapped, unwrapped_binary, im_phase_quality, adjoin, im_mask);    %Unwrap


end

