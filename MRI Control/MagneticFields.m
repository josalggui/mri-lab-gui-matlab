classdef MagneticFields
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods (Static)
        function G = MakeFieldFromGradientInCube(Vx,Vy,Vz,grad,r0)
 %           Vx,Vy,Vz are voxels X,Y,Z, r0 - reference point, grad -
 %           gradient vector
 %           r0 = [Vx(0,0,0) Vy(0,0,0) Vz(0,0,0)];
            G = zeros(size(Vx));
            for i=1:size(G,1)
                for j=1:size(G,2)
                    for k=1:size(G,3)
                        r = [Vx(i,j,k) Vy(i,j,k) Vz(i,j,k)];                        
                        G(i,j,k) = (r-r0)*grad';
                    end
                end
            end
            
        end
    end
    
end

