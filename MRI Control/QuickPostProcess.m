function varargout = QuickPostProcess( varargin )
% this function will replace with zeros any data below a certain percentage of the maximum
% created by alek nacev 10 Oct 2013

threshold = varargin{1};

plotchoice = varargin{2};

gsmooth = varargin{3};
if gsmooth < 1 && gsmooth > 0
    blur = 'GSTD';
elseif gsmooth >= 1
    blur = 'yes';
else
    blur = 'no';
    gsmooth = 1;
end

cmap = varargin{4};
if ischar(cmap)
    if strcmp( cmap, 'custom' )
        clear cmap;
        load('EditedBoneMap.mat');
    end
end

if nargin >= 5
    DataStruct = varargin{5};
else
    global MRIDATA
    DataStruct = MRIDATA;
end

if nargin >= 6
    fig1 = varargin{6};
else
    fig1 = figure;
end

switch plotchoice
    case '2D'
        D = DataStruct.Fid2D;
        E = D;
        E(abs(D)<(threshold/100)*max(max(abs(D)))) = 0;
        
        I = abs( fftshift( ifftn( fftshift( E ) ) ) );
        G = fspecial('gaussian',[gsmooth gsmooth],2);
        
        h = subplot(2,1,1);
        
        switch blur
            case 'yes'
                I = imfilter(I,G,'same');
            otherwise
        end
        
        imagesc(I,'Parent',h);
        colorbar('peer',h);
        colormap(h,cmap)
        title(h,'2D FFT - Magnitude');
        axis(h, 'square')
        
        h = subplot(2,1,2);
        obj.plotAbsKspace = imagesc( abs(E) ,'Parent',h);
        colorbar('peer',h);
        colormap(h,cmap)
        title(h,'Abs of k-space');
        axis(h, 'square')
        
    case '2D Gauss'
        D = DataStruct.Fid2D;
        E = D;
        E(abs(D)<(threshold/100)*max(max(abs(D)))) = 0;
        
        [I,E] = createIslice ( E, blur, 1, gsmooth );
        
        h = subplot(2,1,1);
        imagesc(I,'Parent',h);
        colorbar('peer',h);
        colormap(h,cmap)
        title(h,'2D FFT - Magnitude');
        axis(h, 'tight')
        
        h = subplot(2,1,2);
        obj.plotAbsKspace = imagesc( abs(E) ,'Parent',h);
        colorbar('peer',h);
        colormap(h,cmap)
        title(h,'Abs of k-space');
        axis(h, 'equal')
        
    case '3D'
        D = DataStruct.Fid3D;
        E = D;
        E(abs(D)<(threshold/100)*max(max(max(abs(D))))) = 0;
        
        colormaptemp = colormap(cmap);
        colormap(fig1, flipud(colormaptemp));
        alphamaptemp = alphamap('rampup');
        alphamaptemp = alphamaptemp*0.5;
        alphamaptemp(1:10) = 0;
        alphamap(fig1, alphamaptemp);
        
        
        switch blur
            case 'yes'
                
                
                G = fspecial3('gaussian',[gsmooth gsmooth gsmooth]);
                I = abs(fftshift(ifftn(fftshift( padarray( E, ceil(0.75*size(E)),'both') ))));
                I = imfilter(I,G,'same');
            case 'GSTD'
                s = size(E(:,:,:));
                Gstd = gsmooth;
                G = customgauss3D(s,Gstd*s(1),Gstd*s(2),Gstd*s(3),0,0,1,0*s);
                E = E.*G;
                
                I = abs(fftshift(ifftn(fftshift( padarray( E, ceil(0.75*size(E)),'both') ))));
            otherwise
                I = abs(fftshift(ifftn(fftshift( padarray( E, ceil(0.75*size(E)),'both') ))));
        end
        
        I = permute(I,[3 2 1]);
        I = flip(I,3);
        
        clear E G;
        
        vol3d( 'CData', I, 'texture', '3D');
        title('3D FFT');
        axis('square')
        %
        %         h = subplot(2,1,1);
        %         vol3d( 'CData', I, 'texture', '3D', 'Parent', h);
        %         title(h, '3D FFT');
        %         axis(h, 'square')
        % %
        %         h = subplot(2,1,2);
        %         vol3d( 'CData', abs(E), 'texture', '3D', 'Parent', h);
        %         title(h, '3D FID');
        %         axis(h, 'square')
        
        
    case '2D Slice'
        D = DataStruct.Fid3D;
        E = D;
        E(abs(D)<(threshold/100)*max(max(max(abs(D))))) = 0;
        
        readoutaxis = linspace(1,numel(D(:,1,1)),numel(D(:,1,1)));
        phaseaxis   = linspace(1,numel(D(1,:,1)),numel(D(1,:,1)));
        sliceaxis   = 1;
        
        [X, Y, Z] = ndgrid(readoutaxis,phaseaxis,sliceaxis);
        
        if isfield(DataStruct,'Quaternion');
            % Image Rotation
            
            RotatedAxis = quatrotate(DataStruct.Quaternion, [X(:) Y(:) Z(:)]);
            X = reshape(RotatedAxis(:,1),numel(D(:,1,1)),numel(D(1,:,1)),1);
            Y = reshape(RotatedAxis(:,2),numel(D(:,1,1)),numel(D(1,:,1)),1);
        end
        
        nslicetot = numel( E(1,1,:) );
        
        
        if nslicetot > 7
            if nslicetot > 14
                close (fig1);
                nfigs = ceil( nslicetot / 14 );
                
                countislice = 0;
                for ifig = 1:nfigs
                    fig1(ifig) = figure('units','normalized','outerposition',[0 0 1 1]);
                    
                    for islice = 1:14
                        if nslicetot < islice + countislice
                            % do nothing
                        else
                            I = createIslice ( E, blur, islice + countislice, gsmooth );
                            
                            h = subplot( 2, 7, islice );
                            drawSlice ( X, Y, I, h, cmap );
                            title(h,{'2D FFT - Magnitude', strcat('Slice:',num2str( islice + countislice ))});
                        end
                        
                    end
                    
                    countislice = countislice + 14;
                    
                end
                
            else
                
                set(fig1,'units','normalized','outerposition',[0 0 1 1]);
                for islice = 1:nslicetot
                    
                    I = createIslice ( E, blur, islice, gsmooth );
                    
                    h = subplot( 2, ceil (nslicetot / 2), islice );
                    drawSlice ( X, Y, I, h, cmap );
                    title(h,{'2D FFT - Magnitude', strcat('Slice:',num2str(islice))});
                    
                end
                
            end
            
        else
            
            if nslicetot > 3
                set(fig1,'units','normalized','outerposition',[0 0 1 1]);
            end
            
            for islice = 1:nslicetot
                
                I = createIslice ( E, blur, islice, gsmooth );
                
                h = subplot( 2, nslicetot, islice );
                drawSlice ( X, Y, I, h, cmap );
                title(h,{'2D FFT - Magnitude', strcat('Slice:',num2str(islice))});
                
                h = subplot( 2, nslicetot, islice + nslicetot );
                drawSlice ( X, Y, abs( E(:,:,islice) ) , h, cmap );
                title(h,{'Abs of k-space', strcat('Slice:',num2str(islice))});
                
            end
        end
        
    case '2D Slice Presentation'
        clf(fig1);
        D = DataStruct.Fid3D;
        E = D;
        E(abs(D)<(threshold/100)*max(max(max(abs(D))))) = 0;
        
        readoutaxis = linspace(1,numel(D(:,1,1)),numel(D(:,1,1)));
        phaseaxis   = linspace(1,numel(D(1,:,1)),numel(D(1,:,1)));
        sliceaxis   = 1;
        
        [X, Y, Z] = ndgrid(readoutaxis,phaseaxis,sliceaxis);
        
        if isfield(DataStruct,'Quaternion');
            % Image Rotation
            RotatedAxis = quatrotate(DataStruct.Quaternion, [X(:) Y(:) Z(:)]);
            X = reshape(RotatedAxis(:,1),numel(D(:,1,1)),numel(D(1,:,1)),1);
            Y = reshape(RotatedAxis(:,2),numel(D(:,1,1)),numel(D(1,:,1)),1);
        end
        
        nslicetot = numel( E(1,1,:) );
        hfig = 0.75;
        wfig = 0.15;
        sfig = (1-nslicetot*wfig)/(nslicetot+2);
        FC = 0.85*[1 1 1];
        set(fig1,'Color',[0 0 0]);
        
        if nslicetot >= 4
            OP = [0.1    0.2    0.8    0.6];
            set(fig1,'units','normalized','outerposition',OP);
        end
        
        for islice = 1:nslicetot
            
            I = createIslice ( E, blur, islice, gsmooth );
            
            if islice == 1
                h = subplot( 1, nslicetot, 1, 'Parent', fig1, 'Position', [sfig 5/8*(1-hfig) wfig hfig] );
            else
                h = subplot( 1, nslicetot, islice, 'Parent', fig1, 'Position', [sfig*3+wfig+(islice-2)*(wfig+sfig) 5/8*(1-hfig) wfig hfig]);
            end
            
            drawSlice ( X, Y, I, h, cmap );
            %             axis(h,'off');
            
            if islice == 1
                title(h,{'Projection Image'},'FontSize', 20,'Color',FC);
            else
                title(h,{strcat('Slice Image #',num2str(islice-1))},'FontSize',16,'Color',FC);
            end
            set(h,'YColor', FC*0.8, 'XColor', FC*0.8)
        end
        
        global CONTROL;
        if ~isempty(CONTROL)
            h = axes('units','normalized','Position',[ 0 0 1 1],'Parent',fig1);
            axis(h,'off');
            text('Position',[.01 .08], 'String', strcat('Acquisition Time [sec]:',num2str(toc(CONTROL.timing))),'FontSize',14,'Parent',h,'Color',FC);
        end
        
    case '2D Slice MIP'
        D = DataStruct.Fid3D;
        E = D;
        E(abs(D)<(threshold/100)*max(max(max(abs(D))))) = 0;
        
        readoutaxis = linspace(1,numel(D(:,1,1)),numel(D(:,1,1)));
        phaseaxis   = linspace(1,numel(D(1,:,1)),numel(D(1,:,1)));
        sliceaxis   = 1;
        
        [X, Y, Z] = ndgrid(readoutaxis,phaseaxis,sliceaxis);
        
        if isfield(DataStruct,'Quaternion');
            % Image Rotation
            
            RotatedAxis = quatrotate(DataStruct.Quaternion, [X(:) Y(:) Z(:)]);
            X = reshape(RotatedAxis(:,1),numel(D(:,1,1)),numel(D(1,:,1)),1);
            Y = reshape(RotatedAxis(:,2),numel(D(:,1,1)),numel(D(1,:,1)),1);
        end
        
        nslicetot = numel( E(1,1,:) );
        
        for islice = 1:nslicetot
            I(:,:,islice) = createIslice ( E, blur, islice, gsmooth );
        end
        
        Imax = max(I,[],3);
        Imaxmin = max(I,[],3) - min(I,[],3);
        Imin = min(I,[],3);
        
        
        
        h = subplot( 3, 1, 1 );
        drawSlice ( X, Y, Imax, h, cmap );
        title(h,{'MIP'});
        
        h = subplot( 3, 1, 2 );
        drawSlice ( X, Y, Imin , h, cmap );
        title(h,{'Min Projection'});
        
        h = subplot( 3, 1, 3 );
        drawSlice ( X, Y, Imaxmin , h, cmap );
        title(h,{'Max-Min Projection'});
        
    case 'Resolution'
        D = DataStruct.Fid2D;
        E = D;
        E(abs(D)<(threshold/100)*max(max(abs(D)))) = 0;
        
        s = size(E);
        switch blur
            case 'yes'
                Gstd = varargin{3};
                G = customgauss(s,Gstd*s(1),Gstd*s(2),0,0,1,0*s);
                E = E.*G;
            otherwise
        end
        
        I = abs( fftshift( ifftn( fftshift( E ) ) ) );
        
        h = subplot(1,1,1);
        imagesc(I,'Parent',h);
        colorbar('peer',h);
        colormap(h,cmap)
        title(h,'2D FFT - Magnitude');
        axis(h, 'tight')
        
        %         h2 = subplot(2,1,2);
        %         obj.plotAbsKspace = imagesc( abs(E) ,'Parent',h2);
        %         colorbar('peer',h2);
        %         colormap(h2,cmap)
        %         title(h2,'Abs of k-space');
        %         axis(h2, 'equal')
        
        
        
end


varargout{1} = I;
varargout{2} = E;
end


function [I,E] = createIslice ( E, blur, islice, gsmooth )

switch blur
    case 'yes'
        s = size(E(:,:,islice));
        I = abs( fftshift( ifftn( fftshift( padarray( E(:,:,islice), ceil((s)/2), 'both') ) ) ) );
        G = fspecial('gaussian',[gsmooth gsmooth],2);
        I = imfilter(I,G,'same');
    case 'GSTD'
        s = size(E(:,:,islice));
        Gstd = gsmooth;
        G = customgauss(s,Gstd*s(1),Gstd*s(2),0,0,1,0*s);
        E(:,:,islice) = E(:,:,islice).*G;
        I = abs( fftshift( ifftn( fftshift( padarray( E(:,:,islice), ceil((s)/2), 'both') ) ) ) );
    otherwise
        s = size(E(:,:,islice));
        I = abs( fftshift( ifftn( fftshift( padarray( E(:,:,islice), ceil((s)/2), 'both') ) ) ) );
end

end

function drawSlice ( X, Y, I, h, cmap )
a = imresize(X,size(I));
b = imresize(Y,size(I));
pcolor(a,b,I,'Parent',h);
shading(h,'interp');
colormap(h,cmap)

end
