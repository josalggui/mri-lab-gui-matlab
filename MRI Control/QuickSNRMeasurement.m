% Quick SNR test of Frequency Data

function values = QuickSNRMeasurement( savename )
global MRIDATA

% save name
% savename = '13A_line27';

% load data
F = MRIDATA.Fid2D;
% convert data
G = abs( fft( F ) ) ;

% take SNR measurement
thresh = linspace( 0.1, 0.5, 20 );

for i = 1:numel(thresh)
    SNR(i) = mean( G( G>=thresh(i)*max(G))) / mean( G( G< thresh(i)*max(G)));
end % end loop

SNRmean = mean(SNR);
SNRstd  = std(SNR);
Gmax    = max(G);

% figure;plot(thresh,SNR,'bo');

save( char(strcat(savename,'.mat')), 'MRIDATA','SNR');

if isfield( MRIDATA, 'CF' )
    values = [SNRmean SNRstd Gmax MRIDATA.CF MRIDATA.fwhm];
    SNRmean
    SNRstd
    Gmax
    CF = MRIDATA.CF/(1E6)
    FWHM = MRIDATA.fwhm
else
    values = [SNRmean SNRstd Gmax];
    SNRmean
    SNRstd
    Gmax
  
end

end