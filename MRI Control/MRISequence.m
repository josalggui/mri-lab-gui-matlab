classdef MRISequence < handle
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        cData;
        fidData;
        fftData1D;
        timeVector;
        freqVector;
        ProgramDir = 'C:\SpinCore\MRI SpinAPI\examples';
        path_file; 
        ProgramName;
        SequenceName;
        OutputFilename;
        OutputFilename_CF;
        InputParameters;
        InputParNames;
        InputParValues;
        InputParValuesMin;
        InputParValuesMax;
        InputParUnits;
        InputParHidden; %this parameters are not shown in the input table
        OutputParameters;
        OutputParNames;
        OutputParValues;
        OutputParUnits;
        RF_Shape = 0;
        tx_phase = 0;
        blankingBit = 0;
        adcOffset= 29; %45 (before)
        slice_en = 0;
        phase_en = 0;
        readout_en = 0;
        spoiler_en = 0;
        calibration_en=0;
        shimS_en = 0;
        shimP_en = 0;
        shimR_en = 0;
        FIDPhase = 0;
        NAverages = 10;
        averaging = 0;
        autoPhasing = 0;
        AbortRun = false;
        StatusBar;
        ReferenceCF = 0;
        MaxCF_shift=20*MyUnits.kHz;
        TE = 0;
        TR = 0;
        TTotal = 0;
    end
    
    methods
        function obj = MRISequence
            obj.InputParameters = {...
                'SHIMSLICE','SHIMPHASE','SHIMREADOUT','NSCANS',...
                'NPOINTS','NPHASES','NSLICES','FREQUENCY','SPECTRALWIDTH',...
                'LINEBROADENING','RFAMPLITUDE','PULSETIME','BLANKINGDELAY','TRANSIENTTIME'...
                };
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT'}; %these pars will not be shown in a par table
            obj.InputParNames = containers.Map(obj.InputParameters,...
                {...
                'Slice Shimming','Phase Shimming','Readout Shimming','Number of Scans',...
                'Number of Points', 'Number of Phases', 'Number of Slices', 'Frequency', 'SpectralWidth',...
                'Line Broadening','RF Amplitude','Pulse Time','Blanking Delay','Transient Time'...
                });
            obj.InputParValues = containers.Map(obj.InputParameters,...
                {...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,NaN...
                });
            obj.InputParValuesMin = containers.Map(obj.InputParameters,...
                {...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,NaN...
                });
            obj.InputParValuesMax = containers.Map(obj.InputParameters,...
                {...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,NaN...
                });
            obj.InputParUnits = containers.Map(obj.InputParameters,...
                {...
                '','','','',...
                '','','','MHz','kHz',...
                '','','us','ms','us'...
                });
            obj.OutputParameters = {
                'ADCDECIMATION','ACTUALSPECTRALWIDTH','ACQUISITIONTIME','LASTFREQUENCY'
                };
            obj.OutputParNames = containers.Map(obj.OutputParameters,...
                {...
                'ADC Frequency Decimation', 'Actual Spectral Width', 'Acquisition Time','Last RF Frequency', ...
                });
            obj.OutputParValues = containers.Map(obj.OutputParameters,...
                {...
                NaN,NaN,NaN, NaN...
                });
            obj.OutputParUnits = containers.Map(obj.OutputParameters,...
                {...
                '','MHz','ms','MHz'...
                });
        end
        function SetDefaultParameters(obj)
            [pathstr, name, ext] = fileparts(mfilename('fullpath'));
            obj.path_file = fullfile(pathstr,'FileDir'); % this is the path of the folder where all the generated files
                                                     % will be written and read from
            obj.OutputFilename = fullfile(obj.path_file,'FID_temp');
            obj.OutputFilename_CF = fullfile(obj.path_file,'FID_temp_CF');            
 
            obj.SetParameter('NPOINTS',500)
            obj.SetParameter('NSCANS',1)
            obj.SetParameter('NSLICES',1)
            obj.SetParameter('NPHASES',1)
            obj.SetParameter('NECHOS',4)
            obj.SetParameter('FREQUENCY',14.5*MyUnits.MHz);
            obj.SetParameter('SPECTRALWIDTH',256*MyUnits.kHz);
            obj.SetParameter('LINEBROADENING',0);
            obj.SetParameter('RFAMPLITUDE',0.3);
            obj.SetParameter('PULSETIME',20*MyUnits.us);
            obj.SetParameter('BLANKINGDELAY',3*MyUnits.ms);
            obj.SetParameter('TRANSIENTTIME',0.1*MyUnits.us);
        end
        function SetParameter(obj,par,value)
            if ischar(value); value = str2num(value);end
            if (isempty(value) || isnan(value)); return; end
            if any(strcmp(obj.InputParameters,par))
                %               fprintf('Parameter %s is set to %f\n',par,value);
                if value < obj.InputParValuesMin(par)
                    obj.InputParValues(par) = obj.InputParValuesMin(par);
                elseif value > obj.InputParValuesMax(par)
                    obj.InputParValues(par) = obj.InputParValuesMax(par);
                else
                    obj.InputParValues(par) = value;
                end
            elseif any(strcmp(obj.OutputParameters,par))
                obj.OutputParValues(par) = value;
            else
                fprintf('Parameter %s is not found\n',par);
            end
        end
        function SetParameterMin(obj,par,value)
            if any(strcmp(obj.InputParameters,par))
                obj.InputParValuesMin(par) = value;
            else
                fprintf('Input Parameter %s is not found\n',par);
            end
        end
        function SetScanParameterValue(obj,par,value)
            obj.SetParameter(par,value)
        end
        function SetParameterMax(obj,par,value)
            if any(strcmp(obj.InputParameters,par))
                obj.InputParValuesMax(par) = value;
            else
                fprintf('Input Parameter %s is not found\n',par);
            end
        end
        function SetParameters(obj,ParValuesMap)
            for i=1:ParValuesMap.length()
                SetParameter(ParValuesMap.keys(i),ParValuesMap.values(i));
            end
        end
        function value = GetParameter(obj,par)
            if any(strcmp(obj.InputParameters,par))
                value = obj.InputParValues(par);
            elseif any(strcmp(obj.OutputParameters,par))
                value = obj.OutputParValues(par);
            else
                value = NaN;
            end
        end
        function value = GetParameterUnit(obj,par)
            if any(strcmp(obj.InputParameters,par))
                value = obj.InputParUnits(par);
            elseif any(strcmp(obj.OutputParameters,par))
                value = obj.OutputParUnits(par);
            else
                value = '';
            end
        end
        function value = GetParameterName(obj,par)
            if any(strcmp(obj.InputParameters,par))
                value = obj.InputParNames(par);
            elseif any(strcmp(obj.OutputParameters,par))
                value = obj.OutputParNames(par);
            else
                value = '';
            end
        end
        function [pars,names,values,units] = GetInputParameters(obj)
            pars = obj.InputParameters;
            names = containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());%Matlab trick to get a new copy of container
            values = containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());
            units = containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());
            for i=1:length(obj.InputParHidden)
                par = obj.InputParHidden{i};
                pars(strcmp(pars,par)) = [];
            end
            names.remove(obj.InputParHidden);
            values.remove(obj.InputParHidden);
            units.remove(obj.InputParHidden);
            
        end
        function [pars,names,values,units] = GetOutputParameters(obj)
            pars = obj.OutputParameters;
            names = obj.OutputParNames;
            values = obj.OutputParValues;
            units = obj.OutputParUnits;
        end
        function sw = ExistsInputParameter(obj,par)
            sw = any(strcmp(obj.InputParameters,par));
        end
        function SetFIDPhase(obj,phase)
            obj.FIDPhase = phase;
        end
        function SetNAverages(obj,n)
            obj.NAverages = n;
        end
        function EnableAveraging(obj,sw)
            obj.averaging = sw;
        end
        function EnableAutoPhase(obj,sw)
            obj.autoPhasing = sw;
        end
        function SetRFShape(obj,shape_type)
            obj.RF_Shape = shape_type;
        end
        function SetShimS(obj,v)
            obj.SetParameter('SHIMSLICE',v);
        end
        function SetShimP(obj,v)
            obj.SetParameter('SHIMPHASE',v);
        end
        function SetShimR(obj,v)
            obj.SetParameter('SHIMREADOUT',v);
        end
        function v = GetShimS(obj)
            v = obj.GetParameter('SHIMSLICE');
        end
        function v = GetShimP(obj)
            v = obj.GetParameter('SHIMPHASE');
        end
        function v = GetShimR(obj)
            v = obj.GetParameter('SHIMREADOUT');
        end
        function EnableSliceGradient(obj,sw)
            obj.slice_en = sw;
        end
        function EnablePhaseGradient(obj,sw)
            obj.phase_en = sw;
        end
        function EnableReadoutGradient(obj,sw)
            obj.readout_en = sw;
        end
        function EnableSpoilerGradient(obj,sw)
            obj.spoiler_en = sw;
        end
        function EnableCalibration(obj,sw)
            obj.calibration_en = sw;
        end
        function EnableShimming(obj,sw)
            obj.EnableShimmingSlice(sw);
            obj.EnableShimmingPhase(sw);
            obj.EnableShimmingReadout(sw);
        end
        function EnableShimmingSlice(obj,sw)
            obj.shimS_en = sw;
        end
        function EnableShimmingPhase(obj,sw)
            obj.shimP_en = sw;
        end
        function EnableShimmingReadout(obj,sw)
            obj.shimR_en = sw;
        end
        function [fidData,fftData] = GetData1D(obj)
            fidData = obj.fidData;
            fftData = obj.fftData1D;
        end
        function SaveFID(obj,fname)
            saveFile = fopen(fullfile(obj.path_file,fname),'w');
            for ii = 1:length(obj.timeVector)
                fprintf(saveFile,'%f %f %f\n',obj.timeVector(ii)/MyUnits.us,real(obj.fidData(ii)),imag(obj.fidData(ii)));
            end
            fclose(saveFile);
        end
        function Abort(obj)
            obj.AbortRun = true;
        end
        function SetStatusBar(obj, fig_handle)
            % Set up the color
            obj.StatusBar = statusbar( fig_handle, 'Acquiring Data...' );
            set(obj.StatusBar.TextPanel, 'Foreground', [1 1 1], 'Backgrou', [0 0 0], 'ToolTipText', 'Please wait for MRI sequence to finish.');
            set(obj.StatusBar, 'Background', java.awt.Color.black);
            
            % Set up the intermediate status bar
            obj.StatusBar.ProgressBar.setVisible(true);
            set(obj.StatusBar.ProgressBar,'Minimum',0,'Maximum',1,'Value',0);
            obj.StatusBar.ProgressBar.setIndeterminate(true);
            obj.StatusBar.ProgressBar.setStringPainted(false);
            
            % Hide the corner grip
            cornerGrip = obj.StatusBar.getParent.getComponent(false);
            cornerGrip.setVisible(false);
                   
        end
        function StopStatusBar(obj, fig_handle)
           statusbar( fig_handle ); 
        end
        function CenterFrequency(obj,fname_CF)
%          
            % GET CENTER FREQUENCY
            
            [status,result] = system(fullfile(obj.path_file,fname_CF));

            if status == 0 && ~isempty(result)
                C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                res = C{:};
                obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
            end
            
            DataCF = load(sprintf('%s.txt',obj.OutputFilename_CF));
            cDataCF = complex(DataCF(:,1),DataCF(:,2));

            fftData1D = flipud(fftshift(fft(cDataCF)));
            acq_time = obj.GetParameter('ACQUISITIONTIME');
            freqVector = (ceil(-length(fftData1D)/2):ceil(length(fftData1D)/2)-1)/acq_time;
            idx = find(fftData1D==max(fftData1D),1,'first');
            deltaFreq = freqVector(idx);

            obj.SetParameter('FREQUENCY',obj.GetParameter('FREQUENCY')+deltaFreq);   
            
            if abs(obj.ReferenceCF - obj.GetParameter('FREQUENCY')) >= obj.MaxCF_shift
                load gong.mat;
                sound(y, Fs);
                clear Fs y
                h_warn = warndlg(char(strcat('Please tune coil to: [MHz]', num2str( obj.GetParameter('FREQUENCY')/MyUnits.MHz))),'!!! Waiting to Continue !!!');
                uiwait(h_warn);
            end
        end
    end
    
end

