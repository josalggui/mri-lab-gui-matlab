S = load('Data3D_EA1.mat');
Fid3D = S.Fid3D;
I = abs(fftshift( ifftn( fftshift( Fid3D ) ) ) );
I = circshift(I, [ 0 0 ceil(size(I,3)/2.5)]);
II = I;
II(I<1300) = 0;
figure(5);
vol3d('CData',(II),'texture','3D');alphamap('rampup');