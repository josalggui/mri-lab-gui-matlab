classdef EDDYCURRENTSTEST_analysis < MRI_cartesian_analysis_JM   
    
    methods
        function obj=EDDYCURRENTSTEST_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'IECO_TEST Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)      
            global rawData;
            t = rawData.outputs.fid(:,1)*1e3;
            s = abs(rawData.outputs.fid(:,2:4))*1e3;
            s1 = rawData.outputs.fid(:,2)*1e3;
            s2 = rawData.outputs.fid(:,3)*1e3;
            s3 = rawData.outputs.fid(:,4)*1e3;
            s11 = fit(t,abs(s1),'poly4');       % Fitting to poly4
            s22 = fit(t,abs(s2),'poly4');       % Fitting to poly4
            s33 = fit(t,abs(s3),'poly4');       % Fitting to poly4
            s21 = s2./s11(t);                   % FID+/FID0
            s31 = s3./s11(t);                   % FID-/FID0
            
            % Plot FIDs
            figure(2)
            set(gcf, 'Position', [50, 50, 1400, 800])
            
            subplot(2,2,1)  % Abs
            plot(t,abs(s1),'b',t,abs(s2),'r',t,abs(s3),'g')
            title('ABS')
            xlabel('Time (ms)')
            ylabel('Signal (mV)')
            xlim([0 max(t)])
            ylim([0 1.1*max(s(:))])
            legend('FID0','FID_+','FID_-')
            
            subplot(2,2,2)  % Phase
            plot(t,unwrap(angle(s1))*180/pi,'b',t,unwrap(angle(s2))*180/pi,...
                'r',t,unwrap(angle(s3))*180/pi,'g')
            title('PHASE')
            xlabel('Time (ms)')
            ylabel('Phase (�)')
            xlim([0 max(t)])
            legend('FID0','FID_+','FID_-')
            
            subplot(2,2,3)  % Abs
            plot(t,abs(s21),'r',t,abs(s31),'g')
            title('ABS')
            xlabel('Time (ms)')
            ylabel('Signal (mV)')
            xlim([0 max(t)])
            ylim([0 1.5])
            legend('FID_+/abs(FID_0)','FID_-/abs(FID_0)')
            
            subplot(2,2,4)  % Phase
            plot(t,unwrap(angle(s21))*180/pi,...
                'r',t,unwrap(angle(s31))*180/pi,'g')
            title('PHASE')
            xlabel('Time (ms)')
            ylabel('Phase (�)')
            xlim([0 max(t)])
            legend('FID_+/abs(FID_0)','FID_-/abs(FID_0)')
            
            h = obj.figFID;
            hold off
            plot(t,abs(s1),'b-',t,s11(t),'b--',...
                t,abs(s2),'r-',t,s22(t),'r--',...
                t,abs(s3),'g-',t,s33(t),'g--',...
                'Parent',h);
            ylim(h,[0 1.1*max(s(:))])
            xlim(h,[0,max(t)])
            ylabel(h,'Signal (mV)')
            xlabel(h,'Time (ms)')
            title(h,'FIDs')
            legend(h,'FID_0','Fitting','FID_+','Fitting',...
                'FID_-','Fitting');
            
            h = obj.figFFT;
            hold off
            plot(t,abs(s21),'r',t,abs(s31),'g','Parent',h)
            xlim(h,[0,max(t)])
            ylim(h,[0 1.5])
            ylabel(h,'FID ratio')
            xlabel(h,'Time (ms)')
            title(h,'FID ratio')
            legend(h,'FID_+/FID_0','FID_-/FID_0')
        end  
    end
end

