classdef WAHUHA_analysis < MRI_cartesian_analysis_JM
    %ANALYSIS for Gradient Echo 3D     
    
    methods
        function obj=WAHUHA_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'WAHUHA Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
            global rawData;
            signal = rawData.kSpace.sampled(:,2);
            time = rawData.kSpace.sampled(:,1);
            
            h = obj.figFID; 
            plot(time*1d3,abs(signal),'.','Parent',h)
            pause(0.1)
            grid on
            xlabel(h,'time (ms)')
            ylabel(h,'signal measured (a.u.)')
            ylim(h,[0 inf])
            xlim(h,[0 inf])

%             
%             figure(1)
%             set(gcf, 'Position', [100, 500, 1000, 400])
            
%             subplot(1,2,1)
%             plot(time*1d3,abs(signal),'.',time*1d3,imag(signal),'.')
%             xlabel('time (ms)')
%             ylabel('signal (a.u.)')
%             legend('real','imag')
%             title('FID')
%             
%             subplot(1,2,2)


            

        end
    end
end

