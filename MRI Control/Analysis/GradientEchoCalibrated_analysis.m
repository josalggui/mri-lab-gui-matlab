classdef GradientEchoCalibrated_analysis < MRI_analysis
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        fig2D;
        Fid2D;
        Fid3D;
        Image2D;
        Image3D;
        firstPlot2D;
        firstPlot3D;
        plotImage2D;
        plotImage3D;
        plotImage2DPhase;
        plotImage2DPhaseUnwrapped;
        plotAbsKspace;
        plotRealKspace;
        plotAbsKspace3D;
        phaseTitle;
    end
    
    methods
        function obj=GradientEchoCalibrated_analysis(seq,gfig)
            obj = obj@MRI_analysis(seq,gfig);
            obj.Name = 'GradientEchoCalibrated Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
            global MRIDATA;
            Execute@MRI_analysis(obj);
            seq = obj.seqClass;
            nPoints = seq.GetParameter('NPOINTS');
            nPhases = seq.GetParameter('NPHASES');
            nSlices = seq.GetParameter('NSLICES');
            if length(seq.fidData) ==  nPoints*nPhases*nSlices
                MRIDATA.Fid3D = reshape(seq.fidData, nPoints, nPhases, nSlices);
                obj.Fid3D = MRIDATA.Fid3D;
                obj.Fid2D = flipud(MRIDATA.Fid3D(:,:,ceil(nSlices/2))');
                MRIDATA.Fid2D = obj.Fid2D;
                padding = 0.5*[size(obj.Fid2D,1) size(obj.Fid2D,2)]; %[2^nextpow2(size(obj.Fid2D,1))-size(obj.Fid2D,1) 2^nextpow2(size(obj.Fid2D,2))-size(obj.Fid2D,2)];
                s = size(obj.Fid2D);
                Gstd = seq.GetParameter('GAUSSIANFILTER');
                G = customgauss(s,Gstd*s(1),Gstd*s(2),0,0,1,0*s);
                obj.Fid2D = obj.Fid2D.*G;
                
                obj.Image2D = fftshift( ifftn( fftshift( padarray( obj.Fid2D, ceil(padding/2), 'both' ) ) ) );
                switch obj.PlotSelector
                    case '2D Image'
                        obj.Plot2D();
                    case '2D Phase'
                        obj.Plot2DPhase();
                    case '3D Image'
                        obj.Plot3D();
                    otherwise
                        warning('Unknown Plot','Unexpected plot type. No plot created.');
                end
            end
        end
        function Reset(obj)
            Reset@MRI_analysis(obj);
            obj.firstPlot2D = 1;
            obj.firstPlot3D = 1;
        end
        function ResetPlots(obj)
            ResetPlots@MRI_analysis(obj);
            obj.firstPlot2D = 1;
            obj.firstPlot3D = 1;
        end
        function figs = FigureRequest(obj)
            width = 0.4;
            height = 1;
            figs(1,:) = [width height];
        end
        function SetFigureHandle(obj,h)
            obj.fig2D = h(1);
        end
        function Plot2D(obj)
            if (obj.firstPlot2D)
                figure(obj.fig2D);
                obj.firstPlot2D = 0;
                h = subplot(3,1,1);
                I = abs(obj.Image2D);
                G = fspecial('gaussian',[5 5],2);
                I = imfilter(I,G,'same');
                obj.plotImage2D = imagesc(I,'Parent',h);
                colorbar('peer',h);
                colormap(h,'gray')
                title(h,'2D FFT - Magnitude');
                h = subplot(3,1,2);
                obj.plotAbsKspace = imagesc(abs(obj.Fid2D),'Parent',h);
                colorbar('peer',h);
                colormap(h,'gray')
                title(h,'Abs of k-space');
                h = subplot(3,1,3);
                obj.plotRealKspace = imagesc(real(obj.Fid2D),'Parent',h);
                colorbar('peer',h);
                colormap(h,'gray')
                title(h,'Real of k-space');
            else
                I = abs(obj.Image2D);
                G = fspecial('gaussian',[5 5],2);
                I = imfilter(I,G,'same');
                set(obj.plotImage2D, 'CData', I);
                set(obj.plotAbsKspace, 'CData', abs(obj.Fid2D));
                set(obj.plotRealKspace, 'CData', real(obj.Fid2D));
                
            end
        end
        function Plot2DPhase(obj)
            if (obj.firstPlot2D)
                figure(obj.fig2D);
                colormap(obj.fig2D,'default');
                obj.firstPlot2D = 0;
                h = subplot(3,1,1);
                obj.plotImage2D = imagesc(abs(obj.Image2D),'Parent',h);
                colorbar('peer',h);
                title(h,'2D FFT - Magnitude');
                h = subplot(3,1,2);
                obj.plotImage2DPhase = imagesc(angle(obj.Image2D),'Parent',h);
                colorbar('peer',h);
                obj.phaseTitle = title(h,sprintf('2D FFT - Phase\t %.2f rad',angle(obj.Image2D(ceil(end/2),ceil(end/2)))));
                h = subplot(3,1,3);
                if size(obj.Image2D,1) > 2
                    im_unwrapped = Unwrap2D(obj.Image2D);
                    im_unwrapped = im_unwrapped - im_unwrapped(ceil(end/2),ceil(end/2));
                    obj.plotImage2DPhaseUnwrapped = imagesc(im_unwrapped,'Parent',h,[-2*pi 2*pi]);
                else
                    obj.plotImage2DPhaseUnwrapped = imagesc(angle(obj.Image2D),'Parent',h);
                end
                colorbar('peer',h);
                title(h,'2D FFT - Phase Unwrapped');
                %                 h = subplot(3,1,3)
                %                 obj.plotAbsKspace = imagesc(abs(obj.Fid2D),'Parent',h);
                %                 colorbar('peer',h);
                %                 title( 'Abs of k-space');
            else
                if size(obj.Image2D,1) > 2
                    im_unwrapped = Unwrap2D(obj.Image2D);
                    im_unwrapped = im_unwrapped - im_unwrapped(ceil(end/2),ceil(end/2));
                    set(obj.plotImage2DPhaseUnwrapped, 'CData', im_unwrapped);
                end
                set(obj.plotImage2D, 'CData', abs(obj.Image2D));
                set(obj.plotImage2DPhase, 'CData', angle(obj.Image2D));
                set(obj.phaseTitle,'String',sprintf('2D FFT - Phase\t %.2f rad',angle(obj.Image2D(ceil(end/2),ceil(end/2)))));
            end
            
        end
        function Plot3D(obj)
            if (obj.firstPlot3D)
                figure(obj.fig2D)
                obj.firstPlot3D = 0;
                colormaptemp = colormap('bone');
                colormap(obj.fig2D,flipud(colormaptemp));
                alphamaptemp = alphamap('rampup');
                alphamaptemp = alphamaptemp*0.4;
                alphamaptemp(1:10) = 0;
                alphamap(obj.fig2D, alphamaptemp);
                I = abs(fftshift(ifftn(fftshift( obj.Fid3D ))));
                G = fspecial3('gaussian',[5 5 5]);
                I = imfilter(I,G,'same');
                h = subplot(2,1,1);
                obj.plotImage3D = vol3d( 'CData', I, 'texture', '3D', 'Parent', h);
                title(h, '3D FFT');
                h = subplot(2,1,2);
                obj.plotAbsKspace3D = vol3d( 'CData', abs(obj.Fid3D), 'texture', '3D', 'Parent', h);
                title(h, '3D FID');
            else
                I = abs(fftshift(ifftn(fftshift( obj.Fid3D ))));
                G = fspecial3('gaussian',[5 5 5]);
                I = imfilter(I,G,'same');
                obj.plotImage3D.cdata = I;
                obj.plotAbsKspace3D.cdata  = abs(obj.Fid3D);
                obj.plotImage3D = vol3d(obj.plotImage3D);
                obj.plotAbsKspace3D = vol3d(obj.plotAbsKspace3D);
            end
            
            global MRIDATA
            save('MRIDATA3Dinfo.mat','MRIDATA');
            
        end
    end
end

