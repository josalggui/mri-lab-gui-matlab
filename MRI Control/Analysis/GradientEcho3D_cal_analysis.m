classdef GradientEcho3D_cal_analysis < MRI_cartesian_analysis
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
                
        % Items for Regridding
        kwid = 3;
        overg = 10;
        klength = 100;
        DCF = [];
        KSpace_X = [];
        KSpace_Y = [];
        KGrid = [];
    end
    
    
    methods
        function obj=GradientEcho3D_cal_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis(seq,gfig);
            obj.Name = 'GradientEcho3D Cal Analysis';
            obj.PlotSelectorNames = {'2D Image'};
            obj.PlotSelector = '2D Image';
        end
        function Execute(obj)
            global MRIDATA;
            Execute@MRI_cartesian_analysis(obj);
            seq = obj.seqClass;
            nPoints = seq.GetParameter('NPOINTS');
            nPhases = seq.GetParameter('NPHASES');
            nSlices = seq.GetParameter('NSLICES');
            if ( numel(seq.Cal3) ==  nPoints*nPhases*nSlices ) 
               if  seq.CalCheck && seq.readout_en && seq.phase_en
                    % Make the proper calibration sequence
                    if (obj.iAcq <= 1) || (isempty( obj.KGrid ))
                        obj.Calibrate ();
                    end          
               end
            end
            if length(seq.fidData) ==  nPoints*nPhases*nSlices
%                 seq.calibration_en= false;
                MRIDATA.Fid3D = reshape(seq.fidData, nPoints, nPhases, nSlices);
                obj.Fid3D = MRIDATA.Fid3D;
                obj.Fid2D = flipud(MRIDATA.Fid3D(:,:,ceil(nSlices/2))');
                MRIDATA.Fid2D = obj.Fid2D;
                s = size(obj.Fid2D);
                Gstd = seq.GetParameter('GAUSSIANFILTER');
                G = customgauss(s,Gstd*s(1),Gstd*s(2),0,0,1,0*s);
                obj.Fid2D = obj.Fid2D.*G;              
            end
            
            if ( (numel(seq.Cal3) ==  nPoints*nPhases*nSlices ) && seq.CalCheck && seq.readout_en && seq.phase_en) ||  (length(seq.fidData) ==  nPoints*nPhases*nSlices)
                switch obj.PlotSelector
                    case '2D Image'                        
                        Dat1mean = mean(seq.Dat1,2);
                        
                        if seq.calibration_en && seq.readout_en && seq.phase_en
                            obj.KGrid = gridkb( complex( obj.KSpace_X(:), obj.KSpace_Y(:) ), Dat1mean, obj.DCF(:), ceil( 0.5*(seq.GetParameter('NPOINTS') + seq.GetParameter('NPHASES'))), obj.kwid, obj.overg );
                            obj.Image2D   = fftshift(fft2(fftshift( padarray( obj.KGrid, ceil(0.5*size(obj.KGrid)),'both')  ) ));
                            Kspace=obj.KGrid;
                        else
                            padding = 0.5*[size(obj.Fid2D,1) size(obj.Fid2D,2)]; %[2^nextpow2(size(obj.Fid2D,1))-size(obj.Fid2D,1) 2^nextpow2(size(obj.Fid2D,2))-size(obj.Fid2D,2)];
                            obj.Image2D = fftshift( ifftn( fftshift( padarray( obj.Fid2D, ceil(padding/2), 'both' ) ) ) );
                            Kspace=obj.Fid2D;
                        end
                        obj.Plot2D(Kspace);
                    otherwise
                        warning('Unknown Plot','Unexpected plot type. No plot created.');
                end
                obj.SNR_MinAndMaxValues_Module();
            end
            
            results_file=fullfile(seq.path_file,'results.mat');
            save (results_file, 'seq', 'obj');
        end
        function Calibrate( obj )
            seq = obj.seqClass;
            
            disp('Calibrating KSpace Data.');
            
            % FIRST reshape the appropriate calibration sequences to MATCH
            % the appropriate points in KSpace
            % e.g. READOUT has only one line and PHASE has nPhases
            
            
            % Calculate the KSpace_X axis and offset the KSpace so that the middle value of the Cal2 crosses ZERO phase
            Cal1_angle = PhaseUnwrap( angle( seq.Cal1 ), seq.GetParameter('NPOINTS'), 1.5*pi , 0);
            Cal2_angle = PhaseUnwrap( angle( seq.Cal2 ), seq.GetParameter('NPOINTS'), pi , 1);
            OneLine_X = ( Cal1_angle - Cal1_angle( floor(end/2) ) ) - ( Cal2_angle - Cal2_angle( floor(end/2) ) );
            obj.KSpace_X = repmat(OneLine_X,[seq.GetParameter('NPHASES'), 1]);
            
            % Calculate the KSpace_Y axis
            Cal3_angle = PhaseUnwrap( angle( seq.Cal3 ), numel(seq.Cal3), pi, 0 );
            Cal4_angle = PhaseUnwrap( angle( seq.Cal4 ), numel(seq.Cal3), pi, 1 );
            obj.KSpace_Y = ( Cal3_angle - Cal3_angle( floor(end/2) ) ) - ( Cal4_angle - Cal4_angle( floor(end/2) ) );
            
            % normalize KSpace so that is it always < 0.5 and > -0.5
            obj.KSpace_X = obj.KSpace_X / 2.01 / max(abs(obj.KSpace_X)) ;
            obj.KSpace_Y = obj.KSpace_Y / 2.01 / max(abs(obj.KSpace_Y)) ; 
            
            % calculate the kernel first values
            [kern, ~] = calckbkernel( obj.kwid, obj.overg, obj.klength );

            % calculate the DCF values

            obj.DCF = calcdcflut( complex(obj.KSpace_X, obj.KSpace_Y ), ceil( 0.5*(seq.GetParameter('NPOINTS') + seq.GetParameter('NPHASES'))), kern);
            
        end % Calibrate Sequence based off of calibration data
        
        function Plot2D(obj,Kspace)
            global MRIDATA
            seq = obj.seqClass;

            Plot2D@MRI_cartesian_analysis(obj,Kspace);
            MRIDATA.KGrid = obj.KGrid;
            MRIDATA.KSpace_X = obj.KSpace_X;
            MRIDATA.KSpace_Y = obj.KSpace_Y;
            MRIDATA.Data1 = seq.Dat1;    
        end
        
        
     
        
    end
end

