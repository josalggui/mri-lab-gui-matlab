classdef VIEWS_analysis < MRI_cartesian_analysis_JM
    %ANALYSIS for Gradient Echo 3D     
    
    methods
        function obj=VIEWS_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'MASSIF Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
            global rawData;
            
            % Load output from rawData
            tA = rawData.outputs.measurements(:,1)*1e6;
            sA = mean(rawData.outputs.measurements(:,2:end-3),2);
            tB = rawData.outputs.measurements0(:,1)*1e6;
            sB = mean(rawData.outputs.measurements0(:,2:end),2);
            sC = sA./sB;
            sA = sA/sA(1);
            sB = sB/sB(1);
            sC = sC/sC(1);
            
            % Plot FID
            h = obj.figFID;
%             plot(tA,abs(sA),'b',tA,real(sA),'r',tA,imag(sA),'g',...
%                 tB,abs(sB),'--b',tB,real(sB),'--r',tB,imag(sB),'--g','Parent',h)
            plot(tA,abs(sA),'b',tA,real(sA),'r',tA,imag(sA),'g','Parent',h)
            xlabel('t (\mus)')
            ylabel('signal (a.u.)')
            legend(h,{'abs(s)','real(s)','imag(s)'})
            axis(h,[0 max(tA) -1.5 1.5])
            
            h = obj.figFFT;
            plot(tB,abs(sC),tB,real(sC),tB,imag(sC),'Parent',h)
            xlabel('t (\mus)')
            ylabel('signal (a.u.)')
            legend(h,{'abs(s)','real(s)','imag(s)'})
            axis(h,[0 max(tB) -1.5 1.5])
        end
    end
end

