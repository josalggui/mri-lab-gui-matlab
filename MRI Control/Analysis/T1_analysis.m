classdef T1_analysis < SCAN_analysis
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        UsedRepDelay;
        
    end
    
    methods
        function obj=T1_analysis(seq,gfig,par)
            obj = obj@SCAN_analysis(seq,gfig,par);
            obj.Name = 'T1 Analysis';
            obj.PlotSelectorNames = {'Sequence'};
            obj.PlotSelector = 'Sequence';
        end
        function Execute(obj)
            Execute@SCAN_analysis(obj);
            if obj.iAcq == length(obj.ScanParValues)
                switch obj.ScanPar
                    case 'INVERSIONTIME'
                        obj.FitT1();
                    case 'REPETITIONDELAY'
                        obj.FitTR();
                end
            end
        end
        function FitT1(obj)
            xdata = obj.GetXData();
            ydata = obj.GetYData();
            [ymin,imin] =  min(ydata);
            f = fittype('a*(1-2*exp(-(x-c)/b))');
            opt = fitoptions('Exclude',xdata<=xdata(imin),'Method','NonlinearLeastSquares','StartPoint',[max(ydata) xdata(imin)*2 0]);
            if sum(xdata>xdata(imin)) > 3
                cfun = fit(xdata',ydata',f,opt);
                figure(obj.figScan);
                h = gca;
                l1 = plot(h,xdata,ydata);hold(h,'on');
                plot(h,xdata,cfun(xdata),'r');hold(h,'off');
                set(l1,'Marker','o');
                title(h,sprintf('Inversion Recovery, T1 = %.2f ms, Tmin = %.2f ms + %.2f ms',cfun.b,xdata(imin),-cfun.c));
                xlabel(h,'Inversion Time, ms');set(h,'XScale','log');
                grid(h,'on');
            else
                errordlg('Not enough points for the fit','FitError','modal');
            end
        end
        function FitTR(obj)
           % grab data first
           seq = obj.seqClass;
           xdatafull = obj.GetXData();
           ydatafull = obj.GetYData();
           % drop off the first 5 of the x and y data
           indcut = 20;
           xdata = xdatafull(indcut+1:end);
           xdata1 = log10(xdata);
           ydata = ydatafull(indcut+1:end);
           
           % try fitting to an error function
           f = fittype('y0+a*erf(x-x0)');
           y0 = mean(ydata);%min(ydata)+0.5*(max(ydata)-min(ydata));
           a = max(ydata)-y0;
           x0 = xdata1( find(ydata>=y0,1,'first') );
           opt = fitoptions('Method','NonlinearLeastSquares','StartPoint',[a,x0,y0]);
           cfun = fit(xdata1',ydata',f,opt);
           xdatafit = logspace(log10(xdata(1)),log10(xdata(end)),1000);
           ydatafit =  cfun(log10(xdatafit));
           
           % calculate percentage diff and find index
           percutoff = 1;
           perdiff = (max(abs(ydatafit))-ydatafit)./max(abs(ydatafit))*100;
           ind = find(perdiff <= percutoff , 1, 'first');
           
           if isempty(ind)
               obj.UsedRepDelay = xdata(end)*MyUnits.ms;
               ind = numel(xdatafit);
           else
               ind = ind + 1;
               obj.UsedRepDelay = xdatafit(ind)*MyUnits.ms;
           end
 
           if obj.UsedRepDelay < (50*MyUnits.ms)
               obj.UsedRepDelay = 50*MyUnits.ms;
           end
           
           % set the sequence class with this rep delay now
           seq.SetParameter('REPETITIONDELAY',obj.UsedRepDelay);
           
           % set the xaxis as log
           figure(obj.figScan);
           h = gca;
           set(h,'XScale','log');hold(h,'on');plot(h,xdatafit,ydatafit,'r');
           plot( xdatafit( ind ), ydatafit( ind ), 'r*', 'MarkerSize', 12 );
           title(h, sprintf('Repetition Delay: TR = %.2f ms', max( 50, xdatafit(ind) ) ) );
            
        end
    end
end
