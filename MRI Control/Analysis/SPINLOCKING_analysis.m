classdef SPINLOCKING_analysis < MRI_cartesian_analysis_JM   
    
    methods
        function obj=SPINLOCKING_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'SPINLOCKING Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
            global rawData;
            seqType=  rawData.inputs.SeqType;
            
            if seqType==1
                h = obj.figFID;
%                 plot(rawData.outputs.signal(:,1)*1e3+16.25e-3,real(rawData.outputs.signal(:,2)),...
%                     rawData.outputs.signal(:,1)*1e3+16.25e-3,imag(rawData.outputs.signal(:,2)),...
%                     rawData.outputs.signal(:,1)*1e3+16.25e-3,abs(rawData.outputs.signal(:,2)),'Parent',h);
               
    plot(rawData.outputs.signal(:,1)*1e3+16.25e-3,abs(rawData.outputs.signal(:,2)),'Parent',h);
                
xlabel(h,'Time (ms)');
                ylabel(h,'Signal (mV)')
                grid on
                title(h,'Full Signal acquired')
                legend(h,'Real','Imag','Abs')
                
                h = obj.figFFT;
                gslice = rawData.inputs.gslice;
                plot(rawData.outputs.profile(:,1)*1e2,rawData.outputs.profile(:,2),'Parent',h);
                if gslice==0
                    plot(rawData.outputs.profile(:,1)/42.6d6/1*1e2,rawData.outputs.profile(:,2),'Parent',h);
                end

                
                grid on
                xlabel(h,'z (cm)');
                ylabel(h,'pixel magnitude (a.u.)')
                title(h,'Spin profile')
            end
        end   
    end
end