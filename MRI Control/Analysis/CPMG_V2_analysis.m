classdef CPMG_V2_analysis < MRI_cartesian_analysis_JM   
    
    methods
        function obj=CPMG_V2_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'CPMG_V2 Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
            
            global rawData;
            
            nEchoes = rawData.inputs.nEchoes;
            nPoints = rawData.inputs.nPoints;
            t = rawData.outputs.fidList(:,1)*1e3;
            fid = abs(rawData.outputs.fidList(:,2));
            
%             if(nEchoes==1)
%                 m = max(fid(ceil(end/2)+1:end));
%                 warndlg(strcat('Max signal = ',num2str(m),' mV'))
%             end
            
            h = obj.figFID;
            plot(t,fid,'.','Parent',h);
            axis(h,[0,inf,0,inf])
            xlabel(h,'Time (ms)');
            ylabel(h,'Signal (mV)')
            grid on
            title(h,'Full Signal acquired')
            
            
            h = obj.figFFT;
            plot(t(nPoints+1:end),abs(fid(nPoints+1:end)),'.','Parent',h);
            xlabel(h,'Time (ms)');
            ylabel(h,'Signal (mV)')
            title(h,'Signal without ring down')
            grid on
            pause(0.1)
        end
        
    end
end

