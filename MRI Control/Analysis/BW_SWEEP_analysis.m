classdef BW_SWEEP_analysis < MRI_cartesian_analysis_JM   
    
    methods
        function obj=BW_SWEEP_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'IECO_TEST Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
            close(1)
            figure(1)
            set(gcf, 'Position', [100, 500, 1500, 400])
            global rawData;
            bwMin = rawData.inputs.bwMin*1e-3;
            bwMax = rawData.inputs.bwMax*1e-3;
            nPoints = rawData.inputs.nPoints;
            steps = rawData.inputs.steps;
            
            subplot(1,3,1)
            imagesc([bwMin bwMax],[0 nPoints],abs(rawData.outputs.fidList))
            ylabel('Point')
            xlabel('BW (kHz)')
            
            subplot(1,3,2)
            semilogx(logspace(log10(bwMin),log10(bwMax),steps),abs(rawData.outputs.fidMean)*1e3)
            ylabel('Mean signa (mV)')
            xlabel('BW (kHz)')
            ylim([0,1.1*max(abs(rawData.outputs.fidMean)*1e3)])
            
            subplot(1,3,3)
            plot(logspace(log10(bwMin),log10(bwMax),steps),abs(rawData.outputs.fidStd)*1e3)
            ylabel('Standard deviation (mV)')
            xlabel('BW (kHz)')
            ylim([0,max(abs(rawData.outputs.fidStd)*1e3)])
        end
        
    end
end

