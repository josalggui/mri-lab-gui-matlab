classdef DiagonalSPRITE_analysis < MRI_cartesian_analysis
    %ANALYSIS for Gradient Echo 3D     
    
    methods
        function obj=DiagonalSPRITE_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis(seq,gfig);
            obj.Name = 'Diagonal SPRITE Analysis';
            obj.PlotSelectorNames = {'2D Image','3D Image'};
            obj.PlotSelector = '2D Image';
        end
        function Execute(obj)
            global MRIDATA;
            Execute@MRI_cartesian_analysis(obj);
            seq = obj.seqClass;
            nReadouts = seq.GetParameter('NREADOUTS');
            nPhases = seq.GetParameter('NPHASES');
            nSlices = seq.GetParameter('NSLICES');
            if length(seq.fidData) ==  nReadouts*nPhases*nSlices
                MRIDATA.Fid3D = reshape(seq.fidData, nReadouts, nPhases, nSlices);
                MRIDATA.Data4D(numel(MRIDATA.Data4D)+1).Fid3D = MRIDATA.Fid3D;
                obj.Fid3D = MRIDATA.Fid3D;
                obj.Fid2D = flipud(MRIDATA.Fid3D(:,:,ceil(nSlices/2))');
                MRIDATA.Fid2D = obj.Fid2D;
                padding = [2^nextpow2(size(obj.Fid2D,1))-size(obj.Fid2D,1) 2^nextpow2(size(obj.Fid2D,2))-size(obj.Fid2D,2)];%0.5*[size(obj.Fid2D,1) size(obj.Fid2D,2)]; %
                s = size(obj.Fid2D);
                Gstd = 3;%seq.GetParameter('GAUSSIANFILTER');
                G = customgauss(s,Gstd*s(1),Gstd*s(2),0,0,1,0*s);
                obj.Fid2D = obj.Fid2D.*G;
                
                obj.Image2D = fftshift( ifftn( fftshift( padarray( obj.Fid2D, ceil(padding/2), 'both' ) ) ) );
                switch obj.PlotSelector
                    case '2D Image'
                        Kspace=obj.Fid2D;
                        obj.Plot2D(Kspace);
                    case '3D Image'
                        obj.Plot3D();
                    otherwise
                        warning('Unknown Plot','Unexpected plot type. No plot created.');
                end
                obj.SNR_MinAndMaxValues_Module();
            end
            results_file=fullfile(seq.path_file,'results.mat');
            save (results_file, 'seq', 'obj');
            save('tempMRIDATA.mat','MRIDATA');
        end
    end
end

