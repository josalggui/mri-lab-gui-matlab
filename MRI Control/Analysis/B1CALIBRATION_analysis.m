classdef B1CALIBRATION_analysis < MRI_cartesian_analysis_JM   
    
    methods
        function obj=B1CALIBRATION_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'B1CALIBRATION Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
            global rawData;
        end

    end
end
