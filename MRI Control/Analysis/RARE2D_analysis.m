classdef RARE2D_analysis < MRI_cartesian_Units_analysis
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
       
    methods
        function obj=RARE2D_analysis(seq,gfig)
            obj = obj@MRI_cartesian_Units_analysis(seq,gfig);
            obj.Name = 'RARE Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Image';
        end
        function Execute(obj)
            global rawData;
            nPoints = rawData.inputs.nPoints;
            fov = rawData.inputs.fov*1d3;
            kMax = rawData.aux.kMax*1d-3;
            axes = rawData.inputs.axes;
            data = reshape(rawData.kSpace.sampled(:,4),nPoints);
            imagen = rawData.kSpace.imagen;
            tMax = rawData.inputs.acqTime/2*1e6;
            
            if nPoints(2)~=1
                figure(2)
                set(gcf, 'Position', [200, 550, 1400, 400])
                
                subplot(1,3,1)
                imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],imagen)
                title('Image');
                xlabel(strcat(axes(2),' (mm)'))
                ylabel(strcat(axes(1),' (mm)'))
                colormap gray
                
                subplot(1,3,2)
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],(abs(data)))
                title('k-space magnitude')
                xlabel(strcat('k',axes(2),' (mm^{-1})'))
                ylabel(strcat('k',axes(1),' (mm^{-1})'))
                colormap gray
                
                subplot(1,3,3)
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],(angle(data)))
                title('k-space phase')
                xlabel(strcat('k',axes(2),' (mm^{-1})'))
                ylabel(strcat('k',axes(1),' (mm^{-1})'))
                caxis([-pi,pi])
                colormap gray
            end
            
            figure(3)
            set(gcf, 'Position', [200, 50, 1400, 400])
            
            echoSignal = rawData.kSpace.avT2Cal*1e3;
            realMax = max(abs(real(echoSignal)));
            imagMax = max(abs(imag(echoSignal)));
            limite = 1.1*max([realMax,imagMax]);
            
            subplot(1,3,1)
            plot(abs(echoSignal))
            title('Abs of T2 calibration data')
            xlabel('Echo index')
            ylabel('Signal magnitude (mV)')
            subplot(1,3,2)
            plot(angle(echoSignal))
            title('Phase of T2 calibration data')
            xlabel('Echo index')
            ylabel('Phase (rads)')
            subplot(1,3,3)
            plot(real(echoSignal),imag(echoSignal),'.')
            xlabel('real part')
            ylabel('imag part')
            title('Echo signal')
            xlim([-limite,limite])
            ylim([-limite,limite])
            
            h = obj.figFID;
            t1 = linspace(-tMax(1),tMax(1),nPoints(1));
            t2 = linspace(-tMax(1),tMax(1),nPoints(2));
            plot(t1,abs(data(:,ceil(nPoints(2)/2),ceil(nPoints(3)/2))),...
                t2,abs(data(ceil(nPoints(1)/2),:,ceil(nPoints(3)/2))),...
                'Parent',h);
            xlabel(h,'t (us)');
            ylabel(h,'signal (a.u.)')
            title(h,'Signal magnitude')
            pause(0.1)
            
            h = obj.figFFT;
            x1 = linspace(-fov(1)/2,fov(1)/2,nPoints(1));
            x2 = linspace(-fov(2)/2,fov(2)/2,nPoints(2));
            plot(x1,imagen(:,ceil(nPoints(2)/2),ceil(nPoints(3)/2)),...
                x2,imagen(ceil(nPoints(1)/2),:,ceil(nPoints(3)/2)),...
                'Parent',h);
            legend(h,axes(1),axes(2));
            xlabel(h,'d (mm)');
            ylabel(h,'pixel magnitude (a.u.)')
            title(h,'Image')
            pause(0.1)
        end
    end
end

