classdef GradientEchoMultiSlice_analysis < MRI_cartesian_analysis
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
       
    methods
        function obj=GradientEchoMultiSlice_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis(seq,gfig);
            obj.Name = 'GradientEcho with Slice Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','2D Slices','2D Slices PP'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
            global MRIDATA;
            Execute@MRI_cartesian_analysis(obj);
            seq = obj.seqClass;
            nPoints = seq.GetParameter('NPOINTS');
            if seq.phase_en
                nPhases = seq.GetParameter('NPHASES');
            else
                nPhases = 1;
            end
            if seq.slice_en
                nSlices = seq.GetParameter('NSLICES');
            else
                nSlices = 1;
            end
            
            if length(seq.fidData) ==  nPoints*nPhases*nSlices
                MRIDATA.Fid3D = reshape(seq.fidData, nPoints, nPhases, nSlices);
                
                obj.Fid3D = MRIDATA.Fid3D;
                obj.Fid2D = MRIDATA.Fid3D(:,:,ceil(nSlices/2));
                MRIDATA.Fid2D = obj.Fid2D;
                padding = 0.1*[size(obj.Fid2D,1) size(obj.Fid2D,2)]; %[2^nextpow2(size(obj.Fid2D,1))-size(obj.Fid2D,1) 2^nextpow2(size(obj.Fid2D,2))-size(obj.Fid2D,2)];
                obj.Fid2D = padarray( obj.Fid2D, ceil(padding/2), 'both' );
                                
                obj.Image2D = fftshift( ifftn( fftshift( obj.Fid2D ) ) );
                switch obj.PlotSelector
                    case '2D Image'
                        obj.Plot2DS();
                    case '2D Phase'
                        obj.Plot2DPhase();
                    case '2D Slices'
                        obj.Plot2DSlice();
                    case '2D Slices PP'
%                         obj.Plot2DSlice();
                        obj.Plot2DSlicePP();
                    otherwise
                        warning('Unknown Plot','Unexpected plot type. No plot created.');
                end
                obj.SNR_MinAndMaxValues_Module();
            end
            results_file=fullfile(seq.path_file,'results.mat');
            save (results_file, 'seq', 'obj');
        end
        
        function Plot2DSlicePP(obj)
            global MRIDATA;
            seq = obj.seqClass;
            QuickPostProcess(seq.GetParameter('BLUR'),'2D Slice Presentation',seq.GetParameter('GSTD'),'gray',MRIDATA,obj.firstPlot3D);
            
        end % Plot2DSlicePP
    end
end

