classdef EddyCurrentSE_analysis < MRI_cartesian_analysis_JM   
    
    methods
        function obj=EddyCurrentSE_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'EddyCurrentSE Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)      
            global rawData;
            global scanner;
            t = rawData.outputs.fid(:,1)*1e3;
            s1 = abs(rawData.outputs.fid(:,2))*1e3;
            s2 = abs(rawData.outputs.fid(:,3))*1e3;
            s3 = abs(rawData.outputs.fid(:,4))*1e3;
            s11 = abs(rawData.outputs.fit(:,2))*1e3;
            s22 = abs(rawData.outputs.fit(:,3))*1e3;
            s33 = abs(rawData.outputs.fit(:,4))*1e3;
            
            s = abs(rawData.outputs.fid(:,2:4))*1e3;
            trf = rawData.inputs.pulseTime;
            td = scanner.grad_delay*1e-6;
            TE = rawData.inputs.TE;
            
            tau = 240e-6;
            TEtheo = (TE-trf/2-td-tau*(1-exp(-(TE/2-trf/2-td)/tau)))*1e3;
            
            % Fitting
            TE1 = rawData.outputs.expTE(1)*1e3;
            TE2 = rawData.outputs.expTE(2)*1e3;
            TE3 = rawData.outputs.expTE(3)*1e3;
            
            h = obj.figFID;
            plot(t,s1,'b',t,s2,'r',t,s3,'g',...
                [TEtheo TEtheo],[0 max(s(:))*1.1],'k--',...
                [TE1 TE1],[0 max(s(:))*1.1],'b--',...
                [TE2 TE2],[0 max(s(:))*1.1],'r--',...
                [TE3 TE3],[0 max(s(:))*1.1],'g--',...
                'Parent',h);
            ylim(h,[0,max(s(:))*1.1]);
            xlim(h,[min(t),max(t)])
            ylabel(h,'Signal (mV)')
            xlabel(h,'Time (ms)')
            title(h,'FIDs')
            legend(h,'x axis','y axis','z axis');
            
            h = obj.figFFT;
            plot(t,s11,'b',t,s22,'r',t,s33,'g',...
                [TEtheo TEtheo],[0 max(s(:))*1.1],'k--',...
                [TE1 TE1],[0 max(s(:))*1.1],'b--',...
                [TE2 TE2],[0 max(s(:))*1.1],'r--',...
                [TE3 TE3],[0 max(s(:))*1.1],'g--',...
                'Parent',h);
            ylim(h,[0,max(s(:))*1.1]);
            xlim(h,[min(t),max(t)])
            ylabel(h,'Signal (mV)')
            xlabel(h,'Time (ms)')
            title(h,'Fitting')
            legend(h,'x axis','y axis','z axis');
        end  
    end
end

