classdef EPI2D_analysis < MRI_cartesian_analysis_JM   
    
    methods
        function obj=EPI2D_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'EPI2D Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
            
            global rawData;

%             h = obj.figFID;
%             plot(t,fid,'.','Parent',h);
%             axis(h,[0,inf,0,inf])
%             xlabel(h,'Time (ms)');
%             ylabel(h,'Signal (mV)')
%             grid on
%             title(h,'Full Signal acquired')
%             
%             
%             h = obj.figFFT;
%             plot(t(nPoints+1:end),abs(fid(nPoints+1:end)),'.','Parent',h);
%             xlabel(h,'Time (ms)');
%             ylabel(h,'Signal (mV)')
%             title(h,'Signal without ring down')
%             grid on
%             pause(0.1)
        end
        
    end
end