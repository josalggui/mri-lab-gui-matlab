classdef GE3DPREEMPH_Units_analysis < MRI_cartesian_Units_analysis
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
       
    methods
        function obj=GE3DPREEMPH_Units_analysis(seq,gfig)
            obj = obj@MRI_cartesian_Units_analysis(seq,gfig);
            obj.Name = 'GE3DPREEMPH Real Units Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Image';
        end
        function Execute(obj)
            global MRIDATA;
            global rawData;
            
%             %t0 = (RF+GD+3*CR+obj.PhaseTime)*1e6;
%             figure(7)
%             n = rawData.inputs.matrix;
%             t0 = 0;
%             tmax = t0+rawData.inputs.acqTime*1e6;
%             tvector = linspace(t0,tmax,n(1));
%             plot(tvector,abs(rawData.outputs.kSpace(:,4)));
            
%             n = rawData.inputs.matrix;
%             
%             kSpace = squeeze(reshape(rawData.outputs.kSpace(:,4),n));
%             
%             imagen = abs(ifftshift(ifftn(kSpace)));
%             
%             figure(6)
%             imagesc(imagen)
%             colormap gray
            
            
%             Execute@MRI_cartesian_Units_analysis(obj);
%             seq = obj.seqClass;
%             nPoints = seq.NRO;
%             nPhases = seq.NPH;
%             nSlices = seq.NSL;
%             if length(seq.fidData) ==  nPoints*nPhases*nSlices
%                 MRIDATA.Fid3D = reshape(seq.fidData, nPoints, nPhases, nSlices);
%                 obj.Fid3D = MRIDATA.Fid3D;
%                 obj.Fid2D = flipud(MRIDATA.Fid3D(:,:,ceil(nSlices/2))');
%                 MRIDATA.Fid2D = obj.Fid2D;
%                 padding = 0;%[2^nextpow2(size(obj.Fid2D,1))-size(obj.Fid2D,1) 2^nextpow2(size(obj.Fid2D,2))-size(obj.Fid2D,2)];
%                 obj.Image2D = fftshift( ifftn( fftshift( padarray( obj.Fid2D, ceil(padding/2), 'both' ) ) ) );
%                               
%                 switch obj.PlotSelector
%                     case '2D Image'
%                         Kspace = obj.Fid2D;
%                         obj.Plot2D(Kspace);
%                     case '2D Phase'
%                         obj.Plot2DPhase();
%                     case '3D Image'
%                         obj.Plot3D();
%                     otherwise
%                         warning('Unknown Plot','Unexpected plot type. No plot created.');
%                 end
%                 obj.SNR_MinAndMaxValues_Module();
%             end
%             results_file=fullfile(seq.path_file,'results.mat');
%             save (results_file, 'seq', 'obj');

            close (1);
            nPoints = rawData.inputs.nPoints;
            fov = rawData.inputs.fov*1d3;
            kMax = rawData.aux.kMax*1d-3;
            acqTime = rawData.inputs.acqTime*1d6;
            axes = rawData.inputs.axes;
            kSpace = reshape(rawData.kSpace.sampled(:,4),nPoints);
            imagen = rawData.kSpace.imagen;
            
            figure(1)
            set(gcf, 'Position', [100, 500, 1500, 400])
            
            subplot(1,3,1)
            if(nPoints(3)==1)
                imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],imagen)
            else
                imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],squeeze(imagen(:,:,ceil(nPoints(3)/2))));
            end
            title('image')
            xlabel(strcat(axes(2),' (mm)'))
            ylabel(strcat(axes(1),' (mm)'))
            
            subplot(1,3,2)
            if(nPoints(3)==1)
%                 imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],log(abs(kSpace)))
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],real(kSpace))
            else
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],squeeze(log(abs(kSpace(:,:,ceil(nPoints(3)/2))))));
            end
            title('k-space magnitude')
            xlabel(strcat('k',axes(2),' (mm^{-1})'))
            ylabel(strcat('k',axes(1),' (mm^{-1})'))
            
            subplot(1,3,3)
            if(nPoints(3)==1)
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],angle(kSpace))
            else
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],squeeze(angle(kSpace(:,:,ceil(nPoints(3)/2)))));
            end
            title('k-space phase')
            xlabel(strcat('k',axes(2),' (mm^{-1})'))
            ylabel(strcat('k',axes(1),' (mm^{-1})'))
            colormap gray
            
            
            h = obj.figFID;
            t1 = linspace(-acqTime(1)/2,acqTime(1)/2,nPoints(1));
            t2 = linspace(-acqTime(1)/2,acqTime(1)/2,nPoints(2));
            plot(t1,abs(kSpace(:,ceil(nPoints(2)/2),ceil(nPoints(3)/2))),...
                t2,abs(kSpace(ceil(nPoints(1)/2),:,ceil(nPoints(3)/2))),...
                'Parent',h);
            xlabel(h,'t (us)');
            ylabel(h,'signal (a.u.)')
            title(h,'Signal magnitude')
            pause(0.1)
            
            h = obj.figFFT;
            x1 = linspace(-fov(1)/2,fov(1)/2,nPoints(1));
            x2 = linspace(-fov(2)/2,fov(2)/2,nPoints(2));
            plot(x1,imagen(:,ceil(nPoints(2)/2),ceil(nPoints(3)/2)),...
                x2,imagen(ceil(nPoints(1)/2),:,ceil(nPoints(3)/2)),...
                'Parent',h);
            xlabel(h,'d (mm)');
            ylabel(h,'pixel magnitude (a.u.)')
            title(h,'Image')
            xlim(h,[-max(fov(1:2))/2,max(fov(1:2))/2])
            pause(0.1)
        end
    end
end

