classdef DELAYTEST_analysis < MRI_cartesian_analysis_JM
    %ANALYSIS for Gradient Echo 3D     
    
    methods
        function obj=DELAYTEST_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'DELAYTEST Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
%            
             global rawData;
             figure(6)
             plot(rawData.outputs.measurements(:,1)*1e6,abs(rawData.outputs.measurements(:,2)))
             xlabel('t (us)')
             ylabel('s (a.u.)')
        end
        
    end
end

