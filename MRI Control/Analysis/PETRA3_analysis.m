classdef PETRA3_analysis < MRI_cartesian_analysis_JM
    %ANALYSIS for Gradient Echo 3D     
    
    methods
        function obj=PETRA3_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'PETRA 3 Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
            global rawData;
            close (1);
            nPoints = rawData.inputs.nPoints;
            fov = rawData.inputs.fov*1d3;
            kMax = rawData.aux.kMax*1d-3;
            tMax = rawData.inputs.tMax*1d6;
            axes = rawData.inputs.axes;
            kSpace = reshape(rawData.kSpace.interpolated(:,4),nPoints);
            imagenFFT = rawData.kSpace.complexFFTimagen;
            
            figure(1)
            set(gcf, 'Position', [100, 500, 1500, 400])
            
            subplot(2,3,1)
            if(nPoints(3)==1)
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],log(abs(kSpace)))
            else
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],squeeze(log(abs(kSpace(:,:,ceil(nPoints(3)/2))))));
            end
            title('Log k-space absolute value')
            colorbar
            xlabel(strcat(axes(2),' (mm)'))
            ylabel(strcat(axes(1),' (mm)'))
            pbaspect([kMax(2) kMax(1) 1]);            
            subplot(2,3,2)
            if(nPoints(3)==1)
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],(real(kSpace)))
            else
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],squeeze((real(kSpace(:,:,ceil(nPoints(3)/2))))));
            end
            title('k-space real')
            colorbar
            xlabel(strcat('k',axes(2),' (mm^{-1})'))
            ylabel(strcat('k',axes(1),' (mm^{-1})'))
            pbaspect([kMax(2) kMax(1) 1]);
            
            subplot(2,3,3)
            if(nPoints(3)==1)
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],angle(kSpace))
            else
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],squeeze(angle(kSpace(:,:,ceil(nPoints(3)/2)))));
            end
            title('k-space phase')
            colorbar
            xlabel(strcat('k',axes(2),' (mm^{-1})'))
            ylabel(strcat('k',axes(1),' (mm^{-1})'))
            colormap gray
            pbaspect([kMax(2) kMax(1) 1]);            
            
            
            subplot(2,3,4)
            if(nPoints(3)==1)
                imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],abs(imagenFFT))
            elseif(nPoints(2)==1 && nPoints(3)>1)
                imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],squeeze(abs(imagenFFT)))
            else
                imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],squeeze(abs(imagenFFT(:,:,ceil(nPoints(3)/2)))));
            end
            title('Abs FFT')
            colorbar
            xlabel(strcat(axes(2),' (mm)'))
            ylabel(strcat(axes(1),' (mm)'))
            pbaspect([fov(2) fov(1) 1]);
            
            subplot(2,3,5)
            if(nPoints(3)==1)
                imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],real(imagenFFT))
            elseif(nPoints(2)==1 && nPoints(3)>1)
                imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],squeeze(real(imagenFFT)))
            else
                imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],squeeze(real(imagenFFT(:,:,ceil(nPoints(3)/2)))));
            end
            title('Real FFT')
            colorbar
            xlabel(strcat(axes(2),' (mm)'))
            ylabel(strcat(axes(1),' (mm)'))
            pbaspect([fov(2) fov(1) 1]);
            
            subplot(2,3,6)
            if(nPoints(3)==1)
                imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],angle(imagenFFT))
            elseif(nPoints(2)==1 && nPoints(3)>1)
                imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],squeeze(angle(imagenFFT)))
            else
                imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],squeeze(angle(imagenFFT(:,:,ceil(nPoints(3)/2)))));
            end
            title('Phase FFT')
            colorbar
            xlabel(strcat(axes(2),' (mm)'))
            ylabel(strcat(axes(1),' (mm)'))
            pbaspect([fov(2) fov(1) 1]);
            
            
            
            
       
            
            
%                 Uncomment this tabulated block to push FFT by ART in second row of Figure
%                 nIter=1;
%                 lambda=1;
%                 imagen = (artsequence(nIter,lambda,1,rawData));
% 
%                 subplot(2,3,4)
%                 if(nPoints(3)==1)
%                     imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],abs(imagen))
%                 elseif(nPoints(2)==1 && nPoints(3)>1)
%                     imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],squeeze(abs(imagen)))
%                 else
%                     imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],squeeze(abs(imagen(:,:,ceil(nPoints(3)/2)))));
%                 end
%                 title('Abs ART')
%                 colorbar
%                 xlabel(strcat(axes(2),' (mm)'))
%                 ylabel(strcat(axes(1),' (mm)'))
%                 pbaspect([fov(2) fov(1) 1]);
% 
%                 subplot(2,3,5)
%                 if(nPoints(3)==1)
%                     imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],real(imagen))
%                 elseif(nPoints(2)==1 && nPoints(3)>1)
%                     imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],squeeze(real(imagen)))
%                 else
%                     imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],squeeze(real(imagen(:,:,ceil(nPoints(3)/2)))));
%                 end
%                 title('Real ART')
%                 colorbar
%                 xlabel(strcat(axes(2),' (mm)'))
%                 ylabel(strcat(axes(1),' (mm)'))
%                 pbaspect([fov(2) fov(1) 1]);
% 
%                 subplot(2,3,6)
%                 if(nPoints(3)==1)
%                     imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],angle(imagen))
%                 elseif(nPoints(2)==1 && nPoints(3)>1)
%                     imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],squeeze(angle(imagen)))
%                 else
%                     imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],squeeze(angle(imagen(:,:,ceil(nPoints(3)/2)))));
%                 end
%                 title('Phase ART')
%                 colorbar
%                 xlabel(strcat(axes(2),' (mm)'))
%                 ylabel(strcat(axes(1),' (mm)'))
%                 pbaspect([fov(2) fov(1) 1]);

            h = obj.figFID;
            t1 = linspace(-tMax(1),tMax(1),nPoints(1));
            t2 = linspace(-tMax(1),tMax(1),nPoints(2));
            plot(t1,abs(kSpace(:,ceil(nPoints(2)/2),ceil(nPoints(3)/2))),...
                t2,abs(kSpace(ceil(nPoints(1)/2),:,ceil(nPoints(3)/2))),...
                'Parent',h);
            xlabel(h,'t (us)');
            ylabel(h,'signal (a.u.)')
            title(h,'Signal magnitude')
            pause(0.1)
            
            h = obj.figFFT;
            x1 = linspace(-fov(1)/2,fov(1)/2,nPoints(1));
            x2 = linspace(-fov(2)/2,fov(2)/2,nPoints(2));
            plot(x1,abs(imagenFFT(:,ceil(nPoints(2)/2),ceil(nPoints(3)/2))),...
                x2,abs(imagenFFT(ceil(nPoints(1)/2),:,ceil(nPoints(3)/2))),...
                'Parent',h);
            legend(h,'Vertical spin profile abs FFT','Horizontal spin profile abs FFT');
            xlabel(h,'d (mm)');
            ylabel(h,'pixel magnitude (a.u.)')
            title(h,'Image')
            pause(0.1)
        end
    end
end

