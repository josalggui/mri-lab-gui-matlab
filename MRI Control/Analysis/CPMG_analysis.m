classdef CPMG_analysis < MRI_analysis
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        t2_cfun;
        t2_gof;
        t2_lineFit1;
        t2_lineFit2;
        t2_xvector;
    end
    
    methods
        function obj=CPMG_analysis(seq,gfig)
            obj = obj@MRI_analysis(seq,gfig);
            obj.Name = 'CPMG Analysis';
        end
        function Execute(obj)
            t2str = obj.FitT2();
            Execute@MRI_analysis(obj);
            if obj.ShowFIDCaption
                set(obj.CaptionFID,'string',t2str);
            else
                set(obj.CaptionFID,'string','');
            end
        end
        function t2str = FitT2(obj)
            seq = obj.seqClass;
            nPoints = seq.GetParameter('NPOINTS');
            nEchos = seq.GetParameter('NECHOS');
            tau = seq.GetParameter('TAUDELAY');
            fidData = seq.fidData;
            nPtBlock = floor(nPoints/nEchos);
            t2data = reshape(fidData,nPtBlock,nEchos);
            t2dataMax = max(abs(real(t2data)),[],1);
            t2x = ((0:length(t2dataMax)-1)+0.5)./length(t2dataMax)*2*(tau/MyUnits.ms)*nEchos;
            [cfun,gof] = fit(t2x',t2dataMax','exp1');
            ci = confint(cfun);
            t2 = -1.0/cfun.b;
            t2min = -1.0/ci(1,2);
            t2max = -1.0/ci(2,2);
            t2str = sprintf('T2 = %.3f (%.3f to %.3f) ms',t2,t2min,t2max);
            obj.t2_xvector = t2x;
            obj.t2_cfun = cfun;
            obj.t2_gof = gof;
        end
        function PlotFID(obj)
            h = obj.figFID;
 %           set(obj.gui_fig,'CurrentAxes',h)
            seq = obj.seqClass;
            cfun = obj.t2_cfun;
            t2x = obj.t2_xvector;
            if (obj.firstPlotFID)
                cla;
                PlotFID@MRI_analysis(obj);
                hold(h,'on');
                obj.t2_lineFit1 = plot(h,((0:length(t2x)-1)+0.5)/length(t2x)*seq.timeVector(end)/MyUnits.us,cfun.a*exp(cfun.b*t2x),'Color','g');
                obj.t2_lineFit2 = plot(h,((0:length(t2x)-1)+0.5)/length(t2x)*seq.timeVector(end)/MyUnits.us,-cfun.a*exp(cfun.b*t2x),'Color','g');
                hold(h,'off');
            else
                PlotFID@MRI_analysis(obj);
                set(obj.t2_lineFit1,'YData',cfun.a*exp(cfun.b*t2x));
                set(obj.t2_lineFit2,'YData',-cfun.a*exp(cfun.b*t2x));
            end
        end


    end
    
end

