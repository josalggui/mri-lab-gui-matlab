classdef MRI_cartesian_analysis_JM < MRI_analysis_JM
    % Analysis class defining functions to be used in the sequential sequences using
    % on full Fourier encoded Cartesian acquisitions
    
    % Created on 29.08.2014 Elena Diaz-Caballero
    
    properties
        fig2D;
        Fid2D;
        Fid3D;
        Image2D;
        Image3D;
        firstPlot2D;
        firstPlot3D;
        plotImage2D;
        plotImage3D;
        plotImage2DPhase;
        plotImage2DPhaseUnwrapped;
        plotAbsKspace;
        plotRealKspace;
        plotAbsKspace3D;
        phaseTitle;
    end
    
    methods
        function obj=MRI_cartesian_analysis_JM(seq,gfig)
            obj = obj@MRI_analysis_JM(seq,gfig);
        end
        function Execute(obj)
            Execute@MRI_analysis_JM(obj);
        end
        function Reset(obj)
            Reset@MRI_analysis_JM(obj);
            obj.firstPlot2D = 1;
            obj.firstPlot3D = 1;
        end
        function ResetPlots(obj)
            ResetPlots@MRI_analysis_JM(obj);
            obj.firstPlot2D = 1;
            obj.firstPlot3D = 1;
        end
        function figs = FigureRequest(obj)
            width = 0.4;
            height = 1;
            figs(1,:) = [width height];
        end
        function SetFigureHandle(obj,h)
            obj.fig2D = h(1);
            set(obj.fig2D,'defaultAxesFontUnits','normalized','defaultAxesFontsize', 0.12);
            set(obj.fig2D,'numbertitle','off','name','FFT & k-space');
        end
        
        function SNR_MinAndMaxValues_Module(obj)
            %by Javi: SNR estimation
                    
            global MRIDATA
            seq=obj.seqClass;
            
            % load data
            mkSpace = MRIDATA.Fid2D;
%             % convert data
%             mImage = abs( fft( mkSpace ) ) ;
%             mImageArray = mImage(:);
%             
%             % take SNR measurement
%             thresh = linspace( 0.1, 0.5, 20 );
%             
%             for i = 1:numel(thresh)
%                 SNR(i) = mean( mImageArray( mImageArray>=thresh(i)*max(mImageArray))) / mean( mImageArray( mImageArray< thresh(i)*max(mImageArray)));
%             end % end loop
%             
%             SNRmean = mean(SNR);
            
            SNRmean = mean( abs(mkSpace(:)) ) ./ std( abs(mkSpace(:) ));

            % Display a SNR mean value
            seq.SetParameter('SNRMEANVALUE',SNRmean);
        end
        
        function Plot2D(obj,Kspace)
            % Kspace = obj.Fid2D;
            % Kspace = obj.KGrid; %in case we're calibrating
            
            if (obj.firstPlot2D)
                figure(obj.fig2D);
                obj.firstPlot2D = 0;
                h = subplot(3,1,1);
                I = abs(obj.Image2D);
                % G = fspecial('gaussian',[5 5],2);
                % I = imfilter(I,G,'same');
                obj.plotImage2D = imagesc(I,'Parent',h);
                colorbar('peer',h);
                colormap(h,'gray')
                title(h,'2D FFT - Magnitude');
                h = subplot(3,1,2);
                obj.plotAbsKspace = imagesc(abs(Kspace),'Parent',h);
                colorbar('peer',h);
                colormap(h,'gray')
                title(h,'Abs of k-space');
                h = subplot(3,1,3);
                obj.plotRealKspace = imagesc(angle(Kspace),'Parent',h);
                colorbar('peer',h);
                colormap(h,'gray')
                title(h,'Phase of k-space');
            else
                I = abs(obj.Image2D);
                % G = fspecial('gaussian',[5 5],2);
                % I = imfilter(I,G,'same');
                set(obj.plotImage2D, 'CData', I);
                set(obj.plotAbsKspace, 'CData', abs(Kspace));
                set(obj.plotRealKspace, 'CData', real(Kspace));
            end
            
        end
        function Plot2DPhase(obj)
            if (obj.firstPlot2D)
                figure(obj.fig2D);
                colormap(obj.fig2D,'default');
                obj.firstPlot2D = 0;
                h = subplot(3,1,1);
                obj.plotImage2D = imagesc(abs(obj.Image2D),'Parent',h);
                colorbar('peer',h);
                title(h,'2D FFT - Magnitude');
                h = subplot(3,1,2);
                obj.plotImage2DPhase = imagesc(angle(obj.Image2D),'Parent',h);
                colorbar('peer',h);
                obj.phaseTitle = title(h,sprintf('2D FFT - Phase\t %.2f rad',angle(obj.Image2D(ceil(end/2),ceil(end/2)))));
                h = subplot(3,1,3);
                if size(obj.Image2D,1) > 2
                    im_unwrapped = Unwrap2D(obj.Image2D);
                    im_unwrapped = im_unwrapped - im_unwrapped(ceil(end/2),ceil(end/2));
                    obj.plotImage2DPhaseUnwrapped = imagesc(im_unwrapped,'Parent',h,[-2*pi 2*pi]);
                else
                    obj.plotImage2DPhaseUnwrapped = imagesc(angle(obj.Image2D),'Parent',h);
                end
                colorbar('peer',h);
                title(h,'2D FFT - Phase Unwrapped');
                %                 h = subplot(3,1,3)
                %                 obj.plotAbsKspace = imagesc(abs(obj.Fid2D),'Parent',h);
                %                 colorbar('peer',h);
                %                 title( 'Abs of k-space');
            else
                if size(obj.Image2D,1) > 2
                    im_unwrapped = Unwrap2D(obj.Image2D);
                    im_unwrapped = im_unwrapped - im_unwrapped(ceil(end/2),ceil(end/2));
                    set(obj.plotImage2DPhaseUnwrapped, 'CData', im_unwrapped);
                end
                set(obj.plotImage2D, 'CData', abs(obj.Image2D));
                set(obj.plotImage2DPhase, 'CData', angle(obj.Image2D));
                set(obj.phaseTitle,'String',sprintf('2D FFT - Phase\t %.2f rad',angle(obj.Image2D(ceil(end/2),ceil(end/2)))));
            end
            
        end
        
        function Plot2DS(obj)
            % used when selecting 2D Image in a Slice or MultiSlice
            % sequence
            
            global MRIDATA
            seq = obj.seqClass;
            
            if (obj.firstPlot2D)
                figure(obj.fig2D);
                obj.firstPlot2D = 0;
                
                % Image Rotation
                readoutaxis = linspace(1,numel(obj.Image2D(:,1)),numel(obj.Image2D(:,1)));
                phaseaxis   = linspace(1,numel(obj.Image2D(1,:)),numel(obj.Image2D(1,:)));
                sliceaxis   = 1;
                
                [X, Y, Z] = ndgrid(readoutaxis,phaseaxis,sliceaxis);
                RotatedAxis = quatrotate(MRIDATA.Quaternion, [X(:) Y(:) Z(:)]);
                X = reshape(RotatedAxis(:,1),numel(obj.Image2D(:,1)),numel(obj.Image2D(1,:)),1);
                Y = reshape(RotatedAxis(:,2),numel(obj.Image2D(:,1)),numel(obj.Image2D(1,:)),1);
                
                % Image Plot
                h = subplot(3,1,1);
                I = abs(obj.Image2D);
                G = fspecial('gaussian',[5 5],2);
                I = imfilter(I,G,'same');
                obj.plotImage2D = pcolor(X,Y,I,'Parent',h);
                shading(h, 'interp');
                %                 colorbar('peer',h);
                colormap(h,'gray')
                title(h,'2D FFT - Magnitude');
                h = subplot(3,1,2);
                obj.plotAbsKspace = pcolor(X,Y,abs(obj.Fid2D),'Parent',h);
                shading(h, 'interp');
                %                 colorbar('peer',h);
                colormap(h,'gray')
                title(h,'Abs of k-space');
                h = subplot(3,1,3);
                obj.plotRealKspace = pcolor(X,Y,real(obj.Fid2D),'Parent',h);
                shading(h, 'interp');
                %                 colorbar('peer',h);
                colormap(h,'gray')
                title(h,'Real of k-space');
            else
                I = abs(obj.Image2D);
                G = fspecial('gaussian',[5 5],2);
                I = imfilter(I,G,'same');
                set(obj.plotImage2D, 'CData', I);
                set(obj.plotAbsKspace, 'CData', abs(obj.Fid2D));
                set(obj.plotRealKspace, 'CData', real(obj.Fid2D));
                
            end
            
            temp_file=fullfile(seq.path_file,'SliceData_temp.mat');
            save(temp_file,'MRIDATA');
        end
        
        function Plot2DSlice(obj)
            global MRIDATA
            seq = obj.seqClass;
            
            if (obj.firstPlot3D)
                figure(obj.fig2D)
                obj.firstPlot3D = 0;
                
                % Image Rotation
                readoutaxis = linspace(1,numel(obj.Fid3D(:,1,1)),numel(obj.Fid3D(:,1,1)));
                phaseaxis   = linspace(1,numel(obj.Fid3D(1,:,1)),numel(obj.Fid3D(1,:,1)));
                sliceaxis   = 1;
                
                [X, Y, Z] = ndgrid(readoutaxis,phaseaxis,sliceaxis);
                RotatedAxis = quatrotate(MRIDATA.Quaternion, [X(:) Y(:) Z(:)]);
                X = reshape(RotatedAxis(:,1),numel(obj.Fid3D(:,1,1)),numel(obj.Fid3D(1,:,1)),1);
                Y = reshape(RotatedAxis(:,2),numel(obj.Fid3D(:,1,1)),numel(obj.Fid3D(1,:,1)),1);
                
                nslicetot = numel( obj.Fid3D(1,1,:) );
                SliceL = seq.GetParameter('STARTSLICEFREQ')/1000;
                SliceU = seq.GetParameter('ENDSLICEFREQ')/1000;
                
                if nslicetot == 1
                    slicefreq = SliceL;
                else
                    slicefreq = linspace(SliceL,SliceU,nslicetot);
                end
                
                for islice = 1:nslicetot
                    
                    I = abs(fftshift(ifftn(fftshift( obj.Fid3D(:,:,islice) ))));
                    %                     G = fspecial('gaussian',[5 5],2);
                    %                     I = imfilter(I,G,'same');
                    
                    h = subplot( 2, nslicetot, islice );
                    obj.plotImage3D( islice ) = pcolor(X,Y,I,'Parent',h);
                    %                     colorbar('peer',h);
                    shading(h, 'interp');
                    colormap(h,'gray')
                    title(h,{'2D FFT - Magnitude', strcat('Slice:',num2str(islice),' Freq:',num2str(slicefreq(islice)))});
                    
                    h = subplot( 2, nslicetot, islice + nslicetot );
                    obj.plotAbsKspace3D( islice ) = pcolor(X,Y, abs(obj.Fid3D(:,:,islice) ),'Parent',h);
                    %                     colorbar('peer',h);
                    shading(h, 'interp');
                    colormap(h,'gray')
                    title(h,{'Abs of k-space', strcat('Slice:',num2str(islice))});
                    
                end
            else
                nslicetot = numel( obj.Fid3D(1,1,:) );
                for islice = 1:nslicetot;
                    
                    I = abs(fftshift(ifftn(fftshift( obj.Fid3D(:,:,islice) ))));
                    %                     G = fspecial('gaussian',[5 5],2);
                    %                     I = imfilter(I,G,'same');
                    
                    set( obj.plotImage3D( islice ), 'CData', I);
                    set( obj.plotAbsKspace3D( islice ), 'CData', abs( obj.Fid3D(:,:,islice) ) );
                    
                end
            end
            temp_file=fullfile(seq.path_file,'SliceData_temp.mat');
            save(temp_file,'MRIDATA');
        end
        
        function Plot3D(obj)
%             global MRIDATA
            seq = obj.seqClass;
            if (obj.firstPlot3D)
                figure(obj.fig2D)
                obj.firstPlot3D = 0;
                colormaptemp = colormap('bone');
                colormap(obj.fig2D,flipud(colormaptemp));
                alphamaptemp = alphamap('rampup');
                alphamaptemp = alphamaptemp*0.4;
                alphamaptemp(1:10) = 0;
                alphamap(obj.fig2D, alphamaptemp);
                I = abs(fftshift(ifftn(fftshift( obj.Fid3D ))));
                G = fspecial3('gaussian',[5 5 5]);
                I = imfilter(I,G,'same');
                h = subplot(2,1,1);
                
                if isfield( seq, 'FOV' )             
                    obj.plotImage3D = vol3d( 'CData', I, 'texture', '3D', 'Parent', h, ...
                                         'XData',[-0.5/seq.FOV.RO, 0.5/seq.FOV.RO]*1000,...
                                         'YData',[-0.5/seq.FOV.PH, 0.5/seq.FOV.PH]*1000,...
                                         'ZData',[-0.5/seq.FOV.SL, 0.5/seq.FOV.SL]*1000);
                else
                    obj.plotImage3D = vol3d( 'CData', I, 'texture', '3D', 'Parent', h);
                end
                title(h, '3D FFT');
                h = subplot(2,1,2);
                obj.plotAbsKspace3D = vol3d( 'CData', abs(obj.Fid3D), 'texture', '3D', 'Parent', h);
                title(h, '3D FID');
            else
                I = abs(fftshift(ifftn(fftshift( obj.Fid3D ))));
                G = fspecial3('gaussian',[5 5 5]);
                I = imfilter(I,G,'same');
                obj.plotImage3D.cdata = I;
                obj.plotAbsKspace3D.cdata  = abs(obj.Fid3D);
                obj.plotImage3D = vol3d(obj.plotImage3D);
                obj.plotAbsKspace3D = vol3d(obj.plotAbsKspace3D);
            end
            
%             temp_file=fullfile(seq.path_file,'MRIDATA3Dinfo.mat');
%             save(temp_file,'MRIDATA');
        end
        
    end
    
end

