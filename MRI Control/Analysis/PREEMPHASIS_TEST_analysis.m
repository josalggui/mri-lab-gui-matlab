classdef PREEMPHASIS_TEST_analysis < MRI_cartesian_analysis_JM   
    
    methods
        function obj=PREEMPHASIS_TEST_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'PREEMPHASIS_TEST Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end

        function Execute(obj)            
        end
    end
end