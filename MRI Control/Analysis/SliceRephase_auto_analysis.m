classdef SliceRephase_auto_analysis < SCAN_analysis
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        UsedRepDelay;
        
    end
    
    methods
        function obj=SliceRephase_auto_analysis(seq,gfig,par)
            obj = obj@SCAN_analysis(seq,gfig,par);
            obj.Name = 'Slice Rephase automatic Analysis';
        end
        function Execute(obj)
            Execute@SCAN_analysis(obj);
            if obj.iAcq == length(obj.ScanParValues)
                switch obj.ScanPar
                    case 'SLICEREPHASE'
                        obj.FitPT();
                end
            end
        end
        function FitPT(obj)
            seq = obj.seqClass;
            
            % grab the fata
            xdata = obj.GetXData();
            ydata = obj.GetYData();
            
            % fit the data to an spline
            f = fittype('smoothingspline');
            cfun = fit(xdata',ydata',f);
            xdatafit = linspace(min(xdata),max(xdata),1000);
            ydatafit = cfun(xdatafit);
            RephaseTime = xdatafit( ydatafit == max(ydatafit) );
            
           
            figure(obj.figScan);
            h = gca;
            plot(h,xdata,ydata,'bo');hold(h,'on');
            plot(h,xdatafit,ydatafit,'r');
            plot(h,RephaseTime,max(ydatafit),'r*','MarkerSize',12);hold(h,'off');
            
            title(h,sprintf('Complete Rephase Time: %.2f ',RephaseTime));
            grid(h,'on');
            xlabel('Slice Rephase Ratio');
           
            seq.SetParameter('SLICEREPHASERATIO',RephaseTime);
            
        end
        
    end
end
