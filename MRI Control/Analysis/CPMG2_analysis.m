classdef CPMG2_analysis < MRI_analysis
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        t2_cfun;
        t2_offset;
        t2_gof;
        t2_lineFit1;
        t2_lineFit2;
        t2_xvector;
    end
    
    methods
        function obj=CPMG2_analysis(seq,gfig)
            obj = obj@MRI_analysis(seq,gfig);
            obj.Name = 'CPMG2 Analysis';
        end
        function Execute(obj)
            
            seq = obj.seqClass;
            
            nEchos = 1;
            if seq.GetParameter('INVERSIONON');
                t2str = obj.FitT2();
                nEchos  = seq.GetParameter('NECHOS');
            end
%             Execute@MRI_analysis(obj);
            
            obj.iAcq = obj.iAcq + 1;
            nPoints = seq.GetParameter('NPOINTS');
            
            if seq.phase_en
                nPhases = seq.GetParameter('NPHASES');
            else
                nPhases = 1;
            end
            if seq.slice_en
                nSlices = seq.GetParameter('NSLICES');
            else
                nSlices = 1;
            end
            
            % Should check for change of size here
            obj.fidData = reshape(seq.fidData,nPoints*nEchos,nPhases,nSlices);
            obj.fidData = max(max(obj.fidData(:,:,1),[],3),[],2);
            obj.fftData = flipud(fftshift(fft(obj.fidData(1:nPoints))));
            
            obj.PlotFID();
            obj.PlotFFT();
            
            if obj.ShowFIDCaption && seq.GetParameter('INVERSIONON')
                set(obj.CaptionFID,'string',t2str);
            else
                set(obj.CaptionFID,'string','');
            end
        end
        function t2str = FitT2(obj)
            global MRIDATA
            seq = obj.seqClass;
            nPoints = seq.GetParameter('NPOINTS');
            nEchos = seq.GetParameter('NECHOS');
            tau = seq.GetParameter('TAUDELAY');
            RF = seq.GetParameter('PULSETIME');
            acq_time = seq.GetParameter('NPOINTS')/seq.GetParameter('SPECTRALWIDTH');
            fidData = seq.fidData;
%             nPtBlock = floor(nPoints/nEchos);
            t2data = reshape(fidData,nPoints,nEchos);
            [ t2dataMax, t2maxind ] = max(abs(real(t2data)),[],1);
%             t2x = ((0:length(t2dataMax)-1)+0.5)./length(t2dataMax)*2*(( 2*RF + tau)/MyUnits.ms)*nEchos;
            t2x = ( RF + (tau-acq_time) + t2maxind/nPoints*(acq_time) + (2*RF+2*tau)*(0:length(t2dataMax)-1) ) / MyUnits.ms;
%             f = fittype('a+b*exp(c*x)');
%             [cfun,gof] = fit(t2x',t2dataMax',f);
            t2_offset = t2dataMax(end);%exp(mean(log(t2dataMax)));
            t2dataMax = t2dataMax - t2_offset;
            [ cfun, gof ] = fit(t2x',t2dataMax','exp1');
            ci = confint(cfun);
            t2 = -1.0/cfun.b;
            t2min = -1.0/ci(1,2);
            t2max = -1.0/ci(2,2);
            t2str = sprintf('T2 = %.3f (%.3f to %.3f) ms',t2,t2min,t2max);
            obj.t2_xvector = t2x;
            obj.t2_cfun = cfun;
            obj.t2_gof = gof;
            obj.t2_offset = t2_offset;
            
            MRIDATA.obj = obj;
            MRIDATA.seq = seq;
            
        end
        function PlotFID(obj)
            h = obj.figFID;
 %           set(obj.gui_fig,'CurrentAxes',h)
            seq = obj.seqClass;
            cfun = obj.t2_cfun;
            t2x = obj.t2_xvector;
            if obj.absFID
                fidDataPlot = abs(obj.fidData);
            else
                fidDataPlot = real(obj.fidData);
            end
            
            if (obj.firstPlotFID)
                cla;
                obj.firstPlotFID = 0;
                acq_time = seq.GetParameter('ACQUISITIONTIME');
                xvectorFID = (0:length(fidDataPlot)-1)./length(fidDataPlot)*(acq_time/MyUnits.us);
                fidLine = plot(xvectorFID,fidDataPlot,'Parent',h);
                obj.fidLine = fidLine;
                xlim(h,[min(xvectorFID) max(xvectorFID)]);
                grid(h,'on');
                title(h,'FID')
                xlabel(h,'Time, us');
                
%                 PlotFID@MRI_analysis(obj);
                hold(h,'on');
                if seq.GetParameter('INVERSIONON')
                    obj.t2_lineFit1 = plot(h,((0:length(t2x)-1)+0.5)/length(t2x)*seq.timeVector(end)/MyUnits.us,cfun(t2x)+obj.t2_offset,'Color','g');
%                     obj.t2_lineFit2 = plot(h,((0:length(t2x)-1)+0.5)/length(t2x)*seq.timeVector(end)/MyUnits.us,-cfun(t2x)-obj.t2_offset,'Color','g');
                end
                hold(h,'off');
            else
                set(obj.fidLine,'YData',fidDataPlot);
                if obj.ShowFIDMarkers
                    set(obj.fidLine,'Marker','.');
                else
                    set(obj.fidLine,'Marker','none');
                end
                ylim(h,obj.CalculateYLimit(fidDataPlot,ylim(h)));
%                 PlotFID@MRI_analysis(obj);
                if seq.GetParameter('INVERSIONON')
                    set(obj.t2_lineFit1,'YData',cfun(t2x)+obj.t2_offset);
%                     set(obj.t2_lineFit2,'YData',-cfun(t2x)-obj.t2_offset);
                end
            end
        end


    end
    
end

