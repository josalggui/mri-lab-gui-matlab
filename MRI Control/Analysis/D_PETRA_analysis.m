classdef D_PETRA_analysis < MRI_cartesian_analysis_JM
    %ANALYSIS for Gradient Echo 3D     
    
    methods
        function obj=D_PETRA_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'D-PETRA Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
            global rawData;
            close (1);
            nPoints = rawData.inputs.nPoints;
            fov = rawData.inputs.fov*1d3;
            kMax = rawData.aux.kMax*1d-3;
            bw = rawData.aux.bandwidth;
            tMax =  nPoints/bw/2*1e6;
            axes = rawData.inputs.axes;
            dataS = reshape(rawData.kSpace.interpolatedShort(:,4),nPoints)*1e3;
            dataL = reshape(rawData.kSpace.interpolatedLong(:,4),nPoints)*1e3;
            imagenS = mat2gray(rawData.kSpace.imagenShort);
            imagenL = mat2gray(rawData.kSpace.imagenLong);
            
            figure(1)
            set(gcf, 'Position', [100, 100, 1500, 800])
            
            subplot(2,3,1)
            if(nPoints(3)==1)
                imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],imagenS)
            else
                imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],squeeze(imagenS(:,:,ceil(nPoints(3)/2))));
            end
            title('SE1 image')
            xlabel(strcat(axes(2),' (mm)'))
            ylabel(strcat(axes(1),' (mm)'))
            
            subplot(2,3,2)
            if(nPoints(3)==1)
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],log(abs(dataS)))
            else
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],squeeze(log(abs(dataS(:,:,ceil(nPoints(3)/2))))));
            end
            title('SE1 k-space magnitude')
            xlabel(strcat('k',axes(2),' (mm^{-1})'))
            ylabel(strcat('k',axes(1),' (mm^{-1})'))
            
            subplot(2,3,3)
            if(nPoints(3)==1)
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],angle(dataS))
            else
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],squeeze(angle(dataS(:,:,ceil(nPoints(3)/2)))));
            end
            title('SE1 k-space phase')
            xlabel(strcat('k',axes(2),' (mm^{-1})'))
            ylabel(strcat('k',axes(1),' (mm^{-1})'))
            colormap gray
            
            subplot(2,3,4)
            if(nPoints(3)==1)
                imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],imagenL)
            else
                imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],squeeze(imagenL(:,:,ceil(nPoints(3)/2))));
            end
            title('SE2 image')
            xlabel(strcat(axes(2),' (mm)'))
            ylabel(strcat(axes(1),' (mm)'))
            
            subplot(2,3,5)
            if(nPoints(3)==1)
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],log(abs(dataL)))
            else
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],squeeze(log(abs(dataL(:,:,ceil(nPoints(3)/2))))));
            end
            title('SE2 k-space magnitude')
            xlabel(strcat('k',axes(2),' (mm^{-1})'))
            ylabel(strcat('k',axes(1),' (mm^{-1})'))
            
            subplot(2,3,6)
            if(nPoints(3)==1)
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],angle(dataL))
            else
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],squeeze(angle(dataL(:,:,ceil(nPoints(3)/2)))));
            end
            title('SE2 k-space phase')
            xlabel(strcat('k',axes(2),' (mm^{-1})'))
            ylabel(strcat('k',axes(1),' (mm^{-1})'))
            colormap gray
            
            
            h = obj.figFID;
            t1 = linspace(-tMax(1),tMax(1),nPoints(1));
            t2 = linspace(-tMax(2),tMax(2),nPoints(2));
            plot(t1,abs(dataS(:,ceil(nPoints(2)/2),ceil(nPoints(3)/2))),'b',...
                t2,abs(dataS(ceil(nPoints(1)/2),:,ceil(nPoints(3)/2))),'r',...
                t1,abs(dataL(:,ceil(nPoints(2)/2),ceil(nPoints(3)/2))),'b--',...
                t2,abs(dataL(ceil(nPoints(1)/2),:,ceil(nPoints(3)/2))),'r--',...
                'Parent',h);
            xlabel(h,'t (us)');
            ylabel(h,'signal (mV)')
            title(h,'Signal magnitude')
            pause(0.1)
            
            h = obj.figFFT;
            x1 = linspace(-fov(1)/2,fov(1)/2,nPoints(1));
            x2 = linspace(-fov(2)/2,fov(2)/2,nPoints(2));
            plot(x1,imagenS(:,ceil(nPoints(2)/2),ceil(nPoints(3)/2)),'b',...
                x2,imagenS(ceil(nPoints(1)/2),:,ceil(nPoints(3)/2)),'r',...
                x1,imagenL(:,ceil(nPoints(2)/2),ceil(nPoints(3)/2)),'b--',...
                x2,imagenL(ceil(nPoints(1)/2),:,ceil(nPoints(3)/2)),'r--',...
                'Parent',h);
            xlabel(h,'d (mm)');
            ylabel(h,'pixel magnitude (a.u.)')
            title(h,'Image')
            pause(0.1)
        end
    end
end

