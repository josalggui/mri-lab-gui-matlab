classdef RadialSPRITE_ADC_PP_analysis < MRI_cartesian_analysis
    %ANALYSIS for Gradient Echo 3D     
    
    methods
        function obj=RadialSPRITE_ADC_PP_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis(seq,gfig);
            obj.Name = 'Cenetered SPRITE Analysis';
            obj.PlotSelectorNames = {'2D Image','3D Image'};
            obj.PlotSelector = '2D Image';
        end
        function Execute(obj)
            global MRIDATA;
            
            obj.iAcq = obj.iAcq + 1;
            seq = obj.seqClass;
            if sum( [seq.readout_en, seq.phase_en, seq.slice_en] ) > 0
                
                nPoints = 1;
                if seq.readout_en
                    nReadouts = seq.GetParameter('NREADOUTS');
                else
                    nReadouts = 1;
                end
                if seq.phase_en
                    nPhases = seq.GetParameter('NPHASES');
                else
                    nPhases = 1;
                end
                if seq.slice_en
                    nSlices = seq.GetParameter('NSLICES');
                else
                    nSlices = 1;
                end
            else
                nPoints = seq.GetParameter('NPOINTS');
                nReadouts = 1;
                nPhases = 1;
                nSlices = 1;
            end
            
            % Should check for change of size here
            if sum( [seq.readout_en, seq.phase_en, seq.slice_en] ) > 0
                obj.fidData = reshape(seq.fidData,nReadouts,nPhases,nSlices);
            else
                obj.fidData = seq.fidData(:);
            end
%             obj.fidData = squeeze(reshape(seq.fidData,nPoints,nReadouts,nPhases,nSlices));
            obj.fidData = max(max(obj.fidData(:,:,1),[],3),[],2);
            obj.fftData = flipud(fftshift(fft(obj.fidData)));
            
            obj.PlotFID();
            obj.PlotFFT();
            
%             Execute@MRI_cartesian_analysis(obj);
            seq = obj.seqClass;
            nReadouts = seq.GetParameter('NREADOUTS');
            nPhases = seq.GetParameter('NPHASES');
            nSlices = seq.GetParameter('NSLICES');
            if length(seq.fidData) ==  nReadouts*nPhases*nSlices
                MRIDATA.Fid3D = reshape(seq.fidData, nReadouts, nPhases, nSlices);
                MRIDATA.Data4D(numel(MRIDATA.Data4D)+1).Fid3D = MRIDATA.Fid3D;
                obj.Fid3D = MRIDATA.Fid3D;
                obj.Fid2D = flipud(MRIDATA.Fid3D(:,:,ceil(nSlices/2))');
                MRIDATA.Fid2D = obj.Fid2D;
                padding = [2^nextpow2(size(obj.Fid2D,1))-size(obj.Fid2D,1) 2^nextpow2(size(obj.Fid2D,2))-size(obj.Fid2D,2)];%0.5*[size(obj.Fid2D,1) size(obj.Fid2D,2)]; %
                s = size(obj.Fid2D);
                Gstd = 1;%seq.GetParameter('GAUSSIANFILTER');
                G = customgauss(s,Gstd*s(1),Gstd*s(2),0,0,1,0*s);
                obj.Fid2D = obj.Fid2D.*G;
                
                obj.Image2D = fftshift( ifftn( fftshift( padarray( obj.Fid2D, ceil(padding/2), 'both' ) ) ) );
                switch obj.PlotSelector
                    case '2D Image'
                        Kspace=obj.Fid2D;
                        obj.Plot2D(Kspace);
                    case '3D Image'
                        obj.Plot3D();
                    otherwise
                        warning('Unknown Plot','Unexpected plot type. No plot created.');
                end
                obj.SNR_MinAndMaxValues_Module();
            end
            results_file=fullfile(seq.path_file,'results.mat');
            save (results_file, 'seq', 'obj');
            save('tempMRIDATA.mat','MRIDATA');
        end
        function Plot2D(obj,Kspace) %overload Plot2D function
            % Kspace = obj.Fid2D;
            % Kspace = obj.KGrid; %in case we're calibrating
            
            if (obj.firstPlot2D)
                seq = obj.seqClass;
                
                figure(obj.fig2D);
                obj.firstPlot2D = 0;
                
                h = subplot(3,1,1);
                [m,n] = size(obj.Image2D);
                ROax = linspace(-0.5/seq.FOV.RO, 0.5/seq.FOV.RO, n)*1000;
                PHax = linspace(-0.5/seq.FOV.PH, 0.5/seq.FOV.PH, m)*1000;
                I = abs(obj.Image2D);
                G = fspecial('gaussian',[5 5],2);
                I = imfilter(I,G,'same');
                obj.plotImage2D = imagesc(ROax,PHax,I,'Parent',h);
                colorbar('peer',h);
                colormap(h,'gray')
                title(h,'2D FFT - Magnitude');
                
                h = subplot(3,1,2);
                obj.plotAbsKspace = imagesc(abs(Kspace),'Parent',h);
                colorbar('peer',h);
                colormap(h,'gray')
                title(h,'Abs of k-space');
                
                h = subplot(3,1,3);
                obj.plotRealKspace = imagesc(real(Kspace),'Parent',h);
                colorbar('peer',h);
                colormap(h,'gray')
                title(h,'Real of k-space');
            else
                I = abs(obj.Image2D);
                G = fspecial('gaussian',[5 5],2);
                I = imfilter(I,G,'same');
                set(obj.plotImage2D, 'CData', I);
                set(obj.plotAbsKspace, 'CData', abs(Kspace));
                set(obj.plotRealKspace, 'CData', real(Kspace));
            end
            
        end
        
        function Plot3D(obj)
%             global MRIDATA
            seq = obj.seqClass;
            if (obj.firstPlot3D)
                figure(obj.fig2D)
                obj.firstPlot3D = 0;
                colormaptemp = colormap('bone');
                colormap(obj.fig2D,flipud(colormaptemp));
                alphamaptemp = alphamap('rampup');
                alphamaptemp = alphamaptemp*0.4;
                alphamaptemp(1:10) = 0;
                alphamap(obj.fig2D, alphamaptemp);
                I = abs(fftshift(ifftn(fftshift( obj.Fid3D ))));
                G = fspecial3('gaussian',[5 5 5]);
                I = imfilter(I,G,'same');
                                
                h = subplot(2,1,1);
                obj.plotImage3D = vol3d( 'CData', I, 'texture', '3D', 'Parent', h, ...
                                         'XData',[-0.5/seq.FOV.RO, 0.5/seq.FOV.RO]*1000,...
                                         'YData',[-0.5/seq.FOV.PH, 0.5/seq.FOV.PH]*1000,...
                                         'ZData',[-0.5/seq.FOV.SL, 0.5/seq.FOV.SL]*1000);
                title(h, '3D FFT');
                
                h = subplot(2,1,2);
                obj.plotAbsKspace3D = vol3d( 'CData', abs(obj.Fid3D), 'texture', '3D', 'Parent', h);
                title(h, '3D FID');
            else
                I = abs(fftshift(ifftn(fftshift( obj.Fid3D ))));
                G = fspecial3('gaussian',[5 5 5]);
                I = imfilter(I,G,'same');
                obj.plotImage3D.cdata = I;
                obj.plotAbsKspace3D.cdata  = abs(obj.Fid3D);
                obj.plotImage3D = vol3d(obj.plotImage3D);
                obj.plotAbsKspace3D = vol3d(obj.plotAbsKspace3D);
            end
            
%             temp_file=fullfile(seq.path_file,'MRIDATA3Dinfo.mat');
%             save(temp_file,'MRIDATA');
        end
    end
end

