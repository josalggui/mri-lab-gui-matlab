classdef ARTIFACTTEST_analysis < MRI_cartesian_analysis_JM   
    
    methods
        function obj=ARTIFACTTEST_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'IECO_TEST Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
%            
            global rawData;
%             close(1)
            plot(rawData.outputs.fid(:,1)*1e3,abs(rawData.outputs.fid(:,2)*1e-6),'Parent',obj.figFFT);
            ylim([0,15])
            ylabel('Signal (a.u.)')
            xlabel('Time (ms)')
        end
        
    end
end

