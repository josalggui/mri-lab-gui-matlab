classdef CIST_analysis < SCAN_analysis
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        UsedRepDelay;
        
    end
    
    methods
        function obj=CIST_analysis(seq,gfig,par)
            obj = obj@SCAN_analysis(seq,gfig,par);
            obj.Name = 'CIST Analysis';
        end
        function Execute(obj)
            Execute@SCAN_analysis(obj);
            if obj.iAcq == length(obj.ScanParValues)
                switch obj.ScanPar
                    case 'FREQUENCY'
                        obj.FitPT();
                end
            end
        end
        function FitPT(obj)
%             seq = obj.seqClass;
%             
%             % grab the data
%             xdata = obj.GetXData();
%             ydata = obj.GetYData();
%             
%             % fit the data to an spline
%             f = fittype('smoothingspline');
%             cfun = fit(xdata',ydata',f);
%             xdatafit = linspace(min(xdata),max(xdata),1000);
%             ydatafit = cfun(xdatafit);
%             PulseTime = xdatafit( ydatafit == max(ydatafit) );
%             
%            
%             figure(obj.figScan);
%             h = gca;
%             plot(h,xdata,ydata,'bo');hold(h,'on');
%             plot(h,xdatafit,ydatafit,'r');
%             plot(h,PulseTime,max(ydatafit),'r*','MarkerSize',12);hold(h,'off');
%             
%             title(h,sprintf('90 degree Pulse Time: %.2f us',PulseTime));
%             grid(h,'on');
%             xlabel('Pulse time [us]');
%            
%             seq.SetParameter('PULSETIME',PulseTime*MyUnits.us);
%             
        end
        
        function PlotScan(obj)
            global CONTROL MRIDATA
            MRIDATA.fftDataScan = obj.fftDataScan;
            thresh = 0.25;
%               scanData = sum(abs(obj.fftDataScan(:,CONTROL.skip+1:end)),1).*abs(obj.fidDataScan(1,CONTROL.skip+1:end));
%             FT = abs(obj.fftDataScan(:,CONTROL.skip+1:end));
%             for il = 1:numel(FT(1,:))
%                scanData(il) = sum( FT( FT(:,il) > thresh*max( FT(:,il) ), il) );
%             end
%             scanData = max(FT,[],1);
            scanData = sum(abs(obj.fftDataScan(:,CONTROL.skip+1:end)),1);
            seq = obj.seqClass;
            if obj.firstPlotScan
                figure(obj.figScan);
                obj.axesScan = gca;
                h = obj.axesScan;
                obj.firstPlotScan = 0;
                parUnit = seq.GetParameterUnit(obj.ScanPar);
                xvectorScan = MyUnits.ConverFromStandardUnits(obj.ScanParValues(CONTROL.skip+1:end),parUnit);
                obj.scanLine = plot(xvectorScan,scanData,'o-','Parent',h); %obj.scanLine = plot(xvectorScan,scanData,'Parent',h);
                %                xlim([min(xvectorScan) max(xvectorScan)]);
                grid(h,'on')
                parName = seq.GetParameterName(obj.ScanPar);
                title(h,sprintf('%s Scan',parName));
                if isempty(parUnit)
                    lbl = parName;
                else
                    lbl = sprintf('%s, %s',parName,parUnit);
                end
                xlabel(h,lbl);
            else
                h = obj.axesScan;
                set(obj.scanLine,'YData',scanData,'Parent',h);
%                 set(obj.scanLine,'Marker','-o','Parent',h);

                %                ylim(obj.CalculateYLimit(fidData,ylim));
            end
        end
    end
end
