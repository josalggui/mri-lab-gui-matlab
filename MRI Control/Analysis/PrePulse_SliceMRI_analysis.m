classdef PrePulse_SliceMRI_analysis < MRI_cartesian_analysis
    %ANALYSIS for Gradient Echo 3D     
    
    methods
        function obj=PrePulse_SliceMRI_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis(seq,gfig);
            obj.Name = 'PrePulse Slice Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','2D Slices'};
            obj.PlotSelector = '2D Image';
        end
        function Execute(obj)
            global MRIDATA;
            Execute@MRI_cartesian_analysis(obj);
            seq = obj.seqClass;
            nPoints = seq.GetParameter('NPOINTS');
            nPhases = seq.GetParameter('NPHASES');
            nSlices = seq.GetParameter('NSLICES');
            if length(seq.fidData) ==  nPoints*nPhases*nSlices
                MRIDATA.Fid3D = reshape(seq.fidData, nPoints, nPhases, nSlices);
                MRIDATA.Data3Dnew(numel(MRIDATA.Data3Dnew)+1).Fid3D = MRIDATA.Fid3D;
                obj.Fid3D = MRIDATA.Fid3D;
                obj.Fid2D = flipud(MRIDATA.Fid3D(:,:,ceil(nSlices/2))');
                MRIDATA.Fid2D = obj.Fid2D;
                padding = [2^nextpow2(size(obj.Fid2D,1))-size(obj.Fid2D,1) 2^nextpow2(size(obj.Fid2D,2))-size(obj.Fid2D,2)];%0.5*[size(obj.Fid2D,1) size(obj.Fid2D,2)]; %
                
                obj.Image2D = fftshift( ifftn( fftshift( padarray( obj.Fid2D, ceil(padding/2), 'both' ) ) ) );
                switch obj.PlotSelector
                    case '2D Image'
                        Kspace = obj.Fid2D;
                        obj.Plot2D(Kspace);
                    case '2D Phase'
                        obj.Plot2DPhase();
                    case '2D Slices'
                        obj.Fid3D = permute( obj.Fid3D, [2 1 3]);
                        obj.Plot2DSlice();
                    otherwise
                        warning('Unknown Plot','Unexpected plot type. No plot created.');
                end
                obj.SNR_MinAndMaxValues_Module();
            end
%             results_file=fullfile(seq.path_file,'results.mat');
%             save (results_file, 'seq', 'obj');
            save('tempMRIDATA.mat','MRIDATA');
        end
    end
end

