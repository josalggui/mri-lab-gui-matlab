classdef RING_DOWN_analysis < MRI_cartesian_analysis_JM   
    
    methods
        function obj=RING_DOWN_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'RIGN DOWN Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
            global rawData;
            pulseTime = rawData.inputs.pulseTime*1e6;
            deadTime = rawData.inputs.deadTime*1e6;
            acqTime = rawData.inputs.acquisitionTime*1e6;
            tmin = pulseTime/2+deadTime;
            tmax = tmin+acqTime;
            nPoints = rawData.inputs.nPoints;
            s = rawData.outputs.fidList;
            t = linspace(tmin,tmax,nPoints);
            
            h = obj.figFID;
            plot(t,real(s(:,1)),t,real(s(:,2)),'Parent',h);
            xlabel(h,'t (us)');
            ylabel(h,'signal (V)')
            title(h,'Real part')
            legend('First','Second')
            pause(0.1)
            
            h = obj.figFFT;
            plot(t,imag(s(:,1)),t,imag(s(:,2)),'Parent',h);
            xlabel(h,'t (us)');
            ylabel(h,'signal (V)')
            title(h,'Imaginary part')
            legend('First','Second')
            pause(0.1)
        end
        
    end
end

