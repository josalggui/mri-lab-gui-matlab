classdef SCAN2D_analysis < MRI_analysis
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        figScan;
        axesScan;
        nSamples;
        ScanPars;
        ScanParValues;
        fidDataScan;
        fftDataScan;
        firstPlotScan = 1;
        scanPlot;
    end
    
    methods
        function obj=SCAN2D_analysis(seq,gfig,pars)
            obj = obj@MRI_analysis(seq,gfig);
            obj.Name = 'SCAN2D Analysis';
            obj.ScanPars = pars;
        end
        function Reset(obj)
            Reset@MRI_analysis(obj);
            obj.firstPlotScan = 1;
        end
        function SetFigureHandle(obj,h)
            obj.figScan = h;
        end
        function SetScanParValues(obj,val)
            obj.ScanParValues = val;
            seq = obj.seqClass;
            npt = seq.GetParameter('NPOINTS');
            obj.fidDataScan = zeros(npt);
            obj.fftDataScan = zeros(npt);
        end
        function Execute(obj)
            Execute@MRI_analysis(obj);
            seq = obj.seqClass;
            obj.fidDataScan = seq.fidData;
            obj.fftDataScan = seq.fftData1D;
            obj.PlotScan();
        end
        function cdata = GetCData(obj)
            cdata = get(obj.scanPlot,'CData');
        end
%         function ydata = GetYData(obj)
%             ydata = get(obj.scanLine,'YData');
%         end
        function PlotScan(obj)
%            scanData = sum(abs(obj.fidDataScan));
%            scanData = sum(abs(obj.fftDataScan),1);
            seq = obj.seqClass;
            scanData = obj.fftFWHM / max( norm( obj.fftData(:) ));
            scanData = scanData(1);
            if (obj.firstPlotScan)
                cData = zeros(size(squeeze(obj.ScanParValues(1,:,:))));
                figure(obj.figScan);
                obj.axesScan = gca;
                h = obj.axesScan;
                obj.firstPlotScan = 0;
                gridX = squeeze(obj.ScanParValues(1,:,1));
                gridY = squeeze(obj.ScanParValues(2,1,:));
                cData(1,1) = scanData;
                obj.scanPlot = imagesc(gridX,gridY,cData','Parent',h);
                %                xlim([min(xvectorScan) max(xvectorScan)]);
                grid(h,'on');
%                colormap('gray');
                colorbar('peer',h);
                set(get(obj.scanPlot,'Parent'),'YDir','normal');
                xlabel(h,seq.GetParameterName(obj.ScanPars{1}));
                ylabel(h,seq.GetParameterName(obj.ScanPars{2}));
                xlim(h,'auto');
                ylim(h,'auto');

            else
                h = obj.axesScan;
                cData = get(obj.scanPlot,'CData')';
                i2=floor((obj.iAcq-1)/size(obj.ScanParValues,2))+1;
                i1=mod(obj.iAcq-1,size(obj.ScanParValues,2))+1;
                cData(i1,i2) = scanData;
                set(obj.scanPlot,'CData',cData','Parent',h);
                set(obj.figScan,'Name',sprintf('Minimum FWHM = %.2f kHz',min(min(cData(cData>0)))));
                %                ylim(obj.CalculateYLimit(fidData,ylim));
            end
        end
    end
    
end

