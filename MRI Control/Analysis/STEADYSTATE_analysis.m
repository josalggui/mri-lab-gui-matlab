classdef STEADYSTATE_analysis < MRI_cartesian_analysis_JM
    
    methods
        function obj=STEADYSTATE_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'STEADY STATE Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        
        function Execute(obj)
            global rawData;
            close (1)
            
            figure(1)
            set(gcf,'Position',[50 600 1700 400])
            hold on
            subplot(1,3,1)
            title('Evolution to steady state')
            imagesc(abs(rawData.aux.data))
            colorbar
            xlabel('Number repetitions')
            ylabel('Numbers of points sampled by line')
            
            subplot(1,3,2)
            title('Sampled FID�s')
            xlabel('Adquisition time (us)')
            ylabel('Measured signal')            
            hold on
            time = linspace(0,rawData.aux.acquisitionTime,rawData.inputs.nPoints);
            t1 = time(1);
            t2 = time(2);
            t3 = time(3);
            
            plot(time,abs(rawData.aux.data(:,1)),'k.')
            plot(time,abs(rawData.aux.data(:,2)),'k.')
            plot(time,abs(rawData.aux.data(:,3)),'b.')
            plot(time,abs(rawData.aux.data(:,4)),'g.')
            plot(time,abs(rawData.aux.data(:,5)),'r.')
%             plot(time,abs(rawData.aux.data(:,6)),'m.')
%             plot(time,abs(DataBatch(:,7)),'c.')
%             plot(time,abs(DataBatch(:,8)),'y')
%             plot(time,abs(DataBatch(:,9)),'k')
%             plot(time,abs(DataBatch(:,10)),'b')
%             plot(time,abs(DataBatch(:,11)),'g')
%             plot(time,abs(DataBatch(:,12)),'r')
%             plot(time,abs(DataBatch(:,13)),'m')
%             plot(time,abs(DataBatch(:,14)),'c')
            legend('n=1','n=2','n=3','n=4','n=5','n=6')
            
            n = 1: rawData.inputs.nRepetitions;
            subplot(1,3,3)
            hold on
            
            scatter(n,abs(rawData.aux.data(1,:)),'k','filled')
            scatter(n,abs(rawData.aux.data(2,:)),'r')
            scatter(n,abs(rawData.aux.data(3,:)),'b','filled')
            axis([0 30 1e7 9e7])
            xlabel('Number of repetitions')
            ylabel('Measured signal')            
            legend('t1','t2','t3')

%             plot(n,abs(DataBatch(5,:)),'g.')
%             plot(n,abs(DataBatch(6,:)),'c.')
%             plot(n,abs(DataBatch(13,:)),'r.')
%             plot(n,abs(DataBatch(15,:)),'k.')

             %% FID exponential fitting
%             data = transpose(abs(rawData.aux.data(7,:)));
%             n = transpose (n);
%             
%             f = fit(n,data,'power2');   
%             hold on
%             plot(f,n,data)
%             
%             optimumnPPL =0.05*b(1)/b(2);
%             disp(['The nPPL which stady state is at 95% is ', num2str(optimumnPPL)])
   
        end
    end
end