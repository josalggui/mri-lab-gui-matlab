classdef GE2D_Units_analysis < MRI_cartesian_Units_analysis
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
       
    methods
        function obj=GE2D_Units_analysis(seq,gfig)
            obj = obj@MRI_cartesian_Units_analysis(seq,gfig);
            obj.Name = 'GE2D Real Units Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Image';
        end
        function Execute(obj)
            global rawData;
            
            if(sum(rawData.inputs.axisEnable)<=1)
                figure(7)
                set(gcf, 'Position', [100, 500, 1500, 400])
                n = rawData.inputs.matrix;
                t0 = 0;
                tmax = t0+rawData.inputs.acqTime*1e6;
                tvector = linspace(t0,tmax,n(1));
                fmax = 0.5/(tvector(2)-tvector(1));
                fvector = linspace(-fmax,fmax,n(1));
                imagen = rawData.outputs.imagen;
                kSpace = mean(rawData.outputs.kSpace(:,4:end),2);
                nMisses = rawData.inputs.jumpingFactor;
                
                if(nMisses==0)
                    subplot(1,2,1)
                    plot(tvector,abs(kSpace));
                    title('FID')
                    xlabel('Time (us)')
                    ylabel('Signal (a.u.)')
                    
                    subplot(1,2,2)
                    plot(fvector*1e3,imagen)
                    title('FFT')
                    xlabel('Frequency (Hz)')
                    ylabel('Signal (a.u.)')
                else
                    missedLines = rawData.outputs.missed;
                    subplot(1,3,1)
                    plot(tvector,abs(kSpace),tvector,real(kSpace),tvector,imag(kSpace));
                    title('FID')
                    xlabel('Time (us)')
                    ylabel('Signal (a.u.)')
                    legend('abs','real','imag')
                    
                    subplot(1,3,2)
                    plot(fvector*1e3,imagen)
                    title('FFT')
                    xlabel('Frequency (Hz)')
                    ylabel('Signal (a.u.)')
                    
                    subplot(1,3,3)
%                     plot(unwrap(angle(missedLines))*180/pi,'b')
%                     hold on
                    plot(abs(missedLines),'b')
                    hold on
                    plot(real(missedLines),'g')
                    plot(imag(missedLines),'r')
                    hold off
                    legend('abs','real','imag')
                    title('Hidden Lines')
                    xlabel('Point')
                    ylabel('Signal (a.u.)')
                end
            else
            
                n = rawData.inputs.nPoints;
                
                kSpace = squeeze(reshape(mean(rawData.outputs.kSpace(:,4:end),2),n));
                
                imagen = abs(ifftshift(ifftn(kSpace)));
                FOV = rawData.inputs.fov*1e3;
                
                figure(1)
                set(gcf, 'Position', [100, 500, 1000, 400])
                subplot(1,2,1)
                imagesc([-FOV(2)/2,FOV(2)/2],[-FOV(1)/2,FOV(1)/2],imagen);
                xlabel('Phase (mm)')
                ylabel('Readout (mm)')
                colormap gray
                subplot(1,2,2)
                imagesc(log10(abs(kSpace)));
                xlabel('kp (mm^{-1})')
                ylabel('kr (mm^{-1})')
                colormap gray
            end
            
%             nPoints = rawData.inputs.nPoints;
%             fov = rawData.inputs.fov*1d3;
%             kMax = max(max(max(rawData.outputs.kSpace)))*1d-3;
%             tMax = rawData.inputs.acqTime*1d6;
%             axes = rawData.inputs.axis;
%             imagen = rawData.outputs.imagen;
%             
%             figure(1)
%             set(gcf, 'Position', [100, 500, 1500, 400])
%             
%             subplot(1,3,1)
%             if(nPoints(3)==1)
%                 imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],imagen)
%             else
%                 imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],squeeze(imagen(:,:,ceil(nPoints(3)/2))));
%             end
%             title('image')
%             xlabel(strcat(axes(2),' (mm)'))
%             ylabel(strcat(axes(1),' (mm)'))
%             
%             subplot(1,3,2)
%             if(nPoints(3)==1)
%                 imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],log(abs(data)))
%             else
%                 imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],squeeze(log(abs(data(:,:,ceil(nPoints(3)/2))))));
%             end
%             title('k-space magnitude')
%             xlabel(strcat('k',axes(2),' (mm^{-1})'))
%             ylabel(strcat('k',axes(1),' (mm^{-1})'))
%             
%             subplot(1,3,3)
%             if(nPoints(3)==1)
%                 imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],angle(data))
%             else
%                 imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],squeeze(angle(data(:,:,ceil(nPoints(3)/2)))));
%             end
%             title('k-space phase')
%             xlabel(strcat('k',axes(2),' (mm^{-1})'))
%             ylabel(strcat('k',axes(1),' (mm^{-1})'))
%             colormap gray
%             
%             
%             h = obj.figFID;
%             t1 = linspace(-tMax(1),tMax(1),nPoints(1));
%             t2 = linspace(-tMax(1),tMax(1),nPoints(2));
%             plot(t1,abs(data(:,ceil(nPoints(2)/2),ceil(nPoints(3)/2))),...
%                 t2,abs(data(ceil(nPoints(1)/2),:,ceil(nPoints(3)/2))),...
%                 'Parent',h);
%             xlabel(h,'t (us)');
%             ylabel(h,'signal (a.u.)')
%             title(h,'Signal magnitude')
%             pause(0.1)
%             
%             h = obj.figFFT;
%             x1 = linspace(-fov(1)/2,fov(1)/2,nPoints(1));
%             x2 = linspace(-fov(2)/2,fov(2)/2,nPoints(2));
%             plot(x1,imagen(:,ceil(nPoints(2)/2),ceil(nPoints(3)/2)),...
%                 x2,imagen(ceil(nPoints(1)/2),:,ceil(nPoints(3)/2)),...
%                 'Parent',h);
%             xlabel(h,'d (mm)');
%             ylabel(h,'pixel magnitude (a.u.)')
%             title(h,'Image')
%             pause(0.1)
        end
    end
end

