classdef SPINLOCKING_SWEEP_analysis < MRI_cartesian_analysis_JM   
    
    methods
        function obj=SPINLOCKING_SWEEP_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'SPINLOCKING SWEEP Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
            global rawData;

%               Spin locking temporal evolution for different B1
%                 ninitial=18; %value of FID you want to represent in maps. If equals 1, possibly affected by ring down!
%                 B1ikHz=5;   B1fkHz=5;    B1series=linspace(B1ikHz,B1fkHz,length(rawData.inputs.B1List));
%                 figure('units','normalized','outerposition',[0 0 1 1])
%                 subplot(3,2,1)
%                 imagesc([(rawData.inputs.timeSL+rawData.inputs.deadTime)*1e3],[B1series],abs(rawData.outputs.fidMap(:,:,ninitial)))
%                 colorbar
%                 title('Abs spin locking evolution')
%                 xlabel('Phase locking duration(ms)');
%                 ylabel('B1 (kHz)')
%                 
%                 subplot(3,2,2)
%                 hold on
%                 for i=1:length(rawData.inputs.B1List)
% %                     if mod(rawData.inputs.B1List(i),0.01)==0
%                         plot([(rawData.inputs.timeSL+rawData.inputs.deadTime)*1e3],abs(rawData.outputs.fidMap(i,:,ninitial)))
% %                     end
%                 end
%                 title('Abs spin locking evolution')
%                 legend('')
%                 xlabel('Phase locking duration(ms)');
%                 ylabel('Signal MRN (u.a.)')
%                 
%                 subplot(3,2,3)
%                 imagesc([(rawData.inputs.timeSL+rawData.inputs.deadTime)*1e3],[B1series],real(rawData.outputs.fidMap(:,:,ninitial)))
%                 colorbar
%                 title('Real spin locking evolution')
%                 xlabel('Phase locking duration(ms)');
%                 ylabel('B1 (kHz)')
%                 
%                 subplot(3,2,4)
%                 hold on
%                 for i=1:length(rawData.inputs.B1List)
% %                     if mod(rawData.inputs.B1List(i),rawData.inputs.B1List(i))==0
%                         plot([(rawData.inputs.timeSL+rawData.inputs.deadTime)*1e3],real(rawData.outputs.fidMap(i,:,ninitial)))
% %                     end
%                 end
%                 title('Real spin locking evolution')
%                 legend('')
%                 xlabel('Phase locking duration(ms)');
%                 ylabel('Signal MRN (u.a.)')
% 
%                 
%                 subplot(3,2,5)
%                 imagesc([(rawData.inputs.timeSL+rawData.inputs.deadTime)*1e3],[B1series],imag(rawData.outputs.fidMap(:,:,ninitial)))
%                 colorbar
%                 title('Imag spin locking evolution')
%                 xlabel('Phase locking duration(ms)');
%                 ylabel('B1 (kHz)')
%                 
%                 subplot(3,2,6)
%                 hold on
%                 for i=1:length(rawData.inputs.B1List)
% %                     if mod(rawData.inputs.B1List(i),0.01)==0
%                         plot([(rawData.inputs.timeSL+rawData.inputs.deadTime)*1e3],imag(rawData.outputs.fidMap(i,:,ninitial)))
% %                     end
%                 end
%                 title('Imag spin locking evolution')
%                 legend('')
%                 xlabel('Phase locking duration(ms)');
%                 ylabel('Signal MRN (u.a.)')






                %If you sweep the B1 having Gradient fixed
%                 figure('units','normalized','outerposition',[0 0 1 1])
%                 subplot(2,2,1)
%                 surf(rawData.inputs.B1List(1,:),rawData.inputs.position(1,:)*1e3,rawData.outputs.profilesART)
%                 colorbar
%                 title('Profiles ART')
%                 xlabel('B1 (adim)');
%                 ylabel('Position (mm)')
%                 
%                 subplot(2,2,3)
%                 imagesc(rawData.inputs.B1List(1,:),rawData.inputs.position(1,:)*1e3,rawData.outputs.profilesART)
%                 colorbar
%                 title('Profiles ART')
%                 xlabel('B1 (adim)');
%                 ylabel('Position (mm)')
%                 
%                 subplot(2,2,2)
%                 surf(rawData.inputs.B1List(1,:),rawData.inputs.position(1,:)*1e3,rawData.outputs.profilesFFT)
%                 colorbar
%                 title('Profiles FFT')
%                 xlabel('B1 (adim)');
%                 ylabel('Position (mm)')
%                 
%                 subplot(2,2,4)
%                 imagesc(rawData.inputs.B1List(1,:),rawData.inputs.position(1,:)*1e3,rawData.outputs.profilesFFT)
%                 colorbar
%                 title('Profiles FFT')
%                 xlabel('B1 (adim)');
%                 ylabel('Position (mm)')







                %If you sweep the Gradient having B1 fixed
                %This block contains an error in surf plot because each
                %profile has his corresponding position vector due to field
                %of view depends for each of they when Gradient changes.
                
%                 figure('units','normalized','outerposition',[0 0 1 1])
%                 subplot(2,2,1)
%                 surf([rawData.inputs.GradList*1e3],rawData.inputs.position(:,1)*1e3,rawData.outputs.profilesART)
%                 colorbar
%                 title('Profiles ART')
%                 xlabel('Gradient (mT/m)');
%                 ylabel('Position (mm)')
%                 
%                 subplot(2,2,3)
%                 imagesc([rawData.inputs.GradList*1e3],rawData.inputs.position(:,1)*1e3,rawData.outputs.profilesART)
%                 colorbar
%                 title('Profiles ART')
%                 xlabel('Gradient (mT/m)');
%                 ylabel('Position (mm)')
%                 
%                 subplot(2,2,2)
%                 surf(rawData.inputs.GradList*1e3,rawData.inputs.position*1e3,rawData.outputs.profilesFFT)
%                 colorbar
%                 title('Profiles FFT')
%                 xlabel('Gradient (mT/m)');
%                 ylabel('Position (mm)')
%                 
%                 subplot(2,2,4)
%                 H=pcolor(rawData.inputs.GradList*1e3,rawData.inputs.position*1e3,rawData.outputs.profilesFFT);
%                 set(H,'edgecolor','none')
%                 colorbar
%                 title('Profiles FFT')
%                 xlabel('Gradient (mT/m)');
%                 ylabel('Position (mm)')
        end   
    end
end