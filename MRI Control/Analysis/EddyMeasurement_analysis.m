classdef EddyMeasurement_analysis < MRI_analysis
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        fig2D;
        Fid2D;
        EddyMeasurement;
        Image2D;
        Image3D;
        firstPlot2D;
        firstPlot3D;
        plotImage2D;
        plotImage3D;
        plotImage2DPhase;
        plotImage2DPhaseUnwrapped;
        plotAbsKspace;
        plotRealKspace;
        plotAbsKspace3D;
        phaseTitle;
        PlotFirst;
        PlotSecond;
        PlotComb;
        xvector;
        PlotDiff;
    end
    
    methods
        function obj=EddyMeasurement_analysis(seq,gfig)
            obj = obj@MRI_analysis(seq,gfig);
            obj.Name = 'Eddy Measurement Analysis';
            obj.PlotSelectorNames = {'Presentation 1','Presentation 2'};
            obj.PlotSelector = 'Presentation 1';
        end
        function Execute(obj)
            global MRIDATA;
            Execute@MRI_analysis(obj);
            seq = obj.seqClass;
            nPoints = seq.GetParameter('NPOINTS');
            acq_time = seq.GetParameter('ACQUISITIONTIME');
            obj.xvector = (0:nPoints-1)./nPoints*(acq_time/MyUnits.us);
            if length(seq.fidData) ==  nPoints*3
                MRIDATA.EddyMeasurement = reshape(seq.fidData, nPoints, 3);
                obj.EddyMeasurement = MRIDATA.EddyMeasurement;
                
                switch obj.PlotSelector
                    case 'Presentation 1'
                        obj.Presentation1();
                    case 'Presentation 2'
                        obj.Presentation2();
                    
                    otherwise
                        warning('Unknown Plot','Unexpected plot type. No plot created.');
                end
            end
            results_file=fullfile(seq.path_file,'results.mat');
            save (results_file, 'seq', 'obj');
        end
        function Reset(obj)
            Reset@MRI_analysis(obj);
            obj.firstPlot2D = 1;
            obj.firstPlot3D = 1;
        end
        function ResetPlots(obj)
            ResetPlots@MRI_analysis(obj);
            obj.firstPlot2D = 1;
            obj.firstPlot3D = 1;
        end
        function figs = FigureRequest(obj)
            width = 0.4;
            height = 1;
            figs(1,:) = [width height];
        end
        function SetFigureHandle(obj,h)
            obj.fig2D = h(1);
        end
        function Presentation1(obj)
            if (obj.firstPlot2D)
                figure(obj.fig2D);
                obj.firstPlot2D = 0; 
                First = unwrap(angle(obj.EddyMeasurement(:,2)));
                Second = unwrap(angle(obj.EddyMeasurement(:,3)));
                Comb = 0.5.*(Second-First);
%                 DiffC = diff(Comb);
                
                h = subplot(2,1,1);
                hold(h,'on');
                obj.PlotFirst = plot(obj.xvector,First,'b');
                obj.PlotSecond = plot(obj.xvector,Second,'r');
                hold(h,'off');
                title(h,'Phase of both the Positive and Negative Gradients');
                xlabel(h,'Time [us]');
                ylabel(h,'Phase');
                
                h = subplot(2,1,2);
                obj.PlotComb = plot(Comb,'b');
                title(h,'Phase difference');
                xlabel(h,'Time [us]');
%                 
%                 h = subplot(3,1,3);
%                 obj.PlotDiff = plot(DiffC);
                
               
            else
                First = unwrap(angle(obj.EddyMeasurement(:,2)));
                Second = unwrap(angle(obj.EddyMeasurement(:,3)));
                Comb = 0.5.*(Second-First);
%                 DiffC = diff(Comb);
                
                set(obj.PlotFirst, 'YData', First);
                set(obj.PlotSecond, 'YData', Second);
                set(obj.PlotComb, 'YData', Comb);
%                 set(obj.PlotDiff, 'YData', DiffC);
                
            end
        end
        
        function Presentation2(obj)
                            
                % Normalize signals based on non-gradient signal
                G_pos = (obj.EddyMeasurement(:,2)./abs(obj.EddyMeasurement(:,1)))';
                G_neg = (obj.EddyMeasurement(:,3)./abs(obj.EddyMeasurement(:,1)))';
                
                Xvec = obj.xvector;
                nf = floor(numel(Xvec)/2);
                normalfs = @(a,b,nf,x)(0.5*a(1)+sum(a(2:end).*cos((1:nf)*x)+b.*sin((1:nf)*x))); 
                qfs = @(ai,bi,ar,br,nf,x)(1/(1+((normalfs(ai,bi,nf,x)/normalfs(ar,br,nf,x))^2)));
                derivativefs = @(a,b,nf,x)(sum((1:nf).*b.*cos((1:nf)*x)-(1:nf).*a(2:end).*sin((1:nf)*x)));
                dPhiD = @(ai,bi,ar,br,nf,x)(qfs(ai,bi,ar,br,nf,x)*(1/(normalfs(ar,br,nf,x)^2))*(normalfs(ai,bi,nf,x)*derivativefs(ar,br,nf,x)-normalfs(ar,br,nf,x)*derivativefs(ai,bi,nf,x)));
                
                % Fit real and imaginary components to Fourier Series
                [G_pos_rA G_pos_rB] = Fseries(obj.xvector,real(G_pos),nf);
                [G_pos_iA G_pos_iB] = Fseries(obj.xvector,imag(G_pos),nf);
                
                [G_neg_rA G_neg_rB] = Fseries(obj.xvector,real(G_neg),nf);
                [G_neg_iA G_neg_iB] = Fseries(obj.xvector,imag(G_neg),nf);
                
                Phi1 = zeros(size(Xvec));
                Phi2 = Phi1;
                dPhi = Phi1;
                
                for iPhi = 1:numel(Xvec)
                    
                    Phi1(iPhi) = (angle(complex(normalfs(G_pos_rA',G_pos_rB',nf,Xvec(iPhi)),normalfs(G_pos_iA',G_pos_iB',nf,Xvec(iPhi)))));
                    Phi2(iPhi) = (angle(complex(normalfs(G_neg_rA',G_neg_rB',nf,Xvec(iPhi)),normalfs(G_neg_iA',G_neg_iB',nf,Xvec(iPhi)))));
                    dPhi(iPhi) = dPhiD(G_pos_iA',G_pos_iB',G_pos_rA',G_pos_rB',nf,Xvec(iPhi)) - dPhiD(G_neg_iA',G_neg_iB',G_neg_rA',G_neg_rB',nf,Xvec(iPhi));
                          
                end
                
                Phi1 = unwrap(Phi1);
                Phi2 = unwrap(Phi2);
                
                
            if (obj.firstPlot2D)
                figure(obj.fig2D);
                obj.firstPlot2D = 0; 
                
                h = subplot(3,1,1);
                hold(h,'on');
                obj.PlotFirst = plot(obj.xvector,Phi1,'b');
                obj.PlotSecond = plot(obj.xvector,Phi2,'r');
                hold(h,'off');
                title(h,'Phase of both the Positive and Negative Gradients (fitted)');
                xlabel(h,'Time [us]');
                ylabel(h,'Phase');

                h = subplot(3,1,2);
                obj.PlotComb = plot(obj.xvector,Phi1-Phi2,'b');
                title(h,'Difference between phases [rads]');
                xlabel(h,'Time [us]');
                
                h = subplot(3,1,3);
                obj.PlotDiff = plot(obj.xvector,dPhi,'b');
                title(h,'Derivative of Phase Difference [rads/us]');
                xlabel(h,'Time [us]');


            else
                
                set(obj.PlotFirst, 'YData', Phi1);
                set(obj.PlotSecond, 'YData', Phi2);
                set(obj.PlotComb, 'YData', Phi1-Phi2);
                set(obj.PlotDiff, 'YData', dPhi);

            end
            
            
        end
        
    end
end

