classdef MRI_analysis < handle
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Name='MRI Analysis';
        gui_fig; %gui figure handle
        seqClass; %main sequence class
        figFID; %handle of FID figure
        figFFT; %handle of FFT figure
        CaptionFID; %Caption of FID figure
        CaptionFFT; %Caption of FFT figure
        ShowFIDMarkers; %show markers on the plot
        ShowFIDCaption; %show caption on the plot
        ShowFFTMarkers; %show markers on the plot
        ShowFFTCaption; %show caption on the plot
        absFID=0;
        absFFT=1;
        firstPlotFID=1;
        firstPlotFFT=1;
        fidLine;
        fftLine;
        fftFWHM;
        iAcq = 0; %sample number
        fidData;
        fftData;
        PlotSelector;
        PlotSelectorNames;
    end
    methods(Static)
        function y = CalculateYLimit(data,y)
            mindata = min(min(data));
            maxdata = max(max(data));
            if mindata < y(1)
                y(1) = mindata - 0.2*(maxdata - mindata);
            elseif (mindata - y(1)) > 1*(maxdata - mindata)
                y(1) = mindata - 0.2*(maxdata - mindata);
            end
            if maxdata > y(2)
                y(2) = maxdata + 0.2*(maxdata - mindata);
            elseif (y(2) - maxdata) > 1*(maxdata - mindata)
                y(2) = maxdata - 0.2*(maxdata - mindata);
            end
        end
    end
    methods
        function obj=MRI_analysis(seq,gfig)
            obj.seqClass = seq;
            obj.gui_fig = gfig;
        end
        function Execute(obj)
            obj.iAcq = obj.iAcq + 1;
            seq = obj.seqClass;
            if seq.readout_en && isfield(seq,'NREADOUTS')
                nPoints = seq.GetParameter('NREADOUTS');
            else
                nPoints = seq.GetParameter('NPOINTS');
            end
            if seq.phase_en
                nPhases = seq.GetParameter('NPHASES');
            else
                nPhases = 1;
            end
            if seq.slice_en
                nSlices = seq.GetParameter('NSLICES');
            else
                nSlices = 1;
            end
            
%             if strcmp(seq.SequenceName,'PET experiment')
%                 disp('No MRI analysis performed');
%                 return;
%             end
            
            % Should check for change of size here
            if ~strcmp(seq.SequenceName,'Gradient Echo 3D Cal') && ~strcmp(seq.SequenceName,'CPMG2') && ~strcmp(seq.SequenceName,'CS SPRITE w ADC')
                obj.fidData = reshape(seq.fidData,nPoints,nPhases,nSlices);
                obj.fidData = max(max(obj.fidData(:,:,1),[],3),[],2);
                obj.fftData = flipud(fftshift(fft(obj.fidData)));
            else
                obj.fidData = seq.fidData;
                obj.fftData = flipud(fftshift(fft(obj.fidData)));
            end
            obj.PlotFID();
            obj.PlotFFT();
        end
        function Reset(obj)
            obj.ResetPlots();
            obj.iAcq = 0;
        end
        function ResetPlots(obj)
            obj.firstPlotFID = 1;
            obj.firstPlotFFT = 1;
            obj.fidLine = 0;
            obj.fftLine = 0;
        end
        function figs = FigureRequest(obj)
            figs = [];
        end
        function plots = GetPlotsList(obj)
            plots = obj.PlotSelectorNames;
        end
        function SelectPlot(obj,plot)
            obj.PlotSelector = plot;
        end
        function deltaFreq = GetCentralFrequencyAdjustment(obj)
            seq = obj.seqClass;
            xvectorFFT = seq.freqVector;
            idx = find(obj.fftData==max(obj.fftData),1,'first');
            if seq.ExistsInputParameter('FREQUENCYSHIFT')
                readoutFreqShift = seq.GetParameter('FREQUENCYSHIFT');
            else
                readoutFreqShift = 0;
            end
            deltaFreq = xvectorFFT(idx)+readoutFreqShift;
        end
        function [fidLine, xvectorFID] = PlotFID(obj)
            h = obj.figFID;
 %           set(obj.gui_fig,'CurrentAxes',h)
            seq = obj.seqClass;
            if seq.GetParameter('NREADOUTS') > 1
                nPhases = seq.GetParameter('NPHASES');
                nPoints = seq.GetParameter('NREADOUTS');
            elseif isfield(seq,'NREADOUTS')
                nPoints = seq.GetParameter('NPOINTS');
                nPhases = 1;
            else
                nPhases = seq.GetParameter('NPHASES');
                nPoints = seq.GetParameter('NPOINTS');
            end            
            if length(seq.fidData) ~= nPoints*nPhases
                return;
            end
            fidData = reshape(seq.fidData,nPoints,nPhases);
            fidData = max(fidData',[],1);
            if obj.absFID
                fidDataPlot = abs(obj.fidData);
                
            else
                fidDataPlot = [real(obj.fidData).';imag(obj.fidData).';abs(obj.fidData).'];
            end
            if (obj.firstPlotFID)
                cla;
                obj.firstPlotFID = 0;
                acq_time = seq.GetParameter('ACQUISITIONTIME');
                if obj.absFID
                    fidDataPlot = abs(obj.fidData);
                    xvectorFID = (0:length(fidDataPlot)-1)./length(fidDataPlot)*(acq_time/MyUnits.us);
                    fidLine = plot(xvectorFID,fidDataPlot,'Parent',h);                    
                else                   
                    fidDataPlot = [real(obj.fidData).';imag(obj.fidData).';abs(obj.fidData).'];
                    xvectorFID = (0:length(fidDataPlot(1,:))-1)./length(fidDataPlot(1,:))*(acq_time/MyUnits.us);
                    fidLine = plot(xvectorFID,real(obj.fidData),xvectorFID,imag(obj.fidData),xvectorFID,abs(obj.fidData),'Parent',h);
                    legend(h,'Real', 'Imag','Abs')    
                end
                obj.fidLine = fidLine;
                xlim(h,[min(xvectorFID) max(xvectorFID)]);
                grid(h,'on');
                title(h,'FID')
                xlabel(h,'Time, us');
            else
                %                set(obj.fidLine,'YData',fidData,'Parent',h);
                if obj.absFID
                    set(obj.fidLine,'YData',fidDataPlot);
                else
                    set(obj.fidLine(1),'YData',fidDataPlot(1,:));
                    set(obj.fidLine(2),'YData',fidDataPlot(2,:));
                    set(obj.fidLine(3),'YData',fidDataPlot(3,:));
                end
                if obj.ShowFIDMarkers
                    set(obj.fidLine,'Marker','.');
                else
                    set(obj.fidLine,'Marker','none');
                end
                ylim(h,obj.CalculateYLimit(fidDataPlot,ylim(h)));
            end
        end
        function [fftLine, xvectorFFT] = PlotFFT(obj)
            global MRIDATA
%            figure(obj.gui_fig);
            h = obj.figFFT;
%            set(obj.gui_fig,'CurrentAxes',h)
            seq = obj.seqClass;
            %fftData = reshape(seq.fftData1D,floor(length(seq.fftData1D)/nPhases),nPhases);
            %fftData = max(fftData',[],1);
            acq_time = seq.GetParameter('ACQUISITIONTIME');
            if obj.absFFT
                fftDataPlot = abs(obj.fftData);
            else
                fftDataPlot = real(obj.fftData);
            end
            xvectorFFT = [ceil(-length(fftDataPlot)/2):ceil(length(fftDataPlot)/2)-1]/(acq_time/MyUnits.ms);
            if (obj.firstPlotFFT)
                obj.firstPlotFFT = 0;
                cla;               
                fftLine = plot(xvectorFFT,fftDataPlot,'Parent',h);
                obj.fftLine = fftLine;
                xlim([min(xvectorFFT) max(xvectorFFT)]);
                grid(h,'on');
                title(h,'FFT');
                xlabel(h,'Frequency, kHz');
            else
                set(obj.fftLine,'YData',fftDataPlot,'Parent',h);
                if obj.ShowFFTMarkers
                    set(obj.fftLine,'Marker','.');
                else
                    set(obj.fftLine,'Marker','none');
                end
                ylim(h,obj.CalculateYLimit(fftDataPlot,ylim(h)));
            end
            fVector = abs(obj.fftData);
            [fMax,ind] = max(fVector);
            f0 = xvectorFFT(ind);
            fwhm = xvectorFFT(find(fVector>=fMax/2,1,'last')) - xvectorFFT(find(fVector>=fMax/2,1,'first'));
            obj.fftFWHM = fwhm;
            if obj.ShowFFTCaption
                capstring = sprintf('f0 = %.2f kHz, FWHM = %.2f kHz',f0,fwhm);
            else
                capstring = '';                
            end
            set(obj.CaptionFFT,'string',capstring);
            
            MRIDATA.fwhm = fwhm;
            MRIDATA.CF   = seq.GetParameter('FREQUENCY');
        end
        function SetGUIFigureHandles(obj,h_fid,h_fft)
            obj.figFID = h_fid;
            obj.figFFT = h_fft;
        end
        function SetGUICaptionHandles(obj,h_fid,h_fft)
            obj.CaptionFID = h_fid;
            obj.CaptionFFT = h_fft;
            set(obj.CaptionFID,'string','');            
            set(obj.CaptionFFT,'string','');            
        end
        function EnableFIDMarkers(obj,sw)
            obj.ShowFIDMarkers = sw;
        end
        function EnableFIDCaption(obj,sw)
            obj.ShowFIDCaption = sw;
        end
        function EnableFFTMarkers(obj,sw)
            obj.ShowFFTMarkers = sw;
        end
        function EnableFFTCaption(obj,sw)
            obj.ShowFFTCaption = sw;
        end
        function SetAbsFID(obj,sw)
            obj.absFID = sw;
        end
        function SetAbsFFT(obj,sw)
            obj.absFFT = sw;
        end
    end
    
end

