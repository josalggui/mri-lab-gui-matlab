classdef ERNST_analysis < MRI_cartesian_analysis_JM
    %ANALYSIS for Gradient Echo 3D     
    
    methods
        function obj=ERNST_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'ERNST Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
            global rawData;
            close (1)
            nRepetitions = rawData.inputs.numberOfRepetitions;
            nRFAmplitudes = rawData.inputs.numberOfRFAmplitudes;
            TRmin = rawData.inputs.minTR;
            TRmax = rawData.inputs.maxTR;
            RFmin = rawData.inputs.minRFAmplitude;
            RFmax = rawData.inputs.maxRFAmplitude;
            signal = reshape(rawData.kSpace.signal(:,3),nRepetitions,nRFAmplitudes);
            
            figure(1)
            set(gcf, 'Position', [100, 500, 1000, 400])
            
            subplot(1,2,1)
            imagesc([RFmin RFmax],log10([(TRmin*1e3),(TRmax*1e3)]),abs(signal))
            ylabel('log10(TR (ms))')
            xlabel('RF amplitude (u.a.)')
            colorbar
            title('Signal')
            
            subplot(1,2,2)
            TR = rawData.aux.TRs';
            for ii = 1:nRFAmplitudes
                signal(:,ii) = signal(:,ii)./sqrt(TR);
            end
            imagesc([RFmin RFmax],log10([(TRmin*1e3),(TRmax*1e3)]),abs(signal))
            ylabel('log10(TR (ms))')
            xlabel('RF amplitude (u.a.)')
            colorbar
            title('Time normalized signal')
            
            
%             
%             seq = obj.seqClass;
%             nPoints = seq.GetParameter('NREADOUTS');
%             nPhases = seq.GetParameter('NPHASES');
%             nSlices = seq.GetParameter('NSLICES');
%             if length(seq.fidData) ==  nPoints*nPhases*nSlices
%                 MRIDATA.Fid3D = reshape(seq.fidData, nPoints, nPhases, nSlices);
% %                 MRIDATA.Data3Dnew(numel(MRIDATA.Data3Dnew)+1).Fid3D = MRIDATA.Fid3D;
%                 obj.Fid3D = MRIDATA.Fid3D;
%                 obj.Fid2D = flipud(MRIDATA.Fid3D(:,:,ceil(nSlices/2))');
%                 MRIDATA.Fid2D = obj.Fid2D;
%                 padding = [0 0];
%                 % padding = [2^nextpow2(size(obj.Fid2D,1))-size(obj.Fid2D,1) 2^nextpow2(size(obj.Fid2D,2))-size(obj.Fid2D,2)];%0.5*[size(obj.Fid2D,1) size(obj.Fid2D,2)]; %
%                 % s = size(obj.Fid2D);
%                 % Gstd = seq.GetParameter('GAUSSIANFILTER');
%                 % G = customgauss(s,Gstd*s(1),Gstd*s(2),0,0,1,0*s);
%                 % obj.Fid2D = obj.Fid2D.*G;
%                 
%                 obj.Image2D = fftshift( ifftn( fftshift( padarray( obj.Fid2D, ceil(padding/2), 'both' ) ) ) );
%                 switch obj.PlotSelector
%                     case '2D Image'
%                         Kspace=obj.Fid2D;
%                         obj.Plot2D(Kspace);
%                     case '2D Phase'
%                         obj.Plot2DPhase();
%                     case '3D Image'
%                         obj.Plot3D();
%                     otherwise
%                         warning('Unknown Plot','Unexpected plot type. No plot created.');
%                 end
%                 obj.SNR_MinAndMaxValues_Module();
%             end
%             if nPhases > 1
%                 save('tempMRIDATA.mat','MRIDATA');
%             end
%             results_file=fullfile(seq.path_file,'results.mat');
%             save (results_file, 'seq', 'obj');
        end
    end
end

