classdef WAHUHA_SWEEP_analysis < MRI_cartesian_analysis_JM
    %ANALYSIS for Gradient Echo 3D     
    
    methods
        function obj=WAHUHA_SWEEP_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'WAHUHA SWEEP Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
            warndlg('Sweep ready!','Warning')            
        end
    end
end

