classdef PETRA0_analysis < MRI_cartesian_analysis_JM
    %ANALYSIS for Gradient Echo 3D     
    
    methods
        function obj=PETRA0_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'PETRA Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
            global rawData;
            close (1);
            nPoints = rawData.inputs.nPoints;
            fov = rawData.inputs.fov*1d3;
            kMax = rawData.aux.kMax*1d-3;
            tMax = rawData.inputs.tMax*1d6;
            axes = rawData.inputs.axes;
            data = rawData.kSpace.paddedInterpolation;
            imagen = abs(ifftshift(ifftn(data)));
            figure(1)
            set(gcf, 'Position', [100, 500, 1500, 400])
            
            subplot(1,3,1)
            if(nPoints(3)==1)
                imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],imagen)
            else
                imagesc([-fov(2)/2 fov(2)/2],[-fov(1)/2 fov(1)/2],squeeze(imagen(:,:,ceil(nPoints(3)/2))));
            end
            title('image')
            xlabel(strcat(axes(2),' (mm)'))
            ylabel(strcat(axes(1),' (mm)'))
            
            subplot(1,3,2)
            if(nPoints(3)==1)
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],abs(data))
            else
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],squeeze(abs(data(:,:,ceil(nPoints(3)/2)))));
            end
            title('k-space magnitude')
            xlabel(strcat('k',axes(2),' (mm^{-1})'))
            ylabel(strcat('k',axes(1),' (mm^{-1})'))
            
            subplot(1,3,3)
            if(nPoints(3)==1)
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],angle(data))
            else
                imagesc([-kMax(2) kMax(2)],[-kMax(1) kMax(1)],squeeze(angle(data(:,:,ceil(nPoints(3)/2)))));
            end
            title('k-space phase')
            xlabel(strcat('k',axes(2),' (mm^{-1})'))
            ylabel(strcat('k',axes(1),' (mm^{-1})'))
            colormap gray
            
            
            h = obj.figFID;
            t = linspace(-tMax(1),tMax(1),nPoints(1));
            plot(t,abs(data(:,ceil(nPoints(2)/2),ceil(nPoints(3)/2))),...
                t,abs(data(ceil(nPoints(1)/2),:,ceil(nPoints(3)/2))),...
                'Parent',h);
            xlabel(h,'t (us)');
            ylabel(h,'signal (a.u.)')
            title(h,'Signal magnitude')
            pause(0.1)
            
            h = obj.figFFT;
            x1 = linspace(-fov(1)/2,fov(1)/2,nPoints(1));
            x2 = linspace(-fov(2)/2,fov(2)/2,nPoints(2));
            plot(x1,imagen(:,ceil(nPoints(2)/2),ceil(nPoints(3)/2)),...
                x2,imagen(ceil(nPoints(1)/2),:,ceil(nPoints(3)/2)),...
                'Parent',h);
            xlabel(h,'d (mm)');
            ylabel(h,'pixel magnitude (a.u.)')
            title(h,'Image')
            pause(0.1)
        end
    end
end

