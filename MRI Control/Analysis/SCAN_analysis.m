classdef SCAN_analysis < MRI_analysis
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        figScan;
        axesScan;
        nSamples;
        ScanPar;
        ScanParValues;
        fidDataScan;
        fftDataScan;
        firstPlotScan = 1;
        scanLine;
    end
    
    methods
        function obj=SCAN_analysis(seq,gfig,par)
            obj = obj@MRI_analysis(seq,gfig);
            obj.Name = 'SCAN Analysis';
            obj.ScanPar = par;
        end
        function Reset(obj)
            Reset@MRI_analysis(obj);
            obj.firstPlotScan = 1;
        end
        function SetFigureHandle(obj,h)
            obj.figScan = h;
        end
        function SetScanParValues(obj,val)
            obj.ScanParValues = val;
            n = length(val);
            seq = obj.seqClass;
            npt = seq.GetParameter('NPOINTS');
            obj.fidDataScan = zeros(npt,n);
            obj.fftDataScan = zeros(npt,n);
        end
        function Execute(obj)
            Execute@MRI_analysis(obj);
            index = obj.iAcq;
            seq = obj.seqClass;
            obj.fidDataScan(:,index) = seq.fidData;
            obj.fftDataScan(:,index) = seq.fftData1D;
            obj.PlotScan();
        end
        function xdata = GetXData(obj)
            xdata = get(obj.scanLine,'XData');
        end
        function ydata = GetYData(obj)
            ydata = get(obj.scanLine,'YData');
        end
        function PlotScan(obj)
%             scanData = sum(abs(obj.fidDataScan),1); %we were using this till April 14th, 2020
            scanData = abs(obj.fidDataScan (1,:)); 
            scanData_real = real(obj.fidDataScan (1,:)); 
            scanData_imag = imag(obj.fidDataScan (1,:));
%            scanData = sum(abs(obj.fftDataScan),1);
            seq = obj.seqClass;
            if 1%(obj.firstPlotScan)
                figure(obj.figScan);
                obj.axesScan = gca;
                h = obj.axesScan;
                obj.firstPlotScan = 0;
                parUnit = seq.GetParameterUnit(obj.ScanPar);
                xvectorScan = MyUnits.ConverFromStandardUnits(obj.ScanParValues,parUnit);
                obj.scanLine = plot(xvectorScan,scanData,'-o','Parent',h); %obj.scanLine = plot(xvectorScan,scanData,'Parent',h);
                %                xlim([min(xvectorScan) max(xvectorScan)]);
                hold on
                plot (xvectorScan, scanData_real,'g-*', xvectorScan,scanData_imag, 'm-^', 'Parent',h);
                legend ('abs','real','imag');
                hold off
                grid(h,'on')
                parName = seq.GetParameterName(obj.ScanPar);
                title(h,sprintf('%s Scan',parName));
                if isempty(parUnit)
                    lbl = parName;
                else
                    lbl = sprintf('%s, %s',parName,parUnit);
                end
                xlabel(h,lbl);
            else
                h = obj.axesScan;
                set(obj.scanLine,'YData',scanData,'Parent',h);
                set(obj.scanLine,'Marker','.','Parent',h);

                %                ylim(obj.CalculateYLimit(fidData,ylim));
            end
        end
    end
    
end

