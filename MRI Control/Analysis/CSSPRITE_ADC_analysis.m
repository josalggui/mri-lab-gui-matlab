classdef CSSPRITE_ADC_analysis < MRI_cartesian_analysis

    
    methods
        function obj=CSSPRITE_ADC_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis(seq,gfig);
            obj.Name = 'CS SPRITE ADC Analysis';
            obj.PlotSelectorNames = {'2D Image FFT','2D Image WV','3D Image'};
            obj.PlotSelector = '2D Image FFT';
        end
        function Execute(obj)
            global MRIDATA;
            Execute@MRI_cartesian_analysis(obj);
            seq = obj.seqClass;
            nReadouts = seq.GetParameter('NREADOUTS');
            nPhases = seq.GetParameter('NPHASES');
            nSlices = seq.GetParameter('NSLICES');
            if length(seq.fidData) ==  nReadouts*nPhases*nSlices
                MRIDATA.Fid3D = reshape(seq.fidData, nReadouts, nPhases, nSlices);
                MRIDATA.Data4D(numel(MRIDATA.Data4D)+1).Fid3D = MRIDATA.Fid3D;
                obj.Fid3D = MRIDATA.Fid3D;
                obj.Fid2D = flipud(MRIDATA.Fid3D(:,:,ceil(nSlices/2))');
                MRIDATA.Fid2D = obj.Fid2D;
                padding = [2^nextpow2(size(obj.Fid2D,1))-size(obj.Fid2D,1) 2^nextpow2(size(obj.Fid2D,2))-size(obj.Fid2D,2)];%0.5*[size(obj.Fid2D,1) size(obj.Fid2D,2)]; %
                s = size(obj.Fid2D);
                Gstd = 1;%seq.GetParameter('GAUSSIANFILTER');
                G = customgauss(s,Gstd*s(1),Gstd*s(2),0,0,1,0*s);
                obj.Fid2D = obj.Fid2D.*G;
                
                obj.Image2D = fftshift( ifftn( fftshift( padarray( obj.Fid2D, ceil(padding/2), 'both' ) ) ) );
                switch obj.PlotSelector
                    case '2D Image FFT'
                        Kspace=obj.Fid2D;
                        obj.Plot2D(Kspace);
                    case '2D Image WV'
                        obj.Plot2DWV();
                    case '3D Image'
                        obj.Plot3D();
                    otherwise
                        warning('Unknown Plot','Unexpected plot type. No plot created.');
                end
                obj.SNR_MinAndMaxValues_Module();
            end
            results_file=fullfile(seq.path_file,'results.mat');
            save (results_file, 'seq', 'obj');
            save('tempMRIDATA.mat','MRIDATA');
        end
        
        function Plot2DWV( obj )
            seq = obj.seqClass;
            nReadouts = seq.GetParameter('NREADOUTS');
            nPhases = seq.GetParameter('NPHASES');
            
            % create the data structure
%             data = obj.Fid2D;
            data = zeros(nReadouts,nPhases);
            data( seq.INDuse ) = seq.Data3D;
            
            % create the mask
            mask = false(nReadouts,nPhases);
            mask( seq.INDuse ) = true;
            
            % pad array if need be
            padding = [2^nextpow2(size(data,1))-size(data,1) 2^nextpow2(size(data,2))-size(data,2)];%0.5*[size(obj.Fid2D,1) size(obj.Fid2D,2)]; %
            if mod(padding,2)
                data = padarray( data, ceil(padding/2), 'pre' );
                data = padarray( data, floor(padding/2), 'post' );
                mask = padarray( mask, floor(padding/2), 'post' );
                mask = padarray( mask, ceil(padding/2), 'pre' );
                PDF_mat = padarray( seq.PDF_mat, ceil(padding/2), min(seq.PDF_mat(:)), 'pre' );
                PDF_mat = padarray( PDF_mat, floor(padding/2), min(seq.PDF_mat(:)), 'post' );
            else
                data = padarray( data, padding/2, 'both' );
                mask = padarray( mask, padding/2, 'both' );
                PDF_mat = padarray( seq.PDF_mat, padding/2, min(seq.PDF_mat(:)), 'both' );
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % L1 Recon Parameters 
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%

            N = size(data); 	% image Size
            DN = size(data); 	% data Size
            TVWeight = 0.05; 	% Weight for TV penalty
            xfmWeight = 0.01;	% Weight for Transform L1 penalty
            Itnlim = 150;		% Number of iterations
            
            %generate Fourier sampling operator
            FT = p2DFT(mask, N, 0, 2);

            % scale data
            im_dc = FT'*(data.*mask);
%             im_dc = FT'*(data.*mask./PDF_mat);
            data = data/max(abs(im_dc(:)));
            im_dc = im_dc/max(abs(im_dc(:)));
           
            %generate transform operator
            XFM = Wavelet('Daubechies',10,10);	% Wavelet

            % initialize Parameters for reconstruction
            param = init;
            param.FT = FT;
            param.XFM = XFM;
            param.TV = TVOP;
            param.data = data;
            param.TVWeight =TVWeight;     % TV penalty 
            param.xfmWeight = xfmWeight;  % L1 wavelet penalty
            param.Itnlim = Itnlim;

            res = XFM*im_dc;
            res = fnlCg(res,param);
            im_res = XFM'*res;
            
            % now convert image to kspace to plot in the other function
            obj.Image2D = fliplr(im_res');
            kspace_plot = ifftshift( ifftn( fftshift( im_res )));
            obj.Plot2D(kspace_plot);
            
        end % end plot wavelet
    end
end

