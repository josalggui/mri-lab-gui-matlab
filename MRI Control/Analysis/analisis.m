
            load('C:\Users\josalggui\Documents\repository-mri-lab-2018.11.05\MRI Control\FileDir\rawData-2019.1.25.5.3.49.635.mat')

            nPoints = rawData.inputs.nPoints;
            fov = rawData.inputs.fov*1d3;
            kMax = rawData.aux.kMax*1d-3;
            tMax = rawData.inputs.tMax*1d6;
            axes = rawData.inputs.axes;
            
            data = reshape(rawData.kSpace.interpolated(:,4),nPoints);
            imagen = abs(ifftshift(ifftn(data)));
            figure(1)
            set(gcf, 'Position', [100, 500, 1500, 400])
            
            
            y = 11;
            
            subplot(1,3,1)
            if(nPoints(3)==1)
                imagesc([-fov(1)/2 fov(1)/2],[-fov(2)/2 fov(2)/2],imagen)
            else
                imagesc([-fov(1)/2 fov(1)/2],[-fov(2)/2 fov(2)/2],squeeze(imagen(:,:,y)));
            end
            title('image')
            xlabel(strcat(axes(2),' (mm)'))
            ylabel(strcat(axes(1),' (mm)'))
            
            
            
            subplot(1,3,2)
            if(nPoints(3)==1)
                imagesc([-kMax(1) kMax(1)],[-kMax(2) kMax(2)],abs(data))
            else
                imagesc([-kMax(1) kMax(1)],[-kMax(2) kMax(2)],squeeze(abs(data(:,:,y))));
            end
            title('k-space magnitude')
            xlabel(strcat('k',axes(2),' (mm^{-1})'))
            ylabel(strcat('k',axes(1),' (mm^{-1})'))
            
            
            
            subplot(1,3,3)
            if(nPoints(3)==1)
                imagesc([-kMax(1) kMax(1)],[-kMax(2) kMax(2)],angle(data))
            else
                imagesc([-kMax(1) kMax(1)],[-kMax(2) kMax(2)],squeeze(angle(data(:,:,y))));
            end
            title('k-space phase')
            xlabel(strcat('k',axes(2),' (mm^{-1})'))
            ylabel(strcat('k',axes(1),' (mm^{-1})'))
            colormap gray
            
            
%             h = obj.figFID;
%             t = linspace(-tMax(1),tMax(1),nPoints(1));
%             plot(t,abs(data(:,ceil(nPoints(2)/2),ceil(nPoints(3)/2))),...
%                 t,abs(data(ceil(nPoints(1)/2),:,ceil(nPoints(3)/2))),...
%                 'Parent',h);
%             xlabel(h,'t (us)');
%             ylabel(h,'signal (a.u.)')
%             title(h,'Signal magnitude')
%             
%             h = obj.figFFT;
%             x1 = linspace(-fov(1)/2,fov(1)/2,nPoints(1));
%             x2 = linspace(-fov(2)/2,fov(2)/2,nPoints(2));
%             plot(x1,imagen(:,ceil(nPoints(2)/2),ceil(nPoints(3)/2)),...
%                 x2,imagen(ceil(nPoints(1)/2),:,ceil(nPoints(3)/2)),...
%                 'Parent',h);
%             xlabel(h,'d (mm)');
%             ylabel(h,'pixel magnitude (a.u.)')
%             title(h,'Image')
%             

