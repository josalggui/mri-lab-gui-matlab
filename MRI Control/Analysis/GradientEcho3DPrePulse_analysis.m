classdef GradientEcho3DPrePulse_analysis < MRI_cartesian_analysis
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
       
    methods
        function obj=GradientEcho3DPrePulse_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis(seq,gfig);
            obj.Name = 'GradientEcho3D PrePulse Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase'};
            obj.PlotSelector = '2D Image';
        end
        function Execute(obj)
            global MRIDATA;
            Execute@MRI_cartesian_analysis(obj);
            seq = obj.seqClass;
            nPoints = seq.GetParameter('NPOINTS');
            nPhases = seq.GetParameter('NPHASES');
            nSlices = 1;%seq.GetParameter('NSLICES');
            if length(seq.fidData) ==  nPoints*nPhases*nSlices
                MRIDATA.Fid3D = reshape(seq.fidData, nPoints, nPhases, nSlices);
                obj.Fid3D = MRIDATA.Fid3D;
                obj.Fid2D = flipud(MRIDATA.Fid3D(:,:,ceil(nSlices/2))');
                MRIDATA.Fid2D = obj.Fid2D;
                padding = 0.5*[size(obj.Fid2D,1) size(obj.Fid2D,2)]; %[2^nextpow2(size(obj.Fid2D,1))-size(obj.Fid2D,1) 2^nextpow2(size(obj.Fid2D,2))-size(obj.Fid2D,2)];
                s = size(obj.Fid2D);
                Gstd = seq.GetParameter('GAUSSIANFILTER');
                G = customgauss(s,Gstd*s(1),Gstd*s(2),0,0,1,0*s);
                obj.Fid2D = obj.Fid2D.*G;
                
                obj.Image2D = fftshift( ifftn( fftshift( padarray( obj.Fid2D, ceil(padding/2), 'both' ) ) ) );
                switch obj.PlotSelector
                    case '2D Image'
                        Kspace=obj.Fid2D;
                        obj.Plot2D(Kspace);
                    case '2D Phase'
                        obj.Plot2DPhase();
                    otherwise
                        warning('Unknown Plot','Unexpected plot type. No plot created.');
                end
                obj.SNR_MinAndMaxValues_Module();
            end
            results_file=fullfile(seq.path_file,'results.mat');
            save (results_file, 'seq', 'obj');
        end
  
        function Plot2D(obj,Kspace)
            
            % Dont plot anything....
            
            if ishandle(obj.fig2D)
                close( obj.fig2D);
            end
%             if (obj.firstPlot2D)
%                 figure(obj.fig2D);
%                 obj.firstPlot2D = 0;
%                 h = subplot(3,1,1);
%                 I = abs(obj.Image2D);
%                 G = fspecial('gaussian',[5 5],2);
%                 I = imfilter(I,G,'same');
%                 obj.plotImage2D = imagesc(I,'Parent',h);
%                 colorbar('peer',h);
%                 colormap(h,'gray')
%                 title(h,'2D FFT - Magnitude');
%                 h = subplot(3,1,2);
%                 obj.plotAbsKspace = imagesc(abs(obj.Fid2D),'Parent',h);
%                 colorbar('peer',h);
%                 colormap(h,'gray')
%                 title(h,'Abs of k-space');
%                 h = subplot(3,1,3);
%                 obj.plotRealKspace = imagesc(real(obj.Fid2D),'Parent',h);
%                 colorbar('peer',h);
%                 colormap(h,'gray')
%                 title(h,'Real of k-space');
%             else
%                 I = abs(obj.Image2D);
%                 G = fspecial('gaussian',[5 5],2);
%                 I = imfilter(I,G,'same');
%                 set(obj.plotImage2D, 'CData', I);
%                 set(obj.plotAbsKspace, 'CData', abs(obj.Fid2D));
%                 set(obj.plotRealKspace, 'CData', real(obj.Fid2D));
%                 
%             end
        end


    end
end

