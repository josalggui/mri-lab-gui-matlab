classdef SNR_FID_analysis < MRI_cartesian_analysis_JM   
    
    methods
        function obj=SNR_FID_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'SNR_FID Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
            global rawData;
            signals=rawData.outputs.signals;
            time=rawData.outputs.snr(:,1);
            snr=rawData.outputs.snr(:,2);
            
            h = obj.figFID;
            imagesc(abs(signals),'Parent',h);
            xlabel(h,'Number of FID');
            ylabel(h,'Temporal FID evolution')
            colorbar(h)
            title(h,'Full dataset acquired')
                                
            h = obj.figFFT;
            plot(time*1e3,snr,'Parent',h);
            xlabel(h,'Acquisition time (ms)');
            ylabel(h,'Temporal SNR evolution')
            title(h,'SNR evolution')
        end
    end
end
