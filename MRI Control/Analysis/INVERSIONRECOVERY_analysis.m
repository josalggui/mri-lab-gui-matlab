classdef INVERSIONRECOVERY_analysis < MRI_cartesian_analysis_JM   
    
    methods
        function obj=INVERSIONRECOVERY_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'INVERSIONRECOVERY Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
            
            global rawData;
            
            signals=rawData.outputs.signals;
            TI=rawData.outputs.TI;
            
            h = obj.figFID;
            plot(TI*1e3,real(signals(1,:)),TI*1e3,abs(signals(1,:)),'.','Parent',h);
            grid on
            legend(h,'Real','Abs');
            xlabel(h,'TI (ms)');
            ylabel(h,'Signal (mV)');
            title(h,'Initial signal amplitude as a funcion of TI')
            
            
            h = obj.figFFT;
            imagesc(abs(signals),'Parent',h);
            xlabel(h,'Step');
            ylabel(h,'Temporal FID evolution (mV)')
            title(h,'Full dataset acquired')
            pause(0.1)
        end
        
    end
end
