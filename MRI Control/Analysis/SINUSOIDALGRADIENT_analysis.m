classdef SINUSOIDALGRADIENT_analysis < MRI_cartesian_analysis_JM
    %ANALYSIS for Gradient Echo 3D     
    
    methods
        function obj=SINUSOIDALGRADIENT_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'SINUSOIDAL GRADIENT Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
%            
%              global rawData;
%             close (1)
%             
%             figure(1)
%             subplot(1,2,1)
%             plot(rawData.kSpace.sampled(:,1),real(rawData.kSpace.sampled(:,2)))
%             xlabel('time (s)')
%             ylabel('real (a.u.)')
%             
%             subplot(1,2,2)
%             plot(rawData.kSpace.sampled(:,1),imag(rawData.kSpace.sampled(:,2)))
%         
        end
        
    end
end

