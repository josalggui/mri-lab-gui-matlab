classdef PulseTime_auto_analysis < SCAN_analysis
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        UsedRepDelay;
        
    end
    
    methods
        function obj=PulseTime_auto_analysis(seq,gfig,par)
            obj = obj@SCAN_analysis(seq,gfig,par);
            obj.Name = 'Pulse time automatic Analysis';
        end
        function Execute(obj)
            Execute@SCAN_analysis(obj);
            if obj.iAcq == length(obj.ScanParValues)
                switch obj.ScanPar
                    case 'PULSETIME'
                        obj.FitPT();
                end
            end
        end
        function FitPT(obj)
            seq = obj.seqClass;
            
            % grab the fata
            xdata = obj.GetXData();
            ydata = obj.GetYData();
            
            % fit the data to an spline
            f = fittype('smoothingspline');
            cfun = fit(xdata',ydata',f);
            xdatafit = linspace(min(xdata),max(xdata),1000);
            ydatafit = cfun(xdatafit);
            PulseTime = xdatafit( ydatafit == max(ydatafit) );
            
           
            figure(obj.figScan);
            h = gca;
%             plot(h,xdata,ydata,'bo');
            hold(h,'on');
            plot(h,xdatafit,ydatafit,'r');
            plot(h,PulseTime,max(ydatafit),'r*','MarkerSize',12);hold(h,'off');
            legend ('abs','real','imag','abs fitted','max abs fitted');
            
            title(h,sprintf('90 degree Pulse Time: %.2f us',PulseTime));
            grid(h,'on');
            xlabel('Pulse time [us]');
           
            seq.SetParameter('PULSETIME',PulseTime*MyUnits.us);
            
        end
        
    end
end
