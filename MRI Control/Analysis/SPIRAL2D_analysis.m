classdef SPIRAL2D_analysis < MRI_cartesian_analysis_JM   
    
    methods
        function obj=SPIRAL2D_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'SPIRAL2D Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
            global rawData;
            
%             NX=rawData.inputs.NX;
%             NY=rawData.inputs.NY;
            fovx=rawData.inputs.FOVX*1e3;
            fovy=rawData.inputs.FOVY*1e3;
            imagen=rawData.kSpace.imagen;
%             kSpaceList=rawData.kSpace.sampled(:,4);
%             kSpaceReshaped=reshape(kSpaceList,[NX,NY]);
 
            
            h = obj.figFID;
            imagesc([-fovx/2 fovx/2],[-fovy/2 fovy/2],imagen,'Parent',h);
            pbaspect(h,[1 1 1])
            colormap (h,gray)
            xlabel(h,'Readout (mm)');
            ylabel(h,'Phase (mm)');
            title(h,'FFT')

            nIter=1;
            lambda=1;
            ARTimage = (artsequence(nIter,lambda,1,rawData));
%             ARTimage=art_MRI_CPU_2D(1,1,[rawData.inputs.FOVX,rawData.inputs.FOVY],[rawData.inputs.NX,rawData.inputs.NY],rawData);
%             filt = (PeronaMalikFilter2D(ARTimage, 15, 3/7, 70, 2))';

            h = obj.figFFT;
            imagesc([-fovx/2 fovx/2],[-fovy/2 fovy/2],abs(ARTimage),'Parent',h);
            pbaspect(h,[1 1 1])
            colormap (h,gray)
            xlabel(h,'Readout (mm)');
            ylabel(h,'Phase (mm)');
            title(h,'ART')
            pause(0.1)
            
%             h = obj.figFFT;
%             imagesc(abs(y_est),'Parent',h);
%             colormap (h,gray)
%             xlabel(h,'Kx');
%             ylabel(h,'Ky')
%             title(h,'kSpace')
%             pause(0.1)

        end
    end
end
