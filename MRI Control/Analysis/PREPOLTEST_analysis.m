classdef PREPOLTEST_analysis < MRI_cartesian_analysis_JM   
    
    methods
        function obj=PREPOLTEST_analysis(seq,gfig)
            obj = obj@MRI_cartesian_analysis_JM(seq,gfig);
            obj.Name = 'PREPOLTEST Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Phase';
        end
        function Execute(obj)
            global rawData;
            
            time =rawData.outputs.time;
            fidraw = rawData.outputs.fidraw;
            fidprepol = rawData.outputs.fidprepol;
            spectrumFFTraw = rawData.outputs.spectrumFFTraw;
            spectrumFFTprepol = rawData.outputs.spectrumFFTprepol;
            
            h = obj.figFID;
            plot(time*1e3,abs(fidraw),time*1e3,abs(fidprepol),'Parent',h);
            xlabel(h,'Acquisition time(ms)');
            ylabel(h,'Signal amplitude (a.u.)')
            legend(h,'Abs value FID raw','Abs value FID prepolarized')
            title(h,'Prepol comparison')
                
            h = obj.figFFT;
            plot(spectrumFFTraw(:,1),abs(spectrumFFTraw(:,2)),spectrumFFTprepol(:,1),abs(spectrumFFTprepol(:,2)),'Parent',h);
            xlabel(h,'Frequency (kHz)');
            ylabel(h,'Frequencial component amplitude (a.u.)')
            legend(h,'Abs value FFT raw','Abs value FFT prepolarized')
            title(h,'Spectrum')
            
            
%             h = obj.figFFT;
%             plot(time*1e3,abs(fidprepol)/abs(fidraw),'Parent',h);
%             xlabel(h,'Acquisition time(ms)');
%             ylabel(h,'Signals quocient')
%             title(h,'Prepol comparison')


        end
    end
end
