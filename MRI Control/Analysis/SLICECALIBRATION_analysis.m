classdef SLICECALIBRATION_analysis < MRI_cartesian_Units_analysis
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
       
    methods
        function obj=SLICECALIBRATION_analysis(seq,gfig)
            obj = obj@MRI_cartesian_Units_analysis(seq,gfig);
            obj.Name = 'SLICECALIBRATION Analysis';
            obj.PlotSelectorNames = {'2D Image','2D Phase','3D Image'};
            obj.PlotSelector = '2D Image';
        end
        function Execute(obj)
            global rawData;
            data = rawData.output.data;
            
            h = obj.figFID;
            plot(data(:,1),data(:,2),data(:,1),data(:,3),data(:,1),data(:,4),...
                'Parent',h);
            xlabel(h,'Slice factor');
            ylabel(h,'Slice signal (V)')
            title(h,'Signal magnitude')
            legend(h,'x','y','z')
            pause(0.1)
            
        end
    end
end

