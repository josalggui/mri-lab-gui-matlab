function varargout = mrigui(varargin)
% MRIGUI MATLAB code for mrigui.fig
% MRIGUI is the Graphical User Interface for executing the MRI software.
%      MRIGUI, by itself, creates a new MRIGUI or raises the existing
%      singleton*.
%
%      H = MRIGUI returns the handle to a new MRIGUI or the handle to
%      the existing singleton*.
%
%      MRIGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MRIGUI.M with the given input arguments.
%
%      MRIGUI('Property','Value',...) creates a new MRIGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before mrigui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to mrigui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help mrigui

% Last Modified by GUIDE v2.5 18-May-2020 15:59:52

% Begin initialization code - DO NOT EDIT

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @mrigui_OpeningFcn, ...
    'gui_OutputFcn',  @mrigui_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

global scanner
if isempty(scanner)
    load('scanner_conf_file.mat','scanner'); %Default config file
end

% End initialization code - DO NOT EDIT

% --- Executes just before mrigui is made visible.
function mrigui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to mrigui (see VARARGIN)

global scanner

% Choose default command line output for mrigui
handles.output = hObject;
set(handles.textScanner,'string',sprintf('%s',scanner.name));

% set(hObject,'Units','Pixels','Position',get(0,'ScreenSize'))
% % set(hObject,'Units','Normalized')
% set([findall(hObject, '-property', 'Units'); findall(hObject,'-type','text')], 'Units', 'normalized','FontUnits','normalized')

% This sets up the initial plot - only do when we are invisible
% so window can get raised using mrigui.
if strcmp(get(hObject,'Visible'),'off')
    plot(rand(5));
end

addlistener(handles.sliderPhase,'Value','PreSet',@sliderPhase_RTCallback);

[pathstr, name, ext] = fileparts(mfilename('fullpath'));
seqList = what(fullfile(pathstr,'Sequences'));
menuList = cell(1,length(seqList.m));
for i=1:length(seqList.m)
%     seqList.m(i)
    [~,seqName,~] = fileparts(seqList.m{i});
    menuList{i} = seqName;
end
set(handles.popupmenuSequence,'string',menuList);

oriList = what(fullfile(pathstr,'Orientations'));
OmenuList = cell(1,length(oriList.m));
for i=1:length(oriList.m)
%     oriList.m(i)
    [~,oriName,~] = fileparts(oriList.m{i});
    OmenuList{i} = oriName;
end
set(handles.popupmenuOrientation,'string',OmenuList);

handles.SequenceClassList = containers.Map();
idx = find(ismember(menuList,'GradientEcho3D'));
set(handles.popupmenuSequence,'value',idx);
selectedSequence = MRIUtils.getCurrentPopupString(handles.popupmenuSequence);
handles.SequenceClass = eval(selectedSequence);
handles.SequenceClassList(selectedSequence) = handles.SequenceClass;
selectedSequenceAnalysis = sprintf('%s_analysis',selectedSequence);
if exist(selectedSequenceAnalysis,'class')
    handles.SequenceAnalysisClass = eval(sprintf('%s(handles.SequenceClass,gcf)',selectedSequenceAnalysis));
else
    handles.SequenceAnalysisClass = MRI_Analysis(handles.SequenceClass,gcf); %base analysis class
end

idx = find(ismember(OmenuList,'YZ_orientation'));
set(handles.popupmenuOrientation,'value',idx);
handles.selectedOrientation = MRIUtils.getCurrentPopupString(handles.popupmenuOrientation);
global MRIDATA
MRIDATA.Quaternion = eval(handles.selectedOrientation);

% Update handles structure
guidata(hObject, handles);
SetDefaultParameters(handles);


% UIWAIT makes mrigui wait for user response (see UIRESUME)
% uiwait(handles.figure1);

%populate Par Table
function FillParTable(table,names,values,units,pars)
rowNames = cell(length(pars),1);
tData = cell(length(pars),2);
for i=1:length(pars)
    par = pars{i};
    tableValue = values(par);
    if ~isempty(units(par))
        tableValue = MyUnits.ConverFromStandardUnits(values(par),units(par));
    end
    if isnan(tableValue)
        tableValue = '';
    end
    rowNames{i} = names(par);
    tData(i,1:2) = {tableValue,units(par)}; %spectrometerFrequency_MHz
end
set(table,'RowName',rowNames);
set(table,'Data',tData);

% function SelectSequence(hObject,handles,seqName)
% menuList = get(handles.popupmenuSequence,'String');
%
% guidata(hObject, handles);
% UpdateParametersDisplay(handles);


function UpdateParametersDisplay(handles)
seq = handles.SequenceClass;
seqA = handles.SequenceAnalysisClass;
set(handles.textSequence,'string',sprintf('%s/%s',seq.SequenceName,seqA.Name));
[pars,names,values,units] = seq.GetInputParameters();
FillParTable(handles.tableInput,names,values,units,pars);
[pars,names,values,units] = seq.GetOutputParameters();
FillParTable(handles.tableOutput,names,values,units,pars);
plotList = seqA.GetPlotsList();
set(handles.popupmenuPlots,'string',plotList);
if ~isempty(plotList)
    popupmenuPlots_Callback(handles.popupmenuPlots, [], handles);
end
selectedSeq = MRIUtils.getCurrentPopupString(handles.popupmenuSequence);
if strcmp(selectedSeq,'GradientEcho3D_IECO')
    set(handles.checkboxFiltering,'Enable','on');
else
    set(handles.checkboxFiltering,'Enable','off');
end
if strcmp(selectedSeq,'GradientEcho3D_blank_cal') || strcmp(selectedSeq,'GradientEcho3D_withADC') || strcmp(selectedSeq,'CSSPRITE_ADC') ...
    || strcmp(selectedSeq,'RadialSPRITE_ADC_PP') || strcmp(selectedSeq,'SpinEchoSlice_withADC') || strcmp(selectedSeq,'GE3D_Slice_ADC') ...
    || strcmp(selectedSeq,'SE3D_wSLICE_wADC')
    set(handles.checkboxPerformCal,'Enable','on');
    set(handles.checkboxCalImag,'Enable','on');
elseif(strcmp(selectedSeq,'PETRA_V1'))
    set(handles.checkboxPerformCal,'Enable','off');
    set(handles.checkboxSpoilerGrad,'Enable','off');
    set(handles.radiobuttonSQUARE,'Value',1);
    set(handles.radiobuttonSINC,'Enable','off');
    set(handles.checkboxAutoPhase,'Enable','off');
else
    set(handles.checkboxPerformCal,'Enable','off');
    set(handles.checkboxCalImag,'Enable','off');
end


function SetDefaultParameters(handles)
global CONTROL

seq = handles.SequenceClass;
seq.SetDefaultParameters();
UpdateParametersDisplay(handles)

set(handles.editAveraging,'string',num2str(10));
set(handles.checkboxAveraging,'Value',false);

set(handles.editFiltering,'string',num2str(200));
set(handles.checkboxFiltering,'Value',false);

set(handles.checkboxAutoPhase,'Value',false);

set(handles.editInstrIni,'string',num2str(1));
set(handles.editInstrFin,'string',num2str(20));

set(handles.radiobuttonFIDReal,'Value',true);
set(handles.radiobuttonFFTAbs,'Value',true);

set(handles.pushbuttonStart,'Enable','on');
set(handles.pushbuttonStop,'Enable','off');

selectedSeq = MRIUtils.getCurrentPopupString(handles.popupmenuSequence);
if ~strcmp(selectedSeq,'GradientEcho3D_IECO')
    set(handles.checkboxFiltering,'Enable','off');
end
if ~strcmp(selectedSeq,'GradientEcho3D_blank_cal')
    set(handles.checkboxPerformCal,'Enable','off');
    set(handles.checkboxCalImag,'Enable','off');
end
set(handles.pushbuttonReload,'Enable','on');
set(handles.editAveraging,'string',num2str(10));
set(handles.pushbuttonPlotSequence,'Enable','on');

set(handles.textCaptionFID,'string','Caption FID');
set(handles.textCaptionFFT,'string','Caption FFT');


%h = uicontrol('style','slider','callback',@(src,evt)disp(get(src,'value')));
addlistener(handles.sliderPhase,'Value','PreSet',@sliderPhase_RTCallback);

sliderPhase_Callback(handles.sliderPhase, [], handles)

CONTROL.Run = false;
CONTROL.scanRun = false;
CONTROL.scanRun2D = false;
CONTROL.scanRun3D = false;
CONTROL.ParametersChanged = false;
CONTROL.gui_fig = gcf;
CONTROL.FIDPhase = 0;

CONTROL.HardCoded.BandwidthMax = 0.1;
CONTROL.HardCoded.FrequencyMax = 10.64 *1.05;
CONTROL.HardCoded.FrequencyMin = 10.64 *0.95;
CONTROL.HardCoded.FrequencyCen = 5.5;

% Update TE and TR values next to table 
% selectedSeq = MRIUtils.getCurrentPopupString(handles.popupmenuSequence);
seq.UpdateTETR;
set( handles.text_TE, 'String', num2str( (1E6) * seq.TE ));
set( handles.text_TR, 'String', num2str( (1E3) * seq.TR ));
set( handles.text_TTotal, 'String', num2str( seq.TTotal ));





%Run MRI program
function RunMri(handles)
global CONTROL

CONTROL.ParametersChanged = true;
seq = handles.SequenceClass;
seq.ReferenceCF=seq.GetParameter('FREQUENCY');

iAcq=0;
ana = handles.SequenceAnalysisClass;
if isfield(CONTROL,'scanRun') && CONTROL.scanRun
    seq.SetParameter(CONTROL.scanPar,CONTROL.scanParValues(1));
    ana.SetScanParValues(CONTROL.scanParValues);
    if isfield(CONTROL,'scanFig') && ishandle(CONTROL.scanFig)
        close(CONTROL.scanFig);
    end
    CONTROL.scanFig = figure;
    ana.SetFigureHandle(CONTROL.scanFig);
elseif isfield(CONTROL,'scanRun2D') && CONTROL.scanRun2D
    seq.SetParameter(CONTROL.scanPar{1},CONTROL.scanPar1Values(1,1));
    seq.SetParameter(CONTROL.scanPar{2},CONTROL.scanPar2Values(1,1));
    val(1,:,:) = CONTROL.scanPar1Values;
    val(2,:,:) = CONTROL.scanPar2Values;
    ana.SetScanParValues(val);
    if isfield(CONTROL,'scanFig') && ishandle(CONTROL.scanFig)
        close(CONTROL.scanFig)
    end
    CONTROL.scanFig = figure;
    ana.SetFigureHandle(CONTROL.scanFig);
else
    if isfield(CONTROL,'runFig')
        for i=1:length(CONTROL.runFig)
            if ishandle(CONTROL.runFig(i))
                close(CONTROL.runFig(i));
            end
        end
    end
    figs = ana.FigureRequest;
    if ~isempty(figs)
        scrsz = get(0,'ScreenSize');
        for i=1:size(figs,1)
            height = (scrsz(4)-100)*figs(i,2);
            width = height*figs(i,1);
            CONTROL.runFig(i) = figure('OuterPosition',[10 50 width height]);
        end
        ana.SetFigureHandle(CONTROL.runFig);
    end
end
%
% % set status bar on the figure
% seq.SetStatusBar(CONTROL.runFig);

while true
    if ~CONTROL.Run; break; end
    if CONTROL.ParametersChanged
%         fprintf('Parameters changed\n');
        CONTROL.ParametersChanged = false;
        nPoints = seq.GetParameter('NPOINTS'); %nPoints
        nPhases = seq.GetParameter('NPHASES'); %nPhases
        iAcq=0;
        selectedSeq = MRIUtils.getCurrentPopupString(handles.popupmenuSequence);
        if strcmp(selectedSeq,'GradientEcho3D_IECO')
            seq.EnableFiltering(get(handles.checkboxFiltering,'Value'));
            seq.SetFreqFiltering(str2num(get(handles.editFiltering,'string')));
        end
        seq.EnableAveraging(get(handles.checkboxAveraging,'Value'));
        seq.SetNAverages(str2num(get(handles.editAveraging,'string')));
        seq.EnableAutoPhase(get(handles.checkboxAutoPhase,'Value'));
        seq.EnableAutoFrequency(get(handles.checkboxAutoFreq,'Value'));
        if strcmp(selectedSeq,'GradientEcho3D_blank_cal')
            seq.EnableCalibration(get(handles.checkboxPerformCal,'Value'));
            seq.CalibrateImage(get(handles.checkboxCalImag,'Value'));
        end
        if get(handles.radiobuttonSINC,'Value')
            RF_Shape = 1;
        else
            RF_Shape = 0;
        end
        seq.SetRFShape(RF_Shape)
        seq.SetShimS(str2double(get(handles.editShimS,'String')));
        seq.SetShimP(str2double(get(handles.editShimP,'String')));
        seq.SetShimR(str2double(get(handles.editShimR,'String')));
        ana.Reset();
        
        
    end
    
    iAcq=iAcq+1;
    set(handles.textN,'string',num2str(iAcq));
    
    
    if isfield(CONTROL,'scanRun') && CONTROL.scanRun
        iScan = mod(iAcq-1,length(CONTROL.scanParValues))+1;
        seq.SetScanParameterValue(CONTROL.scanPar,CONTROL.scanParValues(iScan));
        UpdateParametersDisplay(handles);
    elseif isfield(CONTROL,'scanRun2D') && CONTROL.scanRun2D
        iScan2 = floor((iAcq-1)/size(CONTROL.scanPar1Values,1))+1;
        iScan1 = mod(iAcq-1,size(CONTROL.scanPar1Values,1))+1;
        seq.SetScanParameterValue(CONTROL.scanPar{1},CONTROL.scanPar1Values(iScan1,iScan2));
        seq.SetScanParameterValue(CONTROL.scanPar{2},CONTROL.scanPar2Values(iScan1,iScan2));
        UpdateParametersDisplay(handles);
    end
    
    
    phase = degtorad(str2double(get(handles.textFIDPhase,'string')));
    seq.SetFIDPhase(phase);
    %    seq.FIDPhase = CONTROL.FIDPhase;
    SpawnMriCommand(handles,iAcq)
    
    gui_fig = CONTROL.gui_fig;
    %    figure(gui_fig);
    
    if iAcq < 1
        continue;
    elseif iAcq == 1
        ana.SetGUIFigureHandles(handles.FID,handles.FFT);
        ana.SetGUICaptionHandles(handles.textCaptionFID,handles.textCaptionFFT);
        ana.ResetPlots();
    end
    ana.EnableFIDMarkers(get(handles.checkboxFIDMarkers,'Value'));
    ana.EnableFIDCaption(get(handles.checkboxFIDCaption,'Value'));
    ana.EnableFFTMarkers(get(handles.checkboxFFTMarkers,'Value'));
    ana.EnableFFTCaption(get(handles.checkboxFFTCaption,'Value'));
    ana.SetAbsFID(get(handles.radiobuttonFIDAbs,'Value'));
    ana.SetAbsFFT(get(handles.radiobuttonFFTAbs,'Value'));
    ana.Execute();
    
    deltaFreq = ana.GetCentralFrequencyAdjustment();
    if get(handles.checkboxAutoFreq,'Value')
        seq.SetParameter('FREQUENCY',seq.GetParameter('FREQUENCY')+deltaFreq);
        [pars,names,values,units] = seq.GetInputParameters();
        FillParTable(handles.tableInput,names,values,units,pars);
    end
    drawnow
    if isfield(CONTROL,'scanRun') && CONTROL.scanRun && iAcq >= length(CONTROL.scanParValues)
        CONTROL.scanRun = false;
        runStop(handles);
    elseif isfield(CONTROL,'scanRun2D') && CONTROL.scanRun2D && iAcq >= length(CONTROL.scanPar1Values(:))
        CONTROL.scanRun2D = false;
        runStop(handles);
    end
end

% seq.StopStatusBar(CONTROL.runFig);

CONTROL.timingd = toc(CONTROL.timing);
set(handles.text_acq,'String',num2str(CONTROL.timingd));

popupmenuSequence_Callback(handles.popupmenuSequence, [], handles);






function SpawnMriCommand(handles,iAcq)
%% WRITE BATCH FILE AND RUN
seq = handles.SequenceClass;
set(handles.textProgramName,'string',sprintf('%s\\%s',seq.ProgramDir,seq.ProgramName));
[status,result] = seq.Run(iAcq);
if status == 0 && ~isempty(result)
    [pars,names,values,units] = seq.GetOutputParameters();
    FillParTable(handles.tableOutput,names,values,units,pars);
else
    ed = errordlg(result,'BatchFileError','modal');
    %    set(ed, 'WindowStyle', 'modal');
    uiwait(ed);
    runStop(handles);
end

% --- Outputs from this function are returned to the command line.
function varargout = mrigui_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure


varargout{1} = handles.output;

% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
file = uigetfile('*.fig');
if ~isequal(file, 0)
    open(file);
end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
    ['Close ' get(handles.figure1,'Name') '...'],...
    'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end
% close Phidget if it is open
if strcmp( handles.SequenceClass.SequenceName, 'Gradient Echo Single Sided')
    handles.SequenceClass = TurnOffPhidget( handles.SequenceClass.PhHandle );
end

delete(handles.figure1)

% --- Executes when entered data in editable cell(s) in tableInput.
function tableInput_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to tableInput (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)
global CONTROL
CONTROL.ParametersChanged = true;

display(eventdata)
seq = handles.SequenceClass;
[pars,~,~,units] = seq.GetInputParameters();
index = eventdata.Indices(1);
par = pars{index};
unit = units(par);
value = MyUnits.ConverToStandardUnits(eventdata.NewData,unit);
seq.SetParameter(par,value);
[pars,names,values,units] = seq.GetInputParameters();
FillParTable(handles.tableInput,names,values,units,pars);

% Update TE and TR values next to table 
% selectedSeq = MRIUtils.getCurrentPopupString(handles.popupmenuSequence);
seq.UpdateTETR;
set( handles.text_TE, 'String', num2str( (1E6) * seq.TE ));
set( handles.text_TR, 'String', num2str( (1E3) * seq.TR ));
set( handles.text_TTotal, 'String', num2str( seq.TTotal ));




% --- Executes during object creation, after setting all properties.
function FID_CreateFcn(hObject, eventdata, handles)
% hObject    handle to FID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate FID

% --- Executes during object creation, after setting all properties.
function textSequence_CreateFcn(hObject, eventdata, handles)
% hObject    handle to FID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% --- Executes on button press in pushbuttonStart.
function pushbuttonStart_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
runStart(handles);

% --- Executes on button press in pushbuttonStop.
function pushbuttonStop_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonStop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
runStop(handles);

function runStart(handles)
global rawData
rawData = [];
global CONTROL
CONTROL.Run = true;
set(handles.pushbuttonStart,'Enable','off');
set(handles.pushbuttonStop,'Enable','on');
set(handles.popupmenuSequence,'Enable','off');
CONTROL.timing = tic;

if strcmp( handles.SequenceClass.SequenceName, 'Gradient Echo Single Sided')
    start( handles.SequenceClass.PhHandle.timer );
end

RunMri(handles);

function runStop(handles)
global CONTROL
CONTROL.Run = false;
CONTROL.scanRun = false;
CONTROL.scanRun2D = false;
set(handles.pushbuttonStart,'Enable','on');
set(handles.pushbuttonStop,'Enable','off');
set(handles.popupmenuSequence,'Enable','on');
set(handles.checkboxAutoFreq,'Value',false);

if strcmp( handles.SequenceClass.SequenceName, 'Gradient Echo Single Sided')
    stop( handles.SequenceClass.PhHandle.timer );
end

% --- Executes on button press in checkboxSliceGrad.
function checkboxSliceGrad_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxSliceGrad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%

% Hint: get(hObject,'Value') returns toggle state of checkboxSliceGrad
% global CONTROL

seq = handles.SequenceClass;
seq.EnableSliceGradient(get(hObject,'Value'));
% guidata(hObject, handles);


% --- Executes on button press in checkboxPhaseGrad.
function checkboxPhaseGrad_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxPhaseGrad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxPhaseGrad
seq = handles.SequenceClass;
seq.EnablePhaseGradient(get(hObject,'Value'))


% --- Executes on button press in checkboxReadoutGrad.
function checkboxReadoutGrad_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxReadoutGrad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxReadoutGrad
seq = handles.SequenceClass;
seq.EnableReadoutGradient(get(hObject,'Value'))

% --- Executes on button press in checkboxSpoilerGrad.
function checkboxSpoilerGrad_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxSpoilerGrad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxSpoilerGrad
seq = handles.SequenceClass;
seq.EnableSpoilerGradient(get(hObject,'Value'))

% --- Executes on button press in checkboxSpoilerGrad.
function checkboxCalibration_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxCalibration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxSpoilerGrad
seq = handles.SequenceClass;
seq.EnableCalibration(get(hObject,'Value'))

% --- Executes on button press in checkboxAveraging.
function checkboxAveraging_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxAveraging (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxAveraging
global CONTROL
CONTROL.ParametersChanged = true;



function editAveraging_Callback(hObject, eventdata, handles)
% hObject    handle to editAveraging (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editAveraging as text
%        str2double(get(hObject,'String')) returns contents of editAveraging as a double
global CONTROL
CONTROL.ParametersChanged = true;


% --- Executes during object creation, after setting all properties.
function editAveraging_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editAveraging (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function sliderPhase_Callback(hObject, eventdata, handles)
% hObject    handle to sliderPhase (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global CONTROL
min = get(hObject,'Min');
max = get(hObject,'Max');
phase = (get(hObject,'Value')-min)/(max-min)*2*pi;
CONTROL.FIDPhase = phase;
set(handles.textFIDPhase,'string',num2str((phase)*180/pi));
seq = handles.SequenceClass;
seq.SetFIDPhase(phase);


function sliderPhase_RTCallback(hProp, eventdata)
global CONTROL
hObject = eventdata.AffectedObject;
min = get(hObject,'Min');
max = get(hObject,'Max');
phase = get(hObject,hProp.name);
phase = (phase-min)/(max-min)*2*pi;
CONTROL.FIDPhase = phase;

% --- Executes during object creation, after setting all properties.
function sliderPhase_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderPhase (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes when selected object is changed in uipanelSequence.
function uipanelSequence_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uipanelSequence
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbuttonSaveConfig.
function pushbuttonSaveConfig_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonSaveConfig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
seq = handles.SequenceClass;
[pars,names,values,units] = seq.GetInputParameters();
ini = IniConfig();
ini.AddSections({'Sequence', 'Acquisition','Analysis'})
ini.AddKeys('Sequence', 'Sequence Type',seq.SequenceName);
for i = 1:length(pars)
    par = pars{i};
    name = names(par);
    if ~isempty(units(par))
        name = sprintf('%s, %s',name,units(par));
    end
    ini.AddKeys('Acquisition', name,MyUnits.ConverFromStandardUnits(values(par),units(par)));
end

%ini.AddKeys('Analysis', 'Averaging',get(handles.editAveraging,'string'));
[FileName,PathName,FilterIndex] = uiputfile('*.cfg');
if ~FileName; return; end;
ini.WriteFile(fullfile(PathName,FileName));


% --- Executes on button press in pushbuttonLoadConfig.
function pushbuttonLoadConfig_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonLoadConfig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
seq = handles.SequenceClass;
ini = IniConfig();
[FileName,PathName,FilterIndex] = uigetfile('*.cfg');
if ~FileName; return; end;
ini.ReadFile(fullfile(PathName,FileName));

if ~strcmp(ini.GetValues('Sequence', 'Sequence Type'),seq.SequenceName)
    ed = errordlg('Sequence type mismatch in config file','ConfigFileError','modal');
    uiwait(ed);
    return;
end

[pars,names,values,units] = seq.GetInputParameters();
for i = 1:length(pars)
    par = pars{i};
    name = names(par);
    if ~isempty(units(par))
        name = sprintf('%s, %s',name,units(par));
    end
    value = ini.GetValues('Acquisition', name);
    if (~isempty(value))
        seq.SetParameter(par,MyUnits.ConverToStandardUnits(value,units(par)));
    end
end
[pars,names,values,units] = seq.GetInputParameters();
% values.values()
FillParTable(handles.tableInput,names,values,units,pars);

%
% CONTROL.RF_Shape = ini.GetValues('Acquisition', 'RF Pulse Shape');
%
% set(handles.editAveraging,'string',ini.GetValues('Analysis', 'Averaging'));
% set(handles.tableInput,'Data',tData);

% --- Executes on button press in pushbuttonDefaultConfig.
function pushbuttonDefaultConfig_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonDefaultConfig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SetDefaultParameters(handles);

% --------------------------------------------------------------------
function MeasureMenu_Callback(hObject, eventdata, handles)
% hObject    handle to MeasureMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function PulseTimeMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PulseTimeMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CONTROL
selectedSeq = MRIUtils.getCurrentPopupString(handles.popupmenuSequence);
if ~strcmp(selectedSeq,'GradientEcho3D') && ~strcmp(selectedSeq,'GradientEchoSlice') && ~strcmp(selectedSeq,'GradientEchoMultiSlice') &&  ~strcmp(selectedSeq,'GradientEcho3D_cal')...
        && ~strcmp(selectedSeq,'PrePulseMRI') && ~strcmp(selectedSeq,'GradientEcho3D_NScans')...
        && ~strcmp(selectedSeq,'PrePulse_MRI')&& ~strcmp(selectedSeq,'DiagonalSPRITE')...
        && ~strcmp(selectedSeq,'PrePulse_SliceMRI') && ~strcmp(selectedSeq,'InversionRecovery2')...
        && ~strcmp(selectedSeq,'CPMG2') && ~strcmp(selectedSeq,'CSSPRITE') && ~strcmp(selectedSeq,'CS SPRITE w ADC')...
        && ~strcmp(selectedSeq,'GradientEcho3D_withADC') && ~strcmp(selectedSeq,'SpinEchoSlice_withADC')...
        && ~strcmp(selectedSeq,'GE3D_Slice_ADC') && ~strcmp(selectedSeq,'SE3D_wSLICE_wADC') && ~strcmp(selectedSeq,'GE3D_Units')
    helpdlg('Please select a Gradient Echo sequence','Wrong Sequence')
else
    prompt = {'Number of Measurements','Start Pulse Time, us','End Pulse Time, us'};
    defAns = {'100','0.1','200'};
    res = inputdlg(prompt,'Scan Parameters',1,defAns);
    if ~isempty(res)
        seq = handles.SequenceClass;
        CONTROL.scanRun = true;
        CONTROL.scanPar = 'PULSETIME';
        CONTROL.scanParValues = linspace(str2num(res{2}),str2num(res{3}),str2num(res{1})).*MyUnits.us;
        seq.SetParameter(CONTROL.scanPar,CONTROL.scanParValues(1));
        selectedSequenceAnalysis = 'PulseTime_auto_analysis';
        if exist(selectedSequenceAnalysis,'class') == 8
            handles.SequenceAnalysisClass = eval(sprintf('%s(handles.SequenceClass,gcf,CONTROL.scanPar)',selectedSequenceAnalysis));
        else
            handles.SequenceAnalysisClass = SCAN_analysis(handles.SequenceClass,gcf,CONTROL.scanPar); %base analysis class
        end
        guidata(hObject, handles);
        UpdateParametersDisplay(handles);
        disp(handles.SequenceAnalysisClass.Name);
        runStart(handles);
    end
end

% --- Executes on button press in radiobuttonFFTReal.
function radiobuttonFFTReal_Callback(hObject, eventdata, handles)
% hObject    handle to radiobuttonFFTReal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobuttonFFTReal


% --- Executes on button press in radiobuttonFFTAbs.
function radiobuttonFFTAbs_Callback(hObject, eventdata, handles)
% hObject    handle to radiobuttonFFTAbs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobuttonFFTAbs


% --- Executes on button press in checkboxFFTMarkers.
function checkboxFFTMarkers_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxFFTMarkers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxFFTMarkers



function editPhaseN_Callback(hObject, eventdata, handles)
% hObject    handle to editPhaseN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editPhaseN as text
%        str2double(get(hObject,'String')) returns contents of editPhaseN as a double


% --- Executes during object creation, after setting all properties.
function editPhaseN_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editPhaseN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes when selected object is changed in uipanel12.
function uipanel12_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uipanel12
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
switch get(eventdata.NewValue,'Tag') % Get Tag of selected object.
    case 'radiobuttonSQUARE'
        RF_Shape = 0;
    case 'radiobuttonSINC'
        RF_Shape = 1;
end
seq = handles.SequenceClass;
seq.SetRFShape(RF_Shape)
% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in checkboxAutoFreq.
function checkboxAutoFreq_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxAutoFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxAutoFreq
global CONTROL
if get(hObject,'Value')
    set(handles.checkboxAveraging,'Value',false);
    CONTROL.ParametersChanged = true;
end

% --- Executes on button press in checkboxLoop.
function checkboxLoop_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxLoop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxLoop


% --- Executes on selection change in popupmenuSequence.
function popupmenuSequence_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuSequence (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuSequence contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuSequence
selectedSeq = MRIUtils.getCurrentPopupString(handles.popupmenuSequence);
if ~handles.SequenceClassList.isKey(selectedSeq);
    handles.SequenceClass = eval(selectedSeq);
    handles.SequenceClassList(selectedSeq) = handles.SequenceClass;
else
    handles.SequenceClass = handles.SequenceClassList(selectedSeq);
end
selectedSequenceAnalysis = sprintf('%s_analysis',selectedSeq);
if exist(selectedSequenceAnalysis,'class') == 8
    handles.SequenceAnalysisClass = eval(sprintf('%s(handles.SequenceClass,gcf)',selectedSequenceAnalysis));
else
    handles.SequenceAnalysisClass = MRI_analysis(handles.SequenceClass,gcf); %base analysis class
end
disp(handles.SequenceAnalysisClass.Name)
% Update handles structure
guidata(hObject, handles);
UpdateParametersDisplay(handles);


% --- Executes during object creation, after setting all properties.
function popupmenuSequence_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuSequence (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function T1MenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to T1MenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CONTROL
selectedSeq = MRIUtils.getCurrentPopupString(handles.popupmenuSequence);
if ~strcmp(selectedSeq,'InversionRecovery') && ~strcmp(selectedSeq,'InversionRecovery2')
    helpdlg('Please select Inversion Recovery sequence','Wrong Sequence')
else
    seq = handles.SequenceClass;
    prompt = {'Number of Measurements','Start Inversion Time, ms','End Inversion Time, ms','Total Repetition Time, ms'};
    repetitionDelayPar = 'REPETITIONDELAY';
    tr = seq.GetParameter(repetitionDelayPar);
    defAns = {'50','0.1',num2str(tr/MyUnits.ms/5,5),num2str(1.2*tr/MyUnits.ms,5)};
    res = inputdlg(prompt,'Scan Parameters',1,defAns);
    if ~isempty(res)
        CONTROL.scanRun = true;
        CONTROL.scanPar = 'INVERSIONTIME';
        %    CONTROL.scanNMeasurements = str2num(res{1});
        CONTROL.scanParValues = linspace(str2num(res{2}),str2num(res{3}),str2num(res{1})).*MyUnits.ms;
        seq.SetParameter(CONTROL.scanPar,CONTROL.scanParValues(1));
        seq.SetParameter(repetitionDelayPar,str2num(res{4}).*MyUnits.ms - CONTROL.scanParValues(1));
        selectedSequenceAnalysis = 'T1_analysis';
        if exist(selectedSequenceAnalysis,'class') == 8
            handles.SequenceAnalysisClass = eval(sprintf('%s(handles.SequenceClass,gcf,CONTROL.scanPar)',selectedSequenceAnalysis));
        else
            handles.SequenceAnalysisClass = SCAN_analysis(handles.SequenceClass,gcf,CONTROL.scanPar); %base analysis class
        end
        guidata(hObject, handles);
        UpdateParametersDisplay(handles);
        disp(handles.SequenceAnalysisClass.Name);
        runStart(handles);
    end
end


% --- Executes on button press in checkboxFFTCaption.
function checkboxFFTCaption_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxFFTCaption (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxFFTCaption


% --- Executes on button press in checkboxFIDCaption.
function checkboxFIDCaption_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxFIDCaption (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxFIDCaption


% --- Executes on button press in checkboxFIDMarkers.
function checkboxFIDMarkers_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxFIDMarkers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxFIDMarkers


% --------------------------------------------------------------------
function ShimmingMenu_Callback(hObject, eventdata, handles)
% hObject    handle to ShimmingMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- Executes on slider movement.
function sliderShimS_Callback(hObject, eventdata, handles)
% hObject    handle to sliderShimS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.editShimS,'String',sprintf('%.5f',get(hObject,'Value')));
editShimS_Callback(handles.editShimS, [], handles);
%     seq = handles.SequenceClass;
%     seq.SetShimS(get(hObject,'Value'));
%     set(handles.editShimS,'String',sprintf('%.4f',get(hObject,'Value')));


% --- Executes during object creation, after setting all properties.
function sliderShimS_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderShimS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
global scanner
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
set(hObject,'Value',scanner.defaultShim(3));


function editShimS_Callback(hObject, eventdata, handles)
% hObject    handle to editShimS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editShimS as text
%        str2double(get(hObject,'String')) returns contents of editShimS as a double
seq = handles.SequenceClass;
seq.SetShimS(str2double(get(hObject,'String')));
set(handles.sliderShimS,'Value',str2double(get(hObject,'String')));

% --- Executes during object creation, after setting all properties.
function editShimS_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editShimS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
global scanner

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject,'String',sprintf('%.5f',scanner.defaultShim(3)));

% --- Executes on slider movement.
function sliderShimP_Callback(hObject, eventdata, handles)
% hObject    handle to sliderShimP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.editShimP,'String',sprintf('%.5f',get(hObject,'Value')));
editShimP_Callback(handles.editShimP, [], handles);
%     seq = handles.SequenceClass;
%     seq.SetShimP(get(hObject,'Value'));
%     set(handles.editShimP,'String',sprintf('%.5f',get(hObject,'Value')));
%

% --- Executes during object creation, after setting all properties.
function sliderShimP_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderShimP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
global scanner
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
set(hObject,'Value',scanner.defaultShim(2));


% --- Executes on slider movement.
function sliderShimR_Callback(hObject, eventdata, handles)
% hObject    handle to sliderShimR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.editShimR,'String',sprintf('%.5f',get(hObject,'Value')));
editShimR_Callback(handles.editShimR, [], handles);
%     seq = handles.SequenceClass;
%     seq.SetShimR(get(hObject,'Value'));
%     set(handles.editShimR,'String',sprintf('%.4f',get(hObject,'Value')));


% --- Executes during object creation, after setting all properties.
function sliderShimR_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderShimR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
global scanner
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
set(hObject,'Value',scanner.defaultShim(1));


function editShimP_Callback(hObject, eventdata, handles)
% hObject    handle to editShimP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editShimP as text
%        str2double(get(hObject,'String')) returns contents of editShimP as a double
seq = handles.SequenceClass;
seq.SetShimP(str2double(get(hObject,'String')));
set(handles.sliderShimP,'Value',str2double(get(hObject,'String')));


% --- Executes during object creation, after setting all properties.
function editShimP_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editShimP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
global scanner
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject,'String',sprintf('%.5f',scanner.defaultShim(2)));




function editShimR_Callback(hObject, eventdata, handles)
% hObject    handle to editShimR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editShimR as text
%        str2double(get(hObject,'String')) returns contents of editShimR as a double
seq = handles.SequenceClass;
seq.SetShimR(str2double(get(hObject,'String')));
set(handles.sliderShimR,'Value',str2double(get(hObject,'String')));


% --- Executes during object creation, after setting all properties.
function editShimR_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editShimR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
global scanner
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject,'String',sprintf('%.5f',scanner.defaultShim(1)));



% --- Executes on button press in pushbuttonShimmingSave.
function pushbuttonShimmingSave_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonShimmingSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ini = IniConfig();
ini.AddSections({'Shimming'})
ini.AddKeys('Shimming', 'Slice Shimming',str2double(get(handles.editShimS,'String')));
ini.AddKeys('Shimming', 'Phase Shimming',str2double(get(handles.editShimP,'String')));
ini.AddKeys('Shimming', 'Readout Shimming',str2double(get(handles.editShimR,'String')));

[FileName,PathName,FilterIndex] = uiputfile('*.cfg');
if ~FileName; return; end;
ini.WriteFile(fullfile(PathName,FileName));


% --- Executes on button press in pushbuttonShimmingLoad.
function pushbuttonShimmingLoad_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonShimmingLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ini = IniConfig();
[FileName,PathName,FilterIndex] = uigetfile('*.cfg');
if ~FileName; return; end;
ini.ReadFile(fullfile(PathName,FileName));

if any(strcmp(ini.GetSections(),'Shimming'))
    ed = errordlg('Can''t find shimming section config file','ConfigFileError','modal');
    uiwait(ed);
    return;
end

value = ini.GetValues('Shimming', 'Slice Shimming');
if (~isempty(value))
    set(handles.editShimS,'String',sprintf('%.5f',value));
    editShimS_Callback(handles.editShimS, [], handles);
end
value = ini.GetValues('Shimming', 'Phase Shimming');
if (~isempty(value))
    set(handles.editShimP,'String',sprintf('%.5f',value));
    editShimP_Callback(handles.editShimP, [], handles);
end
value = ini.GetValues('Shimming', 'Readout Shimming');
if (~isempty(value))
    set(handles.editShimR,'String',sprintf('%.5f',value));
    editShimR_Callback(handles.editShimR, [], handles);
end


% --- Executes on button press in pushbuttonShimmingDefault.
function pushbuttonShimmingDefault_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonShimmingDefault (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global scanner
set(handles.editShimS,'String',sprintf('%.5f',scanner.defaultShim(3)));
editShimS_Callback(handles.editShimS, [], handles);
set(handles.editShimP,'String',sprintf('%.5f',scanner.defaultShim(2)));
editShimP_Callback(handles.editShimP, [], handles);
set(handles.editShimR,'String',sprintf('%.5f',scanner.defaultShim(1)));
editShimR_Callback(handles.editShimR, [], handles);


% --- Executes on button press in checkboxShimS.
function checkboxShimS_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxShimS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxShimS
seq = handles.SequenceClass;
seq.EnableShimmingSlice(get(hObject,'Value'));
shimOn = get(handles.checkboxShimS,'Value') | get(handles.checkboxShimP,'Value') | get(handles.checkboxShimR,'Value');
set(handles.checkboxShimmingOn,'Value',shimOn);


% --- Executes on button press in checkboxShimP.
function checkboxShimP_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxShimP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxShimP
seq = handles.SequenceClass;
seq.EnableShimmingPhase(get(hObject,'Value'));
shimOn = get(handles.checkboxShimS,'Value') | get(handles.checkboxShimP,'Value') | get(handles.checkboxShimR,'Value');
set(handles.checkboxShimmingOn,'Value',shimOn);


% --- Executes on button press in checkboxShimR.
function checkboxShimR_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxShimR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxShimR
seq = handles.SequenceClass;
seq.EnableShimmingReadout(get(hObject,'Value'));
shimOn = get(handles.checkboxShimS,'Value') | get(handles.checkboxShimP,'Value') | get(handles.checkboxShimR,'Value');
set(handles.checkboxShimmingOn,'Value',shimOn);


% --- Executes on button press in checkboxShimmingOn.
function checkboxShimmingOn_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxShimmingOn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxShimmingOn
set(handles.checkboxShimS,'Value',get(hObject,'Value'));
set(handles.checkboxShimP,'Value',get(hObject,'Value'));
set(handles.checkboxShimR,'Value',get(hObject,'Value'));
checkboxShimS_Callback(handles.checkboxShimS, [], handles);
checkboxShimP_Callback(handles.checkboxShimP, [], handles);
checkboxShimR_Callback(handles.checkboxShimR, [], handles);



% --------------------------------------------------------------------
function TRMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to TRMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CONTROL
selectedSeq = MRIUtils.getCurrentPopupString(handles.popupmenuSequence);
if ~strcmp(selectedSeq,'GradientEcho3D') && ~strcmp(selectedSeq,'GradientEchoSlice')...
        && ~strcmp(selectedSeq,'GradientEchoMultiSlice') && ~strcmp(selectedSeq,'GradientEcho3D_NScans')...
        && ~strcmp(selectedSeq,'DiagonalSPRITE') && ~strcmp(selectedSeq,'PrePulse_MRI')...
        && ~strcmp(selectedSeq,'PrePulse_SliceMRI')
    helpdlg('Please select a Gradient Echo sequence','Wrong Sequence')
else
    prompt = {'Number of Measurements','Start TR, ms','End TR, ms'};
    defAns = {'20','100','5000'};
    res = inputdlg(prompt,'Scan Parameters',1,defAns);
    if ~isempty(res)
        seq = handles.SequenceClass;
        CONTROL.scanRun = true;
        CONTROL.scanPar = 'REPETITIONDELAY';
        CONTROL.scanParValues = linspace(str2num(res{2}),str2num(res{3}),str2num(res{1})).*MyUnits.ms;
        seq.SetParameter(CONTROL.scanPar,CONTROL.scanParValues(1));
        selectedSequenceAnalysis = 'PulseTime_analysis';
        if exist(selectedSequenceAnalysis,'class') == 8
            handles.SequenceAnalysisClass = eval(sprintf('%s(handles.SequenceClass,gcf,CONTROL.scanPar)',selectedSequenceAnalysis));
        else
            handles.SequenceAnalysisClass = SCAN_analysis(handles.SequenceClass,gcf,CONTROL.scanPar); %base analysis class
        end
        guidata(hObject, handles);
        UpdateParametersDisplay(handles);
        disp(handles.SequenceAnalysisClass.Name);
        runStart(handles);
    end
end


% --------------------------------------------------------------------
function ShimmingSPMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to ShimmingSPMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CONTROL
selectedSeq = MRIUtils.getCurrentPopupString(handles.popupmenuSequence);
if ~strcmp(selectedSeq,'GradientEcho3D') && ~strcmp(selectedSeq,'GradientEchoSlice')...
        && ~strcmp(selectedSeq,'GradientEchoMultiSlice' )&& ~strcmp(selectedSeq,'GE3D_EPI')...
        && ~strcmp(selectedSeq,'GradientEcho3D_NScans')&& ~strcmp(selectedSeq,'PrePulse_MRI')...
        && ~strcmp(selectedSeq,'DiagonalSPRITE')&& ~strcmp(selectedSeq,'PrePulse_SliceMRI')...
        && ~strcmp(selectedSeq,'GE3D_Slice_ADC')
    helpdlg('Please select a Gradient Echo sequence','Wrong Sequence')
else
    prompt = {'Number of Measurements Slice',...
        'Start Slice Shim Amp','End Slice Shim Amp',...
        'Number of Measurements Phase',...
        'Start Phase Shim Amp','End Phase Shim Amp','Readout Amplitude'};
    shimS = str2double(get(handles.editShimS,'String'));
    shimP = str2double(get(handles.editShimP,'String'));
    shimR = str2double(get(handles.editShimR,'String'));
    defAns = {'12',num2str(shimS - 2e-3,5),num2str(shimS + 2e-3,5),'10',num2str(shimP - 2e-3,5),num2str(shimP + 2e-3,5),num2str(shimR,5)};
    res = inputdlg(prompt,'Scan Parameters',1,defAns);
    if ~isempty(res)
        seq = handles.SequenceClass;
        CONTROL.scanRun2D = true;
        CONTROL.scanPar = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT'};
        %    CONTROL.scanNMeasurements = str2num(res{1});
        [CONTROL.scanPar1Values,CONTROL.scanPar2Values] = ndgrid(linspace(str2num(res{2}),str2num(res{3}),str2num(res{1})),linspace(str2num(res{5}),str2num(res{6}),str2num(res{4})));
        seq.SetParameter(CONTROL.scanPar{1},CONTROL.scanPar1Values(1,1));
        seq.SetParameter(CONTROL.scanPar{2},CONTROL.scanPar2Values(1,1));
        seq.SetParameter(CONTROL.scanPar{3},str2num(res{7}));
        selectedSequenceAnalysis = 'Shimming_analysis';
        if exist(selectedSequenceAnalysis,'class') == 8
            handles.SequenceAnalysisClass = eval(sprintf('%s(handles.SequenceClass,gcf,CONTROL.scanPar)',selectedSequenceAnalysis));
        else
            handles.SequenceAnalysisClass = SCAN2D_analysis(handles.SequenceClass,gcf,CONTROL.scanPar); %base analysis class
        end
        guidata(hObject, handles);
        UpdateParametersDisplay(handles);
        disp(handles.SequenceAnalysisClass.Name);
        runStart(handles);
    end
end

% --------------------------------------------------------------------
function ShimmingPRMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to ShimmingPRMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CONTROL
selectedSeq = MRIUtils.getCurrentPopupString(handles.popupmenuSequence);
if ~strcmp(selectedSeq,'GradientEcho3D') && ~strcmp(selectedSeq,'GE3D_EPI')...
        && ~strcmp(selectedSeq,'GradientEchoSlice') && ~strcmp(selectedSeq,'GradientEchoMultiSlice')...
        && ~strcmp(selectedSeq,'GradientEcho3D_NScans')&& ~strcmp(selectedSeq,'PrePulse_MRI')...
        && ~strcmp(selectedSeq,'DiagonalSPRITE')&& ~strcmp(selectedSeq,'PrePulse_SliceMRI')...
        && ~strcmp(selectedSeq,'GE3D_Slice_ADC')
    helpdlg('Please select a Gradient Echo sequence','Wrong Sequence')
else
    prompt = {'Number of Measurements Phase',...
        'Start Phase Shim Amp','End Phase Shim Amp',...
        'Number of Measurements Readout',...
        'Start Readout Shim Amp','End Readout Shim Amp','Slice Amplitude'};
    shimS = str2double(get(handles.editShimS,'String'));
    shimP = str2double(get(handles.editShimP,'String'));
    shimR = str2double(get(handles.editShimR,'String'));
    defAns = {'12',num2str(shimP - 2e-3,5),num2str(shimP + 2e-3,5),'10',num2str(shimR - 2e-3,5),num2str(shimR + 2e-3,5),num2str(shimS,5)};
    res = inputdlg(prompt,'Scan Parameters',1,defAns);
    if ~isempty(res)
        seq = handles.SequenceClass;
        CONTROL.scanRun2D = true;
        CONTROL.scanPar = {'SHIMPHASE','SHIMREADOUT','SHIMSLICE'};
        %    CONTROL.scanNMeasurements = str2num(res{1});
        [CONTROL.scanPar1Values,CONTROL.scanPar2Values] = ndgrid(linspace(str2num(res{2}),str2num(res{3}),str2num(res{1})),linspace(str2num(res{5}),str2num(res{6}),str2num(res{4})));
        seq.SetParameter(CONTROL.scanPar{1},CONTROL.scanPar1Values(1,1));
        seq.SetParameter(CONTROL.scanPar{2},CONTROL.scanPar2Values(1,1));
        seq.SetParameter(CONTROL.scanPar{3},str2num(res{7}));
        selectedSequenceAnalysis = 'Shimming_analysis';
        if exist(selectedSequenceAnalysis,'class') == 8
            handles.SequenceAnalysisClass = eval(sprintf('%s(handles.SequenceClass,gcf,CONTROL.scanPar)',selectedSequenceAnalysis));
        else
            handles.SequenceAnalysisClass = SCAN2D_analysis(handles.SequenceClass,gcf,CONTROL.scanPar); %base analysis class
        end
        guidata(hObject, handles);
        UpdateParametersDisplay(handles);
        disp(handles.SequenceAnalysisClass.Name);
        runStart(handles);
    end
end

% --------------------------------------------------------------------
function ShimmingSRMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to ShimmingSRMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CONTROL
selectedSeq = MRIUtils.getCurrentPopupString(handles.popupmenuSequence);
if ~strcmp(selectedSeq,'GradientEcho3D') && ~strcmp(selectedSeq,'GE3D_EPI') && ~strcmp(selectedSeq,'GradientEchoSlice') && ~strcmp(selectedSeq,'GradientEchoMultiSlice')...
        && ~strcmp(selectedSeq,'GradientEcho3D_NScans')&& ~strcmp(selectedSeq,'PrePulse_MRI')&& ~strcmp(selectedSeq,'DiagonalSPRITE')...
        && ~strcmp(selectedSeq,'PrePulse_SliceMRI')...
        && ~strcmp(selectedSeq,'GE3D_Slice_ADC')
    helpdlg('Please select a Gradient Echo sequence','Wrong Sequence')
else
    prompt = {'Number of Measurements Slice',...
        'Start Slice Shim Amp','End Slice Shim Amp',...
        'Number of Measurements Readout',...
        'Start Readout Shim Amp','End Readout Shim Amp','Phase Amplitude'};
    shimS = str2double(get(handles.editShimS,'String'));
    shimP = str2double(get(handles.editShimP,'String'));
    shimR = str2double(get(handles.editShimR,'String'));
    defAns = {'12',num2str(shimS - 2e-3,5),num2str(shimS + 2e-3,5),'10',num2str(shimR - 2e-3,5),num2str(shimR + 2e-3,5),num2str(shimP,5)};
    res = inputdlg(prompt,'Scan Parameters',1,defAns);
    if ~isempty(res)
        seq = handles.SequenceClass;
        CONTROL.scanRun2D = true;
        CONTROL.scanPar = {'SHIMSLICE','SHIMREADOUT','SHIMPHASE'};
        %    CONTROL.scanNMeasurements = str2num(res{1});
        [CONTROL.scanPar1Values,CONTROL.scanPar2Values] = ndgrid(linspace(str2num(res{2}),str2num(res{3}),str2num(res{1})),linspace(str2num(res{5}),str2num(res{6}),str2num(res{4})));
        seq.SetParameter(CONTROL.scanPar{1},CONTROL.scanPar1Values(1,1));
        seq.SetParameter(CONTROL.scanPar{2},CONTROL.scanPar2Values(1,1));
        seq.SetParameter(CONTROL.scanPar{3},str2num(res{7}));
        selectedSequenceAnalysis = 'Shimming_analysis';
        if exist(selectedSequenceAnalysis,'class') == 8
            handles.SequenceAnalysisClass = eval(sprintf('%s(handles.SequenceClass,gcf,CONTROL.scanPar)',selectedSequenceAnalysis));
        else
            handles.SequenceAnalysisClass = SCAN2D_analysis(handles.SequenceClass,gcf,CONTROL.scanPar); %base analysis class
        end
        guidata(hObject, handles);
        UpdateParametersDisplay(handles);
        disp(handles.SequenceAnalysisClass.Name);
        runStart(handles);
    end
end


% --------------------------------------------------------------------
function Shimming3DMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to Shimming3DMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in checkboxAutoPhase.
function checkboxAutoPhase_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxAutoPhase (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxAutoPhase
global CONTROL
CONTROL.ParametersChanged = true;


% --------------------------------------------------------------------
function AnalysisMenu_Callback(hObject, eventdata, handles)
% hObject    handle to AnalysisMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function PhaseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PhaseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ana = Phase_analysis(handles.SequenceClass,gcf);
figs = ana.FigureRequest;
if ~isempty(figs)
    scrsz = get(0,'ScreenSize');
    for i=1:size(figs,1)
        height = (scrsz(4)-100)*figs(i,2);
        width = height*figs(i,1);
        CONTROL.runFig(i) = figure('OuterPosition',[10 50 width height]);
    end
    ana.SetFigureHandle(CONTROL.runFig);
    ana.Execute();
end


% --- Executes on selection change in popupmenuPlots.
function popupmenuPlots_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuPlots (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuPlots contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuPlots
selectedPlot = MRIUtils.getCurrentPopupString(hObject);
seqA = handles.SequenceAnalysisClass;
seqA.SelectPlot(selectedPlot);
seqA.ResetPlots();

% --- Executes during object creation, after setting all properties.
function popupmenuPlots_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuPlots (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% --------------------------------------------------------------------
%function CurrentCalibrationMenuItem_Callback(hObject, eventdata, handles)
%% hObject    handle to CurrentCalibrationMenuItem (see GCBO)
%% eventdata  reserved - to be defined in a future version of MATLAB
%% handles    structure with handles and user data (see GUIDATA)
%global CONTROL
%selectedSeq = MRIUtils.getCurrentPopupString(handles.popupmenuSequence);
%if ~strcmp(selectedSeq,'GradientEcho')
%    helpdlg('Please select Gradient Echo sequence','Wrong Sequence')
%else
%    s = listdlg('PromptString','Which gradients do you wish to calibrate?',...
%                'SelectionMode','multiple',...
%                'ListString',{'Readout','Phase','Slice'});
%    if ~isempty(s)
%        numgrads = numel(s);
%        seq = handles.SequenceClass;
%        switch numgrads
%            case 1
%                % for only one gradient calibration
%                seq.SetParameter('NSCANS',1);
%                seq.SetParameter('NSLICES',1);
%                seq.SetParameter('NPHASES',1);
%                selectedSequenceAnalysis = 'Gradient_calibration';
%
%                if exist(selectedSequenceAnalysis, 'class') == 8
%                    handles.SequenceCalibrationClass = eval(sprintf('%s(handles.SequenceClass,gcf,CONTROL.scanPar)',selectedSequenceAnalysis));
%                else
%                    handles.SequenceCalibrationClass = CALIBRATION_analysis(handles.SequenceClass,gcf,CONTROL.scanPar); %base analysis class
%                end
%
%            case 2
%                % for two gradient calibrations
%            case 3
%                % for all three calibrations
%        end
%
%        guidata(hObject, handles);
%        UpdateParametersDisplay(handles);
%        disp(handles.SequenceAnalysisClass.Name);
%        runStart(handles);
%
%    end
%end


% --- Executes on selection change in popupmenuOrientation.
function popupmenuOrientation_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuOrientation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuOrientation contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuOrientation
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuSequence contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuSequence
global MRIDATA
selectedOri = MRIUtils.getCurrentPopupString(handles.popupmenuOrientation);
MRIDATA.Quaternion = eval(selectedOri);

% Update handles structure
guidata(hObject, handles);
UpdateParametersDisplay(handles);



% --- Executes during object creation, after setting all properties.
function popupmenuOrientation_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuOrientation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu6.
function popupmenu6_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu6 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu6


% --- Executes during object creation, after setting all properties.
function popupmenu6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% % --- Executes on button press in checkboxCalImag.
% function checkboxCalImag_Callback(hObject, eventdata, handles)
% % hObject    handle to checkboxCalImag (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hint: get(hObject,'Value') returns toggle state of checkboxCalImag
% seq = handles.SequenceClass;
% seq.CalibrateImage(get(hObject,'Value'));

% --- Executes during object creation, after setting all properties.
function checkboxCalImag_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editAveraging (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in pushbuttonReload.
function pushbuttonReload_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonReload (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

results_file=fullfile(fileparts(mfilename('fullpath')),'FileDir','results.mat');
load(results_file)
ana=obj;

ana.SetGUIFigureHandles(handles.FID,handles.FFT);
ana.SetGUICaptionHandles(handles.textCaptionFID,handles.textCaptionFFT);

selectedPlot = MRIUtils.getCurrentPopupString(handles.popupmenuPlots);
ana.SelectPlot(selectedPlot);
ana.ResetPlots();

selectedSeq = MRIUtils.getCurrentPopupString(handles.popupmenuSequence);
if strcmp(selectedSeq,'GradientEcho3D_blank_cal')
    seq.CalibrateImage(get(handles.checkboxCalImag,'Value'));
end

ana.EnableFIDMarkers(get(handles.checkboxFIDMarkers,'Value'));
ana.EnableFIDCaption(get(handles.checkboxFIDCaption,'Value'));
ana.EnableFFTMarkers(get(handles.checkboxFFTMarkers,'Value'));
ana.EnableFFTCaption(get(handles.checkboxFFTCaption,'Value'));
ana.SetAbsFID(get(handles.radiobuttonFIDAbs,'Value'));
ana.SetAbsFFT(get(handles.radiobuttonFFTAbs,'Value'));

ana.Execute();

% seq.ReloadImage(get(hObject,'Value'));


% --- Executes on button press in checkboxPerformCal.
function checkboxPerformCal_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxPerformCal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxPerformCal

seq = handles.SequenceClass;

if get(hObject,'Value')
    seq.calibrationactive = 1;
else
    seq.calibrationactive = 0;
end

% seq.EnableCalibration(get(hObject,'Value'));
% if get(hObject,'Value')
%     set(handles.checkboxCalImag,'Enable','on');
%     set(handles.checkboxCalImag,'Value',get(hObject,'Value'));  %   by default, when the k-space calibration is performed,
%     %   we want to see the calibrated image, although not seeing it is also an available option
% else
%     set(handles.checkboxCalImag,'Value',get(hObject,'Value'));  %if the calibration is not performed, the image can't be calibrated
%     set(handles.checkboxCalImag,'Enable','off');
% end

function editInstrFin_Callback(hObject, eventdata, handles)
% hObject    handle to editInstrFin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editInstrFin as text
%        str2double(get(hObject,'String')) returns contents of editInstrFin as a double

% --- Executes during object creation, after setting all properties.
function editInstrFin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editInstrFin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbuttonPlotSequence.
function pushbuttonPlotSequence_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonPlotSequence (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CONTROL

ninstr_ini = str2double(get(handles.editInstrIni,'string'));
ninstr_fin = str2double(get(handles.editInstrFin,'string'));
if (ninstr_ini<1 || ninstr_ini~=round(ninstr_ini) || ninstr_fin~=round(ninstr_fin))
    ed = errordlg('Range of instructions to be represented is not correct','SeqRepresError','modal');
    uiwait(ed);
    return;
end
seq = handles.SequenceClass;
if (ninstr_fin>length(seq.AMPs))
    ed = errordlg(strcat('Range of instructions to be represented exceeds sequence extents (1-',num2str(length(seq.AMPs)),')'),'SeqRepresError','modal');
    uiwait(ed);
    return;
end

if isfield(CONTROL,'figSeq') && ishandle(CONTROL.figSeq)
    close(CONTROL.figSeq);
end
CONTROL.figSeq = figure;
seq.SetFigureHandle(CONTROL.figSeq);
    
seq.plot_diagram(ninstr_ini,ninstr_fin);


% --- Executes during object creation, after setting all properties.
function tableInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tableInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



function editInstrIni_Callback(hObject, eventdata, handles)
% hObject    handle to editInstrIni (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editInstrIni as text
%        str2double(get(hObject,'String')) returns contents of editInstrIni as a double


% --- Executes during object creation, after setting all properties.
function editInstrIni_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editInstrIni (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --------------------------------------------------------------------
function T1Automatic_Callback(hObject, eventdata, handles)
% hObject    handle to T1Automatic (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CONTROL
selectedSeq = MRIUtils.getCurrentPopupString(handles.popupmenuSequence);
if ~strcmp(selectedSeq,'InversionRecovery2')
    helpdlg('Please select Inversion Recovery sequence','Wrong Sequence')
else
    
    load gong.mat;
    
    % first grab the sequence
    seq = handles.SequenceClass;
    selectedSequenceAnalysis = 'T1_analysis';
    % ensure sequence has the proper inversion turned off
    oldinversion = seq.GetParameter('INVERSIONON');
    seq.SetParameter('INVERSIONON',0);
      
    
    % --- CENTER THE PULSE
    seq.CreateSequence(1); % Create the gradient echo sequence for CF
    seq.Sequence2String;
    fname_CF = 'TempBatch_CF.bat';
    seq.WriteBatchFile(fname_CF);
    seq.CenterFrequency(fname_CF);
    seq.SetParameter('LASTFREQUENCY',seq.GetParameter('FREQUENCY'));
    
    % --- TEST THE PULSE ANGLE
    CONTROL.scanRun = true;
    CONTROL.scanPar = 'PULSETIME';
    CONTROL.scanParValues = linspace(1,160,40).*MyUnits.us;
    seq.SetParameter(CONTROL.scanPar,CONTROL.scanParValues(1));
    selectedSequenceAnalysis = 'PulseTime_auto_analysis';
    if exist(selectedSequenceAnalysis,'class') == 8
        handles.SequenceAnalysisClass = eval(sprintf('%s(handles.SequenceClass,gcf,CONTROL.scanPar)',selectedSequenceAnalysis));
    else
        handles.SequenceAnalysisClass = SCAN_analysis(handles.SequenceClass,gcf,CONTROL.scanPar); %base analysis class
    end
    guidata(hObject, handles);
    UpdateParametersDisplay(handles);
    disp(handles.SequenceAnalysisClass.Name);
    runStart(handles);
    
    sound(y);
    h = warndlg('Please shake up sample.','!!ACTION REQUIRED!!');
    uiwait(h);
    
    % --- CENTER THE PULSE
    seq.CreateSequence(1); % Create the gradient echo sequence for CF
    seq.Sequence2String;
    fname_CF = 'TempBatch_CF.bat';
    seq.WriteBatchFile(fname_CF);
    seq.CenterFrequency(fname_CF);
    seq.SetParameter('LASTFREQUENCY',seq.GetParameter('FREQUENCY'));
    
    % --- REP DELAY TEST
    % now determine the proper repitition delay 
    repdelays = [repmat(1*MyUnits.us,1,20), logspace( log10(1*MyUnits.us), log10(10), 40 )];
    repetitionDelayPar = 'REPETITIONDELAY';
    CONTROL.scanRun = true;
    CONTROL.scanPar = repetitionDelayPar;
    CONTROL.scanParValues = repdelays;
    seq.SetParameter(CONTROL.scanPar,CONTROL.scanParValues(1));
    
    if exist(selectedSequenceAnalysis,'class') == 8
        handles.SequenceAnalysisClass = eval(sprintf('%s(handles.SequenceClass,gcf,CONTROL.scanPar)',selectedSequenceAnalysis));
    else
        handles.SequenceAnalysisClass = SCAN_analysis(handles.SequenceClass,gcf,CONTROL.scanPar); %base analysis class
    end
    guidata(hObject, handles);
    UpdateParametersDisplay(handles);
    disp(handles.SequenceAnalysisClass.Name);
    runStart(handles);
    
    sound(y);
    h = warndlg('Please shake up sample.','!!ACTION REQUIRED!!');
    uiwait(h);
        
    % --- CENTER THE PULSE
    seq.CreateSequence(1); % Create the gradient echo sequence for CF
    seq.Sequence2String;
    fname_CF = 'TempBatch_CF.bat';
    seq.WriteBatchFile(fname_CF);
    seq.CenterFrequency(fname_CF);
    seq.SetParameter('LASTFREQUENCY',seq.GetParameter('FREQUENCY'));
    
    % --- INVERSION SEQUENCE TEST
    % now that we have set the rep delay grab this
    repetitionDelayPar = 'REPETITIONDELAY';
    tr = seq.GetParameter(repetitionDelayPar);
    % set the inversion parameter
    seq.SetParameter('INVERSIONON',1);
    % set the inversion times
    inversiontimes = logspace( log10(tr/50), log10(tr*2), 40 );
    
    CONTROL.scanRun = true;
    CONTROL.scanPar = 'INVERSIONTIME';
    CONTROL.scanParValues = inversiontimes;
    seq.SetParameter(CONTROL.scanPar,CONTROL.scanParValues(1));
    selectedSequenceAnalysis = 'T1_analysis';
    
    if exist(selectedSequenceAnalysis,'class') == 8
        handles.SequenceAnalysisClass = eval(sprintf('%s(handles.SequenceClass,gcf,CONTROL.scanPar)',selectedSequenceAnalysis));
    else
        handles.SequenceAnalysisClass = SCAN_analysis(handles.SequenceClass,gcf,CONTROL.scanPar); %base analysis class
    end
    guidata(hObject, handles);
    UpdateParametersDisplay(handles);
    disp(handles.SequenceAnalysisClass.Name);
    runStart(handles);

    % switch the inversion back
    seq.SetParameter('INVERSIONON',oldinversion);
end


% --------------------------------------------------------------------
function CFNMR_Callback(hObject, eventdata, handles)
% hObject    handle to CFNMR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CONTROL
warning off
selectedSeq = MRIUtils.getCurrentPopupString(handles.popupmenuSequence);
if ~strcmp(selectedSeq,'GradientEcho3D') ...
    && ~strcmp(selectedSeq,'GE3D_Slice_ADC') && ~strcmp(selectedSeq,'SE3D_wSLICE_wADC')
    helpdlg('Please select a the correct sequence','Wrong Sequence')
else
    prompt = {'Number of Measurements','Start Frequency Offset, kHz','End Frequency Offset, us','Initial Repeats'};
    defAns = {'100','-25','25','50'};
    res = inputdlg(prompt,'Scan Parameters',1,defAns);
    if ~isempty(res)
        seq = handles.SequenceClass;
        CONTROL.scanRun = true;
        CONTROL.scanPar = 'FREQUENCY';
        CONTROL.skip  = str2num(res{4});
        priorfreq = seq.GetParameter('FREQUENCY');
        CONTROL.scanParValues = priorfreq +[zeros(1,CONTROL.skip), linspace(str2num(res{2}),str2num(res{3}),str2num(res{1})).*MyUnits.kHz];
        seq.SetParameter(CONTROL.scanPar,CONTROL.scanParValues(1));
        selectedSequenceAnalysis = 'CFNMR_analysis';
        if exist(selectedSequenceAnalysis,'class') == 8
            handles.SequenceAnalysisClass = eval(sprintf('%s(handles.SequenceClass,gcf,CONTROL.scanPar)',selectedSequenceAnalysis));
        else
            handles.SequenceAnalysisClass = SCAN_analysis(handles.SequenceClass,gcf,CONTROL.scanPar); %base analysis class
        end
        guidata(hObject, handles);
        UpdateParametersDisplay(handles);
        disp(handles.SequenceAnalysisClass.Name);
        runStart(handles);
    end
    seq.SetParameter('FREQUENCY',priorfreq);
end
warning on


% --------------------------------------------------------------------
function slicerephase_Callback(hObject, eventdata, handles)
% hObject    handle to slicerephase (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CONTROL
selectedSeq = MRIUtils.getCurrentPopupString(handles.popupmenuSequence);
if ~strcmp(selectedSeq,'GE3D_Slice_ADC') && ~strcmp(selectedSeq,'SE3D_wSLICE_wADC')
    helpdlg('Please select a the correct sequence','Wrong Sequence')
else
    prompt = {'Number of Measurements','Start Rephase Ratio','End Rephase Ratio'};
    defAns = {'100','0.1','0.7'};
    res = inputdlg(prompt,'Scan Parameters',1,defAns);
    if ~isempty(res)
        seq = handles.SequenceClass;
        CONTROL.scanRun = true;
        CONTROL.scanPar = 'SLICEREPHASERATIO';
        CONTROL.scanParValues = linspace(str2double(res{2}),str2double(res{3}),str2double(res{1}) );
        seq.SetParameter(CONTROL.scanPar,CONTROL.scanParValues(1));
        selectedSequenceAnalysis = 'SliceRephase_auto_analysis';
        if exist(selectedSequenceAnalysis,'class') == 8
            handles.SequenceAnalysisClass = eval(sprintf('%s(handles.SequenceClass,gcf,CONTROL.scanPar)',selectedSequenceAnalysis));
        else
            handles.SequenceAnalysisClass = SCAN_analysis(handles.SequenceClass,gcf,CONTROL.scanPar); %base analysis class
        end
        guidata(hObject, handles);
        UpdateParametersDisplay(handles);
        disp(handles.SequenceAnalysisClass.Name);
        runStart(handles);
    end
end


% --------------------------------------------------------------------
function CIST_Callback(hObject, eventdata, handles)
% hObject    handle to CIST (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CONTROL
warning off
selectedSeq = MRIUtils.getCurrentPopupString(handles.popupmenuSequence);
if ~strcmp(selectedSeq,'GradientEcho3D_Sat')
    helpdlg('Please select a the correct sequence','Wrong Sequence')
else
    prompt = {'Number of Measurements','Start Frequency Offset, kHz','End Frequency Offset, kHz','Initial Repeats'};
    defAns = {'100','-25','25','50'};
    res = inputdlg(prompt,'Scan Parameters',1,defAns);
    if ~isempty(res)
        seq = handles.SequenceClass;
        CONTROL.scanRun = true;
        CONTROL.scanPar = 'SFOFFSET';
        CONTROL.skip  = str2num(res{4});
        priorfreq = seq.GetParameter('SFOFFSET');
        CONTROL.scanParValues = priorfreq +[zeros(1,CONTROL.skip), linspace(str2num(res{2}),str2num(res{3}),str2num(res{1})).*MyUnits.kHz];
        seq.SetParameter(CONTROL.scanPar,CONTROL.scanParValues(1));
        selectedSequenceAnalysis = 'CIST_analysis';
        if exist(selectedSequenceAnalysis,'class') == 8
            handles.SequenceAnalysisClass = eval(sprintf('%s(handles.SequenceClass,gcf,CONTROL.scanPar)',selectedSequenceAnalysis));
        else
            handles.SequenceAnalysisClass = SCAN_analysis(handles.SequenceClass,gcf,CONTROL.scanPar); %base analysis class
        end
        guidata(hObject, handles);
        UpdateParametersDisplay(handles);
        disp(handles.SequenceAnalysisClass.Name);
        runStart(handles);
    end
    seq.SetParameter('SFOFFSET',priorfreq);
end
warning on


function editFiltering_Callback(hObject, eventdata, handles)
% hObject    handle to editFiltering (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editFiltering as text
%        str2double(get(hObject,'String')) returns contents of editFiltering as a double
global CONTROL
CONTROL.ParametersChanged = true;

% --- Executes during object creation, after setting all properties.
function editFiltering_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editFiltering (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkboxFiltering.
function checkboxFiltering_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxFiltering (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxFiltering
global CONTROL
CONTROL.ParametersChanged = true;


% --- Executes on button press in pushbuttonLoadScanner.
function pushbuttonLoadScanner_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonLoadScanner (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear global scanner

global scanner

try
    [FileName,PathName,FilterIndex] = uigetfile('*.mat');
catch
    warndlg('Please, select a valid Scanner Configuration File (.mat)','Scanner configuration error')
end

lastwarn('');
load(fullfile(PathName,FileName),'scanner');
[~,warnID] = lastwarn;
if strcmp(warnID,'MATLAB:load:variableNotFound')
    warndlg('Please, select a valid Scanner Configuration File (.mat): scanner struct not present in the selected .mat','Scanner configuration error')
else
    set(handles.textScanner,'string',sprintf('%s',scanner.name));
end
seq = handles.SequenceClass;
seq.SetDefaultParameters();
UpdateParametersDisplay(handles);


