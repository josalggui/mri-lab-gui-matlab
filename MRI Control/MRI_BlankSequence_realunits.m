classdef MRI_BlankSequence_realunits < handle
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    %  Elena Diaz Caballero
    
    properties
        cData;
        fidData;
        fftData1D;
        timeVector;
        freqVector;
        ProgramDir = 'C:\SpinCore\MRI_SpinAPI\examples';
        path_file;         
        ProgramName;
        SequenceName;
        OutputFilename;
        OutputFilename_CF;
        InputParameters;
        InputParNames;
        InputParValues;
        InputParValuesMin;
        InputParValuesMax;
        InputParUnits;
        InputParHidden; %this parameters are not shown in the input table
        OutputParameters;
        OutputParNames;
        OutputParValues;
        OutputParUnits;
        RF_Shape = 0;
        tx_phase = 0;
        blankingBit = 0;
        adcOffset = 47; % SPINCORE 2019.09.12
        freq_offset = 0;
        slice_en = 0;
        phase_en = 0;
        readout_en = 0;
        spoiler_en = 0;
        shimS_en = 0;
        shimP_en = 0;
        shimR_en = 0;
        RunCal = false;
        calibration_en = false;
        CalCheck = false;
        FIDPhase = 0;
        NAverages = 10;
        averaging = 0;
        autoPhasing = 0;
        AbortRun = false;
        ReferenceCF = 0;
        MaxCF_shift=20*MyUnits.kHz;
        AMPs =[];
        AMPs_string;
        DACs=[];
        DACs_string;
        WRITEs=[];
        WRITEs_string;
        UPDATEs=[];
        UPDATEs_string;
        CLEARs=[];
        CLEARs_string;
        FREQs=[];
        FREQs_string;
        TXs=[];
        TXs_string;
        PHASEs=[];
        PHASEs_string;
        PhResets=[];
        PhResets_string;
        RXs=[];
        RXs_string;
        ENVELOPEs=[];
        ENVELOPEs_string;
        FLAGs=[];
        FLAGs_string;
        OPCODEs=[];
        OPCODEs_string;
        DELAYs=[];
        DELAYs_string;
        maxInstructions = 2000;
        nInstructions;
        nline_reads;
        Kspace_readout;
        KSpace_phase;
        selectReadout;
        selectPhase;
        selectSlice;
        %% Unused
        %kwid= 3;
        %overg= 6;
        %DCF = [];
    end
    
    methods
        function obj = MRI_BlankSequence_realunits
            obj.InputParameters = {...
                'SHIMSLICE','SHIMPHASE','SHIMREADOUT','NSCANS',...
                'NPOINTS','FREQUENCY','SPECTRALWIDTH',...
                'RFAMPLITUDE','PULSETIME',...
                'READOUT','PHASE','SLICE',...
                'SPOILERAMP', 'SPOILERTIME'...
                };
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT'}; %these pars will not be shown in a par table
            obj.InputParNames = containers.Map(obj.InputParameters,...
                {...
                'Slice Shimming','Phase Shimming','Readout Shimming','Number of Scans',...
                'Number of Points', 'Frequency', 'SpectralWidth',...
                'RF Amplitude','Pulse Time',...
                'Readout Axis','Phase Axis', 'Slice Axis',...
                'Spoiler Amplitude', 'Spoiler Time'...
                });
            obj.InputParValues = containers.Map(obj.InputParameters,...
                {...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,...
                '','','',...
                NaN,NaN...
                });
            obj.InputParValuesMin = containers.Map(obj.InputParameters,...
                {...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,...
                '','','',...
                NaN,NaN...
                });
            obj.InputParValuesMax = containers.Map(obj.InputParameters,...
                {...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,...
                '','','',...
                NaN,NaN...
                });
            obj.InputParUnits = containers.Map(obj.InputParameters,...
                {...
                '','','','',...
                '','MHz','kHz',...
                '','us',...
                '','','',...
                '','us'...
                });
            obj.OutputParameters = {
                'ADCDECIMATION','ACTUALSPECTRALWIDTH','ACQUISITIONTIME','LASTFREQUENCY','SNRMEANVALUE'    
                };
            obj.OutputParNames = containers.Map(obj.OutputParameters,...
                {...
                'ADC Frequency Decimation', 'Actual Spectral Width', 'Acquisition Time','Last RF Frequency','SNR Mean Value'...    
                });
            obj.OutputParValues = containers.Map(obj.OutputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN...   
                });
            obj.OutputParUnits = containers.Map(obj.OutputParameters,...
                {...
                '','MHz','ms','MHz',''...  
                });
        end
        function SetDefaultParameters(obj)
            [pathstr, name, ext] = fileparts(mfilename('fullpath'));
            obj.path_file = fullfile(pathstr,'FileDir'); % this is the path of the folder where all the generated files
                                                     % will be written and read from
            obj.OutputFilename = fullfile(obj.path_file,'FID_temp');
            obj.OutputFilename_CF = fullfile(obj.path_file,'FID_temp_CF');            
            obj.SetParameter('SHIMSLICE',0);
            obj.SetParameter('SHIMPHASE',0);
            obj.SetParameter('SHIMREADOUT',0);
            obj.SetParameter('NPOINTS',600);
            obj.SetParameter('NSLICES',1);
            obj.SetParameter('FREQUENCY',14.5*MyUnits.MHz);
            obj.SetParameter('SPECTRALWIDTH',300*MyUnits.kHz);
            obj.SetParameter('RFAMPLITUDE',0.3);
            obj.SetParameter('PULSETIME',12*MyUnits.us);
            obj.SetParameter('PHASE','y');
            obj.SetParameter('READOUT','x');
            obj.SetParameter('SLICE','z');
            obj.SetParameter('SPOILERTIME',0.1*MyUnits.us);
            obj.SetParameter('SPOILERAMP',0.1);
        end
        function SetParameter(obj,par,value)
%             if ischar(value); value = str2num(value);end
            if (isempty(value) || isnan(value)); return; end
            if any(strcmp(obj.InputParameters,par))
                %               fprintf('Parameter %s is set to %f\n',par,value);
                if value < obj.InputParValuesMin(par)
                    obj.InputParValues(par) = obj.InputParValuesMin(par);
                elseif value > obj.InputParValuesMax(par)
                    obj.InputParValues(par) = obj.InputParValuesMax(par);
                else
                    obj.InputParValues(par) = value;
                end
            elseif any(strcmp(obj.OutputParameters,par))
                obj.OutputParValues(par) = value;
            else
                fprintf('Parameter %s is not found\n',par);
            end
        end
        function SetParameterMin(obj,par,value)
            if any(strcmp(obj.InputParameters,par))
                obj.InputParValuesMin(par) = value;
            else
                fprintf('Input Parameter %s is not found\n',par);
            end
        end
        function SetScanParameterValue(obj,par,value)
            obj.SetParameter(par,value)
        end
        function SetParameterMax(obj,par,value)
            if any(strcmp(obj.InputParameters,par))
                obj.InputParValuesMax(par) = value;
            else
                fprintf('Input Parameter %s is not found\n',par);
            end
        end
        function SetParameters(obj,ParValuesMap)
            for k=1:ParValuesMap.length()
                SetParameter(ParValuesMap.keys(k),ParValuesMap.values(k));
            end
        end
        function value = GetParameter(obj,par)
            if any(strcmp(obj.InputParameters,par))
                value = obj.InputParValues(par);
            elseif any(strcmp(obj.OutputParameters,par))
                value = obj.OutputParValues(par);
            else
                value = NaN;
            end
        end
        function value = GetParameterUnit(obj,par)
            if any(strcmp(obj.InputParameters,par))
                value = obj.InputParUnits(par);
            elseif any(strcmp(obj.OutputParameters,par))
                value = obj.OutputParUnits(par);
            else
                value = '';
            end
        end
        function value = GetParameterName(obj,par)
            if any(strcmp(obj.InputParameters,par))
                value = obj.InputParNames(par);
            elseif any(strcmp(obj.OutputParameters,par))
                value = obj.OutputParNames(par);
            else
                value = '';
            end
        end
        function [pars,names,values,units] = GetInputParameters(obj)
            pars = obj.InputParameters;
            names = containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());%Matlab trick to get a new copy of container
            values = containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());
            units = containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());
            for k=1:length(obj.InputParHidden)
                par = obj.InputParHidden{k};
                pars(strcmp(pars,par)) = [];
            end
            names.remove(obj.InputParHidden);
            values.remove(obj.InputParHidden);
            units.remove(obj.InputParHidden);
            
        end
        function [pars,names,values,units] = GetOutputParameters(obj)
            pars = obj.OutputParameters;
            names = obj.OutputParNames;
            values = obj.OutputParValues;
            units = obj.OutputParUnits;
        end
        function sw = ExistsInputParameter(obj,par)
            sw = any(strcmp(obj.InputParameters,par));
        end
        function SetFIDPhase(obj,phase)
            obj.FIDPhase = phase;
        end
        function SetNAverages(obj,n)
            obj.NAverages = n;
        end
        function EnableAveraging(obj,sw)
            obj.averaging = sw;
        end
        function EnableAutoPhase(obj,sw)
            obj.autoPhasing = sw;
        end
        function SetRFShape(obj,shape_type)
            obj.RF_Shape = shape_type;
        end
        function SetShimS(obj,v)
            obj.SetParameter('SHIMSLICE',v);
        end
        function SetShimP(obj,v)
            obj.SetParameter('SHIMPHASE',v);
        end
        function SetShimR(obj,v)
            obj.SetParameter('SHIMREADOUT',v);
        end
        function v = GetShimS(obj)
            v = obj.GetParameter('SHIMSLICE');
        end
        function v = GetShimP(obj)
            v = obj.GetParameter('SHIMPHASE');
        end
        function v = GetShimR(obj)
            v = obj.GetParameter('SHIMREADOUT');
        end
        function EnableSliceGradient(obj,sw)
            obj.slice_en = sw;
        end
        function EnablePhaseGradient(obj,sw)
            obj.phase_en = sw;
        end
        function EnableReadoutGradient(obj,sw)
            obj.readout_en = sw;
        end
        function EnableSpoilerGradient(obj,sw)
            obj.spoiler_en = sw;
        end
        function EnableShimming(obj,sw)
            obj.EnableShimmingSlice(sw);
            obj.EnableShimmingPhase(sw);
            obj.EnableShimmingReadout(sw);
        end
        function EnableShimmingSlice(obj,sw)
            obj.shimS_en = sw;
        end
        function EnableShimmingPhase(obj,sw)
            obj.shimP_en = sw;
        end
        function EnableShimmingReadout(obj,sw)
            obj.shimR_en = sw;
        end
        function EnableCalibration(obj,sw)
%             obj.RunCal = sw;
            obj.calibration_en = sw;
            obj.CalCheck = sw;
        end
        function CalibrateImage(obj,sw)
            obj.calibration_en = sw;
        end

        function [fidData,fftData] = GetData1D(obj)
            fidData = obj.fidData;
            fftData = obj.fftData1D;
        end
        function SaveFID(obj,fname)
            saveFile = fopen(fullfile(obj.path_file,fname),'w');
            for ii = 1:length(obj.timeVector)
                fprintf(saveFile,'%f %f %f\n',obj.timeVector(ii)/MyUnits.us,real(obj.fidData(ii)),imag(obj.fidData(ii)));
            end
            fclose(saveFile);
        end
        function Abort(obj)
            obj.AbortRun = true;
        end
        function [shimS,shimP,shimR]=SelectAxes(obj) %set the appropriate axes as were decided
            if (obj.GetParameter('READOUT')==obj.GetParameter('PHASE'))
              ed = errordlg('Readout and Phase gradient cannot be applied to the same axis','ConfigFileError','modal');
              uiwait(ed);
              return;
            end
            switch num2str(obj.GetParameter('READOUT'))
                case 'x'
                    obj.selectReadout = 2; shimR = obj.GetParameter('SHIMREADOUT');
                case 'y'
                    obj.selectReadout = 0; shimR = obj.GetParameter('SHIMPHASE');
                case 'z'
                    obj.selectReadout = 1; shimR = obj.GetParameter('SHIMSLICE');
                otherwise
                    obj.selectReadout = 2; shimR = obj.GetParameter('SHIMREADOUT');
            end
            switch num2str(obj.GetParameter('PHASE'))
                case 'x'
                    obj.selectPhase = 2; shimP = obj.GetParameter('SHIMREADOUT');
                case 'y'
                    obj.selectPhase = 0; shimP = obj.GetParameter('SHIMPHASE');
                case 'z'
                    obj.selectPhase = 1; shimP = obj.GetParameter('SHIMSLICE');
                otherwise
                    obj.selectPhase = 0; shimP = obj.GetParameter('SHIMPHASE');
            end
            obj.selectSlice=3-(obj.selectPhase+obj.selectReadout); 
            switch obj.selectSlice;
                case 0
                    shimS = obj.GetParameter('SHIMPHASE');
                case 2
                    shimS = obj.GetParameter('SHIMREADOUT');
                case 1
                    shimS = obj.GetParameter('SHIMSLICE');
                otherwise
                    shimS = obj.GetParameter('SHIMSLICE');
            end
            
            if (obj.shimS_en==0)
                shimS = 0;
            end
            if (obj.shimP_en==0)
                shimP = 0;
            end
            if (obj.shimR_en==0)
                shimR = 0;
            end
        end
        function CenterFrequency(obj,fname_CF)
            % GET CENTER FREQUENCY
            [status,result] = system(fullfile(obj.path_file,fname_CF));

            if status == 0 && ~isempty(result)
                C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                res = C{:};
                obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
            end
            
            DataCF = load(sprintf('%s.txt',obj.OutputFilename_CF));
            cDataCF = complex(DataCF(:,1),DataCF(:,2));

            fftData1D = flipud(fftshift(fft(cDataCF)));
            acq_time = obj.GetParameter('ACQUISITIONTIME');
            freqVector = (ceil(-length(fftData1D)/2):ceil(length(fftData1D)/2)-1)/acq_time;
            idx = find(fftData1D==max(fftData1D),1,'first');
            deltaFreq = freqVector(idx);

            obj.SetParameter('FREQUENCY',obj.GetParameter('FREQUENCY')+deltaFreq);   
            
            if abs(obj.ReferenceCF - obj.GetParameter('FREQUENCY')) >= obj.MaxCF_shift
                load gong.mat;
                sound(y, Fs);
                clear Fs y
                h_warn = warndlg(char(strcat('Please tune coil to: [MHz]', num2str( obj.GetParameter('FREQUENCY')/MyUnits.MHz))),'!!! Waiting to Continue !!!');
                uiwait(h_warn);
            end
        end
        function WriteBatchFile(obj,fname)
            % WRITE BATCH FILE AND RUN            
            fid = fopen(fullfile(obj.path_file,fname), 'wt');
            pars = obj.InputParameters;
            values = obj.InputParValues;
            fprintf( fid, '@echo off\n');
            fprintf( fid, 'SET Debug=%d\n',0);
            if strcmp(fname(end-6:end-4),'_CF')
                    fprintf( fid, 'SET outputFilename=%s\n',obj.OutputFilename_CF);
            else
                    fprintf( fid, 'SET outputFilename=%s\n',obj.OutputFilename);
            end
            fprintf( fid, 'REM ======================\n');
            fprintf( fid, 'REM Acquisition Parameters\n' );
            fprintf( fid, 'REM ======================\n');
            fprintf( fid, 'SET nPoints=%u\n', values('NPOINTS') );
            fprintf( fid, 'SET nScans=%u\n', values('NSCANS') );
            fprintf( fid, 'SET nline_reads=%u\n', obj.nline_reads);
            fprintf( fid, 'SET spectrometerFrequency_MHz=%f\n', values('FREQUENCY')/MyUnits.MHz); 
            fprintf( fid, 'SET sliceFreqOffset=%f\n',obj.freq_offset/MyUnits.MHz);            
            fprintf( fid, 'SET spectralWidth_kHz=%f\n',values('SPECTRALWIDTH')/MyUnits.kHz);
                        
%             fprintf( fid, 'SET linebroadening_value=%f\n',values('LINEBROADENING'));
            
            fprintf( fid, 'REM ===================\n');
            fprintf( fid, 'REM RF Pulse Parameters\n');
            fprintf( fid, 'REM ===================\n'  );
            fprintf( fid, 'SET RF_Shape=%u\n', obj.RF_Shape);
            fprintf( fid, 'SET amplitude=%f\n', values('RFAMPLITUDE'));
            fprintf( fid, 'SET pulseTime_us=%f\n', values('PULSETIME')/MyUnits.us);
            
            % Now put in the vectors of the amplitude selections
            fprintf( fid, 'REM ===================\n');
            fprintf( fid, 'REM Gradient Pulse Schema\n');
            fprintf( fid, 'REM ===================\n'  );
%            fprintf( fid, 'SET nPulses=%u', values('NPULSES'));
            fprintf( fid, 'SET nInstructions=%u\n', obj.nInstructions);
            fprintf( fid, 'SET AMPs=%s\n', obj.AMPs_string);
            fprintf( fid, 'SET DACs=%s\n', obj.DACs_string);
            fprintf( fid, 'SET WRITEs=%s\n', obj.WRITEs_string);
            fprintf( fid, 'SET UPDATEs=%s\n', obj.UPDATEs_string);
            fprintf( fid, 'SET CLEARs=%s\n', obj.CLEARs_string);
            fprintf( fid, 'SET FREQs=%s\n', obj.FREQs_string);
            fprintf( fid, 'SET RXs=%s\n', obj.RXs_string);
            fprintf( fid, 'SET TXs=%s\n', obj.TXs_string);
            fprintf( fid, 'SET PHASEs=%s\n', obj.PHASEs_string);
            fprintf( fid, 'SET PhResets=%s\n', obj.PhResets_string);
            fprintf( fid, 'SET DELAYs=%s\n', obj.DELAYs_string);
            fprintf( fid, 'SET ENVELOPEs=%s\n', obj.ENVELOPEs_string);
            
            fprintf( fid, 'REM ===================\n');
            fprintf( fid, 'REM Flags and configuration\n');
            fprintf( fid, 'REM ===================\n'  );
            fprintf( fid, 'SET FLAGs=%s\n', obj.FLAGs_string);
            fprintf( fid, 'SET OPCODEs=%s\n', obj.OPCODEs_string);
            fprintf( fid, 'SET adcOffset=%u\n', obj.adcOffset);
            
            fprintf( fid, 'echo. | "%s"',fullfile(obj.ProgramDir,obj.ProgramName)); %we need echo. to exit batch file in case of errors (it emulates typing ENTER)
            fclose(fid);
        end
        function [readout,slice,phase,spoiler,SSaux] = ConfigureGradients(obj, mode)
            % mode =  "0", normal execution of the sequence,
            %         "1", central frequency adjustment sequence,
            
            SSaux=obj.selectSlice;
            spoiler =obj.spoiler_en;
            
            if mode == 1  % central frequency adjustment sequence
                
                nSlices = 1;
                nPhases = 1;
                readout = 0; % None of the gradients should be enabled if we are setting the central frequency (CF)
                slice = 0;
                phase = 0;
                spoiler = 0;
                
            elseif mode ==0  % normal execution of the sequence
                
                readout = obj.readout_en;
                slice = obj.slice_en;
                phase = obj.phase_en;
                spoiler = obj.spoiler_en;
            
            end
            
        end
        function CreateSequence( obj, mode )
            % Function rewritten inside each particular sequence
            
            obj.StartSlice = obj.GetParameter('STARTSLICE');
            obj.EndSlice = obj.GetParameter('ENDSLICE');
            
        end
        function [cDataBatch_Cal]=getCalibrationData(obj,mode,fname_Cal,cDataBatch_Cal)
            obj.CreateSequence(mode);
            obj.Sequence2String;
            obj.WriteBatchFile(fname_Cal);
            [status,result] = system(fullfile(obj.path_file,fname_Cal));
            DataBatch_Cal = load(sprintf('%s.txt',obj.OutputFilename));
            cDataBatch_Cal = [ cDataBatch_Cal ; complex(DataBatch_Cal(:,1),DataBatch_Cal(:,2))];
        end
        
        function Sequence2String(obj)
            if length(obj.AMPs)<1
                return;
            end
            obj.AMPs_string     = num2str(obj.AMPs(1));
            obj.DACs_string     = num2str(obj.DACs(1)); 
            obj.WRITEs_string   = num2str(obj.WRITEs(1));  
            obj.UPDATEs_string  = num2str(obj.UPDATEs(1));   
            obj.CLEARs_string   = num2str(obj.CLEARs(1)); 
            obj.FREQs_string    = num2str(obj.FREQs(1));   
            obj.RXs_string      = num2str(obj.RXs(1)); 
            obj.TXs_string      = num2str(obj.TXs(1));
            obj.PHASEs_string   = num2str(obj.PHASEs(1));
            obj.PhResets_string = num2str(obj.PhResets(1));  
            obj.ENVELOPEs_string= num2str(obj.ENVELOPEs(1)); 
            obj.FLAGs_string    = num2str(obj.FLAGs(1)); 
            obj.OPCODEs_string 	= num2str(obj.OPCODEs(1));  
            obj.DELAYs_string   = num2str(obj.DELAYs(1)); 
             for k = 2:length(obj.AMPs)
                  obj.AMPs_string     = char(strcat( obj.AMPs_string, ',', num2str(obj.AMPs(k))));
                  obj.DACs_string     = char(strcat( obj.DACs_string, ',', num2str(obj.DACs(k)))); 
                  obj.WRITEs_string   = char(strcat( obj.WRITEs_string, ',', num2str(obj.WRITEs(k)))); 
                  obj.UPDATEs_string  = char(strcat( obj.UPDATEs_string, ',', num2str(obj.UPDATEs(k))));  
                  obj.CLEARs_string   = char(strcat( obj.CLEARs_string, ',', num2str(obj.CLEARs(k)))); 
                  obj.FREQs_string    = char(strcat( obj.FREQs_string, ',', num2str(obj.FREQs(k))));  
                  obj.RXs_string      = char(strcat( obj.RXs_string, ',', num2str(obj.RXs(k)))); 
                  obj.TXs_string      = char(strcat( obj.TXs_string, ',', num2str(obj.TXs(k))));  
                  obj.PHASEs_string   = char(strcat( obj.PHASEs_string, ',', num2str(obj.PHASEs(k))));
                  obj.PhResets_string = char(strcat( obj.PhResets_string, ',', num2str(obj.PhResets(k)))); 
                  obj.ENVELOPEs_string= char(strcat( obj.ENVELOPEs_string, ',', num2str(obj.ENVELOPEs(k)))); 
                  obj.FLAGs_string    = char(strcat( obj.FLAGs_string, ',', num2str(obj.FLAGs(k)))); 
                  obj.OPCODEs_string 	= char(strcat( obj.OPCODEs_string, ',', num2str(obj.OPCODEs(k))));  
                  obj.DELAYs_string   = char(strcat( obj.DELAYs_string, ',', num2str(obj.DELAYs(k)))); 
            end
        end
    end
    
end

