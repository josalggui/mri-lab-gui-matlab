classdef Constants
    %UNTITLED4 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
        hBar = 1.055e-34; %J*s
        h = 2*pi*Constants.hBar; %J*s
        k = 1.3805e-23; %J/Kelvin
        mu0 = 4*pi*1e-7; %H/m or T*m/A
        gammaProtonRad = 267.513e6; %rad/s/T
        gammaProtonHz = Constants.gammaProtonRad/2/pi; %Hz/T
    end
    
    methods
    end
    
end

