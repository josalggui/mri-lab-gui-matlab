classdef Plot1D
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Constant)
    end
    
    methods(Static)
        function y = CalculateYLimit(data,y)
            if min(data) < y(1)
                y(1) = min(data) - 0.2*(max(data) - min(data));
            elseif (min(data) - y(1)) > 1*(max(data) - min(data))
                y(1) = min(data) - 0.2*(max(data) - min(data));
            end;
            if max(data) > y(2)
                y(2) = max(data) + 0.2*(max(data) - min(data));
            elseif (y(2) - max(data)) > 1*(max(data) - min(data))
                y(2) = max(data) - 0.2*(max(data) - min(data));
            end;
        end
        function [line, xvectorFID] = PlotFID(h,fidData,acq_time)
            xvectorFID = (0:length(fidData)-1)./length(fidData)*(acq_time/MyUnits.us);
            line = plot(xvectorFID,fidData,'Parent',h);
            xlim([min(xvectorFID) max(xvectorFID)]);
            grid on
            title 'FID'
            xlabel('Time, us');
        end
        function [line, xvectorFFT] = PlotFFT(h,fftData,acq_time)
            xvectorFFT = [ceil(-length(fftData)/2):ceil(length(fftData)/2)-1]/(acq_time/MyUnits.ms);
            line = plot(xvectorFFT,fftData,'Parent',h);
            xlim([min(xvectorFFT) max(xvectorFFT)]);
            grid on
            title 'FFT'
            xlabel('Frequency, kHz');
        end
    end
end
