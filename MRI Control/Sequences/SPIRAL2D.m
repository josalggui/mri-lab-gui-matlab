classdef SPIRAL2D < MRI_BlankSequence   
    properties
        maxRepetitionsPerBatch;
        nPhasesBatch;
        lastBatch;
        RepetitionTime;
        AcquisitionTime;
        EndPhase;
        PhasesPerBatch = 20;
        gslice;
        gx;
        gy;
        gSteps=1000;
        tacq;
    end
    methods (Access=private)
        function CreateParameters(obj)
            obj.CreateOneParameter('FOVX','FOV X','mm',50*MyUnits.mm)
            obj.CreateOneParameter('FOVY','FOV Y','mm',50*MyUnits.mm)
            obj.CreateOneParameter('FOVZ','Slice thick','mm',20*MyUnits.mm)
            obj.CreateOneParameter('NX','NX','',40)
            obj.CreateOneParameter('NY','NY','',40)           
            obj.CreateOneParameter('SLICEPOSITION','Slice position','mm',0*MyUnits.mm)
            obj.CreateOneParameter('PULSETIME','Pulse Time','us',250*MyUnits.us);
            obj.CreateOneParameter('REPETITIONTIME','TR','ms',50*MyUnits.ms);
            obj.CreateOneParameter('SLOPERATE','Slope rate','T/m/s',1000);
            obj.CreateOneParameter('LAMBDA','LAMBDA','',1);
            obj.CreateOneParameter('UNDERSAMPLING','Aceleration factor','',1);
            obj.CreateOneParameter('GRADMAX','Maximum gradient','mT/m',100);
            obj.CreateOneParameter('GRADDELAY','Gradient Delay','us',55*MyUnits.us);
            obj.CreateOneParameter('EXCITATIONMODE','1= Soft, 2=Hard','',2);
            obj.CreateOneParameter('OVERSAMPLING','Nyquist oversampling','',1);
            
            obj.InputParHidden = {'NPOINTS','SPECTRALWIDTH','SHIMSLICE','SHIMPHASE','SHIMREADOUT','SPOILERTIME','SPOILERAMP','NSLICES','BLANKINGDELAY'};
        end  
    end

    methods
        function obj = SPIRAL2D(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'SPIRAL2D';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
        end    
         
        function CreateSequence(obj)
            [shimS,shimP,shimR] = SelectAxes(obj);
            obj.nline_reads = 1;
            pulseTime = obj.GetParameter('PULSETIME')*1e6;
            deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
            TR = obj.GetParameter('REPETITIONTIME')*1e6;
            blkDelay = obj.GetParameter('BLANKINGDELAY')*1e6;
            crTime = obj.GetParameter('COILRISETIME')*1e6;
            gradDelayTime = obj.GetParameter('GRADDELAY')*1e6;
            excitationmode =  obj.GetParameter('EXCITATIONMODE');
            
            Tacq=obj.tacq*1e6;
            sliceDelayTime=0.1;
            gapTime = 0.1;
            iIns = 1;
            
            if deadTime>gradDelayTime
                deadTime=deadTime-gradDelayTime;
            end
            
            RR = obj.GetParameter('READOUT');
            PP = obj.GetParameter('PHASE');
            SS = obj.GetParameter('SLICE');
            
            if SS=='x'
                dacslice=2; 
            end
            if SS=='y'
                dacslice=0; 
            end
            if SS=='z'
                dacslice=1;
            end

            if RR=='x'
                dacred=2;
            end
            if RR=='y'
                dacred=0; 
            end
            if RR=='z'
                dacred=1; 
            end

            if PP=='x'
                dacph=2; 
            end
            if PP=='y'
                dacph=0;
            end
            if PP=='z'
                dacph=1;  
            end
            
            PP=dacph;   SS=dacslice;   RR=dacred;
            Gx=obj.gx; Gy=obj.gy; 
            
            if PP==RR || PP==SS || RR==SS
                warndlg('Nono valid axis inputs','Axis Error')
                return;
            end
            
            
            if excitationmode==1
                 obj.nInstructions = 8+4*obj.nSteps+2*obj.gSteps;
                 [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.RFamps,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));

                %% SHIMMING in all gradients (3 instructions)
                iIns = obj.PulseGradient(iIns,shimR,RR,0.1);
                iIns = obj.PulseGradient(iIns,shimP,PP,0.1);
                iIns = obj.PulseGradient(iIns,shimS,SS,0.1);
                
                %% Activate dephasing slice gradient (nSteps instructions)
                for ii = 1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimS+(obj.gslice)*ii/obj.nSteps,SS,crTime/obj.nSteps);
                end
                obj.DELAYs(iIns-1) = crTime/obj.nSteps+gradDelayTime-blkDelay;
                
                %% RF pulse (3 instruction)
                iIns = obj.CreatePulse(iIns,0,blkDelay,pulseTime,0.1);
                obj.ENVELOPEs(iIns-2) = 0;
                
                %% Deactivate dephasing slice gradient (nSteps instructions)
                for ii = 1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimS+(obj.gslice)*(1-ii/obj.nSteps),SS,crTime/obj.nSteps);
                end
                
                %% Activate rephasing slice gradient (nSteps instructions)
                for ii = 1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimS-(obj.gslice)*ii/obj.nSteps,SS,crTime/obj.nSteps);
                end
                if(pulseTime>=crTime-2*gradDelayTime)
                    obj.DELAYs(iIns-1) = crTime/obj.nSteps+(0.5*pulseTime+gradDelayTime-0.5*crTime)+sliceDelayTime;
                else
                    obj.DELAYs(iIns-1) = crTime/obj.nSteps;
                end
                
                %% Deactivate rephasing slice gradient (nSteps instructions)
                for ii = 1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimS-(obj.gslice)*(1-ii/obj.nSteps),SS,crTime/obj.nSteps);
                end
                obj.DELAYs(iIns-1) = 0.1;
                obj.DELAYs(iIns-1) = crTime/obj.nSteps+gapTime;
                
                %% Acquisition (1 ins)
                iIns = obj.Acquisition(iIns,0.1);
                
                %% Gradients (2*gSteps)
                for i=1:obj.gSteps
                    iIns = obj.PulseGradient(iIns,Gx(i),RR,0.1);
                    iIns = obj.PulseGradient(iIns,Gy(i),PP,Tacq/obj.gSteps);
                end
                
                %% Delay for recover T1 (1 ins)
                obj.RepetitionDelay (iIns,1,TR);
            end
            
            
            if excitationmode==2
                obj.nInstructions = (8+2*obj.gSteps);
                [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.RFamps,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
                %% SHIMMING in all gradients (3 instructions)
                iIns = obj.PulseGradient(iIns,shimR,RR,0.1);
                iIns = obj.PulseGradient(iIns,shimP,PP,0.1);
                iIns = obj.PulseGradient(iIns,shimS,SS,0.1);
                
                %% RF pulse (3 instruction)
                iIns = obj.CreatePulse(iIns,0,blkDelay,pulseTime,deadTime);
                obj.ENVELOPEs(iIns-2) = 7;
                
                %% Gradients (2*gSteps)
                for i=1:obj.gSteps
                    iIns = obj.PulseGradient(iIns,shimR+Gx(i),RR,0.1);
                    iIns = obj.PulseGradient(iIns,shimP+Gy(i),PP,Tacq/obj.gSteps-0.1);
                    
                    if (i-1)*Tacq/obj.gSteps<gradDelayTime && (i)*Tacq/obj.gSteps>gradDelayTime
                        obj.DELAYs(iIns-1)= max(gradDelayTime-(i-1)*(Tacq/obj.gSteps+0.1),0.1);
                        % Acquisition (1 ins)
                        iIns = obj.Acquisition(iIns,(i)*Tacq/obj.gSteps-gradDelayTime-0.1);
                    end
                end
                
                %% Delay for recover T1 (1 ins)
                obj.RepetitionDelay (iIns,1,TR-(Tacq+blkDelay+pulseTime+deadTime));
                
            end
        end
        
        
        function [status,result] = Run(obj,~)
            clc
            c = obj.ReorganizeCalibrationData();
            global rawData;
            rawData = [];
            gammabar = 42.6d6;
            gamma=2*pi*gammabar;
 
            NyquistOversampling=obj.GetParameter('OVERSAMPLING') ; SRO=obj.GetParameter('SLOPERATE');L=obj.GetParameter('LAMBDA');u=obj.GetParameter('UNDERSAMPLING');G=obj.GetParameter('GRADMAX')*1e-3; FOVY = obj.GetParameter('FOVY');FOVX = obj.GetParameter('FOVX'); FOVZ = obj.GetParameter('FOVZ'); slicepos= obj.GetParameter('SLICEPOSITION'); NY = obj.GetParameter('NY'); NX = obj.GetParameter('NX');pulseTime90=obj.GetParameter('PULSETIME'); RFamp = obj.GetParameter('RFAMPLITUDE'); pulsetime90 = obj.GetParameter('PULSETIME'); deadTime = obj.GetParameter('TRANSIENTTIME');blkDelay = obj.GetParameter('BLANKINGDELAY');TR = obj.GetParameter('REPETITIONTIME');            
            rawData.inputs.Seq = obj.SequenceName;rawData.inputs.SlopeRate=SRO; rawData.inputs.LAMBDA=L;rawData.inputs.AcelerationFactor = obj.GetParameter('UNDERSAMPLING');rawData.inputs.MaxGradient = G*1e-3; rawData.inputs.rfAmplitude90 = obj.GetParameter('RFAMPLITUDE');rawData.inputs.PulseTime = obj.GetParameter('PULSETIME'); rawData.inputs.pulseTime90 = pulseTime90; rawData.inputs.deadTime = deadTime; rawData.inputs.TR = obj.GetParameter('REPETITIONTIME');rawData.inputs.FOVY = FOVY;rawData.inputs.NY = NY;rawData.inputs.FOVX = FOVX;rawData.inputs.NX = NX;rawData.inputs.SliceThick = FOVZ;
            
            obj.gslice=7.2/(gammabar*FOVZ*pulsetime90*c(3));
            obj.SliceFreqOffset=gammabar*obj.gslice*slicepos;
            
            SRO=SRO/u; %This normalization of SRO has been requested by Fer and is pending of be tested.
            Nshot=1;
            gsteps=obj.gSteps;
            lambda=Nshot/(2*pi*FOVX); %1/m  %Doubt here. What about rectangular FOVs?
            beta=(gamma*SRO)/(2*pi*lambda); %Hz
            a2=(9*beta/4)^(1/3);
            ts=((3*gamma*G)/(4*pi*lambda*(a2^2)))^3;  %Time of transition of slew rate limited to gradient amplitude limited
            
            %Is our trajectory slew rate limited or gradient amp limited?
            thetaS=(beta*ts*ts/2)/((L+(beta*(ts^(4/3))/(2*a2))));
            thetamax=(pi*NX/Nshot);
            
            if thetamax>thetaS
                Tacq=ts+(((pi*lambda)/(gamma*G))*((thetamax^2)-(thetaS^2)));
            else 
                Tacq=((2*pi*FOVX)/(3*Nshot))*sqrt(pi/(gamma*SRO*(FOVX/NX)^3));
            end
            
            %Sampling parameters according to Nyquist criterion
            obj.tacq=Tacq;
            bw=NyquistOversampling*(G*gamma*FOVX)/(4*pi);
            obj.SetParameter('SPECTRALWIDTH',bw);
            sampledpoints = floor(bw*Tacq);
            obj.SetParameter('NPOINTS',sampledpoints);
            
            t=linspace(0,Tacq,gsteps);
            theta=zeros(1,gsteps);

            for i=1:gsteps
                if t(i)<ts
                    theta(i)=(beta*t(i)*t(i)/2)/((L+(beta*(t(i)^(4/3))/(2*a2))));   
                end
                if t(i)>=ts
                    theta(i)=sqrt((thetaS^2)+((gamma*G*(t(i)-ts))/(pi*lambda)));
                end
                kx(i)=u*lambda*theta(i)*cos(theta(i));
                ky(i)=u*lambda*theta(i)*sin(theta(i));
                kx2(i)=u*lambda*theta(i)*cos(pi+theta(i));
                ky2(i)=u*lambda*theta(i)*sin(pi+theta(i));
            end   
                
             Dtheta = diff(theta)./diff(t); Dtheta(end+1)=Dtheta(end);
             
             for i=1:gsteps
                Gx(i)=u*lambda*Dtheta(i)*(cos(theta(i))-theta(i)*sin(theta(i)))/(gammabar);
                Gy(i)=u*lambda*Dtheta(i)*(sin(theta(i))+theta(i)*cos(theta(i)))/(gammabar);
                Gx2(i)=u*lambda*Dtheta(i)*(cos(theta(i)+pi)-theta(i)*sin(pi+theta(i)))/(gammabar);
                Gy2(i)=u*lambda*Dtheta(i)*(sin(theta(i)+pi)+theta(i)*cos(pi+theta(i)))/(gammabar);
             end

             kmaxX=NX/(2*FOVX);
             kmaxY=NY/(2*FOVY);
             kxcart=linspace(-kmaxX,kmaxX,NX);
             kycart=linspace(-kmaxY,kmaxY,NY);
             [X,Y]=meshgrid(kxcart,kycart);
             KXcart=reshape(X,[NX*NY,1]);
             KYcart=reshape(Y,[NX*NY,1]);
             
             figure
             set(gcf, 'Position', [100, 500, 1500, 400])
             subplot(2,3,1)
             plot(t*1e3,theta)
             xlabel('Time (ms)');
             ylabel('theta')
             subplot(2,3,2)
             hold on
             plot(t*1e3,Gx,t*1e3,Gy); plot(t*1e3,Gx2,t*1e3,Gy2);
             legend('Gx 1� arm','Gy 1� arm','Gx 2� arm','Gy 2� arm')
             xlabel('Time (ms)');
             ylabel('G (T/m)')
             subplot(2,3,3)
             hold on
             plot(Gx,Gy,'.-'); plot(Gx2,Gy2,'.-');
             xlabel('Gx (T/m)');
             ylabel('Gy (T/m)')
             legend('1� arm','2� arm')
             subplot(2,3,5)
             hold on
             plot(t*1e3,kx,t*1e3,ky,'.-');  plot(t*1e3,kx2,t*1e3,ky2,'.-');
             xlabel('Time (ms)');
             ylabel('K (1/m)')
             legend('Kx 1� arm','Ky 1� arm','Kx 2� arm','KGy 2� arm')
             subplot(2,3,6)
             hold on
             plot(kx,ky,'.-');  plot(kx2,ky2,'.-');
             plot(KXcart,KYcart,'.');
             xlabel('Kx (1/m)');
             ylabel('Ky (1/m)')
             legend('1� arm','2� arm')
             
%              pause %This stop the sequence here to check in the plot if the inputs give desired sampling
             
             obj.nInstructions = (8+2*obj.gSteps);
             obj.maxRepetitionsPerBatch = floor(2048/obj.nInstructions);
            
            clc
            fprintf('Theta m�ximum = %s rad.\n',thetamax);
            fprintf('Theta for transition = %s rad.\n',thetaS);
            fprintf('Duration of slope-rate limited segment: Ts = %s ms.\n',ts);
            fprintf('Acquisition time = %s ms.\n',Tacq*1e3);
            fprintf('Bandwidth for acquisition: BW = %s kHz.\n',bw*1e-3);
            fprintf('Number of acquired points : N = %s points.\n',sampledpoints);
             
            % Execute the first spiral
            obj.gx=Gx/c(1); obj.gy=Gy/c(2);
            
            [~,~,~,~,~,spoilerEnable,~] = obj.ConfigureGradients(0);
             if(spoilerEnable)
                    fprintf('Calibrating frequency... \n')
                    % Get central frequency
                    f0 = obj.GetFIDParameters;
                    obj.SetParameter('FREQUENCY',f0);
%                     nFreq = nFreq+1;
%                     rawData.aux.freqListRadial(nFreq) = f0;
                    fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
                    tic;
             end
            
            obj.CreateSequence;
            obj.Sequence2String;
            fname = 'TempBatch.bat';
            obj.WriteBatchFile(fname);
            [status,result] = system(fullfile(obj.path_file,fname));
            if status == 0 && ~isempty(result)
                C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                res = C{:};
                obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz'));
                obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms);
                obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
            else
                warndlg('Batch aborted!','Warning')
                return;
            end
            DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
            cDataBatch1 = complex(DataBatch(:,1),DataBatch(:,2));
            
            
            % Execute the second spiral
            obj.gx=Gx2/c(1); obj.gy=Gy2/c(2);
            
            obj.CreateSequence;
            obj.Sequence2String;
            fname = 'TempBatch.bat';
            obj.WriteBatchFile(fname);
            [status,result] = system(fullfile(obj.path_file,fname));
            if status == 0 && ~isempty(result)
                C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                res = C{:};
                obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz'));
                obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms);
                obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
            else
                warndlg('Batch aborted!','Warning')
                return;
            end
            DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
            cDataBatch2 = complex(DataBatch(:,1),DataBatch(:,2));
            
            tsampled=linspace(0,Tacq,floor(bw*Tacq));
            
            kxsampled1=interp1(t,kx,tsampled);     kysampled1=interp1(t,ky,tsampled);
            kxsampled2=interp1(t,kx2,tsampled);    kysampled2=interp1(t,ky2,tsampled);
            
            kxsampled=cat(2,kxsampled1, kxsampled2);   kysampled=cat(2,kysampled1, kysampled2);   signal=cat(1,cDataBatch1,cDataBatch2);
            rawData.kSpace.noninterpolated=[kxsampled' kysampled'  signal];
            
            signalint = griddata(kxsampled',kysampled',signal,KXcart,KYcart);
            signalint(isnan(signalint))=0;
            rawData.kSpace.sampled = [KXcart,KYcart,zeros(length(KYcart),1),signalint];
            
            kSpaceReshaped=reshape(signalint,[NX,NY]);
            imagen = abs(ifftshift(ifftn(kSpaceReshaped)));
            rawData.kSpace.imagen = reshape(imagen,[NX,NY]);
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
    end
end