classdef RARE2D < MRI_BlankSequence
    % 08/10/2020
    % Jos� Miguel Algar�n Guisado
    % Ph.D.
    % MRILab
    % Institute for Instrumentation in Molecular Imaging (I3M)
    % CSIC- UPV
    % josalggui@i3m.upv.es

    properties
        StartSlice;
        EndSlice;
        PhasesPerBatch = 20;
        PhasesinThisBatch = 1;
        StartPhase;
        EndPhase;
        slice_ampl;
        slice_ampl_re;
        acq_time;
        PhaseTime;
        RepDelay;
        RO_strength_rep;
        RO_strength_dep;
        SL_strength_dep;
        SL_strength_rep;
        NRO;
        NPH;
        NSL;
        Axis1;
        Axis2;
        Axis3;
        CAxis1;
        CAxis2;
        CAxis3;
        rdspeed_mult = 1;
        nPhasesBatch;
        NP_calib = 1;
        ROmax = 0;
        CalData;
        FOV;
        gam = 42.57E6; %gyromagnetic ratio, 42.57 MHz/T
        gradientList;
        sliceRephasingTime;
        sincZeroCrossings = 1;        % Number of zero crossing for the RF sinc pulse
        maxBatchTime = 600;             % Maximun batch time in seconds
        lastBatch;
        repetition;
        pointsPerTR = 0;
        instPerTR = 0;
        roDephTime;
        phPhasTime;
    end
    methods (Access=private)
        function CreateParameters(obj)
            
            % Input parameters
            obj.CreateOneParameter('TE','TE time','ms',10*MyUnits.ms);
            obj.CreateOneParameter('TR','TR time','ms',50*MyUnits.ms);
            obj.CreateOneParameter('FOVRO','FOV Readout','mm',10*MyUnits.mm);
            obj.CreateOneParameter('FOVPH','FOV Phase','mm',10*MyUnits.mm);
            obj.CreateOneParameter('FOVSL','FOV Slice','mm',2*MyUnits.mm);
            obj.CreateOneParameter('NPOINTS','N Readout','',10);
            obj.CreateOneParameter('NPHASES','N Phase','',10);
            obj.CreateOneParameter('ROFACTOR','Readout Factor','',1);
            obj.CreateOneParameter('ACQTIME','Acquisition Time','ms',4*MyUnits.ms);
            obj.CreateOneParameter('REPETITIONDELAY','Repetition Delay','ms',50*MyUnits.ms);
            obj.CreateOneParameter('SLICEPOSITION','Slice Position','mm',0*MyUnits.mm);
            obj.CreateOneParameter('SLFACTOR','Slice Factor','',1);
            obj.CreateOneParameter('JUMPINGFACTOR','Sweep Mode','',0);
            obj.CreateOneParameter('DELTARO','RO Deviation','mm',0);
            obj.CreateOneParameter('DELTAPH','PH Deviation','mm',0);
            obj.CreateOneParameter('DELTASL','SL Deviation','mm',0);
            obj.CreateOneParameter('REFOCUSINGTIME','180� Pulse Time','us',400*MyUnits.us);
            obj.CreateOneParameter('PULSETIME','90� Pulse Time','us',200*MyUnits.us);
            obj.CreateOneParameter('ETL','Echo Train Length','',1);
            obj.CreateOneParameter('RFAMPLITUDE2','180� RF Amplitude','',0.1);
            obj.CreateOneParameter('RFAMPLITUDE','90� RF Amplitude','',0.1);
            obj.CreateOneParameter('PHFACTOR','Phase Factor','',1);
            obj.CreateOneParameter('PARTIALACQ', 'Acq Lines', '',0);
            obj.CreateOneParameter('SWEEPMODE','Sweep Mode','k2k/02k',0);
            obj.CreateOneParameter('DUMMY','Dummy Pulses','',0);
            obj.CreateOneParameter('PHASE1', 'Phase Ex', '�',0.0);
            obj.CreateOneParameter('PHASE2', 'Phase Re Odd', '�',344);
            obj.CreateOneParameter('PHASE3', 'Phase Re Even', '�',160);
            obj.CreateOneParameter('PHASE4', 'Phase 4', '',270.0);
            
            % Output parameters
            OutputParameters = {...
                'MAXPH','RO_STRENGTH','MAXSL'...
                };
            obj.OutputParNames = [containers.Map(obj.OutputParNames.keys(),obj.OutputParNames.values());containers.Map(OutputParameters,...
                {...
                'Start Phase', 'Readout amplitude', 'Start Slice'...
                })];
            obj.OutputParValues = [containers.Map(obj.OutputParValues.keys(),obj.OutputParValues.values());containers.Map(OutputParameters,...
                {...
                NaN,NaN,NaN...
                })];
            obj.OutputParUnits = [containers.Map(obj.OutputParUnits.keys(),obj.OutputParUnits.values());containers.Map(OutputParameters,...
                {...
                '','',''...
                })];
            for k=1:length(obj.OutputParameters)
                OutputParameters(strcmp(obj.OutputParameters{k},OutputParameters)) = [];
            end
            obj.OutputParameters = [obj.OutputParameters,OutputParameters];
            
            % Hidden parameters
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
                'BLANKINGDELAY','COILRISETIME',...
                'TRANSIENTTIME','SPOILERAMP',...
                'SPOILERTIME','SPECTRALWIDTH','REPETITIONDELAY',...
                'NSLICES','DELTASL','JUMPINGFACTOR','PHASE4'};


            % Rearange parameters
            obj.MoveParameter(7,5);
            obj.MoveParameter(9,6);
            obj.MoveParameter(10,7);
            obj.MoveParameter(19,8);
            obj.MoveParameter(20,9);
            obj.MoveParameter(25,8);
            obj.MoveParameter(29,9);
            obj.MoveParameter(27,10);
            obj.MoveParameter(13,26);
            obj.MoveParameter(30,33);
            obj.MoveParameter(26,23);
            obj.MoveParameter(27,24);
            obj.MoveParameter(34,8);
            obj.MoveParameter(20,19);
            obj.MoveParameter(36,8);
            obj.MoveParameter(10,15);
            obj.MoveParameter(10,16);
            obj.MoveParameter(37,17);
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = RARE2D(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'RARE2D';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
            
            obj.SetParameter('RFAMPLITUDE',0.01);
            obj.SetParameter('PULSETIME',200*MyUnits.us);
            obj.SetParameter('BLANKINGDELAY',200*MyUnits.us);
            obj.SetParameter('TRANSIENTTIME',0.1*MyUnits.us);
            obj.SetParameter('COILRISETIME',100*MyUnits.us);
            obj.SetParameter('NPOINTS',10);
            obj.SetParameter('NPHASES',10);
            obj.SetParameter('NSLICES',1);
            
            % This value is empherical and picked based on not creating eddy currents
            obj.SetParameter('FREQUENCY',42.58*MyUnits.MHz);
            
            % Grab the calibration data if there
            try
                obj.CalData = GradientCalibrationData();
            catch
                obj.CalData.X_TD = 1;
                obj.CalData.Y_TD = 1;
                obj.CalData.Z_TD = 1;
                obj.CalData.X_TV = 1;
                obj.CalData.Y_TV = 1;
                obj.CalData.Z_TV = 1;
            end
            
        end
        function UpdateTETR(obj)
            ETL = obj.GetParameter('ETL');
            parAcq = obj.GetParameter('PARTIALACQ');
            obj.TE = obj.GetParameter('TE');
            obj.TR = obj.GetParameter('TR');
            [~,~,~,ENSL,ENPH,~,~] = obj.ConfigureGradients(0);
            obj.NPH = obj.GetParameter('NPHASES')*ENPH+1*~ENPH;
            obj.NSL = obj.GetParameter('NSLICES')*ENSL+1*~ENSL;
            
            if(parAcq==0)
                nRep = obj.NPH/ETL;
            else
                nRep = obj.NPH/2/ETL+ceil(parAcq/ETL);
            end
            obj.TTotal = obj.TR*obj.GetParameter('NSCANS')*obj.NSL*nRep;
        end
        
        
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************



        function CreateSequence( obj, mode )
            % mode =  "0", average T2 maks acquisition,
            %         "1", normal execution,
            
            global scanner
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            
            % Configure gradients and axes
            [~,~,ENRO,ENSL,~,~,~] = obj.ConfigureGradients(0);
            
            % Calculate the number of line reads
            obj.nline_reads = 0;
            nPoints = obj.NRO;
            nPhases = obj.NPH;
            dummy = obj.GetParameter('DUMMY');
            
            % Initialize all vectors to zero with dynamic size
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs,obj.RFamps] = deal([]);
            
            % Load timing from RawData;
            crt             = obj.GetParameter('COILRISETIME')*1e6;
            blk             = scanner.BlankingDelay;
            ExTime          = obj.GetParameter('PULSETIME')*1e6;
            ReTime          = obj.GetParameter('REFOCUSINGTIME')*1e6;
            sliceRephaTime  = obj.sliceRephasingTime*1e6;
            roPhaseTime     = obj.roDephTime*1e6;
            phPhaseTime     = obj.phPhasTime*1e6;
            gradDelay       = scanner.grad_delay;
            acqTime         = obj.GetParameter('ACQTIME')*1e6;
            repDelay        = obj.RepDelay*1e6;
            
            % Gradient factors:
            roFactor = obj.GetParameter('ROFACTOR');
            slFactor = obj.GetParameter('SLFACTOR');
            phFactor = obj.GetParameter('PHFACTOR');
            
            % Shimming
            shimG = [shimR,shimP,shimS];
            dac = [obj.selectReadout,obj.selectPhase,obj.selectSlice];
            
            % Readout gradient amplitudes
            dephasingReadout = shimG(1)+obj.RO_strength_dep*ENRO*roFactor;
            rephasingReadout = shimG(1)+obj.RO_strength_rep*ENRO;

            % Slice slection gradient amplitudes
            dephasingSlice = shimG(3)+obj.SL_strength_dep*ENSL;
            rephasingSlice = shimG(3)+obj.SL_strength_rep*ENSL*slFactor;
                        
            % BEGIN THE PULSE PROGRAMMING
            iIns            = 1;
            excitationPhase = 0;
            ready           = 0;
            ETL             = obj.GetParameter('ETL');
            dummyRep        = 1;
            dummyIns        = 0;
            
            while(ready==0)
                if(dummyRep>dummy || mode==0)
                    %% Activate shimming
                    iIns = obj.RampGradient(iIns,[0 0 0],shimG,dac,0.3,1);
                    
                    %% Activate dephasing slice gradient
                    iIns = obj.RampGradient(iIns,shimG(3),dephasingSlice,dac(3),crt,obj.nSteps);
                    obj.DELAYs(iIns-1) = crt/obj.nSteps+gradDelay-blk;
                    
                    %% 90� RF pulse
                    iIns = obj.SetBlanking(iIns,1,blk);
                    iIns = obj.CreateRawPulse(iIns,excitationPhase,ExTime,0);
                    
                    %% Switch from dephasing to rephasing slice gradient
                    %  Also switch on the dephasing readout gradient
                    iIns = obj.RampGradient(iIns,[shimG(1),dephasingSlice],...
                        [dephasingReadout,shimG(3)],[dac(1),dac(3)],crt,obj.nSteps);
                    iIns = obj.RampGradient(iIns,shimG(3),rephasingSlice,dac(3),crt,obj.nSteps);
                    if(ExTime>=crt-2*gradDelay)
                        obj.DELAYs(iIns-1) = crt/obj.nSteps+sliceRephaTime;
                    else
                        obj.DELAYs(iIns-1) = crt/obj.nSteps;
                    end
                    
                    %% Deactivate rephasing slice gradient
                    iIns = obj.RampGradient(iIns,rephasingSlice,shimG(3),dac(3),crt,obj.nSteps);
                    obj.DELAYs(iIns-1) = crt/obj.nSteps+roPhaseTime-2*crt-sliceRephaTime;
                    
                    %% Switch from dephasing to rephasing gradients
                    iIns = obj.RampGradient(iIns,dephasingReadout,...
                        rephasingReadout,dac(1),crt,obj.nSteps);
                    obj.DELAYs(iIns-1) = crt/obj.nSteps+gradDelay-blk;
                    
                    %% Echo train
                    for nEcho = 1:ETL
                        phf = phFactor;
                        % 180� RF pulse
                        if(mod(ETL,2)==1)
                            refocusingPhase = 1;
                        else
                            refocusingPhase = 2;
                        end
                        iIns = obj.SetBlanking(iIns,1,blk);
                        obj.PhResets(iIns-1) = 0;
                        iIns = obj.CreateRawPulse(iIns,refocusingPhase,ReTime,1);
                        obj.FREQs(iIns-1) = 0;
                        obj.ENVELOPEs(iIns-1) = 7;
                        
                        % Dephasing phase gradient trapezoid
                        obj.repetition = obj.repetition+mode;
                        phaseGradient = obj.gradientList(obj.repetition+~mode)*mode;
                        iIns = obj.RampGradient(iIns,shimG(2),shimG(2)+phaseGradient,dac(2),crt,obj.nSteps);
                        obj.DELAYs(iIns-1) = crt/obj.nSteps+phPhaseTime;
                        iIns = obj.RampGradient(iIns,shimG(2)+phaseGradient,shimG(2),dac(2),crt,obj.nSteps);
                        obj.DELAYs(iIns-1) = crt/obj.nSteps+gradDelay;
                        
                        % Acquisition
                        iIns = obj.Acquisition(iIns,acqTime);
                        obj.nline_reads = obj.nline_reads+1;
                        
                        % Rephasing phase gradient trapezoid
                        iIns = obj.RampGradient(iIns,shimG(2),shimG(2)-phaseGradient*phf,dac(2),crt,obj.nSteps);
                        obj.DELAYs(iIns-1) = crt/obj.nSteps+phPhaseTime;
                        iIns = obj.RampGradient(iIns,shimG(2)-phaseGradient*phf,shimG(2),dac(2),crt,obj.nSteps);
                        obj.DELAYs(iIns-1) = crt/obj.nSteps+gradDelay-blk;
                        
                        if(obj.repetition==length(obj.gradientList))
                            break
                        end
                    end
                    
                    %% Repetition delay (1 instruction)
                    if(ETL==obj.GetParameter('NPHASES'))
                        repDelay = obj.GetParameter('TR')*1e6-sum(obj.DELAYs(dummyIns+1:end));
                        iIns = obj.RepetitionDelay(iIns,1,repDelay);
                    else
                        iIns = obj.RepetitionDelay(iIns,1,repDelay);
                    end
                else
                    % Activate shimming
                    iIns = obj.RampGradient(iIns,[0 0 0],shimG,dac,0.3,1);
                    
                    % Activate dephasing slice gradient
                    iIns = obj.RampGradient(iIns,shimG(3),dephasingSlice,dac(3),crt,obj.nSteps);
                    obj.DELAYs(iIns-1) = crt/obj.nSteps+gradDelay-blk;
                    
                    % 90� RF pulse
                    iIns = obj.SetBlanking(iIns,1,blk);
                    iIns = obj.CreateRawPulse(iIns,excitationPhase,ExTime,0);
                    
                    % Switch from dephasing to rephasing slice gradient
                    iIns = obj.RampGradient(iIns,dephasingSlice,rephasingSlice,dac(3),2*crt,2*obj.nSteps);
                    if(ExTime>=crt-2*gradDelay)
                        obj.DELAYs(iIns-1) = crt/obj.nSteps+sliceRephaTime;
                    else
                        obj.DELAYs(iIns-1) = crt/obj.nSteps;
                    end
                    
                    % Deactivate rephasing slice gradient
                    iIns = obj.RampGradient(iIns,rephasingSlice,shimG(3),dac(3),crt,obj.nSteps);
                    obj.DELAYs(iIns-1) = crt/obj.nSteps+roPhaseTime-2*crt-sliceRephaTime;
                    
                    % Get information
                    if(dummyRep==1)
                        dummyDel = sum(obj.DELAYs);
                    end
                    obj.DELAYs(iIns-1) = crt/obj.nSteps+obj.GetParameter('TR')*1e6-dummyDel;
                    dummyIns = iIns-1;
                    dummyRep = dummyRep+1;
                end
                % Check if it is the last line
                if(obj.repetition==length(obj.gradientList))
                    obj.lastBatch = 1;
                    disp('Last Batch!')
                end
                
                % Get the number of points per TR for the first repetition
                if(obj.repetition==ETL)
                    obj.pointsPerTR = obj.nline_reads*nPoints;
                    obj.instPerTR = iIns-1;
                end
                
                % Determine if a new repetition is possible in the current batch
                if(iIns-1+obj.instPerTR>=2048 || obj.nline_reads*nPoints+obj.pointsPerTR>16000 || obj.lastBatch==1 || mode==0)
                    ready = 1;
                end
            end
            obj.nInstructions = iIns-1;
            obj.PhasesPerBatch = obj.nline_reads;
        end

        
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************


        function [status,result] = Run(obj,iAcq)
            clc
            
            global rawData
            global scanner
            rawData = [];
            
            % Check which gradients are switched on
            [~,~,ENRO,ENSL,ENPH,ENSP,~] = obj.ConfigureGradients(0);
            
            % Field of View
            FOVRO = obj.GetParameter('FOVRO');
            FOVPH = obj.GetParameter('FOVPH');
            FOVSL = obj.GetParameter('FOVSL');
            POSSL = obj.GetParameter('SLICEPOSITION');
            DELRO = obj.GetParameter('DELTARO');
            DELPH = obj.GetParameter('DELTAPH');
            nScans = obj.GetParameter('NSCANS');
            parAcq = obj.GetParameter('PARTIALACQ');
            rawData.inputs.Seq = obj.SequenceName;
            rawData.inputs.NEX = nScans;
            rawData.inputs.partialAcquisition = parAcq;
            rawData.inputs.fov = [FOVRO,FOVPH,FOVSL];
            rawData.inputs.slicePosition = POSSL;
            rawData.inputs.fovDeviation = [DELRO,DELPH];
            sweepMode = obj.GetParameter('SWEEPMODE');
            if(sweepMode==0)
                rawData.inputs.sweepMode = 'k2k';
            else
                rawData.inputs.sweepMode = '02k';
            end
            rawData.inputs.dummyPulses = obj.GetParameter('DUMMY');
            
            % Matrix size
            obj.NRO = obj.GetParameter('NPOINTS');
            obj.NPH = obj.GetParameter('NPHASES')*ENPH+1*~ENPH;
            obj.NSL = 1;
            rawData.inputs.axes = [obj.GetParameter('READOUT'),...
                obj.GetParameter('PHASE'),obj.GetParameter('SLICE')];
            rawData.inputs.axisEnable = [ENRO,ENPH,ENSL];
            rawData.inputs.nPoints = [obj.NRO,obj.NPH,obj.NSL];
            
            % Resolution
            rawData.auxiliar.resolution =...
                [FOVRO/obj.NRO,FOVPH/obj.NPH,FOVSL];
            
            % Parameters for image weighting
            TE = obj.GetParameter('TE');
            TR = obj.GetParameter('TR');
            ETL = obj.GetParameter('ETL');
            rawData.inputs.TE = TE;
            rawData.inputs.TR = TR;
            rawData.inputs.ETL = ETL;
            
            % RF parameters
            exTime = obj.GetParameter('PULSETIME');
            reTime = obj.GetParameter('REFOCUSINGTIME');
            obj.acq_time = obj.GetParameter('ACQTIME');
            BW = obj.NRO/obj.acq_time;
            obj.SetParameter('SPECTRALWIDTH',BW);
            rawData.inputs.exitationTime = exTime;
            rawData.inputs.refocusingTime = reTime;
            rawData.inputs.exitationAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.refocusingAmplitude = obj.GetParameter('RFAMPLITUDE2');
            rawData.inputs.acqTime = obj.acq_time;
            rawData.auxiliar.bandwidth = BW;
            rawData.inputs.exPhase = obj.GetParameter('PHASE1');
            rawData.inputs.oddRePhase = obj.GetParameter('PHASE2');
            rawData.inputs.evenRePhase = obj.GetParameter('PHASE3');
            
            % Miscellaneous
            roFactor = obj.GetParameter('ROFACTOR');
            phFactor = obj.GetParameter('PHFACTOR');
            slFactor = obj.GetParameter('SLFACTOR');
            coilRiseTime = obj.GetParameter('COILRISETIME');
            gradientDelay = scanner.grad_delay*1e-6;
            rawData.auxiliar.coilRiseTime = coilRiseTime;
            rawData.auxiliar.gradientDelay = gradientDelay;
            rawData.inputs.gradFactor = [roFactor,phFactor,slFactor];
            
            % Readout dephasing time
            obj.roDephTime = (TE-exTime-reTime)/2-2*coilRiseTime-gradientDelay;
            rawData.auxiliar.roDephasingTime = obj.roDephTime;

            % Phase de and rephasing time
            obj.phPhasTime = (TE-reTime-obj.acq_time)/2-2*coilRiseTime-gradientDelay;
            rawData.auxiliar.phPhaseTime = obj.phPhasTime;
            
            % Slice rephasing time
            obj.sliceRephasingTime = 0.5*exTime+gradientDelay-0.5*coilRiseTime;
            if obj.sliceRephasingTime<0
                obj.sliceRephasingTime = 0;
            end
            rawData.auxiliar.sliceRephasingTime = obj.sliceRephasingTime;
            
            % Readout gradients
            c = obj.ReorganizeCalibrationData();
            obj.RO_strength_rep = BW./(obj.gam*FOVRO)*ENRO;
            obj.RO_strength_dep = 0.5*obj.RO_strength_rep*(TE-coilRiseTime-reTime)/...
                (coilRiseTime+obj.roDephTime);
            rawData.auxiliar.dephasingGradient = obj.RO_strength_dep;
            rawData.auxiliar.rephasingGradient = obj.RO_strength_rep;
            if abs(obj.RO_strength_dep) >= 0.72*c(1)
                warndlg('The readout dephasing gradient is too high.','Readout error')
                return;
            end
            if abs(obj.RO_strength_rep) >= 0.72*c(1)
                warndlg('The readout rephasing gradient is too high.','Readout error')
                return;
            end
            
            % Slice gradient
            obj.SL_strength_dep = 7/(obj.gam*FOVSL*exTime)*ENSL;               % 7 is the number of lobes in the sinc function (3 lobes each side and 1 main lobe)
            if(0.5*exTime+gradientDelay>0.5*coilRiseTime)
                obj.SL_strength_rep = -obj.SL_strength_dep*ENSL;
            else
                obj.SL_strength_rep = -obj.SL_strength_dep*(0.5*exTime+gradientDelay+0.5*coilRiseTime)/coilRiseTime*ENSL;
            end
            rawData.auxiliar.sliceDephasingGradient = obj.SL_strength_dep;
            rawData.auxiliar.sliceRephasingGradient = obj.SL_strength_rep;
            if abs(obj.SL_strength_dep) >= 0.72*c(3)
                warndlg('The slice dephasing gradient is too high. Try making the RF pulse time longer or making the slice thicker.','Slice error')
                return;
            end
            if abs(obj.SL_strength_rep) >= 0.72*c(3)
                warndlg('The slice rephasing gradient is too high. Try making the RF pulse time shorter.','Slice error')
                return;
            end
            
            % Phase gradient
            indF = obj.GetIndex(obj.NPH,ETL,parAcq,sweepMode);
            PH_strength_max = obj.NPH/(2*obj.gam*(obj.phPhasTime+coilRiseTime)*FOVPH);
            phaseGradients = linspace(-PH_strength_max,PH_strength_max,obj.NPH*ENPH+1*~ENPH)'*ENPH;
            phaseGradients = phaseGradients(indF);
            rawData.auxiliar.phaseGradient = PH_strength_max*ENPH;
            rawData.auxiliar.gradientList = phaseGradients;
            obj.gradientList = phaseGradients;
            if abs(PH_strength_max)>=0.72*c(2)
                warndlg('The phase dephasing gradient is too high.','Phase error')
                return;
            end
            
            % Frequency offset to do slice selection
            obj.SliceFreqOffset = obj.gam*obj.SL_strength_dep*POSSL*ENSL;
            rawData.auxiliar.freqOffsetSL = obj.SliceFreqOffset;
            
            % Set the gradient amplitudes in arbitrary units
            obj.RO_strength_rep = obj.RO_strength_rep/c(1);
            obj.RO_strength_dep = obj.RO_strength_dep/c(1);
            obj.SL_strength_rep = obj.SL_strength_rep/c(3);
            obj.SL_strength_dep = obj.SL_strength_dep/c(3);
            obj.gradientList = obj.gradientList/c(2);
            
            % Repetition delay
            obj.RepDelay  = TR-(scanner.BlankingDelay*1e-6+0.5*exTime+ETL*TE+...
                0.5*obj.acq_time+2*coilRiseTime+obj.phPhasTime);
            rawData.auxiliar.repetitionDelay = obj.RepDelay;
            if obj.RepDelay <=0
                warndlg('The TR is too short','TR error')
                return;
            end
            obj.SetParameter('REPETITIONDELAY',obj.RepDelay);
            
            nPoints = obj.NRO;
            nSlices = obj.NSL;
            nPhases = obj.NPH;
            
            % Get value to normalize fid to Volts
            bwCalNormVal = obj.calculate_bwCalNormVal;
            
            Data2D = [];
            obj.repetition = 0;
            obj.lastBatch = 0;
            iBatch = 0;
            tic;
            nFreq = 0;
            while(obj.lastBatch==0)
                tA = clock;
                iBatch = iBatch+1;
                if(iBatch==1)
                    fprintf('Batch %1.0f, estimating remaining time \n',iBatch);
                else
                    tC = tC*(nPhaseBatches-iBatch+1);
                    fprintf('Batch %1.0f/%1.0f, remaining time: %1.0f s \n',iBatch,nPhaseBatches,tC);
                end
                
                % Calibrate frequency
                if(ENSP && (toc>300 || iBatch==1))
                    fprintf('Calibrating frequency... \n')
                    % Get central frequency
                    f0 = obj.GetFIDParameters;
                    obj.SetParameter('FREQUENCY',f0);
                    nFreq = nFreq+1;
                    rawData.aux.freqListRadial(nFreq) = f0;
                    fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
                    tic;
                end
                
                % Calibrate average T2
                if(iBatch==1)
                    fprintf('Average T2 calibration... \n \n')
                    nScans = obj.GetParameter('NSCANS');
                    obj.SetParameter('NSCANS',ceil(10/obj.GetParameter('TR')));
                    obj.CreateSequence(0);
                    obj.Sequence2String;
                    fname = 'TempAverageT2Calibration.bat';
                    obj.WriteBatchFile(fname);
                    [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                    if status == 0 && ~isempty(result)
                        C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                        res = C{:};
                        obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                        obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                        obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                        obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                        obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                    else
                        warndlg('Average T2 calibration aborted!','Warning')
                        return;
                    end
                    DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                    cDataBatch = complex(DataBatch(:,1),DataBatch(:,2))/bwCalNormVal;
                    avT2Cal = reshape(cDataBatch,nPoints,ETL);
                    figure(5)
                    subplot(1,2,1)
                    imagesc(abs(avT2Cal))
                    title('T2 cal abs')
                    subplot(1,2,2)
                    imagesc(angle(avT2Cal))
                    title('T2 cal phase')
                    colormap gray
                    avT2Cal = avT2Cal(ceil(end/2),:);
                    rawData.kSpace.avT2Cal = avT2Cal;
                    obj.SetParameter('NSCANS',nScans);
                end
                
                % NOW RUN ACTUAL ACQUISITION
                obj.CreateSequence(1);
                if(iBatch==1)
                    nPhaseBatches = ceil(nPhases/obj.PhasesPerBatch);
                end
                if(sum(obj.DELAYs<0)~=0)
                    warndlg('At least one delay is negative!','Delay Error')
                    return;
                end
                obj.Sequence2String;
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Batch aborted!','Warning')
                    return;
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2))/bwCalNormVal;
                Data2D = cat(1,Data2D,cDataBatch);
                
                tB = clock;
                tC = etime(tB,tA);
            end
            
            % Do zero padding if partial acquisition
            if(parAcq~=0)
                Data2D(nPoints*(nPhases/2+parAcq):nPoints*nPhases) = 0;
            end
            
            % Apply mask if desired
            Data2D = reshape(Data2D,nPoints,nPhases);
            data2dRaw = zeros(size(Data2D));
            data2dCalPha = zeros(size(Data2D));
            data2dCalPhaMag = zeros(size(Data2D));             
            nn = 0;
            for ii = 1:nPhases*(parAcq==0)+(nPhases/2+parAcq)*(parAcq~=0)
                nn = nn+1;
                ind = indF(ii);
                data2dRaw(:,ind) = Data2D(:,ii);
                % Amplitude and Phase mask
                data2dCalPhaMag(:,ind) = Data2D(:,ii)/abs(avT2Cal(nn))*exp(-1i*(angle(avT2Cal(nn))-angle(avT2Cal(1))));
                data2dCalPha(:,ind) = Data2D(:,ii)*exp(-1i*(angle(avT2Cal(nn))-angle(avT2Cal(1))));
                if nn==ETL
                    nn = 0;
                end
            end
            
            imagenRaw = abs(ifftshift(ifftn(data2dRaw)));
            imagenCalPhaMag = abs(ifftshift(ifftn(data2dCalPhaMag)));
            imagenCalPha = abs(ifftshift(ifftn(data2dCalPha)));
            figure(4)
            set(gcf, 'Position', [200, 550, 1400, 400])
            subplot(1,3,1)
            imagesc(imagenRaw);
            title('raw image')
            subplot(1,3,2)
            imagesc(imagenCalPhaMag);
            title('phase and mag calibrated image')
            subplot(1,3,3)
            imagesc(imagenCalPha);
            title('phase calibrated image')
            colormap gray
            
            Data2D = reshape(data2dCalPha,nPoints*nPhases,1);
            clear Data2Dp
            
            % k-space points
            kMax = 0.5./rawData.auxiliar.resolution;
            kPointsX = linspace(-kMax(1),kMax(1),nPoints);
            kPointsY = linspace(-kMax(2),kMax(2),nPhases);
            kPointsZ = linspace(-kMax(3),kMax(3),nSlices);
            [kPointsX,kPointsY,kPointsZ] = meshgrid(kPointsX,kPointsY,kPointsZ);
            kPointsX = permute(kPointsX,[2,1]);
            kPointsY = permute(kPointsY,[2,1]);
            kPointsZ = permute(kPointsZ,[2,1]);
            kSpaceValues = [kPointsX(:),kPointsY(:),kPointsZ(:)];
            phase = exp(-2*pi*1i*(DELRO*kPointsX(:)+DELPH*kPointsY(:)));
            kSpaceValues = [kSpaceValues,Data2D.*phase];
            
            rawData.aux.kMax = kMax;
            rawData.kSpace.sampled = kSpaceValues;
            kSpaceValues = reshape(kSpaceValues(:,4),obj.NRO,obj.NPH,obj.NSL);
            imagen = abs(ifftshift(ifftn(kSpaceValues)));
            rawData.kSpace.imagen = imagen;
            
            % Save rawData
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


        function ind = GetIndex(obj,np,ETL,parAcq,sweepMode)
            if(sweepMode==0)
                ind = linspace(1,np,np);
                ind(end/2+1:end) = ind(end:-1:end/2+1);
                return;
            end
            if(np==1)
                ind = 1;
                return;
            end
            if(ETL==np)
                if(parAcq==0)
                    indA = (1:np)';
                    ind1 = indA(np/2:-1:1);
                    ind2 = indA(np/2+1:np);
                    
                    ind = zeros(np,1);
                    ind(1:2:end-1) = ind1;
                    ind(2:2:end) = ind2;
                else
                    ind = (np/2+parAcq:-1:1)';
                end
            else
                if(parAcq==0)
                    boxSize = np/(2*ETL);
                    
                    indA = (1:np)';
                    ind1 = indA(np/2:-1:1);
                    ind2 = indA(np/2+1:np);
                    
                    ind11 = zeros(size(ind1));
                    ind22 = zeros(size(ind2));
                    
                    for ii = 1:np/2/ETL
                        ind11((ii-1)*ETL+1:ii*ETL) = ind1(ii:boxSize:np/2-boxSize+ii);
                        ind22((ii-1)*ETL+1:ii*ETL) = ind2(ii:boxSize:np/2-boxSize+ii);
                    end
                    ind = [ind11;ind22];
                else
                    boxSize = np/(2*ETL);
                    
                    indA = (1:np)';
                    ind1 = indA(np/2:-1:1);
                    ind2 = indA(np/2+1:np/2+parAcq);
                    
                    ind11 = zeros(size(ind1));
                    ind22 = zeros(size(ind2));
                    
                    for ii = 1:np/2/ETL
                        ind11((ii-1)*ETL+1:ii*ETL) = ind1(ii:boxSize:np/2-boxSize+ii);
                    end
                    ind = [ind11;ind2];
                end
            end
        end
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        
    end
end

