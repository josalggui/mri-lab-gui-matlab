classdef SLICECALIBRATION < MRI_BlankSequence
    % 29/10/2020
    % Jos� Miguel Algar�n Guisado
    % Ph.D.
    % MRILab
    % Institute for Instrumentation in Molecular Imaging (I3M)
    % CSIC- UPV
    % josalggui@i3m.upv.es

    properties
        StartSlice;
        EndSlice;
        PhasesPerBatch = 20;
        PhasesinThisBatch = 1;
        StartPhase;
        EndPhase;
        slice_ampl;
        slice_ampl_re;
        acq_time;
        PhaseTime;
        RepDelay;
        deX;
        reX;
        deY;
        reY;
        deZ;
        reZ;
        nPhasesBatch;
        gam = 42.57E6; %gyromagnetic ratio, 42.57 MHz/T
        sliceRephasingTime;
    end
    methods (Access=private)
        function CreateParameters(obj)
            
            % Input parameters
            obj.CreateOneParameter('TR','TR time','ms',500*MyUnits.ms);
            obj.CreateOneParameter('NPOINTS','N Readout','',100);
            obj.CreateOneParameter('ACQTIME','Acquisition Time','ms',4*MyUnits.ms);
            obj.CreateOneParameter('REPETITIONDELAY','Repetition Delay','ms',50*MyUnits.ms);
            obj.CreateOneParameter('FOVSL','Slice Width','mm',2*MyUnits.mm);
            obj.CreateOneParameter('MINSLFACTOR','Min SL Factor','',1);
            obj.CreateOneParameter('MAXSLFACTOR','Max SL Factor','',1.2);
            obj.CreateOneParameter('NUMSLFACTOR','Num SL Factor','',10);
            obj.CreateOneParameter('DUMMYPULSES','Dummy Pulses','',0);
            
            % Output parameters
            OutputParameters = {...
                'MAXPH','RO_STRENGTH','MAXSL'...
                };
            obj.OutputParNames = [containers.Map(obj.OutputParNames.keys(),obj.OutputParNames.values());containers.Map(OutputParameters,...
                {...
                'Start Phase', 'Readout amplitude', 'Start Slice'...
                })];
            obj.OutputParValues = [containers.Map(obj.OutputParValues.keys(),obj.OutputParValues.values());containers.Map(OutputParameters,...
                {...
                NaN,NaN,NaN...
                })];
            obj.OutputParUnits = [containers.Map(obj.OutputParUnits.keys(),obj.OutputParUnits.values());containers.Map(OutputParameters,...
                {...
                '','',''...
                })];
            for k=1:length(obj.OutputParameters)
                OutputParameters(strcmp(obj.OutputParameters{k},OutputParameters)) = [];
            end
            obj.OutputParameters = [obj.OutputParameters,OutputParameters];
            
            % Hidden parameters
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
                'BLANKINGDELAY','COILRISETIME',...
                'TRANSIENTTIME','SPOILERAMP',...
                'SPOILERTIME','SPECTRALWIDTH','REPETITIONDELAY',...
                'NSLICES','READOUT','PHASE','SLICE','NPOINTS','ACQUISITIONTIME'};


            % Rearange parameters
%             obj.MoveParameter(7,5);
            
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = SLICECALIBRATION(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'SLICECALIBRATION';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
            
            obj.SetParameter('RFAMPLITUDE',0.01);
            obj.SetParameter('PULSETIME',200*MyUnits.us);
            obj.SetParameter('BLANKINGDELAY',200*MyUnits.us);
            obj.SetParameter('TRANSIENTTIME',0.1*MyUnits.us);
            obj.SetParameter('COILRISETIME',100*MyUnits.us);
            obj.SetParameter('NPOINTS',100);
            obj.SetParameter('RFAMPLITUDE',0.1);
            obj.SetParameter('NPOINTS',1);
            obj.SetParameter('ACQTIME',1*MyUnits.ms);
            
            % This value is empherical and picked based on not creating eddy currents
            obj.SetParameter('FREQUENCY',42.58*MyUnits.MHz);
        end
        function UpdateTETR(obj)
            obj.TTotal = 0;
        end
        
        
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************



        function CreateSequence( obj, mode )
            % mode =  "0", average T2 maks acquisition,
            %         "1", normal execution,
            
            global scanner
            
            % first set the appropriate axes as were decided
            [shimZ,shimY,shimX]=SelectAxes(obj);
            
            % Configure gradients and axes
            [~,~,~,~,~,~,~] = obj.ConfigureGradients(0);
            
            % Initialize all vectors to zero with dynamic size
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs,obj.RFamps] = deal([]);
            
            % Load timing from RawData;
            crt             = obj.GetParameter('COILRISETIME')*1e6;
            blk             = scanner.BlankingDelay;
            ExTime          = obj.GetParameter('PULSETIME')*1e6;
            sliceRephaTime  = obj.sliceRephasingTime*1e6;
            gradDelay       = scanner.grad_delay;
            acqTime         = obj.GetParameter('ACQTIME')*1e6;
            repDelay        = obj.RepDelay*1e6;
            
            % Gradient factors:
            minSlFactor = obj.GetParameter('MINSLFACTOR');
            maxSlFactor = obj.GetParameter('MAXSLFACTOR');
            numSlFactor = obj.GetParameter('NUMSLFACTOR');
            factor = linspace(minSlFactor,maxSlFactor,numSlFactor);
            
            % Shimming
            shimG = [shimX,shimY,shimZ];
            dac = [obj.selectReadout,obj.selectPhase,obj.selectSlice];

            % Slice slection gradient amplitudes
            prephase = [obj.deX,obj.deY,obj.deZ];
            rephase  = [obj.reX,obj.reY,obj.reZ];
            
            % Dummy pulses
            dummy = obj.GetParameter('DUMMYPULSES');
            iIns = 1;
            for jj = 1:dummy
                % Activate shimming
                iIns = obj.RampGradient(iIns,[0 0 0],shimG,dac,0.3,1);
                
                % Activate dephasing slice gradient
                iIns = obj.RampGradient(iIns,shimG(1),shimG(1),dac(1),crt,obj.nSteps);
                obj.DELAYs(iIns-1) = crt/obj.nSteps+gradDelay-blk;
                
                % 90� RF pulse
                iIns = obj.SetBlanking(iIns,1,blk);
                iIns = obj.CreateRawPulse(iIns,0,ExTime,0);
                
                % Switch from dephasing to rephasing slice gradient
                iIns = obj.RampGradient(iIns,shimG(1),shimG(1),dac(1),crt,obj.nSteps);
                iIns = obj.RampGradient(iIns,shimG(1),shimG(1),dac(1),crt,obj.nSteps);
                if(ExTime>=crt-2*gradDelay)
                    obj.DELAYs(iIns-1) = crt/obj.nSteps+sliceRephaTime;
                else
                    obj.DELAYs(iIns-1) = crt/obj.nSteps;
                end
                
                % Deactivate rephasing slice gradient
                iIns = obj.RampGradient(iIns,shimG(1),shimG(1),dac(1),crt,obj.nSteps);
                
                % Read data
                iIns = obj.RepetitionDelay(iIns,0,acqTime);
                obj.nline_reads = obj.nline_reads+1;
                
                % Delay to TR
                iIns = obj.RepetitionDelay(iIns,1,repDelay);
            end
            
            % BEGIN THE PULSE PROGRAMMING
            obj.nline_reads = 0;
            for ii = 1:3
                for jj = 1:numSlFactor
                    % Activate shimming
                    iIns = obj.RampGradient(iIns,[0 0 0],shimG,dac,0.3,1);
                    
                    % Activate dephasing slice gradient
                    iIns = obj.RampGradient(iIns,shimG(ii),shimG(ii)+prephase(ii),dac(ii),crt,obj.nSteps);
                    obj.DELAYs(iIns-1) = crt/obj.nSteps+gradDelay-blk;
                    
                    % 90� RF pulse
                    iIns = obj.SetBlanking(iIns,1,blk);
                    iIns = obj.CreateRawPulse(iIns,0,ExTime,0);
                    
                    % Switch from dephasing to rephasing slice gradient
                    iIns = obj.RampGradient(iIns,shimG(ii)+prephase(ii),shimG(ii),dac(ii),crt,obj.nSteps);
                    iIns = obj.RampGradient(iIns,shimG(ii),shimG(ii)+rephase(ii)*factor(jj),dac(ii),crt,obj.nSteps);
                    if(ExTime>=crt-2*gradDelay)
                        obj.DELAYs(iIns-1) = crt/obj.nSteps+sliceRephaTime;
                    else
                        obj.DELAYs(iIns-1) = crt/obj.nSteps;
                    end
                    
                    % Deactivate rephasing slice gradient
                    iIns = obj.RampGradient(iIns,shimG(ii)+rephase(ii)*factor(jj),shimG(ii),dac(ii),crt,obj.nSteps);
                    
                    % Read data
                    iIns = obj.Acquisition(iIns,acqTime);
                    obj.nline_reads = obj.nline_reads+1;
                    
                    % Delay to TR
                    iIns = obj.RepetitionDelay(iIns,1,repDelay);
                end
            end
                
            obj.nInstructions = iIns-1;
        end

        
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************


        function [status,result] = Run(obj,iAcq)
            clc
            
            global rawData
            global scanner
            rawData = [];
            
            % Check which gradients are switched on
            [~,~,~,~,~,~,~] = obj.ConfigureGradients(0);
            
            % Field of View
            FOVSL = obj.GetParameter('FOVSL');
            nScans = obj.GetParameter('NSCANS');
            rawData.inputs.Seq = obj.SequenceName;
            rawData.inputs.NEX = nScans;
            rawData.inputs.sliceWidth = FOVSL;
            
            % Matrix size
            nPoints = obj.GetParameter('NPOINTS');
            rawData.inputs.axes = [obj.GetParameter('READOUT'),...
                obj.GetParameter('PHASE'),obj.GetParameter('SLICE')];
            rawData.inputs.nPoints = nPoints;
            
            % TR
            TR = obj.GetParameter('TR');
            rawData.inputs.TR = TR;
            
            % Dummy pulses
            rawData.inputs.dummy = obj.GetParameter('DUMMYPULSES');
            
            % RF parameters
            exTime = obj.GetParameter('PULSETIME');
            obj.acq_time = obj.GetParameter('ACQTIME');
            BW = nPoints/obj.acq_time;
            obj.SetParameter('SPECTRALWIDTH',BW);
            rawData.inputs.exitationTime = exTime;
            rawData.inputs.exitationAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.acqTime = obj.acq_time;
            rawData.auxiliar.bandwidth = BW;
            
            % Miscellaneous
            minSlFactor = obj.GetParameter('MINSLFACTOR');
            maxSlFactor = obj.GetParameter('MAXSLFACTOR');
            numSlFactor = obj.GetParameter('NUMSLFACTOR');
            coilRiseTime = obj.GetParameter('COILRISETIME');
            gradientDelay = scanner.grad_delay*1e-6;
            rawData.auxiliar.coilRiseTime = coilRiseTime;
            rawData.auxiliar.gradientDelay = gradientDelay;
            rawData.inputs.minSlFactor = minSlFactor;
            rawData.inputs.maxSlFactor = maxSlFactor;
            rawData.inputs.numSlFactor = numSlFactor;
            
            % Slice rephasing time
            obj.sliceRephasingTime = 0.5*exTime+gradientDelay-0.5*coilRiseTime;
            if obj.sliceRephasingTime<0
                obj.sliceRephasingTime = 0;
            end
            rawData.auxiliar.sliceRephasingTime = obj.sliceRephasingTime;
            
            % Slice gradient
            dephasingG = 7/(obj.gam*FOVSL*exTime);               % 7 is the number of lobes in the sinc function (3 lobes each side and 1 main lobe)
            if(0.5*exTime+gradientDelay>0.5*coilRiseTime)
                rephasingG = -dephasingG;
            else
                rephasingG = -dephasingG*(0.5*exTime+gradientDelay+0.5*coilRiseTime)/coilRiseTime*ENSL;
            end
            rawData.auxiliar.sliceDephasingGradient = dephasingG;
            rawData.auxiliar.sliceRephasingGradient = rephasingG;
            
            % Set the gradient amplitudes in arbitrary units
            c = obj.ReorganizeCalibrationData();
            obj.deX = dephasingG/c(1);
            obj.reX = rephasingG/c(1);
            obj.deY = dephasingG/c(2);
            obj.reY = rephasingG/c(2);
            obj.deZ = dephasingG/c(3);
            obj.reZ = rephasingG/c(3);
            
            % Repetition delay
            obj.RepDelay  = TR-(4*coilRiseTime+scanner.grad_delay*1e-6+obj.acq_time+...
                obj.sliceRephasingTime+obj.acq_time);
            rawData.auxiliar.repetitionDelay = obj.RepDelay;
            obj.SetParameter('REPETITIONDELAY',obj.RepDelay);
            
            % Get value to normalize fid to Volts
            bwCalNormVal = obj.calculate_bwCalNormVal;
            
            % NOW RUN ACTUAL ACQUISITION
            obj.CreateSequence(1);
            if(sum(obj.DELAYs<0)~=0)
                warndlg('At least one delay is negative!','Delay Error')
                return;
            end
            if(sum(abs(obj.AMPs)>0.72)~=0)
                warndlg('At least one gradient amplifier requires more than 400 A')
                return;
            end
            if(obj.nInstructions>=2048)
                warndlg('Too many instructions')
                return;
            end
            obj.Sequence2String;
            fname = 'TempBatch.bat';
            obj.WriteBatchFile(fname);
            [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
            if status == 0 && ~isempty(result)
                C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                res = C{:};
                obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
            else
                warndlg('Batch aborted!','Warning')
                return;
            end
            data = load(sprintf('%s.txt',obj.OutputFilename));
            data = abs(reshape(complex(data(:,1),data(:,2))/bwCalNormVal,nPoints*numSlFactor,3));
            factor = linspace(minSlFactor,maxSlFactor,numSlFactor)';
            data = [factor,data];
            
            rawData.output.data = data;

            % Save rawData
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        
    end
end

