classdef SNR_FID < MRI_BlankSequence   
    properties
        maxRepetitionsPerBatch;
        nPhasesBatch;
        lastBatch;
        RepetitionTime;
        AcquisitionTime;
        EndPhase;
        PhasesPerBatch = 20;
    end
    methods (Access=private)
        function CreateParameters(obj)
            obj.CreateOneParameter('TRANSIENTTIME','Dead Time','us',100*MyUnits.us);
            obj.CreateOneParameter('REPETITIONTIME','TR','ms',50*MyUnits.ms);
            obj.CreateOneParameter('ACQUISITIONTIME','Acquisition time','ms',2*MyUnits.ms);
            obj.CreateOneParameter('NUMBERFIDS','Number of FIDs','',20);
            
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT'...
                'READOUT','PHASE','SLICE','COILRISETIME','SPOILERTIME',...
                'SPOILERAMP','NSLICES','BLANKINGDELAY','NPOINTS'};
        end  
    end
    
    methods
        function obj = SNR_FID(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'SNR_FID';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
        end
        
        function UpdateTETR(obj)
            obj.TTotal = obj.GetParameter('REPETITIONTIME') * obj.GetParameter('NUMBERFIDS');
        end
         
        function CreateSequence( obj)
            [shimS,shimP,shimR] = SelectAxes(obj);     
            acqTime = obj.GetParameter('ACQUISITIONTIME')*1e6;
            bw=obj.GetParameter('SPECTRALWIDTH');
            
            
            nRepet = obj.GetParameter('NUMBERFIDS');
            pulseTime90 = obj.GetParameter('PULSETIME')*1e6;
            deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
            TR = obj.GetParameter('REPETITIONTIME')*1e6;
            blkDelay = obj.GetParameter('BLANKINGDELAY')*1e6;
            
            obj.SetParameter('NPOINTS',bw*acqTime*1E-6);
            
            obj.nline_reads = nRepet;   
            rfphase=0;
            
             obj.nInstructions = (4+8*nRepet);
            [obj.RFamps,obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            
            
            iIns = 1;
            
            iIns = obj.CreatePulse(iIns,rfphase,blkDelay,pulseTime90,deadTime);
            iIns = obj.RepetitionDelay (iIns,1,TR-deadTime); 
            for i=1:nRepet
                %% Shimming all gradients
                iIns = obj.PulseGradient (iIns,shimR,2,0.1);
                iIns = obj.PulseGradient (iIns,shimP,0,0.1);
                iIns = obj.PulseGradient (iIns,shimS,1,0.1);
                
                %% 90� pulse
                iIns = obj.CreatePulse(iIns,rfphase,blkDelay,pulseTime90,deadTime);
                
                %% Acquisition
                iIns = obj.Acquisition(iIns,acqTime);
                
                %% RepetitionDelay
                iIns = obj.RepetitionDelay (iIns,1,TR-acqTime-deadTime); 
            end  
            obj.nInstructions = iIns-1;
        end
        
        function [status,result] = Run(obj,iAcq)
            clc
            global rawData;
            rawData = [];
            
            % Input data
            steps = obj.GetParameter('NUMBERFIDS');
            pulseTime90 = obj.GetParameter('PULSETIME');
            deadTime = obj.GetParameter('TRANSIENTTIME');
            blkDelay = obj.GetParameter('BLANKINGDELAY');
            acqTime = obj.GetParameter('ACQUISITIONTIME');
            bw = obj.GetParameter('SPECTRALWIDTH');
            nPoints = bw*acqTime;
            obj.SetParameter('NPOINTS',nPoints);         

            rawData.inputs.nPoints = obj.GetParameter('NPOINTS');   
            rawData.inputs.rfAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.pulseTime90 = pulseTime90;
            rawData.inputs.deadTime = deadTime;
            rawData.inputs.TR = obj.GetParameter('REPETITIONTIME');
            rawData.inputs.acqTime = acqTime;
            rawData.inputs.Nscans =  obj.GetParameter('NSCANS');

            if nPoints*steps >= 16000
                warndlg('Number of points * steps >16000.','N� points error')
                return
            end

            % Load amplitude calibration for 1V
%             bwCal = load('rawData-BW-Calibration.mat');
%             bwCalVal = bwCal.rawData.outputs.fidMean;
%             bwCalVec = logspace(log10(bwCal.rawData.inputs.bwMin),...
%                 log10(bwCal.rawData.inputs.bwMax),...
%                 bwCal.rawData.inputs.steps);
%             [~,bwCalNormPos] = min(abs(bwCalVec-bw));
%             bwCalNormVal = bwCalVal(bwCalNormPos);
                     
            % Run sequence
            obj.CreateSequence;
            obj.Sequence2String;
            fname = 'TempBatch.bat';
            obj.WriteBatchFile(fname);
            [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
            if status == 0 && ~isempty(result)
                C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                res = C{:};
                obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
%                 obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
            else
                warndlg('Batch aborted!','Warning')
                return;
            end
            DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
            cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
            fidList = cDataBatch;
%           fidList = cDataBatch/bwCalNormVal;

            time=linspace(0,acqTime,nPoints)';
            signals=reshape(fidList,[nPoints,steps]);
            meanfid=mean(abs(signals),2);
            dev=std(abs(signals'))';
            snr=abs(meanfid)./dev;
            
            rawData.outputs.signals = signals;
            rawData.outputs.snr = [time,snr];

            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
    end
end