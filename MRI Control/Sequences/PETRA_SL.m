classdef PETRA_SL < MRI_BlankSequence
    %%
    
    % PETRA bSSFP CODE
    
    %*******************************************************
    %** J. M. Algar�n                                     **
    %** CSIC/UPV                                          **
    %** I3M                                               **
    %** Avda. dels Tarongers, 12,46022, Valencia, (Spain) **
    %** Tel: +34 960 728 111                              **
    %** email: josalggui@i3m.upv.es                       **
    %*******************************************************
    
    %%
    
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    %  Elena Diaz Caballero
    properties
        PhasesPerBatch = 20;
        nPhasesBatch;
        Axis1Vector;
        Axis2Vector;
        Axis3Vector;
        StartSlice;
        lastBatch;
        EndSlice;
        StartPhase;
        EndPhase;
        Stage;              % ZTE -> 1, Single Point ->2 or Prescan -> 0
        AcquisitionTime;
        slice_ampl;
        grSpinlock;
        b1Spinlock;
        repetition = 0;
        gRingDownCal = [0.02 0.01];   % Gradients for ring-down calibration
        line = 0;
    end
    methods (Access=private)
        function CreateParameters(obj)
            
            % HELP %
            % I include here new inputs to discriminate between
            % acquistition and regruidding.
            % -> NPHASES, NREADOUTS and NSLICES are for regridding
            % -> NPOINTS, NCIRCUNFERENCES, NLINESPERCIRCUNFERENCES are for
            %    aqcuisition.
            CreateOneParameter(obj,'NREADOUTS','Nx','',60)
            CreateOneParameter(obj,'NPHASES','Ny','',60)
            CreateOneParameter(obj,'NSLICES','Nz','',60)
            CreateOneParameter(obj,'FOVX','FOV x','mm',60*MyUnits.mm)
            CreateOneParameter(obj,'FOVY','FOV y','mm',60*MyUnits.mm)
            CreateOneParameter(obj,'FOVZ','FOV z','mm',60*MyUnits.mm)
            CreateOneParameter(obj,'TMAX','T max','us',1000*MyUnits.us)
            CreateOneParameter(obj,'REPETITIONTIME','TR','ms',30*MyUnits.ms)
            CreateOneParameter(obj,'UNDERSAMPLING','Undersampling','',8);
            CreateOneParameter(obj,'JUMPINGFACTOR','Jumping factor','',0);
            CreateOneParameter(obj,'TRANSIENTTIME','Dead time','us',0.1e-6);
            CreateOneParameter(obj,'DELTAX','X-axis deviation','mm',0);
            CreateOneParameter(obj,'DELTAY','Y-axis deviation','mm',0);
            CreateOneParameter(obj,'DELTAZ','Z-axis deviation','mm',0);
            CreateOneParameter(obj,'NRADIAL','N radial blocks','',1);
            CreateOneParameter(obj,'SPINLOCKGRADIENT','Spinlock gradient','T/m',0);
            CreateOneParameter(obj,'RFAMPLITUDE2','Spinlock B1','',0);
            CreateOneParameter(obj,'SPINLOCKTIME','Spinlock time','us',500*MyUnits.us);
            CreateOneParameter(obj,'REPETITIONDELAY','Repetition Delay','ms',0.1);
            CreateOneParameter(obj,'SLICEPOSITION','Slice Position','mm',0*MyUnits.mm);
            CreateOneParameter(obj,'PHASE1', 'Phase 1', '',65.0);
            CreateOneParameter(obj,'PHASE2', 'Phase 2', '',155.0);
            CreateOneParameter(obj,'PHASE3', 'Phase 3', '',245.0);
            
            obj.InputParHidden = {'NRADIAL','JUMPINGFACTOR','COILRISETIME','SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
                'NPOINTS','SPECTRALWIDTH','SPOILERAMP',...
                'SPOILERTIME','BLANKINGDELAY','REPETITIONDELAY',...
                'TRANSIENTTIME','BLANKINGDELAY'};
            
            % Reorganizes InputParameters
            obj.MoveParameter(6,20);
            obj.MoveParameter(24,12);
            obj.MoveParameter(25,13);
            obj.MoveParameter(19,15);
            obj.MoveParameter(16,17);
            obj.MoveParameter(28,26);
            obj.MoveParameter(29,27);
            obj.MoveParameter(30,28);
            
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = PETRA_SL(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'PETRA SL';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
        end
        function UpdateTETR(obj)
            
            global rawData;
            rawData = [];
            
            obj.TE = 0;
            obj.TR = obj.GetParameter('REPETITIONTIME');
            
            % Get cartesian parameters
            obj.GetCartesianParameters;
            
            % Get sequence parameters
            obj.GetSequenceParameters;
            
            % Set number of phases, points and slices as well as amplitudes
            obj.SetSamplingParameters;
            
            % Calculate the gradients list for radial sampling
            obj.GetRadialGradients;
            
            % Calculate k-points matrix for radial sampling
            obj.GetRadialKPoints
            
            % k-points for Cartesian sampling
            obj.GetCartesianKPoints;
            
            nRepetitions = sum(rawData.aux.numberOfRadialRepetitions)+...
                rawData.aux.numberOfCartesianRepetitions;
            
            obj.TTotal = obj.TR * obj.GetParameter('NSCANS')*nRepetitions;
        end
        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence
            %         "1", prescan for ring down calibration
            
            global scanner
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR] = SelectAxes(obj);
            
            % Set the number of lines to be read
            nMisses = obj.GetParameter('JUMPINGFACTOR');
            
            % Set number of line reads to 0
            obj.nline_reads = 0;
            nPoints = obj.GetParameter('NPOINTS');
            
            % Initialize all vectors to zero with dinamic size
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs,obj.RFamps] = deal([]);
            dac = [obj.selectReadout,obj.selectPhase,obj.selectSlice];
            acqTime = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6;
            crt = obj.GetParameter('COILRISETIME')*1e6;
            crtShort = 15; %ATENTION HERE!!
            blk = obj.GetParameter('BLANKINGDELAY')*1e6;
            gradDelay = scanner.grad_delay; %time for gradients to rise up + blk
            iIns = 1;
            pulseTime = obj.GetParameter('PULSETIME')*1e6;
            TR = obj.GetParameter('REPETITIONTIME')*1e6;
            slTime = obj.GetParameter('SPINLOCKTIME')*1e6;
            slGrad = obj.GetParameter('SPINLOCKGRADIENT');
            repDelayTime = TR-crtShort-2*gradDelay-2*crt-pulseTime-slTime-acqTime;
            pointsPerTR = 0;
            
            ready = 0;
            ii = 0;
            while(ready==0)
                ii = ii+1;
                if(ii<=nMisses)
                    ind = ceil(rand*length(obj.Axis1Vector));
                    readoutAmplitude = obj.Axis1Vector(ind);
                    phaseAmplitude = obj.Axis2Vector(ind);
                    sliceAmplitude = obj.Axis3Vector(ind);
                    clear ind
                else
                    obj.repetition = obj.repetition+1;
                    readoutAmplitude = obj.Axis1Vector(obj.repetition);
                    phaseAmplitude = obj.Axis2Vector(obj.repetition);
                    sliceAmplitude = obj.Axis3Vector(obj.repetition);
                end
                
                %% Get gradients
                g0  = [shimR,shimP,shimS];
%                 gr  = g0+([readoutAmplitude,phaseAmplitude,sliceAmplitude]/diag(scanner.GradientConstants)); %Here we have an error I guess
                gr  = g0+[readoutAmplitude,phaseAmplitude,sliceAmplitude];
%                 gsl = g0+([0 0 slGrad]/diag(scanner.GradientConstants));
                gsl = g0+([0 0 slGrad]/(scanner.GradientConstants(1)));
                
                % Turn on spinlock gradient
                iIns = obj.RampGradient(iIns,g0,gsl,dac,crt,obj.nSteps);
                obj.DELAYs(iIns-1) = crt/obj.nSteps-0.2+gradDelay-blk;
                
                %% RF pulse
                iIns = SetBlanking(obj,iIns,1,blk);
                iIns = obj.CreateRawPulse(iIns,0,pulseTime,0);
                iIns = obj.CreateRawPulse(iIns,1,(slTime-gradDelay),1); %SPINLOCK
%                 iIns = obj.CreateRawPulse(iIns,3,(slTime-gradDelay)/2,1); 
                obj.FREQs(iIns-2:iIns-1) = 1;
                
                %% Turn off spinlock gradient
                iIns = obj.RampGradient(iIns,gsl,g0,dac,crtShort,obj.nSteps);
                iIns = obj.RampGradient(iIns,g0,-gsl/2,dac,crtShort,obj.nSteps);
                iIns = obj.RampGradient(iIns,-gsl/2,g0,dac,crtShort,obj.nSteps);
                obj.TXs(iIns-9*obj.nSteps:iIns-1) = 1;
                obj.PHASEs(iIns-9*obj.nSteps:iIns-1) = 1;
                obj.ENVELOPEs(iIns-9*obj.nSteps:iIns-1) = 7;
                obj.RFamps(iIns-9*obj.nSteps:iIns-1) = 1;
                obj.FLAGs(iIns-9*obj.nSteps:iIns-1) = 1;
                obj.DELAYs(iIns-1) = crtShort/obj.nSteps-0.2+gradDelay-3*crtShort;
                
                %% Turn on readout gradient
                iIns = obj.RampGradient(iIns,g0,gr,dac,crt,obj.nSteps);
                obj.DELAYs(iIns-1) = crt/obj.nSteps-0.2+gradDelay;
                
                %% Acquisition
                if(ii<=nMisses)
                    iIns = obj.RepetitionDelay(iIns,0,acqTime+repDelayTime-0.1);
                else
                    iIns = obj.Acquisition(iIns,acqTime+repDelayTime-0.1);
                    obj.nline_reads = obj.nline_reads+1;
                end
                
                %% Turn off the readout gradient
                iIns = obj.RampGradient(iIns,gr,g0,dac,crt,obj.nSteps);
                
                %% Repetition Delay
                if(obj.repetition==length(obj.Axis1Vector))
                    iIns = obj.RepetitionDelay(iIns,1,0.1);
                    obj.lastBatch = 1;
                    disp('Last Batch!')
                else
                    iIns = obj.RepetitionDelay(iIns,0,0.1);
                    obj.lastBatch = 0;
                end
                
                %% If it is the first repetition, set up instructions and points to be read per TR
                if(ii==1)
                    instruPerTR = iIns-1;
                end
                if(obj.repetition==1)
                    pointsPerTR = obj.nline_reads*nPoints;
                end
                
                %% Determine if a new repetition is possible in the current batch
                if(iIns-1+instruPerTR>2048 || obj.nline_reads*nPoints+pointsPerTR>16000 || obj.lastBatch==1)
                    ready = 1;
                end
            end
            obj.nInstructions   = iIns-1;
            obj.PhasesPerBatch = obj.nline_reads+nMisses;
        end
        function [status,result] = Run(obj,iAcq)
            tic;
            clc
            
            [~,~,~,~,~,spoilerEnable,~] = obj.ConfigureGradients(0);
            
            %% Constant to relate k with k'
            % HELP: k = gammabar*G*t
            % G = c*A where A is the gradient amplitude voltage from
            % radioprocessor. Then, k' = k/(gammabar*c) = A*t.
            % We still need to calibrate constant c. We also need to take
            % into account that c is different for each axis.
            gammabar = 42.6d6;          % MHz/T
            c = obj.ReorganizeCalibrationData();
            global rawData              % Raw Data contains output info
            rawData = [];
            rawData.inputs.SeqName = obj.SequenceName;
            
            % Get cartesian parameters
            obj.GetCartesianParameters;
            
            % Get sequence parameters
            obj.GetSequenceParameters;
            
            % Set number of phases, points and slices as well as amplitudes
            obj.SetSamplingParameters;
            
            % Calculate the gradients list for radial sampling
            obj.GetRadialGradients;
            gradientVectors1 = rawData.aux.gradientsRadialTeslasPerMeter;
            
            % Calculate k-points matrix for radial sampling
            obj.GetRadialKPoints
            kSpaceValues = rawData.aux.radialKPoints;
            
            % k-points for Cartesian sampling
            obj.GetCartesianKPoints;
            kSinglePoint = rawData.aux.cartesianKPoints;
            kSpaceValues = cat(1,kSpaceValues,kSinglePoint);
            
            % Gradients for Cartesian sampling
            tMin = rawData.aux.tMin;
            gradientVectors2 = kSinglePoint/diag(gammabar*tMin);
            rawData.aux.gradientsCartesianTeslasPerMeter = gradientVectors2;
            
            % Normalize gradients to arbitray units.
            gradientVectors1 = gradientVectors1/diag(c);
            gradientVectors2 = gradientVectors2/diag(c);
            obj.grSpinlock = obj.grSpinlock/c(3);
            if(sum(abs(gradientVectors1(:))>=1)>=1 || sum(isnan(gradientVectors1(:)))>=1 || sum(isinf(gradientVectors1(:)))>=1)
                warndlg('At least one non valid DAC amplitude in radial gradients!','Delay Error')
                return;
            end
            if(sum(abs(gradientVectors2(:))>=1)>=1 || sum(isnan(gradientVectors2(:)))>=1 || sum(isinf(gradientVectors2(:)))>=1)
                warndlg('At least one non valid DAC amplitude in single point gradients!','Delay Error')
                return;
            end
            
            %% Auxiliar
            Data2D = [];
            nPPL = rawData.aux.numberOfPointsPerLine;
            nPoints = rawData.inputs.nPoints;
            nCir = rawData.aux.numberOfCircumferences;
            nLPC = rawData.aux.linesPerCircumference;
            acqTime = rawData.aux.acquisitionTime;
            nRadRepetitions = rawData.aux.numberOfRadialRepetitions;
            nCarRepetitions = rawData.aux.numberOfCartesianRepetitions;
            bw = rawData.aux.bandwidth;
            
            % Set parameters
            obj.AcquisitionTime = acqTime;
            obj.SetParameter('SPECTRALWIDTH',bw);
            obj.SetParameter('NPOINTS',nPPL);
            
            % Get value to normalize fid to Volts
            bwCalNormVal = obj.calculate_bwCalNormVal;
            
            %% Radial
            obj.line = 0;
            tBatchIni = clock;
            
            % Define current stage
            obj.Stage = 1;
                                   
            % Start batches sweep
            tRadial = 0;
            obj.Axis1Vector = gradientVectors1(:,1);
            obj.Axis2Vector = gradientVectors1(:,2);
            obj.Axis3Vector = gradientVectors1(:,3);
            nFreq = 0;
            iBatch = 0;
            obj.repetition = 0;
            obj.lastBatch = 0;
            tic;
            while(obj.lastBatch==0)
                tA = clock;
                iBatch = iBatch+1;
                if(iBatch==1)
                    fprintf('Radial batch %1.0f, estimating remaining time \n',iBatch);
                else
                    tC = tC*(nPhaseBatches-iBatch+1);
                    fprintf('Radial batch %1.0f/%1.0f, remaining time: %1.0f s \n',iBatch,nPhaseBatches,tC);
                end
                if(spoilerEnable && (toc>200 || iBatch==1))
                    fprintf('Calibrating frequency... \n')
                    % Get central frequency
                    f0 = obj.GetFIDParameters;
                    obj.SetParameter('FREQUENCY',f0);
                    nFreq = nFreq+1;
                    rawData.aux.freqListRadial(nFreq) = f0;
                    fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
                    tic;
                end
                                
                % NOW RUN ACTUAL ACQUISTION
                obj.CreateSequence(0);
                if(iBatch==1)
                    nPhaseBatches = ceil(nRadRepetitions/obj.PhasesPerBatch);
                end
                if(sum(obj.DELAYs<0.1)~=0)
                    warndlg('At least one delay is negative!','Delay Error')
                    return;
                end
                obj.Sequence2String;
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                t1 = clock;
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                t2 = clock;
                tRadial = tRadial + etime(t2,t1);
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Radial batch aborted!','Warning')
                    return;
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2))/bwCalNormVal;
                Data2D = cat(1,Data2D,cDataBatch);
                
                tB = clock;
                tC = etime(tB,tA);
            end
            
            %% Multiply the signal by its corresponding phase
%             Data2D = reshape(Data2D,[nPPL,nLPC*nCir]);
%             obj.line = 0;
%             for ii = 1:nLPC*nCir
%                 Data2D(:,ii) = Data2D(:,ii)*exp(1i*pi*(obj.line==1));
%                 obj.line = ~obj.line;
%             end
%             Data2D = reshape(Data2D,nPPL*nLPC*nCir,1);
            linep = obj.line;
            
            %% Single point
            % Define current stage
            obj.Stage = 2;
            
            % Set parameters
            obj.AcquisitionTime = acqTime;
            obj.SetParameter('NPOINTS',1);
            
            % Start batches sweep
            tCartesian = 0;
            obj.Axis1Vector = gradientVectors2(:,1);
            obj.Axis2Vector = gradientVectors2(:,2);
            obj.Axis3Vector = gradientVectors2(:,3);
            nFreq = 0;
            iBatch = 0;
            obj.repetition = 0;
            obj.lastBatch = 0;
            tic;
            while(obj.lastBatch==0)
                tA = clock;
                iBatch = iBatch+1;
                if(iBatch==1)
                    fprintf('Cartesian batch 1, estimating remaining time \n');
                else
                    tC = tC*(nPhaseBatches-iBatch+1);
                    fprintf('Cartesian batch %1.0f/%1.0f, remaining time: %1.0f s \n',iBatch,nPhaseBatches,tC);
                end
                if(spoilerEnable && toc>300)
                    fprintf('Calibrating frequency... \n')
                    % Get central frequency
                    f0 = obj.GetFIDParameters;
                    obj.SetParameter('FREQUENCY',f0);
                    nFreq = nFreq+1;
                    rawData.aux.freqListRadial(nFreq) = f0;
                    fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
                    tic;
                end
                                
                % NOW RUN ACTUAL ACQUISTION
                obj.CreateSequence(0);
                if(iBatch==1)
                    nPhaseBatches = ceil(nCarRepetitions/obj.PhasesPerBatch);
                end
                if(sum(obj.DELAYs<0.1)~=0)
                    warndlg('At least one delay is negative!','Delay Error')
                    return;
                end
                obj.Sequence2String;
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                t1 = clock;
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                t2 = clock;
                tCartesian = tCartesian + etime(t2,t1);
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Radial batch aborted!','Warning')
                    return;
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2))/bwCalNormVal;
                Data2D = cat(1,Data2D,cDataBatch);
                
                tB = clock;
                tC = etime(tB,tA);
            end
            
            %% Multiply data by its corresponding phase
            obj.line = linep;
%             for ii = length(Data2D)-nCarRepetitions+1:length(Data2D)
%                 Data2D(ii) = Data2D(ii)*exp(1i*pi*(obj.line==1));
%                 obj.line = ~obj.line;
%             end
            
            obj.SetParameter('NPOINTS',nPPL(1));
            disp(' ');
            kSpaceValues = [kSpaceValues,Data2D(:)];
            %             kSpaceValuesCal = kSpaceValues;
            %             kSpaceValuesCal(:,4) = Data2DCal(:);
            
            tBatchFin = clock;
            tBatch = etime(tBatchFin,tBatchIni);
            rawData.kSpace.sampled = kSpaceValues;
            %             rawData.kSpace.sampledCal = kSpaceValuesCal;
            
            %% Regridding
            %             kSpaceValues = kSpaceValuesCal;
            kCartesian = rawData.aux.kCartesian;
            if(nCir>1)
                valCartesian = griddata(kSpaceValues(:,1),kSpaceValues(:,2),kSpaceValues(:,3),kSpaceValues(:,4),kCartesian(:,1),kCartesian(:,2),kCartesian(:,3));
            elseif(nCir==1 && nLPC>2)
                valCartesian = griddata(kSpaceValues(:,1),kSpaceValues(:,2),kSpaceValues(:,4),kCartesian(:,1),kCartesian(:,2));
            elseif(nCir==1 && nLPC==2)
                valCartesian = pchip(kSpaceValues(:,1),kSpaceValues(:,4),kCartesian(:,1));
            end
            valCartesian(isnan(valCartesian)) = 0;
            
            % Add phase according to fovDeviation
            DELX = rawData.inputs.fovDeviation(1);
            DELY = rawData.inputs.fovDeviation(2);
            DELZ = rawData.inputs.fovDeviation(3);
            phase = exp(-2*pi*1i*(DELX*kCartesian(:,1)+DELY*kCartesian(:,2)+DELZ*kCartesian(:,3)));
            
            % Save interpolated data
            interpolated = [kCartesian,valCartesian(:).*phase];
            rawData.kSpace.interpolated = interpolated;
            interpolated = reshape(interpolated(:,4),nPoints);
            
            imagen = abs(ifftshift(ifftn(interpolated)));
            rawData.kSpace.imagen = imagen;
            
            rawData.kSpace.complexFFTimagen = (ifftshift(ifftn(interpolated)));
            
             %% Uncomment this block to amend "chess board" problem in real component of FFT image
                rF=real(rawData.kSpace.complexFFTimagen);
                iF=imag(rawData.kSpace.complexFFTimagen);
                rF2=rF;
                iF2=rF;
                NX=nPoints(1);
                NY=nPoints(2);
                NZ=nPoints(3);
                for i=1:NX
                    for j=1:NY
                        for k=1:NZ
                            rF2(i,j,k)=(-1)^(i+j+k+1)*rF(i,j,k);
                            iF2(i,j,k)=(-1)^(i+j+k)*iF(i,j,k);
                        end
                    end
                end
                rawData.kSpace.complexFFTimagen = rF2 + 1i*iF2;
            
            
            %% Save data
            elapsedTime = toc;
            rawData.aux.tRadial = tRadial;
            rawData.aux.tCartesian = tCartesian;
            rawData.aux.tBatch = tBatch;
            rawData.aux.elapsedTime = elapsedTime;
            
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');            
        end
        
        
        %**************************************************************************
        %**************************************************************************
        %**************************************************************************
        
        
        function GetCartesianParameters(obj)
            global rawData;
            
            nPoints = ...
                [obj.GetParameter('NREADOUTS'),...
                obj.GetParameter('NPHASES'),...
                obj.GetParameter('NSLICES')];
            fov     = ...
                [obj.GetParameter('FOVX'),...
                obj.GetParameter('FOVY'),...
                obj.GetParameter('FOVZ')];
            deltaK  = 1./fov;
            kMax    = nPoints./(2*fov);
            DELX    = obj.GetParameter('DELTAX');
            DELY    = obj.GetParameter('DELTAY');
            DELZ    = obj.GetParameter('DELTAZ');
            
            rawData.inputs.NEX          = obj.GetParameter('NSCANS');
            rawData.inputs.nPoints      = nPoints;
            rawData.inputs.fov          = fov;
            rawData.inputs.fovDeviation = [DELX,DELY,DELZ];
            rawData.aux.deltaK          = deltaK;
            rawData.aux.kMax            = kMax;
        end
        function GetSequenceParameters(obj)
            
            global rawData;
            coilRiseTime = obj.GetParameter('COILRISETIME');
            trf = obj.GetParameter('PULSETIME');                            % RF pulse time
            tMax = obj.GetParameter('TMAX');                                % Max time for acquisition, it should be the shortest T2 to be acquired
            underSampling = obj.GetParameter('UNDERSAMPLING');              % Undersampling for azimutal acquisition.
            underSampling = sqrt(underSampling);
            TR = obj.GetParameter('REPETITIONTIME');
            nMisses = obj.GetParameter('JUMPINGFACTOR');
            slGrad = obj.GetParameter('SPINLOCKGRADIENT');
            slPosi = obj.GetParameter('SLICEPOSITION');
            obj.SliceFreqOffset = 42.58d6*slGrad*slPosi;
            
            rawData.inputs.pulseTime = trf;
            rawData.inputs.tMax = tMax;
            rawData.inputs.coilRiseTime = coilRiseTime;
            rawData.inputs.undersampling = underSampling;
            rawData.inputs.jumpingFactor = nMisses;
            rawData.inputs.rfAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.rfFrequency = obj.GetParameter('FREQUENCY');
            rawData.inputs.repetitionTime = TR;
            rawData.inputs.axes = [obj.GetParameter('READOUT'),...
                obj.GetParameter('PHASE'),obj.GetParameter('SLICE')];
            rawData.inputs.gradientSL = obj.GetParameter('SPINLOCKGRADIENT');
            rawData.inputs.B1SL = obj.GetParameter('RFAMPLITUDE2');
            rawData.inputs.tSL = obj.GetParameter('SPINLOCKTIME');
            rawData.inputs.posSL = slPosi;
            rawData.aux.freqOffsetSL = obj.SliceFreqOffset;
            
        end
        function SetSamplingParameters(obj)
            % gradientAmplitude = normalized amplitude to the MRIGUI input
            % nLPC = number of lines per circunferece in k-space
            % nCir = number of circunfereces in gradient space
            % nPPL = number of acquired points per line in k-space
            
            global rawData;
            global scanner;
            gammabar        = 42.6d6;
            tMax            = rawData.inputs.tMax;
            kMax            = rawData.aux.kMax;
            nPoints         = rawData.inputs.nPoints;
            trf             = rawData.inputs.pulseTime;
            underSampling   = rawData.inputs.undersampling;
            gradDelay       = scanner.grad_delay*1e-6;
            crt             = rawData.inputs.coilRiseTime;
            TR              = rawData.inputs.repetitionTime;
            slTime          = rawData.inputs.tSL;
            
            % Set first block parameters
            bw                       = max(nPoints)/(2*tMax);                                     % ideal acquisition bandwidth
            kLimits                = kMax;                                             % k limits for each radial block
            gradientAmplitudes     = kLimits./(gammabar*tMax).*~(nPoints==1);     % gradient amplitudes for each radial block (T/m)
            nPPL                     = ceil((sqrt(3)*tMax-crt-gradDelay)*bw)+1;          % number of points per line
            nLPC                     = ceil(max(nPoints(1:2))*pi/underSampling);         % max number of lines per circumference
            nLPC                     = max(nLPC-mod(nLPC,2),1);
            nCir                     = max(ceil(nPoints(3)*pi/2/underSampling)+1,1);     % number of circumferences
            if(nPoints(3)==1)
                nCir = 1;
            end
            if(nPoints(2)==1)
                nLPC = 2;
            end
            
            % Get acquisition time
            acqTime = nPPL./bw;
            
            % Get repDelay
            repDelayTime = TR-10e-6-2*gradDelay-2*crt-trf-slTime-acqTime;
            obj.SetParameter('REPETITIONDELAY',repDelayTime);
            
            % Save data in rawData
            rawData.aux.bandwidth               = bw;
            rawData.aux.kLimits                 = kLimits;
            rawData.aux.acquisitionTime         = acqTime;
            rawData.aux.numberOfPointsPerLine   = nPPL;
            rawData.aux.linesPerCircumference   = nLPC;
            rawData.aux.numberOfCircumferences  = nCir;
            rawData.aux.gradientAmplitudes      = gradientAmplitudes;
            rawData.aux.repDelayTime            = repDelayTime;
        end
        function GetRadialGradients(obj)
            % Gradient are defined by the sphere parametric equation. The
            % two parameteres are: theta in [0,pi] and phi [0,2pi].
            % Gradients are normalized to the amplifier amplitude.
            % First, I calculate the number of repetitions:
            
            % Load rawData
            global rawData;
            nCir                = rawData.aux.numberOfCircumferences;
            nLPC                = rawData.aux.linesPerCircumference;
            gradientAmplitudes  = rawData.aux.gradientAmplitudes;
            
            % Get number of radial repetitions
            nRepetitions = 0;
            % Get theta vector for current block
            if(nCir==1)
                theta = pi/2;
            else
                theta = linspace(0,pi,nCir);
            end
            % Get number of radial repetitions for current block;
            for jj = 1:nCir
                nRepetitions = nRepetitions+max(ceil(nLPC*sin(theta(jj))),1);
            end
%             gradShuffle = randperm(nRepetitions);
            
            % Calculate radial gradients
            normalizedGradientsRadial = zeros(sum(nRepetitions),3);
            n = 0;
            % Get theta vector for current block
            if(nCir==1)
                theta = pi/2;
            else
                theta = linspace(0,pi,nCir);
            end
            
            % Calculate the normalized gradients:
            for jj = 1:nCir
                nLPCjj = max(ceil(nLPC*sin(theta(jj))),1);
                deltaPhi = 2*pi/nLPCjj;
                phi = linspace(0,2*pi-deltaPhi,nLPCjj);
                for kk = 1:nLPCjj
                    n = n+1;
                    normalizedGradientsRadial(n,1) = sin(theta(jj))*cos(phi(kk));
                    normalizedGradientsRadial(n,2) = sin(theta(jj))*sin(phi(kk));
                    normalizedGradientsRadial(n,3) = cos(theta(jj));
%                     normalizedGradientsRadial(gradShuffle(n),1) = sin(theta(jj))*cos(phi(kk));
%                     normalizedGradientsRadial(gradShuffle(n),2) = sin(theta(jj))*sin(phi(kk));
%                     normalizedGradientsRadial(gradShuffle(n),3) = cos(theta(jj));
                end
            end
            
            % Set gradients to T/m
            gradientVectors1 = ...
                normalizedGradientsRadial*diag(gradientAmplitudes);
            
            % Save to rawData
            rawData.aux.numberOfRadialRepetitions = nRepetitions;
            rawData.aux.normalizedGradientsRadial = normalizedGradientsRadial;
            rawData.aux.gradientsRadialTeslasPerMeter = gradientVectors1;
        end
        function GetRadialKPoints(obj)
            
            %Load rawData
            global rawData;
            gammabar = 42.6d6;
            nPPL                        = rawData.aux.numberOfPointsPerLine;
            crt                         = rawData.inputs.coilRiseTime;
            bw                          = rawData.aux.bandwidth;
            normalizedGradientsRadial   = rawData.aux.normalizedGradientsRadial;
            nRepetitions                = rawData.aux.numberOfRadialRepetitions;
            gradientAmplitudes          = rawData.aux.gradientAmplitudes;
            
            % Calculate the radial k points for each block
            kRadial = [];
            % Calculate k-points after gradient ramp up
            normalizedKRadial = zeros(nRepetitions,3,nPPL);
            normalizedKRadial(:,:,1) = (0.5*crt+0.5/bw)*normalizedGradientsRadial;
            % Calculate all k-points
            for jj=2:nPPL
                normalizedKRadial(:,:,jj) = normalizedKRadial(:,:,1)+(jj-1)*normalizedGradientsRadial/bw;
            end
            normalizedKRadial = reshape(permute(normalizedKRadial,[3,1,2]),[nRepetitions*nPPL,3]);
            kRadial = cat(1,kRadial,normalizedKRadial*diag(gammabar*gradientAmplitudes));
            
            % Save to rawData
            rawData.aux.radialKPoints = kRadial;
        end
        
        
        %******************************************************************
        %******************************************************************
        %******************************************************************
        
        
        function GetCartesianKPoints(obj)
            
            % Load rawData
            global rawData;
            gammabar = 42.6d6;
            crt = rawData.inputs.coilRiseTime;
            bw = rawData.aux.bandwidth;
            nPoints = rawData.inputs.nPoints;
            kMax = rawData.aux.kMax;
            gradientAmplitudes = rawData.aux.gradientAmplitudes;
            % Get minimun time
            tMin = 0.5*crt+0.5/bw;
            
            % Get the full cartesian points
            kx = linspace(-kMax(1)*~isequal(nPoints(1),1),kMax(1)*~isequal(nPoints(1),1),nPoints(1));
            ky = linspace(-kMax(2)*~isequal(nPoints(2),1),kMax(2)*~isequal(nPoints(2),1),nPoints(2));
            kz = linspace(-kMax(3)*~isequal(nPoints(3),1),kMax(3)*~isequal(nPoints(3),1),nPoints(3));
            [kx,ky,kz] = meshgrid(kx,ky,kz);
            kx = permute(kx,[2,1,3]);
            ky = permute(ky,[2,1,3]);
            kz = permute(kz,[2,1,3]);
            kCartesian(:,1) = kx(:);
            kCartesian(:,2) = ky(:);
            kCartesian(:,3) = kz(:);
            
            % Get the points that should be acquired in a time shorter than
            % tMin
            normalizedKCartesian = kCartesian/diag(gammabar*(gradientAmplitudes+double(gradientAmplitudes==0)));
            normalizedKCartesian(:,4) = sqrt(sum(normalizedKCartesian.^2,2));
            normalizedKSinglePoint = normalizedKCartesian(normalizedKCartesian(:,4)<tMin,1:3);
            kSinglePoint = normalizedKSinglePoint*diag(gammabar*gradientAmplitudes);
%             kShuffle = randperm(size(kSinglePoint,1));
%             kSinglePoint = kSinglePoint(kShuffle,:);
            
            % Save to rawData
            rawData.aux.numberOfCartesianRepetitions = size(kSinglePoint,1);
            rawData.aux.tMin = tMin;
            rawData.aux.cartesianKPoints = kSinglePoint;
            rawData.aux.kCartesian = kCartesian;
        end
        
    end
    
end

