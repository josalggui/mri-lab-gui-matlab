classdef D_PETRA < MRI_BlankSequence
    %%
    
    % PETRA CODE
    
    %*******************************************************
    %** J. M. Algar�n                                     **
    %** CSIC/UPV                                          **
    %** I3M                                               **
    %** Avda. dels Tarongers, 12,46022, Valencia, (Spain) **
    %** Tel: +34 960 728 111                              **
    %** email: josalggui@i3m.upv.es                       **
    %*******************************************************
    
    %%
    
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    %  Elena Diaz Caballero
    properties
        PhasesPerBatch = 20;
        nPhasesBatch;
        Axis1Vector;
        Axis2Vector;
        Axis3Vector;
        StartSlice;
        lastBatch;
        EndSlice;
        StartPhase;
        EndPhase;
        Stage;              % ZTE -> 1, Single Point ->2 or Prescan -> 0
        AcquisitionTime;
        slice_ampl;
        readoutAmplitudeP = 0;
        phaseAmplitudeP = 0;
        sliceAmplitudeP = 0;
        repetitionDelay;
        gradDelay = 55e-6;
        
    end
    methods (Access=private)
        function CreateParameters(obj)
            
            % HELP %
            % I include here new inputs to discriminate between
            % acquistition and regruidding.
            % -> NPHASES, NREADOUTS and NSLICES are for regridding
            % -> NPOINTS, NCIRCUNFERENCES, NLINESPERCIRCUNFERENCES are for
            %    aqcuisition.
            CreateOneParameter(obj,'NREADOUTS','Nx','',50)
            CreateOneParameter(obj,'NPHASES','Ny','',50)
            CreateOneParameter(obj,'NSLICES','Nz','',50)
            CreateOneParameter(obj,'FOVX','FOV x','mm',20*MyUnits.mm)
            CreateOneParameter(obj,'FOVY','FOV y','mm',20*MyUnits.mm)
            CreateOneParameter(obj,'FOVZ','FOV z','mm',20*MyUnits.mm)
            CreateOneParameter(obj,'TMAX','T max','us',70*MyUnits.us)
            CreateOneParameter(obj,'REPETITIONTIME','TR','ms',50*MyUnits.ms)
            CreateOneParameter(obj,'UNDERSAMPLING','Undersampling','',1);
            CreateOneParameter(obj,'JUMPINGFACTOR','Jumping factor','',0);
            CreateOneParameter(obj,'TRANSIENTTIME','Dead time','us',0.1*MyUnits.us);
            CreateOneParameter(obj,'REPETITIONDELAY','Repetition Delay','ms',0.1*MyUnits.ms);
            CreateOneParameter(obj,'DELTAX','X-axis deviation','mm',0);
            CreateOneParameter(obj,'DELTAY','Y-axis deviation','mm',0);
            CreateOneParameter(obj,'DELTAZ','Z-axis deviation','mm',0);
            CreateOneParameter(obj,'ECHOTIME','TE','ms',1*MyUnits.ms);
            CreateOneParameter(obj,'ECHODELAY','Echo delay','us',0*MyUnits.us);
            
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
                'NPOINTS','SPECTRALWIDTH','SPOILERAMP','REPETITIONDELAY',...
                'SPOILERTIME','BLANKINGDELAY','COILRISETIME'};
            
            % Reorganizes InputParameters
            obj.MoveParameter(6,20);
            obj.MoveParameter(24,12);
            obj.MoveParameter(25,13);
            obj.MoveParameter(19,15);
            obj.MoveParameter(16,17);
            obj.MoveParameter(29,26);
            obj.MoveParameter(30,27);
            obj.MoveParameter(31,28);
            obj.MoveParameter(32,13);
            obj.MoveParameter(33,14);
            
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = D_PETRA(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'D-PETRA';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
        end
        function UpdateTETR(obj)
            
            global rawData;
            rawData = [];
            
            obj.TE = 0;
            obj.TR = obj.GetParameter('REPETITIONTIME');
            
            % Get cartesian parameters
            obj.GetCartesianParameters;
            
            % Get sequence parameters
            obj.GetSequenceParameters;
            
            % Set number of phases, points and slices as well as amplitudes
            obj.SetSamplingParameters;
            
            % Calculate the gradients list for radial sampling
            obj.GetRadialGradients;
            
            % Calculate k-points matrix for radial sampling
            obj.GetRadialKPoints
            
            % k-points for Cartesian sampling
            obj.GetCartesianKPoints;
            
            nRepetitions = sum(rawData.aux.numberOfRadialRepetitions)+...
                rawData.aux.numberOfCartesianRepetitions;
            
            obj.TTotal = obj.TR * obj.GetParameter('NSCANS')*nRepetitions;
        end
        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence
            %         "1", prescan for ring down calibration
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR] = SelectAxes(obj);
            
            % Set the number of lines to be read
            nMisses = obj.GetParameter('JUMPINGFACTOR');
            
            if(mode==1)
                [~,nPhases,~,~,~,~,~] = obj.ConfigureGradients(mode);
                obj.Axis1Vector = 0;
                obj.Axis2Vector = 0;
                obj.Axis3Vector = 0;
            elseif(mode==0)
                [~,~,~,~,~,~,~] = obj.ConfigureGradients(mode);
                nPhases = obj.nPhasesBatch+nMisses;
            end
            
            % Calculate the number of line reads
            if obj.Stage==1
                obj.nline_reads = 2*(nPhases-nMisses);
            else
                obj.nline_reads = (nPhases-nMisses);
            end
                
            % Initialize all vectors to zero with the appropriate size
            if obj.Stage==1
                obj.nInstructions = (9*obj.nSteps+6)*nPhases;
            else
                obj.nInstructions = (3*obj.nSteps+5)*nPhases;
            end
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            acqTime = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6;
            crt = obj.GetParameter('COILRISETIME')*1e6;
            blk = obj.GetParameter('BLANKINGDELAY')*1e6;
            gradientDelay = obj.gradDelay*1e6;
            pulseTime = obj.GetParameter('PULSETIME')*1e6;
            deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
            repDelayTime = obj.GetParameter('REPETITIONDELAY')*1e6;
            TE = obj.GetParameter('ECHOTIME')*1e6;
            ED = obj.GetParameter('ECHODELAY')*1e6;
            tx = 0.5*pulseTime+deadTime+gradientDelay;
            tw = TE+ED-0.5*pulseTime-deadTime-2*crt-2*acqTime-gradientDelay-tx;
            iIns = 1;
            for phasei = 1:nPhases
                if(phasei<=nMisses)
                    readoutAmplitude = 0;
                    phaseAmplitude = 0;
                    sliceAmplitude = 0;
                else
                    readoutAmplitude = obj.Axis1Vector(phasei-nMisses);
                    phaseAmplitude = obj.Axis2Vector(phasei-nMisses);
                    sliceAmplitude = obj.Axis3Vector(phasei-nMisses);
                end
                
                %% Activate the total gradient (3*nSteps instrucctions)
                for ii=1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimR+obj.readoutAmplitudeP+(readoutAmplitude-obj.readoutAmplitudeP)/obj.nSteps*ii,RR,0.1);
                    iIns = obj.PulseGradient(iIns,shimP+obj.phaseAmplitudeP+(phaseAmplitude-obj.phaseAmplitudeP)/obj.nSteps*ii,PP,0.1);
                    iIns = obj.PulseGradient(iIns,shimS+obj.sliceAmplitudeP+(sliceAmplitude-obj.sliceAmplitudeP)/obj.nSteps*ii,SS,crt/obj.nSteps-0.2);
                end
                obj.readoutAmplitudeP = readoutAmplitude;
                obj.phaseAmplitudeP = phaseAmplitude;
                obj.sliceAmplitudeP = sliceAmplitude;
                obj.DELAYs(iIns-1) = crt/obj.nSteps-0.2+gradientDelay-blk;
                
                %% RF pulse (3 instructions)
                iIns = obj.CreatePulse(iIns,0,blk,pulseTime,deadTime);
                if(obj.Stage==0)
                    obj.TXs(iIns-2) = 0;
                end
                
                %% Acquisition (1 instruction)
                if(phasei<=nMisses)
                    iIns = obj.RepetitionDelay(iIns,0,acqTime);
                else
                    iIns = obj.Acquisition(iIns,acqTime);
                end
                
                %% Only for radial acquisition
                if obj.Stage==1
                    %% Turn off gradient and wait to next gradient (3*nSteps instructions)
                    for ii=1:obj.nSteps
                        iIns = obj.PulseGradient(iIns,shimR+obj.readoutAmplitudeP+(0-obj.readoutAmplitudeP)/obj.nSteps*ii,RR,0.1);
                        iIns = obj.PulseGradient(iIns,shimP+obj.phaseAmplitudeP+(0-obj.phaseAmplitudeP)/obj.nSteps*ii,PP,0.1);
                        iIns = obj.PulseGradient(iIns,shimS+obj.sliceAmplitudeP+(0-obj.sliceAmplitudeP)/obj.nSteps*ii,SS,crt/obj.nSteps-0.2);
                    end
                    obj.readoutAmplitudeP = 0;
                    obj.phaseAmplitudeP = 0;
                    obj.sliceAmplitudeP = 0;
                    obj.DELAYs(iIns-1) = crt/obj.nSteps-0.2+tw;
                    
                    %% Turn on gradient and wait to acquisition (3*nSteps instructions)
                    for ii=1:obj.nSteps
                        iIns = obj.PulseGradient(iIns,shimR+obj.readoutAmplitudeP+(-readoutAmplitude-obj.readoutAmplitudeP)/obj.nSteps*ii,RR,0.1);
                        iIns = obj.PulseGradient(iIns,shimP+obj.phaseAmplitudeP+(-phaseAmplitude-obj.phaseAmplitudeP)/obj.nSteps*ii,PP,0.1);
                        iIns = obj.PulseGradient(iIns,shimS+obj.sliceAmplitudeP+(-sliceAmplitude-obj.sliceAmplitudeP)/obj.nSteps*ii,SS,crt/obj.nSteps-0.2);
                    end
                    obj.readoutAmplitudeP = shimR;
                    obj.phaseAmplitudeP = shimP;
                    obj.sliceAmplitudeP = shimS;
                    obj.DELAYs(iIns-1) = crt/obj.nSteps-0.2+gradientDelay+tx+ED;
                    
                    %% Acquisition (1 instruction)
                    if(phasei<=nMisses)
                        iIns = obj.RepetitionDelay(iIns,0,acqTime);
                    else
                        iIns = obj.Acquisition(iIns,acqTime);
                    end
                end
                
                %% Repetition Delay (1 instruction)
                iIns = obj.RepetitionDelay(iIns,1,repDelayTime);
                if(obj.Stage~=0)
                    obj.DELAYs(iIns-1)   = repDelayTime;
                else
                    obj.DELAYs(iIns-1)   = acqTime;
                end
                
            end
        end
        function [status,result] = Run(obj,iAcq)
            tic;
            clc
%             obj.nSteps = 5;
            [~,~,~,~,~,spoilerEnable,~] = obj.ConfigureGradients(0);
            
            %% Constant to relate k with k'
            % HELP: k = gammabar*G*t
            % G = c*A where A is the gradient amplitude voltage from
            % radioprocessor. Then, k' = k/(gammabar*c) = A*t.
            % We still need to calibrate constant c. We also need to take
            % into account that c is different for each axis.
            gammabar = 42.6d6;          % MHz/T
            c = obj.ReorganizeCalibrationData();
            global rawData              % Raw Data contains output info
            rawData = [];
            rawData.inputs.seqName = obj.SequenceName;
            
            % Get cartesian parameters
            obj.GetCartesianParameters;
            
            % Get sequence parameters
            obj.GetSequenceParameters;
            
            % Set number of phases, points and slices as well as amplitudes
            obj.SetSamplingParameters;
            
            % Calculate the gradients list for radial sampling
            obj.GetRadialGradients;
            gradientVectorsShort = rawData.aux.gradientsRadialTeslasPerMeter;
            
            % Calculate k-points matrix for radial sampling
            obj.GetRadialKPoints
            kSpaceValuesShort = rawData.aux.radialKPointsShort;
            kSpaceValuesLong = rawData.aux.radialKPointsLong;
            
            % k-points for Cartesian sampling
            obj.GetCartesianKPoints;
            kSinglePoint = rawData.aux.cartesianKPoints;
            kSpaceValuesShort = cat(1,kSpaceValuesShort,kSinglePoint);
            
            % Gradients for Cartesian sampling
            tMin = rawData.aux.tMin;
            gradientVectors2 = kSinglePoint/diag(gammabar*tMin);
            rawData.aux.gradientsCartesianTeslasPerMeter = gradientVectors2;
            
            % Normalize gradients to arbitray units.
            gradientVectorsShort = gradientVectorsShort/diag(c);
            gradientVectors2 = gradientVectors2/diag(c);
            if(sum(abs(gradientVectorsShort(:))>=1)>=1 || sum(isnan(gradientVectorsShort(:)))>=1 || sum(isinf(gradientVectorsShort(:)))>=1)
                warndlg('At least one non valid DAC amplitude in radial gradients!','Delay Error')
                return;
            end
            if(sum(abs(gradientVectors2(:))>=1)>=1 || sum(isnan(gradientVectors2(:)))>=1 || sum(isinf(gradientVectors2(:)))>=1)
                warndlg('At least one non valid DAC amplitude in single point gradients!','Delay Error')
                return;
            end
            
            %% Auxiliar
            dataShort = [];
            dataLong = [];
            nPPL = rawData.aux.numberOfPointsPerLine;
            nLPC = rawData.aux.linesPerCircumference;
            nMisses = rawData.inputs.jumpingFactor;
            nPoints = rawData.inputs.nPoints;
            nCir = rawData.aux.numberOfCircumferences;
            acqTime = rawData.aux.acquisitionTime;
            nRepetitions = rawData.aux.numberOfRadialRepetitions;
            bw = rawData.aux.bandwidth;
            
            %% Radial
            % Create waiting bar
            obj.Stage = 1;
            nFreq = 0;
            
            % Set parameters for current block
            obj.AcquisitionTime = acqTime;
            obj.SetParameter('SPECTRALWIDTH',bw);
            obj.SetParameter('NPOINTS',nPPL);
            obj.SetParameter('REPETITIONDELAY',rawData.aux.delayRadial);
           
            % Get value to normalize fid to Volts
            bwCalNormVal = obj.calculate_bwCalNormVal;
            
            % Repetitions per batch and number of repetitions
            obj.PhasesPerBatch = floor(min([2048/(9*obj.nSteps+6),16000/(2*nPPL)]))-nMisses;                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
            nPhaseBatches = ceil(nRepetitions/obj.PhasesPerBatch);
            
            % Get the gradients for current block
            gradientVectors = gradientVectorsShort;
                      
            % Start batches sweep
            obj.readoutAmplitudeP = 0;
            obj.phaseAmplitudeP = 0;
            obj.sliceAmplitudeP = 0;
            for iBatch = 1:nPhaseBatches
                tA = clock;
                if(iBatch==1)
                    fprintf('Radial, batch %1.0f/%1.0f, estimating remaining time \n',iBatch,nPhaseBatches);
                else
                    tC = tC*(nPhaseBatches-iBatch+1);
                    fprintf('Radial, batch %1.0f/%1.0f, remaining time: %1.0f s \n',iBatch,nPhaseBatches,tC);
                end
                if(spoilerEnable)
                    fprintf('Calibrating frequency... \n')
                    % Get central frequency
                    f0 = obj.GetFIDParameters;
                    obj.SetParameter('FREQUENCY',f0);
                    nFreq = nFreq+1;
                    rawData.aux.freqListRadial(nFreq) = f0;
                    fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
                end
                
                % Get the appropiate axis
                if(iBatch<nPhaseBatches)
                    obj.Axis1Vector = gradientVectors((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,1);
                    obj.Axis2Vector = gradientVectors((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,2);
                    obj.Axis3Vector = gradientVectors((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,3);
                    obj.lastBatch = 0;
                else
                    obj.Axis1Vector = gradientVectors((iBatch-1)*obj.PhasesPerBatch+1:nRepetitions,1);
                    obj.Axis2Vector = gradientVectors((iBatch-1)*obj.PhasesPerBatch+1:nRepetitions,2);
                    obj.Axis3Vector = gradientVectors((iBatch-1)*obj.PhasesPerBatch+1:nRepetitions,3);
                    obj.lastBatch = 0;
                end
                obj.nPhasesBatch = length(obj.Axis1Vector);
                
                % NOW RUN ACTUAL ACQUISTION
                obj.CreateSequence(0);
                if(sum(obj.DELAYs<0.1)~=0)
                    warndlg('At least one delay is negative!','Delay Error')
                    return;
                end
                obj.Sequence2String;
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Radial batch aborted!','Warning')
                    return;
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = reshape(complex(DataBatch(:,1),DataBatch(:,2))/bwCalNormVal,nPPL,2*obj.nPhasesBatch);
                dataShort = cat(1,dataShort,reshape(cDataBatch(:,1:2:end),nPPL*obj.nPhasesBatch,1));
                cDataBatch(:,1:2:end) = [];
                dataLong = cat(1,dataLong,reshape(cDataBatch,nPPL*obj.nPhasesBatch,1));
                tB = clock;
                tC = etime(tB,tA);
            end
            
            %% Single point
            % Create waiting bar
            obj.Stage = 2;
            obj.PhasesPerBatch = floor(min([2048/(3*obj.nSteps+5),16000/1]))-nMisses;
            nAcquiredPoints = size(gradientVectors2,1);
            nPhaseBatches = ceil(nAcquiredPoints/obj.PhasesPerBatch);
            
            % Start batches sweep
            fprintf('SINGLE POINT \n');
            obj.SetParameter('NPOINTS',1);
            obj.SetParameter('REPETITIONDELAY',rawData.aux.delayCartesian);
            nFreq = 0;
            for iBatch = 1:nPhaseBatches
                if(iBatch==1)
                    fprintf('Single point, batch %1.0f/%1.0f, estimating remaining time \n',iBatch,nPhaseBatches);
                else
                    tC = tC*(nPhaseBatches-iBatch+1);
                    fprintf('Single point, batch %1.0f/%1.0f, remaining time: %1.0f s \n',iBatch,nPhaseBatches,tC);
                end
                if(spoilerEnable)
                    fprintf('Calibrating frequency... \n')
                    % Get central frequency
                    f0 = obj.GetFIDParameters;
                    obj.SetParameter('FREQUENCY',f0);
                    nFreq = nFreq+1;
                    rawData.aux.freqListCart(nFreq) = f0;
                    fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
                end
                % Get the appropiate axis
                if(iBatch<nPhaseBatches)
                    obj.Axis1Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,1);
                    obj.Axis2Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,2);
                    obj.Axis3Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,3);
                    obj.lastBatch = 0;
                else
                    obj.Axis1Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:end,1);
                    obj.Axis2Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:end,2);
                    obj.Axis3Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:end,3);
                    obj.lastBatch = 1;
                end
                obj.nPhasesBatch = length(obj.Axis1Vector);
                
                % NOW RUN ACTUAL ACQUISTION
                obj.CreateSequence(0);
                if(sum(obj.DELAYs<0.1)~=0)
                    warndlg('At least one delay is negative!','Delay Error')
                    return;
                end
                obj.Sequence2String;
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Single Point batch aborted!','Warning')
                    return;
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2))/bwCalNormVal;
                dataShort = cat(1,dataShort,cDataBatch);
            end
            obj.SetParameter('NPOINTS',nPPL);
            obj.SetParameter('REPETITIONDELAY',rawData.aux.delayRadial);
            disp(' ');
            
            %% Regridding
            kSpaceValuesShort = [kSpaceValuesShort,dataShort(:)];
            kSpaceValuesLong = [kSpaceValuesLong,dataLong(:)];
            rawData.kSpace.sampledShort = kSpaceValuesShort;
            rawData.kSpace.sampledLong = kSpaceValuesLong;
            
            kCartesian = rawData.aux.kCartesian;
            if(nCir>1)
                valCartesianShort = griddata(kSpaceValuesShort(:,1),kSpaceValuesShort(:,2),kSpaceValuesShort(:,3),kSpaceValuesShort(:,4),kCartesian(:,1),kCartesian(:,2),kCartesian(:,3));
                valCartesianLong = griddata(kSpaceValuesLong(:,1),kSpaceValuesLong(:,2),kSpaceValuesLong(:,3),kSpaceValuesLong(:,4),kCartesian(:,1),kCartesian(:,2),kCartesian(:,3));
            elseif(nCir==1 && nLPC>2)
                valCartesianShort = griddata(kSpaceValuesShort(:,1),kSpaceValuesShort(:,2),kSpaceValuesShort(:,4),kCartesian(:,1),kCartesian(:,2));
                valCartesianLong = griddata(kSpaceValuesLong(:,1),kSpaceValuesLong(:,2),kSpaceValuesLong(:,4),kCartesian(:,1),kCartesian(:,2));
            elseif(nCir==1 && nLPC==2)
                valCartesianShort = pchip(kSpaceValuesShort(:,1),kSpaceValuesShort(:,4),kCartesian(:,1));
                valCartesianLong = pchip(kSpaceValuesLong(:,1),kSpaceValuesLong(:,4),kCartesian(:,1));
            end
            valCartesianShort(isnan(valCartesianShort)) = 0;
            valCartesianLong(isnan(valCartesianLong)) = 0;
            
            % Add phase according to fovDeviation
            DELX = rawData.inputs.fovDeviation(1);
            DELY = rawData.inputs.fovDeviation(2);
            DELZ = rawData.inputs.fovDeviation(3);
            phase = exp(-2*pi*1i*(DELX*kCartesian(:,1)+DELY*kCartesian(:,2)+DELZ*kCartesian(:,3)));
            
            % Save interpolated data
            interpolatedShort = [kCartesian,valCartesianShort(:).*phase];
            interpolatedLong = [kCartesian,valCartesianLong(:).*phase];
            rawData.kSpace.interpolatedShort = interpolatedShort;
            rawData.kSpace.interpolatedLong = interpolatedLong;
            interpolatedShort = reshape(interpolatedShort(:,4),nPoints);
            interpolatedLong = reshape(interpolatedLong(:,4),nPoints);
            
            imagenShort = abs(ifftshift(ifftn(interpolatedShort)));
            imagenLong = abs(ifftshift(ifftn(interpolatedLong)));
            
            rawData.kSpace.imagenShort = imagenShort;
            rawData.kSpace.imagenLong = imagenLong;
            
            %% Save data
            % Short
            rawData.kSpace.sampled = kSpaceValuesShort;
            rawData.kSpace.imagen = imagenShort;
            time = clock;
            name = strcat('rawDataShort-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
            
            % Long
            rawData.kSpace.sampled = kSpaceValuesLong;
            rawData.kSpace.imagen = imagenLong;
            name = strcat('rawDataLong-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
            
        end
        
        
        %**************************************************************************
        %**************************************************************************
        %**************************************************************************
        
        
        function GetCartesianParameters(obj)
            global rawData;
            
            nPoints = ...
                [obj.GetParameter('NREADOUTS'),...
                obj.GetParameter('NPHASES'),...
                obj.GetParameter('NSLICES')];
            fov     = ...
                [obj.GetParameter('FOVX'),...
                obj.GetParameter('FOVY'),...
                obj.GetParameter('FOVZ')];
            deltaK  = 1./fov;
            kMax    = nPoints./(2*fov);
            DELX    = obj.GetParameter('DELTAX');
            DELY    = obj.GetParameter('DELTAY');
            DELZ    = obj.GetParameter('DELTAZ');
            
            rawData.inputs.NEX          = obj.GetParameter('NSCANS');
            rawData.inputs.nPoints      = nPoints;
            rawData.inputs.fov          = fov;
            rawData.inputs.fovDeviation = [DELX,DELY,DELZ];
            rawData.aux.deltaK          = deltaK;
            rawData.aux.kMax            = kMax;
        end
        function GetSequenceParameters(obj)
            
            global rawData;
            
            coilRiseTime = obj.GetParameter('COILRISETIME');
            blankingTime = obj.GetParameter('BLANKINGDELAY');
            trf = obj.GetParameter('PULSETIME');                            % RF pulse time
            td = obj.GetParameter('TRANSIENTTIME');                         % Dead time to wait until TX/RX switching
            tMax = obj.GetParameter('TMAX');                                % Max time for acquisition, it should be the shortest T2 to be acquired
            underSampling = obj.GetParameter('UNDERSAMPLING');              % Undersampling for azimutal acquisition.
            TE = obj.GetParameter('ECHOTIME');
            ED = obj.GetParameter('ECHODELAY');
            TR = obj.GetParameter('REPETITIONTIME');
            gradientDelay = obj.gradDelay; %s for Gradients to rise up + blk
%             obj.repetitionDelay = TR-coilRiseTime-gradientDelay-0.5*trf-TE-ED;
%             obj.SetParameter('REPETITIONDELAY',obj.repetitionDelay);
            nMisses = obj.GetParameter('JUMPINGFACTOR');
            
            rawData.inputs.pulseTime = trf;
            rawData.inputs.deadTime = td;
            rawData.inputs.tMax = tMax;
            rawData.inputs.coilRiseTime = coilRiseTime;
            rawData.inputs.gradientDelay = gradientDelay;
            rawData.inputs.blankingTime = blankingTime;
%             rawData.aux.delay = obj.repetitionDelay;
            rawData.inputs.undersampling = underSampling;
            rawData.inputs.jumpingFactor = nMisses;
            rawData.inputs.rfAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.rfFrequency = obj.GetParameter('FREQUENCY');
            rawData.inputs.echoTime = TE;
            rawData.inputs.echoDelay = ED;
            rawData.inputs.repetitionTime = TR;
            rawData.inputs.axes = [obj.GetParameter('READOUT'),...
                obj.GetParameter('PHASE'),obj.GetParameter('SLICE')];
        end
        function SetSamplingParameters(obj)
            % gradientAmplitude = normalized amplitude to the MRIGUI input
            % nLPC = number of lines per circunferece in k-space
            % nCir = number of circunfereces in gradient space
            % nPPL = number of acquired points per line in k-space
            
            % Load rawData
            global rawData;
            gammabar        = 42.6d6;
            tMax            = rawData.inputs.tMax;
            kMax            = rawData.aux.kMax;
            nPoints         = rawData.inputs.nPoints;
            underSampling   = sqrt(rawData.inputs.undersampling);
            
            % Set first block parameters
            bw                          = max(nPoints)/(2*tMax);                            % ideal acquisition bandwidth
            gradientAmplitudes          = kMax./(gammabar*tMax).*~(nPoints==1);             % gradient amplitudes for each radial block (T/m)
            nPPL                        = ceil(sqrt(3)*tMax*bw)+1;                          % number of points per line
            nLPC                        = ceil(max(nPoints(1:2))*pi/underSampling);         % max number of lines per circumference
            nLPC                        = max(nLPC-mod(nLPC,2),1);
            nCir                        = max(ceil(nPoints(3)*pi/2/underSampling)+1,1);     % number of circumferences
            if(nPoints(3)==1)
                nCir = 1;
            end
            if(nPoints(2)==1)
                nLPC = 2;
            end
            
            % Get acquisition time
            acqTime = nPPL./bw;
            
            % Repetition delay for radial and cartesian part
            TR = rawData.inputs.repetitionTime;
            coilRiseTime = rawData.inputs.coilRiseTime;
            gradientDelay = rawData.inputs.gradientDelay;
            trf = rawData.inputs.pulseTime;
            TE = rawData.inputs.echoTime;
            ED = rawData.inputs.echoDelay;
            deadTime = rawData.inputs.deadTime;
            
            rawData.aux.delayRadial = TR-coilRiseTime-gradientDelay-0.5*trf-TE-ED;
            rawData.aux.delayCartesian = TR-coilRiseTime-gradientDelay-0.5*trf-deadTime-1/bw;
            
            % Save data in rawData
            rawData.aux.bandwidth               = bw;
            rawData.aux.acquisitionTime         = acqTime;
            rawData.aux.numberOfPointsPerLine   = nPPL;
            rawData.aux.linesPerCircumference   = nLPC;
            rawData.aux.numberOfCircumferences  = nCir;
            rawData.aux.gradientAmplitudes      = gradientAmplitudes;
        end
        function GetRadialGradients(obj)
            % Gradient are defined by the sphere parametric equation. The
            % two parameteres are: theta in [0,pi] and phi [0,2pi].
            % Gradients are normalized to the amplifier amplitude.
            % First, I calculate the number of repetitions:
            
            % Load rawData
            global rawData;
            nCir                = rawData.aux.numberOfCircumferences;
            nLPC                = rawData.aux.linesPerCircumference;
            gradientAmplitudes  = rawData.aux.gradientAmplitudes;
            
            % Get number of radial repetitions
            % Get theta vector for current block
            if(nCir==1)
                theta = pi/2;
            else
                theta = linspace(0,pi,nCir);
            end
            % Get number of radial repetitions for current block;
            nRepetitions = 0;
            for jj = 1:nCir
                nRepetitions = nRepetitions+max(ceil(nLPC*sin(theta(jj))),1);
            end
            
            % Calculate radial gradients
            normalizedGradientsRadial = zeros(sum(nRepetitions),3);
            n = 0;
            
            % Calculate the normalized gradients:
            for jj = 1:nCir
                nLPCjj = max(ceil(nLPC*sin(theta(jj))),1);
                deltaPhi = 2*pi/nLPCjj;
                phi = linspace(0,2*pi-deltaPhi,nLPCjj);
                for kk = 1:nLPCjj
                    n = n+1;
                    normalizedGradientsRadial(n,1) = sin(theta(jj))*cos(phi(kk));
                    normalizedGradientsRadial(n,2) = sin(theta(jj))*sin(phi(kk));
                    normalizedGradientsRadial(n,3) = cos(theta(jj));
                end
            end
            
            % Set gradients to T/m
            gradientVectors1 = normalizedGradientsRadial*diag(gradientAmplitudes);
            
            % Save to rawData
            rawData.aux.numberOfRadialRepetitions = nRepetitions;
            rawData.aux.normalizedGradientsRadial = normalizedGradientsRadial;
            rawData.aux.gradientsRadialTeslasPerMeter = gradientVectors1;
        end
        function GetRadialKPoints(obj)
            
            %Load rawData
            global rawData;
            gammabar = 42.6d6;
            nPPL                        = rawData.aux.numberOfPointsPerLine;
            trf                         = rawData.inputs.pulseTime;
            td                          = rawData.inputs.deadTime;
            bw                          = rawData.aux.bandwidth;
            normalizedGradientsRadial   = rawData.aux.normalizedGradientsRadial;
            nRepetitions                = rawData.aux.numberOfRadialRepetitions;
            gradientAmplitudes          = rawData.aux.gradientAmplitudes;
            
            % Calculate the radial k points for first acquisition
            % Calculate k-points at t = 0.5*trf+td+0.5/bw
            normalizedKRadial1 = zeros(nRepetitions,3,nPPL);
            normalizedKRadial1(:,:,1) = (0.5*trf+td+0.5/bw)*normalizedGradientsRadial;
            % Calculate all k-points
            for jj=2:nPPL
                normalizedKRadial1(:,:,jj) = normalizedKRadial1(:,:,1)+(jj-1)*normalizedGradientsRadial/bw;
            end
            normalizedKRadial1 = reshape(permute(normalizedKRadial1,[3,1,2]),[nRepetitions*nPPL,3]);
            kRadial1 = normalizedKRadial1*diag(gammabar*gradientAmplitudes);
            
            % Calculate the radial k points for second acquisition
            % calculate k-points at t = 0.5/bw
            normalizedKRadial2 = zeros(nRepetitions,3,nPPL);
            normalizedKRadial2(:,:,1) = normalizedGradientsRadial*(0.5+nPPL-1)/bw;
            % Calculate all k-points
            for jj=2:nPPL
                normalizedKRadial2(:,:,jj) = normalizedKRadial2(:,:,1)-(jj-1)*normalizedGradientsRadial/bw;
            end
            normalizedKRadial2 = reshape(permute(normalizedKRadial2,[3,1,2]),[nRepetitions*nPPL,3]);
            kRadial2 = normalizedKRadial2*diag(gammabar*gradientAmplitudes);
            
            % Save to rawData
            rawData.aux.radialKPointsShort = kRadial1;
            rawData.aux.radialKPointsLong = kRadial2;
        end
        function GetCartesianKPoints(obj)
            
            % Load rawData
            global rawData;
            gammabar = 42.6d6;
            trf = rawData.inputs.pulseTime;
            td = rawData.inputs.deadTime;
            bw = rawData.aux.bandwidth;
            nPoints = rawData.inputs.nPoints;
            kMax = rawData.aux.kMax;
            gradientAmplitudes = rawData.aux.gradientAmplitudes;
            % Get minimun time
            tMin = 0.5*trf+td+0.5/bw;
            
            % Get the full cartesian points
            kx = linspace(-kMax(1)*~isequal(nPoints(1),1),kMax(1)*~isequal(nPoints(1),1),nPoints(1));
            ky = linspace(-kMax(2)*~isequal(nPoints(2),1),kMax(2)*~isequal(nPoints(2),1),nPoints(2));
            kz = linspace(-kMax(3)*~isequal(nPoints(3),1),kMax(3)*~isequal(nPoints(3),1),nPoints(3));
            [kx,ky,kz] = meshgrid(kx,ky,kz);
            kx = permute(kx,[2,1,3]);
            ky = permute(ky,[2,1,3]);
            kz = permute(kz,[2,1,3]);
            kCartesian(:,1) = kx(:);
            kCartesian(:,2) = ky(:);
            kCartesian(:,3) = kz(:);
            
            % Get the points that should be acquired in a time shorter than
            % tMin
            normalizedKCartesian = kCartesian/diag(gammabar*(gradientAmplitudes+double(gradientAmplitudes==0)));
            normalizedKCartesian(:,4) = sqrt(sum(normalizedKCartesian.^2,2));
            normalizedKSinglePoint = normalizedKCartesian(normalizedKCartesian(:,4)<tMin,1:3);
            kSinglePoint = normalizedKSinglePoint*diag(gammabar*gradientAmplitudes);
            
            % Save to rawData
            rawData.aux.numberOfCartesianRepetitions = size(kSinglePoint,1);
            rawData.aux.tMin = tMin;
            rawData.aux.cartesianKPoints = kSinglePoint;
            rawData.aux.kCartesian = kCartesian;
        end
        
    end
    
end

