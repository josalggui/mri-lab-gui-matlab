classdef EDDYCURRENTSTEST < MRI_BlankSequence
    properties
        maxRepetitionsPerBatch;
        nPhasesBatch;
        lastBatch;
        RepetitionTime;
        AcquisitionTime;  
        StartSlice;
        EndSlice;
        PhasesPerBatch = 20;
        PhasesinThisBatch = 1;
        StartPhase;
        EndPhase;
    end
    
    methods (Access=private)
        function CreateParameters(obj)
            obj.CreateOneParameter('GRADIENTAMPL','Gradient Amplitude','',0.01)
            obj.CreateOneParameter('GRADIENTTIME','Gradient Time','us',500*MyUnits.us)
            obj.CreateOneParameter('RFDELAY','RFDELAY','us',200*MyUnits.us)
            obj.CreateOneParameter('REPETITIONDELAY','Repetition Delay','ms',50*MyUnits.ms)
            obj.CreateOneParameter('TR','TR','ms',1000*MyUnits.ms);
            
            
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT','READOUT'...
                'NSLICES','BLANKINGDELAY','PHASE','SLICE','SPOILERTIME','SPOILERAMP',...
                'REPETITIONDELAY'}; 
        end  
    end
    
    

    methods
        function obj = EDDYCURRENTSTEST(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'EDDYCURRENTSTEST';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
        end
        
         
        function CreateSequence(obj)
            
            [shimS,shimP,shimR] = SelectAxes(obj);
            shimG = [shimR,shimP,shimS];
            [~,~,ENRO,ENSL,ENPH,~,~] = obj.ConfigureGradients(0);
            
            obj.nline_reads = 0;
            
            eje = 1;
            if(ENRO==1)
                eje = 1;
            elseif(ENPH==1)
                eje = 2;
            elseif(ENSL==1)
                eje = 3;
            end
            dac = [obj.selectReadout,obj.selectPhase,obj.selectSlice];
            
            % Initialize all vectors to zero with dynamic size
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs,obj.RFamps] = deal([]);
            
            global scanner
            iIns = 1;    
            blkTime         = scanner.BlankingDelay;
            pulseTime       = obj.GetParameter('PULSETIME')*1e6;
            deadTime        = obj.GetParameter('TRANSIENTTIME')*1e6;
            TR              = obj.GetParameter('TR')*1e6;
            rfDelay         = obj.GetParameter('RFDELAY')*1e6;
            crTime          = obj.GetParameter('COILRISETIME')*1e6;
            acqTime         = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6;
            gradTime        = obj.GetParameter('GRADIENTTIME')*1e6; 
            gradAmp         = obj.GetParameter('GRADIENTAMPL'); 
            gradDelayTime   = scanner.grad_delay;
            
            if(ENRO+ENPH+ENSL==0)
                gradAmp = 0;
            end
            gradList = [0,gradAmp,-gradAmp];
            
            %%%%%%%%%%%%%%%%%%%%%%%%
            % Sequence starts here %
            %%%%%%%%%%%%%%%%%%%%%%%%
            
            % Activate shimming
            iIns = obj.RampGradient(iIns,[0 0 0],shimG,dac,0.3,1);
            
            for ii = 1:3
                % Generate zero gradient
                iIns = obj.RampGradient(iIns,shimG(eje),shimG(eje)+gradList(ii),dac(eje),crTime,obj.nSteps);
                obj.DELAYs(iIns-1) = crTime/obj.nSteps+gradTime;
                iIns = obj.RampGradient(iIns,shimG(eje)+gradList(ii),shimG(eje),dac(eje),crTime,obj.nSteps);
                obj.DELAYs(iIns-1) = crTime/obj.nSteps+rfDelay-blkTime+gradDelayTime;
                
                % Excitiation pulse
                iIns = obj.CreatePulse(iIns,0,blkTime,pulseTime,deadTime);
            
                % Acquisition
                iIns = obj.Acquisition(iIns,acqTime);
                obj.nline_reads = obj.nline_reads+1;
                
                % Repetition delay
                if ii==1
                    repDelay = TR-sum(obj.DELAYs(4:end));
                end
                iIns = RepetitionDelay (obj,iIns,ii==3,repDelay);
            end
            obj.nInstructions = iIns-1;
        end
        
        function [status,result] = Run(obj,iAcq)
            clc
            global rawData
            obj.nSteps = 300;
            rawData = [];
            rawData.sequenceName = obj.SequenceName;
            rawData.nPoints = obj.GetParameter('NPOINTS');
            rawData.gradientAmpl = obj.GetParameter('GRADIENTAMPL');
            rawData.gradientTime = obj.GetParameter('GRADIENTTIME');
            rawData.rfDelay = obj.GetParameter('RFDELAY');
            rawData.deadTime = obj.GetParameter('TRANSIENTTIME');
            rawData.coilRiseTime = obj.GetParameter('COILRISETIME');
            
            obj.CreateSequence;
            obj.Sequence2String;
            fname = 'TempBatch.bat';
            obj.WriteBatchFile(fname);
            [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
            if status == 0 && ~isempty(result)
                C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                res = C{:};
                obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
            else
                warndlg('Batch aborted!','Warning')
                return;
            end
            DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
            bwCalNormVal = obj.calculate_bwCalNormVal;
            cDataBatch = complex(DataBatch(:,1),DataBatch(:,2))/bwCalNormVal;
            
            nPoints = obj.GetParameter('NPOINTS');
            BW = obj.GetParameter('SPECTRALWIDTH');
            data = reshape(cDataBatch,nPoints,3);
            trf = obj.GetParameter('PULSETIME');
            td = obj.GetParameter('TRANSIENTTIME');
            timeVector = 0.5*trf+td+linspace(0,nPoints/BW,nPoints)';
            rawData.outputs.fid = [timeVector,data];
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
    end
    
end