classdef PREEMPHASIS_TEST < MRI_BlankSequence  
    properties
        maxRepetitionsPerBatch;
        nPhasesBatch;
        lastBatch;
        RepetitionTime;  
    end
    
    methods (Access=private)
        function CreateParameters(obj)
            CreateOneParameter(obj,'NREPETITIONS','Periode repetitions','',5)
            CreateOneParameter(obj,'RISETIME','Rise time','us',100*MyUnits.us)
            CreateOneParameter(obj,'DUTYCICLE','Duty cicle','',50)
            CreateOneParameter(obj,'GRADAMPLITUDEMAX','Gradient ampltude (T/m)','',0.1)
            CreateOneParameter(obj,'FLATTOP','Flat top time','us',500*MyUnits.us)
            CreateOneParameter(obj,'THICKNESS','Thickness shielding','um',50*MyUnits.um)
            
            obj.InputParHidden = {'SHIMSLICE','NSCANS','SHIMPHASE','SHIMREADOUT',...
                'NPOINTS','NSLICES','FREQUENCY','SPECTRALWIDTH','SPOILERTIME','SPOILERAMP'...
                'RFAMPLITUDE','PULSETIME','BLANKINGDELAY','TRANSIENTTIME','COILRISETIME',...
                'PHASE','SLICE'}; 
        end  
    end
    
    methods
        function obj = PREEMPHASIS_TEST(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'PREEMPHASIS_TEST';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
        end
        
        function UpdateTETR(obj)
        end
         
        function CreateSequence( obj)
            nRepetitions = obj.GetParameter('NREPETITIONS');
            obj.nline_reads = nRepetitions; 
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            dutycicle = obj.GetParameter('DUTYCICLE');
            trise = obj.GetParameter('RISETIME')*1e6;
            tflattop = obj.GetParameter('FLATTOP')*1e6; 
            G=obj.GetParameter('GRADAMPLITUDEMAX');
            periode = (100*(2*trise+tflattop))/dutycicle;
            delay = periode-(2*trise+tflattop);
            th=obj.GetParameter('THICKNESS');
            c = load('Gradient Constants.txt');
            selectReadout = obj.selectReadout;
            preempgradient = obj.HalfPreemphasis(trise,tflattop,G,th);
            dt=preempgradient(2,1)-preempgradient(1,1);
            
            
            switch num2str(obj.GetParameter('READOUT'))
                case 'x'
                    obj.selectReadout = 2; G=G/c(1);
                case 'y'
                    obj.selectReadout = 0; G=G/c(2);
                case'z'
                    obj.selectReadout = 1; G=G/c(3);
            end
            
            iIns = 1;    
            
            for i = 1:length(preempgradient)
                iIns = obj.PulseGradient(iIns,preempgradient(i,2),selectReadout,dt);
            end
            
            iIns = RepetitionDelay (obj,iIns,1,delay);
            obj.DACs(iIns-1) =  selectReadout;
            obj.nInstructions=iIns-1;
        end
        
        function [status,result] = Run(obj,iAcq)
            clc            
            nBatches = 1;
            t=0;
            for iBatch = 1:nBatches
                obj.CreateSequence;
                obj.Sequence2String;            
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                t1 = clock;
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                t2 = clock;
                t = t + etime(t2,t1);
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Batch aborted!','Warning')
                    return;
                end
            end
        end
    end   
end

