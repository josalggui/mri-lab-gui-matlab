classdef SINCSLICESELECT < MRI_BlankSequence   
    properties
        maxRepetitionsPerBatch;
        nPhasesBatch;
        lastBatch;
        RepetitionTime;
        AcquisitionTime;
        EndPhase;
        PhasesPerBatch = 20;
        nSteps=5;
        gslice;
        gSteps=1000;
    end
    methods (Access=private)
        function CreateParameters(obj)
            obj.CreateOneParameter('FOVZ','Slice thick','mm',20*MyUnits.mm)
            obj.CreateOneParameter('SLICEPOSITION','Slice position','mm',0*MyUnits.mm)
            obj.CreateOneParameter('PULSETIME','SINC-Pulse Time','us',250*MyUnits.us);
            obj.CreateOneParameter('REPETITIONTIME','TR','ms',50*MyUnits.ms);
            obj.CreateOneParameter('ACQTIME','Acq time','ms',5*MyUnits.ms);
            obj.InputParHidden = {'SPECTRALWIDTH','SHIMSLICE','SHIMPHASE','SHIMREADOUT','READOUT','PHASE','SLICE','SPOILERTIME','SPOILERAMP','NSLICES','BLANKINGDELAY','TRANSIENTTIME'};
        end  
    end

    methods
        function obj = SINCSLICESELECT(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'SINCSLICESELECT';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
        end    
         
        function CreateSequence(obj)
            [shimS,shimP,shimR] = SelectAxes(obj);
            obj.nline_reads = 1;
            pulseTime = obj.GetParameter('PULSETIME')*1e6;
            deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
            TR = obj.GetParameter('REPETITIONTIME')*1e6;
            blkDelay = obj.GetParameter('BLANKINGDELAY')*1e6;
            crTime = obj.GetParameter('COILRISETIME')*1e6;
            Tacq=obj.GetParameter('ACQTIME')*1e6;
            nPoints = obj.GetParameter('NPOINTS');
            gradDelayTime=55;
            sliceDelayTime=0.1;
            gapTime = 0.1;
            iIns = 1;
            RR=2;
            PP=0;
            SS=1;
            obj.SetParameter('SPECTRALWIDTH',nPoints/(Tacq*1e-6));
            obj.nInstructions = 8+4*obj.nSteps;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.RFamps,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));

            %% SHIMMING in all gradients (3 instructions)
            iIns = obj.PulseGradient(iIns,shimR,RR,0.1);
            iIns = obj.PulseGradient(iIns,shimP,PP,0.1);
            iIns = obj.PulseGradient(iIns,shimS,SS,0.1);
            
            %% Activate dephasing slice gradient (nSteps instructions)
            for ii = 1:obj.nSteps
                iIns = obj.PulseGradient(iIns,shimS+(obj.gslice)*ii/obj.nSteps,SS,crTime/obj.nSteps);
            end
            obj.DELAYs(iIns-1) = crTime/obj.nSteps+gradDelayTime-blkDelay;
            
            %% RF pulse (3 instruction)
            iIns = obj.CreatePulse(iIns,0,blkDelay,pulseTime,0.1);
            obj.ENVELOPEs(iIns-2) = 0;
            
            %% Deactivate dephasing slice gradient (nSteps instructions)
            for ii = 1:obj.nSteps
                iIns = obj.PulseGradient(iIns,shimS+(obj.gslice)*(1-ii/obj.nSteps),SS,crTime/obj.nSteps);
            end
            
            %% Activate rephasing slice gradient (nSteps instructions)
            for ii = 1:obj.nSteps
                iIns = obj.PulseGradient(iIns,shimS-(obj.gslice)*ii/obj.nSteps,SS,crTime/obj.nSteps);
            end
            if(pulseTime>=crTime-2*gradDelayTime)
                obj.DELAYs(iIns-1) = crTime/obj.nSteps+(0.5*pulseTime+gradDelayTime-0.5*crTime)+sliceDelayTime;
            else
                obj.DELAYs(iIns-1) = crTime/obj.nSteps;
            end
            
            %% Deactivate rephasing slice gradient (nSteps instructions)
            for ii = 1:obj.nSteps
                iIns = obj.PulseGradient(iIns,shimS-(obj.gslice)*(1-ii/obj.nSteps),SS,crTime/obj.nSteps);
            end
            obj.DELAYs(iIns-1) = 0.1;

            %% Acquisition (1 ins)
            iIns = obj.Acquisition(iIns,Tacq);

            %% Delay for recover T1 (1 ins)
            obj.RepetitionDelay (iIns,1,TR);
        end
        
        
        function [status,result] = Run(obj,~)
            clc
            c = obj.ReorganizeCalibrationData();
            global rawData;
            rawData = [];
            gammabar = 42.6d6;
            gamma=2*pi*gammabar;
 
            FOVZ = obj.GetParameter('FOVZ'); slicepos= obj.GetParameter('SLICEPOSITION');pulseTime90=obj.GetParameter('PULSETIME'); RFamp = obj.GetParameter('RFAMPLITUDE'); pulsetime90 = obj.GetParameter('PULSETIME'); deadTime = obj.GetParameter('TRANSIENTTIME');blkDelay = obj.GetParameter('BLANKINGDELAY');TR = obj.GetParameter('REPETITIONTIME');            
%             rawData.inputs.SlopeRate=SRO; rawData.inputs.LAMBDA=L;rawData.inputs.AcelerationFactor = obj.GetParameter('UNDERSAMPLING');rawData.inputs.MaxGradient = G*1e-3; rawData.inputs.rfAmplitude90 = obj.GetParameter('RFAMPLITUDE');rawData.inputs.PulseTime = obj.GetParameter('PULSETIME'); rawData.inputs.pulseTime90 = pulseTime90; rawData.inputs.deadTime = deadTime; rawData.inputs.TR = obj.GetParameter('REPETITIONTIME');rawData.inputs.FOVY = FOVY;rawData.inputs.NY = NY;rawData.inputs.FOVX = FOVX;rawData.inputs.NX = NX;rawData.inputs.SliceThick = FOVZ;
            
            gslice=7.2/(gammabar*FOVZ*pulsetime90);
            obj.gslice=gslice/c(3);
            obj.SliceFreqOffset=gammabar*obj.gslice*slicepos;
           
            obj.nInstructions = 8+4*obj.nSteps;
            obj.maxRepetitionsPerBatch = floor(2048/obj.nInstructions);
            
            obj.CreateSequence;
            obj.Sequence2String;
            fname = 'TempBatch.bat';
            obj.WriteBatchFile(fname);
            [status,result] = system(fullfile(obj.path_file,fname));
            if status == 0 && ~isempty(result)
                C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                res = C{:};
                obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz'));
                obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms);
                obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
            else
                warndlg('Batch aborted!','Warning')
                return;
            end
            DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
            cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
            
%             tsampled=linspace(0,Tacq,floor(bw*Tacq));
%             kxsampled=interp1(t,kx,tsampled);     kysampled=interp1(t,ky,tsampled);
%             kSpace=[kxsampled' kysampled'  cDataBatch];
%             
%             signalint = griddata(kxsampled',kysampled',cDataBatch,KXcart,KYcart);
            
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');

        end
    end
end