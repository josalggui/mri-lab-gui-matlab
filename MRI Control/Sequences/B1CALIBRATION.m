classdef B1CALIBRATION < MRI_BlankSequence   
    properties
        maxRepetitionsPerBatch;
        nPhasesBatch;
        lastBatch;
        RepetitionTime;
        AcquisitionTime;
        EndPhase;
        PhasesPerBatch = 20;
    end
    methods (Access=private)
        function CreateParameters(obj)
            obj.CreateOneParameter('TRANSIENTTIME','Dead Time','us',100*MyUnits.us);
            obj.CreateOneParameter('REPETITIONTIME','TR','ms',50*MyUnits.ms);
            obj.CreateOneParameter('B1STEPS','B1 steps','',20);
            obj.CreateOneParameter('B1MIN','RFamp min','',0.001);
            obj.CreateOneParameter('B1MAX','RFamp max','',0.01);
            obj.CreateOneParameter('TSTEPS','Temporal steps','',20);
            obj.CreateOneParameter('TMIN','RF pulse min','us',0*MyUnits.ms);
            obj.CreateOneParameter('TMAX','RF pulse max','us',50*MyUnits.us);
            
            obj.InputParHidden = {'SHIMSLICE','PULSETIME','RFAMPLITUDE','SHIMPHASE','SHIMREADOUT'...
                'READOUT','PHASE','SLICE','COILRISETIME','SPOILERTIME',...
                'SPOILERAMP','NSLICES','BLANKINGDELAY','NPOINTS'};
        end  
    end
    
    methods
        function obj = B1CALIBRATION(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'B1CALIBRATION';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
        end
        
        function UpdateTETR(obj)
            obj.TTotal = obj.GetParameter('NSCANS')* obj.GetParameter('REPETITIONTIME') * obj.GetParameter('TSTEPS')* obj.GetParameter('B1STEPS');
        end
         
        function CreateSequence( obj)
            [shimS,shimP,shimR] = SelectAxes(obj);     
            
            bw=obj.GetParameter('SPECTRALWIDTH');
            acqTime = 1/bw*1e6;
            Tsteps = obj.GetParameter('TSTEPS');
            B1steps = obj.GetParameter('B1STEPS');
            B1min = obj.GetParameter('B1MIN');
            B1max = obj.GetParameter('B1MAX');
            pulseTimemin = obj.GetParameter('TMIN')*1e6;
            pulseTimemax = obj.GetParameter('TMAX')*1e6;
            deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
            TR = obj.GetParameter('REPETITIONTIME')*1e6;
            blkDelay = obj.GetParameter('BLANKINGDELAY')*1e6;
            pulsetimelist=linspace(pulseTimemin,pulseTimemax,Tsteps);
            obj.SetParameter('NPOINTS',1);
            
            obj.nline_reads = Tsteps;   
            rfphase=0;
            
             obj.nInstructions = 0;
            [obj.RFamps,obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            iIns = 1;

                for j=1:Tsteps
                    %% Shimming all gradients
                    iIns = obj.PulseGradient (iIns,shimR,2,0.1);
                    iIns = obj.PulseGradient (iIns,shimP,0,0.1);
                    iIns = obj.PulseGradient (iIns,shimS,1,0.1);
                    
                    %% 90º pulse
                    iIns = obj.CreatePulse(iIns,rfphase,blkDelay,pulsetimelist(j),deadTime);
                    
                    %% Acquisition
                    iIns = obj.Acquisition(iIns,acqTime);
                    
                    %% RepetitionDelay
                    iIns = obj.RepetitionDelay (iIns,1,TR-acqTime-deadTime-blkDelay-pulsetimelist(j));
                end
            obj.nInstructions = iIns-1;
   end
        
        function [status,result] = Run(obj,iAcq)
            clc
            global rawData;
            rawData = [];
            
            % Input data
            B1steps = obj.GetParameter('B1STEPS');
            RFmin = obj.GetParameter('B1MIN');
            RFmax = obj.GetParameter('B1MAX');
            Tsteps = obj.GetParameter('TSTEPS');
            RFtimemin = obj.GetParameter('TMIN');
            RFtimemax = obj.GetParameter('TMAX');
            deadTime = obj.GetParameter('TRANSIENTTIME');
            blkDelay = obj.GetParameter('BLANKINGDELAY');
            bw = obj.GetParameter('SPECTRALWIDTH');
            obj.SetParameter('NPOINTS',1);  
            acqTime = obj.GetParameter('NPOINTS')/bw;
            pulsetimelist=linspace(RFtimemin,RFtimemax,Tsteps);
            RFamp=linspace(RFmin,RFmax,B1steps);
            
            rawData.inputs.B1steps=B1steps;
            rawData.inputs.RFmin=RFmin;
            rawData.inputs.RFmax=RFmax;
            rawData.inputs.RFtimemin=RFtimemin;
            rawData.inputs.RFtimemax=RFtimemax;
            rawData.inputs.Tsteps=Tsteps;
            rawData.inputs.blkDelay=blkDelay;
            rawData.inputs.bw=bw;
            rawData.inputs.nPoints = obj.GetParameter('NPOINTS');   
            rawData.inputs.rfAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.deadTime = deadTime;
            rawData.inputs.TR = obj.GetParameter('REPETITIONTIME');
            rawData.inputs.acqTime = acqTime;
            rawData.inputs.Nscans =  obj.GetParameter('NSCANS');

            data=zeros(Tsteps,B1steps);
            plotindex=1;

            % Load amplitude calibration for 1V
            bwCal = load('rawData-BW-Calibration.mat');
            bwCalVal = bwCal.rawData.outputs.fidMean;
            bwCalVec = logspace(log10(bwCal.rawData.inputs.bwMin),...
                log10(bwCal.rawData.inputs.bwMax),...
                bwCal.rawData.inputs.steps);
            [~,bwCalNormPos] = min(abs(bwCalVec-bw));
            bwCalNormVal = bwCalVal(bwCalNormPos);
            
            figure
            
            for ii=1:B1steps
                obj.SetParameter('RFAMPLITUDE',RFamp(ii))
                obj.CreateSequence;
                obj.Sequence2String;
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    %                 obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Batch aborted!','Warning')
                    return;
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                fidList = abs(cDataBatch/bwCalNormVal);
                data(:,ii)=(fidList);
                
                % grab the fata
                xdata = 1e6*pulsetimelist';
                ydata = fidList;

                % fit the data to an spline
                f = fittype('smoothingspline');
                cfun = fit(xdata,ydata,f);
                xdatafit = linspace(min(xdata),max(xdata),1000);
                ydatafit = cfun(xdatafit);
                PulseTime = xdatafit( ydatafit == max(ydatafit) );
                
                PulseTimeList(1,ii)=PulseTime; %us
                B1list(1,ii)=1/(PulseTime*4*42.56)*1e6; %uT
                Rabifreq(1,ii)=B1list(1,ii)*42.56*1e-3; %kHz
                
                subplot(3,3,plotindex)
                hold on
                plot(xdata,ydata,'bo')
                plot(xdatafit,ydatafit,'r');
                plot(PulseTime,max(ydatafit),'r*','MarkerSize',12);
                title(sprintf('RFamp=%.4f... t90=%.1f us... B1= %.2f uT... f= %.3f kHz',RFamp(ii),PulseTime,B1list(1,ii), Rabifreq(1,ii)))
                grid('on');
                xlabel('Pulse time [us]');
                
                plotindex=plotindex+1;
                pause(0.4)
            end
            
            rawData.outputs.data=data;
            rawData.outputs.pulseTime=PulseTimeList; %us
            rawData.outputs.B1list=B1list; %uT
            rawData.outputs.Rabifreq=Rabifreq; %kHz
                
            figure
            subplot(1,3,1)
            plot(RFamp,PulseTimeList,'o-')
            grid('on');
            xlabel('RF amp (u.a.)');
            ylabel('Pulse time 90º (us)');
            title('90º RF pulse time VS RFamp')
            
            subplot(1,3,2)
            plot(RFamp,B1list,'o-')
            grid('on');
            xlabel('RF amp (u.a.)');
            ylabel('B1 (uT)');
            title('B1 strength VS RFamp')
            
            subplot(1,3,3)
            plot(RFamp,Rabifreq,'o-')
            grid('on');
            xlabel('RF amp (u.a.)');
            ylabel('Rabi freq (kHz)');
            title('Rabi frequency VS RFamp')

            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
    end
end