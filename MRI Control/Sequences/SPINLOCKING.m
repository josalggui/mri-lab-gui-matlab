classdef SPINLOCKING < MRI_BlankSequence   
    properties
        maxRepetitionsPerBatch;
        nPhasesBatch;
        lastBatch;
        RepetitionTime;
        AcquisitionTime;
        EndPhase;
        PhasesPerBatch = 20;
        gx;
    end
    methods (Access=private)
        function CreateParameters(obj)
            obj.CreateOneParameter('PULSELOCKINGTIME','Pulse locking time','us',0.5*MyUnits.ms);
            obj.CreateOneParameter('GRADIENTSLICE','Gslice strength','T/m',0.05);
            obj.CreateOneParameter('GRADIENTIMAGING','Gimaging strength','T/m',0);
            CreateOneParameter(obj,'RFAMPLITUDE2','RF amp spin locking','',0.02);
            obj.CreateOneParameter('REPETITIONTIME','TR','ms',50*MyUnits.ms);
            obj.CreateOneParameter('ACQTIME','Acq time (shorter T2)','us',2000*MyUnits.us);
            obj.CreateOneParameter('SEQUENCETYPE','SL, SLZTE, SLCHASE','',1);
%             obj.CreateOneParameter('FOVX','FOV x','mm',50*MyUnits.mm)
%             obj.CreateOneParameter('TAU','Tau time (CHASE)','us',100*MyUnits.us)
%             obj.CreateOneParameter('PULSETIME180','180� Pulse time (CHASE)','us',20*MyUnits.us)
            CreateOneParameter(obj,'PHASE1', 'Phase 1', '',0.0);
            CreateOneParameter(obj,'PHASE2', 'Phase 2', '',90.0);
            CreateOneParameter(obj,'PHASE3', 'Phase 3', '',180.0);
            CreateOneParameter(obj,'PHASE4', 'Phase 4', '',270.0);
            CreateOneParameter(obj,'REPETITIONDELAY','Repetition delay','ms',1);
            
            obj.InputParHidden = {'GRADIENTIMAGING','SHIMSLICE','SHIMPHASE','TRANSIENTTIME','SHIMREADOUT','SPECTRALWIDTH'...
               'COILRISETIME','SPOILERTIME','SPOILERAMP','NSLICES','BLANKINGDELAY','REPETITIONDELAY'};
        end  
    end

    methods
        function obj = SPINLOCKING(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'SPINLOCKING';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
        end  
         
        function CreateSequence(obj)
            
            seqType = obj.GetParameter('SEQUENCETYPE');
            if(seqType==1)  
                seqType = "SL";
            elseif(seqType==2)
                seqType = "SLZTE";
            elseif(seqType==3)
                seqType = "SLCHASE";
            else
                warndlg('Non valid sequence type input')
                return;
            end
            
            global scanner
            
            % If you want introduce B1 soft pulse in kHz instead of as adimensional, uncomment this block
%             adim90B1 = obj.GetParameter('RFAMPLITUDE');
%             tp = obj.GetParameter('PULSETIME');
%             B190 = (pi/2)/(2*pi*42.56e6*tp); %T
%             f90 = 42.56e6*B190*1e-3; %kHz
%             convertB1 = f90/adim90B1; %kHz/adim
%             obj.SetParameter('RFAMPLITUDE2',obj.GetParameter('RFAMPLITUDE2')*1e-3/convertB1);

            [shimS,shimP,shimR] = SelectAxes(obj);
            c = obj.ReorganizeCalibrationData();
            obj.nline_reads = 1;
            step=obj.nSteps;
            gz = obj.GetParameter('GRADIENTSLICE')/c(3);
            gSpoiler=obj.GetParameter('GRADIENTIMAGING')/c(1);
            tau=obj.GetParameter('TAU')*1e6;
            pulseTime180=obj.GetParameter('PULSETIME180')*1e6;
            acqTime = obj.GetParameter('ACQTIME')*1e6;
            nPoints = obj.GetParameter('NPOINTS');
            bw = (nPoints/acqTime)*1e3;
            obj.SetParameter('SPECTRALWIDTH',bw*1e3);
            pulseTime90 = obj.GetParameter('PULSETIME')*1e6;
            pulselock = obj.GetParameter('PULSELOCKINGTIME')*1e6;
            deadTime = 150;
            TR = obj.GetParameter('REPETITIONTIME')*1e6;
            blkDelay = scanner.BlankingDelay;
            crt = obj.GetParameter('COILRISETIME')*1e6;
            gradientDelay = scanner.grad_delay;

            phase0 = 0;
            phase90 = 1;
            phase180 = 2;
            phase270 = 3;
                        
            RR = obj.GetParameter('READOUT');
            PP = obj.GetParameter('PHASE');
            SS = obj.GetParameter('SLICE');
            
            if PP==RR || PP==SS || RR==SS
                warndlg('Nono valid axis inputs','Axis Error')
                return;
            end
            
            if SS=='x'
                dacslice=2; 
            end
            if SS=='y'
                dacslice=0; 
            end
            if SS=='z'
                dacslice=1;
            end

            if RR=='x'
                dacred=2;
            end
            if RR=='y'
                dacred=0; 
            end
            if RR=='z'
                dacred=1; 
            end

            if PP=='x'
                dacph=2; 
            end
            if PP=='y'
                dacph=0;
            end
            if PP=='z'
                dacph=1;  
            end
            
            PP=dacph;   SS=dacslice;   RR=dacred;
            
            iIns = 1;
            if(strcmp(seqType,"SL")) 
                obj.nline_reads = 2;
                [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs,obj.RFamps] = deal([]);
              
                for ii = 1:2         
                    %% Stablish shimming during all the sequence (3 ins)
                    iIns = obj.PulseGradient (iIns,shimR,RR,0.1);
                    iIns = obj.PulseGradient (iIns,shimP,PP,0.1);
                    iIns = obj.PulseGradient (iIns,shimS,SS,0.1);

                    %% Pulse the gradient z before the pulses (1 ins)
                    iIns = obj.RampGradient(iIns,shimS,shimS+(gz)*(-1)^ii,SS,crt,scanner.nSteps);
                    obj.DELAYs(iIns-1) = scanner.grad_delay-blkDelay;

                    %% Combination of hard 90X pulse followed without delay by a soft spin locking Y pulse and -X storaged pulse (3 ins)
                    iIns = SetBlanking(obj,iIns,1,blkDelay);
                    iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0); 
                    iIns = CreateRawPulse(obj,iIns,phase90,pulselock,1); 
%                     iIns = CreateRawPulse(obj,iIns,phase180,pulseTime90,0);
%                     iIns = SetBlanking(obj,iIns,0,17);
%                     iIns = SetBlanking(obj,iIns,1,8);
%                     iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0);   
                     
%                     iIns = CreateRawPulse(obj,iIns,phase90,pulselock,1); 
%                     iIns = CreateRawPulse(obj,iIns,phase180,pulseTime90,0);
%                     iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0);
%                     iIns = obj.RepetitionDelay(iIns,0,25); 
%                     iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0);
%                     
                    
%                     iIns = CreateRawPulse(obj,iIns,phase180,pulseTime90,0); %TO TEST IF WE CAN REVERT PULSES
%                     iIns = CreateRawPulse(obj,iIns,phase90,pulselock,1);  %PHASELOCKING
%                     iIns = CreateRawPulse(obj,iIns,phase270,pulselock/2,1);
 
%                     
%                     iIns = CreateRawPulse(obj,iIns,phase180,pulseTime90,0);  %STORAGING
%                     iIns = obj.RepetitionDelay(iIns,0,0.1);
%                     iIns = SetBlanking(obj,iIns,1,5);
%                     iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0); %FOR TEST
%                     
                    %% Switch off the slice gradient and switch on the imaging/spoiler gradient (2 ins)
%                     iIns = obj.PulseGradient(iIns,(shimS),SS,0.1);
%                     iIns = obj.PulseGradient(iIns,(shimR)+gSpoiler*(-1)^ii,PP,crt-blkDelay);

                    %% Hard 90X pulse to tip the storaged M (2 ins)
%                     iIns = SetBlanking(obj,iIns,1,blkDelay);
%                     iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0);
                     
                    %% Acquisition (2 ins)
                    iIns = obj.RepetitionDelay(iIns,0,deadTime);
                    iIns = obj.Acquisition(iIns,acqTime);
                 
                    %% Delay for recover T1 (1 ins)
                    iIns = obj.RepetitionDelay (iIns,1,TR-acqTime-deadTime-1*crt-pulselock-pulseTime90);
                    
                end
                obj.nInstructions = iIns-1;    
             end
               
%               if(strcmp(seqType,"SLCHASE"))
%                  obj.nline_reads = 2;
%                 [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs,obj.RFamps] = deal([]);
%                 
%                 for ii = 1:2         
%                     %% Stablish shimming during all the sequence (3 ins)
%                     iIns = obj.PulseGradient (iIns,shimR,RR,0.1);
%                     iIns = obj.PulseGradient (iIns,shimP,PP,0.1);
%                     iIns = obj.PulseGradient (iIns,shimS,SS,0.1);
% 
%                     %% Pulse the gradient z before the pulses (1 ins)
%                     iIns = obj.PulseGradient(iIns,shimS+gz,SS,crt-blkDelay);
% 
%                     %% Combination of hard 90X pulse followed without delay by a soft spin locking Y pulse and -X storaged pulse (3 ins)
%                     iIns = SetBlanking(obj,iIns,1,blkDelay);
%                     iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0);
%                     iIns = CreateRawPulse(obj,iIns,phase90,pulselock,1);  %PHASELOCKING
%                     
%                     %% Switch off the slice gradient and switch on the imaging/spoiler gradient (2 ins)
%                     iIns = obj.PulseGradient(iIns,(shimS),SS,0.1);
%                     iIns = obj.PulseGradient(iIns,(shimR)+gSpoiler*(-1)^ii,PP,tau);
% 
%                     %% CHASE-5 block
%                     iIns = SetBlanking(obj,iIns,1,blkDelay);
%                     iIns = CreateRawPulse(obj,iIns,phase180,pulseTime90,0);  %90-X
%                     iIns = obj.RepetitionDelay(iIns,0,tau);
%                     
%                     iIns = SetBlanking(obj,iIns,1,blkDelay);
%                     iIns = CreateRawPulse(obj,iIns,phase90,pulseTime90,0);  %90Y
%                     iIns = obj.RepetitionDelay(iIns,0,tau);
%                     
%                     iIns = SetBlanking(obj,iIns,1,blkDelay);
%                     iIns = CreateRawPulse(obj,iIns,phase0,pulseTime180,0);  %180X
%                     iIns = obj.RepetitionDelay(iIns,0,tau);
%                     
%                     iIns = SetBlanking(obj,iIns,1,blkDelay);
%                     iIns = CreateRawPulse(obj,iIns,phase90,pulseTime90,0);  %90Y
%                     iIns = obj.RepetitionDelay(iIns,0,tau);
%                      
%                     iIns = SetBlanking(obj,iIns,1,blkDelay);
%                     iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0);  %90X
%                     iIns = obj.RepetitionDelay(iIns,0,tau);
%                     
%                     %% Phase corrections
%                     iIns = obj.PulseGradient(iIns,(shimS)+gz,SS,0.1);
%                     iIns = obj.PulseGradient(iIns,(shimR),PP,tau/2);
%                     
%                     iIns = obj.PulseGradient(iIns,(shimS),SS,0.1);
%                     iIns = obj.PulseGradient(iIns,(shimR)+gSpoiler,PP,tau/2);
%                     
%                     %% Acquisition (2 ins)
%                     iIns = obj.Acquisition(iIns,acqTime);
%                  
%                     %% Delay for recover T1 (1 ins)
%                     iIns = obj.RepetitionDelay (iIns,1,TR);
%                     
%                 end
%                 obj.nInstructions = iIns-1;
%               end
              
        end
        
        
        function [status,result] = Run(obj,~)
            clc
            global rawData;
            rawData = [];   
            [~,~,~,~,~,spoilerEnable,~] = obj.ConfigureGradients(0);
            tic

            % Hard pulse is define from adim RF amplitude and 90� pulse time, so assuming linear correlation we calculate the Khz to adim conversion factor
            % (necessary inside Run function to write in rawData the RF amplitude as they should be
%             adim90B1 = obj.GetParameter('RFAMPLITUDE');
%             tp = obj.GetParameter('PULSETIME');
%             B190 = (pi/2)/(2*pi*42.56e6*tp); %T
%             f90 = 42.56e6*B190*1e-3; %kHz
%             convertB1 = f90/adim90B1; %kHz/adim
               
            if(spoilerEnable)
                tic
                % Get the central frequency
                fprintf('Calibrating frequency... \n')
                f0 = obj.GetFIDParameters;
                obj.SetParameter('FREQUENCY',f0);
                fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
            end
            
            seqType = obj.GetParameter('SEQUENCETYPE');
                
            if(seqType==1)
                nPoints = obj.GetParameter('NPOINTS');gimaging=obj.GetParameter('GRADIENTIMAGING');gslice=obj.GetParameter('GRADIENTSLICE');pulseTime90 = obj.GetParameter('PULSETIME');pulselocking = obj.GetParameter('PULSELOCKINGTIME');deadTime = obj.GetParameter('TRANSIENTTIME');blkDelay = obj.GetParameter('BLANKINGDELAY');acqTime = obj.GetParameter('ACQTIME');bw = nPoints/acqTime;
                obj.SetParameter('SPECTRALWIDTH',bw*1e-3);
                rawData.inputs.SeqType = seqType; rawData.inputs.gslice=gslice; rawData.inputs.gimaging=gimaging;rawData.inputs.blk=blkDelay;rawData.inputs.NEX = obj.GetParameter('NSCANS');rawData.inputs.nPoints = nPoints; rawData.inputs.rfAmplitude90 = obj.GetParameter('RFAMPLITUDE');rawData.inputs.rfAmplitudeLockingPulse = obj.GetParameter('RFAMPLITUDE2'); rawData.inputs.pulseTime90 = pulseTime90; rawData.inputs.pulselockingTime = pulselocking;rawData.inputs.deadTime = deadTime; rawData.inputs.TR = obj.GetParameter('REPETITIONTIME');rawData.inputs.acqTime = acqTime;
                obj.nInstructions=21;
                obj.maxRepetitionsPerBatch = floor(2048/obj.nInstructions);
                
                % Get k-points
                tp = obj.GetParameter('PULSETIME');
                td = obj.GetParameter('TRANSIENTTIME');
                ts = 1/bw;
                acqTime = obj.GetParameter('ACQTIME');
                nPoints = obj.GetParameter('NPOINTS');
                tv = linspace(tp/2+td+ts/2,tp/2+td+ts/2+acqTime,nPoints);
                kv1 = 42.6d6*obj.GetParameter('GRADIENTSLICE')*tv;
                kv2 = -kv1(end:-1:1);
                kv = [kv2,kv1];
                tv = [-tv(end:-1:1),tv];
                obj.SetParameter('REPETITIONDELAY',obj.GetParameter('REPETITIONTIME'));
                if(obj.GetParameter('GRADIENTSLICE')==0)
                    FOV = 1/ts;
                else
                    FOV = abs(1/(kv(2)-kv(1)));  %FOV=1/deltaK
                end
                rawData.inputs.FOV=FOV;

                obj.CreateSequence;
                obj.Sequence2String;
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                [status,result] = system(fullfile(obj.path_file,fname));
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz'));
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms);
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Batch aborted!','Warning')
                    return;
                end
                
                bwCalNormVal = obj.calculate_bwCalNormVal;
                
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch=complex(DataBatch(:,1),DataBatch(:,2));
                
                % Reorganize signal
                signal = cDataBatch./bwCalNormVal;
                signal(1:nPoints) = signal(nPoints:-1:1);
                
%                 rawData.outputs.signalMap(nii,njj) = abs(signal(end/2));
                rawData.outputs.signal = [tv',signal];
                rawData.outputs.profile(:,2) = abs(ifftshift(ifftn(signal)));
                rawData.outputs.profile(:,1) = linspace(-FOV/2,FOV/2,2*nPoints);
            end
            
            
            
%             if(seqType==3) 
%                 % Get k-points
%                 tp = obj.GetParameter('PULSETIME');
%                 td = obj.GetParameter('TRANSIENTTIME');
%                 ts = 1/obj.GetParameter('SPECTRALWIDTH');
%                 acqTime = obj.GetParameter('ACQTIME');
%                 nPoints = obj.GetParameter('NPOINTS');
%                 tv = linspace(tp/2+td+ts/2,tp/2+td+ts/2+acqTime,nPoints);
%                 kv1 = 42.6d6*obj.GetParameter('GRADIENTSLICE')*tv;
%                 kv2 = -kv1(end:-1:1);
%                 kv = [kv2,kv1];
%                 tv = [-tv(end:-1:1),tv];
%                 if(obj.GetParameter('GRADIENTSLICE')==0)
%                     FOV = 1/ts;
%                 else
%                     FOV = abs(1/(kv(2)-kv(1)));  %FOV=1/deltaK
%                 end
% 
%                 nPoints = obj.GetParameter('NPOINTS');gimaging=obj.GetParameter('GRADIENTIMAGING');gslice=obj.GetParameter('GRADIENTSLICE');pulseTime90 = obj.GetParameter('PULSETIME');pulselocking = obj.GetParameter('PULSELOCKINGTIME');deadTime = obj.GetParameter('TRANSIENTTIME');blkDelay = obj.GetParameter('BLANKINGDELAY');acqTime = obj.GetParameter('ACQTIME');bw = nPoints/acqTime;
%                 obj.SetParameter('SPECTRALWIDTH',bw*1e-3);
%                 rawData.inputs.SeqType = seqType; rawData.inputs.gslice=gslice; rawData.inputs.gimaging=gimaging;rawData.inputs.blk=blkDelay;rawData.inputs.NEX = obj.GetParameter('NSCANS');rawData.inputs.nPoints = nPoints; rawData.inputs.rfAmplitude90 = obj.GetParameter('RFAMPLITUDE');rawData.inputs.rfAmplitudeLockingPulse = obj.GetParameter('RFAMPLITUDE2'); rawData.inputs.pulseTime90 = pulseTime90; rawData.inputs.pulselockingTime = pulselocking;rawData.inputs.deadTime = deadTime; rawData.inputs.TR = obj.GetParameter('REPETITIONTIME');rawData.inputs.acqTime = acqTime;
%                 obj.nInstructions=21;
%                 obj.maxRepetitionsPerBatch = floor(2048/obj.nInstructions);
%                 
%                 if(spoilerEnable)
%                 % Get the central frequency
%                     fprintf('Calibrating frequency... \n')
%                     f0 = obj.GetFIDParameters;
%                     obj.SetParameter('FREQUENCY',f0);
%                     fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
%                 end
%                 
%                 obj.CreateSequence;
%                 obj.Sequence2String;
%                 fname = 'TempBatch.bat';
%                 obj.WriteBatchFile(fname);
%                 [status,result] = system(fullfile(obj.path_file,fname));  
%                 if status == 0 && ~isempty(result)
%                     C = textscan(result, '%s', 'delimiter', sprintf('\f'));
%                     res = C{:};
%                     obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
%                     obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); 
%                     obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms);
%                     obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
%                     obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
%                 else
%                     warndlg('Batch aborted!','Warning')
%                     return;
%                 end
%                 
%                 bwCalNormVal = obj.calculate_bwCalNormVal;
%                 
%                 DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
%                 cDataBatch=complex(DataBatch(:,1),DataBatch(:,2));
%                 
%                 % Reorganize signal
%                 signal = cDataBatch./bwCalNormVal;
%                 signal(1:nPoints) = signal(nPoints:-1:1);
%                 
% 
%                 rawData.outputs.signal = [tv',signal];
%                 rawData.outputs.profile(:,2) = abs(ifftshift(ifftn(signal)));
%                 rawData.outputs.profile(:,1) = linspace(-FOV/2,FOV/2,2*nPoints);  
%             end
            
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
            
        end
        end
%         save(fullfile(obj.path_file,name),'rawData');
%     end
%     end
    
end