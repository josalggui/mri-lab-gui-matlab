classdef PREPOLTEST < MRI_BlankSequence   
    properties
        maxRepetitionsPerBatch;
        nPhasesBatch;
        lastBatch;
        RepetitionTime;
        AcquisitionTime;
        EndPhase;
        PhasesPerBatch = 20;
    end
    methods (Access=private)
        function CreateParameters(obj)
            obj.CreateOneParameter('TRANSIENTTIME','Dead Time','us',100*MyUnits.us);
            obj.CreateOneParameter('REPETITIONTIME','TR','ms',50*MyUnits.ms);
            obj.CreateOneParameter('ACQUISITIONTIME','Acquisition time','ms',2*MyUnits.ms);
            obj.CreateOneParameter('PREPOLTIME','Prepol duration','ms',500*MyUnits.ms);
            obj.CreateOneParameter('DELAYPREPOL','Delay prepol-RF','ms',30*MyUnits.ms);
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT'...
                'READOUT','PHASE','SLICE','COILRISETIME','SPOILERTIME',...
                'SPOILERAMP','NSLICES','BLANKINGDELAY','NPOINTS'};
        end  
    end
    
    methods
        function obj = PREPOLTEST(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'PREPOLTEST';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
        end
        
        function UpdateTETR(obj)
            obj.TTotal = obj.GetParameter('REPETITIONTIME') * obj.GetParameter('NUMBERFIDS');
        end
         
        function CreateSequence( obj)
            [shimS,shimP,shimR] = SelectAxes(obj);     
            acqTime = obj.GetParameter('ACQUISITIONTIME')*1e6;
            bw=obj.GetParameter('SPECTRALWIDTH');
            
            pulseTime90 = obj.GetParameter('PULSETIME')*1e6;
            deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
            TR = obj.GetParameter('REPETITIONTIME')*1e6;
            blkDelay = obj.GetParameter('BLANKINGDELAY')*1e6;
            prepolTime = obj.GetParameter('PREPOLTIME')*1e6;
            delayprepolRF = obj.GetParameter('DELAYPREPOL')*1e6;
            obj.SetParameter('NPOINTS',bw*acqTime*1E-6);
            
            obj.nline_reads = 2;   
            rfphase=0;
            
             obj.nInstructions = (8+9);
            [obj.RFamps,obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            iIns = 1;
            

                %% Shimming all gradients
                iIns = obj.PulseGradient (iIns,shimR,2,0.1);
                iIns = obj.PulseGradient (iIns,shimP,0,0.1);
                iIns = obj.PulseGradient (iIns,shimS,1,0.1);
                
                %% 90� pulse
                iIns = obj.CreatePulse(iIns,rfphase,blkDelay,pulseTime90,deadTime);
                
                %% Acquisition
                iIns = obj.Acquisition(iIns,acqTime);
                
                %% RepetitionDelay
                iIns = obj.RepetitionDelay (iIns,1,TR-acqTime-deadTime); 
                           
                %% Shimming all gradients
                iIns = obj.PulseGradient (iIns,shimR,2,0.1);
                iIns = obj.PulseGradient (iIns,shimP,0,0.1);
                iIns = obj.PulseGradient (iIns,shimS,1,0.1);
                
                %% Set prepol
                iIns = obj.SetBlanking(iIns,2,prepolTime);
                iIns = obj.RepetitionDelay(iIns,0,delayprepolRF);
                
                %% 90� pulse
                iIns = obj.CreatePulse(iIns,rfphase,blkDelay,pulseTime90,deadTime);
                obj.PhResets(iIns-1) = 0; %%ATENTION HERE
                
                %% Acquisition
                iIns = obj.Acquisition(iIns,acqTime);
                
                %% RepetitionDelay
                iIns = obj.RepetitionDelay (iIns,1,TR-acqTime-deadTime-delayprepolRF); 

            obj.nInstructions = iIns-1;
        end
        
        function [status,result] = Run(obj,iAcq)
            clc
            global rawData;
            rawData = [];
            
            % Input data
            pulseTime90 = obj.GetParameter('PULSETIME');
            deadTime = obj.GetParameter('TRANSIENTTIME');
            blkDelay = obj.GetParameter('BLANKINGDELAY');
            acqTime = obj.GetParameter('ACQUISITIONTIME');
            bw = obj.GetParameter('SPECTRALWIDTH');
            nPoints = bw*acqTime;
            obj.SetParameter('NPOINTS',nPoints);   
            if mod(nPoints,2)~=0
                warndlg('Number of points must be integer. Change SpectralWidth or AcqTime','RF Error')
                return;
            end  

            rawData.inputs.nPoints = obj.GetParameter('NPOINTS');   
            rawData.inputs.rfAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.pulseTime90 = pulseTime90;
            rawData.inputs.deadTime = deadTime;
            rawData.inputs.TR = obj.GetParameter('REPETITIONTIME');
            rawData.inputs.acqTime = acqTime;
            rawData.inputs.Nscans =  obj.GetParameter('NSCANS');
            rawData.inputs.delayprepolRF = obj.GetParameter('DELAYPREPOL');

            % Run sequence
            obj.CreateSequence;
            obj.Sequence2String;
            fname = 'TempBatch.bat';
            obj.WriteBatchFile(fname);
            [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
            if status == 0 && ~isempty(result)
                C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                res = C{:};
                obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
%                 obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
            else
                warndlg('Batch aborted!','Warning')
                return;
            end
            DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
            cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
            bwCalNormVal = obj.calculate_bwCalNormVal;
            fidList = cDataBatch./bwCalNormVal;            
           
            time=linspace(0,acqTime,nPoints)';
            time2fids=linspace(0,2*acqTime,2*nPoints)';
            signals=reshape(fidList,[nPoints,2]);
            
            rawData.outputs.spectrumFFTraw(:,1)= linspace(-bw/2,bw/2,nPoints)*1e-3;
            rawData.outputs.spectrumFFTprepol(:,1)= linspace(-bw/2,bw/2,nPoints)*1e-3;
            rawData.outputs.spectrumFFTraw(:,2) = abs(ifftshift(ifftn(signals(:,1))));
            rawData.outputs.spectrumFFTprepol(:,2) = abs(ifftshift(ifftn(signals(:,2))));
            
            rawData.outputs.fidraw = signals(:,1);
            rawData.outputs.fidprepol = signals(:,2);
            rawData.outputs.fidList =fidList;
            rawData.outputs.time = time;
            rawData.outputs.time2fids = time2fids;

            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
    end
end