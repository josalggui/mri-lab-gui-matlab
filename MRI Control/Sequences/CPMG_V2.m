classdef CPMG_V2 < MRI_BlankSequence   
    properties
        maxRepetitionsPerBatch;
        nPhasesBatch;
        lastBatch;
        RepetitionTime;
        AcquisitionTime;
        EndPhase;
        PhasesPerBatch = 20;
    end
    methods (Access=private)
        function CreateParameters(obj)
            obj.CreateOneParameter('SEQUENCETYPE','CP, APCP, CPMG, APCPMG','',1);
            obj.CreateOneParameter('TRANSIENTTIME','Dead Time','us',100*MyUnits.us);
            obj.CreateOneParameter('PULSETIME90','90� Pulse Time','us',10*MyUnits.us);
            obj.CreateOneParameter('PULSETIME180','180� PulseTime','us',20*MyUnits.us);
            obj.CreateOneParameter('ECHOTIME','TE','ms',1*MyUnits.ms);
            obj.CreateOneParameter('REPETITIONTIME','TR','ms',50*MyUnits.ms);
            obj.CreateOneParameter('NECHOES','Number of Echoes','',1);
            obj.CreateOneParameter('ACQUISITIONTIME','Acquisition time','us',200*MyUnits.us);
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT','SPECTRALWIDTH'...
                'READOUT','PHASE','SLICE','COILRISETIME','SPOILERTIME',...
                'SPOILERAMP','NSLICES','BLANKINGDELAY',...
                'PULSETIME'};
            obj.MoveParameter(19,4);
        end  
    end
    
    

    methods
        function obj = CPMG_V2(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'CPMG_V2';
            obj.ProgramName = 'MRI_BlankSequenceJM.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
        end
        
         
        function CreateSequence( obj)
            
            [shimS,shimP,shimR] = SelectAxes(obj);
            
            seqType = obj.GetParameter('SEQUENCETYPE');
            if(seqType==1)  
                seqType = "CP";
            elseif(seqType==2)
                seqType = "APCP";
            elseif(seqType==3)
                seqType = "CPMG";
            elseif(seqType==4)
                seqType = "APCPMG";
            else
                warndlg('Non valid sequence type input')
                return;
            end
            
            obj.nline_reads = obj.GetParameter('NECHOES')+1;
            obj.nInstructions = 4*obj.GetParameter('NECHOES')+4+1+3;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            acqTime = obj.GetParameter('ACQUISITIONTIME')*1e6;
            nPoints = obj.GetParameter('NPOINTS');
            bw = (nPoints/acqTime)*1e3;
            obj.SetParameter('SPECTRALWIDTH',bw*1e3);
            pulseTime90 = obj.GetParameter('PULSETIME90')*1e6;
            pulseTime180 = obj.GetParameter('PULSETIME180')*1e6;
            deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
            TR = obj.GetParameter('REPETITIONTIME')*1e6;
            TE = obj.GetParameter('ECHOTIME')*1e6;
            blkDelay = obj.GetParameter('BLANKINGDELAY')*1e6;
            repDelay = TR-0.5*pulseTime90-(2*obj.GetParameter('NECHOES')+1)/2*TE-blkDelay;
            
            iIns = 1;
            tx = TE/2-0.5*acqTime-0.5*pulseTime180;
            ty = TE/2-blkDelay-0.5*pulseTime180-0.5*acqTime;
            tz = TE/2-acqTime-blkDelay-deadTime-0.5*pulseTime90-0.5*pulseTime180;
            
            iIns = obj.PulseGradient (iIns,shimR,2,0.1);
            iIns = obj.PulseGradient (iIns,shimP,0,0.1);
            iIns = obj.PulseGradient (iIns,shimS,1,0.1);
            
            % 90� pulse
            iIns = obj.CreatePulse(iIns,0,blkDelay,pulseTime90,deadTime);
            obj.PHASEs(iIns-2) = 0;
            iIns = obj.Acquisition(iIns,acqTime+tz);
            
            
            % 180� pulse train
            for ii = 1:obj.GetParameter('NECHOES')
                % 180� pulse
                iIns = obj.CreatePulse(iIns,0,blkDelay,pulseTime180,tx);
                obj.PhResets(iIns-3) = 0; %This line is needed to follow the phase origin of first 90� pulse and not restart it in each refocusing pulse
                if(strcmp(seqType,"CP"))
                    obj.PHASEs(iIns-2) = 0;
                elseif(strcmp(seqType,"APCP") && mod(ii,2)==0)
                    obj.PHASEs(iIns-2) = 0;
                elseif(strcmp(seqType,"APCP") && mod(ii,2)==1)
                    obj.PHASEs(iIns-2) = 2;
                elseif(strcmp(seqType,"CPMG"))
                    obj.PHASEs(iIns-2) = 1;
                elseif(strcmp(seqType,"APCPMG") && mod(ii,2)==0)
                    obj.PHASEs(iIns-2) = 1;
                elseif(strcmp(seqType,"APCPMG") && mod(ii,2)==1)
                    obj.PHASEs(iIns-2) = 3;
                end
                % Acquisition
                iIns = obj.Acquisition(iIns,acqTime+ty);
                
            end 
            obj.DELAYs(iIns-1) = acqTime+ty+blkDelay+0.5*pulseTime180+repDelay;
            obj.RepetitionDelay (iIns,1,0.1);
        end
        
        function [status,result] = Run(obj,iAcq)
            clc
            global rawData;
            rawData = [];
            
            % Input data
            seqType = obj.GetParameter('SEQUENCETYPE');
            nPoints = obj.GetParameter('NPOINTS');
            nEchoes = obj.GetParameter('NECHOES');
            TE = obj.GetParameter('ECHOTIME');
            pulseTime90 = obj.GetParameter('PULSETIME90');
            pulseTime180 = obj.GetParameter('PULSETIME180');
            deadTime = obj.GetParameter('TRANSIENTTIME');
            blkDelay = obj.GetParameter('BLANKINGDELAY');
            acqTime = obj.GetParameter('ACQUISITIONTIME');
            bw = obj.GetParameter('SPECTRALWIDTH');
            obj.SetParameter('SPECTRALWIDTH',bw);
            rawData.inputs.sequence = obj.SequenceName;
            if(seqType==1)
                rawData.inputs.sequenceType = 'CP';
            elseif(seqType==2)
                rawData.inputs.sequenceType = 'APCP';
            elseif(seqType==3)
                rawData.inputs.sequenceType = 'CPMG';
            elseif(seqType==4)
                rawData.inputs.sequenceType = 'APCPMG';
            end
            rawData.inputs.NEX = obj.GetParameter('NSCANS');
%             
%             if nEchoes==0
%                 obj.SetParameter('NPOINTS',900);
%                 nPoints = obj.GetParameter('NPOINTS');
%             end
%             
            rawData.inputs.nEchoes = nEchoes;
            rawData.inputs.nPoints = nPoints;      
            rawData.inputs.rfAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.pulseTime90 = pulseTime90;
            rawData.inputs.pulseTime180 = pulseTime180;
            rawData.inputs.deadTime = deadTime;
            rawData.inputs.TE = TE;
            rawData.inputs.TR = obj.GetParameter('REPETITIONTIME');
            rawData.inputs.acqTime = acqTime;
            
            if(acqTime>TE/2-0.5*pulseTime90-0.5*pulseTime180-deadTime-blkDelay && nEchoes>0)
                warndlg('Acquisition time too long','Acquisition Time Error')
                return;
            end

            % Load amplitude calibration for 1V
%             bwCal = load('rawData-BW-Calibration.mat');
%             bwCalVal = bwCal.rawData.outputs.fidMean;
%             bwCalVec = logspace(log10(bwCal.rawData.inputs.bwMin),...
%                 log10(bwCal.rawData.inputs.bwMax),...
%                 bwCal.rawData.inputs.steps);
%             [~,bwCalNormPos] = min(abs(bwCalVec-bw));
%             bwCalNormVal = bwCalVal(bwCalNormPos);
            
            % Send sequence
            obj.nInstructions = 4*obj.GetParameter('NECHOES')+4;
            obj.maxRepetitionsPerBatch = floor(2048/obj.nInstructions);                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
            
            % Run sequence
            obj.CreateSequence;
            obj.Sequence2String;
            fname = 'TempBatch.bat';
            obj.WriteBatchFile(fname);
            [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
            if status == 0 && ~isempty(result)
                C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                res = C{:};
                obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
%                 obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
            else
                warndlg('Batch aborted!','Warning')
                return;
            end
            DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
            cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
            fidList = cDataBatch;
%           fidList = cDataBatch/bwCalNormVal;
            
            % Get time vector
            timeStep = 1/bw;
            tx = TE/2-0.5*acqTime-0.5*pulseTime180;
            ti = pulseTime90/2+deadTime+timeStep/2;
            tf = ti+acqTime-timeStep;
            tA = linspace(ti,tf,nPoints)';
            
            ti = TE/2+pulseTime180/2+tx+timeStep/2;
            tf = ti+acqTime-timeStep;
            tB = linspace(ti,tf,nPoints)';
            if nEchoes==0
                t = tA;
            else
                t = cat(1,tA,tB);
            end
            if nEchoes>1
                for ii = 2:nEchoes
                    t = cat(1,t,tB+(ii-1)*TE);
                end
            end
            
            rawData.outputs.fidList = [t,fidList];

            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
    end
    
end