classdef RARE3D < MRI_BlankSequence
    % 05/11/2020
    % Jos� Miguel Algar�n Guisado
    % Ph.D.
    % MRILab
    % Institute for Instrumentation in Molecular Imaging (I3M)
    % CSIC- UPV
    % josalggui@i3m.upv.es

    properties
        StartSlice;
        EndSlice;
        PhasesPerBatch;
        PhasesinThisBatch;
        StartPhase;
        EndPhase;
        slice_ampl;
        slice_ampl_re;
        acq_time;
        PhaseTime;
        RepDelay;
        RO_strength_dep;
        RO_strength_rep;
        NRO;
        NPH;
        NSL;
        Axis1;
        Axis2;
        Axis3;
        CAxis1;
        CAxis2;
        CAxis3;
        rdspeed_mult = 1;
        nPhasesBatch;
        NP_calib = 1;
        ROmax = 0;
        CalData;
        FOV;
        gam = 42.57E6; %gyromagnetic ratio, 42.57 MHz/T
        gradientList;
        sliceRephasingTime;
        sincZeroCrossings = 1;        % Number of zero crossing for the RF sinc pulse
        maxBatchTime = 600;             % Maximun batch time in seconds
        lastBatch;
        repetition;
        pointsPerTR = 0;
        instPerTR = 0;
        roDephTime;
        phaseTime;
        phaseDelay;
    end
    methods (Access=private)
        function CreateParameters(obj)
            
            % Input parameters
            obj.CreateOneParameter('TE','TE time','ms',10*MyUnits.ms);
            obj.CreateOneParameter('TR','TR time','ms',50*MyUnits.ms);
            obj.CreateOneParameter('FOVRO','FOV Readout','mm',10*MyUnits.mm);
            obj.CreateOneParameter('FOVPH','FOV Phase','mm',10*MyUnits.mm);
            obj.CreateOneParameter('FOVSL','FOV Slice','mm',10*MyUnits.mm);
            obj.CreateOneParameter('NPOINTS','N Readout','',16);
            obj.CreateOneParameter('NPHASES','N Phase','',16);
            obj.CreateOneParameter('NSLICES','N Slice','',16);
            obj.CreateOneParameter('ROFACTOR','Readout Factor','',1);
            obj.CreateOneParameter('ACQTIME','Acquisition Time','ms',2*MyUnits.ms);
            obj.CreateOneParameter('REPETITIONDELAY','Repetition Delay','ms',50*MyUnits.ms);
            obj.CreateOneParameter('DELTARO','RO Deviation','mm',0);
            obj.CreateOneParameter('DELTAPH','PH Deviation','mm',0);
            obj.CreateOneParameter('DELTASL','SL Deviation','mm',0);
            obj.CreateOneParameter('PULSETIME','RF Pulse Time','us',30*MyUnits.us);
            obj.CreateOneParameter('ETL','Echo Train Length','',1);
            obj.CreateOneParameter('RFAMPLITUDE2','180� RF Amplitude','',0.2);
            obj.CreateOneParameter('RFAMPLITUDE','90� RF Amplitude','',0.1);
            obj.CreateOneParameter('PARTIALACQ', 'Acq Lines', '',0);
            obj.CreateOneParameter('SWEEPMODE','Sweep Mode','k2k/02k',0);
            obj.CreateOneParameter('DUMMY','Dummy Pulses','',0);
            obj.CreateOneParameter('PHFACTOR','Phase Factor','',1);
            obj.CreateOneParameter('SLFACTOR','Slice Factor','',1);
            obj.CreateOneParameter('PHASETIME','Phase Time','us',0*MyUnits.us);
            obj.CreateOneParameter('PREPOLTIME','Prepol duration','ms',0*MyUnits.ms);
            obj.CreateOneParameter('DELAYPREPOL','Delay prepol-RF','ms',0*MyUnits.ms);
            obj.CreateOneParameter('PREPOLCORRECTION','Prepol calibration','ms',0*MyUnits.ms);
            
            % Output parameters
            OutputParameters = {...
                'MAXPH','RO_STRENGTH','MAXSL'...
                };
            obj.OutputParNames = [containers.Map(obj.OutputParNames.keys(),obj.OutputParNames.values());containers.Map(OutputParameters,...
                {...
                'Start Phase', 'Readout amplitude', 'Start Slice'...
                })];
            obj.OutputParValues = [containers.Map(obj.OutputParValues.keys(),obj.OutputParValues.values());containers.Map(OutputParameters,...
                {...
                NaN,NaN,NaN...
                })];
            obj.OutputParUnits = [containers.Map(obj.OutputParUnits.keys(),obj.OutputParUnits.values());containers.Map(OutputParameters,...
                {...
                '','',''...
                })];
            for k=1:length(obj.OutputParameters)
                OutputParameters(strcmp(obj.OutputParameters{k},OutputParameters)) = [];
            end
            obj.OutputParameters = [obj.OutputParameters,OutputParameters];
            
            % Hidden parameters
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
                'BLANKINGDELAY','COILRISETIME',...
                'TRANSIENTTIME','SPOILERAMP',...
                'SPOILERTIME','SPECTRALWIDTH','REPETITIONDELAY'};

            % Rearange parameters
            obj.MoveParameter('FREQUENCY','NPOINTS');
            obj.MoveParameter('RFAMPLITUDE','NPOINTS');
            obj.MoveParameter('RFAMPLITUDE2','NPOINTS');
            obj.MoveParameter('PULSETIME','NPOINTS');
            obj.MoveParameter('ACQTIME','NPOINTS');
            obj.MoveParameter('TE','NPOINTS');
            obj.MoveParameter('TR','NPOINTS');
            obj.MoveParameter('ROFACTOR','NPOINTS');
            obj.MoveParameter('READOUT','NPOINTS');
            obj.MoveParameter('PHASE','NPOINTS');
            obj.MoveParameter('SLICE','NPOINTS');
            obj.MoveParameter('NPHASES','NSLICES');
            obj.MoveParameter('PHFACTOR','READOUT');
            obj.MoveParameter('SLFACTOR','READOUT');
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = RARE3D(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'RARE3D';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
            
            obj.SetParameter('RFAMPLITUDE',0.1);
            obj.SetParameter('PULSETIME',30*MyUnits.us);
            obj.SetParameter('NPOINTS',16);
            obj.SetParameter('NPHASES',16);
            obj.SetParameter('NSLICES',16);
            
            % This value is empherical and picked based on not creating eddy currents
            obj.SetParameter('FREQUENCY',42.58*MyUnits.MHz);
            
            % Grab the calibration data if there
            try
                obj.CalData = GradientCalibrationData();
            catch
                obj.CalData.X_TD = 1;
                obj.CalData.Y_TD = 1;
                obj.CalData.Z_TD = 1;
                obj.CalData.X_TV = 1;
                obj.CalData.Y_TV = 1;
                obj.CalData.Z_TV = 1;
            end
            
        end
        function UpdateTETR(obj)
            ETL = obj.GetParameter('ETL');
            parAcq = obj.GetParameter('PARTIALACQ');
            obj.TE = obj.GetParameter('TE');
            obj.TR = obj.GetParameter('TR');
            [~,~,~,ENSL,ENPH,~,~] = obj.ConfigureGradients(0);
            obj.NPH = obj.GetParameter('NPHASES')*ENPH+1*~ENPH;
            obj.NSL = obj.GetParameter('NSLICES')*ENSL+1*~ENSL;
            
            if(parAcq==0)
                nRep = obj.NPH/ETL;
            else
                nRep = obj.NPH/2/ETL+ceil(parAcq/ETL);
            end
            obj.TTotal = obj.TR*obj.GetParameter('NSCANS')*obj.NSL*nRep;
        end
        
        
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************



        function CreateSequence( obj, mode )
            % mode =  "0", average T2 maks acquisition,
            %         "1", normal execution,
            
            global scanner
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            
            % Configure gradients and axes
            [~,~,ENRO,~,~,~,~] = obj.ConfigureGradients(0);
            
            % Calculate the number of line reads
            obj.nline_reads = 0;
            nPoints = obj.NRO;
            dummy = obj.GetParameter('DUMMY');
            
            % Initialize all vectors to zero with dynamic size
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs,obj.RFamps] = deal([]);
            
            % Load timing from RawData;
            crt             = obj.GetParameter('COILRISETIME')*1e6;
            blk             = scanner.BlankingDelay;
            pulseTime       = obj.GetParameter('PULSETIME')*1e6;
            roPhaseTime     = obj.roDephTime*1e6;
            phTime          = obj.phaseTime*1e6;
            phDelay         = obj.phaseDelay*1e6;
            gradDelay       = scanner.grad_delay;
            acqTime         = obj.GetParameter('ACQTIME')*1e6;
            repDelay        = obj.RepDelay*1e6;
            prepolTime      = obj.GetParameter('PREPOLTIME')*1e6;
            delayprepolRF   = obj.GetParameter('DELAYPREPOL')*1e6;
            prepolCorrectionTime = obj.GetParameter('PREPOLCORRECTION')*1e6;
            prepolTime      = prepolTime+prepolCorrectionTime;
            
            % Gradient factors:
            roFactor = obj.GetParameter('ROFACTOR');
            phFactor = obj.GetParameter('PHFACTOR');
            slFactor = obj.GetParameter('SLFACTOR');
            
            % Shimming
            shimG = [shimR,shimP,shimS];
            dac = [obj.selectReadout,obj.selectPhase,obj.selectSlice];
            
            % Readout gradient amplitudes
            dephasingReadout = shimG(1)+obj.RO_strength_dep*ENRO*roFactor;
            rephasingReadout = shimG(1)+obj.RO_strength_rep*ENRO;
            
            % BEGIN THE PULSE PROGRAMMING
            iIns            = 1;
            exPhase         = 0;
            rePhase         = 1;
            exAmpli         = 0;
            reAmpli         = 1;
            ready           = 0;
            ETL             = obj.GetParameter('ETL');
            dummyRep        = 1;
            
            while(ready==0)
                if(dummyRep>dummy || mode==0)
                    % Activate shimming
                    iIns = obj.RampGradient(iIns,[0 0 0],shimG,dac,0.3,1);
                    
                     % Prepol activation
                        if obj.GetParameter('PREPOLTIME')>0
                            iIns = obj.SetBlanking(iIns,2,prepolTime);
                            iIns = obj.RepetitionDelay(iIns,0,delayprepolRF);
                        end
                        
                    % 90� RF pulse
                    iIns = obj.SetBlanking(iIns,1,blk);
                    iIns = obj.CreateRawPulse(iIns,exPhase,pulseTime,exAmpli);
                    
                    %  Switch on the dephasing readout gradient
                    iIns = obj.RampGradient(iIns,shimG(1),dephasingReadout,dac(1),crt,obj.nSteps);
                    obj.DELAYs(iIns-1) = crt/obj.nSteps+roPhaseTime;
                    
                    % Switch from dephasing to rephasing gradients
                    iIns = obj.RampGradient(iIns,dephasingReadout,rephasingReadout,dac(1),crt,obj.nSteps);
                    obj.DELAYs(iIns-1) = crt/obj.nSteps+gradDelay-blk;
                    
                    % Echo train
                    for nEcho = 1:ETL
                        % 180� RF pulse
                        iIns = obj.SetBlanking(iIns,1,blk);
                        obj.PhResets(iIns-1) = 0;
                        iIns = obj.CreateRawPulse(iIns,rePhase,pulseTime,reAmpli);
                        
                        % Dephasing phase and slice gradient trapezoid
                        obj.repetition = obj.repetition+mode;
                        phGradient = obj.gradientList(obj.repetition+~mode,1)*mode;
                        slGradient = obj.gradientList(obj.repetition+~mode,2)*mode;
                        iIns = obj.RampGradient(iIns,shimG(2:3),shimG(2:3)+[phGradient,slGradient],dac(2:3),crt,obj.nSteps);
                        obj.DELAYs(iIns-1) = crt/obj.nSteps+phTime;
                        iIns = obj.RampGradient(iIns,shimG(2:3)+[phGradient,slGradient],shimG(2:3),dac(2:3),crt,obj.nSteps);
                        obj.DELAYs(iIns-1) = crt/obj.nSteps+phDelay;
                        
                        % Acquisition
                        iIns = obj.Acquisition(iIns,acqTime);
                        obj.nline_reads = obj.nline_reads+1;
                        
                        % Rephasing phase gradient trapezoid
                        if(nEcho~=ETL)
                            iIns = obj.RampGradient(iIns,shimG(2:3),shimG(2:3)-[phFactor*phGradient,slFactor*slGradient],dac(2:3),crt,obj.nSteps);
                            obj.DELAYs(iIns-1) = crt/obj.nSteps+phTime;
                            iIns = obj.RampGradient(iIns,shimG(2:3)-[phFactor*phGradient,slFactor*slGradient],shimG(2:3),dac(2:3),crt,obj.nSteps);
                            obj.DELAYs(iIns-1) = crt/obj.nSteps+phDelay-blk;
                        end
                        
                        if(obj.repetition==size(obj.gradientList,1))
                            break
                        end
                    end
                    
                    % Repetition delay (1 instruction)
                    iIns = obj.RepetitionDelay(iIns,1,repDelay);
                else    % Shimming
                    % Activate shimming
                    iIns = obj.RampGradient(iIns,[0 0 0],shimG,dac,0.3,1);
                    
                    % 90� RF pulse
                    iIns = obj.SetBlanking(iIns,1,blk);
                    iIns = obj.CreateRawPulse(iIns,exPhase,pulseTime,exAmpli);
                    
                    % Get information
                    if(dummyRep==1)
                        dummyDel = sum(obj.DELAYs);
                    end
                    obj.DELAYs(iIns-1) = crt/obj.nSteps+obj.GetParameter('TR')*1e6-dummyDel;
                    dummyRep = dummyRep+1;
                end
                
                % Check if it is the last line
                if(obj.repetition==size(obj.gradientList,1))
                    obj.lastBatch = 1;
                    disp('Last Batch!')
                end
                
                % Get the number of points per TR for the first repetition
                if(obj.repetition==ETL)
                    obj.pointsPerTR = obj.nline_reads*nPoints;
                    obj.instPerTR = iIns-1;
                end
                
                % Determine if a new repetition is possible in the current batch
                if(iIns-1+obj.instPerTR>=2048 || obj.nline_reads*nPoints+obj.pointsPerTR>16000 || obj.lastBatch==1 || mode==0)
                    ready = 1;
                end
            end
            obj.nInstructions = iIns-1;
            obj.PhasesPerBatch = obj.nline_reads;
        end

        
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************


        function [status,result] = Run(obj,iAcq)
            clc
            
            global rawData
            global scanner
            
            % Check which gradients are switched on
            [~,~,ENRO,ENSL,ENPH,ENSP,~] = obj.ConfigureGradients(0);
            
            % Field of View
            FOVRO = obj.GetParameter('FOVRO');
            FOVPH = obj.GetParameter('FOVPH');
            FOVSL = obj.GetParameter('FOVSL');
            DELRO = obj.GetParameter('DELTARO');
            DELPH = obj.GetParameter('DELTAPH');
            DELSL = obj.GetParameter('DELTASL');
            nScans = obj.GetParameter('NSCANS');
            parAcq = obj.GetParameter('PARTIALACQ');
            rawData.inputs.Seq = obj.SequenceName;
            if(~isfield(rawData.iputs,'NEX'))
                rawData.inputs.NEX = nScans;
            else
                rawData.inputs.NEX = rawData.inputs.NEX+nScans;
            end
            rawData.inputs.NEX = nScans;
            rawData.inputs.partialAcquisition = parAcq;
            rawData.inputs.fov = [FOVRO,FOVPH,FOVSL];
            rawData.inputs.fovDeviation = [DELRO,DELPH,DELSL];
            sweepMode = obj.GetParameter('SWEEPMODE');
            if(sweepMode==0)
                rawData.inputs.sweepMode = 'k2k';
            else
                rawData.inputs.sweepMode = '02k';
            end
            rawData.inputs.dummyPulses = obj.GetParameter('DUMMY');
            
            % Matrix size
            obj.NRO = obj.GetParameter('NPOINTS');
            obj.NPH = obj.GetParameter('NPHASES')*ENPH+1*~ENPH;
            obj.NSL = obj.GetParameter('NSLICES')*ENSL+1*~ENSL;
            rawData.inputs.axes = [obj.GetParameter('READOUT'),...
                obj.GetParameter('PHASE'),obj.GetParameter('SLICE')];
            rawData.inputs.axisEnable = [ENRO,ENPH,ENSL];
            rawData.inputs.nPoints = [obj.NRO,obj.NPH,obj.NSL];
            
            % Resolution
            rawData.auxiliar.resolution =...
                [FOVRO/obj.NRO,FOVPH/obj.NPH,FOVSL/obj.NSL];
            
            % Parameters for image weighting
            TE = obj.GetParameter('TE');
            TR = obj.GetParameter('TR');
            ETL = obj.GetParameter('ETL');
            rawData.inputs.TE = TE;
            rawData.inputs.TR = TR;
            rawData.inputs.ETL = ETL;
            
            % RF parameters
            pulseTime = obj.GetParameter('PULSETIME');
            obj.acq_time = obj.GetParameter('ACQTIME');
            BW = obj.NRO/obj.acq_time;
            obj.SetParameter('SPECTRALWIDTH',BW);
            rawData.inputs.pulseTime = pulseTime;
            rawData.inputs.exitationAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.refocusingAmplitude = obj.GetParameter('RFAMPLITUDE2');
            rawData.inputs.acqTime = obj.acq_time;
            rawData.auxiliar.bandwidth = BW;
            
            % Miscellaneous
            coilRiseTime = obj.GetParameter('COILRISETIME');
            gradientDelay = scanner.grad_delay*1e-6;
            rawData.auxiliar.coilRiseTime = coilRiseTime;
            rawData.auxiliar.gradientDelay = gradientDelay;
            rawData.inputs.readoutFactor = obj.GetParameter('ROFACTOR');
            rawData.inputs.phaseFactor = obj.GetParameter('PHFACTOR');
            rawData.inputs.sliceFactor = obj.GetParameter('SLFACTOR');
            prepolTime = obj.GetParameter('PREPOLTIME');
            prepolcorrection = obj.GetParameter('PREPOLCORRECTION');
            delayPP = obj.GetParameter('DELAYPREPOL');
            
            if prepolTime>0
                rawData.inputs.prepolTime = prepolTime;
                rawData.inputs.prepolCorrection = prepolcorrection;
                rawData.inputs.delayPrepolRF = delayPP;
            end
            
            % Readout dephasing time
            obj.roDephTime = TE/2-pulseTime-2*coilRiseTime-gradientDelay;
            rawData.auxiliar.roDephasingTime = obj.roDephTime;
            
            % Phase de and rephasing time
            obj.phaseTime = obj.GetParameter('PHASETIME');
            phaseTimeMax = (TE-pulseTime-obj.acq_time)/2-2*coilRiseTime-gradientDelay;
            if(obj.phaseTime==0)
                obj.phaseTime = phaseTimeMax;
            elseif(obj.phaseTime>phaseTimeMax)
                warndlg('Phase time is longer than maximum allowed. Reduce phase time or set to zero to autoset.',...
                    'Phase time too long');
                return;
            end
            obj.phaseDelay = (TE-pulseTime-obj.acq_time)/2-2*coilRiseTime-obj.phaseTime;
            rawData.inputs.phaseTime = obj.phaseTime;
            rawData.auxiliar.phaseDelay = obj.phaseDelay;
            
            % Readout gradients
            c = obj.ReorganizeCalibrationData();
            obj.RO_strength_rep = BW./(obj.gam*FOVRO)*ENRO;
            obj.RO_strength_dep = 0.5*obj.RO_strength_rep*(TE-coilRiseTime-pulseTime)/...
                (coilRiseTime+obj.roDephTime);
            rawData.auxiliar.dephasingGradient = obj.RO_strength_dep;
            rawData.auxiliar.rephasingGradient = obj.RO_strength_rep;
            if abs(obj.RO_strength_dep) >= 0.72*c(1)
                warndlg('The readout dephasing gradient is too high.','Readout error')
                return;
            end
            if abs(obj.RO_strength_rep) >= 0.72*c(1)
                warndlg('The readout rephasing gradient is too high.','Readout error')
                return;
            end
            
            % Phase and slice gradients
            indF = obj.GetIndex(obj.NPH,ETL,parAcq,sweepMode);
            maxPhGrad = obj.NPH/(2*obj.gam*(obj.phaseTime+coilRiseTime)*FOVPH);
            maxSlGrad = obj.NSL/(2*obj.gam*(obj.phaseTime+coilRiseTime)*FOVSL);
            phaseGradients = linspace(-maxPhGrad,maxPhGrad,obj.NPH*ENPH+1*~ENPH)'*ENPH;
            sliceGradients = linspace(-maxSlGrad,maxSlGrad,obj.NSL*ENSL+1*~ENSL)'*ENSL;
            phaseGradients = phaseGradients(indF);
            obj.gradientList = [];
            for ii = 1:obj.NSL
                obj.gradientList = cat(1,obj.gradientList,[phaseGradients,ones(obj.NPH,1)*sliceGradients(ii)]);
            end
            rawData.auxiliar.phaseGradient = maxPhGrad*ENPH;
            rawData.auxiliar.sliceGradient = maxSlGrad*ENSL;
            rawData.auxiliar.gradientList = obj.gradientList;
            if abs(maxPhGrad)>=0.72*c(2)
                warndlg('The phase dephasing gradient is too high.','Phase error')
                return;
            end
            if abs(maxSlGrad)>=0.72*c(3)
                warndlg('The slice dephasing gradient is too high.','Slice error')
                return;
            end
            
            % Set the gradient amplitudes in arbitrary units
            obj.RO_strength_rep = obj.RO_strength_rep/c(1);
            obj.RO_strength_dep = obj.RO_strength_dep/c(1);
            obj.gradientList = obj.gradientList./[c(2) c(3)];
            
            % Repetition delay
            obj.RepDelay  = TR-(prepolcorrection+prepolTime+delayPP+scanner.BlankingDelay*1e-6+pulseTime/2+ETL*TE+...
                obj.acq_time/2);
            rawData.auxiliar.repetitionDelay = obj.RepDelay;
            if obj.RepDelay <=0
                warndlg('The TR is too short','TR error')
                return;
            end
            obj.SetParameter('REPETITIONDELAY',obj.RepDelay);
            
            nPoints = obj.NRO;
            nSlices = obj.NSL;
            nPhases = obj.NPH;
            
            % Get value to normalize fid to Volts
            bwCalNormVal = obj.calculate_bwCalNormVal;
            
            Data2D = [];
            obj.repetition = 0;
            obj.lastBatch = 0;
            iBatch = 0;
            tic;
            
            fLi = obj.GetParameter('FREQUENCY'); %Save initial Larmor Freq.
            fileID = fopen('C:\Users\MI PC\Desktop\REMOTE CONTROL\chilldaq\matlabcurrent.txt','r');
            iSOld = fscanf(fileID,'%s');
            fclose(fileID);
            iSOld = replace(iSOld,',','.');
            iOld = str2double(iSOld);
            
            if(~isfield(rawData.auxiliar,'freqList'))
                rawData.auxiliar.freqList = [];
            end
            while(obj.lastBatch==0)
                tA = clock;
                iBatch = iBatch+1;
                if(iBatch==1)
                    fprintf('Batch %1.0f, estimating remaining time \n',iBatch);
                else
                    tC = tC*(nPhaseBatches-iBatch+1);
                    fprintf('Batch %1.0f/%1.0f, remaining time: %1.0f s \n',iBatch,nPhaseBatches,tC);
                end
               
                % Calibrate frequency
                if(strcmp(scanner.name, 'HISTO') && ENSP)
                    %Change Larmor Frequency
                    fNew = (fLi-BW/2)+BW*rand;
                    iNew=5.3463e-6*(fNew-fLi)+iOld; %Calibration Line
                    if iNew<190 && iNew>180 %
                        iSNew = num2str(iNew);
                        iSNew = replace(iSNew,'.',',');
                        fileID = fopen('C:\Users\MI PC\Desktop\REMOTE CONTROL\chilldaq\matlabcurrent.txt','w');
                        fprintf(fileID,'%s\n',iSNew);
                        fclose(fileID);
                        pause(2);
                    end %
                    
                    fprintf('Calibrating frequency... \n')
                    % Get central frequency
                    f0 = obj.GetFIDParameters;
                    obj.SetParameter('FREQUENCY',f0);
                    rawData.auxiliar.freqList(nFreq) = cat(2,rawData.auxiliar.freqList,f0);
                    fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
                elseif(strcmp(scanner.name, 'Dental') && ENSP && (toc>300 || iBatch==1))
                    fprintf('Calibrating frequency... \n')
                    % Get central frequency
                    f0 = obj.GetFIDParameters;
                    obj.SetParameter('FREQUENCY',f0);
                    rawData.auxiliar.freqList(nFreq) = cat(2,rawData.auxiliar.freqList,f0);
                    fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
                    tic;
                end
                
                % Calibrate average T2
                if(iBatch==1)
                    fprintf('Average T2 calibration... \n \n')
                    nScans = obj.GetParameter('NSCANS');
                    obj.SetParameter('NSCANS',ceil(10/obj.GetParameter('TR')));
                    obj.SetParameter('NSCANS',1);
                    obj.CreateSequence(0);
                    obj.Sequence2String;
                    fname = 'TempAverageT2Calibration.bat';
                    obj.WriteBatchFile(fname);
                    [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                    if status == 0 && ~isempty(result)
                        C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                        res = C{:};
                        obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                        obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                        obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                        obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                        obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                    else
                        warndlg('Average T2 calibration aborted!','Warning')
                        return;
                    end
                    DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                    cDataBatch = complex(DataBatch(:,1),DataBatch(:,2))/bwCalNormVal;
                    avT2Cal = reshape(cDataBatch,nPoints,ETL);
                    figure(5)
                    subplot(1,2,1)
                    imagesc(abs(avT2Cal))
                    title('T2 cal abs')
                    subplot(1,2,2)
                    imagesc(angle(avT2Cal))
                    title('T2 cal phase')
                    colormap gray
                    avT2Cal = avT2Cal(ceil(end/2),:);
                    rawData.kSpace.avT2Cal = avT2Cal;
                    obj.SetParameter('NSCANS',nScans);
                end
                
                % NOW RUN ACTUAL ACQUISITION
                obj.CreateSequence(1);
                if(iBatch==1)
                    nPhaseBatches = ceil(nPhases/obj.PhasesPerBatch);
                end
                if(sum(obj.DELAYs<0)~=0)
                    warndlg('At least one delay is negative!','Delay Error')
                    return;
                end
                obj.Sequence2String;
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Batch aborted!','Warning')
                    return;
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2))/bwCalNormVal;
                Data2D = cat(1,Data2D,cDataBatch);
                
                
                tB = clock;
                tC = etime(tB,tA);
                nPhaseBatches = ceil(size(obj.gradientList,1)/obj.nline_reads);
            end
            
            %Recuperate initial Larmor Freq.
            
            fileID = fopen('C:\Users\MI PC\Desktop\REMOTE CONTROL\chilldaq\matlabcurrent.txt','w');
            iSOld = replace(iSOld,'.',',');
            fprintf(fileID,'%s\n',iSOld);
            fclose(fileID);
            pause(1);
            fLi = obj.GetFIDParameters;
            obj.SetParameter('FREQUENCY',fLi);
            
            % Do zero padding if partial acquisition
%             if(parAcq~=0)
%                 Data2D(nPoints*(nPhases/2+parAcq):nPoints*nPhases) = 0;
%             end
            
            % Apply mask if desired
            Data2D = reshape(Data2D,nPoints,nPhases,nSlices);
            data2dRaw = zeros(size(Data2D));
%             data2dCalPha = zeros(size(Data2D));
%             data2dCalPhaMag = zeros(size(Data2D));             
            nn = 0;
            for ii = 1:nPhases*(parAcq==0)+(nPhases/2+parAcq)*(parAcq~=0)
                nn = nn+1;
                ind = indF(ii);
                data2dRaw(:,ind,:) = Data2D(:,ii,:);
%                 % Amplitude and Phase mask
                data2dCalPhaMag(:,ind) = Data2D(:,ii)/abs(avT2Cal(nn))*exp(-1i*(angle(avT2Cal(nn))-angle(avT2Cal(1))));
                data2dCalPha(:,ind,:) = Data2D(:,ii,:)*exp(-1i*(angle(avT2Cal(nn))-angle(avT2Cal(1))));
                if nn==ETL
                    nn = 0;
                end
            end
            
%             imagenRaw = abs(ifftshift(ifftn(data2dRaw)));
%             imagenCalPhaMag = abs(ifftshift(ifftn(data2dCalPhaMag)));
%             imagenCalPha = abs(ifftshift(ifftn(data2dCalPha)));
%             figure(4)
%             set(gcf, 'Position', [200, 550, 1400, 400])
%             subplot(1,3,1)
%             imagesc(imagenRaw);
%             title('raw image')
%             subplot(1,3,2)
%             imagesc(imagenCalPhaMag);
%             title('phase and mag calibrated image')
%             subplot(1,3,3)
%             imagesc(imagenCalPha);
%             title('phase calibrated image')
%             colormap gray
            
            Data2D = reshape(data2dRaw,nPoints*nPhases*nSlices,1);
            clear Data2Dp
            
            % k-space points
            kMax = 1./(2*rawData.auxiliar.resolution);
            kPointsX = linspace(-kMax(1),kMax(1),nPoints);
            kPointsY = linspace(-kMax(2),kMax(2),nPhases)*ENPH;
            kPointsZ = linspace(-kMax(3),kMax(3),nSlices)*ENSL;
            [kPointsX,kPointsY,kPointsZ] = meshgrid(kPointsX,kPointsY,kPointsZ);
            kPointsX = permute(kPointsX,[2,1,3]);
            kPointsY = permute(kPointsY,[2,1,3]);
            kPointsZ = permute(kPointsZ,[2,1,3]);
            kSpaceValues = [kPointsX(:),kPointsY(:),kPointsZ(:)];
            phase = exp(-2*pi*1i*(DELRO*kPointsX(:)+DELPH*kPointsY(:)+DELSL*kPointsZ(:)));
            kSpaceValues = [kSpaceValues,Data2D.*phase];
            
            rawData.auxiliar.kMax = kMax;
            if(~isfield(rawData.kSpace,'sampledInd'))
                rawData.kSpace.sampledInd = kSpaceValues;
                rawData.kSpace.sampled = kSpaceValues;
                rawData.auxiliar.time = clock;
            else
                rawData.kSpace.sampledInd = cat(2,rawData.kSpace.sampledInd,kSpaceValues(:,4));
                rawData.kSpace.sampled(:,4) = mean(kSpaceValues(:,4:end),2);
            end
            kSpaceValues = reshape(rawData.kSpace.sampled(:,4),obj.NRO,obj.NPH,obj.NSL);
            imagen = abs(ifftshift(ifftn(kSpaceValues)));
            rawData.kSpace.imagen = imagen;
            rawData.kSpace.complexFFTimagen  = (ifftshift(ifftn(kSpaceValues)));
            
            % Uncomment this block to amend "chess board" problem in real component of FFT image
            rF=real(rawData.kSpace.complexFFTimagen);
            iF=imag(rawData.kSpace.complexFFTimagen);
            rF2=rF;
            iF2=rF;
            NX=obj.NRO;
            NY=obj.NPH;
            NZ=obj.NSL;
            for i=1:NX
                for j=1:NY
                    for k=1:NZ
                        rF2(i,j,k)=(-1)^(i+j+k+1)*rF(i,j,k);
                        iF2(i,j,k)=(-1)^(i+j+k)*iF(i,j,k);
                    end
                end
            end
            rawData.kSpace.complexFFTimagen = rF2 + 1i*iF2;
                
            % Save rawData
            time = rawData.auxiliar.time;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


        function ind = GetIndex(obj,np,ETL,parAcq,sweepMode)
            if(sweepMode==0 && ETL==np)
                ind = linspace(1,np,np);
                return;
            end
            if(np==1)
                ind = 1;
                return;
            end
            if(ETL==np)
                if(parAcq==0)
                    indA = (1:np)';
                    ind1 = indA(np/2:-1:1);
                    ind2 = indA(np/2+1:np);
                    
                    ind = zeros(np,1);
                    ind(1:2:end-1) = ind1;
                    ind(2:2:end) = ind2;
                else
                    ind = (np/2+parAcq:-1:1)';
                end
            else
                if(parAcq==0)
                    boxSize = np/(2*ETL);
                    
                    indA = (1:np)';
                    ind1 = indA(np/2:-1:1);
                    ind2 = indA(np/2+1:np);
                    
                    ind11 = zeros(size(ind1));
                    ind22 = zeros(size(ind2));
                    
                    for ii = 1:np/2/ETL
                        ind11((ii-1)*ETL+1:ii*ETL) = ind1(ii:boxSize:np/2-boxSize+ii);
                        ind22((ii-1)*ETL+1:ii*ETL) = ind2(ii:boxSize:np/2-boxSize+ii);
                    end
                    ind = [ind11;ind22];
                else
                    boxSize = np/(2*ETL);
                    
                    indA = (1:np)';
                    ind1 = indA(np/2:-1:1);
                    ind2 = indA(np/2+1:np/2+parAcq);
                    
                    ind11 = zeros(size(ind1));
                    ind22 = zeros(size(ind2));
                    
                    for ii = 1:np/2/ETL
                        ind11((ii-1)*ETL+1:ii*ETL) = ind1(ii:boxSize:np/2-boxSize+ii);
                    end
                    ind = [ind11;ind2];
                end
            end
            if(sweepMode==0)
                ind = ind(end:-1:1);
            end
        end
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        
    end
end

