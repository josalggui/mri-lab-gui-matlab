classdef GSE2D_Units < MRI_BlankSequence
    % Gradient Echo sequence so that the input parameters are user
    % parameters: FOV in each axes, resolution, etc.
    
    %  Elena Diaz Caballero
    
    % 14.07.2019 - Mods from Jos� Miguel Algar�n.
    
    properties
        StartSlice;
        EndSlice;
        PhasesPerBatch = 20;
        PhasesinThisBatch = 1;
        StartPhase;
        EndPhase;
        slice_ampl;
        slice_ampl_re;
        acq_time;
        PhaseTime;
        RepDelay;
        RO_strength_rep;
        RO_strength_dep;
        SL_strength_dep;
        SL_strength_rep;
        NRO;
        NPH;
        NSL;
        Axis1;
        Axis2;
        Axis3;
        CAxis1;
        CAxis2;
        CAxis3;
        rdspeed_mult = 1;
        nPhasesBatch;
        NP_calib = 1;
        ROmax = 0;
        CalData;
        FOV;
        gam = 42.57E6; %gyromagnetic ratio, 42.57 MHz/T
        gradientList;
        sliceRephasingTime;
        gapTime1;
        gapTime2;
        sincZeroCrossings = 1;        % Number of zero crossing for the RF sinc pulse
        maxBatchTime = 600;             % Maximun batch time in seconds
        lastBatch;
        repetition;
        pointsPerTR = 0;
    end
    methods (Access=private)
        function CreateParameters(obj)
            
            % Input parameters
            obj.CreateOneParameter('TE','TE time','ms',10*MyUnits.ms);
            obj.CreateOneParameter('TR','TR time','ms',50*MyUnits.ms);
            obj.CreateOneParameter('FOVRO','FOV Readout','mm',10*MyUnits.mm);
            obj.CreateOneParameter('FOVPH','FOV Phase','mm',10*MyUnits.mm);
            obj.CreateOneParameter('FOVSL','FOV Slice','mm',2*MyUnits.mm);
            obj.CreateOneParameter('NPOINTS','N Readout','',10);
            obj.CreateOneParameter('NPHASES','N Phase','',10);
            obj.CreateOneParameter('REPHASEFACTOR','Dephasing factor','',1);
            obj.CreateOneParameter('ACQTIME','Acquisition Time','us',400*MyUnits.us);
            obj.CreateOneParameter('REPETITIONDELAY','Repetition Delay','ms',50*MyUnits.ms);
            obj.CreateOneParameter('SLICEPOSITION','Slice Position','mm',0*MyUnits.mm);
            obj.CreateOneParameter('SLICEFACTOR','Slice Factor','',1);
            obj.CreateOneParameter('JUMPINGFACTOR','Jumping Factor','',1);
            obj.CreateOneParameter('DELTARO','RO Deviation','mm',0);
            obj.CreateOneParameter('DELTAPH','PH Deviation','mm',0);
            obj.CreateOneParameter('DELTASL','SL Deviation','mm',0);
            obj.CreateOneParameter('REFOCUSINGTIME','180� Pulse time','us',400*MyUnits.us);
            obj.CreateOneParameter('PULSETIME','90� Pulse time','us',200*MyUnits.us);
            
            % Output parameters
            OutputParameters = {...
                'MAXPH','RO_STRENGTH','MAXSL'...
                };
            obj.OutputParNames = [containers.Map(obj.OutputParNames.keys(),obj.OutputParNames.values());containers.Map(OutputParameters,...
                {...
                'Start Phase', 'Readout amplitude', 'Start Slice'...
                })];
            obj.OutputParValues = [containers.Map(obj.OutputParValues.keys(),obj.OutputParValues.values());containers.Map(OutputParameters,...
                {...
                NaN,NaN,NaN...
                })];
            obj.OutputParUnits = [containers.Map(obj.OutputParUnits.keys(),obj.OutputParUnits.values());containers.Map(OutputParameters,...
                {...
                '','',''...
                })];
            for k=1:length(obj.OutputParameters)
                OutputParameters(strcmp(obj.OutputParameters{k},OutputParameters)) = [];
            end
            obj.OutputParameters = [obj.OutputParameters,OutputParameters];
            
            % Hide parameters
%             obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
%                 'BLANKINGDELAY',...
%                 'TRANSIENTTIME','COILRISETIME','SPOILERAMP',...
%                 'SPOILERTIME','SPECTRALWIDTH','REPETITIONDELAY',...
%                 'NSLICES'};
            
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
                'BLANKINGDELAY','COILRISETIME',...
                'TRANSIENTTIME','SPOILERAMP',...
                'SPOILERTIME','SPECTRALWIDTH','REPETITIONDELAY',...
                'NSLICES','DELTASL'};


            % Rearange parameters
            obj.MoveParameter(7,5);
            obj.MoveParameter(9,6);
            obj.MoveParameter(10,7);
            obj.MoveParameter(19,8);
            obj.MoveParameter(20,9);
            obj.MoveParameter(25,8);
            obj.MoveParameter(29,9);
            obj.MoveParameter(27,10);
            obj.MoveParameter(13,26);
            obj.MoveParameter(30,33);
            obj.MoveParameter(26,23);
            obj.MoveParameter(27,24);
            obj.MoveParameter(34,8);
            obj.MoveParameter(20,19);
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = GSE2D_Units(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'GSE2D Real units';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
            
            obj.SetParameter('RFAMPLITUDE',0.01);
            obj.SetParameter('PULSETIME',100*MyUnits.us);
            obj.SetParameter('BLANKINGDELAY',200*MyUnits.us);
            obj.SetParameter('TRANSIENTTIME',0.1*MyUnits.us);
            obj.SetParameter('COILRISETIME',100*MyUnits.us);
            obj.SetParameter('NPOINTS',10);
            obj.SetParameter('NPHASES',10);
            obj.SetParameter('NSLICES',1);
            
            % This value is empherical and picked based on not creating eddy currents
            obj.SetParameter('FREQUENCY',11*MyUnits.MHz);
            
            % Grab the calibration data if there
            try
                obj.CalData = GradientCalibrationData();
            catch
                obj.CalData.X_TD = 1;
                obj.CalData.Y_TD = 1;
                obj.CalData.Z_TD = 1;
                obj.CalData.X_TV = 1;
                obj.CalData.Y_TV = 1;
                obj.CalData.Z_TV = 1;
            end
            
        end
        function UpdateTETR(obj)
            obj.TE = obj.GetParameter('TE');
            obj.TR = obj.GetParameter('TR');
            [~,~,~,ENSL,ENPH,~,~] = obj.ConfigureGradients(0);
            obj.NPH = obj.GetParameter('NPHASES')*ENPH+1*~ENPH;
            obj.NSL = obj.GetParameter('NSLICES')*ENSL+1*~ENSL;

            obj.TTotal = obj.TR * obj.GetParameter('NSCANS')* obj.NSL * obj.NPH;
        end
        
        
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************



        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence,
            %         "1", slice gradient delay adjustment,
            
            global scanner
            c = obj.ReorganizeCalibrationData();
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            
            % Configure gradients and axes
            [~,~,ENRO,ENSL,ENPH,~,~] = obj.ConfigureGradients(mode);
            
            % Set the number of lines to be read
            nMisses = obj.GetParameter('JUMPINGFACTOR');
            
            % Calculate the number of line reads
            obj.nline_reads = 0;
            nPoints = obj.GetParameter('NPOINTS');
            
            % Initialize all vectors to zero with dynamic size
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs,obj.RFamps] = deal([]);
            iIns = 1;
            
            % Load timing from RawData;
            crt             = obj.GetParameter('COILRISETIME')*1e6;
            blk             = scanner.BlankingDelay;
            ExTime          = obj.GetParameter('PULSETIME')*1e6;
            ReTime          = obj.GetParameter('REFOCUSINGTIME')*1e6;
            sliceRephaTime  = obj.sliceRephasingTime*1e6;
            gapTimeBefore   = obj.gapTime1*1e6;
            gapTimeAfter    = obj.gapTime2*1e6;
            phaseTime       = obj.PhaseTime*1e6;
            gradDelay       = scanner.grad_delay;
            acqTime         = obj.GetParameter('ACQTIME')*1e6;
            repDelay        = obj.RepDelay*1e6;
            
            % Shimming
            shimG = [shimR,shimP,shimS];
            dac = [obj.selectReadout,obj.selectPhase,obj.selectReadout];
            
            % Readout gradient amplitudes
            dephasingReadout = shimG(1)+obj.RO_strength_dep*ENRO;
            rephasingReadout = shimG(1)+obj.RO_strength_rep*ENRO;

            % Slice slection gradient amplitudes
            gds = shimG(1)+obj.SL_strength_dep*ENSL*c(3)/c(1);
            grs = shimG(1)+obj.SL_strength_rep*ENSL*c(3)/c(1);
            
            obj.ROmax = abs(rephasingReadout);
            
            % BEGIN THE PULSE PROGRAMMING
            iIns = 1;
            excitationPhase = 0;
            refocusingPhase = 1;
            ready = 0;
            ii = 0;
            while(ready==0)
                ii = ii+1;
                
                % Get the amplitude for the current line
                if(ii<=nMisses)
                    ind = ceil(rand*length(obj.gradientList));
                    phaseAmplitude = shimG(2)+obj.gradientList(ind);
                    clear ind
                else
                    obj.repetition = obj.repetition+1;
                    phaseAmplitude = shimG(2)+obj.gradientList(obj.repetition);
                end
                
                %% Activate shimming
                iIns = obj.RampGradient(iIns,[0 0 0],shimG,dac,0.3,1);
                
                %% Activate dephasing slice gradient
                iIns = obj.RampGradient(iIns,shimS,gds,dac(3),crt,obj.nSteps);
                obj.DELAYs(iIns-1) = crt/obj.nSteps+gradDelay-blk;
                
                %% 90� RF pulse
                iIns = obj.SetBlanking(iIns,1,blk);
                iIns = obj.CreateRawPulse(iIns,excitationPhase,ExTime,0);
                
                %% Switch from dephasing to rephasing slice gradient
                iIns = obj.RampGradient(iIns,gds,shimG(1),dac(3),crt,obj.nSteps);
                iIns = obj.RampGradient(iIns,shimG(1),grs,dac(3),crt,obj.nSteps);
                if(ExTime>=crt-2*gradDelay)
                    obj.DELAYs(iIns-1) = crt/obj.nSteps+sliceRephaTime;
                else
                    obj.DELAYs(iIns-1) = crt/obj.nSteps;
                end
                
                %% Deactivate rephasing slice gradient
                iIns = obj.RampGradient(iIns,grs,shimG(1),dac(3),crt,obj.nSteps);
                obj.DELAYs(iIns-1) = crt/obj.nSteps+gapTimeBefore-blk;
                
                %% 180� RF pulse
                iIns = obj.SetBlanking(iIns,1,blk);
                obj.PhResets(iIns-1) = 0;
                iIns = obj.CreateRawPulse(iIns,refocusingPhase,ReTime,0);
                obj.FREQs(iIns-1) = 0;
                iIns = obj.RepetitionDelay(iIns,0,gapTimeAfter);
                
                %% Activate dephasing gradients - readout, phase
                g0 = [shimR,shimP];
                g1 = [dephasingReadout,phaseAmplitude];
                iIns = obj.RampGradient(iIns,g0,g1,dac(1:2),crt,obj.nSteps);
                obj.DELAYs(iIns-1) = crt/obj.nSteps-0.1+phaseTime;
                
                %% Deactivate dephasing gradients - readout, phase
                iIns = obj.RampGradient(iIns,g1,g0,dac(1:2),crt,obj.nSteps);
                
                %% Activate rephasing gradient - readout
                iIns = obj.RampGradient(iIns,shimR,rephasingReadout,dac(1),crt,obj.nSteps);
                obj.DELAYs(iIns-1) = crt/obj.nSteps+gradDelay;
            
                %% Acquisition
                if(ii<=nMisses)
                    iIns = obj.RepetitionDelay(iIns,0,acqTime);
                else
                    iIns = obj.Acquisition(iIns,acqTime);
                    obj.nline_reads = obj.nline_reads+1;
                end
                
                %% Deactivate rephasing gradient - readout
                iIns = obj.RampGradient(iIns,rephasingReadout,shimR,dac(1),crt,obj.nSteps);
                
                %% Repetition delay (1 instruction)
                if(obj.repetition==length(obj.gradientList))
                    iIns = obj.RepetitionDelay(iIns,1,repDelay);
                    obj.lastBatch = 1;
                    disp('Last Batch!')
                else
                    iIns = obj.RepetitionDelay(iIns,0,repDelay);
                    obj.lastBatch = 0;
                end
                
                %% If it is the first repetition, set up instructions and points to be read per TR
                if(ii==1)
                    instruPerTR = iIns-1;
                end
                if(obj.repetition==1)
                    obj.pointsPerTR = obj.nline_reads*nPoints;
                end
                
                %% Determine if a new repetition is possible in the current batch
                if(iIns-1+instruPerTR>2048 || obj.nline_reads*nPoints+obj.pointsPerTR>16000 || obj.lastBatch==1)
                    ready = 1;
                end
                
            end
            obj.nInstructions = iIns-1;
            obj.PhasesPerBatch = obj.nline_reads+nMisses;
        end

        
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************


        function [status,result] = Run(obj,iAcq)
            clc
            
            global rawData
            global scanner
            rawData = [];
            
            obj.SetParameter('BLANKINGDELAY',100e-6);
            
            % Check which gradients are switched on
            [~,~,ENRO,ENSL,ENPH,ENSP,~] = obj.ConfigureGradients(0);
            
            % Field of View
            FOVRO = obj.GetParameter('FOVRO');
            FOVPH = obj.GetParameter('FOVPH');
            FOVSL = obj.GetParameter('FOVSL');
            POSSL = obj.GetParameter('SLICEPOSITION');
            DELRO = obj.GetParameter('DELTARO');
            DELPH = obj.GetParameter('DELTAPH');
            DELSL = obj.GetParameter('DELTASL');
            nScans = obj.GetParameter('NSCANS');
            rawData.inputs.axes = [obj.GetParameter('READOUT'),...
                obj.GetParameter('PHASE'),obj.GetParameter('SLICE')];
            rawData.inputs.Seq = obj.SequenceName;
            rawData.inputs.NEX = nScans;
            rawData.inputs.fov = [FOVRO,FOVPH,FOVSL];
            rawData.inputs.POS = POSSL;
            rawData.inputs.DEV = [DELRO,DELPH];
            
            % Matrix size
            obj.NRO = obj.GetParameter('NPOINTS');
            obj.NPH = obj.GetParameter('NPHASES')*ENPH+1*~ENPH;
            obj.NSL = 1;
            rawData.inputs.axis = strcat(obj.GetParameter('READOUT'),...
                obj.GetParameter('PHASE'),obj.GetParameter('SLICE'));
            rawData.inputs.axisEnable = [ENRO,ENPH,ENSL];
            rawData.inputs.nPoints = [obj.NRO,obj.NPH,1];
            
            % Resolution
            rawData.auxiliar.resolution =...
                [FOVRO/obj.NRO,FOVPH/obj.NPH,FOVSL];
            
            % Parameters for image weighting
            TE = obj.GetParameter('TE');
            TR = obj.GetParameter('TR');
            rawData.inputs.TE = TE;
            rawData.inputs.TR = TR;
            
            % RF parameters
            exTime = obj.GetParameter('PULSETIME');
            reTime = obj.GetParameter('REFOCUSINGTIME');
            obj.acq_time = obj.GetParameter('ACQTIME');
            BW = obj.NRO/(obj.acq_time);
            obj.SetParameter('SPECTRALWIDTH',BW);
            rawData.inputs.exitationTime = exTime;
            rawData.inputs.refocusingTime = reTime;
            rawData.inputs.amplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.acqTime = obj.acq_time;
            rawData.inputs.rfShape = obj.RF_Shape;
            rawData.auxiliar.bandwidth = BW;
            
            % Miscellaneous
            coilRiseTime = obj.GetParameter('COILRISETIME');
            gradientDelay = scanner.grad_delay*1e-6;
            rawData.inputs.jumpingFactor = obj.GetParameter('JUMPINGFACTOR');
            rawData.auxiliar.coilRiseTime = coilRiseTime;
            rawData.auxiliar.gradientDelay = gradientDelay;
            
            % Dephasing time (Phase and Readout)
            obj.PhaseTime = 0.5*obj.acq_time;
            rawData.auxiliar.phaseTime = obj.PhaseTime;
            
            % Rephasing time (Slice)
            obj.sliceRephasingTime = 0.5*exTime+gradientDelay-0.5*coilRiseTime;
            if obj.sliceRephasingTime<0
                obj.sliceRephasingTime = 0;
            end
            rawData.auxiliar.sliceRephasingTime = obj.sliceRephasingTime;
            
            % Repetition delay
            tprov = 2*coilRiseTime+gradientDelay+0.5*exTime+TE+0.5*obj.acq_time;
            obj.RepDelay  = TR-tprov;
            rawData.auxiliar.repetitionDelay = obj.RepDelay;
            % check if the phase time can be possible given the selected TE, if not adjust TE as needed
            if obj.RepDelay <=0
                warndlg('The repetition delay is negative! Try making your TR longer','TR error')
                fprintf('TR should be longer than %1.3f ms \n',tprov*1e3)
                return;
            end
            clear tprov
            obj.SetParameter('REPETITIONDELAY',obj.RepDelay);
            
            % Gap time
            obj.gapTime1 = TE/2-exTime/2-reTime/2-3*coilRiseTime-obj.sliceRephasingTime;
            obj.gapTime2 = TE/2-3*coilRiseTime-reTime/2-obj.PhaseTime-1/2*obj.acq_time-scanner.grad_delay*1e-6;
            if(obj.gapTime1<0)
                warndlg('Gap time between slice gradient and 180� pulse is negative! Try making your TE longer','Echo error')
                fprintf('TE should be longer than %1.3f ms \n',...
                    (exTime/2+reTime/2+3*coilRiseTime+obj.sliceRephasingTime)*2e3);
                return;
            end
            if(obj.gapTime2<0)
                warndlg('Gap time between 180� pulse and dephasing gradient is negative! Try making your TE longer','Echo error')
                fprintf('TE should be longer than %1.3f ms \n',...
                    (3*coilRiseTime+reTime/2+obj.PhaseTime+obj.acq_time/2)*2e3);
                return;
            end
           
            % Readout gradients
            c = obj.ReorganizeCalibrationData();
            rephaseFactor = obj.GetParameter('REPHASEFACTOR');
            obj.RO_strength_rep = BW./(obj.gam*FOVRO)*ENRO;
            obj.RO_strength_dep = -obj.NRO./(2*obj.gam*FOVRO*(coilRiseTime+obj.PhaseTime))*ENRO;
            obj.RO_strength_dep = obj.RO_strength_dep*rephaseFactor;
            rawData.inputs.rephaseFactor = rephaseFactor;
            rawData.auxiliar.dephasingGradient = obj.RO_strength_dep;
            rawData.auxiliar.rephasingGradient = obj.RO_strength_rep;
            if abs(obj.RO_strength_dep) >= 0.95*c(1)
                warndlg('The readout dephasing gradient is too high.','Readout error')
                return;
            end
            if abs(obj.RO_strength_rep) >= 0.95*c(1)
                warndlg('The readout rephasing gradient is too high.','Readout error')
                return;
            end
            
            % Slice gradient
            sliceFactor = obj.GetParameter('SLICEFACTOR');
            obj.SL_strength_dep = 7/(obj.gam*FOVSL*exTime)*ENSL;               % 7 is the number of lobes in the sinc function (3 lobes each side and 1 main lobe)
            if(exTime>=coilRiseTime-2*gradientDelay)
                obj.SL_strength_rep = -sliceFactor*obj.SL_strength_dep*ENSL;
            else
                obj.SL_strength_rep = -sliceFactor*0.5*obj.SL_strength_dep*(exTime+gradientDelay+coilRiseTime/2)/coilRiseTime*ENSL;
            end
            rawData.auxiliar.sliceFactor = sliceFactor;
            rawData.auxiliar.sliceDephasingGradient = obj.SL_strength_dep;
            rawData.auxiliar.sliceRephasingGradient = obj.SL_strength_rep;
            if abs(obj.SL_strength_dep/c(1)) >= 0.72
                warndlg('The slice dephasing gradient is too high. Try making the RF pulse time longer or making the slice thicker.','Slice error')
                return;
            end
            if abs(obj.SL_strength_rep/c(1)) >= 0.72
                warndlg('The slice rephasing gradient is too high. Try making the RF pulse time shorter.','Slice error')
                return;
            end
            
            % Phase gradient
            PH_strength_max = obj.NPH/(2*obj.gam*(obj.PhaseTime+coilRiseTime)*FOVPH);
            phaseGradients = linspace(-PH_strength_max,PH_strength_max,obj.NPH*ENPH+1*~ENPH)'*ENPH;
            rawData.auxiliar.phaseGradient = PH_strength_max*ENPH;
            rawData.auxiliar.gradientList = phaseGradients;
            obj.gradientList = phaseGradients;
            if abs(PH_strength_max) >= 0.95*c(2)
                warndlg('The phase dephasing gradient too high.','Phase error')
                return;
            end
            
            % Set the gradient amplitudes in arbitrary units
            gradientsArray = rawData.auxiliar.gradientList/c(2);
            obj.RO_strength_rep = obj.RO_strength_rep/c(1);
            obj.RO_strength_dep = obj.RO_strength_dep/c(1);
            obj.SL_strength_rep = obj.SL_strength_rep/c(3);
            obj.SL_strength_dep = obj.SL_strength_dep/c(3);

            % Frequency offset to do slice selection
            obj.SliceFreqOffset = obj.gam*obj.SL_strength_dep*POSSL*ENSL;
            rawData.auxiliar.freqOffsetSL = obj.SliceFreqOffset;
            
            nPoints = obj.NRO;
            nSlices = obj.NSL;
            nPhases = obj.NPH;
            
            % Get value to normalize fid to Volts
            bwCalNormVal = obj.calculate_bwCalNormVal;
            
%             obj.SetParameter('MAXPH',PH_strength_max);
%             obj.SetParameter('RO_STRENGTH',obj.RO_strength_rep);
%             obj.SetParameter('MAXSL',SL_strength_max);
            
            Data2D = [];
            obj.repetition = 0;
            obj.lastBatch = 0;
            iBatch = 0;
            tic;
            nFreq = 0;
            while(obj.lastBatch==0)
                tA = clock;
                iBatch = iBatch+1;
                if(iBatch==1)
                    fprintf('Radial batch %1.0f, estimating remaining time \n',iBatch);
                else
                    tC = tC*(nPhaseBatches-iBatch+1);
                    fprintf('Radial batch %1.0f/%1.0f, remaining time: %1.0f s \n',iBatch,nPhaseBatches,tC);
                end
                if(ENSP && (toc>300 || iBatch==1))
                    fprintf('Calibrating frequency... \n')
                    % Get central frequency
                    f0 = obj.GetFIDParameters;
                    obj.SetParameter('FREQUENCY',f0);
                    nFreq = nFreq+1;
                    rawData.aux.freqListRadial(nFreq) = f0;
                    fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
                    tic;
                end
                
                % NOW RUN ACTUAL ACQUISITION
                obj.CreateSequence(0);
                if(iBatch==1)
                    nPhaseBatches = ceil(nPhases/obj.PhasesPerBatch);
                end
                if(sum(obj.DELAYs<0)~=0)
                    warndlg('At least one delay is negative!','Delay Error')
                    return;
                end
                obj.Sequence2String;
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Batch aborted!','Warning')
                    return;
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2))/bwCalNormVal;
                Data2D = cat(1,Data2D,cDataBatch);
                
                tB = clock;
                tC = etime(tB,tA);
            end
                
            % k-space points
            kMax = 0.5./rawData.auxiliar.resolution;
            kPointsX = linspace(-kMax(1),kMax(1),nPoints);
            kPointsY = linspace(-kMax(2),kMax(2),nPhases);
            kPointsZ = linspace(-kMax(3),kMax(3),nSlices);
            [kPointsX,kPointsY,kPointsZ] = meshgrid(kPointsX,kPointsY,kPointsZ);
            kPointsX = permute(kPointsX,[2,1]);
            kPointsY = permute(kPointsY,[2,1]);
            kPointsZ = permute(kPointsZ,[2,1]);
            kSpaceValues = [kPointsX(:),kPointsY(:),kPointsZ(:)];
            phase = exp(-2*pi*1i*(DELRO*kPointsX(:)+DELPH*kPointsY(:)+DELSL*kPointsZ(:)));
            kSpaceValues = [kSpaceValues,Data2D(:).*phase];
            
            rawData.aux.kMax = kMax;
            rawData.kSpace.sampled = kSpaceValues;
            kSpaceValues = reshape(kSpaceValues(:,4),obj.NRO,obj.NPH,obj.NSL);
            imagen = abs(ifftshift(ifftn(kSpaceValues)));
            rawData.kSpace.imagen = imagen;
            
            % Save rawData
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
        
        
        %******************************************************************
        %******************************************************************
        %******************************************************************        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    end
end

