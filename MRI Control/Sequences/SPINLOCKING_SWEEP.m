% classdef SPINLOCKING_SWEEP < MRI_BlankSequence   
%     properties
%         maxRepetitionsPerBatch;
%         nPhasesBatch;
%         lastBatch;
%         RepetitionTime;
%         AcquisitionTime;
%         EndPhase;
%         PhasesPerBatch = 20;
%         gx;
%     end
%     methods (Access=private)
%         function CreateParameters(obj)
%             obj.CreateOneParameter('PULSELOCKINGTIME','Pulse locking time','us',0.5*MyUnits.ms);
%             obj.CreateOneParameter('GRADIENTSLICE','Gslice strength','T/m',0.05);
% %             CreateOneParameter(obj,'RFAMPLITUDE2','B1','kHz',5*MyUnits.kHz);
%             CreateOneParameter(obj,'RFAMPLITUDE2','RF amp SL','',0.01);
%             obj.CreateOneParameter('REPETITIONTIME','TR','ms',50*MyUnits.ms);
%             obj.CreateOneParameter('ACQTIME','Acq time (shorter T2)','us',2000*MyUnits.us);
% %             CreateOneParameter(obj,'PHASE1', 'Phase 1', '',0.0);
% %             CreateOneParameter(obj,'PHASE2', 'Phase 2', '',90.0);
% %             CreateOneParameter(obj,'PHASE3', 'Phase 3', '',180.0);
% %             CreateOneParameter(obj,'PHASE4', 'Phase 4', '',270.0);
%             CreateOneParameter(obj,'REPETITIONDELAY','Repetition delay','ms',1);
%             obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','TRANSIENTTIME','SHIMREADOUT','SPECTRALWIDTH'...
%                'COILRISETIME','SPOILERTIME','SPOILERAMP','NSLICES','BLANKINGDELAY','REPETITIONDELAY'};
%         end  
%     end
% 
%     methods
%         function obj = SPINLOCKING_SWEEP(program)
%             obj = obj@MRI_BlankSequence();
%             obj.SequenceName = 'SPINLOCKING SWEEP';
%             obj.ProgramName = 'MRI_BlankSequence3.exe';
%             if nargin > 0
%                 obj.ProgramName = program;
%             end
%             obj.CreateParameters();
%             obj.SetDefaultParameters();
%         end
%         
%         function SetDefaultParameters(obj)
%             SetDefaultParameters@MRI_BlankSequence(obj);
% %             obj.SetParameter('RFAMPLITUDE2',10);
%         end  
%          
%         function CreateSequence(obj)
%             
%             global scanner
%             
%             % If you want introduce B1 soft pulse in kHz instead of as adimensional, uncomment this block
% %             adim90B1 = obj.GetParameter('RFAMPLITUDE');
% %             tp = obj.GetParameter('PULSETIME');
% %             B190 = (pi/2)/(2*pi*42.56e6*tp); %T
% %             f90 = 42.56e6*B190*1e-3; %kHz
% %             convertB1 = f90/adim90B1; %kHz/adim
% %             obj.SetParameter('RFAMPLITUDE2',obj.GetParameter('RFAMPLITUDE2')*1e-3/convertB1);
%             
%             [shimS,shimP,shimR] = SelectAxes(obj);
%             c = obj.ReorganizeCalibrationData();
%             obj.nline_reads = 1;
%             step=obj.nSteps;
%             gz= obj.GetParameter('GRADIENTSLICE')/c(3);
%             gSpoiler=obj.GetParameter('GRADIENTIMAGING')/c(1);
%             acqTime = obj.GetParameter('ACQTIME')*1e6;
%             nPoints = obj.GetParameter('NPOINTS');
%             bw = (nPoints/acqTime)*1e3;
%             obj.SetParameter('SPECTRALWIDTH',bw*1e3);
%             pulseTime90 = obj.GetParameter('PULSETIME')*1e6;
%             pulselock = obj.GetParameter('PULSELOCKINGTIME')*1e6;
%             deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
%             TR = obj.GetParameter('REPETITIONTIME')*1e6;
%             blkDelay = scanner.BlankingDelay;
%             crt = obj.GetParameter('COILRISETIME')*1e6;
%             gradientDelay = scanner.grad_delay;
% 
%             phase0 = 0;
%             phase90 = 1;
%             phase180 = 2;
%             phase270 = 3;
%                         
%             RR = obj.GetParameter('READOUT');
%             PP = obj.GetParameter('PHASE');
%             SS = obj.GetParameter('SLICE');
%             
%             if PP==RR || PP==SS || RR==SS
%                 warndlg('Nono valid axis inputs','Axis Error')
%                 return;
%             end
%             
%             if SS=='x'
%                 dacslice=2; 
%             end
%             if SS=='y'
%                 dacslice=0; 
%             end
%             if SS=='z'
%                 dacslice=1;
%             end
% 
%             if RR=='x'
%                 dacred=2;
%             end
%             if RR=='y'
%                 dacred=0; 
%             end
%             if RR=='z'
%                 dacred=1; 
%             end
% 
%             if PP=='x'
%                 dacph=2; 
%             end
%             if PP=='y'
%                 dacph=0;
%             end
%             if PP=='z'
%                 dacph=1;  
%             end
%             
%             PP=dacph;   SS=dacslice;   RR=dacred;
%             
%             iIns = 1;
%             obj.nline_reads = 2;
%             [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs,obj.RFamps] = deal([]);
%             
%             for ii = 1:2
%                 %% Stablish shimming during all the sequence (3 ins)
%                 iIns = obj.PulseGradient (iIns,shimR,RR,0.1);
%                 iIns = obj.PulseGradient (iIns,shimP,PP,0.1);
%                 iIns = obj.PulseGradient (iIns,shimS,SS,0.1);
%                 
%                 %% Pulse the gradient z before the pulses (1 ins)
%                 iIns = obj.RampGradient(iIns,shimS,(shimS+gz)*(-1)^ii,SS,crt,scanner.nSteps);
%                 obj.DELAYs(iIns-1) = gradientDelay-blkDelay;
%                 
%                 %% Combination of hard 90X pulse followed without delay by a soft spin locking Y pulse and -X storaged pulse (3 ins)
%                 iIns = SetBlanking(obj,iIns,1,blkDelay);
%                 iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0);
% %                 iIns = CreateRawPulse(obj,iIns,phase180,pulseTime90,0); %TO TEST IF WE CAN REVERT PULSES
%                 iIns = CreateRawPulse(obj,iIns,phase90,pulselock,1);  %PHASELOCKING
% %                 
% %                 iIns = SetBlanking(obj,iIns,1,5);
% %                 iIns = CreateRawPulse(obj,iIns,phase180,pulseTime90,0);  %STORAGING
% %                 iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0); %FOR TEST
% %                 
% %                 % Switch off the slice gradient and switch on the imaging/spoiler gradient (2 ins)
% %                 iIns = obj.PulseGradient(iIns,(shimS),SS,0.1);
% %                 iIns = obj.PulseGradient(iIns,(shimR)+gSpoiler*(-1)^ii,PP,crt-blkDelay);
% %                 
% %                 % Hard 90X pulse to tip the storaged M (2 ins)
% %                 iIns = SetBlanking(obj,iIns,1,blkDelay);
% %                 iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0);
%                 
%                 %% Acquisition (2 ins)
%                 iIns = obj.RepetitionDelay(iIns,0,deadTime);
%                 iIns = obj.Acquisition(iIns,acqTime);
%                 
%                 %% Delay for recover T1 (1 ins)
%                 iIns = obj.RepetitionDelay (iIns,1,TR-acqTime-deadTime-1*crt-pulselock-pulseTime90);
%                 
%             end
%             obj.nInstructions = iIns-1;
%         end
%         
%         
%         function [status,result] = Run(obj,~)
%             clc
%             global rawData;
%             rawData = [];   
%             [~,~,~,~,~,spoilerEnable,~] = obj.ConfigureGradients(0);
%             tic
%             
%             % Hard pulse is define from adim RF amplitude and 90� pulse time, so assuming linear correlation we calculate the Khz to adim conversion factor
%             % (necessary inside Run function to write in rawData the RF amplitude as they should be
% %             adim90B1 = obj.GetParameter('RFAMPLITUDE');
% %             tp = obj.GetParameter('PULSETIME');
% %             B190 = (pi/2)/(2*pi*42.56e6*tp); %T
% %             f90 = 42.56e6*B190*1e-3; %kHz
% %             convertB1 = f90/adim90B1; %kHz/adim
% 
%             nii = 0;
%             signalMap = [];
%             profMap = [];
%             Nii = 60;
%             Njj = 1;
%             nIter = 0;
%             NIter = Nii*Njj;
%             for nii = 1:Nii+1
%                 rfamp2 = nii*0.001/3; 
%                 for njj = 1:1
%                     tic
%                     nIter = nIter+1;
%                     tsl = 500;
%                     
%                     obj.SetParameter('PULSELOCKINGTIME',tsl*1e-6);
%                     obj.SetParameter('RFAMPLITUDE2',rfamp2)
%                     
%                     if(spoilerEnable && (toc>60 || (nii==1 && njj==1)))
%                         tic
%                         % Get the central frequency
%                         fprintf('Calibrating frequency... \n')
%                         f0 = obj.GetFIDParameters;
%                         obj.SetParameter('FREQUENCY',f0);
%                         fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
%                     end
%                     
%                     nPoints = obj.GetParameter('NPOINTS'); gslice=obj.GetParameter('GRADIENTSLICE');pulseTime90 = obj.GetParameter('PULSETIME');pulselocking = obj.GetParameter('PULSELOCKINGTIME');deadTime = obj.GetParameter('TRANSIENTTIME');blkDelay = obj.GetParameter('BLANKINGDELAY');acqTime = obj.GetParameter('ACQTIME');bw = nPoints/acqTime;
%                     obj.SetParameter('SPECTRALWIDTH',bw*1e-3);
%                     
%                     rawData.inputs.RFampB1softAdim=obj.GetParameter('RFAMPLITUDE2');rawData.inputs.RFampB1hardAdim=obj.GetParameter('RFAMPLITUDE');rawData.inputs.gslice=gslice; rawData.inputs.blk=blkDelay;rawData.inputs.NEX = obj.GetParameter('NSCANS');rawData.inputs.nPoints = nPoints; rawData.inputs.B1hard_kHz = obj.GetParameter('RFAMPLITUDE');rawData.inputs.B1soft_kHz = obj.GetParameter('RFAMPLITUDE2')*1e-3; rawData.inputs.pulseTime90 = pulseTime90; rawData.inputs.pulselockingTime = pulselocking;rawData.inputs.deadTime = deadTime; rawData.inputs.TR = obj.GetParameter('REPETITIONTIME');rawData.inputs.acqTime = acqTime;
%                     obj.nInstructions=21;
%                     obj.maxRepetitionsPerBatch = floor(2048/obj.nInstructions);
%                     
%                     % Get k-points
%                     tp = obj.GetParameter('PULSETIME');
%                     td = obj.GetParameter('TRANSIENTTIME');
%                     ts = 1/obj.GetParameter('SPECTRALWIDTH');
%                     acqTime = obj.GetParameter('ACQTIME');
%                     nPoints = obj.GetParameter('NPOINTS');
%                     tv = linspace(tp/2+td+ts/2,tp/2+td+ts/2+acqTime,nPoints);
%                     kv1 = 42.6d6*obj.GetParameter('GRADIENTSLICE')*tv;
%                     kv2 = -kv1(end:-1:1);
%                     kv = [kv2,kv1];
%                     tv = [-tv(end:-1:1),tv];
%                     obj.SetParameter('REPETITIONDELAY',obj.GetParameter('REPETITIONTIME'));
%                     if(obj.GetParameter('GRADIENTSLICE')==0)
%                         FOV = 1/ts;
%                     else
%                         FOV = abs(1/(kv(2)-kv(1)));  %FOV=1/deltaK
%                     end
%                     rawData.inputs.FOV=FOV;                    
%                     
%                     obj.CreateSequence;
%                     obj.Sequence2String;
%                     fname = 'TempBatch.bat';
%                     obj.WriteBatchFile(fname);
%                     [status,result] = system(fullfile(obj.path_file,fname));
%                     if status == 0 && ~isempty(result)
%                         C = textscan(result, '%s', 'delimiter', sprintf('\f'));
%                         res = C{:};
%                         obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
%                         obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz'));
%                         obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms);
%                         obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
%                         obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
%                     else
%                         warndlg('Batch aborted!','Warning')
%                         return;
%                     end
%                     
%                     bwCalNormVal = obj.calculate_bwCalNormVal;
%                     
%                     DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
%                     cDataBatch=complex(DataBatch(:,1),DataBatch(:,2));
%                     
%                     % Reorganize signal
%                     signal = cDataBatch./bwCalNormVal;
%                     signal(1:nPoints) = signal(nPoints:-1:1);
%                     signalMap = cat(2,signalMap,abs(signal));
%                     
% %                     rawData.outputs.signalMap(nii,njj) = abs(signal(end/2));
%                     rawData.outputs.signalMap = signalMap;
%                     rawData.outputs.signal = [tv',signal];
%                     rawData.outputs.kValues = kv';
%                     
%                     %Which tecnique you want to reconstruct the signal?
%                     rawData.outputs.profileART(:,2) = art_MRI_CPU_1D(1,1,FOV,length(kv),kv,rawData.outputs.signal);
%                     rawData.outputs.profileFFT(:,2) = abs(ifftshift(ifftn(signal)));
%                     
%                     
%                     rawData.outputs.profile(:,1) = linspace(-FOV/2,FOV/2,2*nPoints);
%                     profMap = cat(2,profMap,rawData.outputs.profile(:,2));
%                     rawData.outputs.profMap = profMap;
%                     
%                     time = clock;
%                     name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
%                         num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
%                     dT = toc;
%                     disp(strcat('Remaining time: ',num2str(dT*(NIter-nIter)),' s'))
%                 end
%             end
%             save(fullfile(obj.path_file,name),'rawData');
%         end
%     end
% end
% 
% 
% 








classdef SPINLOCKING_SWEEP < MRI_BlankSequence   
    properties
        maxRepetitionsPerBatch;
        nPhasesBatch;
        lastBatch;
        RepetitionTime;
        AcquisitionTime;
        EndPhase;
        PhasesPerBatch = 20;
        RFampSL;
        spinlockingtime
    end
    methods (Access=private)
        function CreateParameters(obj)
            obj.CreateOneParameter('PULSELOCKINGTIMEFINAL','FINAL Pulse locking time','us',0.5*MyUnits.ms);
            obj.CreateOneParameter('PULSELOCKINGTIMEINITIAL','INITIAL Pulse locking time','us',0.1*MyUnits.ms);
            CreateOneParameter(obj,'STEPSPULSELOCKINGTIME','Pulse locking time Steps','',50);
            obj.CreateOneParameter('GRADIENTSLICE','Gslice strength','T/m',0);
            CreateOneParameter(obj,'RFAMPLITUDE2','RF amp SL','',0.01);
            CreateOneParameter(obj,'MAXB1','B1 max','ua',0.05);
            CreateOneParameter(obj,'MINB1','B1 min','ua',0.01);
            CreateOneParameter(obj,'STEPS','B1 Steps','',30);
            obj.CreateOneParameter('REPETITIONTIME','TR','ms',50*MyUnits.ms);
            obj.CreateOneParameter('ACQTIME','Acq time (shorter T2)','us',2000*MyUnits.us);
            CreateOneParameter(obj,'PHASE1', 'Phase 1', '',0.0);
            CreateOneParameter(obj,'PHASE2', 'Phase 2', '',90.0);
            CreateOneParameter(obj,'PHASE3', 'Phase 3', '',180.0);
            CreateOneParameter(obj,'PHASE4', 'Phase 4', '',270.0);
            CreateOneParameter(obj,'REPETITIONDELAY','Repetition delay','ms',1);
            CreateOneParameter(obj,'SLICEPOSITION','Slice position','mm',0*MyUnits.mm);
            obj.InputParHidden = {'GRADIENTSLICE','RFAMPLITUDE2','SHIMSLICE','SHIMPHASE','TRANSIENTTIME','SHIMREADOUT','SPECTRALWIDTH'...
               'COILRISETIME','SPOILERTIME','SPOILERAMP','NSLICES','BLANKINGDELAY','REPETITIONDELAY'};
        end  
    end

    methods
        function obj = SPINLOCKING_SWEEP(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'SPINLOCKING SWEEP';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
%             obj.SetParameter('RFAMPLITUDE2',10);
        end  
         
        function CreateSequence(obj)
            
            global scanner
            
            % If you want introduce B1 soft pulse in kHz instead of as adimensional, uncomment this block
%             adim90B1 = obj.GetParameter('RFAMPLITUDE');
%             tp = obj.GetParameter('PULSETIME');
%             B190 = (pi/2)/(2*pi*42.56e6*tp); %T
%             f90 = 42.56e6*B190*1e-3; %kHz
%             convertB1 = f90/adim90B1; %kHz/adim
%             obj.SetParameter('RFAMPLITUDE2',obj.GetParameter('RFAMPLITUDE2')*1e-3/convertB1);

            obj.SetParameter('RFAMPLITUDE2',obj.RFampSL);
            [shimS,shimP,shimR] = SelectAxes(obj);
            c = obj.ReorganizeCalibrationData();
            acqTime = obj.GetParameter('ACQTIME')*1e6;
            nPoints = obj.GetParameter('NPOINTS');
            bw = (nPoints/acqTime)*1e3;
            obj.SetParameter('SPECTRALWIDTH',bw*1e3);
            pulseTime90 = obj.GetParameter('PULSETIME')*1e6;
            deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
            TR = obj.GetParameter('REPETITIONTIME')*1e6;
            blkDelay = scanner.BlankingDelay;
            crt = obj.GetParameter('COILRISETIME')*1e6;
            phase0 = 0;
            phase90 = 1;
            phase180 = 2;
            phase270 = 3;
            RR = obj.GetParameter('READOUT');
            PP = obj.GetParameter('PHASE');
            SS = obj.GetParameter('SLICE');
            
            if PP==RR || PP==SS || RR==SS
                warndlg('Non valid axis inputs','Axis Error')
                return;
            end
            
            if SS=='x'
                dacslice=2; 
            end
            if SS=='y'
                dacslice=0; 
            end
            if SS=='z'
                dacslice=1;
            end

            if RR=='x'
                dacred=2;
            end
            if RR=='y'
                dacred=0; 
            end
            if RR=='z'
                dacred=1; 
            end

            if PP=='x'
                dacph=2; 
            end
            if PP=='y'
                dacph=0;
            end
            if PP=='z'
                dacph=1;  
            end
            
            PP=dacph;   SS=dacslice;   RR=dacred;
            
            iIns = 1;
            obj.nline_reads = 1;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs,obj.RFamps] = deal([]);
           
 
            %% SHIMMING in all gradients (3 instructions)
            iIns = obj.PulseGradient(iIns,shimR,RR,0.1);
            iIns = obj.PulseGradient(iIns,shimP,PP,0.1);
            iIns = obj.PulseGradient(iIns,shimS,SS,0.1);
            
            %% Combination of hard 90X pulse followed without delay by a soft spin locking Y pulse and -X storaged pulse (3 ins)
            iIns = SetBlanking(obj,iIns,1,blkDelay);
            iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0); %EXCITATION
            obj.FREQs(iIns-1) = 0;
            iIns = CreateRawPulse(obj,iIns,phase90,(obj.spinlockingtime)*1e6,1);  %PHASELOCKING
%             iIns = CreateRawPulse(obj,iIns,phase270,(obj.spinlockingtime/2)*1e6,1);
%             iIns = CreateRawPulse(obj,iIns,phase180,pulseTime90,0); %STORAGE PULSE
%             iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0); %REEXCITATION
            obj.FREQs(iIns-1) = 1;
            
            %% Acquisition (2 ins)
            iIns = obj.RepetitionDelay(iIns,0,deadTime);
            iIns = obj.Acquisition(iIns,acqTime);
            
            %% Delay for recover T1 (1 ins)
            iIns = obj.RepetitionDelay (iIns,1,TR-acqTime-deadTime-1*crt-obj.spinlockingtime*1e6-pulseTime90);
            
            obj.nInstructions = iIns-1;
        end
        
        
        function [status,result] = Run(obj,~)
            clc
            global rawData;
            rawData = [];   
            [~,~,~,~,~,spoilerEnable,~] = obj.ConfigureGradients(0);
            tic
            
            % Hard pulse is define from adim RF amplitude and 90� pulse time, so assuming linear correlation we calculate the Khz to adim conversion factor
            % (necessary inside Run function to write in rawData the RF amplitude as they should be
%             adim90B1 = obj.GetParameter('RFAMPLITUDE');
%             tp = obj.GetParameter('PULSETIME');
%             B190 = (pi/2)/(2*pi*42.56e6*tp); %T
%             f90 = 42.56e6*B190*1e-3; %kHz
%             convertB1 = f90/adim90B1; %kHz/adim

            Nii = obj.GetParameter('STEPS');   %Number of steps for B1 SL sweep
            minB1 = obj.GetParameter('MINB1'); %adim
            maxB1 = obj.GetParameter('MAXB1'); %adim
            B1SL = linspace(minB1,maxB1,Nii);  %adim
            rawData.inputs.B1List = B1SL;
            rawData.inputs.minB1 = minB1;
            rawData.inputs.maxB1 = maxB1;
            
            timeslinitial= obj.GetParameter('PULSELOCKINGTIMEINITIAL');
            timeslfinal= obj.GetParameter('PULSELOCKINGTIMEFINAL');
            stepsSL= obj.GetParameter('STEPSPULSELOCKINGTIME');
            timeSL = linspace(timeslinitial,timeslfinal,stepsSL); %s
            rawData.inputs.timeSL = timeSL;
            rawData.inputs.timeSLmax = timeslfinal;
            rawData.inputs.timeSLmin = timeslinitial;
            rawData.inputs.SeqName = obj.SequenceName;
            
            nPoints = obj.GetParameter('NPOINTS'); gslice=obj.GetParameter('GRADIENTSLICE');pulseTime90 = obj.GetParameter('PULSETIME');deadTime = obj.GetParameter('TRANSIENTTIME');blkDelay = obj.GetParameter('BLANKINGDELAY');acqTime = obj.GetParameter('ACQTIME');bw = nPoints/acqTime;
            slPosi = obj.GetParameter('SLICEPOSITION');
            obj.SliceFreqOffset = 42.58d6*gslice*slPosi;
            obj.SetParameter('SPECTRALWIDTH',bw*1e-3);
            rawData.inputs.timeFID = linspace(deadTime,acqTime,nPoints );
            rawData.inputs.RFampB1hardAdim=obj.GetParameter('RFAMPLITUDE');rawData.inputs.gslice=gslice; rawData.inputs.blk=blkDelay;rawData.inputs.NEX = obj.GetParameter('NSCANS');rawData.inputs.nPoints = nPoints; rawData.inputs.B1hard_kHz = obj.GetParameter('RFAMPLITUDE');rawData.inputs.B1soft_kHz = obj.GetParameter('RFAMPLITUDE2')*1e-3; rawData.inputs.pulseTime90 = pulseTime90;rawData.inputs.deadTime = deadTime; rawData.inputs.TR = obj.GetParameter('REPETITIONTIME');rawData.inputs.acqTime = acqTime;
            obj.nInstructions=9;
            obj.maxRepetitionsPerBatch = floor(2048/obj.nInstructions);
            rawData.inputs.slicePosition = slPosi;
            rawData.inputs.freqOffset = obj.SliceFreqOffset;

            signalMap = zeros(Nii,stepsSL);
            fidMap = zeros(Nii,stepsSL,nPoints);


            
            for nii = 1:Nii
                obj.RFampSL=B1SL(nii);
                fprintf('B1SL= %1.4f \n',obj.RFampSL);
                    if(spoilerEnable)
                        tic
                        % Get the central frequency
                        fprintf('Calibrating frequency... \n')
                        f0 = obj.GetFIDParameters;
                        obj.SetParameter('FREQUENCY',f0);
                        fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
                    end
            
                for njj = 1:stepsSL
                                      
                    if(spoilerEnable && mod(njj,20)==0)
                        tic
                        % Get the central frequency
                        fprintf('Calibrating frequency... \n')
                        f0 = obj.GetFIDParameters;
                        obj.SetParameter('FREQUENCY',f0);
                        fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
                    end
                    obj.spinlockingtime=timeSL(njj);
%                     fprintf('B1SL= %1.4f, SLtime= %1.4f ms \n',obj.RFampSL,obj.spinlockingtime*1000);
 
                    obj.CreateSequence;
                    obj.Sequence2String;
                    fname = 'TempBatch.bat';
                    obj.WriteBatchFile(fname);
                                pause(0.1)
                    [status,result] = system(fullfile(obj.path_file,fname));
                                pause(0.1)
                    if status == 0 && ~isempty(result)
                        C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                        res = C{:};
                        obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                        obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz'));
                        obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms);
                        obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                        obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                    else
                        warndlg('Batch aborted!','Warning')
                        return;
                    end
                    bwCalNormVal = obj.calculate_bwCalNormVal;
                    DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                    cDataBatch=complex(DataBatch(:,1),DataBatch(:,2));
                    signal = cDataBatch./bwCalNormVal;
                    signalMap(nii,njj) = (signal(1));
                    fidMap(nii,njj,:) = signal;
                end
            end

            rawData.outputs.signalMap = signalMap;
            rawData.outputs.fidMap = fidMap;
            
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
    end
end





























% THIS CLASSDEF IS FOR SWEEP THE SPIN LOCKING TIME FOR DIFFERENT B1 TO OBTAIN T1RHO
% THIS CODE HAS BEEN USED DURING 03/09
% classdef SPINLOCKING_SWEEP < MRI_BlankSequence   
%     properties
%         maxRepetitionsPerBatch;
%         nPhasesBatch;
%         lastBatch;
%         RepetitionTime;
%         AcquisitionTime;
%         EndPhase;
%         PhasesPerBatch = 20;
%         RFampSL;
%         spinlockingtime
%     end
%     methods (Access=private)
%         function CreateParameters(obj)
%             obj.CreateOneParameter('PULSELOCKINGTIMEFINAL','FINAL Pulse locking time','us',0.5*MyUnits.ms);
%             obj.CreateOneParameter('PULSELOCKINGTIMEINITIAL','INITIAL Pulse locking time','us',0.1*MyUnits.ms);
%             CreateOneParameter(obj,'STEPSPULSELOCKINGTIME','Pulse locking time Steps','',50);
%             obj.CreateOneParameter('GRADIENTSLICE','Gslice strength','T/m',0);
%             CreateOneParameter(obj,'RFAMPLITUDE2','RF amp SL','',0.01);
%             CreateOneParameter(obj,'MAXB1','B1 max','ua',0.05);
%             CreateOneParameter(obj,'MINB1','B1 min','ua',0.01);
%             CreateOneParameter(obj,'STEPS','B1 Steps','',30);
%             obj.CreateOneParameter('REPETITIONTIME','TR','ms',50*MyUnits.ms);
%             obj.CreateOneParameter('ACQTIME','Acq time (shorter T2)','us',2000*MyUnits.us);
%             CreateOneParameter(obj,'PHASE1', 'Phase 1', '',0.0);
%             CreateOneParameter(obj,'PHASE2', 'Phase 2', '',90.0);
%             CreateOneParameter(obj,'PHASE3', 'Phase 3', '',180.0);
%             CreateOneParameter(obj,'PHASE4', 'Phase 4', '',270.0);
%             CreateOneParameter(obj,'REPETITIONDELAY','Repetition delay','ms',1);
%             CreateOneParameter(obj,'SLICEPOSITION','Slice position','mm',0*MyUnits.mm);
%             obj.InputParHidden = {'GRADIENTSLICE','RFAMPLITUDE2','SHIMSLICE','SHIMPHASE','TRANSIENTTIME','SHIMREADOUT','SPECTRALWIDTH'...
%                'COILRISETIME','SPOILERTIME','SPOILERAMP','NSLICES','BLANKINGDELAY','REPETITIONDELAY'};
%         end  
%     end
% 
%     methods
%         function obj = SPINLOCKING_SWEEP(program)
%             obj = obj@MRI_BlankSequence();
%             obj.SequenceName = 'SPINLOCKING SWEEP';
%             obj.ProgramName = 'MRI_BlankSequence3.exe';
%             if nargin > 0
%                 obj.ProgramName = program;
%             end
%             obj.CreateParameters();
%             obj.SetDefaultParameters();
%         end
%         
%         function SetDefaultParameters(obj)
%             SetDefaultParameters@MRI_BlankSequence(obj);
% %             obj.SetParameter('RFAMPLITUDE2',10);
%         end  
%          
%         function CreateSequence(obj)
%             
%             global scanner
%             
%             % If you want introduce B1 soft pulse in kHz instead of as adimensional, uncomment this block
% %             adim90B1 = obj.GetParameter('RFAMPLITUDE');
% %             tp = obj.GetParameter('PULSETIME');
% %             B190 = (pi/2)/(2*pi*42.56e6*tp); %T
% %             f90 = 42.56e6*B190*1e-3; %kHz
% %             convertB1 = f90/adim90B1; %kHz/adim
% %             obj.SetParameter('RFAMPLITUDE2',obj.GetParameter('RFAMPLITUDE2')*1e-3/convertB1);
% 
%             obj.SetParameter('RFAMPLITUDE2',obj.RFampSL);
%             [shimS,shimP,shimR] = SelectAxes(obj);
%             c = obj.ReorganizeCalibrationData();
%             acqTime = obj.GetParameter('ACQTIME')*1e6;
%             nPoints = obj.GetParameter('NPOINTS');
%             bw = (nPoints/acqTime)*1e3;
%             obj.SetParameter('SPECTRALWIDTH',bw*1e3);
%             pulseTime90 = obj.GetParameter('PULSETIME')*1e6;
%             deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
%             TR = obj.GetParameter('REPETITIONTIME')*1e6;
%             blkDelay = scanner.BlankingDelay;
%             crt = obj.GetParameter('COILRISETIME')*1e6;
%             phase0 = 0;
%             phase90 = 1;
%             phase180 = 2;
%             phase270 = 3;
%             RR = obj.GetParameter('READOUT');
%             PP = obj.GetParameter('PHASE');
%             SS = obj.GetParameter('SLICE');
%             
%             if PP==RR || PP==SS || RR==SS
%                 warndlg('Non valid axis inputs','Axis Error')
%                 return;
%             end
%             
%             if SS=='x'
%                 dacslice=2; 
%             end
%             if SS=='y'
%                 dacslice=0; 
%             end
%             if SS=='z'
%                 dacslice=1;
%             end
% 
%             if RR=='x'
%                 dacred=2;
%             end
%             if RR=='y'
%                 dacred=0; 
%             end
%             if RR=='z'
%                 dacred=1; 
%             end
% 
%             if PP=='x'
%                 dacph=2; 
%             end
%             if PP=='y'
%                 dacph=0;
%             end
%             if PP=='z'
%                 dacph=1;  
%             end
%             
%             PP=dacph;   SS=dacslice;   RR=dacred;
%             
%             iIns = 1;
%             obj.nline_reads = 1;
%             [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs,obj.RFamps] = deal([]);
%             
%  
%                 %% Stablish shimming during all the sequence (3 ins)
%                 iIns = obj.PulseGradient (iIns,shimR,RR,0.1);
%                 iIns = obj.PulseGradient (iIns,shimP,PP,0.1);
%                 iIns = obj.PulseGradient (iIns,shimS,SS,0.1);
%                 
%                 %% Combination of hard 90X pulse followed without delay by a soft spin locking Y pulse and -X storaged pulse (3 ins)
%                 iIns = SetBlanking(obj,iIns,1,blkDelay);
%                 iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0); %EXCITATION
%                 obj.FREQs(iIns-1) = 0;
%                 iIns = CreateRawPulse(obj,iIns,phase90,(obj.spinlockingtime)*1e6,1);  %PHASELOCKING
% %                 iIns = CreateRawPulse(obj,iIns,phase270,(obj.spinlockingtime/2)*1e6,1);
% %                 iIns = CreateRawPulse(obj,iIns,phase180,pulseTime90,0); %STORAGE PULSE
% %                 iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0); %REEXCITATION
%                 obj.FREQs(iIns-1) = 1;
%                 
%                 %% Acquisition (2 ins)
%                 iIns = obj.RepetitionDelay(iIns,0,deadTime);
%                 iIns = obj.Acquisition(iIns,acqTime);
%                 
%                 %% Delay for recover T1 (1 ins)
%                 iIns = obj.RepetitionDelay (iIns,1,TR-acqTime-deadTime-1*crt-obj.spinlockingtime*1e6-pulseTime90);
%                 
%             obj.nInstructions = iIns-1;
%         end
%         
%         
%         function [status,result] = Run(obj,~)
%             clc
%             global rawData;
%             rawData = [];   
%             [~,~,~,~,~,spoilerEnable,~] = obj.ConfigureGradients(0);
%             tic
%             
%             % Hard pulse is define from adim RF amplitude and 90� pulse time, so assuming linear correlation we calculate the Khz to adim conversion factor
%             % (necessary inside Run function to write in rawData the RF amplitude as they should be
% %             adim90B1 = obj.GetParameter('RFAMPLITUDE');
% %             tp = obj.GetParameter('PULSETIME');
% %             B190 = (pi/2)/(2*pi*42.56e6*tp); %T
% %             f90 = 42.56e6*B190*1e-3; %kHz
% %             convertB1 = f90/adim90B1; %kHz/adim
% 
%             Nii = obj.GetParameter('STEPS');   %Number of steps for B1 SL sweep
%             minB1 = obj.GetParameter('MINB1'); %adim
%             maxB1 = obj.GetParameter('MAXB1'); %adim
%             B1SL = linspace(minB1,maxB1,Nii);  %adim
%             rawData.inputs.B1List = B1SL;
%             rawData.inputs.minB1 = minB1;
%             rawData.inputs.maxB1 = maxB1;
%             
%             timeslinitial= obj.GetParameter('PULSELOCKINGTIMEINITIAL');
%             timeslfinal= obj.GetParameter('PULSELOCKINGTIMEFINAL');
%             stepsSL= obj.GetParameter('STEPSPULSELOCKINGTIME');
%             timeSL = linspace(timeslinitial,timeslfinal,stepsSL); %s
%             rawData.inputs.timeSL = timeSL;
%             rawData.inputs.timeSLmax = timeslfinal;
%             rawData.inputs.timeSLmin = timeslinitial;
%             
%             nPoints = obj.GetParameter('NPOINTS'); gslice=obj.GetParameter('GRADIENTSLICE');pulseTime90 = obj.GetParameter('PULSETIME');deadTime = obj.GetParameter('TRANSIENTTIME');blkDelay = obj.GetParameter('BLANKINGDELAY');acqTime = obj.GetParameter('ACQTIME');bw = nPoints/acqTime;
%             slPosi = obj.GetParameter('SLICEPOSITION');
%             obj.SliceFreqOffset = 42.58d6*gslice*slPosi;
%             obj.SetParameter('SPECTRALWIDTH',bw*1e-3);
%             rawData.inputs.RFampB1hardAdim=obj.GetParameter('RFAMPLITUDE');rawData.inputs.gslice=gslice; rawData.inputs.blk=blkDelay;rawData.inputs.NEX = obj.GetParameter('NSCANS');rawData.inputs.nPoints = nPoints; rawData.inputs.B1hard_kHz = obj.GetParameter('RFAMPLITUDE');rawData.inputs.B1soft_kHz = obj.GetParameter('RFAMPLITUDE2')*1e-3; rawData.inputs.pulseTime90 = pulseTime90;rawData.inputs.deadTime = deadTime; rawData.inputs.TR = obj.GetParameter('REPETITIONTIME');rawData.inputs.acqTime = acqTime;
%             obj.nInstructions=9;
%             obj.maxRepetitionsPerBatch = floor(2048/obj.nInstructions);
%             rawData.inputs.slicePosition = slPosi;
%             rawData.inputs.freqOffset = obj.SliceFreqOffset;
% 
%             signalMap = zeros(Nii,stepsSL);
%             
% 
%             
%             for nii = 1:Nii
%                 obj.RFampSL=B1SL(nii);
%                 
%                 if(spoilerEnable)
%                     tic
%                     % Get the central frequency
%                     fprintf('Calibrating frequency... \n')
%                     f0 = obj.GetFIDParameters;
%                     obj.SetParameter('FREQUENCY',f0);
%                     fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
%                 end
%                 
%                 for njj = 1:stepsSL
%                     obj.spinlockingtime=timeSL(njj);
%                     obj.CreateSequence;
%                     obj.Sequence2String;
%                     fname = 'TempBatch.bat';
%                     obj.WriteBatchFile(fname);
%                     [status,result] = system(fullfile(obj.path_file,fname));
%                     if status == 0 && ~isempty(result)
%                         C = textscan(result, '%s', 'delimiter', sprintf('\f'));
%                         res = C{:};
%                         obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
%                         obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz'));
%                         obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms);
%                         obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
%                         obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
%                     else
%                         warndlg('Batch aborted!','Warning')
%                         return;
%                     end
%                     bwCalNormVal = obj.calculate_bwCalNormVal;
%                     DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
%                     cDataBatch=complex(DataBatch(:,1),DataBatch(:,2));
%                     signal = cDataBatch./bwCalNormVal;
%                     signalMap(nii,njj) = (signal(1));
%                 end
%             end
% 
%             rawData.outputs.signalMap = signalMap;
%             
%             time = clock;
%             name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
%                 num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
%             save(fullfile(obj.path_file,name),'rawData');
%         end
%     end
% end













% THIS CLASSDEF IS FOR SWEEP THE B1 VALUES FOR FIXED GRADIENT AND SL TIME
% classdef SPINLOCKING_SWEEP < MRI_BlankSequence   
%     properties
%         maxRepetitionsPerBatch;
%         nPhasesBatch;
%         lastBatch;
%         RepetitionTime;
%         AcquisitionTime;
%         EndPhase;
%         PhasesPerBatch = 20;
%         gx;
%         RFampSL;
%     end
%     methods (Access=private)
%         function CreateParameters(obj)
%             obj.CreateOneParameter('PULSELOCKINGTIME','Pulse locking time','us',0.5*MyUnits.ms);
%             obj.CreateOneParameter('GRADIENTSLICE','Gslice strength','T/m',0.05);
%             CreateOneParameter(obj,'RFAMPLITUDE2','RF amp SL','',0.01);
%             CreateOneParameter(obj,'MAXB1','B1 max','ua',0.05);
%             CreateOneParameter(obj,'MINB1','B1 min','ua',0.01);
%             CreateOneParameter(obj,'STEPS','B1 Steps','',30);
%             obj.CreateOneParameter('REPETITIONTIME','TR','ms',50*MyUnits.ms);
%             obj.CreateOneParameter('ACQTIME','Acq time (shorter T2)','us',2000*MyUnits.us);
%             CreateOneParameter(obj,'PHASE1', 'Phase 1', '',0.0);
%             CreateOneParameter(obj,'PHASE2', 'Phase 2', '',90.0);
% %             CreateOneParameter(obj,'PHASE3', 'Phase 3', '',180.0);
% %             CreateOneParameter(obj,'PHASE4', 'Phase 4', '',270.0);
%             CreateOneParameter(obj,'REPETITIONDELAY','Repetition delay','ms',1);
%             CreateOneParameter(obj,'SLICEPOSITION','Slice position','mm',0*MyUnits.mm);
%             obj.InputParHidden = {'RFAMPLITUDE2','SHIMSLICE','SHIMPHASE','TRANSIENTTIME','SHIMREADOUT','SPECTRALWIDTH'...
%                'COILRISETIME','SPOILERTIME','SPOILERAMP','NSLICES','BLANKINGDELAY','REPETITIONDELAY'};
%         end  
%     end
% 
%     methods
%         function obj = SPINLOCKING_SWEEP(program)
%             obj = obj@MRI_BlankSequence();
%             obj.SequenceName = 'SPINLOCKING SWEEP';
%             obj.ProgramName = 'MRI_BlankSequence3.exe';
%             if nargin > 0
%                 obj.ProgramName = program;
%             end
%             obj.CreateParameters();
%             obj.SetDefaultParameters();
%         end
%         
%         function SetDefaultParameters(obj)
%             SetDefaultParameters@MRI_BlankSequence(obj);
% %             obj.SetParameter('RFAMPLITUDE2',10);
%         end  
%          
%         function CreateSequence(obj)
%             
%             global scanner
%             
%             % If you want introduce B1 soft pulse in kHz instead of as adimensional, uncomment this block
% %             adim90B1 = obj.GetParameter('RFAMPLITUDE');
% %             tp = obj.GetParameter('PULSETIME');
% %             B190 = (pi/2)/(2*pi*42.56e6*tp); %T
% %             f90 = 42.56e6*B190*1e-3; %kHz
% %             convertB1 = f90/adim90B1; %kHz/adim
% %             obj.SetParameter('RFAMPLITUDE2',obj.GetParameter('RFAMPLITUDE2')*1e-3/convertB1);
% 
%             obj.SetParameter('RFAMPLITUDE2',obj.RFampSL);
%             [shimS,shimP,shimR] = SelectAxes(obj);
%             c = obj.ReorganizeCalibrationData();
%             step=obj.nSteps;
%             gz= obj.GetParameter('GRADIENTSLICE')/c(3);
%             gSpoiler=obj.GetParameter('GRADIENTIMAGING')/c(1);
%             acqTime = obj.GetParameter('ACQTIME')*1e6;
%             nPoints = obj.GetParameter('NPOINTS');
%             bw = (nPoints/acqTime)*1e3;
%             obj.SetParameter('SPECTRALWIDTH',bw*1e3);
%             pulseTime90 = obj.GetParameter('PULSETIME')*1e6;
%             pulselock = obj.GetParameter('PULSELOCKINGTIME')*1e6;
%             deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
%             TR = obj.GetParameter('REPETITIONTIME')*1e6;
%             blkDelay = scanner.BlankingDelay;
%             crt = obj.GetParameter('COILRISETIME')*1e6;
%             gradientDelay = scanner.grad_delay;
% 
%             phase0 = 0;
%             phase90 = 1;
%             phase180 = 2;
%             phase270 = 3;
%                         
%             RR = obj.GetParameter('READOUT');
%             PP = obj.GetParameter('PHASE');
%             SS = obj.GetParameter('SLICE');
%             
%             if PP==RR || PP==SS || RR==SS
%                 warndlg('Non valid axis inputs','Axis Error')
%                 return;
%             end
%             
%             if SS=='x'
%                 dacslice=2; 
%             end
%             if SS=='y'
%                 dacslice=0; 
%             end
%             if SS=='z'
%                 dacslice=1;
%             end
% 
%             if RR=='x'
%                 dacred=2;
%             end
%             if RR=='y'
%                 dacred=0; 
%             end
%             if RR=='z'
%                 dacred=1; 
%             end
% 
%             if PP=='x'
%                 dacph=2; 
%             end
%             if PP=='y'
%                 dacph=0;
%             end
%             if PP=='z'
%                 dacph=1;  
%             end
%             
%             PP=dacph;   SS=dacslice;   RR=dacred;
%             
%             iIns = 1;
%             obj.nline_reads = 2;
%             [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs,obj.RFamps] = deal([]);
%             
%             for ii = 1:2
%                 %% Stablish shimming during all the sequence (3 ins)
%                 iIns = obj.PulseGradient (iIns,shimR,RR,0.1);
%                 iIns = obj.PulseGradient (iIns,shimP,PP,0.1);
%                 iIns = obj.PulseGradient (iIns,shimS,SS,0.1);
%                 
%                 %% Pulse the gradient z before the pulses (1 ins)
%                 iIns = obj.RampGradient(iIns,shimS,(shimS+gz)*(-1)^ii,SS,crt,scanner.nSteps);
%                 obj.DELAYs(iIns-1) = gradientDelay-blkDelay;
%                 
%                 %% Combination of hard 90X pulse followed without delay by a soft spin locking Y pulse and -X storaged pulse (3 ins)
%                 iIns = SetBlanking(obj,iIns,1,blkDelay);
%                 iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0);
%                 obj.FREQs(iIns-1) = 0;
% %                 iIns = CreateRawPulse(obj,iIns,phase180,pulseTime90,0); %TO TEST IF WE CAN REVERT PULSES
%                 iIns = CreateRawPulse(obj,iIns,phase90,pulselock,1);  %PHASELOCKING
%                 obj.FREQs(iIns-1) = 1;
% %                 
% %                 iIns = SetBlanking(obj,iIns,1,5);
% %                 iIns = CreateRawPulse(obj,iIns,phase180,pulseTime90,0);  %STORAGING
% %                 iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0); %FOR TEST
% %                 
% %                 % Switch off the slice gradient and switch on the imaging/spoiler gradient (2 ins)
% %                 iIns = obj.PulseGradient(iIns,(shimS),SS,0.1);
% %                 iIns = obj.PulseGradient(iIns,(shimR)+gSpoiler*(-1)^ii,PP,crt-blkDelay);
% %                 
% %                 % Hard 90X pulse to tip the storaged M (2 ins)
% %                 iIns = SetBlanking(obj,iIns,1,blkDelay);
% %                 iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0);
%                 
%                 %% Acquisition (2 ins)
%                 iIns = obj.RepetitionDelay(iIns,0,deadTime);
%                 iIns = obj.Acquisition(iIns,acqTime);
%                 
%                 %% Delay for recover T1 (1 ins)
%                 iIns = obj.RepetitionDelay (iIns,1,TR-acqTime-deadTime-1*crt-pulselock-pulseTime90);
%                 
%             end
%             obj.nInstructions = iIns-1;
%         end
%         
%         
%         function [status,result] = Run(obj,~)
%             clc
%             global rawData;
%             rawData = [];   
%             [~,~,~,~,~,spoilerEnable,~] = obj.ConfigureGradients(0);
%             tic
%             
%             % Hard pulse is define from adim RF amplitude and 90� pulse time, so assuming linear correlation we calculate the Khz to adim conversion factor
%             % (necessary inside Run function to write in rawData the RF amplitude as they should be
% %             adim90B1 = obj.GetParameter('RFAMPLITUDE');
% %             tp = obj.GetParameter('PULSETIME');
% %             B190 = (pi/2)/(2*pi*42.56e6*tp); %T
% %             f90 = 42.56e6*B190*1e-3; %kHz
% %             convertB1 = f90/adim90B1; %kHz/adim
% 
%             Nii = obj.GetParameter('STEPS'); %Number of steps for B1 SL sweep
%             minB1 = obj.GetParameter('MINB1'); %adim
%             maxB1 = obj.GetParameter('MAXB1'); %adim
%             B1SL = linspace(minB1,maxB1,Nii);
%             rawData.inputs.B1List = B1SL;
%             Njj = 1; %Number of steps for G
%             
%             nIter = 0;
%             NIter = Nii*Njj;
%             
%             nPoints = obj.GetParameter('NPOINTS'); gslice=obj.GetParameter('GRADIENTSLICE');pulseTime90 = obj.GetParameter('PULSETIME');pulselocking = obj.GetParameter('PULSELOCKINGTIME');deadTime = obj.GetParameter('TRANSIENTTIME');blkDelay = obj.GetParameter('BLANKINGDELAY');acqTime = obj.GetParameter('ACQTIME');bw = nPoints/acqTime;
%             slPosi = obj.GetParameter('SLICEPOSITION');
%             obj.SliceFreqOffset = 42.58d6*gslice*slPosi;
%             obj.SetParameter('SPECTRALWIDTH',bw*1e-3);
%             rawData.inputs.RFampB1hardAdim=obj.GetParameter('RFAMPLITUDE');rawData.inputs.gslice=gslice; rawData.inputs.blk=blkDelay;rawData.inputs.NEX = obj.GetParameter('NSCANS');rawData.inputs.nPoints = nPoints; rawData.inputs.B1hard_kHz = obj.GetParameter('RFAMPLITUDE');rawData.inputs.B1soft_kHz = obj.GetParameter('RFAMPLITUDE2')*1e-3; rawData.inputs.pulseTime90 = pulseTime90; rawData.inputs.pulselockingTime = pulselocking;rawData.inputs.deadTime = deadTime; rawData.inputs.TR = obj.GetParameter('REPETITIONTIME');rawData.inputs.acqTime = acqTime;
%             obj.nInstructions=21;
%             obj.maxRepetitionsPerBatch = floor(2048/obj.nInstructions);
%             rawData.inputs.slicePosition = slPosi;
%             rawData.inputs.freqOffset = obj.SliceFreqOffset;
%             
%             % Get k-points
%             tp = obj.GetParameter('PULSETIME');
%             td = obj.GetParameter('TRANSIENTTIME');
%             ts = 1/obj.GetParameter('SPECTRALWIDTH');
%             acqTime = obj.GetParameter('ACQTIME');
%             nPoints = obj.GetParameter('NPOINTS');
%             tv = linspace(tp/2+td+ts/2,tp/2+td+ts/2+acqTime,nPoints);
%             kv1 = 42.6d6*obj.GetParameter('GRADIENTSLICE')*tv;
%             kv2 = -kv1(end:-1:1);
%             kv = [kv2,kv1];
%             tv = [-tv(end:-1:1),tv];
%             obj.SetParameter('REPETITIONDELAY',obj.GetParameter('REPETITIONTIME'));
%             if(obj.GetParameter('GRADIENTSLICE')==0)
%                 FOV = 1/ts;
%             else
%                 FOV = abs(1/(kv(2)-kv(1)));  %FOV=1/deltaK
%             end
%             rawData.inputs.FOV=FOV;
%             rawData.inputs.position = linspace(-FOV/2,FOV/2,2*nPoints);
%             
%             signalMap = zeros(2*obj.GetParameter('NPOINTS'),Nii);
%             
%             if(spoilerEnable )
%                 tic
%                 % Get the central frequency
%                 fprintf('Calibrating frequency... \n')
%                 f0 = obj.GetFIDParameters;
%                 obj.SetParameter('FREQUENCY',f0);
%                 fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
%             end
%             
%             for nii = 1:Nii
%                 rfamp2 = B1SL(nii);
%                 obj.RFampSL=rfamp2;
% 
%                                 
%                 obj.CreateSequence;
%                 obj.Sequence2String;
%                 fname = 'TempBatch.bat';
%                 obj.WriteBatchFile(fname);
%                 [status,result] = system(fullfile(obj.path_file,fname));
%                 if status == 0 && ~isempty(result)
%                     C = textscan(result, '%s', 'delimiter', sprintf('\f'));
%                     res = C{:};
%                     obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
%                     obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz'));
%                     obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms);
%                     obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
%                     obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
%                 else
%                     warndlg('Batch aborted!','Warning')
%                     return;
%                 end
%                 
%                 bwCalNormVal = obj.calculate_bwCalNormVal;
%                 
%                 DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
%                 cDataBatch=complex(DataBatch(:,1),DataBatch(:,2));
%                 
%                 % Reorganize signal and storaging
%                 signal = cDataBatch./bwCalNormVal;
%                 signal(1:nPoints) = signal(nPoints:-1:1);
%                 signalMap(:,nii) = (signal);
%                 
%                 dT = toc;
% %                 disp(strcat('Remaining time: ',num2str(dT*(NIter-nIter)),' s'))
%             end
%             
%             rawData.outputs.signalMap = signalMap;
%             rawData.outputs.timeValues = tv';
%             rawData.outputs.kValues = kv';
%             
%             for jj=1:Nii
%                 rawData.outputs.profilesART(:,jj) = abs(art_MRI_CPU_1D(2,0.5,FOV,length(kv),kv',rawData.outputs.signalMap(:,jj)));
%                 rawData.outputs.profilesFFT(:,jj) = abs(ifftshift(ifftn(rawData.outputs.signalMap(:,jj))));
%             end
%             
%             time = clock;
%             name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
%                 num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
%             save(fullfile(obj.path_file,name),'rawData');
%         end
%     end
% end
% 
% 
% 










% THIS CLASSDEF IS FOR SWEEP THE G VALUES FOR FIXED B1 AND SL TIME
% classdef SPINLOCKING_SWEEP < MRI_BlankSequence   
%     properties
%         maxRepetitionsPerBatch;
%         nPhasesBatch;
%         lastBatch;
%         RepetitionTime;
%         AcquisitionTime;
%         EndPhase;
%         PhasesPerBatch = 20;
%         gx;
%     end
%     methods (Access=private)
%         function CreateParameters(obj)
%             obj.CreateOneParameter('PULSELOCKINGTIME','Pulse locking time','us',0.5*MyUnits.ms);
%             obj.CreateOneParameter('GRADIENTSLICE','Gslice strength','T/m',0.05);
%             CreateOneParameter(obj,'RFAMPLITUDE2','B1','kHz',5*MyUnits.kHz);
%             CreateOneParameter(obj,'RFAMPLITUDE2','RF amp SL','',0.01);
%             obj.CreateOneParameter('REPETITIONTIME','TR','ms',50*MyUnits.ms);
%             obj.CreateOneParameter('ACQTIME','Acq time (shorter T2)','us',2000*MyUnits.us);
% %             CreateOneParameter(obj,'PHASE1', 'Phase 1', '',0.0);
% %             CreateOneParameter(obj,'PHASE2', 'Phase 2', '',90.0);
% %             CreateOneParameter(obj,'PHASE3', 'Phase 3', '',180.0);
% %             CreateOneParameter(obj,'PHASE4', 'Phase 4', '',270.0);
%             CreateOneParameter(obj,'REPETITIONDELAY','Repetition delay','ms',1);
%             CreateOneParameter(obj,'SLICEPOSITION','Slice position','mm',0*MyUnits.mm);
%             obj.InputParHidden = {'GRADIENTSLICE','SHIMSLICE','SHIMPHASE','TRANSIENTTIME','SHIMREADOUT','SPECTRALWIDTH'...
%                'COILRISETIME','SPOILERTIME','SPOILERAMP','NSLICES','BLANKINGDELAY','REPETITIONDELAY'};
%         end  
%     end
% 
%     methods
%         function obj = SPINLOCKING_SWEEP(program)
%             obj = obj@MRI_BlankSequence();
%             obj.SequenceName = 'SPINLOCKING SWEEP';
%             obj.ProgramName = 'MRI_BlankSequence3.exe';
%             if nargin > 0
%                 obj.ProgramName = program;
%             end
%             obj.CreateParameters();
%             obj.SetDefaultParameters();
%         end
%         
%         function SetDefaultParameters(obj)
%             SetDefaultParameters@MRI_BlankSequence(obj);
% %             obj.SetParameter('RFAMPLITUDE2',10);
%         end  
%          
%         function CreateSequence(obj)
%             
%             global scanner
%             
%             % If you want introduce B1 soft pulse in kHz instead of as adimensional, uncomment this block
% %             adim90B1 = obj.GetParameter('RFAMPLITUDE');
% %             tp = obj.GetParameter('PULSETIME');
% %             B190 = (pi/2)/(2*pi*42.56e6*tp); %T
% %             f90 = 42.56e6*B190*1e-3; %kHz
% %             convertB1 = f90/adim90B1; %kHz/adim
% %             obj.SetParameter('RFAMPLITUDE2',obj.GetParameter('RFAMPLITUDE2')*1e-3/convertB1);
% 
%             [shimS,shimP,shimR] = SelectAxes(obj);
%             c = obj.ReorganizeCalibrationData();
%             step=obj.nSteps;
%             gz= obj.gx/c(3);
%             gSpoiler=obj.GetParameter('GRADIENTIMAGING')/c(1);
%             acqTime = obj.GetParameter('ACQTIME')*1e6;
%             nPoints = obj.GetParameter('NPOINTS');
%             bw = (nPoints/acqTime)*1e3;
%             obj.SetParameter('SPECTRALWIDTH',bw*1e3);
%             pulseTime90 = obj.GetParameter('PULSETIME')*1e6;
%             pulselock = obj.GetParameter('PULSELOCKINGTIME')*1e6;
%             deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
%             TR = obj.GetParameter('REPETITIONTIME')*1e6;
%             blkDelay = scanner.BlankingDelay;
%             crt = obj.GetParameter('COILRISETIME')*1e6;
%             gradientDelay = scanner.grad_delay;
% 
%             phase0 = 0;
%             phase90 = 1;
%             phase180 = 2;
%             phase270 = 3;
%                         
%             RR = obj.GetParameter('READOUT');
%             PP = obj.GetParameter('PHASE');
%             SS = obj.GetParameter('SLICE');
%             
%             if PP==RR || PP==SS || RR==SS
%                 warndlg('Non valid axis inputs','Axis Error')
%                 return;
%             end
%             
%             if SS=='x'
%                 dacslice=2; 
%             end
%             if SS=='y'
%                 dacslice=0; 
%             end
%             if SS=='z'
%                 dacslice=1;
%             end
% 
%             if RR=='x'
%                 dacred=2;
%             end
%             if RR=='y'
%                 dacred=0; 
%             end
%             if RR=='z'
%                 dacred=1; 
%             end
% 
%             if PP=='x'
%                 dacph=2; 
%             end
%             if PP=='y'
%                 dacph=0;
%             end
%             if PP=='z'
%                 dacph=1;  
%             end
%             
%             PP=dacph;   SS=dacslice;   RR=dacred;
%             
%             iIns = 1;
%             obj.nline_reads = 2;
%             [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs,obj.RFamps] = deal([]);
%             
%             for ii = 1:2
%                 %% Stablish shimming during all the sequence (3 ins)
%                 iIns = obj.PulseGradient (iIns,shimR,RR,0.1);
%                 iIns = obj.PulseGradient (iIns,shimP,PP,0.1);
%                 iIns = obj.PulseGradient (iIns,shimS,SS,0.1);
%                 
%                 %% Pulse the gradient z before the pulses (1 ins)
%                 iIns = obj.RampGradient(iIns,shimS,(shimS+gz)*(-1)^ii,SS,crt,scanner.nSteps);
%                 obj.DELAYs(iIns-1) = gradientDelay-blkDelay;
%                 
%                 %% Combination of hard 90X pulse followed without delay by a soft spin locking Y pulse and -X storaged pulse (3 ins)
%                 iIns = SetBlanking(obj,iIns,1,blkDelay);
%                 iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0);
% %                 iIns = CreateRawPulse(obj,iIns,phase180,pulseTime90,0); %TO TEST IF WE CAN REVERT PULSES
%                 iIns = CreateRawPulse(obj,iIns,phase90,pulselock,1);  %PHASELOCKING
% %                 
% %                 iIns = SetBlanking(obj,iIns,1,5);
% %                 iIns = CreateRawPulse(obj,iIns,phase180,pulseTime90,0);  %STORAGING
% %                 iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0); %FOR TEST
% %                 
% %                 % Switch off the slice gradient and switch on the imaging/spoiler gradient (2 ins)
% %                 iIns = obj.PulseGradient(iIns,(shimS),SS,0.1);
% %                 iIns = obj.PulseGradient(iIns,(shimR)+gSpoiler*(-1)^ii,PP,crt-blkDelay);
% %                 
% %                 % Hard 90X pulse to tip the storaged M (2 ins)
% %                 iIns = SetBlanking(obj,iIns,1,blkDelay);
% %                 iIns = CreateRawPulse(obj,iIns,phase0,pulseTime90,0);
%                 
%                 %% Acquisition (2 ins)
%                 iIns = obj.RepetitionDelay(iIns,0,deadTime);
%                 iIns = obj.Acquisition(iIns,acqTime);
%                 
%                 %% Delay for recover T1 (1 ins)
%                 iIns = obj.RepetitionDelay (iIns,1,TR-acqTime-deadTime-1*crt-pulselock-pulseTime90);
%                 
%             end
%             obj.nInstructions = iIns-1;
%         end
%         
%         
%         function [status,result] = Run(obj,~)
%             clc
%             global rawData;
%             rawData = [];  
%             
%             [~,~,~,~,~,spoilerEnable,~] = obj.ConfigureGradients(0);
%             tic
%             
%             % Hard pulse is define from adim RF amplitude and 90� pulse time, so assuming linear correlation we calculate the Khz to adim conversion factor
%             % (necessary inside Run function to write in rawData the RF amplitude as they should be
% %             adim90B1 = obj.GetParameter('RFAMPLITUDE');
% %             tp = obj.GetParameter('PULSETIME');
% %             B190 = (pi/2)/(2*pi*42.56e6*tp); %T
% %             f90 = 42.56e6*B190*1e-3; %kHz
% %             convertB1 = f90/adim90B1; %kHz/adim
% 
%             Nii = 50; %Number of steps for G sweep
%             minG = 0.001; %T/m
%             maxG = 0.01; %T/m
%             GradList = linspace(minG,maxG,Nii);
%             rawData.inputs.GradList = GradList; %T/m
%             
%             nPoints = obj.GetParameter('NPOINTS');pulseTime90 = obj.GetParameter('PULSETIME');pulselocking = obj.GetParameter('PULSELOCKINGTIME');deadTime = obj.GetParameter('TRANSIENTTIME');blkDelay = obj.GetParameter('BLANKINGDELAY');acqTime = obj.GetParameter('ACQTIME');bw = nPoints/acqTime;
% %             slPosi = obj.GetParameter('SLICEPOSITION');
% %             obj.SliceFreqOffset = 42.58d6*gslice*slPosi;
%             obj.SetParameter('SPECTRALWIDTH',bw*1e-3);
%             rawData.inputs.RFampB1softAdim=obj.GetParameter('RFAMPLITUDE2');rawData.inputs.RFampB1hardAdim=obj.GetParameter('RFAMPLITUDE'); rawData.inputs.blk=blkDelay;rawData.inputs.NEX = obj.GetParameter('NSCANS');rawData.inputs.nPoints = nPoints; rawData.inputs.B1hard_kHz = obj.GetParameter('RFAMPLITUDE');rawData.inputs.B1soft_kHz = obj.GetParameter('RFAMPLITUDE2')*1e-3; rawData.inputs.pulseTime90 = pulseTime90; rawData.inputs.pulselockingTime = pulselocking;rawData.inputs.deadTime = deadTime; rawData.inputs.TR = obj.GetParameter('REPETITIONTIME');rawData.inputs.acqTime = acqTime;
%             obj.nInstructions=21;
%             obj.maxRepetitionsPerBatch = floor(2048/obj.nInstructions);
% %             rawData.inputs.slicePosition = slPosi;
% %             rawData.inputs.freqOffset = obj.SliceFreqOffset;
%             
%             % Get k-points
%             tp = obj.GetParameter('PULSETIME');
%             td = obj.GetParameter('TRANSIENTTIME');
%             ts = 1/obj.GetParameter('SPECTRALWIDTH');
%             acqTime = obj.GetParameter('ACQTIME');
%             nPoints = obj.GetParameter('NPOINTS');
%             tv = linspace(tp/2+td+ts/2,tp/2+td+ts/2+acqTime,nPoints);
%             tv = [-tv(end:-1:1),tv];
%             obj.SetParameter('REPETITIONDELAY',obj.GetParameter('REPETITIONTIME'));
%             
%             kv = zeros(2*obj.GetParameter('NPOINTS'),Nii);
%             signalMap = zeros(2*obj.GetParameter('NPOINTS'),Nii);
%             
%             if(spoilerEnable)
%                 tic
%                 % Get the central frequency
%                 fprintf('Calibrating frequency... \n')
%                 f0 = obj.GetFIDParameters;
%                 obj.SetParameter('FREQUENCY',f0);
%                 fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
%             end
%             
%             
%             for nii = 1:Nii
%                 kv(:,nii) = 42.6d6*GradList(nii)*tv;
%                 if(GradList(nii)==0)
%                     FOV = 1/ts;
%                 else
%                     FOV = abs(1/(kv(2,nii)-kv(1,nii)));  %FOV=1/deltaK
%                 end
%                 rawData.outputs.kValues(:,nii) =  kv(:,nii);
%                 rawData.inputs.FOV(nii) = FOV;
%                 rawData.inputs.position(:,nii) = linspace(-FOV/2,FOV/2,2*nPoints);
%                 rawData.inputs.gslice(nii)=GradList(nii);
%                 obj.gx = GradList(nii); %T/m
%                 
%                 obj.CreateSequence;
%                 obj.Sequence2String;
%                 fname = 'TempBatch.bat';
%                 obj.WriteBatchFile(fname);
%                 [status,result] = system(fullfile(obj.path_file,fname));
%                 if status == 0 && ~isempty(result)
%                     C = textscan(result, '%s', 'delimiter', sprintf('\f'));
%                     res = C{:};
%                     obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
%                     obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz'));
%                     obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms);
%                     obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
%                     obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
%                 else
%                     warndlg('Batch aborted!','Warning')
%                     return;
%                 end
%                 
%                 bwCalNormVal = obj.calculate_bwCalNormVal;
%                 
%                 DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
%                 cDataBatch=complex(DataBatch(:,1),DataBatch(:,2));
%                 
%                 % Reorganize signal and storaging
%                 signal = cDataBatch./bwCalNormVal;
%                 signal(1:nPoints) = signal(nPoints:-1:1);
%                 signalMap(:,nii) = (signal);
%             end
%             
%             rawData.outputs.signalMap = signalMap;
%             rawData.outputs.timeValues = tv';
%             
%             
%             for jj=1:Nii
%                 rawData.outputs.profilesART(:,jj) = art_MRI_CPU_1D(1,1, rawData.inputs.FOV(jj),length(rawData.outputs.kValues(:,jj)),rawData.outputs.kValues(:,jj),rawData.outputs.signalMap(:,jj));
%                 rawData.outputs.profilesFFT(:,jj) = abs(ifftshift(ifftn(rawData.outputs.signalMap(:,jj))));
%             end
%             
%             time = clock;
%             name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
%                 num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
%             save(fullfile(obj.path_file,name),'rawData');
%         end
%     end
% end