%%acquiring FID in the first tau after pi/2 pulse

classdef WAHUHA < MRI_BlankSequence
    properties
        PhasesPerBatch = 20;
        nPhasesBatch;
        Axis1Vector;
        Axis2Vector;
        Axis3Vector;
        StartSlice;
        lastBatch;
        EndSlice;
        StartPhase;
        EndPhase;
        RepetitionTime;
        slice_ampl;
        AcquisitionTime;
    end
    methods (Access=private)
        function CreateParameters(obj)
            obj.CreateOneParameter('SEQUENCETYPE','FID, WAHUHA, MREV8, CHASE','',1);
            obj.CreateOneParameter('PULSETIME180','180� Pulse Time (only in CHASE)','us',20*MyUnits.us);

            InputParameters = {'REPETITIONDELAY','TAUTIME','NREADOUTS','NPHASES'};
            obj.InputParHidden = {'READOUT','PHASE','SLICE','COILRISETIME','SHIMSLICE','SHIMPHASE','SHIMREADOUT','BLANKINGDELAY','NSLICES','NPHASES','SPOILERAMP','SPOILERTIME'};
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {'Repetition Delay','Tau Time','Echoes',''})];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
                {NaN,NaN,NaN,NaN})];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {NaN,NaN,NaN,NaN})];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {NaN,NaN,NaN,NaN})];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {'ms','us','',''})];
            
            for k=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{k},InputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = WAHUHA(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'WAHUHA';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
            obj.SetParameter('REPETITIONDELAY',100*MyUnits.ms);
            obj.SetParameter('TAUTIME',500*MyUnits.us);
            obj.SetParameter('TRANSIENTTIME',100*MyUnits.us);
            obj.SetParameter('BLANKINGDELAY',5*MyUnits.us);
            obj.SetParameter('NPOINTS',1);
            obj.SetParameter('NREADOUTS',10);
            obj.SetParameter('NPHASES',1);
            obj.SetParameter('RFAMPLITUDE',0.08);
            obj.SetParameter('PULSETIME',8*MyUnits.us);
        end
        function UpdateTETR(obj)
            obj.TE = obj.GetParameter('TRANSIENTTIME') + ...
                    0.5 * obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH');
            obj.TR = obj.TE +  0.5*(obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH')) + 0.5*obj.GetParameter('PULSETIME') +...
                    obj.GetParameter('BLANKINGDELAY') + obj.GetParameter('REPETITIONDELAY');
    
            if obj.spoiler_en
                obj.TR = obj.TR + obj.GetParameter( 'SPOILERTIME' );
            end
            obj.TTotal = obj.TR * obj.GetParameter('NSCANS') * obj.GetParameter('NSLICES') * obj.GetParameter('NPHASES');
        end
        function CreateSequence( obj, mode )
            [shimS,shimP,shimR] = SelectAxes(obj);
              
            seqType = obj.GetParameter('SEQUENCETYPE');
            if(seqType==0)  
                seqType = "FID";
            elseif(seqType==1)
                seqType = "WAHUHA";
            elseif(seqType==2)
                seqType = "MREV8";
            elseif(seqType==3)
                seqType = "CHASE";
            else
                warndlg('Non valid sequence type input')
                return;
            end
            
            if(strcmp(seqType,"WAHUHA")) 
                    nEcos = obj.GetParameter('NREADOUTS');
                    % Initialize all vectors to zero with the appropriate size
                    obj.nInstructions=3+13*(nEcos)+1+3+1;
                    [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));

                    % Calculate the number of line reads
                    obj.nline_reads = nEcos+1;

                    % Timing
                    acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6;
                    blk = obj.GetParameter('BLANKINGDELAY')*1e6;
                    trf = obj.GetParameter('PULSETIME')*1e6;
                    tau = obj.GetParameter('TAUTIME')*1e6;
                    td = obj.GetParameter('TRANSIENTTIME')*1e6;

                    if(acq_time>tau-td)
                       warndlg('You are measuring the refocusing pulse. Decrease number of points or enlarge SpectralWidth')
                    end
                                        
                    iIns = 1;
                    bbit = 1;

                    iIns = obj.PulseGradient (iIns,shimR,2,0.1);
                    iIns = obj.PulseGradient (iIns,shimP,0,0.1);
                    iIns = obj.PulseGradient (iIns,shimS,1,0.1);

                    %% Create the excitation pulse X pulse
                    nIns  = 3; % set number of instructions in this group
                    vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                        obj.PhResets(vIns) = [ 1 0 0 ];
                        obj.TXs(vIns) = [0 1 0];
                        obj.PHASEs(vIns) = [0 0 0];
                        obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                        obj.FLAGs(vIns)    = [bbit bbit 0];
                        obj.DELAYs(vIns)   = [blk trf td]; %time in us
                    iIns = iIns + nIns; % update the number of instructions


                    %% Acquisition
                        nIns = 1;
                        vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                            obj.RXs(vIns)  = 1;
                            obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                            obj.DELAYs(vIns) = tau-td;
                        iIns = iIns + nIns; % update the number of instructions in this block


                    for pointi = 1:nEcos
                        %% -X Pulse
                        nIns  = 3; % set number of instructions in this group
                        vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                            obj.TXs(vIns) = [0 1 0];
                            obj.PHASEs(vIns) = [0 2 0];
                            obj.PhResets(vIns) = [ 0 0 0 ];
                            obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                            obj.FLAGs(vIns)    = [ bbit bbit 0 ];
                            obj.DELAYs(vIns)   = [blk trf tau]; %time in us
                        iIns = iIns + nIns; % update the number of instructions

                        %% Y Pulse
                        nIns  = 3; % set number of instructions in this group
                        vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                            obj.TXs(vIns) = [0 1 0];
                            obj.PHASEs(vIns) = [0 1 0];
                            obj.PhResets(vIns) = [ 0 0 0 ];
                            obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                            obj.FLAGs(vIns)    = [ bbit bbit 0 ];
                            obj.DELAYs(vIns)   = [blk trf 2*tau]; %time in us
                        iIns = iIns + nIns; % update the number of instructions

                        %% -Y Pulse
                        nIns  = 3; % set number of instructions in this group
                        vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                            obj.TXs(vIns) = [0 1 0];
                            obj.PHASEs(vIns) = [0 3 0];
                            obj.PhResets(vIns) = [ 0 0 0 ];
                            obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                            obj.FLAGs(vIns)    = [ bbit bbit 0 ];
                            obj.DELAYs(vIns)   = [blk trf tau]; %time in us
                        iIns = iIns + nIns; % update the number of instructions

                        %% X Pulse
                        nIns  = 3; % set number of instructions in this group
                        vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                            obj.TXs(vIns) = [0 1 0];
                            obj.PHASEs(vIns) = [0 0 0];
                            obj.PhResets(vIns) = [ 0 0 0 ];
                            obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                            obj.FLAGs(vIns)    = [ bbit bbit 0 ];
                            obj.DELAYs(vIns)   = [blk trf tau]; %time in us
                        iIns = iIns + nIns; % update the number of instructions

                        %% Acquisition
                        nIns = 1;
                        vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                            obj.RXs(vIns)  = 1;
                            obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                            obj.DELAYs(vIns) = tau;
                        iIns = iIns + nIns; % update the number of instructions in this block
                    end

                    %% Repetition Delay
                    nIns  = 1; % set number of instructions in this group
                    vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                        obj.PhResets(vIns) = 0;
                        obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                        obj.DELAYs(vIns)   = obj.GetParameter('REPETITIONDELAY')*1e6; %time in us
                    iIns = iIns + nIns; % update the number of instructions
            

                    
            
            elseif(strcmp(seqType,"MREV8"))
                
                    nEcos = obj.GetParameter('NREADOUTS');
                    % Initialize all vectors to zero with the appropriate size
                    obj.nInstructions=3+3+1+(24+1)*(nEcos)+1;
                    [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
                    PP=obj.selectPhase;
                    RR=obj.selectReadout;
                    SS=obj.selectSlice;

                    % Calculate the number of line reads
                    obj.nline_reads=nEcos+1;

                    % Timing

                    acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6;
                    blk = obj.GetParameter('BLANKINGDELAY')*1e6;
                    trf = obj.GetParameter('PULSETIME')*1e6;
                    tau = obj.GetParameter('TAUTIME')*1e6;
                    td = obj.GetParameter('TRANSIENTTIME')*1e6;

                    iIns = 1;
                    bbit = 1;
                    
                    iIns = obj.PulseGradient (iIns,shimR,2,0.1);
                    iIns = obj.PulseGradient (iIns,shimP,0,0.1);
                    iIns = obj.PulseGradient (iIns,shimS,1,0.1);

                    %% Create the excitation pulse
                    nIns  = 3; % set number of instructions in this group
                    vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                        obj.PhResets(vIns) = [ 1 0 0 ];
                        obj.TXs(vIns) = [0 1 0];
                        obj.PHASEs(vIns) = [0 1 0];
                        obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                        obj.FLAGs(vIns)    = [bbit bbit 0];
                        obj.DELAYs(vIns)   = [blk trf td]; %time in us
                    iIns = iIns + nIns; % update the number of instructions


                       %% Acquisition
                        nIns = 1;
                        vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                            obj.RXs(vIns)  = 1;
                            obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                            obj.DELAYs(vIns)   = tau; %(5 us less to ensure no blanking influence in the acquisition
                        iIns = iIns + nIns; % update the number of instructions in this block


                    for pointi = 1:nEcos
                        %% -X Pulse
                        nIns  = 3; % set number of instructions in this group
                        vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                            obj.TXs(vIns) = [0 1 0];
                            obj.PHASEs(vIns) = [0 0 0];
                            obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                            obj.FLAGs(vIns)    = [ bbit bbit 0 ];
                            obj.DELAYs(vIns)   = [blk trf tau]; %time in us 
                        iIns = iIns + nIns; % update the number of instructions

                        %% Y Pulse
                        nIns  = 3; % set number of instructions in this group
                        vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                            obj.TXs(vIns) = [0 1 0];
                            obj.PHASEs(vIns) = [0 3 0];
                            obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                            obj.FLAGs(vIns)    = [ bbit bbit 0 ];
                            obj.DELAYs(vIns)   = [blk trf 2*tau]; %time in us
                        iIns = iIns + nIns; % update the number of instructions

                        %% -Y Pulse
                        nIns  = 3; % set number of instructions in this group
                        vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                            obj.TXs(vIns) = [0 1 0];
                            obj.PHASEs(vIns) = [0 1 0];
                            obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                            obj.FLAGs(vIns)    = [ bbit bbit 0 ];
                            obj.DELAYs(vIns)   = [blk trf tau]; %time in us
                        iIns = iIns + nIns; % update the number of instructions

                        %% X Pulse
                        nIns  = 3; % set number of instructions in this group
                        vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                            obj.TXs(vIns) = [0 1 0];
                            obj.PHASEs(vIns) = [0 2 0];
                            obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                            obj.FLAGs(vIns)    = [ bbit bbit 0 ];
                            obj.DELAYs(vIns)   = [blk trf 2*tau]; %time in us
                        iIns = iIns + nIns; % update the number of instructions


                        %% X Pulse
                        nIns  = 3; % set number of instructions in this group
                        vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                            obj.TXs(vIns) = [0 1 0];
                            obj.PHASEs(vIns) = [0 0 0];
                            obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                            obj.FLAGs(vIns)    = [ bbit bbit 0 ];
                            obj.DELAYs(vIns)   = [blk trf tau]; %time in us
                        iIns = iIns + nIns; % update the number of instructions

                        %% Y Pulse
                        nIns  = 3; % set number of instructions in this group
                        vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                            obj.TXs(vIns) = [0 1 0];
                            obj.PHASEs(vIns) = [0 3 0];
                            obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                            obj.FLAGs(vIns)    = [ bbit bbit 0 ];
                            obj.DELAYs(vIns)   = [blk trf 2*tau]; %time in us
                        iIns = iIns + nIns; % update the number of instructions

                        %% -Y Pulse
                        nIns  = 3; % set number of instructions in this group
                        vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                            obj.TXs(vIns) = [0 1 0];
                            obj.PHASEs(vIns) = [0 1 0];
                            obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                            obj.FLAGs(vIns)    = [ bbit bbit 0 ];
                            obj.DELAYs(vIns)   = [blk trf tau]; %time in us
                        iIns = iIns + nIns; % update the number of instructions

                        %% -X Pulse
                        nIns  = 3; % set number of instructions in this group
                        vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                            obj.TXs(vIns) = [0 1 0];
                            obj.PHASEs(vIns) = [0 2 0];
                            obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                            obj.FLAGs(vIns)    = [ bbit bbit 0 ];
                            obj.DELAYs(vIns)   = [blk trf tau]; %time in us
                        iIns = iIns + nIns; % update the number of instructions

                        %% Acquisition
                        nIns = 1;
                        vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                            obj.RXs(vIns)  = 1;
                            obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                            obj.DELAYs(vIns)   = tau;
                        iIns = iIns + nIns; % update the number of instructions in this block
                        
                    end

                    %% Repetition Delay
                    nIns  = 1; % set number of instructions in this group
                    vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                        obj.PhResets(vIns) = 0;
                        obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                        obj.DELAYs(vIns)   = obj.GetParameter('REPETITIONDELAY')*1e6; %time in us
                    iIns = iIns + nIns; % update the number of instructions

                    
            elseif(strcmp(seqType,"CHASE"))
                pulseTime180 = obj.GetParameter('PULSETIME180')*1e6;
                tau = obj.GetParameter('TAUTIME')*1e6;
                obj.nInstructions = 23;
                [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.RFamps,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
                acqTime = obj.GetParameter('ACQUISITIONTIME')*1e6;
                nPoints = obj.GetParameter('NPOINTS');
                bw = (nPoints/acqTime)*1e3;
                obj.SetParameter('SPECTRALWIDTH',bw*1e3);
                pulseTime90 = obj.GetParameter('PULSETIME90')*1e6;
                TR = obj.GetParameter('REPETITIONTIME')*1e6;
                blkDelay = obj.GetParameter('BLANKINGDELAY')*1e6;
                iIns=1;
                %% Shimming on
                iIns = obj.PulseGradient (iIns,shimR,2,0.1);
                iIns = obj.PulseGradient (iIns,shimP,0,0.1);
                iIns = obj.PulseGradient (iIns,shimS,1,0.1);
                
                %% Create the excitation pulse
                iIns = obj.CreatePulse(iIns,0,blkDelay,pulseTime90,tau);
                
                %% 90� -X Pulse
                iIns = obj.CreatePulse(iIns,2,blkDelay,pulseTime90,tau);
                
                %% 90� Y Pulse
                iIns = obj.CreatePulse(iIns,1,blkDelay,pulseTime90,tau);
                
                %% 180� X Pulse
                iIns = obj.CreatePulse(iIns,0,blkDelay,pulseTime180,tau);
                
                %% 90� Y Pulse
                iIns = obj.CreatePulse(iIns,1,blkDelay,pulseTime90,tau);
                
                %% 90 X Pulse
                iIns = obj.CreatePulse(iIns,0,blkDelay,pulseTime90,tau);
                
                %% Acquisition
                iIns = obj.Acquisition(iIns,acqTime);
                
                %% Delay for recover T1 (1 ins)
                obj.RepetitionDelay (iIns,1,TR-(6*tau+(blkDelay+pulseTime180)+5*(blkDelay+pulseTime90)));
          
            end
            
            
            
                
        
            
        end
   
        
        
        
        function [status,result] = Run(obj,iAcq)
            clc
            tic;
            global MRIDATA
            [~,~,~,~,~,~,~] = obj.ConfigureGradients(0);
            
            %% Get sequence parameters
            global rawData;               % Raw Data contains output info
            rawData = [];
            
            td = obj.GetParameter('TRANSIENTTIME');
            nEchoes = obj.GetParameter('NREADOUTS');
            nPoints = obj.GetParameter('NPOINTS');
            tauTime = obj.GetParameter('TAUTIME');
            trf = obj.GetParameter('PULSETIME');
            bw = obj.GetParameter('SPECTRALWIDTH');
            blk = obj.GetParameter('BLANKINGDELAY');
            seqType = obj.GetParameter('SEQUENCETYPE');
            taq = 1/bw;
            
            if(seqType==0)
                rawData.inputs.sequenceType = 'FID';
                
            elseif(seqType==1)
                rawData.inputs.sequenceType = 'WAHUHA';
                timefid= linspace(blk+trf+td, blk+trf+td+(nPoints)/bw,nPoints);
                timeVector = linspace(6*tauTime+5*(blk+trf),6*tauTime+5*(blk+trf)+(nPoints)/bw,nPoints);
                time = zeros(1,nPoints*(nEchoes));
                for ii = 1:nEchoes
                    time(nPoints*(ii-1)+1:nPoints*ii) = timeVector+(7*tauTime+5*(trf+blk))*(ii-1);
                end
                time=[timefid time];
                   
            elseif(seqType==2)
                rawData.inputs.sequenceType = 'MREV8';
                timeVector = linspace(12*tauTime+9*(blk+trf), 12*tauTime+9*(blk+trf)+(nPoints/bw), nPoints);
                time = zeros(1,nPoints*(nEchoes));
                for ii = 1:nEchoes
                    time(nPoints*(ii-1)+1 : nPoints*ii) = (timeVector+(12*tauTime+9*(trf+blk))*(ii-1));
                end
                timefid= linspace(blk+trf+td, blk+trf+td+(nPoints)/bw,nPoints);
                time=[timefid time];
            end
            
            seqType = rawData.inputs.sequenceType;
            rawData.inputs.NEX = obj.GetParameter('NSCANS');
            rawData.inputs.nPoints = nPoints;
            rawData.inputs.nEchoes = nEchoes;
            rawData.inputs.pulseTime = trf;
            rawData.inputs.tauTime = tauTime;
            rawData.aux.acquisitionTime = nPoints*taq;
            rawData.inputs.repetitiondelay = obj.GetParameter('REPETITIONDELAY');
            rawData.inputs.rfAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.rfFrequency = obj.GetParameter('FREQUENCY');
            rawData.inputs.bandwidth = obj.GetParameter('SPECTRALWIDTH');
            rawData.inputs.deadtime = obj.GetParameter('TRANSIENTTIME');
            rawData.aux.bandwidth = bw;
            rawData.inputs.pulsetime180 = obj.GetParameter('PULSETIME180');
            rawData.aux.nInstructions = 3+13*obj.GetParameter('NREADOUTS');+1;
            
            acq_time = taq;
            obj.AcquisitionTime = acq_time;            
            
            Data2D = [];
            
            %% Acquisition
            %obj.PhasesPerBatch = floor(min([1024/7,16000/1]));                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
            
            % NOW RUN ACTUAL ACQUISTION

            if(strcmp(seqType,"FID"))
                obj.CreateSequenceFID;
                time= linspace(blk+trf+td, blk+trf+td+(nPoints)/bw,nPoints);
            else
                obj.CreateSequence(0);
            end
            
            obj.Sequence2String;
            fname = 'TempBatch.bat';
            obj.WriteBatchFile(fname);
            [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
            if status == 0 && ~isempty(result)
                C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                res = C{:};
                obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
            else
                warndlg('Acquisition aborted!','Warning')
                return;
            end
            DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
            cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
            Data2D = cDataBatch(:);
            rawData.kSpace.sampled = [time',Data2D];
            
            %% Create MRIDATA.Data3Dnew
            %obj.SetParameter('NPOINTS',obj.GetParameter('NREADOUTS'));
            Data3D = zeros(nEchoes,1,1);
            if isfield(MRIDATA,'Data3Dnew')
                if ( isequal(size(MRIDATA.Data3Dnew),[nEchoes, 1, 1, obj.NAverages])) || (iAcq == 1)
                    MRIDATA.Data3Dnew = [];
                end
            else
                MRIDATA.Data3Dnew = [];
            end
            
            %% Save data
            Data3D = Data2D;
            MRIDATA.Data3Dnew(end+1).Fid3D = Data3D;
            if length(obj.cData) ~= prod(nEchoes)
                obj.cData = zeros(obj.NAverages,prod(nEchoes));
            end
            if obj.averaging
                Nav = obj.NAverages;
            else
                Nav = 1;
            end
            index = mod(iAcq-1,Nav)+1;
            if obj.autoPhasing
                pt = max(Data3D(:));
                Data3D = Data3D.*exp(complex(0,-angle(pt)));
            end
            obj.cData(index,1:length(Data3D(:))) = Data3D(:);
            obj.fidData = Data2D;
            obj.fftData1D = fliplr(ifftshift(ifft(obj.fidData)));
            obj.SetParameter('ACQUISITIONTIME',6*nEchoes*tauTime);
            acq_time = obj.GetParameter('ACQUISITIONTIME');
            %obj.timeVector = linspace(6*tauTime,6*tauTime*nEchoes,nEchoes);
            obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time/nEchoes;
            MRIDATA.obj = obj;
                        
            % Save rawData
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
    end
    
end
% 
% 
% classdef WAHUHA < MRI_BlankSequence
%     properties
%         PhasesPerBatch = 20;
%         nPhasesBatch;
%         Axis1Vector;
%         Axis2Vector;
%         Axis3Vector;
%         StartSlice;
%         lastBatch;
%         EndSlice;
%         StartPhase;
%         EndPhase;
%         RepetitionTime;
%         slice_ampl;
%         AcquisitionTime;
%     end
%     methods (Access=private)
%         function CreateParameters(obj)
% 
%             % HELP %
%             % I include here new inputs to discriminate between
%             % acquistition and regruidding.
%             % -> NPHASES, NREADOUTS and NSLICES are for regridding
%             % -> NPOINTS, NCIRCUNFERENCES, NLINESPERCIRCUNFERENCES are for
%             %    aqcuisition.
% 
%             InputParameters = {...
%                 'REPETITIONDELAY',...
%                 'TAUTIME','NREADOUTS','NPHASES',...
%                 };
%             
%             obj.InputParHidden = {'READOUT','PHASE','SLICE',...
%                 'COILRISETIME','SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
%                 'BLANKINGDELAY','NSLICES','NPHASES','SPOILERAMP','SPOILERTIME'};
%             
%             obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
%                 {...
%                 'Repetition Delay',...
%                 'Tau Time','Echoes','',...
%                 })];
%             obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
%                 {...
%                 NaN,...
%                 NaN,NaN,NaN,...
%                 })];
%             obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
%                 {...
%                 NaN,...
%                 NaN,NaN,NaN,...
%                 })];
%             obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
%                 {...
%                 NaN,...
%                 NaN,NaN,NaN,...
%                 })];
%             obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
%                 {...
%                 'ms',...
%                 'us','','',...
%                 })];
%             
%             for k=1:length(obj.InputParameters)
%                 InputParameters(strcmp(obj.InputParameters{k},InputParameters)) = [];
%             end
%             obj.InputParameters = [obj.InputParameters,InputParameters];
% %             % Reorganizes InputParameters
% %             obj.MoveParameter(6,21);
% %             obj.MoveParameter(13:21,19:27);
% %             obj.MoveParameter(17:18,26:27);
% %             obj.MoveParameter(17,18);
% %             obj.MoveParameter(23,24);
%         end
%         
% %         function MoveParameter(obj,x,y)
% %             xx = obj.InputParameters(x);
% %             output = obj.InputParameters;
% %             output(x) = [];
% %             output(y+1:end+1) = output(y:end);
% %             output(y) = xx;
% %             obj.InputParameters = output;
% %         end
%     end
%     
%     
%     %======================== Public Methods =================================
%     methods
%         function obj = WAHUHA(program)
%             obj = obj@MRI_BlankSequence();
%             obj.SequenceName = 'WAHUHA';
%             obj.ProgramName = 'MRI_BlankSequenceJM.exe';
%             if nargin > 0
%                 obj.ProgramName = program;
%             end
%             obj.CreateParameters();
%             obj.SetDefaultParameters();
%         end
%         function SetDefaultParameters(obj)
%             SetDefaultParameters@MRI_BlankSequence(obj);
%             obj.SetParameter('REPETITIONDELAY',100*MyUnits.ms);
%             obj.SetParameter('TAUTIME',500*MyUnits.us);
%             obj.SetParameter('TRANSIENTTIME',100*MyUnits.us);
%             obj.SetParameter('BLANKINGDELAY',5*MyUnits.us);
%             obj.SetParameter('NPOINTS',1);
%             obj.SetParameter('NREADOUTS',10);
%             obj.SetParameter('NPHASES',1);
%             obj.SetParameter('RFAMPLITUDE',0.08);
%             obj.SetParameter('PULSETIME',8*MyUnits.us);
%         end
%         function UpdateTETR(obj)
%             obj.TE = obj.GetParameter('TRANSIENTTIME') + ...
%                     0.5 * obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH');
%             obj.TR = obj.TE +  0.5*(obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH')) + 0.5*obj.GetParameter('PULSETIME') +...
%                     obj.GetParameter('BLANKINGDELAY') + obj.GetParameter('REPETITIONDELAY');
%     
%             if obj.spoiler_en
%                 obj.TR = obj.TR + obj.GetParameter( 'SPOILERTIME' );
%             end
%             obj.TTotal = obj.TR * obj.GetParameter('NSCANS') * obj.GetParameter('NSLICES') * obj.GetParameter('NPHASES');
%         end
%         function CreateSequence( obj, mode )
%             [shimS,shimP,shimR] = SelectAxes(obj);
%             nEcos = obj.GetParameter('NREADOUTS');
%             % Initialize all vectors to zero with the appropriate size
%             obj.nInstructions=3+13*(nEcos)+1+3;
%             [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
% 
%             % Calculate the number of line reads
%             obj.nline_reads = nEcos+1;
%             
%             % Timing
%             acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6;
%             blk = obj.GetParameter('BLANKINGDELAY')*1e6;
%             trf = obj.GetParameter('PULSETIME')*1e6;
%             tau = obj.GetParameter('TAUTIME')*1e6;
%             td = obj.GetParameter('TRANSIENTTIME')*1e6;
%             
%             iIns = 1; 
%             bbit = 1;
%             
%             iIns = obj.PulseGradient (iIns,shimR,2,0.1);
%             iIns = obj.PulseGradient (iIns,shimP,0,0.1);
%             iIns = obj.PulseGradient (iIns,shimS,1,0.1);
%                      
%             %% Create the excitation pulse X pulse
%             nIns  = 3; % set number of instructions in this group
%             vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
%                 obj.PhResets(vIns) = [ 1 0 0 ];
%                 obj.TXs(vIns) = [0 1 0];
%                 obj.PHASEs(vIns) = [0 0 0];
%                 obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
%                 obj.FLAGs(vIns)    = [bbit bbit 0];
%                 obj.DELAYs(vIns)   = [blk trf td]; %time in us
%             iIns = iIns + nIns; % update the number of instructions
% 
%             for pointi = 1:nEcos
%                 %% -X Pulse
%                 nIns  = 3; % set number of instructions in this group
%                 vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
%                     obj.TXs(vIns) = [0 1 0];
%                     obj.PHASEs(vIns) = [0 2 0];
%                     obj.PhResets(vIns) = [ 0 0 0 ];
%                     obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
%                     obj.FLAGs(vIns)    = [ bbit bbit 0 ];
%                     obj.DELAYs(vIns)   = [blk trf tau]; %time in us
%                 iIns = iIns + nIns; % update the number of instructions
%                 
%                 %% Y Pulse
%                 nIns  = 3; % set number of instructions in this group
%                 vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
%                     obj.TXs(vIns) = [0 1 0];
%                     obj.PHASEs(vIns) = [0 1 0];
%                     obj.PhResets(vIns) = [ 0 0 0 ];
%                     obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
%                     obj.FLAGs(vIns)    = [ bbit bbit 0 ];
%                     obj.DELAYs(vIns)   = [blk trf 2*tau]; %time in us
%                 iIns = iIns + nIns; % update the number of instructions
%                 
%                 %% -Y Pulse
%                 nIns  = 3; % set number of instructions in this group
%                 vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
%                     obj.TXs(vIns) = [0 1 0];
%                     obj.PHASEs(vIns) = [0 3 0];
%                     obj.PhResets(vIns) = [ 0 0 0 ];
%                     obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
%                     obj.FLAGs(vIns)    = [ bbit bbit 0 ];
%                     obj.DELAYs(vIns)   = [blk trf tau]; %time in us
%                 iIns = iIns + nIns; % update the number of instructions
%                 
%                 %% X Pulse
%                 nIns  = 3; % set number of instructions in this group
%                 vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
%                     obj.TXs(vIns) = [0 1 0];
%                     obj.PHASEs(vIns) = [0 0 0];
%                     obj.PhResets(vIns) = [ 0 0 0 ];
%                     obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
%                     obj.FLAGs(vIns)    = [ bbit bbit 0 ];
%                     obj.DELAYs(vIns)   = [blk trf tau]; %time in us
%                 iIns = iIns + nIns; % update the number of instructions
%                 
%                 %% Acquisition
%                 nIns = 1;
%                 vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
%                     obj.RXs(vIns)  = 1;
%                     obj.ENVELOPEs(vIns)= 7; % (7=no shape)
%                     obj.DELAYs(vIns) = tau;
%                 iIns = iIns + nIns; % update the number of instructions in this block
%             end
%             
%             %% Repetition Delay
%             nIns  = 1; % set number of instructions in this group
%             vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
%                 obj.PhResets(vIns) = 0;
%                 obj.ENVELOPEs(vIns)= 7; % (7=no shape)
%                 obj.DELAYs(vIns)   = obj.GetParameter('REPETITIONDELAY')*1e6; %time in us
%             iIns = iIns + nIns; % update the number of instructions
%             
%         end
%         function [status,result] = Run(obj,iAcq)
%             clc
%             tic;
%             global MRIDATA
%             [~,~,~,~,~,~,~] = obj.ConfigureGradients(0);
%             
%             %% Get sequence parameters
%             global rawData;               % Raw Data contains output info
%             rawData = [];
%             
%             %obj.SetParameter('NREADOUTS',1);
%             td = obj.GetParameter('TRANSIENTTIME');
%             nEchoes = obj.GetParameter('NREADOUTS');
%             nPoints = obj.GetParameter('NPOINTS');
%             tauTime = obj.GetParameter('TAUTIME');
%             trf = obj.GetParameter('PULSETIME');
%             bw = obj.GetParameter('SPECTRALWIDTH');
%             blk = obj.GetParameter('BLANKINGDELAY');
%             
%             taq = 1/bw;
%             timeVector = linspace(0,(nPoints/bw),nPoints);
%             time = zeros(1,nPoints*(nEchoes));
%             
%             for ii = 1:nEchoes
%                 time(nPoints*(ii-1)+1:nPoints*ii) = timeVector+(6*tauTime+5*(trf+blk))*(ii)+(tauTime-(nPoints/bw))*(ii-1);
%             end
% 
%             
%             rawData.inputs.sequence = obj.SequenceName;
%             rawData.inputs.NEX = obj.GetParameter('NSCANS');
%             rawData.inputs.nPoints = nPoints;
%             rawData.inputs.nEchoes = nEchoes;
%             rawData.inputs.pulseTime = trf;
%             rawData.inputs.tauTime = tauTime;
%             rawData.aux.acquisitionTime = taq;
%             rawData.inputs.delay = obj.GetParameter('REPETITIONDELAY');
%             rawData.inputs.rfAmplitude = obj.GetParameter('RFAMPLITUDE');
%             rawData.inputs.rfFrequency = obj.GetParameter('FREQUENCY');
%             rawData.inputs.bandwidth = obj.GetParameter('SPECTRALWIDTH');
%             rawData.inputs.deadtime = obj.GetParameter('TRANSIENTTIME');
%             rawData.aux.bandwidth = bw;
%             rawData.aux.nInstructions = 3+13*obj.GetParameter('NREADOUTS');+1;
%             
%             acq_time = taq;
%             obj.AcquisitionTime = acq_time;            
%             
%             Data2D = [];
%             
%             %% Acquisition
%             %obj.PhasesPerBatch = floor(min([1024/7,16000/1]));                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
%             
%             % NOW RUN ACTUAL ACQUISTION
%             disp('New acquisition')
%             if(nEchoes==0)
%                 obj.CreateSequenceFID;
%                  time = linspace(td,nPoints/bw,nPoints);
%             else
%                 obj.CreateSequence(0);
%             end
%             obj.Sequence2String;
%             fname = 'TempBatch.bat';
%             obj.WriteBatchFile(fname);
%             [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
%             if status == 0 && ~isempty(result)
%                 C = textscan(result, '%s', 'delimiter', sprintf('\f'));
%                 res = C{:};
%                 obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
%                 obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
%                 obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
%                 obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
%                 obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
%             else
%                 warndlg('Acquisition aborted!','Warning')
%                 return;
%             end
%             DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
%             cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
%             Data2D = cDataBatch(:);
%             rawData.kSpace.sampled = [time',Data2D];
%             
%             %% Create MRIDATA.Data3Dnew
%             %obj.SetParameter('NPOINTS',obj.GetParameter('NREADOUTS'));
%             Data3D = zeros(nEchoes,1,1);
%             if isfield(MRIDATA,'Data3Dnew')
%                 if ( isequal(size(MRIDATA.Data3Dnew),[nEchoes, 1, 1, obj.NAverages])) || (iAcq == 1)
%                     MRIDATA.Data3Dnew = [];
%                 end
%             else
%                 MRIDATA.Data3Dnew = [];
%             end
%             
%             %% Save data
%             Data3D = Data2D;
%             MRIDATA.Data3Dnew(end+1).Fid3D = Data3D;
%             if length(obj.cData) ~= prod(nEchoes)
%                 obj.cData = zeros(obj.NAverages,prod(nEchoes));
%             end
%             if obj.averaging
%                 Nav = obj.NAverages;
%             else
%                 Nav = 1;
%             end
%             index = mod(iAcq-1,Nav)+1;
%             if obj.autoPhasing
%                 pt = max(Data3D(:));
%                 Data3D = Data3D.*exp(complex(0,-angle(pt)));
%             end
%             obj.cData(index,1:length(Data3D(:))) = Data3D(:);
%             obj.fidData = Data2D;
%             obj.fftData1D = fliplr(ifftshift(ifft(obj.fidData)));
%             obj.SetParameter('ACQUISITIONTIME',6*nEchoes*tauTime);
%             acq_time = obj.GetParameter('ACQUISITIONTIME');
%             %obj.timeVector = linspace(6*tauTime,6*tauTime*nEchoes,nEchoes);
%             obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time/nEchoes;
%             MRIDATA.obj = obj;
%                         
%             % Save rawData
%             time = clock;
%             name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
%                 num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
%             save(fullfile(obj.path_file,name),'rawData');
%         end
%     end
%     
% end
