classdef EddyCurrentSE < MRI_BlankSequence
    properties
        maxRepetitionsPerBatch;
        nPhasesBatch;
        lastBatch;
        RepetitionTime;
        AcquisitionTime;  
        StartSlice;
        EndSlice;
        PhasesPerBatch = 20;
        PhasesinThisBatch = 1;
        StartPhase;
        EndPhase;
    end
    
    methods (Access=private)
        function CreateParameters(obj)
            obj.CreateOneParameter('GRADIENTAMPL','Gradient Amplitude','T/m',0.050);
            obj.CreateOneParameter('REPETITIONDELAY','Repetition Delay','ms',50*MyUnits.ms)
            obj.CreateOneParameter('TE','TE','ms',5*MyUnits.ms);
            obj.CreateOneParameter('TR','TR','ms',1000*MyUnits.ms);
            obj.CreateOneParameter('ACQTIME','Acquisition Time','ms',3*MyUnits.ms);
            obj.CreateOneParameter('RFAMPLITUDE2','180� RF Amplitude','',0.2);
            obj.CreateOneParameter('RFAMPLITUDE','90� RF Amplitude','',0.2);
            
            
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT','READOUT'...
                'NSLICES','BLANKINGDELAY','PHASE','SLICE','SPOILERTIME','SPOILERAMP',...
                'REPETITIONDELAY','SPECTRALWIDTH','TRANSIENTTIME'}; 
        end  
    end
    
    

    methods
        function obj = EddyCurrentSE(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'EddyCurrentSE';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
        end
        
         
        function CreateSequence(obj)
            
            [shimS,shimP,shimR] = SelectAxes(obj);
            shimG = [shimR,shimP,shimS];
            [~,~,~,~,~,~,~] = obj.ConfigureGradients(0);
            obj.nline_reads = 0;
            dac = [obj.selectReadout,obj.selectPhase,obj.selectSlice];
            
            % Initialize all vectors to zero with dynamic size
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs,obj.RFamps] = deal([]);
            
            global scanner
            c = scanner.GradientConstants;
            iIns = 1;    
            blkTime         = scanner.BlankingDelay;
            pulseTime       = obj.GetParameter('PULSETIME')*1e6;
            TR              = obj.GetParameter('TR')*1e6;
            crTime          = obj.GetParameter('COILRISETIME')*1e6;
            acqTime         = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6;
            gradAmp         = obj.GetParameter('GRADIENTAMPL');
            gradDelay       = scanner.grad_delay;
            TE              = obj.GetParameter('TE')*1e6;
            TR              = obj.GetParameter('TR')*1e6;
            
            gAU = gradAmp./c;
            exPhase = 0;
            rePhase = 1;
            exAmpli = 0;
            reAmpli = 1;
            
            %%%%%%%%%%%%%%%%%%%%%%%%
            % Sequence starts here %
            %%%%%%%%%%%%%%%%%%%%%%%%
            
            % Activate shimming
            iIns = obj.RampGradient(iIns,[0 0 0],shimG,dac,0.3,1);
            
            for eje = 1:3
                % 90� RF pulse
                iIns = obj.SetBlanking(iIns,1,blkTime);
                iIns = obj.CreateRawPulse(iIns,exPhase,pulseTime,exAmpli);
                
                % Turn on gradient
                iIns = obj.RampGradient(iIns,shimG(eje),shimG(eje)+gAU(eje),dac(eje),crTime,obj.nSteps);
                obj.DELAYs(iIns-1) = crTime/obj.nSteps+TE/2-pulseTime-gradDelay-crTime;
                
                % 180� RF pulse
                iIns = obj.SetBlanking(iIns,1,blkTime);
                obj.PhResets(iIns-1) = 0;
                iIns = obj.CreateRawPulse(iIns,rePhase,pulseTime,reAmpli);
                iIns = obj.SetBlanking(iIns,0,TE/2-pulseTime/2-acqTime/2);
                obj.PhResets(iIns-1) = 0;
                
                % Acquisition
                iIns = obj.Acquisition(iIns,acqTime);
                obj.nline_reads = obj.nline_reads+1;
                
                % Turn off the gradient
                iIns = obj.RampGradient(iIns,shimG(eje)+gAU(eje),shimG(eje),dac(eje),crTime,obj.nSteps);
                
                % Repetition delay
                if eje==1
                    repDelay = TR-sum(obj.DELAYs(4:end));
                end
                if eje<3
                    iIns = obj.RepetitionDelay(iIns,0,repDelay);
                else
                    iIns = obj.RepetitionDelay(iIns,1,repDelay);
                end
                
            end
            
            obj.nInstructions = iIns-1;
        end
        
        function [status,result] = Run(obj,iAcq)
            clc
            global rawData
            global scanner
            obj.nSteps = 5;
            rawData = [];
            rawData.inputs.sequenceName = obj.SequenceName;
            rawData.inputs.nPoints = obj.GetParameter('NPOINTS');
            rawData.inputs.gradientAmpl = obj.GetParameter('GRADIENTAMPL');
            rawData.inputs.coilRiseTime = obj.GetParameter('COILRISETIME');
            rawData.inputs.TE = obj.GetParameter('TE');
            rawData.inputs.TR = obj.GetParameter('TR');
            rawData.inputs.acqTime = obj.GetParameter('ACQTIME');
            rawData.auxiliar.bandwidth = rawData.inputs.nPoints/rawData.inputs.acqTime;
            rawData.inputs.frequency = obj.GetParameter('RFFREQUENCY');
            rawData.inputs.pulseTime = obj.GetParameter('PULSETIME');
            rawData.inputs.exRFAmp = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.reRFAmp = obj.GetParameter('RFAMPLITUDE2');
            rawData.auxiliar.gradDelay = scanner.grad_delay*1e-6;
            obj.SetParameter('SPECTRALWIDTH',rawData.auxiliar.bandwidth);
            
            obj.CreateSequence;
            obj.Sequence2String;
            fname = 'TempBatch.bat';
            obj.WriteBatchFile(fname);
            [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
            if status == 0 && ~isempty(result)
                C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                res = C{:};
                obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
            else
                warndlg('Batch aborted!','Warning')
                return;
            end
            DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
            bwCalNormVal = obj.calculate_bwCalNormVal;
            cDataBatch = complex(DataBatch(:,1),DataBatch(:,2))/bwCalNormVal;
            
            nPoints = obj.GetParameter('NPOINTS');
            BW = obj.GetParameter('SPECTRALWIDTH');
            data = reshape(cDataBatch,nPoints,3);
            TE = obj.GetParameter('TE');
            timeVector = TE+linspace(-nPoints/BW/2,nPoints/BW/2,nPoints)';
            rawData.outputs.fid = [timeVector,data];
            
            % Fitting to get the experimental TE
            t = rawData.outputs.fid(:,1);
            s1 = abs(rawData.outputs.fid(:,2));
            s2 = abs(rawData.outputs.fid(:,3));
            s3 = abs(rawData.outputs.fid(:,4));
            [a,ind] = max(s1);
            b = t(ind);
            c = 0.2*rawData.inputs.acqTime;
            d = mean(s1(ceil(end/2):end));
            startPoints = [a b c d];
            gaussEqn = 'a*exp(-((x-b)/c)^2)+d';
            s11 = fit(t,s1,gaussEqn,'Start',startPoints);
            s22 = fit(t,s2,gaussEqn,'Start',startPoints);
            s33 = fit(t,s3,gaussEqn,'Start',startPoints);
            rawData.outputs.fit = [t,s11(t),s22(t),s33(t)];
            rawData.outputs.expTE = [s11.b,s22.b,s33.b];
            
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
    end
    
end