classdef GE3D_EPI < MRI_BlankSequence
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    %  Elena Diaz Caballero
    
    properties
        StartSlice;
        EndSlice;
        PhasesPerBatch = 20;
        PhasesIndex = [];
        MaxPoints = 4094;
        MaxInstructions = floor((1000)/15);
        StartPhase;
        EndPhase;
        nPhasesBatch;
        Axis1Vector;
        Axis2Vector;
        Axis3Vector;
        Axis1IND;
        Axis2IND;
        Axis3IND;
        nloops;
        PhaseBlip;
        PhaseBlipTime;
    end
    methods (Access=private)
        function CreateParameters(obj)
            InputParameters = {...
                'NPHASES','PHASESPEREPI','PHASETIME','ECHODELAY','REPETITIONDELAY','READOUTAMPLITUDE','READOUTAMPLITUDE2',...
                'SLICEBANDWIDTH','STARTPHASE',...
                'ENDPHASE','STARTSLICE','ENDSLICE',...
                'PREPULSETIME','PREPULSEDELAY','PREPULSETR',...
                };
            
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {...
                'Number of Phases','Echo Train Length','Phase Time','Gradient Echo Delay','Repetition Delay','DEP Readout Amplitude','REP Readout Amplitude',...
                'Slice Bandwidth','Start Phase',...
                'End Phase','Start Slice','End Slice',...
                'Prepulse Duration','Prepulse Delay','Prepulse TR',...
                })];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                })];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {...
                NaN,1,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                })];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {...
                NaN,obj.PhasesPerBatch,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                })];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {...
                '','','us','us','ms','','',...
                'MHz','',...
                '','','',...
                'ms','ms','s',...
                })];
           
            for k=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{k},InputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = GE3D_EPI(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'GE 3D EPI';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
            obj.SetParameter('PHASESPEREPI',4);
            obj.SetParameter('PHASETIME',50*MyUnits.us);
            obj.SetParameter('ECHODELAY',20*MyUnits.us);
            obj.SetParameter('REPETITIONDELAY',50*MyUnits.ms);
            obj.SetParameter('READOUTAMPLITUDE',0.1);
            obj.SetParameter('READOUTAMPLITUDE2',0.1);
            obj.SetParameter('NPHASES',1)
            obj.SetParameter('STARTPHASE',-0.1)
            obj.SetParameter('ENDPHASE',0.1)
            obj.SetParameter('STARTSLICE',-0.1)
            obj.SetParameter('ENDSLICE',0.1)
            
            obj.SetParameter('PREPULSETIME',10*MyUnits.ms);
            obj.SetParameter('PREPULSEDELAY',10*MyUnits.ms);
            obj.SetParameter('PREPULSETR',1*MyUnits.s);
            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
        end
        
        function UpdateTETR( obj )
            nPoints = obj.GetParameter('NPOINTS');
            InsPerBatch = min( floor(obj.MaxPoints / nPoints), obj.MaxInstructions );
            
            % number for each axes
            nReadouts = obj.GetParameter('NREADOUTS');
            nPhases = obj.GetParameter('NPHASES');
            nSlices = obj.GetParameter('NSLICES');
            
            % do switch for if the readouts and gradients are enabled
            if ~obj.readout_en
                nReadouts = 1; StartReadout = 0; EndReadout = 0;
            end
            if ~obj.phase_en
                nPhases = 1; StartPhase = 0; EndPhase = 0;
            end
            if ~obj.slice_en
                nSlices = 1; StartSlice = 0; EndSlice = 0;
            end
            
            ncenters = 10;
                
            Ntot = nReadouts*nPhases*nSlices;
                
            if obj.readout_en && obj.phase_en
                Ntot = Ntot + ncenters;
            end
                
            nBatches = ceil( Ntot / InsPerBatch );
               
                
            obj.TE = 0.5*obj.GetParameter('PULSETIME') + obj.GetParameter('TRANSIENTTIME')+obj.GetParameter('TP');
            obj.TR = obj.GetParameter('COILRISETIME')+InsPerBatch*(obj.GetParameter('COILRISETIME') + obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')+...
                obj.GetParameter('BLANKINGDELAY') + 0.5*obj.GetParameter('PULSETIME') +...
                obj.GetParameter('REPETITIONDELAY') +obj.TE);
           if obj.GetParameter('PREPULSETIME') > 0
               obj.TR = obj.TR + obj.GetParameter('PREPULSETIME') + obj.GetParameter('PREPULSEDELAY') + obj.GetParameter('PREPULSETR');
           end
           obj.TTotal = obj.TR * nBatches * obj.GetParameter('NSCANS') ;
        end
        
        function CreateSequence( obj, mode )
                        
            obj.PhasesIndex = [];
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            
            % First calculate how many iterations of the pulses will happen
            nblips  = obj.GetParameter('PHASESPEREPI');
            nPhases = obj.nPhasesBatch;
            niter   = ceil(nPhases/nblips);
            
            %blanking bits
            bbit = 1;            
            bbit2 = 2;

            % then grab the appropriate switches for IF the
            % readout/slice/phases were selected
            [~,~,readout,slice,phase,spoiler,~] = obj.ConfigureGradients(mode);
                                    
            % Calculate the number of line reads based on the length of the Axis vector
            obj.nline_reads = nPhases ;
                        
            % Initialize all vectors to zero with the appropriate size
            if obj.GetParameter('PREPULSETIME') > 0
                obj.nInstructions=7*obj.nline_reads+15*niter;
            else
                obj.nInstructions=7*obj.nline_reads+13*niter;
            end
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
                           
            % calculate the acquisition time
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
                        
            %%% Gradient amplitude calculation
               % set the bounds to the vectors
            MinVector = - 1;
            MaxVector = + 1;
            
            % readout gradient amplitude calculation
            if( readout == 0 )
                dephasing_readout = shimR;
                phasing_readoutP = shimR;
                phasing_readoutN = shimR;
            elseif( acq_time  > 2 * (obj.GetParameter('PHASETIME') + 1e-6)  )
                dephasing_readout = min(1.0, max( -1.0, shimR +1.0 * obj.GetParameter('READOUTAMPLITUDE')));
                phasing_readoutP = min( 1.0, max( -1.0, shimR -2 *obj.GetParameter('READOUTAMPLITUDE2') * (obj.GetParameter('PHASETIME') + 1e-6) / ( acq_time )));
                phasing_readoutN = min( 1.0, max( -1.0, shimR +2 *obj.GetParameter('READOUTAMPLITUDE2') * (obj.GetParameter('PHASETIME') + 1e-6) / ( acq_time )));
%                 phasing_readoutP = -dephasing_readout;
%                 phasing_readoutN = dephasing_readout;
            else
                dephasing_readout = max( -1.0, min( 1.0, shimR + obj.GetParameter('READOUTAMPLITUDE') *  acq_time / (2 * (obj.GetParameter('PHASETIME') + 1e-6) )));
                phasing_readoutP = min(1.0, max( -1.0, shimR -1.0 * obj.GetParameter('READOUTAMPLITUDE2')));
                phasing_readoutN = min(1.0, max( -1.0, shimR +1.0 * obj.GetParameter('READOUTAMPLITUDE2')));
%                 phasing_readoutP = -dephasing_readout;
%                 phasing_readoutN = dephasing_readout;
            end
            
            % phase switch set to Axis2
            if( phase == 0 )
                StartPhase = shimP;
                EndPhase   = shimP;
            else
                StartPhase = max( min(MaxVector,obj.StartPhase + shimP), MinVector );
                EndPhase = max( min(MaxVector,obj.EndPhase + shimP), MinVector );
            end
            PhaseList  = linspace(StartPhase,EndPhase,nPhases);
            PhaseStep  = (EndPhase - StartPhase)/(nPhases-1);
            
            % slice switch set to Axis3
            if( slice == 0 )
                SliceDephase = shimS;
                SliceRephase = shimS;
            else
                SliceDephase = max( min(MaxVector,obj.StartSlice + shimS), MinVector );
                SliceRephase = max( min(MaxVector,shimS - obj.StartSlice), MinVector );
            end
            
            %%% BEGIN THE PULSE PROGRAMMING
            iIns = 1;
            for iiter = 1:niter
                
                
                %------ Blanking Delay and Shimming in all gradients (3 instructions)
                nIns = 3; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [shimP shimR shimS ];
                    obj.DACs(vIns)     = [PP RR SS ];
                    obj.WRITEs(vIns)   = [1 1 1];
                    obj.UPDATEs(vIns)  = [1 1 1];
                    obj.PhResets(vIns) = [0 0 1];
                    obj.ENVELOPEs(vIns)= [7 7 7]; % (7=no shape)
                    obj.FLAGs(vIns)    = [0 0 bbit];
                    obj.DELAYs(vIns)   = [0.1 0.1 obj.GetParameter('BLANKINGDELAY')*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                % PREPULSE IF THE PREPULSETIME IS GREATER THAN 0
                if obj.GetParameter('PREPULSETIME') > 0
                    nIns  = 2; % set number of instructions in this group
                    vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                        obj.AMPs(vIns)     = [shimR shimR];
                        obj.DACs(vIns)     = [RR RR];
                        obj.WRITEs(vIns)   = [1 1];
                        obj.UPDATEs(vIns)  = [1 1];
                        obj.PhResets(vIns) = [0 1];
                        obj.ENVELOPEs(vIns)= [7 7]; % (7=no shape)
                        obj.FLAGs(vIns)    = [bbit2 0];
                        obj.DELAYs(vIns)   = [obj.GetParameter('PREPULSETIME')*1e6 obj.GetParameter('PREPULSEDELAY')*1e6 ]; %time in us
                    iIns = iIns + nIns; % update the number of instructions
                end

                %------ RF pulse (1 instruction)
                nIns = 1; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.TXs(vIns)      = 1;
                    if (obj.RF_Shape) % Shaped RF pulse
                        obj.ENVELOPEs(vIns)= 0;
                    else
                        obj.ENVELOPEs(vIns)= 7;
                    end
                    obj.FLAGs(vIns)    = bbit;
                    obj.OPCODEs(vIns)  = 0;
                    obj.DELAYs(vIns)   = obj.GetParameter('PULSETIME')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block

                %----- Transient Delay and TP Delay (1 instructions)
                nIns = 1; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.WRITEs(vIns)   = 0;
                    obj.UPDATEs(vIns)  = 0;
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.DELAYs(vIns)   = (obj.GetParameter('TRANSIENTTIME'))*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block

                %------ Set DePhasing gradients (3 instructions)
                nIns = 3; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [dephasing_readout PhaseList(1+(iiter-1)*nblips) SliceDephase];
                    obj.DACs(vIns)     = [RR PP SS ];
                    obj.WRITEs(vIns)   = [1 1 1];
                    obj.UPDATEs(vIns)  = [1 1 1];
                    obj.PhResets(vIns) = [0 0 0];
                    obj.ENVELOPEs(vIns)= [7 7 7]; % (7=no shape)
                    obj.FLAGs(vIns)    = [0 0 0];
                    obj.DELAYs(vIns)   = [0.1 0.1 (obj.GetParameter('PHASETIME')+obj.GetParameter('COILRISETIME'))*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                
                if iiter == niter
                    nblipsrun = mod(nPhases,nblips);
                    if nblipsrun == 0
                        nblipsrun = nblips;
                    end
                else
                    nblipsrun = nblips;
                end
                    
                % --- NOW START THE GRADIENT ECHO TRAIN
                for ipulse = 1:nblipsrun
                    
                    if mod((iiter-1)*nblips+ipulse,2) == 0
                        phasing_readout = phasing_readoutN;
%                         phasing_readout = phasing_readoutN+0.073; % I add this offset trying to align odd and even lines in the k-space. Es una chapuza... pero de momento no s� lo que pasa 

                    else
                        phasing_readout = phasing_readoutP;
                    end
                    
                    %------ Shimming in two gradients PLUS gradient echo delay (3 instructions)
                    nIns = 4; % set the number of instruction in this block
                    vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                        obj.AMPs(vIns)     = [shimP shimS shimR phasing_readout];
                        obj.DACs(vIns)     = [PP SS RR RR];
                        obj.WRITEs(vIns)   = [1 1 1 1];
                        obj.UPDATEs(vIns)  = [1 1 1 1];
                        obj.PhResets(vIns) = [0 0 0 0];
                        obj.ENVELOPEs(vIns)= [7 7 7 7]; % (7=no shape)
                        obj.FLAGs(vIns)    = [0 0 0 0];
                        obj.DELAYs(vIns)   = [0.1 0.1 obj.GetParameter('COILRISETIME')*1e6 (obj.GetParameter('ECHODELAY') + obj.GetParameter('COILRISETIME'))*1e6]; %time in us
                    iIns = iIns + nIns; % update the number of instructions in this block
                    
                    %----- Data Acquisition (1 instruction)
                    nIns = 1; % set the number of instruction in this block
                    vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                        obj.AMPs(vIns)     = phasing_readout;
                        obj.DACs(vIns)     = RR;
                        obj.WRITEs(vIns)   = 1;
                        obj.UPDATEs(vIns)  = 1;
                        obj.RXs(vIns)      = 1;
                        obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                        obj.DELAYs(vIns)   = acq_time*1e6;
                    iIns = iIns + nIns; % update the number of instructions in this block   

%                     PhaseStep = 0;
                    %------ Shimming in readout gradient plus phase blip (3 instructions)
                    nIns = 2; % set the number of instruction in this block
                    vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
%                         obj.AMPs(vIns)     = [obj.PhaseBlip shimR];
%                         obj.AMPs(vIns)     = [shimR obj.PhaseBlip];
%                         obj.AMPs(vIns)     = [shimR PhaseList((iiter-1)*nblips+ipulse)];
                        obj.AMPs(vIns)     = [shimR PhaseStep];
                        obj.DACs(vIns)     = [RR PP];
                        obj.WRITEs(vIns)   = [1 1];
                        obj.UPDATEs(vIns)  = [1 1];
                        obj.PhResets(vIns) = [0 0];
                        obj.ENVELOPEs(vIns)= [7 7]; % (7=no shape)
                        obj.FLAGs(vIns)    = [0 0];
                        obj.DELAYs(vIns)   = [obj.GetParameter('COILRISETIME')*1e6 obj.PhaseBlipTime*1e6]; %time in us
                    iIns = iIns + nIns; % update the number of instructions in this block

                end

                %------ Rewind Slice and Shim in all other gradients (4 instructions)
                nIns = 4; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [shimP shimR SliceRephase shimS];
                    obj.DACs(vIns)     = [PP RR SS SS];
                    obj.WRITEs(vIns)   = [1 1 1 1];
                    obj.UPDATEs(vIns)  = [1 1 1 1];
                    obj.PhResets(vIns) = [0 0 0 0];
                    obj.ENVELOPEs(vIns)= [7 7 7 7]; % (7=no shape)
                    obj.FLAGs(vIns)    = [0 0 0 0];
                    obj.DELAYs(vIns)   = [0.1 0.1 ( obj.GetParameter('PHASETIME')+obj.GetParameter('COILRISETIME') )*1e6 obj.GetParameter('COILRISETIME')*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %----- TR (1 instructions)
                nIns = 1; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.WRITEs(vIns)   = 1;
                    obj.UPDATEs(vIns)  = 1;
                    obj.CLEARs(vIns)   = 1;
                    obj.ENVELOPEs(vIns)= 7;
                    if (ipulse == obj.nline_reads) && (obj.GetParameter('PREPULSETIME') > 0)
                        obj.DELAYs(vIns)   = (obj.GetParameter('PREPULSETR')+obj.GetParameter('REPETITIONDELAY'))*1e6; %time in us
                    else
                        obj.DELAYs(vIns)   = obj.GetParameter('REPETITIONDELAY')*1e6; %time in us
                    end
                iIns = iIns + nIns; % update the number of instructions in this block
                
            end
            
            
        end % end create gradient echo pulse sequence
        function [status,result] = Run(obj,iAcq)
            global MRIDATA
            % Now calculate the batches that need to be run    
            nPoints = obj.GetParameter('NPOINTS');
            InsPerBatch = min( floor(obj.MaxPoints / nPoints), obj.MaxInstructions );
            
            %%% CALCULATE THE AXISVECTORS
            % Begin and end axes
            StartPhase = obj.GetParameter('STARTPHASE');
            EndPhase = obj.GetParameter('ENDPHASE');
            StartSlice = obj.GetParameter('STARTSLICE');
            EndSlice = obj.GetParameter('ENDSLICE');
            
            % number for each axes
            nSlices = obj.GetParameter('NSLICES');
            nPhases = obj.GetParameter('NPHASES');
            
            nblips  = obj.GetParameter('PHASESPEREPI');
                        
            % do switch for if the readouts and gradients are enabled
            if ~obj.readout_en
                nReadouts = 1; StartReadout = 0; EndReadout = 0;
            end
            if ~obj.phase_en
                nPhases = 1; StartPhase = 0; EndPhase = 0;
            end
            if ~obj.slice_en
                nSlices = 1; StartSlice = 0; EndSlice = 0;
            end
            
            % calculate slice and phase step
            if nPhases > 1
                PhaseStep = (EndPhase - StartPhase)/(nPhases-1);
                obj.PhaseBlip = - StartPhase; 
                obj.PhaseBlipTime =  (2*obj.GetParameter('PHASETIME'))/ (nPhases-1);
%                 obj.PhaseBlipTime =  (2*obj.GetParameter('PHASETIME')+2*obj.GetParameter('COILRISETIME'))/ (nPhases-1);
%                 obj.PhaseBlipTime =  obj.GetParameter('PHASETIME')/ (nblips-1);
            else
                PhaseStep = 0;
                obj.PhaseBlip = 0;
%                 obj.PhaseBlipTime = obj.GetParameter('COILRISETIME');
                obj.PhaseBlipTime = 0.1;
            end
                        
            
            Phase_axis = linspace(StartPhase, EndPhase, nPhases);
            Slice_axis = linspace(StartSlice, EndSlice, nSlices);
            
            % now calculate the total number of batches needed
            nPBatches = ceil(nPhases/InsPerBatch);
            nPoints = obj.GetParameter('NPOINTS');
            Data3D = zeros(nPoints,nPhases,nSlices);
%             Data3DOdd = zeros(nPoints,nPhases,nSlices);
            cn = nSlices*nPBatches;
            count = 0;
            if nPBatches>1
                h_waitbar = waitbar(count/cn,'Please wait (por favor espere)...');
            end
            
            
            
            for iSlice = 1:nSlices
                    
                obj.StartSlice = Slice_axis(iSlice);
                
                if nPhases > 1
                    Data2D = [];%zeros(nPoints,nPhases);
%                     Data2DOdd = [];%zeros(nPoints,nPhases);
                else 
                    Data2D = [];
%                     Data2DOdd = [];
                end
                
                for iBatch = 1:nPBatches
                    
                    % REDO THE PHASES SECTION
                     if nPhases > 1
                         if iBatch == nPBatches
                            obj.StartPhase = Phase_axis(1+InsPerBatch*(iBatch-1));
                            obj.EndPhase = Phase_axis(end);
                            nP = numel( Phase_axis(1+InsPerBatch*(iBatch-1):end));
                         else
                            obj.StartPhase =  Phase_axis(1+InsPerBatch*(iBatch-1));
                            obj.EndPhase = Phase_axis(iBatch*InsPerBatch);
                            nP = InsPerBatch;
                         end
                         
                     else
                        obj.StartPhase = 0;
                        obj.EndPhase = 0 ;
                        nP = 1;
                     end
                     obj.nPhasesBatch = nP;
                     
%                       if nPhases >1
%                           obj.SetParameter('NPHASES',1);
%                           obj.nPhasesBatch = 1;
%                           obj.CreateSequence(1); % Create the gradient echo sequence for CF
%                           obj.Sequence2String;
%                           fname_CF = 'TempBatch_CF.bat';
%                           obj.WriteBatchFile(fname_CF);
%                           obj.CenterFrequency(fname_CF);
%                           obj.SetParameter('NPHASES',nPhases);
%                           obj.nPhasesBatch = nP;
%                       end
%                       obj.SetParameter('LASTFREQUENCY',obj.GetParameter('FREQUENCY'));

                    if isfield(MRIDATA,'Data4D')
                        if obj.averaging
                            if iAcq == 1
                                MRIDATA.Data4D = [];
                            end
                        else
                            MRIDATA.Data4D = [];
                        end
                    else
                        MRIDATA.Data4D = [];
                    end

                    if isfield(MRIDATA,'Data4DCF')
                        if obj.averaging
                            if iAcq == 1
                                MRIDATA.Data4DCF = [];
                            end
                        else
                            MRIDATA.Data4DCF = [];
                        end
                    else
                        MRIDATA.Data4DCF = [];
                    end

                    % NOW RUN ACTUAL ACQUISTION
                    
%                     % since its EPI you need to do it twice
%                     tempR1 = obj.GetParameter('READOUTAMPLITUDE');
%                     tempR2 = obj.GetParameter('READOUTAMPLITUDE2');
                    
                    
                        obj.CreateSequence(0); % Create the normal gradient echo sequence

                        obj.Sequence2String;


                        fname = 'TempBatch.bat';
                        obj.WriteBatchFile(fname);
                        [status,result] = system(fullfile(obj.path_file,fname));

                        if status == 0 && ~isempty(result)
                            C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                            res = C{:};
                            obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                            obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                            obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                            obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                            obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                        end

                        DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                        cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                        nPhasesInBatch=length(cDataBatch)/nPoints;
                        d1 = reshape(cDataBatch,nPoints,nPhasesInBatch);
                    
%                     % now flip the sign
%                     obj.SetParameter('READOUTAMPLITUDE',-tempR1);
%                     obj.SetParameter('READOUTAMPLITUDE2',-tempR2);
%                     
%                         obj.CreateSequence(0); % Create the normal gradient echo sequence
% 
%                         obj.Sequence2String;
% 
% 
%                         fname = 'TempBatch.bat';
%                         obj.WriteBatchFile(fname);
%                         [status,result] = system(fullfile(obj.path_file,fname));
% 
%                         if status == 0 && ~isempty(result)
%                             C = textscan(result, '%s', 'delimiter', sprintf('\f'));
%                             res = C{:};
%                             obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
%                             obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
%                             obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
%                             obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
%                             obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
%                         end
% 
%                         DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
%                         cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
%                         nPhasesInBatch=length(cDataBatch)/nPoints;
%                         d2 = reshape(cDataBatch,nPoints,nPhasesInBatch);
%                         
%                     obj.SetParameter('READOUTAMPLITUDE',tempR1);
%                     obj.SetParameter('READOUTAMPLITUDE2',tempR2);
                    
                    
                    % scan through the data and flip the sides
                    d1(:,2:2:end) = flipud(d1(:,2:2:end));
%                     DataEven = d1;
%                     DataOdd  = d2;
                    
%                     DataEven(:,2:2:end) = flipud(d2(:,2:2:end));
%                     DataOdd(:,2:2:end) = flipud(d1(:,2:2:end));
                        
                    Data2D = cat(2,Data2D,d1);
%                     Data2D = cat(2,Data2D,DataEven);
%                     Data2DOdd = cat(2,Data2DOdd,DataOdd);
                    
                    count = count+1;
                    if nPBatches>1
                        waitbar(count/cn,h_waitbar,sprintf('Por favor espere..... %5.2f %%',100*count/cn));
                    end
                end
                Data3D(:,:,iSlice) = Data2D;
%                 Data3DOdd(:,:,iSlice) = Data2DOdd;
                if length(obj.cData) ~= nPoints*nPhases*nSlices;
                    obj.cData = zeros(obj.NAverages,nPoints*nPhases*nSlices);
                end
                if obj.averaging
                    Nav = obj.NAverages;
                else
                    Nav = 1;
                end
                index = mod(iAcq-1,Nav)+1;
                if obj.autoPhasing
%                    pt = max(Data3D(:,ceil(size(Data3D,2)/2),ceil(size(Data3D,3)/2)));
                    pt = max(Data3D(:));
                    Data3D = Data3D.*exp(complex(0,-angle(pt)));
                end
                obj.cData(index,1:length(Data3D(:))) = Data3D(:);
                obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
                obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
                %    obj.fData1D = fftshift(ifft(fftshift(obj.cDataAv)));
                obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
                acq_time = obj.GetParameter('ACQUISITIONTIME');
                obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*acq_time/nPhases;
                obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time/nPhases;
                
               
                                   
                MRIDATA.Data2D = Data2D;
                MRIDATA.Data3D = Data3D;
%                 MRIDATA.Data2DOdd = Data2DOdd;
%                 MRIDATA.Data3DOdd = Data3DOdd;
                
            end
            if nPBatches>1
                delete(h_waitbar);
            end
        end
    end
    
end

