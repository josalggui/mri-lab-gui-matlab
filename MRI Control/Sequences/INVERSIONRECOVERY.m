classdef INVERSIONRECOVERY < MRI_BlankSequence   
    properties
        maxRepetitionsPerBatch;
        nPhasesBatch;
        lastBatch;
        RepetitionTime;
        AcquisitionTime;
        EndPhase;
        PhasesPerBatch = 20;
    end
    methods (Access=private)
        function CreateParameters(obj)
            obj.CreateOneParameter('TRANSIENTTIME','Dead Time','us',100*MyUnits.us);
            obj.CreateOneParameter('PULSETIME180','180� PulseTime','us',20*MyUnits.us);
            obj.CreateOneParameter('TIMIN','TI min','ms',1000*MyUnits.ms);
            obj.CreateOneParameter('TIMAX','TI max','ms',2000*MyUnits.ms);
            obj.CreateOneParameter('STEPS','TI steps','',50);
            obj.CreateOneParameter('REPETITIONTIME','TR','ms',50*MyUnits.ms);
            obj.CreateOneParameter('ACQUISITIONTIME','Acquisition time','ms',2*MyUnits.ms);
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT','SPECTRALWIDTH'...
                'READOUT','PHASE','SLICE','COILRISETIME','SPOILERTIME',...
                'SPOILERAMP','NSLICES','BLANKINGDELAY'};
        end  
    end
    
    methods
        function obj = INVERSIONRECOVERY(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'INVERSIONRECOVERY';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
        end
        
        function UpdateTETR(obj)
            obj.TTotal = obj.GetParameter('REPETITIONTIME') * obj.GetParameter('STEPS') * obj.GetParameter('NSCANS');
        end
         
        function CreateSequence( obj)
            [shimS,shimP,shimR] = SelectAxes(obj);     
            acqTime = obj.GetParameter('ACQUISITIONTIME')*1e6;
            nPoints = obj.GetParameter('NPOINTS');
            bw = (nPoints/acqTime)*1e3;
            obj.SetParameter('SPECTRALWIDTH',bw*1e3);
            pulseTime90 = obj.GetParameter('PULSETIME')*1e6;
            pulseTime180 = obj.GetParameter('PULSETIME180')*1e6;
            deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
            TR = obj.GetParameter('REPETITIONTIME')*1e6;
            blkDelay = obj.GetParameter('BLANKINGDELAY')*1e6;
            TImin= obj.GetParameter('TIMIN')*1e6;
            TImax= obj.GetParameter('TIMAX')*1e6;
            steps= obj.GetParameter('STEPS');
            obj.nline_reads = steps;   
            TI=linspace(TImin,TImax,steps);
            rfphase=0;
            
            iIns = 1;
            iIns = obj.CreatePulse(iIns,rfphase,blkDelay,pulseTime180,TI(1));
            iIns = obj.CreatePulse(iIns,rfphase,blkDelay,pulseTime90,deadTime);
            iIns = obj.RepetitionDelay (iIns,1,TR-acqTime-deadTime-TI(1));
            
            for i=1:steps
                %% Shimming all gradients
                iIns = obj.PulseGradient (iIns,shimR,2,0.1);
                iIns = obj.PulseGradient (iIns,shimP,0,0.1);
                iIns = obj.PulseGradient (iIns,shimS,1,0.1);
                
                %% 180� pulse
                iIns = obj.CreatePulse(iIns,rfphase,blkDelay,pulseTime180,TI(i));
                
                %% 90� pulse
                iIns = obj.CreatePulse(iIns,rfphase,blkDelay,pulseTime90,deadTime);
                
                %% Acquisition
                iIns = obj.Acquisition(iIns,acqTime);
                
                %% RepetitionDelay
                iIns = obj.RepetitionDelay (iIns,1,TR-acqTime-deadTime-TI(i)); 
            end  
            obj.nInstructions = iIns-1;
        end
        
        function [status,result] = Run(obj,iAcq)
            clc
            global rawData;
            rawData = [];
            
            % Input data
            nPoints = obj.GetParameter('NPOINTS');
            steps = obj.GetParameter('STEPS');
            TImin = obj.GetParameter('TIMIN');
            TImax = obj.GetParameter('TIMAX');
            pulseTime90 = obj.GetParameter('PULSETIME');
            pulseTime180 = obj.GetParameter('PULSETIME180');
            deadTime = obj.GetParameter('TRANSIENTTIME');
            blkDelay = obj.GetParameter('BLANKINGDELAY');
            acqTime = obj.GetParameter('ACQUISITIONTIME');
            bw = obj.GetParameter('SPECTRALWIDTH');
            TR = obj.GetParameter('REPETITIONTIME');
            obj.SetParameter('SPECTRALWIDTH',bw);            

            rawData.inputs.nPoints = nPoints;      
            rawData.inputs.rfAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.pulseTime90 = pulseTime90;
            rawData.inputs.pulseTime180 = pulseTime180;
            rawData.inputs.deadTime = deadTime;
            rawData.inputs.TR = TR;
            rawData.inputs.acqTime = acqTime;
            
            if nPoints*steps >= 16000
                warndlg('Number of points * steps >16000.','N� points error')
                return
            end

            if TImax >= TR
                warndlg('TR must be larger than TI max','TR error')
                return
            end
            
            bwCalNormVal = obj.calculate_bwCalNormVal;
                     
            % Run sequence
            obj.CreateSequence;
            obj.Sequence2String;
            fname = 'TempBatch.bat';
            obj.WriteBatchFile(fname);
            [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
            if status == 0 && ~isempty(result)
                C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                res = C{:};
                obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
%                 obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
            else
                warndlg('Batch aborted!','Warning')
                return;
            end
            DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
            cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
            fidList = cDataBatch;
%           fidList = cDataBatch/bwCalNormVal;

            TI=linspace(TImin,TImax,steps)';
            signals=reshape(fidList,[nPoints,steps]);
            TInull=min(abs(signals(1,:)))*1e3; %to take it in ms
            t1=TInull/log(2);
            
            rawData.outputs.signals = signals;
            rawData.outputs.TI = TI;
            rawData.outputs.TInull = TInull;
            rawData.outputs.T1 = t1;
            
            fprintf('T1 is ',t1, 'seconds');

            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
    end
end