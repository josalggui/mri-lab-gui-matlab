classdef PETRA_CONTRAST < MRI_BlankSequence6
    %% 
    
    % PETRA CODE
    
    %*******************************************************
    %** J. M. Algar�n                                     **
    %** CSIC/UPV                                          **
    %** I3M                                               **
    %** Avda. dels Tarongers, 12,46022, Valencia, (Spain) **
    %** Tel: +34 960 728 111                              **
    %** email: josalggui@i3m.upv.es                       **
    %*******************************************************
    
    %%

    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    %  Elena Diaz Caballero
    properties
        PhasesPerBatch = 20;
        nPhasesBatch;
        Axis1Vector;
        Axis2Vector;
        Axis3Vector;
        StartSlice;
        lastBatch;
        EndSlice;
        StartPhase;
        EndPhase;
        Stage;              % ZTE -> 1, Single Point ->2 or Prescan -> 0
        RepetitionTime;
        AcquisitionTime;
        slice_ampl;
        gradientDelay = 50;     % Gradient time delay
        tOn;                    % Time the gradient is ON febore switching
    end
    methods (Access=private)
        function CreateParameters(obj)

            % HELP %
            % I include here new inputs to discriminate between
            % acquistition and regruidding.
            % -> NPHASES, NREADOUTS and NSLICES are for regridding
            % -> NPOINTS, NCIRCUNFERENCES, NLINESPERCIRCUNFERENCES are for
            %    aqcuisition.
            
            obj.CreateOneParameter('FOVX','FOVx','mm',10*MyUnits.mm);
            obj.CreateOneParameter('FOVY','FOVy','mm',10*MyUnits.mm);
            obj.CreateOneParameter('FOVZ','FOVz','mm',10*MyUnits.mm);
            obj.CreateOneParameter('NPHASES','Ny','',40);
            obj.CreateOneParameter('NREADOUTS','Nx','',40);
            obj.CreateOneParameter('NSLICES','Nz','',1);
            obj.CreateOneParameter('REPETITIONDELAY','Repetition Delay','ms',50*MyUnits.ms);
            obj.CreateOneParameter('TMAX','T max','us',100*MyUnits.us);
            obj.CreateOneParameter('OVERSAMPLING','Oversampling','',1);
            obj.CreateOneParameter('JUMPINGFACTOR','Jumping Factor','',1);
            obj.CreateOneParameter('PHASE','Y Axis','','y');
            obj.CreateOneParameter('READOUT','X Axis','','x');
            obj.CreateOneParameter('SLICE','Z Axis','','h');
            obj.CreateOneParameter('THETA','Altitud','',0);
            obj.CreateOneParameter('PHI','Azimuth','',0);
            obj.CreateOneParameter('NGRADSTEPS','Gradient Steps','',5);
            
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
                'NPOINTS','SPECTRALWIDTH','OVERSAMPLING','JUMPINGFACTOR',...
                'THETA','PHI'};

            % Reorganizes InputParameters
            obj.MoveParameter(6,21);
            obj.MoveParameter(13:21,20:28);
            obj.MoveParameter(26,27);
            obj.MoveParameter(20,21);
        end
        
%         function MoveParameter(obj,x,y)
%             xx = obj.InputParameters(x);
%             output = obj.InputParameters;
%             output(x) = [];
%             output(y+1:end+1) = output(y:end);
%             output(y) = xx;
%             obj.InputParameters = output;
%         end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = PETRA_CONTRAST(program)
            obj = obj@MRI_BlankSequence6();
            obj.SequenceName = 'PETRA';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence6(obj);
            
            %obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            %obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
        end
        function UpdateTETR(obj)
            obj.TE = obj.GetParameter('TRANSIENTTIME') + ...
                    0.5 * obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH');
            obj.TR = obj.TE +  0.5*(obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH')) + 0.5*obj.GetParameter('PULSETIME') +...
                    obj.GetParameter('BLANKINGDELAY') + obj.GetParameter('REPETITIONDELAY');
    
            if obj.spoiler_en
                obj.TR = obj.TR + obj.GetParameter( 'SPOILERTIME' );
            end
            obj.TTotal = obj.TR * obj.GetParameter('NSCANS') * obj.GetParameter('NSLICES') * obj.GetParameter('NPHASES');
        end
        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence,
            %         "1", central frequency adjustment sequence,
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            nGradSteps = obj.GetParameter('NGRADSTEPS');
            
            if(mode==1)
                [~,nPhases,~,~,~,~,~] = obj.ConfigureGradients(mode);
                obj.Axis1Vector = 0;
                obj.Axis2Vector = 0;
                obj.Axis3Vector = 0;
            elseif(mode==0)
                [~,~,~,~,~,~,~] = obj.ConfigureGradients(mode);
                nPhases = obj.nPhasesBatch;
            end

            % Initialize all vectors to zero with the appropriate size
            obj.nline_reads = nPhases;
            obj.nInstructions = (4*3*nGradSteps+3+1+1+1)*nPhases;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            acqTime = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6;
            crt = obj.GetParameter('COILRISETIME')*1e6;
            blk = max(obj.GetParameter('BLANKINGDELAY')*1e6,obj.gradientDelay);
            pulseTime = obj.GetParameter('PULSETIME')*1e6;
            deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
            tMax = obj.GetParameter('TMAX')*1e6;
            iIns = 1;
            for phasei = 1:nPhases
                %% Rise the total gradient (3 instrucctions * nGradSteps)
                readoutAmplitude = obj.Axis1Vector(phasei)+shimR;
                phaseAmplitude = obj.Axis2Vector(phasei)+shimP;
                sliceAmplitude = obj.Axis3Vector(phasei)+shimS;
                for ii=1:nGradSteps
                    iIns = obj.PulseGradient(iIns,readoutAmplitude/nGradSteps*ii,RR,0.1);
                    iIns = obj.PulseGradient(iIns,phaseAmplitude/nGradSteps*ii,PP,0.1);
                    iIns = obj.PulseGradient(iIns,sliceAmplitude/nGradSteps*ii,SS,crt/nGradSteps-0.2);
                end
                
                %% RF pulse (3 instructions)
                iIns = obj.CreatePulse(iIns,7*(obj.RF_Shape==0),blk,pulseTime,obj.tOn*1e6-pulseTime/2-obj.gradientDelay);
                
                %% Fall the total gradient (3 instrucctions * nGradSteps)
                for ii=1:nGradSteps
                    iIns = obj.PulseGradient(iIns,readoutAmplitude*(1-ii/nGradSteps),RR,0.1);
                    iIns = obj.PulseGradient(iIns,phaseAmplitude*(1-ii/nGradSteps),PP,0.1);
                    iIns = obj.PulseGradient(iIns,sliceAmplitude*(1-ii/nGradSteps),SS,crt/nGradSteps-0.2);
                end
                
                %% Rise the total negative gradient (3 instrucctions * nGradSteps)
                readoutAmplitude = -obj.Axis1Vector(phasei)+shimR;
                phaseAmplitude = -obj.Axis2Vector(phasei)+shimP;
                sliceAmplitude = -obj.Axis3Vector(phasei)+shimS;
                for ii=1:nGradSteps
                    iIns = obj.PulseGradient(iIns,readoutAmplitude/nGradSteps*ii,RR,0.1);
                    iIns = obj.PulseGradient(iIns,phaseAmplitude/nGradSteps*ii,PP,0.1);
                    iIns = obj.PulseGradient(iIns,sliceAmplitude/nGradSteps*ii,SS,crt/nGradSteps-0.2);
                end
                
                %% graientDelay
                iIns = obj.RepetitionDelay(iIns,0,obj.gradientDelay);
                
                %% Acquisition (1 instruction)
                iIns = obj.Acquisition(iIns,acqTime-obj.gradientDelay);
                
                %% Fall the total negative gradient (3 instrucctions * nGradSteps)
                for ii=1:nGradSteps
                    iIns = obj.PulseGradient(iIns,readoutAmplitude*(1-ii/nGradSteps),RR,0.1);
                    iIns = obj.PulseGradient(iIns,phaseAmplitude*(1-ii/nGradSteps),PP,0.1);
                    iIns = obj.PulseGradient(iIns,sliceAmplitude*(1-ii/nGradSteps),SS,crt/nGradSteps-0.2);
                end
                
                %% Repetition Delay (1 instruction)
                if(obj.Stage~=0)
                    iIns = obj.RepetitionDelay(iIns,1,obj.GetParameter('REPETITIONDELAY')*1e6);
                else
                    iIns = obj.RepetitionDelay(iIns,1,acq_time*1e6);
                end
                if(obj.lastBatch==1 && phasei==nPhases)
                    disp('Last Batch!')
                end
            end % end for nPhases
            %             end %end for slicei    
        end
        
        function [status,result] = Run(obj,iAcq)
            tic;
            clc
            global MRIDATA
            [~,~,readout,slice,phase,~,~] = obj.ConfigureGradients(0);
            
            %% Constant to relate k with k'
            % HELP: k = gammabar*G*t
            % G = c*A where A is the gradient amplitude voltage from
            % radioprocessor. Then, k' = k/(gammabar*c) = A*t.
            % We still need to calibrate constant c. We also need to take
            % into account that c is different for each axis. 
            gammabar = 42.6d6;          % MHz/T
            c = [12,12,7.5];          % T/m/V
            global rawData              % Raw Data contains output info
            
            %% Get cartesian parameters
            nPoints = [obj.GetParameter('NREADOUTS'),obj.GetParameter('NPHASES'),obj.GetParameter('NSLICES')];
            fov = [obj.GetParameter('FOVX'),obj.GetParameter('FOVY'),obj.GetParameter('FOVZ')];
            deltaK = 1./fov;
            kMax = nPoints./(2*fov);
            nGradSteps = obj.GetParameter('NGRADSTEPS');
            
            rawData.inputs.NEX = obj.GetParameter('NSCANS');
            rawData.inputs.nPoints = nPoints;
            rawData.inputs.fov = fov;
            rawData.inputs.nGradSteps = nGradSteps;
            rawData.aux.deltaK = deltaK;
            rawData.aux.kMax = kMax;
            
            %% Reorganize calibration data
            readout = obj.GetParameter('READOUT');
            phase = obj.GetParameter('PHASE');
            slice = obj.GetParameter('SLICE');
            if(readout=='x');       readout = 1;
            elseif(readout=='y');   readout = 2;
            elseif(readout=='z');   readout = 3;
            end
            if(phase=='x');         phase = 1;
            elseif(phase=='y');     phase = 2;
            elseif(phase=='z');     phase = 3;
            end
            if(slice=='x');         slice = 1;
            elseif(slice=='y');     slice = 2;
            elseif(slice=='z');     slice = 3;
            end
            c = c([readout phase slice]);            
            
            %% Get sequence parameters
            tMax = obj.GetParameter('TMAX');                                % Max time for acquisition, it should be the shortest T2 to be acquired
            ov = obj.GetParameter('OVERSAMPLING');                          % Oversampling for radial acquisition. It helps for better regridding
            bw = max(nPoints)/(2*tMax);                                     % Ideal acquisition bandwidth
            bwov = ov*bw;                                                   % Acquisition bandwidth taking into account the oversampling
            obj.SetParameter('SPECTRALWIDTH',bwov);                         % Set the SPECTRALWIDTH to bw
            trf = obj.GetParameter('PULSETIME');                            % RF pulse time
            td = obj.GetParameter('TRANSIENTTIME');                         % Dead time to wait until TX/RX switching
            obj.RepetitionTime = obj.GetParameter('BLANKINGDELAY')+...      % Repetition time
                obj.GetParameter('COILRISETIME')+...
                sqrt(3)*tMax+obj.GetParameter('TIMEDELAY');
            
            rawData.inputs.pulseTime = trf;
            rawData.inputs.deadTime = td;
            rawData.inputs.tMax = tMax;
            rawData.inputs.delay = obj.GetParameter('REPETITIONDELAY');
            rawData.inputs.oversampling = ov;
            rawData.inputs.jumpingFactor = obj.GetParameter('JUMPINGFACTOR');
            rawData.inputs.rfAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.rfFrequency = obj.GetParameter('FREQUENCY');
            rawData.aux.bandwidth = bw;
            rawData.aux.bandwidthov = bwov;
            rawData.aux.repetitionTime = obj.RepetitionTime;
            rawData.inputs.axes = [obj.GetParameter('READOUT'),...
                obj.GetParameter('PHASE'),obj.GetParameter('SLICE')];
            
            %% Set number of phases, points and slices as well as amplitudes
            % gradientAmplitude = normalized amplitude to the MRIGUI input
            % nLPC = number of lines per circunferece in k-space
            % nCir = number of circunfereces in gradient space
            % nPPL = number of acquired points per line in k-space
            gradientAmplitudes = kMax./(gammabar*tMax).*~(nPoints==1);
            nPPL = ceil(2*sqrt(3)*tMax*bwov);
            nPPL = nPPL+~mod(nPPL,2);
            obj.tOn = nPPL/(2*bwov);
            nLPC = ceil((max(nPoints(1:2))*pi/2)/2);    % Ideal should be *pi instead of *pi/2 but I will use *pi/2 to save time
            nLPC = max(nLPC-mod(nLPC,2),1);
            nCir = max(ceil(nPoints(3)*pi/2)-1,1);
            obj.SetParameter('NPOINTS',nPPL);
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
            obj.AcquisitionTime = acq_time;
            
            rawData.aux.acquisitionTime = obj.AcquisitionTime;
            rawData.aux.numberOfPointsPerLine = nPPL;
            rawData.aux.linesPerCircumference = nLPC;
            rawData.aux.numberOfCircumference = nCir;
            rawData.aux.gradientAmplitudes = gradientAmplitudes;

            %% Calculate the gradients list
            % Gradient are defined by the sphere parametric equation. The
            % two parameteres are: theta in [0,pi] and phi [0,2pi].
            % Gradients are normalized to the amplifier amplitude.
            deltaTheta = pi/(nCir+1);
            theta = linspace(deltaTheta,pi-deltaTheta,nCir);
            deltaPhi = pi/nLPC;
            phi = linspace(0,pi-deltaPhi,nLPC);
            normalizedGradientsRadial = zeros(nLPC*nCir,3);                % gradientVectors1 has 3 columns with Gx, Gy and Gz
            n = 0;
            for ii = 1:nCir
                for jj = 1:nLPC
                    n = n+1;
                    normalizedGradientsRadial(n,1) = sin(theta(ii))*cos(phi(jj));
                    normalizedGradientsRadial(n,2) = sin(theta(ii))*sin(phi(jj));
                    normalizedGradientsRadial(n,3) = cos(theta(ii));
                end
            end
            % Reorganize the gradient values with a given jumping factor
            jumpingFactor = obj.GetParameter('JUMPINGFACTOR');
            index = 1:jumpingFactor:size(normalizedGradientsRadial,1);
            if(jumpingFactor>1)
                for ii = 2:jumpingFactor
                    index = cat(2,index,ii:jumpingFactor:size(normalizedGradientsRadial,1));
                end
            end
            normalizedGradientsRadial = normalizedGradientsRadial(index,:);
            gradientVectors1 = normalizedGradientsRadial*diag(gradientAmplitudes);
            clear index
            
            %% Calculate k-points matrix for radial sampling
            normalizedKRadial = zeros(nCir*nLPC,3,nPPL);
            % Calculate k-points at t = 0.5*trf+td
            normalizedKRadial(:,:,1) = (-obj.tOn+0.5/bwov)*normalizedGradientsRadial;
            % Calculate all k-points
            for ii=2:nPPL
                normalizedKRadial(:,:,ii) = normalizedKRadial(:,:,1)+(ii-1)*normalizedGradientsRadial/obj.GetParameter('SPECTRALWIDTH');
            end
            normalizedKRadial = reshape(permute(normalizedKRadial,[3,1,2]),[nCir*nLPC*nPPL,3]);
            kRadial = normalizedKRadial*diag(gammabar*gradientAmplitudes);
            kSpaceValues = kRadial;
            
            %% k-points for cartesina gridding
            kx = linspace(-kMax(1)*~isequal(nPoints(1),1),kMax(1)*~isequal(nPoints(1),1),nPoints(1));
            ky = linspace(-kMax(2)*~isequal(nPoints(2),1),kMax(2)*~isequal(nPoints(2),1),nPoints(2));
            kz = linspace(-kMax(3)*~isequal(nPoints(3),1),kMax(3)*~isequal(nPoints(3),1),nPoints(3));
            [kx,ky,kz] = meshgrid(kx,ky,kz);
            kx = permute(kx,[2,1,3]);
            ky = permute(ky,[2,1,3]);
            kz = permute(kz,[2,1,3]);
            
            %% Normalize gradient to arbitrary units
            rawData.aux.gradientsRadial = gradientVectors1;
            gradientVectors1 = gradientVectors1/diag(c);
            
            %% Auxiliar
            Data2D = [];
            
            %% Radial
            % Create waiting bar
            obj.Stage = 1; 
            insPerPhase = (4*3*nGradSteps+3+1+1+1);
            obj.PhasesPerBatch = floor(min([1024/insPerPhase,16000/nPPL])); % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
            nPhaseBatches = ceil(nLPC*nCir/obj.PhasesPerBatch);
            rawData.aux.numberOfBatches = nPhaseBatches;
            cn = nPhaseBatches;
            count = 0;
            if nPhaseBatches>1
                h_waitbar = waitbar(count/cn,'Radial (por favor espere)...');
            end
            % Start batches sweep
            disp('RADIAL');
            tRadial = 0;
            rawData.inputs.rfFrequencyRadial = zeros(nPhaseBatches,1);
            for iBatch = 1:nPhaseBatches
                disp('New batch');
                
%                 % Get the central frequency
%                 obj.SetParameter('NPOINTS',100);
%                 obj.SetParameter('SPECTRALWIDTH',100*MyUnits.kHz);
%                 obj.SetParameter('NSCANS',1);
%                 obj.SetParameter('PULSETIME',4*MyUnits.us);
%                 obj.SetParameter('RFAMPLITUDE',1);
%                 obj.CreateSequence(1); % Create the gradient echo sequence for CF                
%                 obj.Sequence2String;
%                 fname = 'TempBatch.bat';
%                 obj.WriteBatchFile(fname);
%                 [status,result] = system(fullfile(obj.path_file,fname));
%                 if status == 0 && ~isempty(result)
%                     C = textscan(result, '%s', 'delimiter', sprintf('\f'));
%                     res = C{:};
%                     obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
%                     obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
%                     obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
%                     obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
%                     obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
%                 else
%                     warndlg('Radial batch aborted!','Warning')
%                     return;
%                 end
%                 DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
%                 cDataCF = complex(DataBatch(:,1),DataBatch(:,2));
%                 fftData1D = flipud(fftshift(fft(cDataCF)));
%                 acq_time = obj.GetParameter('ACQUISITIONTIME');
%                 freqVector = (ceil(-length(fftData1D)/2):ceil(length(fftData1D)/2)-1)/acq_time;
%                 idx = find(fftData1D==max(fftData1D),1,'first');
%                 deltaFreq = freqVector(idx);
%                 
%                 obj.SetParameter('FREQUENCY',obj.GetParameter('FREQUENCY')+deltaFreq);
%                 obj.SetParameter('NPOINTS',nPPL);
%                 obj.SetParameter('SPECTRALWIDTH',bwov);
%                 obj.SetParameter('NSCANS',rawData.inputs.NEX);
%                 obj.SetParameter('PULSETIME',rawData.inputs.pulseTime);
%                 obj.SetParameter('RFAMPLITUDE',rawData.inputs.rfAmplitude);
%                 rawData.aux.frequenciesRadial(iBatch) = obj.GetParameter('FREQUENCY');
                
                % Get the appropiate axis
                if(iBatch<nPhaseBatches)
                    obj.Axis1Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,1);
                    obj.Axis2Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,2);
                    obj.Axis3Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,3);
                    obj.lastBatch = 0;
                else
                    obj.Axis1Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:nLPC*nCir,1);
                    obj.Axis2Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:nLPC*nCir,2);
                    obj.Axis3Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:nLPC*nCir,3);
                    obj.lastBatch = 1;
                end
                obj.nPhasesBatch = length(obj.Axis1Vector);
                
                % NOW RUN ACTUAL ACQUISTION
                obj.CreateSequence(0);
                obj.Sequence2String;            
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                t1 = clock;
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                t2 = clock;
                tRadial = tRadial + etime(t2,t1);
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Radial batch aborted!','Warning')
                    return;
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                nPhasesInBatch=length(cDataBatch)/nPPL;
                d = reshape(cDataBatch,nPPL,nPhasesInBatch);
                Data2D = cat(2,Data2D,d);
                count = count+1;
                if nPhaseBatches>1
                    waitbar(count/cn,h_waitbar,sprintf('Radial (por favor espere)... %5.2f %%',100*count/cn));
                end
            end
            if nPhaseBatches>1
                delete(h_waitbar);
            end
            Data2D = Data2D(:);
            kSpaceValues = [kSpaceValues,Data2D(:)];
            tBatch2 = clock;
            tBatch = etime(tBatch2,tBatch1);
            rawData.kSpace.sampled = kSpaceValues;
            
            %% Create MRIDATA.Data3Dnew
            Data3D = zeros(nPoints(1),nPoints(2),nPoints(3));
            if isfield(MRIDATA,'Data3Dnew')
                if ( isequal(size(MRIDATA.Data3Dnew),[nPPL, nLPC, nCir, obj.NAverages])) || (iAcq == 1)
                    MRIDATA.Data3Dnew = [];
                end
            else
                MRIDATA.Data3Dnew = [];
            end
            
            %% Regridding
            if(nCir>1)
                valCartesian = griddata(kSpaceValues(:,1),kSpaceValues(:,2),kSpaceValues(:,3),kSpaceValues(:,4),kCartesian(:,1),kCartesian(:,2),kCartesian(:,3));
            else
                valCartesian = griddata(kSpaceValues(:,1),kSpaceValues(:,2),kSpaceValues(:,4),kCartesian(:,1),kCartesian(:,2));
            end
            valCartesian(isnan(valCartesian)) = 0;
            rawData.kSpace.interpolated = [kx(:),ky(:),kz(:),valCartesian(:)];
            
            %% Save data
            Data3D = valCartesian;
            MRIDATA.Measurements = kSpaceValues;
            MRIDATA.Data3Dnew(end+1).Fid3D = Data3D;
            if length(obj.cData) ~= prod(nPoints);
                obj.cData = zeros(obj.NAverages,prod(nPoints));
            end
            if obj.averaging
                Nav = obj.NAverages;
            else
                Nav = 1;
            end
            index = mod(iAcq-1,Nav)+1;
            if obj.autoPhasing
                %                    pt = max(Data3D(:,ceil(size(Data3D,2)/2),ceil(size(Data3D,3)/2)));
                pt = max(Data3D(:));
                Data3D = Data3D.*exp(complex(0,-angle(pt)));
            end
            obj.cData(index,1:length(Data3D(:))) = Data3D(:);
            obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
            obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
            %    obj.fData1D = fftshift(ifft(fftshift(obj.cDataAv)));
            obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
            acq_time = obj.GetParameter('ACQUISITIONTIME');
            obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*acq_time/nLPC;
            obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time/nLPC;
            MRIDATA.obj = obj;
            obj.SetParameter('TRANSIENTTIME',td);
            
            % Save elapsed times to raw data
            elapsedTime = toc;
            rawData.aux.tPrescan = tPrescan;
            rawData.aux.tRadial = tRadial;
            rawData.aux.tCartesian = tCartesian;
            rawData.aux.tBatch = tBatch;
            rawData.aux.elapsedTime = elapsedTime;
            
            % Save rawData
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(strcat('C:\Users\josalggui\Documents\repository-mri-lab-2018.11.05\MRI Control\FileDir\',name),'rawData');
        end
    end
    
end

