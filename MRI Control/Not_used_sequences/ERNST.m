classdef ERNST < MRI_BlankSequence    
    properties
        PhasesPerBatch = 20;
        nPhasesBatch;
        Axis1Vector;
        Axis2Vector;
        Axis3Vector;
        StartSlice;
        lastBatch;
        EndSlice;
        StartPhase;
        EndPhase;
        RepetitionTime;
        slice_ampl;
        AcquisitionTime;
    end
    methods (Access=private)
        function CreateParameters(obj)
            obj.CreateOneParameter('MINREPETITIONTIME','TR min','ms',10*MyUnits.ms);
            obj.CreateOneParameter('MAXREPETITIONTIME','TR max','ms',100*MyUnits.ms);
            obj.CreateOneParameter('MINRFAMPLITUDE','RF amplitude min','',0);
            obj.CreateOneParameter('MAXRFAMPLITUDE','RF amplitude max','',0.1);
            obj.CreateOneParameter('NREADOUTS','N TR','',10);
            obj.CreateOneParameter('NPHASES','N Amplitudes','',10);
            obj.CreateOneParameter('NMISSINGPOINTS','N missing','',5);
            
            obj.InputParHidden = {'READOUT','PHASE','SLICE',...
                'COILRISETIME','SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
                'NPOINTS','BLANKINGDELAY','NSLICES','RFAMPLITUDE',...
                'SPOILERAMP','SPOILERTIME'};
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = ERNST(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'ERNST';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
            obj.SetParameter('TRANSIENTTIME',20*MyUnits.us);
        end
        function UpdateTETR(obj)
            obj.TE = 0;
            obj.TR = 0;
    
            if obj.spoiler_en
                obj.TR = obj.TR + obj.GetParameter( 'SPOILERTIME' );
            end
            
            obj.TTotal = 0;
        end
        function CreateSequence( obj, ~)
            % mode =  "0", normal execution of the sequence,
            %         "1", central frequency adjustment sequence,
            
            % first set the appropriate axes as were decided
            [~,~,~]=SelectAxes(obj);
            
            %[~,~,readout,slice,phase,~,~] = obj.ConfigureGradients(mode);
            nMissing = obj.GetParameter('NMISSINGPOINTS');
            
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions = 4*(1+nMissing);
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            % Calculate the number of line reads
            obj.nline_reads = 1;
            
            % Timing
            acq_time = max([obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6,10]);
            blankingDelay = obj.GetParameter('BLANKINGDELAY')*1e6;
            trf = obj.GetParameter('PULSETIME')*1e6;
            transientTime = obj.GetParameter('TRANSIENTTIME')*1e6;
            delayTime = obj.TR-blankingDelay-trf-transientTime-acq_time;
            iIns = 1;
            
            %% Create the sequence
            for ii = 1:obj.GetParameter('NMISSINGPOINTS')
                iIns = obj.CreatePulse(iIns,0,blankingDelay,trf,transientTime);
                iIns = obj.RepetitionDelay(iIns,0,acq_time+delayTime);
            end
            iIns = obj.CreatePulse(iIns,0,blankingDelay,trf,transientTime);
            iIns = obj.Acquisition(iIns,acq_time+delayTime);
        end
        function [status,result] = Run(obj,iAcq)
            clc
            tic;
            [~,~,~,~,~,~,~] = obj.ConfigureGradients(0);
            obj.SetParameter('NPOINTS',1);
            
            %% Get sequence parameters
            global rawData;               % Raw Data contains output info
            rawData = [];
            rawData.inputs.sequence = obj.SequenceName;
            rawData.inputs.nex = obj.GetParameter('NSCANS');
            rawData.inputs.frequency = obj.GetParameter('FREQUENCY');
            rawData.inputs.bandwidth = obj.GetParameter('SPECTRALWIDTH');
            rawData.inputs.pulseTime = obj.GetParameter('PULSETIME');
            rawData.inputs.minRFAmplitude = obj.GetParameter('MINRFAMPLITUDE');
            rawData.inputs.maxRFAmplitude = obj.GetParameter('MAXRFAMPLITUDE');
            rawData.inputs.minTR = obj.GetParameter('MINREPETITIONTIME');
            rawData.inputs.maxTR = obj.GetParameter('MAXREPETITIONTIME');
            rawData.inputs.numberOfRepetitions = obj.GetParameter('NREADOUTS');
            rawData.inputs.numberOfRFAmplitudes = obj.GetParameter('NPHASES');
            rawData.inputs.numberOfMissingPoints = obj.GetParameter('NMISSINGPOINTS');
            rawData.aux.acquisitionTime = 1/obj.GetParameter('SPECTRALWIDTH');
            
            rfAmplitudes = linspace(...
                obj.GetParameter('MINRFAMPLITUDE'),...
                obj.GetParameter('MAXRFAMPLITUDE'),...
                obj.GetParameter('NPHASES'));
            TRs = logspace(...
                log10(obj.GetParameter('MINREPETITIONTIME')),...
                log10(obj.GetParameter('MAXREPETITIONTIME')),...
                obj.GetParameter('NREADOUTS'))*1e6;
            
            rawData.aux.rfAmplitudes = rfAmplitudes;
            rawData.aux.TRs = TRs;
                        
            acq_time = 1/obj.GetParameter('SPECTRALWIDTH');
            obj.AcquisitionTime = acq_time;            
            
            %% Acquisition
            
            % NOW RUN ACTUAL ACQUISTION
            nRepetitions = obj.GetParameter('NREADOUTS');
            nAmplitudes = obj.GetParameter('NPHASES');
            Data2D = zeros(nAmplitudes*nRepetitions,1);
            n = 0;
            for ii = 1:nAmplitudes
                clc
                disp(strcat('Batch number:',num2str(ii),'/',num2str(nAmplitudes)))
                for jj = 1:nRepetitions
                    n = n+1;
                    obj.TR = TRs(jj);
                    obj.SetParameter('RFAMPLITUDE',rfAmplitudes(ii));
                    obj.CreateSequence(0);
                    obj.Sequence2String;
                    fname = 'TempBatch.bat';
                    obj.WriteBatchFile(fname);
                    [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                    if status == 0 && ~isempty(result)
                        C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                        res = C{:};
                        obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                        obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                        obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                        obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                        obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                    else
                        warndlg('Acquisition aborted!','Warning')
                        return;
                    end
                    DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                    cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                    Data2D(n) = cDataBatch(:);
                end
            end
            
            %% Create map
            [rfAmplitudes,TRs] = meshgrid(rfAmplitudes,TRs);
            rfAmplitudes = rfAmplitudes(:);
            TRs = TRs(:);
            rawData.kSpace.signal = [rfAmplitudes,TRs,Data2D];
                        
            %% Save rawData
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
    end
    
end

