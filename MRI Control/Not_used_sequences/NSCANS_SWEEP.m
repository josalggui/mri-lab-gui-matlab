classdef NSCANS_SWEEP < MRI_BlankSequence
    properties
        maxRepetitionsPerBatch;
        nPhasesBatch;
        lastBatch;
        RepetitionTime;
        AcquisitionTime;
        nSteps=5;
        
        
    end
    methods (Access=private)
        function CreateParameters(obj)
            obj.CreateOneParameter('NSCANSMIN','Minimun nScans','',1);
            obj.CreateOneParameter('NSCANSMAX','Maximun nScans','',10);
            obj.CreateOneParameter('NSTEPS','Steps','',10);
            obj.CreateOneParameter('TRANSIENTTIME','Dead Time','us',100*MyUnits.us);
            obj.CreateOneParameter('REPETITIONTIME','TR','ms',50*MyUnits.ms);
            
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT','READOUT'...
                'NSLICES','BLANKINGDELAY','PHASE','SLICE',...
                'NSCANS','COILRISETIME',...
                'REPETITIONDELAY'}; 
        end  
    end
    
    

    methods
        function obj = NSCANS_SWEEP(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'NSCANS SWEEP';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
        end
        
         
        function CreateSequence( obj)
            obj.nline_reads = 1;
            obj.nInstructions = 4;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            acqTime = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6;
            pulseTime = obj.GetParameter('PULSETIME')*1e6;
            deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
            TR = obj.GetParameter('REPETITIONTIME')*1e6;
            repDelay = TR-deadTime-acqTime-pulseTime-5;
            iIns = 1;
            
            obj.CreatePulse(iIns,5,pulseTime,deadTime);
            obj.Acquisition(iIns,acqTime+repDelay);
        end
        
        function [status,result] = Run(obj,iAcq)
            clc
            
            % Load amplitude calibration for 1V
            bwCal = load('rawData-BW-Calibration.mat');
            bwCalVal = bwCal.rawData.outputs.fidMean;
            bwCalVec = logspace(log10(bwCal.rawData.inputs.bwMin),...
                log10(bwCal.rawData.inputs.bwMax),...
                bwCal.rawData.inputs.steps);
            % Get value to normalize fid to Volts
            [~,bwCalNormPos] = min(abs(bwCalVec-obj.GetParameter('SPECTRALWIDHT')));
            bwCalNormVal = bwCalVal(bwCalNormPos);
            
            fprintf('Calibrating frequency... \n')
            % Get central frequency
            f0 = obj.GetFIDParameters;
            obj.SetParameter('FREQUENCY',f0);
            fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
            
            % Set parameters
            obj.nInstructions = 4;
            obj.maxRepetitionsPerBatch = floor(1024/obj.nInstructions);                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
            nBatches = obj.GetParameter('NSTEPS');
            nScansMin = obj.GetParameter('NSCANSMIN');
            nScansMax = obj.GetParameter('NSCANSMAX');
%             nScansList = logspace(log10(nScansMin),log10(nScansMax),nBatches);
            nScansList = linspace(nScansMin,nScansMax,nBatches);
            fidList = zeros(obj.GetParameter('NPOINTS'),nBatches);
            
            % Run sequence
            for iBatch = 1:nBatches
                fprintf('Batch %1.0f/%1.0f\n',iBatch,nBatches)
                obj.SetParameter('NSCANS',nScansList(iBatch));
                
                obj.CreateSequence;
                obj.Sequence2String;            
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Batch aborted!','Warning')
                    return;
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                fidList(:,iBatch) = cDataBatch/bwCalNormVal;
            end
            
            % Save rawData
            global rawData
            rawData = [];
            rawData.inputs.nPoints = obj.GetParameter('NPOINTS');
            rawData.inputs.nScansMin = nScansMin;
            rawData.inputs.nScansMax = nScansMax;
            rawData.inputs.steps = nBatches;
            rawData.inputs.rfFrequency = f0;
            rawData.inputs.rfAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.pulseTime = obj.GetParameter('PULSETIME');
            rawData.inputs.deadTime = obj.GetParameter('TRANSIENTTIME');
            rawData.inputs.TR = obj.GetParameter('REPETITIONTIME');           
            rawData.outputs.fidList = fidList;
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');

        end
    end
    
end