classdef EddyMeasurement < MRISequence
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        G0_amplitude
    end
    methods (Access=private)
        function CreateParameters(obj)
            InputParameters = {...
                'PHASETIME','ECHODELAY','REPETITIONDELAY','STATICG0AMPLITUDE'...
                };
            
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {...
                'Phase Time','Gradient Delay','Repetition Delay','Gradient Amplitude'...
                })];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN
                })];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN
                })];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN
                })];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {...
                'us','us','ms',''
                })];
            OutputParameters = {
                'DEPHASINGREADOUTAMPLITUDE','PHASINGREADOUTAMPLITUDE'...
                };
            obj.OutputParNames = [containers.Map(obj.OutputParNames.keys(),obj.OutputParNames.values());containers.Map(OutputParameters,...
                {...
                'Dephasing Readout Amplitude', 'Phasing Readout Amplitude'...
                })];
            obj.OutputParValues = [containers.Map(obj.OutputParValues.keys(),obj.OutputParValues.values());containers.Map(OutputParameters,...
                {...
                NaN,NaN...
                })];
            obj.OutputParUnits = [containers.Map(obj.OutputParUnits.keys(),obj.OutputParUnits.values());containers.Map(OutputParameters,...
                {...
                '',''...
                })];
            for i=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{i},InputParameters)) = [];
            end
            for i=1:length(obj.OutputParameters)
                OutputParameters(strcmp(obj.OutputParameters{i},OutputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
            obj.OutputParameters = [obj.OutputParameters,OutputParameters];
        end
        function WriteBatchFile(obj,fname)
            %% WRITE BATCH FILE AND RUN
           
            fid = fopen(fullfile(obj.path_file,fname), 'wt');
            pars = obj.InputParameters;
            values = obj.InputParValues;
            fprintf( fid, '@echo off\n');
            fprintf( fid, 'SET Debug=%d\n',0);
            fprintf( fid, 'SET outputFilename=%s\n',obj.OutputFilename);
            fprintf( fid, 'REM ======================\n');
            fprintf( fid, 'REM Acquisition Parameters\n' );
            fprintf( fid, 'REM ======================\n');
            fprintf( fid, 'SET nPoints=%u\n', values('NPOINTS') );
            fprintf( fid, 'SET nScans=%u\n', values('NSCANS') );
            fprintf( fid, 'SET spectrometerFrequency_MHz=%f\n', values('FREQUENCY')/MyUnits.MHz);
            fprintf( fid, 'SET spectralWidth_kHz=%f\n',values('SPECTRALWIDTH')/MyUnits.kHz);
            fprintf( fid, 'SET linebroadening_value=%f\n',values('LINEBROADENING'));
            
            fprintf( fid, 'REM ================\n' );
            fprintf( fid, 'REM Gradient Enables\n' );
            fprintf( fid, 'REM ================\n' );
            fprintf( fid, 'SET slice_en=%u\n', obj.slice_en);
            fprintf( fid, 'SET phase_en=%u\n', obj.phase_en);
            fprintf( fid, 'SET readout_en=%u\n', obj.readout_en);
            
            fprintf( fid, 'REM ===================\n');
            fprintf( fid, 'REM RF Pulse Parameters\n');
            fprintf( fid, 'REM ===================\n'  );
            fprintf( fid, 'SET RF_Shape=%u\n', obj.RF_Shape);
            fprintf( fid, 'SET amplitude=%f\n', values('RFAMPLITUDE'));
            fprintf( fid, 'SET pulseTime_us=%f\n', values('PULSETIME')/MyUnits.us);
            
            fprintf( fid, 'REM ======\n');
            fprintf( fid, 'REM Delays\n');
            fprintf( fid, 'REM ======\n');
            fprintf( fid, 'SET blankingDelay_ms=%f\n', values('BLANKINGDELAY')/MyUnits.ms);
            fprintf( fid, 'SET transTime_us=%f\n', values('TRANSIENTTIME')/MyUnits.us);
            fprintf( fid, 'SET phaseTime_us=%f\n', values('PHASETIME')/MyUnits.us);
            fprintf( fid, 'SET gradientEchoDelay_us=%f\n', values('ECHODELAY')/MyUnits.us);
            fprintf( fid, 'SET G0_amplitude=%f\n', obj.G0_amplitude);
            fprintf( fid, 'SET repetitionDelay_s=%f\n', values('REPETITIONDELAY')/MyUnits.s);
            
            
            shimS = 0;
            shimP = 0;
            shimR = 0;
            if (obj.shimS_en)
                shimS = obj.GetShimS();
            end
            if (obj.shimP_en)
                shimP = obj.GetShimP();
            end
            if (obj.shimR_en)
                shimR = obj.GetShimR();
            end
            fprintf( fid, 'SET shimSlice=%f\n', shimS);
            fprintf( fid, 'SET shimPhase=%f\n', shimP);
            fprintf( fid, 'SET shimReadout=%f\n', shimR);
            
            fprintf( fid, 'SET tx_phase=%f\n', obj.tx_phase);
            fprintf( fid, 'SET blankingBit=%u\n', obj.blankingBit);
            
            fprintf( fid, 'SET adcOffset=%u\n', obj.adcOffset);
            
            fprintf( fid, 'echo. | "%s"',fullfile(obj.ProgramDir,obj.ProgramName)); %we need echo. to exit batch file in case of errors (it emulates typing ENTER)
            fclose(fid);
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = EddyMeasurement(program)
            obj = obj@MRISequence();
            obj.SequenceName = 'Eddy Measurement';
            obj.ProgramName = 'EddyMeasurement.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRISequence(obj);
            obj.SetParameter('SHIMSLICE',0);
            obj.SetParameter('SHIMPHASE',0);
            obj.SetParameter('SHIMREADOUT',0);
            obj.SetParameter('PHASETIME',10*MyUnits.us);
            obj.SetParameter('ECHODELAY',5*MyUnits.us);
            obj.SetParameter('REPETITIONDELAY',50*MyUnits.ms);
            obj.SetParameter('STATICG0AMPLITUDE',0.1);
            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
        end
        function [status,result] = Run(obj,iAcq)
           
            nPoints = obj.GetParameter('NPOINTS');
            Data3D = zeros(nPoints,3,1);
            
            for iEddy = 1:3
                
                obj.G0_amplitude = ((-1)^(iEddy-1))*obj.GetParameter('STATICG0AMPLITUDE')*(iEddy>1);
               
                    fname = 'TempBatch.bat';
                    obj.WriteBatchFile(fname);
                    [status,result] = system(fullfile(obj.path_file,fname));
                    
                    if status == 0 && ~isempty(result)
                        C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                        res = C{:};
                        obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                        obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                        obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                        obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                        obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                        
                    end
                    
                    DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                    cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                    Data3D(:,iEddy) = cDataBatch;
                    
            end
            
                if length(obj.cData) ~= nPoints*3;
                    obj.cData = zeros(obj.NAverages,nPoints*3);
                end
                if obj.averaging
                    Nav = obj.NAverages;
                else
                    Nav = 1;
                end
                index = mod(iAcq-1,Nav)+1;
                
                obj.cData(index,1:length(Data3D(:))) = Data3D(:);
                obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
                obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
                
                obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
                acq_time = obj.GetParameter('ACQUISITIONTIME');
                obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*acq_time;
                obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time;
            
        end
    end
    
end

