classdef GradientEcho3DPrePulse < MRI_BlankSequence
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    %  Elena Diaz Caballero
    
    properties
        StartSlice;
        EndSlice;
        PhasesPerBatch = 20;
        StartPhase;
        EndPhase;
    end
    methods (Access=private)
        function CreateParameters(obj)
            InputParameters = {...
                'NPHASES','PHASETIME','ECHODELAY','REPETITIONDELAY','READOUTAMPLITUDE','READOUTAMPLITUDE2',...
                'SLICEBANDWIDTH','STARTPHASE',...
                'ENDPHASE','PREPULSE',...
                'GAUSSIANFILTER'...
                };
            
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {...
                'Number of Phases','Phase Time','Gradient Echo Delay','Repetition Delay','DEP Readout Amplitude','REP Readout Amplitude',...
                'Slice Bandwidth','Start Phase',...
                'End Phase','PrePulse Time',...
                'Gaussian'...
                })];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,...
                NaN...
                })];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,...
                0 ...
                })];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,...
                1 ...
                })];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {...
                '','us','us','ms','','',...
                'MHz','',...
                '','us',...
                ''...
                })];
           
            for k=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{k},InputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = GradientEcho3DPrePulse(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'Gradient Echo 3D w/PrePulse';
            obj.ProgramName = 'MRI_BlankSequence.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
            obj.SetParameter('PHASETIME',50*MyUnits.us);
            obj.SetParameter('ECHODELAY',20*MyUnits.us);
            obj.SetParameter('REPETITIONDELAY',50*MyUnits.ms);
            obj.SetParameter('READOUTAMPLITUDE',0.1);
            obj.SetParameter('READOUTAMPLITUDE2',0.1);
            obj.SetParameter('NPHASES',1)
%             obj.SetParameter('NSLICES',1)
            obj.SetParameter('STARTPHASE',-0.1)
            obj.SetParameter('ENDPHASE',0.1)
            obj.SetParameter('PREPULSE',100*MyUnits.us)
            obj.SetParameter('GAUSSIANFILTER',1);
            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
        end
        function CreateSequence( obj, CF )
                        
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            
            shimS = 0;
            if (CF)
                nSlices = 1;
                nPhases = 1;
                readout =0; % None of the gradients should be enabled if we are setting the central frequency (CF)
                slice =0;
                phase =0;
                spoiler =0;
            else
               nSlices = 1;
               if (abs(obj.EndPhase - obj.GetParameter('STARTPHASE')) - abs(obj.GetParameter('ENDPHASE') - obj.GetParameter('STARTPHASE'))) > 1e-6 %last batch may be smaller
                   nPhases = mod(obj.GetParameter('NPHASES'),obj.PhasesPerBatch);
                   obj.EndPhase = obj.GetParameter('ENDPHASE');
               else
                   nPhases = min(obj.GetParameter('NPHASES'),obj.PhasesPerBatch);
               end
                readout =obj.readout_en;
                slice =obj.slice_en;
                phase =obj.phase_en;
                spoiler =obj.spoiler_en;
            end
            obj.nline_reads=nPhases*nSlices;
            
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions=17*nPhases*nSlices;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
            
            if slice
                sliceamp = 1.0;
            else
                sliceamp = shimS;
            end
            
            % readout gradient amplitude calculation
            if( readout == 0 ) 
              dephasing_readout = shimR; 
              phasing_readout = shimR; 
            elseif( obj.GetParameter('PHASETIME') == 0 ) 
              dephasing_readout = shimR; 
              phasing_readout = min(1.0, max( -1.0, shimR -1.0 * obj.GetParameter('READOUTAMPLITUDE2')));
            elseif( acq_time  > 2 * (obj.GetParameter('PHASETIME') + 1e-6)  ) 
              dephasing_readout = min(1.0, max( -1.0, shimR +1.0 * obj.GetParameter('READOUTAMPLITUDE')));
              phasing_readout = min( 1.0, max( -1.0, shimR -2 *obj.GetParameter('READOUTAMPLITUDE2') * (obj.GetParameter('PHASETIME') + 1e-6) / ( acq_time )));
            else 
              phasing_readout = min(1.0, max( -1.0, shimR -1.0 * obj.GetParameter('READOUTAMPLITUDE2')));
              dephasing_readout = max( -1.0, min( 1.0, shimR + obj.GetParameter('READOUTAMPLITUDE') *  acq_time / (2 * (obj.GetParameter('PHASETIME') + 1e-6) )));
            end
            
             
              
              for phasei = 1:nPhases   
                  
                % SET FOR THE TOTAL NUMBER OF INSTRUCTIONS = 17  
                iline=17*((1-1)*nPhases+(phasei-1));
                
                %------ Blanking Delay and Shimming in all gradients (2 instructions)
                tn = 0; % count of instructions so far
                in = 2; % number of instructions in this segment
                obj.AMPs(iline+(tn+1:tn+in))     = [shimP shimR ];
                obj.DACs(iline+(tn+1:tn+in))     = [PP RR ];
                obj.WRITEs(iline+(tn+1:tn+in))   = [1 1];
                obj.UPDATEs(iline+(tn+1:tn+in))  = [1 1];
                obj.PhResets(iline+(tn+1:tn+in)) = [0 0];
                obj.ENVELOPEs(iline+(tn+1:tn+in))= [7 7]; % (7=no shape)
                obj.FLAGs(iline+(tn+1:tn+in))    = [1 1];
                obj.DELAYs(iline+(tn+1:tn+in))   = [0.1 obj.GetParameter('BLANKINGDELAY')*1e6]; %time in us
                tn = tn + in;
                
                %------ ADD IN PULSE TIMING FOR PRE-PULSE (2 instructions)
                in = 2;
                obj.AMPs(iline+(tn+1:tn+in))     = [sliceamp shimS];
                obj.DACs(iline+(tn+1:tn+in))     = [SS SS];
                obj.WRITEs(iline+(tn+1:tn+in))   = [1 1];
                obj.UPDATEs(iline+(tn+1:tn+in))  = [1 1];
                obj.PhResets(iline+(tn+1:tn+in)) = [0 1];
                obj.ENVELOPEs(iline+(tn+1:tn+in))= [7 7]; % (7=no shape)
                obj.FLAGs(iline+(tn+1:tn+in))    = [1 1];
                obj.DELAYs(iline+(tn+1:tn+in))   = [obj.GetParameter('PREPULSE')*1e6 0.1]; %time in us
                tn = tn + in;
                
                %------ RF pulse (1 instruction)         
                in = 1;
                obj.AMPs(iline+(tn+1:tn+in))     = 0.0;
                obj.DACs(iline+(tn+1:tn+in))     = 3;
                obj.TXs(iline+(tn+1:tn+in))      = 1;
                if (obj.RF_Shape) % Shaped RF pulse
                  obj.ENVELOPEs(iline+(tn+1:tn+in))= 0;
                else
                  obj.ENVELOPEs(iline+(tn+1:tn+in))= 7;
                end
                obj.FLAGs(iline+(tn+1:tn+in))    = 1;
                obj.OPCODEs(iline+(tn+1:tn+in))  = 0;
                obj.DELAYs(iline+(tn+1:tn+in))   = obj.GetParameter('PULSETIME')*1e6; %time in us
                tn = tn + in;
                
                %----- Transient Delay and setting the slice and readout dephasing gradients (2 instructions)
                in = 2;
                obj.AMPs(iline+(tn+1:tn+in))     = [shimS dephasing_readout];
                obj.DACs(iline+(tn+1:tn+in))     = [SS RR];
                obj.WRITEs(iline+(tn+1:tn+in))   = [1 1];
                obj.UPDATEs(iline+(tn+1:tn+in))  = [1 1];
                obj.ENVELOPEs(iline+(tn+1:tn+in))= [7 7]; % (7=no shape)
                obj.DELAYs(iline+(tn+1:tn+in))   = [obj.GetParameter('TRANSIENTTIME')*1e6 0.1]; %time in us
                tn = tn + in;                 
                
                % phase gradient amplitude calculation
                if( phase == 0 )
                  phase_amplitude = shimP;
                else 
                  phase_amplitude =  min(1.0 , max(-1.0,shimP + (obj.EndPhase - obj.StartPhase) * (phasei-1)/(nPhases-1) + obj.StartPhase));
                end

                %----- Phase gradient, rephasing readout gradient, reset slice and phase gradients and echo delay (4 instructions)
                in = 4;
                obj.AMPs(iline+(tn+1:tn+in))     = [phase_amplitude shimP phasing_readout shimS ];
                obj.DACs(iline+(tn+1:tn+in))     = [PP PP RR SS];
                obj.WRITEs(iline+(tn+1:tn+in))   = [1 1 1 1];
                obj.UPDATEs(iline+(tn+1:tn+in))  = [1 1 1 1];
                obj.ENVELOPEs(iline+(tn+1:tn+in))= [7 7 7 7]; % (7=no shape)
                obj.DELAYs(iline+(tn+1:tn+in))   = [(obj.GetParameter('PHASETIME')+obj.GetParameter('COILRISETIME'))*1e6 0.1 obj.GetParameter('COILRISETIME')*1e6 obj.GetParameter('ECHODELAY')*1e6]; %time in us
                tn = tn + in;
                
                %----- Rephasing Readout Gradient and Data Acquisition. Rewinding lobe for readout, slice and phase gradients (4 instructions)
                in = 4;
                obj.AMPs(iline+(tn+1:tn+in))     = [phasing_readout shimR shimS -phase_amplitude];
                obj.DACs(iline+(tn+1:tn+in))     = [RR RR SS PP];
                obj.WRITEs(iline+(tn+1:tn+in))   = [1 1 1 1];
                obj.UPDATEs(iline+(tn+1:tn+in))  = [1 1 1 1];
                obj.RXs(iline+(tn+1:tn+in))      = [1 0 0 0];
                obj.ENVELOPEs(iline+(tn+1:tn+in))= [7 7 7 7]; % (7=no shape)
                obj.DELAYs(iline+(tn+1:tn+in))   = [acq_time*1e6 obj.GetParameter('COILRISETIME')*1e6 0.1 (obj.GetParameter('PHASETIME')+obj.GetParameter('COILRISETIME'))*1e6]; %time in us
                tn = tn + in;
                
                %----- Spoiler for slice gradient (1 instruction)
                in = 1;
                if (spoiler)
                  obj.AMPs(iline+(tn+1:tn+in))     = obj.GetParameter('SPOILERAMP');
                else
                  obj.AMPs(iline+(tn+1:tn+in))     = shimS;
                end
                obj.DACs(iline+(tn+1:tn+in))     = SS;
                obj.WRITEs(iline+(tn+1:tn+in))   = 1;
                obj.UPDATEs(iline+(tn+1:tn+in))  = 1;
                obj.CLEARs(iline+(tn+1:tn+in))   = 1;
                obj.ENVELOPEs(iline+(tn+1:tn+in))= 7; % (7=no shape)
                obj.DELAYs(iline+(tn+1:tn+in))   = obj.GetParameter('SPOILERTIME')*1e6; %time in us
                tn = tn + in;
                
                %----- Repetition Delay (1 instruction)
                in = 1;
                obj.AMPs(iline+(tn+1:tn+in))     = 0.0;
                obj.DACs(iline+(tn+1:tn+in))     = 3;
                obj.WRITEs(iline+(tn+1:tn+in))   = 1;
                obj.UPDATEs(iline+(tn+1:tn+in))  = 1;
                obj.CLEARs(iline+(tn+1:tn+in))   = 1;
                obj.ENVELOPEs(iline+(tn+1:tn+in))= 7; % (7=no shape)
                obj.DELAYs(iline+(tn+1:tn+in))   = obj.GetParameter('REPETITIONDELAY')*1e6; %time in us
                tn = tn + in;
                
              end % end for nPhases
           
        end % end create gradient echo pulse sequence
        function [status,result] = Run(obj,iAcq)
                  
            obj.StartPhase = obj.GetParameter('STARTPHASE');
            obj.EndPhase = obj.GetParameter('ENDPHASE');
            nSlices = 1; 
            iSlice = 1;
            
            nPhases = obj.GetParameter('NPHASES');
            if nPhases > 1
                PhaseStep = (obj.EndPhase - obj.StartPhase)/(nPhases-1);
            else
                PhaseStep = 0;
            end
            nPhaseBatches = ceil(nPhases/obj.PhasesPerBatch);
            nPoints = obj.GetParameter('NPOINTS');
            Data3D = zeros(nPoints,nPhases,nSlices);
            cn = nSlices*nPhaseBatches;
            count = 0;
            if nPhaseBatches>1
                h_waitbar = waitbar(count/cn,'Please wait (por favor espere)...');
            end
            
                Data2D = [];
                for iBatch = 1:nPhaseBatches
                
                      if nPhases >1
                          obj.CreateSequence(1); % Create the gradient echo sequence for CF
                          obj.Sequence2String;
                          fname_CF = 'TempBatch_CF.bat';
                          obj.WriteBatchFile(fname_CF);
                          obj.CenterFrequency(fname_CF);
                      end
                      obj.SetParameter('LASTFREQUENCY',obj.GetParameter('FREQUENCY'));
                    
                    
                    % NOW RUN ACTUAL ACQUISTION
                    obj.StartPhase = obj.GetParameter('STARTPHASE') + (iBatch-1)*obj.PhasesPerBatch*PhaseStep;
                    obj.EndPhase = obj.StartPhase + (obj.PhasesPerBatch-1)*PhaseStep;
                    
                    obj.CreateSequence(0); % Create the normal gradient echo sequence
                    
                    obj.Sequence2String;
                    
                                       
                    fname = 'TempBatch.bat';
                    obj.WriteBatchFile(fname);
                    [status,result] = system(fullfile(obj.path_file,fname));

                    if status == 0 && ~isempty(result)
                        C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                        res = C{:};
                        obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                        obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                        obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                        obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                        obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                    end
                    
                    DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                    cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                    nPhasesInBatch=length(cDataBatch)/nPoints;
                    d = reshape(cDataBatch,nPoints,nPhasesInBatch);
                    Data2D = cat(2,Data2D,d);
                    count = count+1;
                    if nPhaseBatches>1
                        waitbar(count/cn,h_waitbar,sprintf('Por favor espere..... %5.2f %%',100*count/cn));
                    end
                end
                Data3D(:,:,iSlice) = Data2D;
                if length(obj.cData) ~= nPoints*nPhases*nSlices;
                    obj.cData = zeros(obj.NAverages,nPoints*nPhases*nSlices);
                end
                if obj.averaging
                    Nav = obj.NAverages;
                else
                    Nav = 1;
                end
                index = mod(iAcq-1,Nav)+1;
                if obj.autoPhasing
%                    pt = max(Data3D(:,ceil(size(Data3D,2)/2),ceil(size(Data3D,3)/2)));
                    pt = max(Data3D(:));
                    Data3D = Data3D.*exp(complex(0,-angle(pt)));
                end
                obj.cData(index,1:length(Data3D(:))) = Data3D(:);
                obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
                obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
                %    obj.fData1D = fftshift(ifft(fftshift(obj.cDataAv)));
                obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
                acq_time = obj.GetParameter('ACQUISITIONTIME');
                obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*acq_time/nPhases;
                obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time/nPhases;
            
            if nPhaseBatches>1
                delete(h_waitbar);
            end
        end
    end
    
end

