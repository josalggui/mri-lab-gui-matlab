classdef SE3D_wSLICE_wADC< MRI_BlankSequence
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        StartSlice;
        EndSlice;
        PhasesPerBatch = 20;
        PhasesinThisBatch = 1;
        StartPhase;
        EndPhase;
        nPhasesBatch;
        slice_ampl;
        slice_ampl_re;
        tau_filling_90L = 1e-7;
        tau_filling_90R = 1e-7;
        tau_filling_180 = 1e-7;
        calibrationactive = 0;
        calibrated = 0;
        Axis1;
        Axis2;
        Axis3;
        CAxis1;
        CAxis2;
        CAxis3;
        rdspeed_mult = 1;
        NP_calib = 1;
        ROmax = 0;
        CalData;
        FOV;
        gam = 42.57E6;
        slicefreq_axis;
        shimSF;
        rephase_slice_rat;
        T2D;
        rephasing_slice_time;
        rephasing_slice_time_180;
    end
    methods (Access=private)
        function CreateParameters(obj)
            InputParameters = {...
                'NPHASES','PHASETIME','TAUTIME','ECHODELAY','NOECHODELAY',...
                'REPETITIONDELAY','READOUTAMPLITUDE',...
                'STARTPHASE','ENDPHASE',...
                'STARTSLICE','ENDSLICE',...
                'SLICEAXIS','SLICEOFFSET','SLICESECTION',...
                'TIMEBETWEEN' ...
                };
            
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {...
                'Number of Phases','Phase Time','Tau Time','Readout Echo Delay', 'No-Readout Echo Delay',...
                'Repetition Delay','Readout Amplitude',...
                'Start Phase','End Phase',...
                'Start Slice','End Slice',...
                'Slice Freq Axis','Slice Offset','Slice Section',...
                'Time between Phases' ...
                })];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,NaN,...
                NaN ...
                })];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,NaN,...
                NaN ...
                })];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {...
                
                NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,NaN,...
                NaN ...
                })];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {...
                '','us','ms','us','us',...
                'ms','',...
                '','',...
                '','',...
                '','mm','mm',...
                'ms' ...
                })];
            OutputParameters = {
                'DEPHASINGREADOUTAMPLITUDE','PHASINGREADOUTAMPLITUDE','ACTUALPHASETIME'...
                };
            obj.OutputParNames = [containers.Map(obj.OutputParNames.keys(),obj.OutputParNames.values());containers.Map(OutputParameters,...
                {...
                'Dephasing Readout Amplitude', 'Phasing Readout Amplitude','Actual Phase Time'...
                })];
            obj.OutputParValues = [containers.Map(obj.OutputParValues.keys(),obj.OutputParValues.values());containers.Map(OutputParameters,...
                {...
                NaN,NaN,NaN...
                })];
            obj.OutputParUnits = [containers.Map(obj.OutputParUnits.keys(),obj.OutputParUnits.values());containers.Map(OutputParameters,...
                {...
                '','','us'...
                })];
            for i=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{i},InputParameters)) = [];
            end
            for i=1:length(obj.OutputParameters)
                OutputParameters(strcmp(obj.OutputParameters{i},OutputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
            obj.OutputParameters = [obj.OutputParameters,OutputParameters];
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = SE3D_wSLICE_wADC(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'SE 3D with ADC';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
            obj.SetParameter('NPHASES',1);
            obj.SetParameter('TAUTIME',2*MyUnits.ms);
            obj.SetParameter('PHASETIME',50*MyUnits.us);
            obj.SetParameter('ECHODELAY',20*MyUnits.us);
            obj.SetParameter('NOECHODELAY',10*MyUnits.us);
            obj.SetParameter('REPETITIONDELAY',50*MyUnits.ms);
            obj.SetParameter('READOUTAMPLITUDE',0.4);
            obj.SetParameter('STARTPHASE',-0.1);
            obj.SetParameter('ENDPHASE',0.1);
            
            obj.SetParameter('STARTSLICE',-0.1)
            obj.SetParameter('ENDSLICE',0.1)
            
            obj.SetParameter('SLICEOFFSET',0*MyUnits.mm);
            obj.SetParameter('SLICEAXIS','z');
            obj.SetParameter('SLICESECTION',0*MyUnits.mm);
            
            obj.SetParameter('TIMEBETWEEN',1*MyUnits.ms);
            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
            
            % Grab the calibration data if there
            try
                obj.CalData = GradientCalibrationData();
            catch
                obj.CalData.X_TD = 1;
                obj.CalData.Y_TD = 1;
                obj.CalData.Z_TD = 1;
                obj.CalData.X_TV = 1;
                obj.CalData.Y_TV = 1;
                obj.CalData.Z_TV = 1;
            end
            
        end
        function UpdateTETR( obj )
            
            [AcqT, start_measure, ~, ~, total_measure_time] = CalcTiming( obj );
            obj.TE = start_measure + 0.5*AcqT;
            obj.TR = total_measure_time + obj.GetParameter('REPETITIONDELAY');
            
        end 
        
        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence,
            %         "1", central frequency adjustment sequence,
            
            %blanking bits
            bbit = 1;            
            bbit2 = 2;
            crusher = 0.15;
            obj.rephase_slice_rat = 0.775;
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            SF = obj.slicefreq_axis;
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
                        
            CR = obj.GetParameter('COILRISETIME');
            
            [~,nPhases,readout,slice,phase,spoiler,~] = obj.ConfigureGradients(mode);
            
            % Calculate the number of line reads
            obj.nline_reads=nPhases;
            
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions=31*nPhases;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            
            obj.rephasing_slice_time = max( 1E-7, obj.rephase_slice_rat*obj.GetParameter('PULSETIME'));
            obj.rephasing_slice_time_180 = 1E-7;
%             obj.rephasing_slice_time_180 = max( 1E-7, obj.rephase_slice_rat*obj.GetParameter('PULSETIME')*2 );
            
            if( readout == 0 ) 
                  dephasing_readout = shimR; 
                  phasing_readout = shimR; 
            elseif( obj.GetParameter('TAUTIME') == 0 ) 
                  dephasing_readout = shimR; 
                  phasing_readout = min(1.0, max( -1.0, shimR +1.0 * obj.GetParameter('READOUTAMPLITUDE')));
%             elseif( acq_time  > 2 * (obj.GetParameter('PHASETIME') + 1e-6) ) 
%                   dephasing_readout = min(1.0, max( -1.0, shimR +1.0 * obj.GetParameter('READOUTAMPLITUDE')));
%                   phasing_readout = min( 1.0, max( -1.0, shimR +2 *obj.GetParameter('READOUTAMPLITUDE') * (obj.GetParameter('PHASETIME') + 1e-6) / ( acq_time)));
            else 
                  dephasing_readout = min(1.0, max( -1.0, shimR +1.0 * obj.GetParameter('READOUTAMPLITUDE')));
                  phasing_readout = min( 1.0, max( -1.0, shimR + 2.0 * obj.GetParameter('READOUTAMPLITUDE')  * ( 2.0*CR + obj.GetParameter('PHASETIME')) / ( acq_time + 2*CR) ));
            end
            
            obj.ROmax = abs(phasing_readout);
        
            %phase time limitation and time filling tau calculation (modification 8)
            phase_time = obj.GetParameter('PHASETIME');
            TauTime = obj.GetParameter('TAUTIME');
            
            tau_left =  ( 0.5*obj.GetParameter('PULSETIME') + CR + CR + obj.rephasing_slice_time + ...
                               CR + obj.GetParameter('TRANSIENTTIME') ) + (2e-7);
            tau_right = ( CR + obj.GetParameter('BLANKINGDELAY') + CR + obj.GetParameter('PULSETIME') ) + (2e-7);
            accumul_tau_time = 0.5*(TauTime - phase_time);
            
            obj.tau_filling_90L = 1E-7;
            obj.tau_filling_90L = max( obj.tau_filling_90L , accumul_tau_time - tau_left );
            
            obj.tau_filling_90R = 1E-7;
            obj.tau_filling_90R = max( obj.tau_filling_90R , accumul_tau_time - tau_right );
            
            if (obj.tau_filling_90L == 1e-7) || (obj.tau_filling_90L == 1e-7 )
                TauTime = obj.tau_filling_90L + obj.tau_filling_90R + phase_time + tau_left + tau_right;
                obj.SetParameter('TAUTIME',TauTime);
            end
            
            obj.tau_filling_180 = 1E-7;
            obj.tau_filling_180 = max( obj.tau_filling_180, TauTime - 0.5*acq_time - obj.GetParameter('PULSETIME') - 2.0*CR - obj.rephasing_slice_time_180 -1e-7 );
            
%             obj.SetParameter('ACTUALPHASETIME',phase_time);
            
            if( slice == 0 )
                slice_amplitude = shimS;
                slice_rephase_amplitude = shimS;
            else
                slice_amplitude =  obj.slice_ampl;
                slice_rephase_amplitude =  obj.slice_ampl_re;
            end
            
                       
            if abs( obj.GetParameter('SLICESECTION'))  > 0
                nlobes = 4;
                t0 = obj.GetParameter('PULSETIME')/(nlobes*2);
                dF = 1/t0;
                SG = dF / obj.gam / obj.GetParameter('SLICESECTION') ;
                
                slice_amplitude_F = min( 1.0, max( -1.0, obj.shimSF + SG*obj.T2D ) );
                slice_amplitude_F_rephase =  min( 1.0, max( -1.0, obj.shimSF - SG*obj.T2D ) );
                freqRF = 1;
                obj.SliceFreqOffset = obj.GetParameter('SLICEOFFSET')*MyUnits.mm*SG*obj.gam;
            else
                slice_amplitude_F = obj.shimSF;
                slice_amplitude_F_rephase = obj.shimSF;
                freqRF = 0;
                obj.SliceFreqOffset = 0;
            end
            
            %%% BEGIN THE PULSE PROGRAMMING
            iIns = 1;
            
            
            for phasei = 1:nPhases
                phaseo = 0;
                
                
                % phase gradient amplitude calculation
                if( phase == 0 ) || ( obj.GetParameter('NPHASES') < 2 )
                    phase_amplitude = shimP;
                    phase_rephase_amplitude = shimP;
                else
                    phase_amplitude =  min(1.0 , max(-1.0,shimP + (obj.EndPhase - obj.StartPhase) * (phasei-1)/(nPhases-1) + obj.StartPhase));
                    phase_rephase_amplitude = min(1.0 , max(-1.0,shimP - (obj.EndPhase - obj.StartPhase) * (phasei-1)/(nPhases-1) - obj.StartPhase));
                end
                
                % BLANKING DELAY and SHIMMING in all gradients AND PULSE
                % SENT TO ADC FOR ACQUISITION
                nIns  = 4; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [ shimP shimR shimS slice_amplitude_F ];
                    obj.DACs(vIns)     = [ PP RR SS SF ];
                    obj.WRITEs(vIns)   = [ 1 1 1 1 ];
                    obj.UPDATEs(vIns)  = [ 1 1 1 1 ];
                    obj.PhResets(vIns) = [ 0 0 0 1 ];
                    obj.PHASEs(vIns)   = [ phaseo phaseo phaseo phaseo ];
                    obj.ENVELOPEs(vIns)= [ 7 7 7 7 ]; % (7=no shape)
                    obj.FLAGs(vIns)    = [ 0 0 bbit bbit ];
                    obj.DELAYs(vIns)   = [0.1 0.1 obj.GetParameter('BLANKINGDELAY')*1e6 CR*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions
            
                %------ 90 degree RF pulse (1 instruction)
                nIns = 1; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.TXs(vIns)      = 1;
                    if (obj.RF_Shape) % Shaped RF pulse
                        obj.ENVELOPEs(vIns)= 0;
                    else
                        obj.ENVELOPEs(vIns)= 7;
                    end
                    obj.PHASEs(vIns)   = phaseo;
                    obj.FLAGs(vIns)    = bbit;
                    obj.OPCODEs(vIns)  = 0;
                    obj.FREQs(vIns)    = freqRF;
                    obj.DELAYs(vIns)   = obj.GetParameter('PULSETIME')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %----- Coil Fall time and Slice rephasing time and CR rise time
                nIns = 3; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [ obj.shimSF slice_amplitude_F_rephase obj.shimSF];
                    obj.DACs(vIns)     = [ SF SF SF];
                    obj.WRITEs(vIns)   = [1 1 1];
                    obj.UPDATEs(vIns)  = [1 1 1];
                    obj.PhResets(vIns) = [0 0 0];
                    obj.PHASEs(vIns)   = [phaseo phaseo phaseo];
                    obj.ENVELOPEs(vIns)= [7 7 7]; % (7=no shape)
                    obj.FLAGs(vIns)    = [0 0 0];
                    obj.DELAYs(vIns)   = [CR*1e6 obj.rephasing_slice_time*1e6 CR*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
             
             
                %----- Transient Delay and setting the slice and readout dephasing gradients (5 instructions)
                nIns = 7; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [ shimS slice_amplitude phase_amplitude dephasing_readout shimR shimP shimS];
                    obj.DACs(vIns)     = [ SS SS PP RR RR PP SS];
                    obj.WRITEs(vIns)   = [1 1 1 1 1 1 1];
                    obj.UPDATEs(vIns)  = [1 1 1 1 1 1 1];
                    obj.PhResets(vIns) = [0 0 0 0 0 0 0];
                    obj.PHASEs(vIns)   = [phaseo phaseo phaseo phaseo phaseo phaseo phaseo];
                    obj.ENVELOPEs(vIns)= [7 7 7 7 7 7 7]; % (7=no shape)
                    obj.FLAGs(vIns)    = [0 0 0 0 0 0 0];
                    obj.DELAYs(vIns)   = [(obj.GetParameter('TRANSIENTTIME')+obj.tau_filling_90L)*1e6 0.1 0.1 (obj.GetParameter('PHASETIME')+CR)*1e6 0.1 0.1 (CR+obj.tau_filling_90R)*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
             
                %------ 180 degree RF pulse WITH SLICE RISE AND FALL (3 instruction)
                nIns = 4; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [ shimS slice_amplitude_F+crusher slice_amplitude_F obj.shimSF+crusher ];
                    obj.DACs(vIns)     = [ SS SF SF SF ];
                    obj.WRITEs(vIns)   = [1 1 1 1 ];
                    obj.UPDATEs(vIns)  = [1 1 1 1 ];
                    obj.TXs(vIns)      = [ 0 0 1 0 ];
                    obj.PhResets(vIns) = [0 0 0 0];
                    if (obj.RF_Shape) % Shaped RF pulse
                        obj.ENVELOPEs(vIns)= [ 7  7 0 7 ];
                    else
                        obj.ENVELOPEs(vIns)= [ 7  7 7 7 ];
                    end
                    obj.PHASEs(vIns)   = [ phaseo phaseo 2 phaseo];
                    obj.FREQs(vIns)    = [ 0 0 freqRF 0 ];
                    obj.FLAGs(vIns)    = [ 0 0 bbit 0 ];
                    obj.OPCODEs(vIns)  = [ 0 0 0 0 ];
                    obj.DELAYs(vIns)   = [ obj.GetParameter('BLANKINGDELAY')*1e6 CR*1e6 2*obj.GetParameter('PULSETIME')*1e6 CR*1e6 ]; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
    
                %----- Rephasing Readout Gradient and Data Acquisition. Rewinding lobe for readout, slice and phase gradients (4 instructions)
                nIns = 6;
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [slice_amplitude_F_rephase obj.shimSF obj.shimSF phasing_readout phasing_readout shimR ];
                    obj.DACs(vIns)     = [SF SF SF RR RR RR];
                    obj.WRITEs(vIns)   = [1 1 1 1 1 1];
                    obj.UPDATEs(vIns)  = [1 1 1 1 1 1];
                    obj.PHASEs(vIns)   = [phaseo phaseo phaseo phaseo phaseo phaseo];
                    obj.RXs(vIns)      = [0 0 0 0 1 0];
                    obj.FREQs(vIns)    = [0 0 0 0 0 freqRF];
                    obj.ENVELOPEs(vIns)= [7 7 7 7 7 7]; % (7=no shape)
                    obj.DELAYs(vIns)   = [obj.rephasing_slice_time_180*1e6 0.1 (obj.tau_filling_180+obj.GetParameter('NOECHODELAY'))*1e6 (CR+obj.GetParameter('ECHODELAY'))*1e6 acq_time*1e6 0.1]; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %----- Rephasing Phase and Slice (4 instructions)
                nIns = 4;
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [ phase_rephase_amplitude slice_rephase_amplitude shimP shimS];
                    obj.DACs(vIns)     = [PP SS PP SS];
                    obj.WRITEs(vIns)   = [1 1 1 1 ];
                    obj.UPDATEs(vIns)  = [1 1 1 1 ];
                    obj.PHASEs(vIns)   = [ phaseo phaseo phaseo phaseo ];
                    obj.RXs(vIns)      = [0 0 0 0 ];
                    obj.ENVELOPEs(vIns)= [7 7 7 7 ]; % (7=no shape)
                    obj.DELAYs(vIns)   = [0.1 (obj.GetParameter('PHASETIME')+CR)*1e6 0.1 0.1]; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %----- Spoiler for slice gradient (1 instruction)
                nIns = 1;
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    if (spoiler)
                        obj.AMPs(vIns)     = obj.GetParameter('SPOILERAMP');
                    else
                        obj.AMPs(vIns)     = shimS;
                    end
                    obj.DACs(vIns)     = SS;
                    obj.WRITEs(vIns)   = 1;
                    obj.UPDATEs(vIns)  = 1;
                    obj.CLEARs(vIns)   = 1;
                    obj.PHASEs(vIns)   = phaseo;
                    obj.FLAGs(vIns)    = 0;
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.DELAYs(vIns)   = obj.GetParameter('SPOILERTIME')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %----- Repetition Delay (1 instruction)
                nIns = 1;
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.WRITEs(vIns)   = 1;
                    obj.UPDATEs(vIns)  = 1;
                    obj.PHASEs(vIns)  = phaseo;
                    obj.CLEARs(vIns)   = 1;
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.DELAYs(vIns)   = obj.GetParameter('REPETITIONDELAY')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
            end % end for nPhases
            %             end %end for slicei
            
            
            
        end % end create spin echo pulse sequence

        function [status,result] = Run(obj,iAcq)
            global MRIDATA
            
            MRIDATA.ADC = [];
            
            % grab which axis is the slice freq axis
            switch num2str(obj.GetParameter('SLICEAXIS'))
                case 'x'
                    obj.slicefreq_axis = 2; obj.shimSF = obj.GetParameter('SHIMREADOUT');obj.T2D = obj.CalData.X_TD;
                case 'y'
                    obj.slicefreq_axis = 0; obj.shimSF = obj.GetParameter('SHIMPHASE');obj.T2D = obj.CalData.Y_TD;
                case 'z'
                    obj.slicefreq_axis = 1; obj.shimSF = obj.GetParameter('SHIMSLICE');obj.T2D = obj.CalData.Z_TD;
                otherwise
                    obj.slicefreq_axis = 1; obj.shimSF = obj.GetParameter('SHIMSLICE');obj.T2D = obj.CalData.Z_TD;
            end
            
            obj.StartSlice = obj.GetParameter('STARTSLICE');
            obj.EndSlice = obj.GetParameter('ENDSLICE');
            obj.StartPhase = obj.GetParameter('STARTPHASE');
            obj.EndPhase = obj.GetParameter('ENDPHASE');
            nSlices = obj.GetParameter('NSLICES');
            if nSlices > 1
                SliceStep = (obj.EndSlice - obj.StartSlice)/(nSlices-1);
            else
                SliceStep = 0;
            end
            nPhases = obj.GetParameter('NPHASES');
            if nPhases > 1
                PhaseStep = (obj.EndPhase - obj.StartPhase)/(nPhases-1);
            else
                PhaseStep = 0;
            end
            nPhaseBatches = ceil(nPhases/obj.PhasesPerBatch);
            nPoints = obj.GetParameter('NPOINTS');
            Data3D = zeros(nPoints,nPhases,nSlices);
            
            cn = nSlices*nPhaseBatches;
            count = 0;
            if nPhaseBatches>1
                h_waitbar = waitbar(count/cn,'Please wait (por favor espere)...');
            end
            
            if isfield(MRIDATA,'Data3Dnew')
                if ( isequal(size(MRIDATA.Data3Dnew),[nPoints, nPhases, nSlices, obj.NAverages])) || (iAcq == 1)
                    MRIDATA.Data3Dnew = [];
                end
                else
                    MRIDATA.Data3Dnew = [];
            end
            
            pausingtime = obj.GetParameter('TIMEBETWEEN');
            
            % start loops
            for iSlice = 1:nSlices
                obj.StartSlice = obj.GetParameter('STARTSLICE') + (iSlice-1)*SliceStep;
                obj.EndSlice = obj.StartSlice;
                
                [shimS,~,~]=SelectAxes(obj);
                
                if (nSlices == 1)
                    obj.slice_ampl = obj.StartSlice;
                    obj.slice_ampl_re = -obj.StartSlice;
                else
                    obj.slice_ampl =  min(1.0 , max(-1.0,shimS + (obj.EndSlice - obj.StartSlice) * (iSlice-1)/(nSlices-1) + obj.StartSlice));
                    obj.slice_ampl_re =  min(1.0 , max(-1.0,shimS - (obj.EndSlice - obj.StartSlice) * (iSlice-1)/(nSlices-1) - obj.StartSlice));
                end
                
                Data2D = [];
                
                    
                for iBatch = 1:nPhaseBatches
                    obj.StartPhase = obj.GetParameter('STARTPHASE') + (iBatch-1)*obj.PhasesPerBatch*PhaseStep;
                    if iBatch == nPhaseBatches
                        obj.nPhasesBatch = obj.GetParameter('NPHASES') - obj.PhasesPerBatch*(iBatch-1);
                        obj.EndPhase = obj.GetParameter('ENDPHASE');
                        obj.PhasesinThisBatch = obj.nPhasesBatch;
                    else
                        obj.nPhasesBatch = obj.PhasesPerBatch;
                        obj.EndPhase = obj.StartPhase + (obj.PhasesPerBatch-1)*PhaseStep;
                        obj.PhasesinThisBatch = obj.nPhasesBatch;
                    end
                    
                    if nPhases >1
                        obj.CreateSequence(1); % Create the spin echo sequence for CF
                        obj.Sequence2String;
                        fname_CF = 'TempBatch_CF.bat';
                        obj.WriteBatchFile(fname_CF);
                        obj.CenterFrequency(fname_CF);
                    end
                    obj.SetParameter('LASTFREQUENCY',obj.GetParameter('FREQUENCY'));
                    
                    % NOW RUN ACTUAL ACQUISTION
                    
                    obj.CreateSequence(0); % Create the normal spin echo sequence
                    obj.Sequence2String;
                    
                    fname = 'TempBatch.bat';
                    obj.WriteBatchFile(fname);
                    
                    if obj.calibrationactive
                            % Right before we start the process of running the
                            % script, FIRST implement the ADC board to monitor for
                            % the sequence
                            % now add prompt
                            obj.calibrated = 0;
                            
%                             if ical == 1 && iBatch == 1
%                                 waitdlg = warndlg({'Please connect only the READOUT measurement device.'});
%                                 uiwait(waitdlg);
%                             elseif ical == 2 && iBatch == 1
%                                 waitdlg = warndlg({'Please connect only the PHASE measurement device.'});
%                                 uiwait(waitdlg);
%                             end
                            fname2 = 'RunADC_3.bat';
                            obj.WriteCalibBatchFile( fname2 );
                            system(char(['start /min ',fullfile(obj.path_file,fname2),' ^& exit']));
                            pause(1);
                    end
                    
                    
                    [status,result] = system(fullfile(obj.path_file,fname));
                    %            [status,result] = system(fname,'-echo');
                    %delete(fname);% delete the batch file
                    if status == 0 && ~isempty(result)
                        C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                        res = C{:};
                        obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                        obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                        obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                        obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                        obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                        obj.SetParameter('ACTUALPHASETIME',sscanf(res{6},'Actual Phase Time: %f us')*MyUnits.us);
                    end
                    
                    DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                    cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                    nPhasesInBatch=length(cDataBatch)/nPoints;
                    d = reshape(cDataBatch,nPoints,nPhasesInBatch);
                    
                    if obj.calibrationactive
                        % Now load in the data about the position of the points
                        MRIDATA.ADC(iBatch+nPhaseBatches*(iSlice-1)).CMatrix1 = ReadADCData;
                        MRIDATA.ADC(iBatch+nPhaseBatches*(iSlice-1)).nPhases = obj.PhasesinThisBatch;
                    end
                    
                    Data2D = cat(2,Data2D,d);
                    
                    count = count+1;
                    if nPhaseBatches>1
                        waitbar(count/cn,h_waitbar,sprintf('Por favor espere..... %5.2f %%',100*count/cn));
                    end
                    
                    % add pausing step to ensure proper encoding for each line
                    pause( pausingtime );
                end
                
                if length(obj.cData) ~= nPoints*nPhases*nSlices;
                    obj.cData = zeros(obj.NAverages,nPoints*nPhases*nSlices);
                    obj.calibrated = 0;
                end
                
                d1 = Data2D;
                Data3D(:,:,iSlice) = d1;
                
            end
            
                % HERE WE MUST ADD THE CALIBRATION IF IT WAS ADDED
                ndim = sum( [ obj.readout_en, obj.phase_en, obj.slice_en ] );
                maxPH = max( abs([ obj.GetParameter('STARTPHASE'),  obj.GetParameter('ENDPHASE') ]));
                
                [m, n, o] = size(Data3D);
                AX1v = linspace(-obj.ROmax,obj.ROmax,m);
                AX2v = linspace(-maxPH,maxPH,n);
                AX3v = linspace(-1,1,o);
                
                
%                 Data3D = Data3D(:);
                
                if obj.calibrationactive == 1
                    
                    try
                        obj.CalData = GradientCalibrationData();
                    catch
                        obj.CalData.X_TD = 1;
                        obj.CalData.Y_TD = 1;
                        obj.CalData.Z_TD = 1;
                        obj.CalData.X_TV = 1;
                        obj.CalData.Y_TV = 1;
                        obj.CalData.Z_TV = 1;
                    end
            
                    obj.RunCalibrationAxisGeneration;
                    
                    switch obj.GetParameter('READOUT')
                        case 'x'
                            MRIDATA.Calib.Ax1 = obj.CAxis1 * obj.CalData.X_TV;
                        case 'y'
                            MRIDATA.Calib.Ax1 = obj.CAxis2 * obj.CalData.Y_TV;
                        case 'z'
                            MRIDATA.Calib.Ax1 = obj.CAxis3 * obj.CalData.Z_TV;
                    end
                    switch obj.GetParameter('PHASE')
                        case 'x'
                            MRIDATA.Calib.Ax2 = obj.CAxis1 * obj.CalData.X_TV;
                        case 'y'
                            MRIDATA.Calib.Ax2 = obj.CAxis2 * obj.CalData.Y_TV;
                        case 'z'
                            MRIDATA.Calib.Ax2 = obj.CAxis3 * obj.CalData.Z_TV;
                    end
                    switch obj.GetParameter('SLICE')
                        case 'x'
                            MRIDATA.Calib.Ax3 = obj.CAxis1 * obj.CalData.X_TV;
                        case 'y'
                            MRIDATA.Calib.Ax3 = obj.CAxis2 * obj.CalData.Y_TV;
                        case 'z'
                            MRIDATA.Calib.Ax3 = obj.CAxis3 * obj.CalData.Z_TV;
                    end
                    
                    AX1v = linspace(min(MRIDATA.Calib.Ax1),max(MRIDATA.Calib.Ax1),m);
                    AX2v = linspace(min(MRIDATA.Calib.Ax2),max(MRIDATA.Calib.Ax2),n);
                    AX3v = linspace(min(MRIDATA.Calib.Ax3),max(MRIDATA.Calib.Ax3),o);
                    
                    MRIDATA.Calib.Dat = Data3D(:);
                    obj.calibrated = 1;
                end
                
                [ AX1, AX2, AX3 ] = ndgrid( AX1v, AX2v, AX3v );
                obj.Axis1 = AX1(:);
                obj.Axis2 = AX2(:);
                obj.Axis3 = AX3(:);
               
                % create the resolution and FOV constructs for image creation
                
                obj.FOV.RO = (max(AX1v)-min(AX1v))/max((m-1),1);
                obj.FOV.PH = (max(AX2v)-min(AX2v))/max((n-1),1);
                obj.FOV.SL = (max(AX3v)-min(AX3v))/max((o-1),1);
                
                if obj.calibrated == 1
                    obj.Axis1 = MRIDATA.Calib.Ax1;
                    obj.Axis2 = MRIDATA.Calib.Ax2;
                    obj.Axis3 = MRIDATA.Calib.Ax3;
                end
                
                if ndim == 3
                    
                    
                    warning off;
                    Data3D_Fun = scatteredInterpolant(obj.Axis1,obj.Axis2,obj.Axis3,...
                                                      Data3D(:),'natural','none');
                    warning on;
                    Data3D_use = Data3D_Fun(AX1,AX2,AX3);
                    Data3D_use(isnan(Data3D_use)) = 0;
                    
                    
                elseif ndim == 2
%                     warning off;
%                     Data3D_Fun = scatteredInterpolant(obj.Axis1,obj.Axis2,obj.Axis3,...
%                                                       Data3D(:),'natural','none');
%                     warning on;
%                     Data3D_use = Data3D_Fun(AX1,AX2,0.*AX3+mean(obj.Axis3(:)));
%                     Data3D_use(isnan(Data3D_use)) = 0;
                    warning off;
                    Data3D_Fun = scatteredInterpolant(obj.Axis1,obj.Axis2,...
                                                      Data3D(:),'natural','nearest');
                    warning on;
                    Data3D_use = Data3D_Fun(AX1,AX2);
                    Data3D_use(isnan(Data3D_use)) = 0;
                    
                elseif ndim == 1
                    if nSlices > 1
                        axis_use = obj.Axis3;
                        axis_find = AX3;
                    elseif nPhases > 1
                        axis_use = obj.Axis2;
                        axis_find = AX2;
                    else
                        axis_use = obj.Axis1;
                        axis_find = AX1;
                    end
                    Data3D_use = interp1(axis_use, Data3D(:), axis_find, 'spline',0);
                else
                    Data3D_use = Data3D(:);
                end
                
                
                
                if obj.averaging
                    Nav = obj.NAverages;
                else
                    Nav = 1;
                end
                index = mod(iAcq-1,Nav)+1;
                if obj.autoPhasing
                    %                    pt = max(Data3D(:,ceil(size(Data3D,2)/2),ceil(size(Data3D,3)/2)));
                    pt = max(Data3D_use(:));
                    Data3D_use = Data3D_use.*exp(complex(0,-angle(pt)));
                end
                obj.cData(index,1:length(Data3D_use(:))) = Data3D_use(:);
                
                obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
                obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
                %    obj.fData1D = fftshift(ifft(fftshift(obj.cDataAv)));
                obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
                acq_time = obj.GetParameter('ACQUISITIONTIME');
                obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*acq_time/nPhases;
                obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time/nPhases;
            
%             MRIDATA.Data3Dnew(:,:,:,index) = Data3D;
            
            if nPhaseBatches>1
                delete(h_waitbar);
            end
        end
        function RunCalibrationAxisGeneration( obj )
            % this will run the calibration on the actively stored data
            global MRIDATA
            
            obj.CAxis1 = [];
            obj.CAxis2 = [];
            obj.CAxis3 = [];
            
            % keep in mind that this is for only 2 axis at the moment
            % first axis is the readout direction, second is the phase
            % direction
            for iADC = 1:numel(MRIDATA.ADC)
                
                    Full_TriggerAxis = MRIDATA.ADC(iADC).CMatrix1(1,:)';
                    Full_DAxis1 = MRIDATA.ADC(iADC).CMatrix1(2,:)';
                    Full_DAxis2 = MRIDATA.ADC(iADC).CMatrix1(3,:)';
                    Full_DAxis3 = MRIDATA.ADC(iADC).CMatrix1(4,:)';
                    
                    % pulse parameters
                    
                    [AcqT, After2RF, start_measure, end_measure, after_RF, total_measure_time] = obj.CalcTiming;
                                       
                    nP = MRIDATA.ADC(iADC).nPhases;
                    NP = obj.GetParameter('NPOINTS');
                    
                    % measurement parameters
                    RdSpeed = (40E6)/obj.rdspeed_mult;
                    dt = 1/RdSpeed;

                    % measure inds
                    After_2ndRF_ind = floor(After2RF / dt);
                    start_ind = floor(start_measure/dt);
                    end_ind   = obj.NP_calib;
                    after_RF_ind = floor(after_RF/dt);
                    
                    % find the ind when the trigger is below a certain threshold that is
                    % considered as '0'
                    trigthresh_up   = 1E-1;
                    Full_TriggerAxis ( Full_TriggerAxis > trigthresh_up )  =  1;
                    Full_TriggerAxis = round(Full_TriggerAxis);
                    
                    % starting offset
                    DAxis1Sum = zeros(NP,nP);
                    DAxis2Sum = zeros(NP,nP);
                    DAxis3Sum = zeros(NP,nP);
                    
%                     dfilter = fdesign.lowpass('N,Fc',1,1,100);
%                     Hd = design(dfilter);
                    
                    for ip = 1:nP
                        % Rewrite the axes
                        TriggerAxis = Full_TriggerAxis(1+(ip-1)*obj.NP_calib:ip*obj.NP_calib);
                        DAxis1 = Full_DAxis1(1+(ip-1)*obj.NP_calib:ip*obj.NP_calib);
                        DAxis2 = Full_DAxis2(1+(ip-1)*obj.NP_calib:ip*obj.NP_calib);
                        DAxis3 = Full_DAxis3(1+(ip-1)*obj.NP_calib:ip*obj.NP_calib);
                        
                        
                        % find the ind where the start is and then where the end is
                        inds = find( TriggerAxis == 1, 1, 'first' );
                        inde = find( TriggerAxis(inds:end) == 0, 1, 'first' )+inds-1;
                        
                        % Grab the 'zero' amount - remove the shimming bias
                        d1shim = 0;%median( DAxis1(inds:inde) );
                        d2shim = 0;%median( DAxis2(inds:inde) );
                        d3shim = 0;%median( DAxis3(inds:inde) );
                        
                        
                        % grab data and smooth it
%                         SelectedData1 = filter( Hd, DAxis1(inde+after_RF_ind:inde+end_ind)-d1shim );
                        SelectedData1 = [ -(DAxis1(inde+after_RF_ind:inde+after_RF_ind+After_2ndRF_ind)-d1shim);...
                                           (DAxis1(inde+after_RF_ind+After_2ndRF_ind+1:end)-d1shim) ];
                        
%                         SelectedData1 = smooth( DAxis1(inde+after_RF_ind:inde+end_ind)-d1shim,11, 'rloess' );
                        % now sum the data to determine where in KSpace you are
                        DAxis1SumFull = obj.gam * ( cumsum( SelectedData1 * dt ) );
                        DAxis1SumSelect = DAxis1SumFull(start_ind-after_RF_ind:end);
                        D1interp = interp1(linspace(1,numel(DAxis1SumSelect),numel(DAxis1SumSelect)),...
                                          DAxis1SumSelect, linspace(1,numel(DAxis1SumSelect),NP),'pchip');
                        
                        DAxis1Sum(:,ip) = D1interp;
                        
                        % grab data and smooth it
%                         SelectedData2 = filter(Hd, DAxis2(inde+after_RF_ind:inde+end_ind)-d2shim );
                        SelectedData2 = [ -(DAxis2(inde+after_RF_ind:inde+after_RF_ind+After_2ndRF_ind)-d2shim);...
                                           (DAxis2(inde+after_RF_ind+After_2ndRF_ind+1:end)-d2shim) ];
%                         SelectedData2 = smooth( DAxis2(inde+after_RF_ind:inde+end_ind)-d2shim,11, 'rloess' );
                        % now sum the data to determine where in KSpace you are
                        DAxis2SumFull = obj.gam * ( cumsum( SelectedData2 * dt ));
                        DAxis2SumSelect = DAxis2SumFull(start_ind-after_RF_ind:end);
                        D2interp = interp1(linspace(1,numel(DAxis2SumSelect),numel(DAxis2SumSelect)),...
                                          DAxis2SumSelect, linspace(1,numel(DAxis2SumSelect),NP),'pchip');
                        
                        DAxis2Sum(:,ip) = D2interp;
                        
                        % grab data and smooth it
% %                         SelectedData3 = filter(Hd, DAxis3(inde+after_RF_ind:inde+end_ind)-d3shim );
                        SelectedData3 = [ -(DAxis3(inde+after_RF_ind:inde+after_RF_ind+After_2ndRF_ind)-d3shim);...
                                           (DAxis3(inde+after_RF_ind+After_2ndRF_ind+1:end)-d3shim) ];
%                         SelectedData3 = smooth( DAxis3(inde+after_RF_ind:inde+end_ind)-d3shim,11, 'rloess' );
                        % now sum the data to determine where in KSpace you are
                        DAxis3SumFull = obj.gam * ( cumsum( SelectedData3 * dt ));
                        DAxis3SumSelect = DAxis3SumFull(start_ind-after_RF_ind:end);
                        D3interp = interp1(linspace(1,numel(DAxis3SumSelect),numel(DAxis3SumSelect)),...
                                          DAxis3SumSelect, linspace(1,numel(DAxis3SumSelect),NP),'pchip');
                        
                        DAxis3Sum(:,ip) = D3interp;
                        
                    end
                    
                        obj.CAxis1 = cat(2, obj.CAxis1, DAxis1Sum);
                        obj.CAxis2 = cat(2, obj.CAxis2, DAxis2Sum);
                        obj.CAxis3 = cat(2, obj.CAxis3, DAxis3Sum);
                        
            end
            
%             reform axis values
            obj.CAxis1 = obj.CAxis1(:);
            obj.CAxis2 = obj.CAxis2(:);
            obj.CAxis3 = obj.CAxis3(:);
            
        end
        
        
        function [AcqT, after2ndRF, start_measure, end_measure, after_RF, total_measure_time] = CalcTiming( obj )
            
            % pulse parameters
                    BD = obj.GetParameter('BLANKINGDELAY');
                    RF = obj.GetParameter('PULSETIME');
                    SRP = obj.rephase_slice_rat*obj.GetParameter('PULSETIME');
                    TT = obj.GetParameter('TRANSIENTTIME');
                    Tau = obj.GetParameter('TAUTIME');
                    AddP = 0.1E-6;
                    PT = obj.GetParameter('PHASETIME');
                    CR = obj.GetParameter('COILRISETIME');
                    ED = obj.GetParameter('ECHODELAY');
                    NED = obj.GetParameter('NOECHODELAY');
                    NP = obj.GetParameter('NPOINTS');
                    SW = obj.GetParameter('SPECTRALWIDTH');
                    AcqT = NP/SW;
                    

                    % specify start and end of reading
                    during_RF = BD + RF + CR;
                    after_RF = 0;%1*BD+RF;
                    after2ndRF = Tau -0.5*RF;
                    start_measure = obj.rephasing_slice_time_180 + after2ndRF + RF + obj.tau_filling_180 + NED + ED + CR + 1*AddP + CR;
%                     start_measure = after2ndRF + RF + obj.tau_filling_180 + NED + ED + CR + 2*AddP + CR;
                    end_measure   = start_measure+AcqT;
                    total_measure_time = end_measure+during_RF;

        end
    end
    
end

