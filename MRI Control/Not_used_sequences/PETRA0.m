classdef PETRA0 < MRI_BlankSequence
    %%
    
    % PETRA CODE
    
    %*******************************************************
    %** J. M. Algar�n                                     **
    %** CSIC/UPV                                          **
    %** I3M                                               **
    %** Avda. dels Tarongers, 12,46022, Valencia, (Spain) **
    %** Tel: +34 960 728 111                              **
    %** email: josalggui@i3m.upv.es                       **
    %*******************************************************
    
    %%
    
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    %  Elena Diaz Caballero
    properties
        PhasesPerBatch = 20;
        nPhasesBatch;
        Axis1Vector;
        Axis2Vector;
        Axis3Vector;
        StartSlice;
        lastBatch;
        EndSlice;
        StartPhase;
        EndPhase;
        Stage;              % ZTE -> 1, Single Point ->2 or Prescan -> 0
        AcquisitionTime;
        slice_ampl;
        nSteps = 5;
        readoutAmplitudeP = 0;
        phaseAmplitudeP = 0;
        sliceAmplitudeP = 0;
        repetitionDelay;
    end
    methods (Access=private)
        function CreateParameters(obj)
            
            % HELP %
            % I include here new inputs to discriminate between
            % acquistition and regruidding.
            % -> NPHASES, NREADOUTS and NSLICES are for regridding
            % -> NPOINTS, NCIRCUNFERENCES, NLINESPERCIRCUNFERENCES are for
            %    aqcuisition.
            CreateOneParameter(obj,'NREADOUTS','Nx','',50)
            CreateOneParameter(obj,'NPHASES','Ny','',50)
            CreateOneParameter(obj,'NSLICES','Nz','',50)
            CreateOneParameter(obj,'FOVX','FOV x','mm',20*MyUnits.mm)
            CreateOneParameter(obj,'FOVY','FOV y','mm',20*MyUnits.mm)
            CreateOneParameter(obj,'FOVZ','FOV z','mm',20*MyUnits.mm)
            CreateOneParameter(obj,'TMAX','T max','us',70*MyUnits.us)
            CreateOneParameter(obj,'REPETITIONTIME','TR','ms',500*MyUnits.ms)
            CreateOneParameter(obj,'UNDERSAMPLING','Undersampling','',1);
            CreateOneParameter(obj,'JUMPINGFACTOR','Jumping factor','',0);
            CreateOneParameter(obj,'TRANSIENTTIME','Dead time','us',0.1e-6);
            CreateOneParameter(obj,'REPETITIONDELAY','Repetition Delay','ms',0.1);
            CreateOneParameter(obj,'DELTAX','X-axis deviation','mm',0);
            CreateOneParameter(obj,'DELTAY','Y-axis deviation','mm',0);
            CreateOneParameter(obj,'DELTAZ','Z-axis deviation','mm',0);
            CreateOneParameter(obj,'PADDING','Padding Factor','',1);
            
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
                'NPOINTS','SPECTRALWIDTH','SPOILERAMP','REPETITIONDELAY',...
                'SPOILERTIME'};
            
            % Reorganizes InputParameters
            obj.MoveParameter(6,20);
            obj.MoveParameter(24,12);
            obj.MoveParameter(25,13);
            obj.MoveParameter(19,15);
            obj.MoveParameter(16,17);
            obj.MoveParameter(29,26);
            obj.MoveParameter(30,27);
            obj.MoveParameter(31,28);
            
        end
        
        %         function MoveParameter(obj,x,y)
        %             xx = obj.InputParameters(x);
        %             output = obj.InputParameters;
        %             output(x) = [];
        %             output(y+1:end+1) = output(y:end);
        %             output(y) = xx;
        %             obj.InputParameters = output;
        %         end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = PETRA0(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'PETRAO';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
        end
        function UpdateTETR(obj)
            obj.TE = 0;
            obj.TR = obj.GetParameter('REPETITIONTIME');
            nLPC = ceil(max([obj.GetParameter('NREADOUTS'),obj.GetParameter('NPHASES')])*pi/obj.GetParameter('UNDERSAMPLING'));
            nLPC = max(nLPC-mod(nLPC,2),1);
            nCir = max(ceil(obj.GetParameter('NSLICES')*pi/2)-1,1);
            disp([nLPC nCir])
            
            obj.TTotal = obj.TR * obj.GetParameter('NSCANS')*nLPC*nCir;
        end
        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence
            %         "1", prescan for ring down calibration
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);

            % Set the number of lines to be read
            nMisses = obj.GetParameter('JUMPINGFACTOR');
            
            if(mode==1)
                [~,nPhases,~,~,~,~,~] = obj.ConfigureGradients(mode);
                obj.Axis1Vector = 0;
                obj.Axis2Vector = 0;
                obj.Axis3Vector = 0;
            elseif(mode==0)
                [~,~,~,~,~,~,~] = obj.ConfigureGradients(mode);
                nPhases = obj.nPhasesBatch+nMisses;
            end
            
            % Calculate the number of line reads
            obj.nline_reads=nPhases-nMisses;
            
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions= (3*obj.nSteps+5)*nPhases;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6;
            crt = obj.GetParameter('COILRISETIME')*1e6;
            blk = obj.GetParameter('BLANKINGDELAY')*1e6;
            iIns = 1;
            pulseTime = obj.GetParameter('PULSETIME')*1e6;
            deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
            repDelayTime = obj.GetParameter('REPETITIONDELAY')*1e6;
            spoilerTime =  obj.GetParameter('SPOILERTIME')*1e6;
            maxGrad = max([max(obj.Axis1Vector),max(obj.Axis2Vector),max(obj.Axis3Vector)]);
            for phasei = 1:nPhases
                if(phasei<=nMisses)
                    readoutAmplitude = 0;
                    phaseAmplitude = 0;
                    sliceAmplitude = 0;
                else
                    readoutAmplitude = obj.Axis1Vector(phasei-nMisses);
                    phaseAmplitude = obj.Axis2Vector(phasei-nMisses);
                    sliceAmplitude = obj.Axis3Vector(phasei-nMisses);
                end
                
                %% Activate the total gradient (3 instrucctions)
                for ii=1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimR+obj.readoutAmplitudeP+(readoutAmplitude-obj.readoutAmplitudeP)/obj.nSteps*ii,RR,0.1);
                    iIns = obj.PulseGradient(iIns,shimP+obj.phaseAmplitudeP+(phaseAmplitude-obj.phaseAmplitudeP)/obj.nSteps*ii,PP,0.1);
                    iIns = obj.PulseGradient(iIns,shimS+obj.sliceAmplitudeP+(sliceAmplitude-obj.sliceAmplitudeP)/obj.nSteps*ii,SS,crt/obj.nSteps-0.2);
                end
                obj.readoutAmplitudeP = readoutAmplitude;
                obj.phaseAmplitudeP = phaseAmplitude;
                obj.sliceAmplitudeP = sliceAmplitude;
                
                %% RF pulse (3 instructions)
                iIns = obj.CreatePulse(iIns,0,blk,pulseTime,deadTime);
                if(obj.Stage==0)
                    obj.TXs(iIns-2) = 0;
                end
                
                %% Acquisition
                if(phasei<=nMisses)
                    iIns = obj.RepetitionDelay(iIns,0,acq_time+spoilerTime);
                else
                    iIns = obj.Acquisition(iIns,acq_time+spoilerTime);
                end
                
                %% Repetition Delay (1 instruction)
                if(obj.lastBatch==1 && phasei==nPhases)
                    iIns = obj.RepetitionDelay(iIns,1,repDelayTime);
                    disp('Last Batch!')
                elseif(maxGrad>0.25)
                    iIns = obj.RepetitionDelay(iIns,1,repDelayTime);
                else
                    iIns = obj.RepetitionDelay(iIns,0,repDelayTime);
                end
                if(obj.Stage~=0)
                    obj.DELAYs(iIns-1)   = repDelayTime; %time in us
                else
                    obj.DELAYs(iIns-1)   = acq_time;
                end
            end
        end
        
        function [status,result] = Run(obj,iAcq)
            tic;
            clc
            
            [~,~,~,~,~,spoilerEnable,~] = obj.ConfigureGradients(0);
            
            %% Constant to relate k with k'
            % HELP: k = gammabar*G*t
            % G = c*A where A is the gradient amplitude voltage from
            % radioprocessor. Then, k' = k/(gammabar*c) = A*t.
            % We still need to calibrate constant c. We also need to take
            % into account that c is different for each axis.
            gammabar = 42.6d6;          % MHz/T
            c = obj.ReorganizeCalibrationData();
            global rawData              % Raw Data contains output info
            rawData = [];
            
            %% Get cartesian parameters
            nPoints = [obj.GetParameter('NREADOUTS'),obj.GetParameter('NPHASES'),obj.GetParameter('NSLICES')];
            fov = [obj.GetParameter('FOVX'),obj.GetParameter('FOVY'),obj.GetParameter('FOVZ')];
            deltaK = 1./fov;
            kMax = nPoints./(2*fov);
            DELX = obj.GetParameter('DELTAX');
            DELY = obj.GetParameter('DELTAY');
            DELZ = obj.GetParameter('DELTAZ');
            
            rawData.inputs.NEX = obj.GetParameter('NSCANS');
            rawData.inputs.nPoints = nPoints;
            rawData.inputs.fov = fov;
            rawData.inputs.fovDeviation = [DELX,DELY,DELZ];
            rawData.aux.deltaK = deltaK;
            rawData.aux.kMax = kMax;
            
            %% Reorganize calibration data
            readout = obj.GetParameter('READOUT');
            phase = obj.GetParameter('PHASE');
            slice = obj.GetParameter('SLICE');
            if(readout=='x');       readout = 1;
            elseif(readout=='y');   readout = 2;
            elseif(readout=='z');   readout = 3;
            end
            if(phase=='x');         phase = 1;
            elseif(phase=='y');     phase = 2;
            elseif(phase=='z');     phase = 3;
            end
            if(slice=='x');         slice = 1;
            elseif(slice=='y');     slice = 2;
            elseif(slice=='z');     slice = 3;
            end
            c = c([readout phase slice]);
            
            %% Get sequence parameters
            coilRiseTime = obj.GetParameter('COILRISETIME');
            blankingTime = obj.GetParameter('BLANKINGDELAY');
            trf = obj.GetParameter('PULSETIME');                            % RF pulse time
            td = obj.GetParameter('TRANSIENTTIME');                         % Dead time to wait until TX/RX switching
            tMax = obj.GetParameter('TMAX');                                % Max time for acquisition, it should be the shortest T2 to be acquired
            spoilerTime = obj.GetParameter('SPOILERTIME');
            underSampling = obj.GetParameter('UNDERSAMPLING');              % Undersampling for azimutal acquisition.
            bw = max(nPoints)/(2*tMax);                                     % Ideal acquisition bandwidth
            obj.SetParameter('SPECTRALWIDTH',bw);                         % Set the SPECTRALWIDTH to bw
            origin = 0.5;                                                   % Set the origing where k = 0;
            TR = obj.GetParameter('REPETITIONTIME');
            obj.repetitionDelay = TR-coilRiseTime-blankingTime-0.5*trf-tMax-spoilerTime;
            obj.SetParameter('REPETITIONDELAY',obj.repetitionDelay);
            nMisses = obj.GetParameter('JUMPINGFACTOR');
            
            rawData.inputs.pulseTime = trf;
            rawData.inputs.deadTime = td;
            rawData.inputs.tMax = tMax;
            rawData.inputs.coilRiseTime = coilRiseTime;
            rawData.inputs.blankingTime = blankingTime;
            rawData.inputs.spoilerTime = spoilerTime;
            rawData.aux.delay = obj.repetitionDelay;
            rawData.inputs.undersampling = underSampling;
            rawData.inputs.jumpingFactor = nMisses;
            rawData.inputs.rfAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.rfFrequency = obj.GetParameter('FREQUENCY');
            rawData.aux.bandwidth = bw;
            rawData.inputs.repetitionTime = TR;
            rawData.inputs.axes = [obj.GetParameter('READOUT'),...
                obj.GetParameter('PHASE'),obj.GetParameter('SLICE')];
            
            %% Set number of phases, points and slices as well as amplitudes
            % gradientAmplitude = normalized amplitude to the MRIGUI input
            % nLPC = number of lines per circunferece in k-space
            % nCir = number of circunfereces in gradient space
            % nPPL = number of acquired points per line in k-space
            gradientAmplitudes = kMax./(gammabar*tMax).*~(nPoints==1);
            nPPL = ceil((sqrt(3)*tMax-td-origin*trf)*bw)+1;
            nLPC = ceil(max(nPoints(1:2))*pi/underSampling);    % Ideal should be *pi instead of *pi/2 but I will use *pi/2 to save time
            nLPC = max(nLPC-mod(nLPC,2),1);
            nCir = max(ceil(nPoints(3)*pi/2)-1,1);
            obj.SetParameter('NPOINTS',nPPL);
            if(nPoints(2)==1)
                nLPC = 2;
            end
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
            obj.AcquisitionTime = acq_time;
            
            rawData.aux.acquisitionTime = obj.AcquisitionTime;
            rawData.aux.numberOfPointsPerLine = nPPL;
            rawData.aux.linesPerCircumference = nLPC;
            rawData.aux.circumferences = nCir;
            rawData.aux.gradientAmplitudes = gradientAmplitudes;
            
            %% Calculate the gradients list for radial sampling
            % Gradient are defined by the sphere parametric equation. The
            % two parameteres are: theta in [0,pi] and phi [0,2pi].
            % Gradients are normalized to the amplifier amplitude.
            deltaTheta = pi/(nCir+1);
            theta = linspace(deltaTheta,pi-deltaTheta,nCir);
            deltaPhi = 2*pi/nLPC;
            phi = linspace(0,2*pi-deltaPhi,nLPC);
            normalizedGradientsRadial = zeros(nLPC*nCir,3);                % gradientVectors1 has 3 columns with Gx, Gy and Gz
            n = 0;
            for ii = 1:nCir
                for jj = 1:nLPC
                    n = n+1;
                    normalizedGradientsRadial(n,1) = sin(theta(ii))*cos(phi(jj));
                    normalizedGradientsRadial(n,2) = sin(theta(ii))*sin(phi(jj));
                    normalizedGradientsRadial(n,3) = cos(theta(ii));
                end
            end
            gradientVectors1 = normalizedGradientsRadial*diag(gradientAmplitudes);
            
            %% Calculate k-points matrix for radial sampling
            normalizedKRadial = zeros(nCir*nLPC,3,nPPL);
            % Calculate k-points at t = 0.5*trf+td
            normalizedKRadial(:,:,1) = (origin*trf+td+0.5/bw)*normalizedGradientsRadial;
            % Calculate all k-points
            for ii=2:nPPL
                normalizedKRadial(:,:,ii) = normalizedKRadial(:,:,1)+(ii-1)*normalizedGradientsRadial/obj.GetParameter('SPECTRALWIDTH');
            end
            normalizedKRadial = reshape(permute(normalizedKRadial,[3,1,2]),[nCir*nLPC*nPPL,3]);
            kRadial = normalizedKRadial*diag(gammabar*gradientAmplitudes);
            kSpaceValues = kRadial;
            
            %% k-points for Cartesian sampling
            tMin = origin*trf+td+0.5/bw;
            kx = linspace(-kMax(1)*~isequal(nPoints(1),1),kMax(1)*~isequal(nPoints(1),1),nPoints(1));
            ky = linspace(-kMax(2)*~isequal(nPoints(2),1),kMax(2)*~isequal(nPoints(2),1),nPoints(2));
            kz = linspace(-kMax(3)*~isequal(nPoints(3),1),kMax(3)*~isequal(nPoints(3),1),nPoints(3));
            [kx,ky,kz] = meshgrid(kx,ky,kz);
            kx = permute(kx,[2,1,3]);
            ky = permute(ky,[2,1,3]);
            kz = permute(kz,[2,1,3]);
            kCartesian(:,1) = kx(:);
            kCartesian(:,2) = ky(:);
            kCartesian(:,3) = kz(:);
            normalizedKCartesian = kCartesian/diag(gammabar*(gradientAmplitudes+double(gradientAmplitudes==0)));
            normalizedKCartesian(:,4) = sqrt(sum(normalizedKCartesian.^2,2));
            normalizedKSinglePoint = normalizedKCartesian(normalizedKCartesian(:,4)<tMin,1:3);
            kSinglePoint = normalizedKSinglePoint*diag(gammabar*gradientAmplitudes);
            kSpaceValues = cat(1,kSpaceValues,kSinglePoint);
            
            %% Gradients for Cartesian sampling
            gradientVectors2 = kSinglePoint/diag(gammabar*tMin);
            
            %% Save the gradients in the rawData and normalize to arbitray units.
            rawData.aux.gradientsRadial = gradientVectors1;
            rawData.aux.gradientsSinglePoint = gradientVectors2;
            gradientVectors1 = gradientVectors1/diag(c);
            gradientVectors2 = gradientVectors2/diag(c);
            if(sum(abs(gradientVectors1(:))>=1)>=1 || sum(isnan(gradientVectors1(:)))>=1 || sum(isinf(gradientVectors1(:)))>=1)
                warndlg('At least one non valid DAC amplitude in radial gradients!','Delay Error')
                return;
            end
            if(sum(abs(gradientVectors2(:))>=1)>=1 || sum(isnan(gradientVectors2(:)))>=1 || sum(isinf(gradientVectors2(:)))>=1)
                warndlg('At least one non valid DAC amplitude in single point gradients!','Delay Error')
                return;
            end
            
            %% Auxiliar
            Data2D = [];
            
            %% Prescan
            %             tBatch1 = clock;
            %             if(spoilerEnable)
            %                 obj.Stage = 0;
            %                 obj.PhasesPerBatch = floor(min([2048/(3*obj.nSteps+5),16000/nPPL]))-nMisses;
            %                 disp('New acquisition');
            %                 disp('PRESCAN');
            %                 NEX = obj.GetParameter('NSCANS');
            %                 obj.SetParameter('NSCANS',1000);
            %                 tPrescan = 0;
            %                 for iBatch = 1:1
            %                     obj.nPhasesBatch = 1;
            %                     obj.Axis1Vector = gradientVectors1(1,1);
            %                     obj.Axis2Vector = gradientVectors1(1,2);
            %                     obj.Axis3Vector = gradientVectors1(1,3);
            %                     obj.lastBatch = 0;
            %                     %                 NOW RUN ACTUAL ACQUISTION
            %                     obj.CreateSequence(1);
            %                     obj.Sequence2String;
            %                     fname = 'TempBatch.bat';
            %                     obj.WriteBatchFile(fname);
            %                     t1 = clock;
            %                     [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
            %                     t2 = clock;
            %                     tPrescan = tPrescan + etime(t2,t1);
            %                     if status == 0 && ~isempty(result)
            %                         C = textscan(result, '%s', 'delimiter', sprintf('\f'));
            %                         res = C{:};
            %                         obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
            %                         obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
            %                         obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
            %                         obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
            %                         obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
            %                     else
            %                         warndlg('Prescan batch aborted!','Warning')
            %                         return;
            %                     end
            %                     DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
            %                     cDataCal = complex(DataBatch(:,1),DataBatch(:,2));
            %                 end
            %                 obj.SetParameter('NSCANS',NEX);
            %                 rawData.kSpace.calibra = cDataCal;
            %                 clear NEX
            %             else
            %                 cDataCal = zeros(nPPL,1);
            %                 tPrescan = 0;
            %             end
            %
            tBatch1 = clock;
            cDataCal = zeros(nPPL,1);
            tPrescan = 0;
            
            numimag = 1; % number of images that we want to average
            imagen = zeros(nPoints(1),nPoints(2),nPoints(3),numimag);

            
            for kk = 1:numimag
                
                %% Radial
                % Create waiting bar
                
                obj.Stage = 1;
                obj.PhasesPerBatch = floor(min([2048/(3*obj.nSteps+5),16000/nPPL]))-nMisses;                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
                nPhaseBatches = ceil(nLPC*nCir/obj.PhasesPerBatch);
                rawData.aux.numberOfBatches = nPhaseBatches;
                cn = nPhaseBatches;
                count = 0;
                if nPhaseBatches>1
                    h_waitbar = waitbar(count/cn,'Radial (por favor espere)...');
                end
                % Start batches sweep
                fprintf('RADIAL \n');
                tRadial = 0;
                rawData.inputs.rfFrequencyRadial = zeros(nPhaseBatches,1);
                obj.readoutAmplitudeP = 0;
                obj.phaseAmplitudeP = 0;
                obj.sliceAmplitudeP = 0;
                for iBatch = 1:nPhaseBatches
                    
%                     nuevorawdata=load(strcat('prueba.mat'));
%                     RadialFreq=nuevorawdata.rawData.aux.freqListRadial;
%                     obj.SetParameter('FREQUENCY',RadialFreq(nPhaseBatches));
%                     obj.SetParameter('RFAMPLITUDE',0);
                    
                    fprintf('Radial batch %1.0f/%1.0f \n',iBatch,nPhaseBatches);
                    
                    
                    
                    
                    
                    %                 fprintf('Calibrating frequency... \n')
                    %                 % Get central frequency
                    %                 [~,f0] = obj.GetFIDParameters;
                    %                 obj.SetParameter('FREQUENCY',f0);
                    %                 rawData.aux.freqListRadial(iBatch) = f0;
                    %                 fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
                    
                    % Get the appropiate axis
                    if(iBatch<nPhaseBatches)
                        obj.Axis1Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,1);
                        obj.Axis2Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,2);
                        obj.Axis3Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,3);
                        obj.lastBatch = 0;
                    else
                        obj.Axis1Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:nLPC*nCir,1);
                        obj.Axis2Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:nLPC*nCir,2);
                        obj.Axis3Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:nLPC*nCir,3);
                        obj.lastBatch = 0;
                    end
                    obj.nPhasesBatch = length(obj.Axis1Vector);
                    
                    % NOW RUN ACTUAL ACQUISTION
                    obj.CreateSequence(0);
                    if(sum(obj.DELAYs<0.1)~=0)
                        warndlg('At least one delay is negative!','Delay Error')
                        return;
                    end
                    obj.Sequence2String;
                    fname = 'TempBatch.bat';
                    obj.WriteBatchFile(fname);
                    t1 = clock;
                    [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                    t2 = clock;
                    tRadial = tRadial + etime(t2,t1);
                    if status == 0 && ~isempty(result)
                        C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                        res = C{:};
                        obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                        obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                        obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                        obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                        obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                    else
                        warndlg('Radial batch aborted!','Warning')
                        return;
                    end
                    DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                    cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                    nPhasesInBatch=length(cDataBatch)/nPPL;
                    d = reshape(cDataBatch,nPPL,nPhasesInBatch);
                    Data2D = cat(2,Data2D,d);
                    count = count+1;
                    if nPhaseBatches>1
                        waitbar(count/cn,h_waitbar,sprintf('Radial (por favor espere)... %5.2f %%',100*count/cn));
                    end
                end
                if nPhaseBatches>1
                    delete(h_waitbar);
                end
                Data2D = Data2D(:);
                cDataCal = permute(ones(nLPC*nCir,nPPL)*diag(cDataCal),[2 1]);
                cDataCal = cDataCal(:);
                Data2Dcal = Data2D-cDataCal;
                
                %% Single point
                % Create waiting bar
                obj.Stage = 2;
                obj.PhasesPerBatch = floor(min([2048/(3*obj.nSteps+5),16000/nPPL]))-nMisses;
                nAcquiredPoints = size(gradientVectors2,1);
                nPhaseBatches = ceil(nAcquiredPoints/obj.PhasesPerBatch);
                cn = nPhaseBatches;
                count = 0;
                if nPhaseBatches>1
                    h_waitbar = waitbar(count/cn,'Cartesian (por favor espere)...');
                end
                % Start batches sweep
                fprintf('SINGLE POINT \n');
                obj.SetParameter('NPOINTS',1);
                repDelay = obj.GetParameter('REPETITIONDELAY');
                obj.SetParameter('REPETITIONDELAY',repDelay+acq_time);
                tCartesian = 0;
                rawData.inputs.rfFrequencySinglePoint = zeros(nPhaseBatches,1);
                for iBatch = 1:nPhaseBatches
                    fprintf('Single point batch %1.0f/%1.0f \n',iBatch,nPhaseBatches);
                    
%                     
%                     nuevorawdata=load(strcat('prueba.mat'));
%                     CartesianFreq=nuevorawdata.rawData.aux.freqListCartesian;
%                     obj.SetParameter('FREQUENCY',CartesianFreq(nPhaseBatches));
%                     obj.SetParameter('RFAMPLITUDE',0);
                    
                    
                    %                 fprintf('Calibrating frequency... \n')
                    %                 % Get central frequency and T2
                    %                 [~,f0] = obj.GetFIDParameters;
                    %                 obj.SetParameter('FREQUENCY',f0);
                    %                 rawData.aux.freqListCartesian(iBatch) = f0;
                    %                 fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
                    
                    % Get the appropiate axis
                    if(iBatch<nPhaseBatches)
                        obj.Axis1Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,1);
                        obj.Axis2Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,2);
                        obj.Axis3Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,3);
                        obj.lastBatch = 0;
                    else
                        obj.Axis1Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:end,1);
                        obj.Axis2Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:end,2);
                        obj.Axis3Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:end,3);
                        obj.lastBatch = 1;
                    end
                    obj.nPhasesBatch = length(obj.Axis1Vector);
                    
                    % NOW RUN ACTUAL ACQUISTION
                    obj.CreateSequence(0);
                    if(sum(obj.DELAYs<0.1)~=0)
                        warndlg('At least one delay is negative!','Delay Error')
                        return;
                    end
                    obj.Sequence2String;
                    fname = 'TempBatch.bat';
                    obj.WriteBatchFile(fname);
                    t1 = clock;
                    [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                    t2 = clock;
                    tCartesian = tCartesian + etime(t2,t1);
                    if status == 0 && ~isempty(result)
                        C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                        res = C{:};
                        obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                        obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                        obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                        obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                        obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                    else
                        warndlg('Single Point batch aborted!','Warning')
                        return;
                    end
                    DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                    cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                    d = cDataBatch;
                    dcal = cDataBatch-cDataCal(1);
                    Data2D = cat(1,Data2D,d);
                    Data2Dcal = cat(1,Data2Dcal,dcal);
                    count = count+1;
                    if nPhaseBatches>1
                        waitbar(count/cn,h_waitbar,sprintf('Cartesian (por favor espere)... %5.2f %%',100*count/cn));
                    end
                end
                if nPhaseBatches>1
                    delete(h_waitbar);
                end
                obj.SetParameter('NPOINTS',nPPL);
                obj.SetParameter('REPETITIONDELAY',repDelay);
                disp(' ');
                kSpaceValuesCal = [kSpaceValues,Data2Dcal(:)];
                kSpaceValues = [kSpaceValues,Data2D(:)];
                tBatch2 = clock;
                tBatch = etime(tBatch2,tBatch1);
                rawData.kSpace.sampled = kSpaceValues;
                rawData.kSpace.sampledCalibrated = kSpaceValuesCal;
                
                %% Regridding
                kSpaceValues(:,4) = kSpaceValuesCal(:,4);
                if(nCir>1)
                    valCartesian = griddata(kSpaceValues(:,1),kSpaceValues(:,2),kSpaceValues(:,3),kSpaceValues(:,4),kCartesian(:,1),kCartesian(:,2),kCartesian(:,3));
                else
                    valCartesian = griddata(kSpaceValues(:,1),kSpaceValues(:,2),kSpaceValues(:,4),kCartesian(:,1),kCartesian(:,2));
                end
                valCartesian(isnan(valCartesian)) = 0;
                phase = exp(-2*pi*1i*(DELX*kx(:)+DELY*ky(:)+DELZ*kz(:)));
                rawData.kSpace.interpolated = [kx(:),ky(:),kz(:),valCartesian(:).*phase];
                paddingFactor = obj.GetParameter('PADDING');
                rawData.inputs.paddingFactor = paddingFactor;
                if(nCir>1)
                    valPadding = padarray(reshape(valCartesian(:).*phase,nPoints),nPoints*(paddingFactor-1),0,'both');
                else
                    valPadding = padarray(reshape(valCartesian(:).*phase,nPoints),nPoints(1:2)*(paddingFactor-1),0,'both');
                end
                
                rawData.kSpace.paddedInterpolation = valPadding;
                
                imagen(:,:,:,kk) = abs(ifftshift(ifftn(valPadding)));
            end
            imagen = mean(imagen,4);
            rawData.kSpace.imagen = imagen;
            
            
            %% Save data
            elapsedTime = toc;
            rawData.aux.tPrescan = tPrescan;
            rawData.aux.tRadial = tRadial;
            rawData.aux.tCartesian = tCartesian;
            rawData.aux.tBatch = tBatch;
            rawData.aux.elapsedTime = elapsedTime;
            
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
    end
    
end

