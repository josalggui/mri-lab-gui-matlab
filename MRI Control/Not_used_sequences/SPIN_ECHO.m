classdef SPIN_ECHO < MRI_BlankSequence
    properties
        PhasesPerBatch = 20;
        nPhasesBatch;
        Axis1Vector;
        Axis2Vector;
        Axis3Vector;
        StartSlice;
        lastBatch;
        EndSlice;
        StartPhase;
        EndPhase;
        Stage;              % Normal ->1 or Prescan -> 0
        RepetitionTime;
        slice_ampl;
        AcquisitionTime;
    end
    methods (Access=private)
        function CreateParameters(obj)

            % HELP %
            % I include here new inputs to discriminate between
            % acquistition and regruidding.
            % -> NPHASES, NREADOUTS and NSLICES are for regridding
            % -> NPOINTS, NCIRCUNFERENCES, NLINESPERCIRCUNFERENCES are for
            %    aqcuisition.

            InputParameters = {...
                'ECHOTIME','REPETITIONTIME',...
                };
            
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT','PHASE','READOUT','SLICE','NSLICES','COILRISETIME'};
            
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {...
                'TE','TR'...
                })];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,...
                })];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {...
                0,0,...
                })];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {...
                NaN,NaN...
                })];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {...
                'us','ms',...
                })];
            
            for k=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{k},InputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
%             % Reorganizes InputParameters
%             obj.MoveParameter(6,21);
%             obj.MoveParameter(13:21,19:27);
%             obj.MoveParameter(17:18,26:27);
%             obj.MoveParameter(17,18);
%             obj.MoveParameter(23,24);
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = SPIN_ECHO(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'SPIN ECHO';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
            obj.SetParameter('ECHOTIME',100*MyUnits.us);
            obj.SetParameter('TRANSIENTTIME',20*MyUnits.us);
            obj.SetParameter('BLANKINGDELAY',15*MyUnits.us);
            obj.SetParameter('REPETITIONTIME',10*MyUnits.ms);

      
        end
        function UpdateTETR(obj)
            obj.TE = obj.GetParameter('TRANSIENTTIME') + ...
                    0.5 * obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH');
            obj.TR = obj.TE +  0.5*(obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH')) + 0.5*obj.GetParameter('PULSETIME') +...
                    obj.GetParameter('BLANKINGDELAY') + obj.GetParameter('REPETITIONDELAY');
    
            if obj.spoiler_en
                obj.TR = obj.TR + obj.GetParameter( 'SPOILERTIME' );
            end
            obj.TTotal = obj.TR * obj.GetParameter('NSCANS') * obj.GetParameter('NSLICES') * obj.GetParameter('NPHASES');
        end
        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence,
            %         "1", central frequency adjustment sequence,
            
            % first set the appropriate axes as were decided
            [~,~,~] = SelectAxes(obj);                                     % Shimming
            [~,~,~,~,~,~,~] = obj.ConfigureGradients(mode);                % Gradientes
            
            % Calculate the number of line reads
            nPhases = 1;
            obj.nline_reads = nPhases;
            
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions=8*(nPhases);
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            % Timing
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6;
            trf = obj.GetParameter('PULSETIME')*1e6;
            tau = obj.GetParameter('ECHOTIME')/2*1e6;
            td =  obj.GetParameter('TRANSIENTTIME')*1e6;
            blk = obj.GetParameter('BLANKINGDELAY')*1e6;
            timeDelay = obj.GetParameter('REPETITIONTIME')*1e6;             % Hay que restar el tiempo de Tx/Rx
            iIns = 1;
            bbit = 1;            
            for phasei = 1:nPhases
                %% 90� RF pulse (3 instruction)
                nIns = 3; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.FLAGs(vIns)         = [1 1 0];
                    obj.TXs(vIns)           = [0 1 0];
                    obj.ENVELOPEs(vIns)     = [7 7 7];
                    obj.PHASEs(vIns)        = [0 0 0];
                    obj.PhResets(vIns)      = [1 0 0];
                    obj.DELAYs(vIns)   = [blk trf tau-trf-0.5*trf-blk]; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %% 180� RF pulse (3 instructions)
                nIns = 3; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.FLAGs(vIns)         = [1 1 0];
                    obj.TXs(vIns)           = [0 1 0];
                    obj.ENVELOPEs(vIns)     = [7 7 7];
                    obj.PHASEs(vIns)        = [0 0 0];
                    obj.PhResets(vIns)      = [1 0 0];
                    obj.DELAYs(vIns)   = [blk 2*trf td]; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                                
                %% Acquisition (1 instruction)
                nIns = 1;
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.RXs(vIns)  = 1;
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.DELAYs(vIns)   = acq_time;
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %% Repetition Delay (1 instruction)
                nIns = 1;
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns) = 0;
                    obj.DACs(vIns) = 3;
                    obj.WRITEs(vIns) = 1;
                    obj.UPDATEs(vIns) = 1;
                    obj.CLEARs(vIns)   = 1;
                    obj.ENVELOPEs(vIns) = 7;
                    obj.DELAYs(vIns)   = timeDelay; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
            end % end for nPhases
            %             end %end for slicei
            
        end
        function [status,result] = Run(obj,iAcq)
            clc
            global MRIDATA
            [~,~,~,~,~,~,~] = obj.ConfigureGradients(0);
            
            % Raw Data contains output info
            global rawData;
            rawData.inputs.sequence = obj.SequenceName;
            rawData.inputs.NEX = obj.GetParameter('NSCANS');
            rawData.inputs.nPoints = obj.GetParameter('NPOINTS');
            % Falta completar con todos los datos de entrada
            
            %% Get sequence parameters
            TE = obj.GetParameter('ECHOTIME');                              % Echo time
            td = obj.GetParameter('TRANSIENTTIME');                         % Dead time
            trf = obj.GetParameter('PULSETIME');                            % RF time
            taq = obj.GetParameter('NPOINTS')/...
                obj.GetParameter('SPECTRALWIDTH');                          % Acquisition time
            nPPL = obj.GetParameter('NPOINTS');
            
            %% Auxiliar
            Data2D = [];
            
            %% Execute the sequence
            % Create waiting bar
            obj.PhasesPerBatch = floor(min([1024/8,16000/nPPL]));                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
            nPhaseBatches = 1;
            
            % Start batches sweep
            disp('MIDIENDO SPIN ECHO');
            for iBatch = 1:nPhaseBatches
                % NOW RUN ACTUAL ACQUISTION
                obj.CreateSequence(0);
                obj.Sequence2String;            
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Radial batch aborted!','Warning')
                    return;
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                Data2D = cDataBatch;
            end
            Data2D = Data2D(:);
            timei = TE/2+trf+td;
            timef = timei+taq;
            time = linspace(timei,timef,nPPL)'*1d6;
            medidas = [time,Data2D(:)];
            
 
            rawData.kSpace.sampled = medidas;
                       
            % Save rawData
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
    end
    
end

