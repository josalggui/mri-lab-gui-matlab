classdef GradientEchoMultiSlice < MRISequence
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        SliceGradient;
        StartSliceFreq;
        EndSliceFreq;
        SliceFreqOffsetHigh;
        SliceFreqOffsetMid;
        SliceFreqOffsetLow;
        PhasesPerBatch = 10;
        SlicesPerBatch = 4;
        StartPhase;
        EndPhase;
    end
    methods (Access=private)
        function CreateParameters(obj)
            InputParameters = {...
                'PHASETIME','ECHODELAY','REPETITIONDELAY','READOUTAMPLITUDE','SPOILERPHASEAMP',...
                'SPOILERREADOUTAMP','SPOILERTIME','NSLICES','STARTPHASE',...
                'ENDPHASE','SLICEGRADIENT','STARTSLICEFREQ', 'ENDSLICEFREQ', ...
                'GSTD','BLUR',...
                };
            
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {...
                'Phase Time','Gradient Echo Delay','Repetition Delay','Readout Amplitude','Spoiler Amp Phase',...
                'Spoiler Amp Readout','Spoiler Time','Number of Slices','Start Phase',...
                'End Phase','Slice Gradient','Start Slice Freq','End Slice Freq',...
                'Gaussian','Blur'...
                })];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,...
                NaN,NaN...
                })];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,...
                NaN,NaN...
                })];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,...
                NaN,NaN...
                })];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {...
                'us','us','ms','','',...
                '','us','','',...
                '','','kHz','kHz',...
                '',''...
                })];
            OutputParameters = {
                'DEPHASINGREADOUTAMPLITUDE','PHASINGREADOUTAMPLITUDE','SLICEAMPLITUDE1','SLICEAMPLITUDE2'...
                };
            obj.OutputParNames = [containers.Map(obj.OutputParNames.keys(),obj.OutputParNames.values());containers.Map(OutputParameters,...
                {...
                'Dephasing Readout Amplitude', 'Phasing Readout Amplitude','Slice Amplitude 1','Slice Amplitude 2'...
                })];
            obj.OutputParValues = [containers.Map(obj.OutputParValues.keys(),obj.OutputParValues.values());containers.Map(OutputParameters,...
                {...
                NaN,NaN,NaN,NaN...
                })];
            obj.OutputParUnits = [containers.Map(obj.OutputParUnits.keys(),obj.OutputParUnits.values());containers.Map(OutputParameters,...
                {...
                '','','',''...
                })];
            for i=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{i},InputParameters)) = [];
            end
            for i=1:length(obj.OutputParameters)
                OutputParameters(strcmp(obj.OutputParameters{i},OutputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
            obj.OutputParameters = [obj.OutputParameters,OutputParameters];
        end
        function WriteBatchFile(obj,fname)
            %% WRITE BATCH FILE AND RUN
            if (abs(obj.EndPhase - obj.GetParameter('STARTPHASE')) - abs(obj.GetParameter('ENDPHASE') - obj.GetParameter('STARTPHASE'))) > 1e-6 %last batch may be smaller
                nPhases = mod(obj.GetParameter('NPHASES'),obj.PhasesPerBatch);
                obj.EndPhase = obj.GetParameter('ENDPHASE');
            else
                nPhases = min(obj.GetParameter('NPHASES'),obj.PhasesPerBatch);
            end
            fid = fopen(fullfile(obj.path_file,fname), 'wt');
            pars = obj.InputParameters;
            values = obj.InputParValues;
            fprintf( fid, '@echo off\n');
            fprintf( fid, 'SET Debug=%d\n',0);
            fprintf( fid, 'SET outputFilename=%s\n',obj.OutputFilename);
            fprintf( fid, 'REM ======================\n');
            fprintf( fid, 'REM Acquisition Parameters\n' );
            fprintf( fid, 'REM ======================\n');
            fprintf( fid, 'SET nPoints=%u\n', values('NPOINTS') );
            fprintf( fid, 'SET nScans=%u\n', values('NSCANS') );
            fprintf( fid, 'SET nPhases=%u\n', nPhases );
            fprintf( fid, 'SET spectrometerFrequency_MHz=%f\n', values('FREQUENCY')/MyUnits.MHz);
            fprintf( fid, 'SET spectralWidth_kHz=%f\n',values('SPECTRALWIDTH')/MyUnits.kHz);
            fprintf( fid, 'SET linebroadening_value=%f\n',values('LINEBROADENING'));
            
            fprintf( fid, 'REM ================\n' );
            fprintf( fid, 'REM Gradient Enables\n' );
            fprintf( fid, 'REM ================\n' );
            fprintf( fid, 'SET slice_en=%u\n', obj.slice_en);
            fprintf( fid, 'SET phase_en=%u\n', obj.phase_en);
            fprintf( fid, 'SET readout_en=%u\n', obj.readout_en);
            fprintf( fid, 'SET spoiler_en=%u\n', obj.spoiler_en);
            
            fprintf( fid, 'REM ===================\n');
            fprintf( fid, 'REM RF Pulse Parameters\n');
            fprintf( fid, 'REM ===================\n'  );
            fprintf( fid, 'SET RF_Shape=%u\n', obj.RF_Shape);
            fprintf( fid, 'SET amplitude=%f\n', values('RFAMPLITUDE'));
            fprintf( fid, 'SET pulseTime_us=%f\n', values('PULSETIME')/MyUnits.us);
            
            fprintf( fid, 'REM ======\n');
            fprintf( fid, 'REM Delays\n');
            fprintf( fid, 'REM ======\n');
            fprintf( fid, 'SET blankingDelay_ms=%f\n', values('BLANKINGDELAY')/MyUnits.ms);
            fprintf( fid, 'SET transTime_us=%f\n', values('TRANSIENTTIME')/MyUnits.us);
            fprintf( fid, 'SET phaseTime_us=%f\n', values('PHASETIME')/MyUnits.us);
            fprintf( fid, 'SET gradientEchoDelay_us=%f\n', values('ECHODELAY')/MyUnits.us);
            fprintf( fid, 'SET readout_amplitude=%f\n', values('READOUTAMPLITUDE'));
            fprintf( fid, 'SET repetitionDelay_s=%f\n', values('REPETITIONDELAY')/MyUnits.s);
            
            fprintf( fid, 'SET spoilerTime_us=%f\n', values('SPOILERTIME')/MyUnits.us);
            fprintf( fid, 'SET spoilerPhaseamp=%f\n', values('SPOILERPHASEAMP'));
            fprintf( fid, 'SET spoilerReadoutamp=%f\n', values('SPOILERREADOUTAMP'));
            
            shimS = 0;
            shimP = 0;
            shimR = 0;
            if (obj.shimS_en)
                shimS = obj.GetShimS();
            end
            if (obj.shimP_en)
                shimP = obj.GetShimP();
            end
            if (obj.shimR_en)
                shimR = obj.GetShimR();
            end
            fprintf( fid, 'SET shimSlice=%f\n', shimS);
            fprintf( fid, 'SET shimPhase=%f\n', shimP);
            fprintf( fid, 'SET shimReadout=%f\n', shimR);
            
            fprintf( fid, 'SET nSlice=%u\n', values('NSLICES') );
            fprintf( fid, 'SET sliceGradient=%f\n', obj.SliceGradient);
            fprintf( fid, 'SET sliceFreqOffsetHigh=%f\n', obj.SliceFreqOffsetHigh / MyUnits.MHz );
            fprintf( fid, 'SET sliceFreqOffsetMid=%f\n', obj.SliceFreqOffsetMid / MyUnits.MHz );
            fprintf( fid, 'SET sliceFreqOffsetLow=%f\n', obj.SliceFreqOffsetLow / MyUnits.MHz );
            
            fprintf( fid, 'SET startPhase=%f\n', obj.StartPhase );
            fprintf( fid, 'SET endPhase=%f\n', obj.EndPhase );
            
            fprintf( fid, 'SET tx_phase=%f\n', obj.tx_phase);
            fprintf( fid, 'SET blankingBit=%u\n', obj.blankingBit);
            
            fprintf( fid, 'SET adcOffset=%u\n', obj.adcOffset);
            
            fprintf( fid, 'echo. | "%s"',fullfile(obj.ProgramDir,obj.ProgramName)); %we need echo. to exit batch file in case of errors (it emulates typing ENTER)
            fclose(fid);
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = GradientEchoMultiSlice(program)
            obj = obj@MRISequence();
            obj.SequenceName = 'Gradient Echo with MultiSlice';
            obj.ProgramName = 'GradientEchoMultiSliceWP.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRISequence(obj);
            obj.SetParameter('SHIMSLICE',0);
            obj.SetParameter('SHIMPHASE',0);
            obj.SetParameter('SHIMREADOUT',0);
            obj.SetParameter('PHASETIME',50*MyUnits.us);
            obj.SetParameter('ECHODELAY',20*MyUnits.us);
            obj.SetParameter('REPETITIONDELAY',50*MyUnits.ms);
            obj.SetParameter('READOUTAMPLITUDE',0.1);
            obj.SetParameter('SPOILERTIME',100*MyUnits.us);
            obj.SetParameter('SPOILERPHASEAMP',0.1);
            obj.SetParameter('SPOILERREADOUTAMP',0.1);
            obj.SetParameter('NSLICES',1)
            obj.SetParameter('STARTPHASE',-0.1)
            obj.SetParameter('ENDPHASE',0.1)
            obj.SetParameter('SLICEGRADIENT',0.1);
            obj.SetParameter('STARTSLICEFREQ',-20*MyUnits.kHz)
            obj.SetParameter('ENDSLICEFREQ',20*MyUnits.kHz)
            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
            obj.SetParameter('GSTD',0.5);
            obj.SetParameter('BLUR',3);
        end
        function UpdateTETR(obj)
            obj.TE = obj.GetParameter('TRANSIENTTIME') + obj.GetParameter('PHASETIME') + ...
                    obj.GetParameter('ECHODELAY') + 0.5 * obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH');
            obj.TR = obj.TE +  0.5*(obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH')) + 0.5*obj.GetParameter('PULSETIME') +...
                    obj.GetParameter('BLANKINGDELAY') + obj.GetParameter('REPETITIONDELAY');
    
            if obj.spoiler_en
                obj.TR = obj.TR + obj.GetParameter( 'SPOILERTIME' );
            end
            obj.TTotal = obj.TR * obj.GetParameter('NSCANS') * obj.GetParameter('NSLICES') * obj.GetParameter('NPHASES');
        end
        function [status,result] = Run(obj,iAcq)
            obj.SliceGradient = obj.GetParameter('SLICEGRADIENT');
            obj.StartSliceFreq = obj.GetParameter('STARTSLICEFREQ');
            obj.EndSliceFreq = obj.GetParameter('ENDSLICEFREQ');
            obj.StartPhase = obj.GetParameter('STARTPHASE');
            obj.EndPhase = obj.GetParameter('ENDPHASE');
            
            if obj.slice_en
                nSlices = obj.GetParameter('NSLICES');
            else 
                nSlices = 1;
            end
            
            if nSlices > 1
                SliceStep = (obj.EndSliceFreq - obj.StartSliceFreq)/(nSlices-2);
            else
                SliceStep = 0;
            end
            
            if obj.phase_en
                nPhases = obj.GetParameter('NPHASES');
            else
                nPhases = 1;
            end
            
            if nPhases > 1
                PhaseStep = (obj.EndPhase - obj.StartPhase)/(nPhases-1);
            else
                PhaseStep = 0;
            end
            nSliceBatches = ceil(nSlices/obj.SlicesPerBatch);
            nPhaseBatches = ceil(nPhases/obj.PhasesPerBatch);
            nPoints = obj.GetParameter('NPOINTS');
            Data3D = [];%zeros(nPoints,nPhases,nSlices);
            cn = nSliceBatches*nPhaseBatches;
            count = 0;
            
            if nPhaseBatches>1
                global CONTROL
                OP = [0.1    0.2    0.8    0.6];
                warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
                jFrame = get(CONTROL.gui_fig,'JavaFrame');
                jFrame.setMinimized(true);
                set(CONTROL.runFig,'units','normalized','outerposition',OP,'Color',[0 0 0]);
%                 obj.StatusBar.ProgressBar.setIndeterminate(false); % I
% %                 comment the lines regarding the progressBar, because the
% %                 setting and stop of the progressBar was commented in the
% %                 gui at some point.
%                 obj.StatusBar.ProgressBar.setStringPainted(true);
%                 set(obj.StatusBar.ProgressBar,'Minimum',0,'Maximum',cn,'Value',0);
            end
            for iSlice = 1:nSliceBatches
                obj.SliceFreqOffsetHigh = ( obj.StartSliceFreq + (iSlice-1)*SliceStep*obj.SlicesPerBatch ) ;
                obj.SliceFreqOffsetMid = ( obj.StartSliceFreq + (iSlice-1)*SliceStep*obj.SlicesPerBatch + SliceStep) ;
                obj.SliceFreqOffsetLow = ( obj.StartSliceFreq + (iSlice-1)*SliceStep*obj.SlicesPerBatch + 2*SliceStep) ;
                
                
                for iBatch = 1:nPhaseBatches
                    obj.StartPhase = obj.GetParameter('STARTPHASE') + (iBatch-1)*obj.PhasesPerBatch*PhaseStep;
                    obj.EndPhase = obj.StartPhase + (obj.PhasesPerBatch-1)*PhaseStep;
                    fname = 'TempBatch.bat';
                    obj.WriteBatchFile(fname);
                    [status,result] = system(fullfile(obj.path_file,fname));
                    %            [status,result] = system(fname,'-echo');
                    %delete(fname);% delete the batch file
                    if status == 0 && ~isempty(result)
                        C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                        res = C{:};
                        obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                        obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                        obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                        obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                        obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                        obj.SetParameter('SLICEAMPLITUDE1',sscanf(res{6},'Slice amplitude 1: %f'));
                        obj.SetParameter('SLICEAMPLITUDE2',sscanf(res{7},'Slice amplitude 2: %f'));
                    end
                    
                    
                    DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                    cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                    
                    if obj.slice_en && obj.phase_en
                        d = reshape(cDataBatch,nPoints,min(nSlices,obj.SlicesPerBatch),min(nPhases,obj.PhasesPerBatch));
                        d = permute(d,[1 3 2]);
                        Data3D = cat(2,Data3D,d);
                    elseif obj.phase_en
                        d = reshape(cDataBatch,nPoints,obj.PhasesPerBatch);
                        Data3D = cat(2,Data3D,d);
                    else
                        Data3D = cat(2,Data3D,cDataBatch);
                    end
                    count = count+1;
                    if nPhaseBatches>1
%                         set(obj.StatusBar.ProgressBar,'Value',count);
                    end
                end
                                
                if length(obj.cData) ~= nPoints*nPhases*nSlices;
                    obj.cData = zeros(obj.NAverages,nPoints*nPhases*nSlices);
                end
                if obj.averaging
                    Nav = obj.NAverages;
                else
                    Nav = 1;
                end
                index = mod(iAcq-1,Nav)+1;
                if obj.autoPhasing
%                    pt = max(Data3D(:,ceil(size(Data3D,2)/2),ceil(size(Data3D,3)/2)));
                    pt = max(Data3D(:));
                    Data3D = Data3D.*exp(complex(0,-angle(pt)));
                end
                obj.cData(index,1:length(Data3D(:))) = Data3D(:);
                obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
                obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
                %    obj.fData1D = fftshift(ifft(fftshift(obj.cDataAv)));
                obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
                acq_time = obj.GetParameter('ACQUISITIONTIME');
                obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*acq_time/nPhases;
                obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time/nPhases;
            end
        end
    end
    
end

