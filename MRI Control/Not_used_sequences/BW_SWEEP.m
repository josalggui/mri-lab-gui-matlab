classdef BW_SWEEP < MRI_BlankSequence6   
    properties
        maxRepetitionsPerBatch;
        nPhasesBatch;
        lastBatch;
        RepetitionTime;
        AcquisitionTime;
        nSteps=5;
        
        
    end
    methods (Access=private)
        function CreateParameters(obj)
            obj.CreateOneParameter('BWMIN','Minimun Bandwidth','kHz',10*MyUnits.kHz);
            obj.CreateOneParameter('BWMAX','Maximun Bandwidth','kHz',100*MyUnits.kHz);
            obj.CreateOneParameter('NSTEPS','Steps','',10);
            obj.CreateOneParameter('REPETITIONDELAY','Repetition Delay','ms',1*MyUnits.ms)
            
            
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT','READOUT'...
                'NSLICES','BLANKINGDELAY','PHASE','SLICE','RFAMPLITUDE',...
                'FREQUENCY','SPECTRALWIDTH','PULSETIME','COILRISETIME',...
                'TRANSIENTTIME','REPETITIONDELAY'}; 
        end  
    end
    
    

    methods
        function obj = BW_SWEEP(program)
            obj = obj@MRI_BlankSequence6();
            obj.SequenceName = 'EDDYCURRENTSTEST';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence6(obj);
        end
        
         
        function CreateSequence( obj)
            obj.nline_reads = 1;
            obj.nInstructions = 4;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            acqTime = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6;
            iIns = 1;
            iIns = obj.CreatePulse(iIns,0,300,15,10);
%             iIns = obj.RepetitionDelay(iIns,1,acqTime);
            obj.FLAGs(iIns-1) = 0;
%             obj.TXs(iIns-2) = 0;
            iIns = obj.Acquisition(iIns,acqTime);
            obj.FLAGs(iIns-1) = 0;
        end
        
        function [status,result] = Run(obj,iAcq)
            clc
            obj.SetParameter('FREQUENCY',11*MyUnits.MHz);
            obj.SetParameter('RFAMPLITUDE',0.07);
            
            % Load amplitude calibration for 1V
            bwCal = load('rawData-BW-Calibration.mat');
            bwCalVal = bwCal.rawData.outputs.fidMean;
            bwCalVec = logspace(log10(bwCal.rawData.inputs.bwMin),...
                log10(bwCal.rawData.inputs.bwMax),...
                bwCal.rawData.inputs.steps);
            
            obj.nInstructions = 1;
            obj.maxRepetitionsPerBatch = floor(1024/obj.nInstructions);                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
            nBatches = obj.GetParameter('NSTEPS');
            bwMin = obj.GetParameter('BWMIN');
            bwMax = obj.GetParameter('BWMAX');
            bwList = logspace(log10(bwMin),log10(bwMax),nBatches);
            fidList = zeros(obj.GetParameter('NPOINTS'),nBatches);
            for iBatch = 1:nBatches
                fprintf('Batch %1.0f/%1.0f\n',iBatch,nBatches)
                obj.SetParameter('SPECTRALWIDTH',bwList(iBatch));
                
                % Get value to normalize fid to Volts
                [~,bwCalNormPos] = min(abs(bwCalVec-bwList(iBatch)));
                bwCalNormVal = bwCalVal(bwCalNormPos);
                
                obj.CreateSequence;
                obj.Sequence2String;            
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Batch aborted!','Warning')
                    return;
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                fidList(:,iBatch) = cDataBatch/bwCalNormVal;
            end
            
            global rawData
            rawData = [];
            rawData.inputs.nPoints = obj.GetParameter('NPOINTS');
            rawData.inputs.bwMin = bwMin;
            rawData.inputs.bwMax = bwMax;
            rawData.inputs.steps = nBatches;
            rawData.outputs.fidList = fidList;
            rawData.outputs.fidMean = mean(abs(fidList),1);
            rawData.outputs.fidStd = std(abs(fidList),1);
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');

        end
    end
    
end