classdef MASSIF < MRI_BlankSequence6
    %% MASSIF CODE
    
    %*******************************************************
    %** J. M. Algar�n                                     **
    %** CSIC/UPV                                          **
    %** I3M                                               **
    %** Avda. dels Tarongers, 12,46022, Valencia, (Spain) **
    %** Tel: +34 960 728 111                              **
    %** email: josalggui@i3m.upv.es                       **
    %*******************************************************
    
    %% Properties
    properties
        gradientList;
        gradientPoints = 339;   % This will produce 1022 orders
        gradientTimeResolution;
    end
    
    
    %% Methods
    methods
        function obj = MASSIF(program)
            obj = obj@MRI_BlankSequence6();
            obj.SequenceName = 'MASSIF';
            obj.ProgramName = 'MRI_BlankSequenceJM.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function CreateParameters(obj)
            % Create parameters
            obj.CreateOneParameter('FREQUENCY','RF frequency','MHz',14*MyUnits.MHz);
            obj.CreateOneParameter('PULSETIME','RF pulse time','us',4*MyUnits.us); 
            obj.CreateOneParameter('NELEMENTS','Number of elements','',4);
            obj.CreateOneParameter('NPERIODS','Number of periods','',10);
            obj.CreateOneParameter('GAMPLITUDE','Gradient amplitude','T/m',0.1);
            obj.CreateOneParameter('GFREQUENCY','Gradient frequency','kHz',1*MyUnits.kHz);
            obj.CreateOneParameter('GAMPLITUDEZ','Z gradient amplitude','T/m',0);
            obj.CreateOneParameter('GFREQUENCYZ','Z gradient frequency','kHz',1*MyUnits.kHz);
            obj.CreateOneParameter('TRANSIENTTIME','Acquisition delay','us',20*MyUnits.us);
            obj.CreateOneParameter('REPETITIONDELAY','Repetition delay','s',1);
            % Hidden parameters
            obj.InputParHidden = {...
                'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
                'SPECTRALWIDTH','NSLICES',...
                'BLANKINGDELAY','COILRISETIME','SLICE'};
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence6(obj);
            obj.SetParameter('NPOINTS',20);
            obj.SetParameter('PULSETIME',4*MyUnits.us);
            obj.SetParameter('RFAMPLITUDE',1);
            obj.SetParameter('TRANSIENTTIME',20*MyUnits.us);
            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
        end
        function CreateSequence(obj,mode)
            % Get the shimming and axes
            % SelectAxes save the axes at:
            % -obj.selectReadout
            % -obj.selectPhase
            % -obj.selectSlice
            [shimS,shimP,shimR] = SelectAxes(obj);
            
            % Number of times you need to read data (for batch file)
            obj.nline_reads = 1;
            
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions = 4+3*obj.gradientPoints+1;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            % Get time delays for instructions
            acquisitionTime = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6;
            deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
            pulseTime = obj.GetParameter('PULSETIME')*1e6;
            repetitionDelay = obj.GetParameter('REPETITIONDELAY')*1e6;
            
            % Crate pulse sequence
            iIns = 1;
            % RF pulse (1 instruction)
            iIns = obj.CreatePulse(iIns,0,15,pulseTime,deadTime);
            iIns = obj.Acquisition(iIns,0.1);
            for ii = 1:obj.gradientPoints
                % Gradient list
                rAmp = obj.gradientList(ii,1)-shimR;
                pAmp = obj.gradientList(ii,2)-shimP;
                sAmp = obj.gradientList(ii,3)-shimS;
                % Turn on the first gradient
                iIns = obj.PulseGradient(iIns,rAmp,...
                    obj.selectReadout,0.1);
                % Turn on the second gradient
                iIns = obj.PulseGradient(iIns,pAmp,...
                    obj.selectPhase,0.1);
                % Turn on the third gradient
                iIns = obj.PulseGradient(iIns,sAmp,...
                    obj.selectSlice,obj.gradientTimeResolution*1e6-0.2);
                % Run acquisition
            end
            % Repetition delay and turn all gradients off
            iIns = obj.RepetitionDelay(iIns,1,repetitionDelay);
        end
        function [status,result] = Run(obj,iAcq)
            tic;
            clc
            
            %% Constant to relate k with k'
            % HELP: k = gammabar*G*t
            % G = c*A where A is the gradient amplitude voltage from
            % radioprocessor. Then, k' = k/(gammabar*c) = A*t.
            % We still need to calibrate constant c. We also need to take
            % into account that c is different for each axis. 
            gammabar = 42.6d6;          % MHz/T
            c = obj.ReorganizeCalibrationData();
            clear rawData
            global rawData;              % Raw Data contains output info
            
            %% Get sequence parameters
            pulseTime = obj.GetParameter('PULSETIME');
            nPeriods = obj.GetParameter('NPERIODS');
            gAmplitude = obj.GetParameter('GAMPLITUDE');
            gFrequency = obj.GetParameter('GFREQUENCY');
            gAmplitudeZ = obj.GetParameter('GAMPLITUDEZ');
            gFrequencyZ = obj.GetParameter('GFREQUENCYZ');
            nPoints = obj.GetParameter('NPOINTS');
            
            rawData.inputs.Seq = obj.SequenceName;
            rawData.inputs.NEX = obj.GetParameter('NSCANS');
            rawData.inputs.nPoints = nPoints;
            rawData.inputs.rfAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.rfFrequency = obj.GetParameter('FREQUENCY');
            rawData.inputs.pulseTime = pulseTime;
            rawData.inputs.acqDelay = obj.GetParameter('TRANSIENTTIME');
            rawData.inputs.axis = strcat(obj.GetParameter('READOUT'),obj.GetParameter('PHASE'));
            rawData.inputs.nPeriods = nPeriods;
            rawData.inputs.gAmplitude = gAmplitude;
            rawData.inputs.gFrequency = gFrequency;
            rawData.inputs.gAmplitudeZ = gAmplitudeZ;
            rawData.inputs.gFrequencyZ = gFrequencyZ;
            rawData.inputs.repDelay = obj.GetParameter('REPETITIONDELAY');
            
            %% Set the spectral width
            period = 1/gFrequency;
            acquisitionTime = period*nPeriods; % We need to substract 0.1us per gradient
            bw = nPoints/acquisitionTime;
            obj.SetParameter('SPECTRALWIDTH',bw);
            rawData.aux.acquisitionTime = acquisitionTime;
            rawData.aux.bandwidth = bw;
            
            %% Run sequence
            % Get number of averages
            if(obj.averaging)
                nAverages = obj.NAverages;
            else
                nAverages = 1;
            end
            
            % Get acquired time points
            tSignal = linspace(0,acquisitionTime-1/bw,nPoints)'+0.5/bw;
            tGradient = linspace(0,acquisitionTime,obj.gradientPoints);
            obj.gradientTimeResolution = tGradient(2)-tGradient(1);
            measurements = zeros(length(tSignal),nAverages+1);
            measurements0 = measurements;
            measurements(:,1) = tSignal;
            measurements0(:,1) = tSignal;
            
            % Open waiting bar
            if nAverages>1
                h_waitbar = waitbar(0/nAverages,'Midiendo (por favor espere)...');
            end
            
            tWithout = 0;
            tWith = 0;
            for ii = 1:nAverages
                fprintf('Repetition %1.0f/%1.0f \n',ii,nAverages)
%                 % Get central frequency and T2
%                 fprintf('Calibrating frequency... \n')
%                 [~,f0] = obj.GetFIDParameters;
%                 rawData.aux.freqList(ii) = f0;
%                 fprintf('Frequency = %1.4f MHz \n',f0*1d-6)
                
                % FID withoug gradients
                fprintf('FID with gradients OFF... \n')
                obj.gradientList = zeros(obj.gradientPoints,3);
                clear cDataBatch status result
                [cDataBatch,tWithout,status,result] = RunData(obj,0,tWithout);
                if(obj.autoPhasing)
                    measurements0(:,ii+1) = cDataBatch*exp(-1i*angle(cDataBatch(1)));
                else
                    measurements0(:,ii+1) = cDataBatch;
                end
                
                % FID with gradients
                fprintf('FID with gradients ON... \n')
                obj.gradientList(:,1) = gAmplitude*sin(2*pi*gFrequency*tGradient);
                obj.gradientList(:,2) = gAmplitude*cos(2*pi*gFrequency*tGradient);
                obj.gradientList(:,3) = gAmplitudeZ*cos(2*pi*gFrequencyZ*tGradient);
                rawData.aux.gradientList = obj.gradientList;
                obj.gradientList = obj.gradientList/diag(c);
                clear cDataBatch status result
                [cDataBatch,tWith,status,result] = RunData(obj,0,tWith);
                if(obj.autoPhasing)
                    measurements(:,ii+1) = cDataBatch*exp(-1i*angle(cDataBatch(1)));
                else
                    measurements(:,ii+1) = cDataBatch;
                end
                if nAverages>1
                    waitbar(ii/nAverages,h_waitbar,sprintf('Midiendo (por favor espere)... %5.2f %%',100*ii/nAverages));
                end
                disp(' ')
            end
            
            % Close waiting bar
            if nAverages>1
                delete(h_waitbar);
            end
            %% Regridding
            rawData.outputs.measurements = measurements;
            rawData.outputs.measurements0 = measurements0;
            clear tBlock Dt

            %% Save elapsed times to raw data
            elapsedTime = toc;
            rawData.aux.tWithout = tWithout;
            rawData.aux.tWith = tWith;
            rawData.aux.elapsedTime = elapsedTime;
            
            % Save rawData
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
    end
    
end

