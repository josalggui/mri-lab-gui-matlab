classdef RING_DOWN < MRI_BlankSequence   
    properties
        maxRepetitionsPerBatch;
        nPhasesBatch;
        lastBatch;
        RepetitionTime;
        AcquisitionTime;
        EndPhase;
        PhasesPerBatch = 20;
        nSteps=5;
        
        
    end
    methods (Access=private)
        function CreateParameters(obj)
            obj.CreateOneParameter('TRANSIENTTIME','Dead Time','us',10*MyUnits.us);
            obj.CreateOneParameter('ACQUISITIONTIME','Acquisition Time','us',200*MyUnits.us);
            obj.CreateOneParameter('REPETITIONDELAY','Repetition Delay','ms',1000*MyUnits.ms);
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
                'READOUT','PHASE','SLICE','COILRISETIME','SPOILERTIME',...
                'SPOILERAMP','NSLICES','BLANKINGDELAY','SPECTRALWIDTH'};

        end  
    end
    
    

    methods
        function obj = RING_DOWN(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'RING_DOWN';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
        end
        
         
        function CreateSequence( obj)
            obj.nline_reads = 2;
            obj.nInstructions = 8;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            acqTime = obj.GetParameter('ACQUISITIONTIME')*1e6;
            pulseTime = obj.GetParameter('PULSETIME')*1e6;
            deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
            repDelay = obj.GetParameter('REPETITIONDELAY')*1e6;
            blk = 5;
            iIns = 1;
            % First pulse
            iIns = obj.CreatePulse(iIns,0,blk,pulseTime,deadTime);
            % First acquisition
            iIns = obj.Acquisition(iIns,acqTime);
            % Second pulse
            iIns = obj.CreatePulse(iIns,0,blk,pulseTime,deadTime);
            % Second acquisition
            iIns = obj.Acquisition(iIns,acqTime+repDelay);
        end
        
        function [status,result] = Run(obj,iAcq)
            clc
            global rawData;
            rawData = [];
            obj.adcOffset = -6;
            
            % Input data
            nPoints = obj.GetParameter('NPOINTS');
            acqTime = obj.GetParameter('ACQUISITIONTIME');
            bw = nPoints/acqTime;
            obj.SetParameter('SPECTRALWIDTH',bw);
            
            rawData.inputs.sequence = obj.SequenceName;
            rawData.inputs.NEX = obj.GetParameter('NSCANS');
            rawData.inputs.nPoints = nPoints;
            rawData.inputs.rfAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.pulseTime = obj.GetParameter('PULSETIME');
            rawData.inputs.deadTime = obj.GetParameter('TRANSIENTTIME');
            rawData.inputs.acquisitionTime = obj.GetParameter('ACQUISITIONTIME');
            rawData.inputs.repetitionDelay = obj.GetParameter('REPETITIONDELAY');
            rawData.auxiliar.bandwidth = bw;
                        
            % Load amplitude calibration for 1V
            bwCal = load('rawData-BW-Calibration.mat');
            bwCalVal = bwCal.rawData.outputs.fidMean;
            bwCalVec = logspace(log10(bwCal.rawData.inputs.bwMin),...
                log10(bwCal.rawData.inputs.bwMax),...
                bwCal.rawData.inputs.steps);
            
            % Send sequence
            obj.nInstructions = 8;
            obj.maxRepetitionsPerBatch = floor(2048/obj.nInstructions);                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
            fidList = zeros(obj.GetParameter('NPOINTS'),2);
            
            % Get value to normalize fid to Volts
            [~,bwCalNormPos] = min(abs(bwCalVec-bw));
            bwCalNormVal = bwCalVal(bwCalNormPos);
            
            % Center the frequency
            fprintf('Calibrating frequency... \n')
            [~,f0] = obj.GetFIDParameters;
            obj.SetParameter('FREQUENCY',f0);
            rawData.aux.rfFrequency = f0;
            fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
                        
            % Run sequence
            obj.CreateSequence;
            obj.Sequence2String;
            fname = 'TempBatch.bat';
            obj.WriteBatchFile(fname);
            [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
            if status == 0 && ~isempty(result)
                C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                res = C{:};
                obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
            else
                warndlg('Batch aborted!','Warning')
                return;
            end
            DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
            cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
            fidList(:,1) = cDataBatch(1:nPoints)/bwCalNormVal;
            fidList(:,2) = cDataBatch(nPoints+1:end)/bwCalNormVal;
            
            rawData.outputs.fidList = fidList;
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
    end
    
end