classdef PrePulse_SliceMRI < MRI_BlankSequence
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    %  Elena Diaz Caballero
    
    properties
        StartSlice;
        EndSlice;
        PhasesPerBatch = 1;
        nScansPerBatch = 20;
        nScansB;
        StartPhase;
        EndPhase;
        PhaseVector;
        PhaseVectorIND;
        slice_ampl;
    end
    methods (Access=private)
        function CreateParameters(obj)
            InputParameters = {...
                'NPHASES','PHASETIME',...
                'PREPULSETIME','PREPULSEDELAY','PREPULSETR',...
                'ECHODELAY','REPETITIONDELAY',...
                'READOUTAMPLITUDE','READOUTAMPLITUDE2',...
                'STARTPHASE','ENDPHASE',...
                'SLICEGRADIENT','STARTSLICEFREQ','ENDSLICEFREQ',...
                };
            
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {...
                'Number of Phases','Phase Time',...
                'PrePulse Time','Prepulse Delay','Prepulse TR',...
                'Gradient Echo Delay','Repetition Delay',...
                'DEP Readout Amp','REP Readout Amp',...
                'Start Phase','End Phase',...
                'Slice Gradient','Start Slice Freq','End Slice Freq',...
                })];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,NaN...
                })];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,NaN...
                })];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,NaN...
                })];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {...
                '','us',...
                'ms','ms','s',...
                'us','ms',...
                '','',...
                '','',...
                '','kHz','kHz'...
                })];
            
            for k=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{k},InputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = PrePulse_SliceMRI(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'PrePulse Slice MRI';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
            obj.SetParameter('NPHASES',1);
            obj.SetParameter('PHASETIME',50*MyUnits.us);
            
            obj.SetParameter('PREPULSETIME',10*MyUnits.ms);
            obj.SetParameter('PREPULSEDELAY',10*MyUnits.ms);
            obj.SetParameter('PREPULSETR',1*MyUnits.s);
            
            obj.SetParameter('ECHODELAY',0.1*MyUnits.us);
            obj.SetParameter('REPETITIONDELAY',50*MyUnits.ms);
            
            obj.SetParameter('READOUTAMPLITUDE',0.1);
            obj.SetParameter('READOUTAMPLITUDE2',0.1);
            
            obj.SetParameter('STARTPHASE',-0.1)
            obj.SetParameter('ENDPHASE',0.1)
            
            obj.SetParameter('SLICEGRADIENT',0.1);
            obj.SetParameter('STARTSLICEFREQ',-0.1*MyUnits.kHz)
            obj.SetParameter('ENDSLICEFREQ',0.1*MyUnits.kHz)
            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
        end
        function WriteBatchFile(obj,fname) 
            nscan_aux = obj.GetParameter('NSCANS');
            obj.SetParameter( 'NSCANS', 1); % this was previously directly done in MRI_BlankSequence2.m
            WriteBatchFile@MRI_BlankSequence(obj,fname);
            obj.SetParameter( 'NSCANS', nscan_aux);
        end
        function UpdateTETR( obj )
            nPoints = obj.GetParameter('NPOINTS');
            
            % number for each axes
            nPhases = obj.GetParameter('NPHASES');
            nSlices = obj.GetParameter('NSLICES');
            nScans = obj.GetParameter('NSCANS');
            
            % do switch for if the readouts and gradients are enabled
            if ~obj.readout_en
                nReadouts = 1; StartReadout = 0; EndReadout = 0;
            end
            if ~obj.phase_en
                nPhases = 1; StartPhase = 0; EndPhase = 0;
            end
            if ~obj.slice_en
                nSlices = 1; StartSlice = 0; EndSlice = 0;
            end
            
            nPhaseBatches = ceil(nPhases/obj.PhasesPerBatch);
                        
            
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
                
            obj.TE = 0.5*acq_time + (0.5+.5)*obj.GetParameter('PULSETIME') + ...
                     4*obj.GetParameter('COILRISETIME')+ ...
                     obj.GetParameter('TRANSIENTTIME')+ ...
                     obj.GetParameter('PHASETIME')+...
                     obj.GetParameter('ECHODELAY');
            obj.TR = 0.5*acq_time + 0.5*obj.GetParameter('PULSETIME')+...
                     4*obj.GetParameter('COILRISETIME')+ ...
                     obj.GetParameter('BLANKINGDELAY') +...
                     obj.GetParameter('PHASETIME')+...
                     obj.GetParameter('REPETITIONDELAY') + obj.TE;
           if obj.spoiler_en
               obj.TR = obj.TR + obj.GetParameter('SPOILERTIME'); 
           end
           
           tempTR = obj.TR;
           if obj.GetParameter('PREPULSETIME') > 0
               obj.TR = obj.TR + obj.GetParameter('PREPULSETIME') + obj.GetParameter('PREPULSEDELAY') + obj.GetParameter('PREPULSETR');
           end
           
           
           obj.TTotal = ( tempTR * (nPhases-nPhaseBatches) + obj.TR*nPhaseBatches )  * nSlices * nScans ;
        end
        
        
        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence,
            %         "1", central frequency adjustment sequence,
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            
            [~,nPhases,readout,slice,phase,spoiler,~] = obj.ConfigureGradients(mode);
            
            % Calculate the number of line reads
            obj.nline_reads=obj.nScansB;
            
            
            % Initialize all vectors to zero with the appropriate size
            if obj.GetParameter('PREPULSETIME') > 0
                obj.nInstructions=15*obj.nline_reads+5;
            else
                obj.nInstructions=15*obj.nline_reads+3;
            end
            
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
%             
%             disp(numel(obj.AMPs))
            
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
                        
            % readout gradient amplitude calculation
            if( readout == 0 )
                dephasing_readout = shimR;
                phasing_readout = shimR;
            elseif( obj.GetParameter('PHASETIME') == 0 )
                dephasing_readout = shimR;
                phasing_readout = min(1.0, max( -1.0, shimR -1.0 * obj.GetParameter('READOUTAMPLITUDE2')));
            elseif( acq_time  > 2 * (obj.GetParameter('PHASETIME') + 1e-6)  )
                dephasing_readout = min(1.0, max( -1.0, shimR +1.0 * obj.GetParameter('READOUTAMPLITUDE')));
                phasing_readout = min( 1.0, max( -1.0, shimR -2 *obj.GetParameter('READOUTAMPLITUDE2') * (obj.GetParameter('PHASETIME') + 1e-6) / ( acq_time )));           
            else
                phasing_readout = min(1.0, max( -1.0, shimR -1.0 * obj.GetParameter('READOUTAMPLITUDE2')));
                dephasing_readout = max( -1.0, min( 1.0, shimR + obj.GetParameter('READOUTAMPLITUDE') *  acq_time / (2 * (obj.GetParameter('PHASETIME') + 1e-6) )));        
            end
            
            % slice amplitude calculation
            rephasing_slice_time = .75*obj.GetParameter('PULSETIME')*obj.GetParameter('SLICEGRADIENT')/0.4;
            
            if( slice == 0 )
                slice_dephase_amplitude = shimS;
                slice_rephase_amplitude = shimS;
            else
                slice_dephase_amplitude = min(1.0 , max(-1.0,shimS + obj.GetParameter('SLICEGRADIENT')));
                slice_rephase_amplitude = min(1.0 , max(-1.0,shimS - sign(obj.GetParameter('SLICEGRADIENT'))*0.4));
            end
            
            
            %%% BEGIN THE PULSE PROGRAMMING
            iIns  = 1;
            bbit = 1;
            bbit2 = 2;
 
            
            %------ Shimming in all gradients (3 instructions)
            nIns  = 3; % set number of instructions in this group
            vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                obj.AMPs(vIns)     = [shimP shimR shimS];
                obj.DACs(vIns)     = [PP RR SS];
                obj.WRITEs(vIns)   = [1 1 1];
                obj.UPDATEs(vIns)  = [1 1 1];
                obj.PhResets(vIns) = [0 0 0];
                obj.ENVELOPEs(vIns)= [7 7 7]; % (7=no shape)
                obj.FLAGs(vIns)    = [0 0 0];
                obj.DELAYs(vIns)   = [0.1 0.1 obj.GetParameter('COILRISETIME')*1e6]; %time in us
            iIns = iIns + nIns; % update the number of instructions in this block
            
            % PREPULSE IF THE PREPULSETIME IS GREATER THAN 0
            if obj.GetParameter('PREPULSETIME') > 0
                nIns  = 2; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [shimR shimR];
                    obj.DACs(vIns)     = [RR RR];
                    obj.WRITEs(vIns)   = [1 1];
                    obj.UPDATEs(vIns)  = [1 1];
                    obj.PhResets(vIns) = [0 0];
                    obj.ENVELOPEs(vIns)= [7 7]; % (7=no shape)
                    obj.FLAGs(vIns)    = [bbit2 0];
                    obj.DELAYs(vIns)   = [obj.GetParameter('PREPULSETIME')*1e6 obj.GetParameter('PREPULSEDELAY')*1e6 ]; %time in us
                iIns = iIns + nIns; % update the number of instructions
            end

            for scani = 1:obj.nScansB

                % BLANKING DELAY (1 instruction)
                nIns  = 1; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.WRITEs(vIns)   = 0;
                    obj.UPDATEs(vIns)  = 0;
                    obj.PhResets(vIns) = 0;
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.FLAGs(vIns)    = bbit;
                    obj.DELAYs(vIns)   = obj.GetParameter('BLANKINGDELAY')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions
            
                % COILRISE TIME SLICE SELECT DELAY (1 instruction)
                nIns  = 1; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = slice_dephase_amplitude;
                    obj.DACs(vIns)     = SS;
                    obj.WRITEs(vIns)   = 1;
                    obj.UPDATEs(vIns)  = 1;
                    obj.PhResets(vIns) = 1;
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.FLAGs(vIns)    = bbit;
                    obj.DELAYs(vIns)   = obj.GetParameter('COILRISETIME')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
                %------ RF pulse (1 instruction)
                nIns = 1; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.TXs(vIns)      = 1;
                    obj.FREQs(vIns)    = slice;
                    if (obj.RF_Shape) % Shaped RF pulse
                        obj.ENVELOPEs(vIns)= 0;
                    else
                        obj.ENVELOPEs(vIns)= 7;
                    end
                    obj.FLAGs(vIns)    = bbit;
                    obj.OPCODEs(vIns)  = 0;
                    obj.DELAYs(vIns)   = obj.GetParameter('PULSETIME')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                                
                % COILFALL, SLICE SELECT REPHASE and TRANSIENTTIME (3 instruction)
                nIns  = 3; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [shimS slice_rephase_amplitude shimS];
                    obj.DACs(vIns)     = [SS SS SS];
                    obj.WRITEs(vIns)   = [1 1 1];
                    obj.UPDATEs(vIns)  = [1 1 1];
                    obj.PhResets(vIns) = [0 0 0];
                    obj.ENVELOPEs(vIns)= [7 7 7]; % (7=no shape)
                    obj.FLAGs(vIns)    = [0 0 0];
                    obj.DELAYs(vIns)   = [obj.GetParameter('COILRISETIME')*1e6 rephasing_slice_time*1e6 (obj.GetParameter('TRANSIENTTIME')+obj.GetParameter('COILRISETIME'))*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
                % phase gradient amplitude calculation
                if( phase == 0 )
                    phase_dephase_amplitude = shimP;
                    phase_rephase_amplitude = shimP;
                else
                    phase_dephase_amplitude =  min(1.0 , max(-1.0,shimP + obj.PhaseVector(obj.StartPhase)));
                    phase_rephase_amplitude =  min(1.0 , max(-1.0,shimP - obj.PhaseVector(obj.StartPhase)));
                end
                
                %----- Phase gradient, rephasing readout gradient, reset phase gradients and echo delay (4 instructions)
                nIns  = 4; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [dephasing_readout phase_dephase_amplitude shimP phasing_readout];
                    obj.DACs(vIns)     = [RR PP PP RR];
                    obj.WRITEs(vIns)   = [1 1 1 1];
                    obj.UPDATEs(vIns)  = [1 1 1 1];
                    obj.ENVELOPEs(vIns)= [7 7 7 7]; % (7=no shape)
                    obj.DELAYs(vIns)   = [0.1 (obj.GetParameter('PHASETIME')+obj.GetParameter('COILRISETIME'))*1e6 0.1 (obj.GetParameter('COILRISETIME') + obj.GetParameter('ECHODELAY'))*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
                %----- Rephasing Readout Gradient and Data Acquisition. Rewinding lobe for readout, slice and phase gradients (3 instructions)
                nIns  = 3; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [phasing_readout shimR phase_rephase_amplitude];
                    obj.DACs(vIns)     = [RR RR PP];
                    obj.WRITEs(vIns)   = [1 1 1];
                    obj.UPDATEs(vIns)  = [1 1 1];
                	obj.RXs(vIns)      = [1 0 0];
                    obj.FREQs(vIns)    = [slice 0 0];
                    obj.ENVELOPEs(vIns)= [7 7 7]; % (7=no shape)
                    obj.DELAYs(vIns)   = [acq_time*1e6 obj.GetParameter('COILRISETIME')*1e6 (obj.GetParameter('PHASETIME')+obj.GetParameter('COILRISETIME'))*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
                
                %  Spoiler for slice gradient (1 instruction)
                if spoiler
                    nIns  = 1; % set number of instructions in this group
                    vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                        obj.AMPs(vIns)     = obj.GetParameter('SPOILERAMP');
                        obj.DACs(vIns)     = SS;
                        obj.WRITEs(vIns)   = 1;
                        obj.UPDATEs(vIns)  = 1;
                        obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                        obj.DELAYs(vIns)   = obj.GetParameter('SPOILERTIME')*1e6; %time in us
                    iIns = iIns + nIns; % update the number of instructions
                else
                    nIns  = 1; % set number of instructions in this group
                    vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                        obj.AMPs(vIns)     = 0.0;
                        obj.DACs(vIns)     = 3;
                        obj.WRITEs(vIns)   = 0;
                        obj.UPDATEs(vIns)  = 0;
                        obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                        obj.DELAYs(vIns)   = 0.1; %time in us
                    iIns = iIns + nIns; % update the number of instructions
                end
                
                
                %----- Repetition Delay (1 instruction)
                nIns  = 1; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.WRITEs(vIns)   = 1;
                    obj.UPDATEs(vIns)  = 1;
                    obj.CLEARs(vIns)   = 1;
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    if (scani == obj.nScansB) && (obj.GetParameter('PREPULSETIME') > 0)
                        obj.DELAYs(vIns)   = (obj.GetParameter('PREPULSETR')+obj.GetParameter('REPETITIONDELAY'))*1e6; %time in us
                    else
                        obj.DELAYs(vIns)   = obj.GetParameter('REPETITIONDELAY')*1e6; %time in us
                    end
                iIns = iIns + nIns; % update the number of instructions
                    
            end % end for nPhases
        end % end create gradient echo pulse sequence
        
        function [status,result] = Run(obj,iAcq)
            global MRIDATA
            
            SliceGradient = obj.GetParameter('SLICEGRADIENT');
            StartSliceFreq = obj.GetParameter('STARTSLICEFREQ');
            EndSliceFreq = obj.GetParameter('ENDSLICEFREQ');
            StartPhase = obj.GetParameter('STARTPHASE');
            EndPhase = obj.GetParameter('ENDPHASE');
            nSlices = obj.GetParameter('NSLICES');
            
            if nSlices > 1
                SliceStep = (EndSliceFreq - StartSliceFreq)/(nSlices-1);
            else
                SliceStep = 0;
            end
            
            nPhases = obj.GetParameter('NPHASES');
            if nPhases > 1
                PhaseStep = (EndPhase - StartPhase)/(nPhases-1);
                
                % make phase vector
                tempVector = StartPhase:PhaseStep:EndPhase;
                obj.PhaseVectorIND = 1:length(tempVector);%randperm( length(tempVector) );
                obj.PhaseVector = tempVector( obj.PhaseVectorIND );
            else
                PhaseStep = 0;
                % make phase vector
                tempVector = 0;
                obj.PhaseVectorIND = 1;
                obj.PhaseVector = 0;
                
            end
            nPhaseBatches = ceil(nPhases/obj.PhasesPerBatch);
            
            nPoints = obj.GetParameter('NPOINTS');
            Data3D = zeros(nPoints,nPhases,nSlices);
            nScans = obj.GetParameter('NSCANS');
            Data4D = zeros(nPoints,nPhases,nSlices,nScans);
            Data4DCF = zeros(nPoints,nPhases,nSlices,nScans);
            
            if isfield(MRIDATA,'Data3Dnew')
                if obj.averaging
                    if iAcq == 1
                        MRIDATA.Data3Dnew = [];
                    end
                else
                    MRIDATA.Data3Dnew = [];
                end
            else
                MRIDATA.Data3Dnew = [];
            end
            
            if isfield(MRIDATA,'Data4D')
                if obj.averaging
                    if iAcq == 1
                        MRIDATA.Data4D = [];
                    end
                else
                    MRIDATA.Data4D = [];
                end
            else
                MRIDATA.Data4D = [];
            end
            
            if isfield(MRIDATA,'Data4DCF')
                if obj.averaging
                    if iAcq == 1
                        MRIDATA.Data4DCF = [];
                    end
                else
                    MRIDATA.Data4DCF = [];
                end
            else
                MRIDATA.Data4DCF = [];
            end
            
            cn = nSlices*nPhaseBatches;
            count = 0;
            if nPhaseBatches>1
                h_waitbar = waitbar(count/cn,'Please wait (por favor espere)...');
            end
            
            
            for iSlice = 1:nSlices
                
                obj.SliceFreqOffset = ( StartSliceFreq + (iSlice-1)*SliceStep ) ;
                
                Data2D = zeros(nPoints,nPhases);
                
                for iBatch = 1:nPhaseBatches
                    
                    nScansLoops = ceil(obj.GetParameter('NSCANS')/obj.nScansPerBatch);
                    
                    DataScan = [];
                    CFDataScan = [];
                    for iSBatch = 1:nScansLoops
                        
                        if iSBatch == nScansLoops
                            if mod(obj.GetParameter('NSCANS'),obj.nScansPerBatch) == 0
                                obj.nScansB = obj.nScansPerBatch;
                            else
                                obj.nScansB = mod(obj.GetParameter('NSCANS'),obj.nScansPerBatch);
                            end
                        else
                            obj.nScansB = obj.nScansPerBatch;
                        end
                        
                        if nPhases >1
                            obj.CreateSequence(1); % Create the gradient echo sequence for CF
                            obj.Sequence2String;
                            fname_CF = 'TempBatch_CF.bat';
                            obj.WriteBatchFile(fname_CF);
                            obj.CenterFrequency(fname_CF);
                        end
                        obj.SetParameter('LASTFREQUENCY',obj.GetParameter('FREQUENCY'));


                        % NOW RUN ACTUAL ACQUISTION
                        obj.StartPhase = iBatch;% obj.GetParameter('STARTPHASE') + (iBatch-1)*obj.PhasesPerBatch*PhaseStep;
                        obj.EndPhase = obj.StartPhase + (obj.PhasesPerBatch-1)*PhaseStep;

                        obj.CreateSequence(0); % Create the normal gradient echo sequence

                        obj.Sequence2String;


                        fname = 'TempBatch.bat';
                        obj.WriteBatchFile(fname);
                        [status,result] = system(fullfile(obj.path_file,fname));

                        if status == 0 && ~isempty(result)
                            C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                            res = C{:};
                            obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                            obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                            obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                            obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                            obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                        end

                        DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                        cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                        nScansInBatch=length(cDataBatch)/nPoints;
                        d = reshape(cDataBatch,nPoints,nScansInBatch);
                        DataScan = cat(2,DataScan,d);
                        
                        if nPhases == 1
                            MRIDATA.TempCF = cDataBatch;
                        end
                        CFDataScan = cat(2,CFDataScan,reshape(MRIDATA.TempCF, nPoints,nScansInBatch));
                    end
                    
                    Data4D(:,obj.PhaseVectorIND(iBatch),iSlice,:) = permute(DataScan,[1, 3, 4, 2]);
                    Data4DCF(:,obj.PhaseVectorIND(iBatch),iSlice,:) = permute(CFDataScan,[1, 3, 4, 2]);
                    
                    Data2D(:,obj.PhaseVectorIND(iBatch)) = mean(DataScan,2);
                    count = count+1;
                    if nPhaseBatches>1
                        waitbar(count/cn,h_waitbar,sprintf('Por favor espere..... %5.2f %%',100*count/cn));
                    end
                end
                Data3D(:,:,iSlice) = Data2D;
                if length(obj.cData) ~= nPoints*nPhases*nSlices;
                    obj.cData = zeros(obj.NAverages,nPoints*nPhases*nSlices);
                end
                if obj.averaging
                    Nav = obj.NAverages;
                else
                    Nav = 1;
                end
                index = mod(iAcq-1,Nav)+1;
                if obj.autoPhasing
                    %                    pt = max(Data3D(:,ceil(size(Data3D,2)/2),ceil(size(Data3D,3)/2)));
                    pt = max(Data3D(:));
                    Data3D = Data3D.*exp(complex(0,-angle(pt)));
                end
                obj.cData(index,1:length(Data3D(:))) = Data3D(:);
                obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
                obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
                %    obj.fData1D = fftshift(ifft(fftshift(obj.cDataAv)));
                obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
                acq_time = obj.GetParameter('ACQUISITIONTIME');
                obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*acq_time/nPhases;
                obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time/nPhases;
            end
            MRIDATA.Data4D(numel(MRIDATA.Data4D)+1).Fid4D = Data4D;
            MRIDATA.Data4DCF(numel(MRIDATA.Data4DCF)+1).Fid4D = Data4DCF;
            if nPhaseBatches>1
                delete(h_waitbar);
            end
        end
    end
    
end

