classdef GradientEcho3D_DWI < MRI_BlankSequence
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    %  Elena Diaz Caballero
    %  Ryan Hilaman
    %  30 July 2014
    
    properties
        StartSlice;
        EndSlice;
        PhasesPerBatch = 20;
        StartPhase;
        EndPhase;
    end
    methods (Access=private)
        function CreateParameters(obj)
            InputParameters = {...
                'NPHASES','PHASETIME','ECHODELAY','REPETITIONDELAY','READOUTAMPLITUDE','READOUTAMPLITUDE2',...
                'DWIAMPLITUDE','DWITIME'...
                'SLICEBANDWIDTH','STARTPHASE',...
                'ENDPHASE','STARTSLICE','ENDSLICE',...
                'GAUSSIANFILTER'...
                };
            
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {...
                'Number of Phases','Phase Time','Gradient Echo Delay','Repetition Delay','DEP Readout Amplitude','REP Readout Amplitude',...
                'DWI Amplitude','DWI Time',...
                'Slice Bandwidth','Start Phase',...
                'End Phase','Start Slice','End Slice',...
                'Gaussian'...
                })];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,NaN,...
                NaN...
                })];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,NaN,...
                0 ...
                })];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,NaN,...
                1 ...
                })];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {...
                '','us','us','ms','','',...
                '','us',...
                'MHz','',...
                '','','',...
                ''...
                })];
           
            for k=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{k},InputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = GradientEcho3D_DWI(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'Gradient Echo DWI';
            obj.ProgramName = 'MRI_BlankSequence.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
            obj.SetParameter('PHASETIME',50*MyUnits.us);
            obj.SetParameter('ECHODELAY',20*MyUnits.us);
            obj.SetParameter('REPETITIONDELAY',50*MyUnits.ms);
            obj.SetParameter('READOUTAMPLITUDE',0.1);
            obj.SetParameter('READOUTAMPLITUDE2',0.1);
            obj.SetParameter('DWIAMPLITUDE',0.1);
            obj.SetParameter('DWITIME',10*MyUnits.us);
            obj.SetParameter('NPHASES',1);
%             obj.SetParameter('NSLICES',1);
            obj.SetParameter('STARTPHASE',-0.1);
            obj.SetParameter('ENDPHASE',0.1);
            obj.SetParameter('STARTSLICE',-0.1);
            obj.SetParameter('ENDSLICE',0.1);
            obj.SetParameter('GAUSSIANFILTER',1);
            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
        end
        function CreateSequence( obj, CF )
                        
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            
            if (CF)
                nSlices = 1;
                nPhases = 1;
                readout =0; % None of the gradients should be enabled if we are setting the central frequency (CF)
                slice =0;
                phase =0;
                spoiler =0;
            else
               obj.StartSlice = obj.GetParameter('STARTSLICE');
               obj.EndSlice = obj.GetParameter('ENDSLICE');
               nSlices = obj.GetParameter('NSLICES');
               if (abs(obj.EndPhase - obj.GetParameter('STARTPHASE')) - abs(obj.GetParameter('ENDPHASE') - obj.GetParameter('STARTPHASE'))) > 1e-6 %last batch may be smaller
                   nPhases = mod(obj.GetParameter('NPHASES'),obj.PhasesPerBatch);
                   obj.EndPhase = obj.GetParameter('ENDPHASE');
               else
                   nPhases = min(obj.GetParameter('NPHASES'),obj.PhasesPerBatch);
               end
                readout =obj.readout_en;
                slice =obj.slice_en;
                phase =obj.phase_en;
                spoiler =obj.spoiler_en;
                %DWI = obj.dwi_en;
            end
            obj.nline_reads=nPhases*nSlices;
            
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions=23*nPhases*nSlices;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
            
            % readout gradient amplitude calculation
            if( readout == 0 ) 
              dephasing_readout = shimR; 
              phasing_readout = shimR; 
            elseif( obj.GetParameter('PHASETIME') == 0 ) 
              dephasing_readout = shimR; 
              phasing_readout = min(1.0, max( -1.0, shimR -1.0 * obj.GetParameter('READOUTAMPLITUDE2')));
            elseif( acq_time  > 2 * (obj.GetParameter('PHASETIME') + 1e-6)  ) 
              dephasing_readout = min(1.0, max( -1.0, shimR +1.0 * obj.GetParameter('READOUTAMPLITUDE')));
              phasing_readout = min( 1.0, max( -1.0, shimR -2 *obj.GetParameter('READOUTAMPLITUDE2') * (obj.GetParameter('PHASETIME') + 1e-6) / ( acq_time )));
            else 
              phasing_readout = min(1.0, max( -1.0, shimR -1.0 * obj.GetParameter('READOUTAMPLITUDE2')));
              dephasing_readout = max( -1.0, min( 1.0, shimR + obj.GetParameter('READOUTAMPLITUDE') *  acq_time / (2 * (obj.GetParameter('PHASETIME') + 1e-6) )));
            end
            
            for slicei = 1:nSlices 
              if( slice == 0 ) 
                  slice_amplitude = shimS;
              else 
                  if (nSlices == 1) 
                    slice_amplitude = obj.StartSlice;
                  else 
                    slice_amplitude =  min(1.0 , max(-1.0,shimS + (obj.EndSlice - obj.StartSlice) * (slicei-1)/(nSlices-1) + obj.StartSlice));
                  end
              end
              
              for phasei = 1:nPhases   
                iline=23*((slicei-1)*nPhases+(phasei-1));
                %------ Blanking Delay and Shimming in all gradients (3 instructions)
                obj.AMPs(iline+(1:3))     = [shimP shimR shimS];
                obj.DACs(iline+(1:3))     = [PP RR SS ];
                obj.WRITEs(iline+(1:3))   = [1 1 1];
                obj.UPDATEs(iline+(1:3))  = [1 1 1];
                obj.PhResets(iline+(1:3)) = [0 0 1];
                obj.ENVELOPEs(iline+(1:3))= [7 7 7]; % (7=no shape)
                obj.FLAGs(iline+(1:3))    = [1 1 1];
                obj.DELAYs(iline+(1:3))   = [0.1 0.1 obj.GetParameter('BLANKINGDELAY')*1e6]; %time in us
                
                %------ RF pulse (1 instruction) 
                obj.AMPs(iline+4)     = 0.0;
                obj.DACs(iline+4)     = 3;
                obj.TXs(iline+4)      = 1;
                if (obj.RF_Shape) % Shaped RF pulse
                  obj.ENVELOPEs(iline+4)= 0;
                else
                  obj.ENVELOPEs(iline+4)= 7;
                end
                obj.FLAGs(iline+4)    = 1;
                obj.OPCODEs(iline+4)  = 0;
                obj.DELAYs(iline+4)   = obj.GetParameter('PULSETIME')*1e6; %time in us
                
                %----- Transient Delay and setting the slice and readout dephasing gradients (3 instructions)
                obj.AMPs(iline+(5:7))     = [shimS slice_amplitude dephasing_readout];
                obj.DACs(iline+(5:7))     = [SS SS RR];
                obj.WRITEs(iline+(5:7))   = [1 1 1];
                obj.UPDATEs(iline+(5:7))  = [0 1 1];
                obj.ENVELOPEs(iline+(5:7))= [7 7 7]; % (7=no shape)
                obj.DELAYs(iline+(5:7))   = [obj.GetParameter('TRANSIENTTIME')*1e6 0.1 0.1]; %time in us
                
                %---- Diffusion gradient amplitude calculation
              
                dwiR_amplitude1 = min(1.0, max(-1.0, 1.0*obj.GetParameter('DWIAMPLITUDE')));
                dwiR_amplitude2 = max(-1.0, min(1.0, -1.0*obj.GetParameter('DWIAMPLITUDE')));
                dwiP_amplitude1 = min(1.0,max(-1.0, 1.0*obj.GetParameter('DWIAMPLITUDE')));
                dwiP_amplitude2 = max(-1.0, min(1.0, -1.0*obj.GetParameter('DWIAMPLITUDE')));
                
                %---- Diffusion gradient positive lobe (3 instructions)
                obj.AMPs(iline+(8:10))     = [shimS dwiR_amplitude1 dwiP_amplitude1];
                obj.DACs(iline+(8:10))     = [SS RR PP];
                obj.WRITEs(iline+(8:10))    = [1 1 1];
                obj.UPDATEs(iline+(8:10))   = [1 1 1];
                obj.ENVELOPEs(iline+(8:10))= [7 7 7];
                obj.DELAYs(iline+(8:10))   = [0.1 0.1 obj.GetParameter('DWITIME')*1e6];
                
                %---- Diffusion gradient negative lobe (3 instructions)
                obj.AMPs(iline+(11:13))     = [shimS dwiR_amplitude2 dwiP_amplitude2 ];
                obj.DACs(iline+(11:13))     = [SS RR PP];
                obj.WRITEs(iline+(11:13))    = [1 1 1];
                obj.UPDATEs(iline+(11:13))   = [1 1 1];
                obj.ENVELOPEs(iline+(11:13))= [7 7 7];
                obj.DELAYs(iline+(11:13))   = [0.1 0.1 obj.GetParameter('DWITIME')*1e6];
                
                % phase gradient amplitude calculation
                if( phase == 0 )
                  phase_amplitude = shimP;
                else 
                  phase_amplitude =  min(1.0 , max(-1.0,shimP + (obj.EndPhase - obj.StartPhase) * (phasei-1)/(nPhases-1) + obj.StartPhase));
                end
                
                %----- Phase gradient, rephasing readout gradient, reset slice and phase gradients and echo delay (4 instructions)
                obj.AMPs(iline+(14:17))     = [phase_amplitude shimP phasing_readout shimS];
                obj.DACs(iline+(14:17))     = [PP PP RR SS];
                obj.WRITEs(iline+(14:17))   = [1 1 1 1];
                obj.UPDATEs(iline+(14:17))  = [1 1 1 1];
                obj.ENVELOPEs(iline+(14:17))= [7 7 7 7]; % (7=no shape)
                obj.DELAYs(iline+(14:17))   = [(obj.GetParameter('PHASETIME')+obj.GetParameter('COILRISETIME'))*1e6 0.1 obj.GetParameter('COILRISETIME')*1e6 obj.GetParameter('ECHODELAY')*1e6]; %time in us
                %----- Rephasing Readout Gradient and Data Acquisition. Rewinding lobe for readout, slice and phase gradients (4 instructions)
                obj.AMPs(iline+(18:21))     = [phasing_readout shimR -slice_amplitude -phase_amplitude];
                obj.DACs(iline+(18:21))     = [RR RR SS PP];
                obj.WRITEs(iline+(18:21))   = [1 1 1 1];
                obj.UPDATEs(iline+(18:21))  = [1 1 1 1];
                obj.RXs(iline+(18:21))      = [1 0 0 0];
                obj.ENVELOPEs(iline+(18:21))= [7 7 7 7]; % (7=no shape)
                obj.DELAYs(iline+(18:21))   = [acq_time*1e6 obj.GetParameter('COILRISETIME')*1e6 0.1 (obj.GetParameter('PHASETIME')+obj.GetParameter('COILRISETIME'))*1e6]; %time in us
                %----- Spoiler for slice gradient (1 instruction)
                if (spoiler)
                  obj.AMPs(iline+22)     = obj.GetParameter('SPOILERAMP');
                else
                  obj.AMPs(iline+22)     = shimS;
                end
                obj.DACs(iline+22)     = SS;
                obj.WRITEs(iline+22)   = 1;
                obj.UPDATEs(iline+22)  = 1;
                obj.CLEARs(iline+22)   = 1;
                obj.ENVELOPEs(iline+22)= 7; % (7=no shape)
                obj.DELAYs(iline+22)   = obj.GetParameter('SPOILERTIME')*1e6; %time in us
                %----- Repetition Delay (1 instruction)
                obj.AMPs(iline+23)     = 0.0;
                obj.DACs(iline+23)     = 3;
                obj.WRITEs(iline+23)   = 1;
                obj.UPDATEs(iline+23)  = 1;
                obj.CLEARs(iline+23)   = 1;
                obj.ENVELOPEs(iline+23)= 7; % (7=no shape)
                obj.DELAYs(iline+23)   = obj.GetParameter('REPETITIONDELAY')*1e6; %time in us
              end % end for nPhases
            end %end for slicei
           
        end % end create gradient echo pulse sequence
        function [status,result] = Run(obj,iAcq)
                  
            obj.StartSlice = obj.GetParameter('STARTSLICE');
            obj.EndSlice = obj.GetParameter('ENDSLICE');
            obj.StartPhase = obj.GetParameter('STARTPHASE');
            obj.EndPhase = obj.GetParameter('ENDPHASE');
            nSlices = obj.GetParameter('NSLICES');
            if nSlices > 1
                SliceStep = (obj.EndSlice - obj.StartSlice)/(nSlices-1);
            else
                SliceStep = 0;
            end
            nPhases = obj.GetParameter('NPHASES');
            if nPhases > 1
                PhaseStep = (obj.EndPhase - obj.StartPhase)/(nPhases-1);
            else
                PhaseStep = 0;
            end
            nPhaseBatches = ceil(nPhases/obj.PhasesPerBatch);
            nPoints = obj.GetParameter('NPOINTS');
            Data3D = zeros(nPoints,nPhases,nSlices);
            cn = nSlices*nPhaseBatches;
            count = 0;
            if nPhaseBatches>1
                h_waitbar = waitbar(count/cn,'Please wait (por favor espere)...');
            end
            for iSlice = 1:nSlices
                    
                obj.StartSlice = obj.GetParameter('STARTSLICE') + (iSlice-1)*SliceStep;
                obj.EndSlice = obj.StartSlice;
                Data2D = [];
                for iBatch = 1:nPhaseBatches
                
                      if nPhases >1
                          obj.CreateSequence(1); % Create the gradient echo sequence for CF
                          obj.Sequence2String;
                          fname_CF = 'TempBatch_CF.bat';
                          obj.WriteBatchFile(fname_CF);
                          obj.CenterFrequency(fname_CF);
                      end
                      obj.SetParameter('LASTFREQUENCY',obj.GetParameter('FREQUENCY'));
                    
                    
                    % NOW RUN ACTUAL ACQUISTION
                    obj.StartPhase = obj.GetParameter('STARTPHASE') + (iBatch-1)*obj.PhasesPerBatch*PhaseStep;
                    obj.EndPhase = obj.StartPhase + (obj.PhasesPerBatch-1)*PhaseStep;
                    
                    obj.CreateSequence(0); % Create the normal gradient echo sequence
                    
                    obj.Sequence2String;
                    
                                       
                    fname = 'TempBatch.bat';
                    obj.WriteBatchFile(fname);
                    [status,result] = system(fullfile(obj.path_file,fname));

                    if status == 0 && ~isempty(result)
                        C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                        res = C{:};
                        obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                        obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                        obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                        obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                        obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                    end
                    
                    DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                    cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                    nPhasesInBatch=length(cDataBatch)/nPoints;
                    d = reshape(cDataBatch,nPoints,nPhasesInBatch);
                    Data2D = cat(2,Data2D,d);
                    count = count+1;
                    if nPhaseBatches>1
                        waitbar(count/cn,h_waitbar,sprintf('Por favor espere..... %5.2f %%',100*count/cn));
                    end
                end
                Data3D(:,:,iSlice) = Data2D;
                if length(obj.cData) ~= nPoints*nPhases*nSlices;
                    obj.cData = zeros(obj.NAverages,nPoints*nPhases*nSlices);
                end
                if obj.averaging
                    Nav = obj.NAverages;
                else
                    Nav = 1;
                end
                index = mod(iAcq-1,Nav)+1;
                if obj.autoPhasing
%                    pt = max(Data3D(:,ceil(size(Data3D,2)/2),ceil(size(Data3D,3)/2)));
                    pt = max(Data3D(:));
                    Data3D = Data3D.*exp(complex(0,-angle(pt)));
                end
                obj.cData(index,1:length(Data3D(:))) = Data3D(:);
                obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
                obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
                %    obj.fData1D = fftshift(ifft(fftshift(obj.cDataAv)));
                obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
                acq_time = obj.GetParameter('ACQUISITIONTIME');
                obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*acq_time/nPhases;
                obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time/nPhases;
            end
            if nPhaseBatches>1
                delete(h_waitbar);
            end
        end
    end
    
end 

