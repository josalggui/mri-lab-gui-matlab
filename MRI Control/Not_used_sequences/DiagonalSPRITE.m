classdef DiagonalSPRITE < MRI_BlankSequence3
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    %  Elena Diaz Caballero
    
    properties
        MaxPoints = 4096;
        MaxInstructions = floor((1000)/7);
        Axis1Vector;
        Axis2Vector;
        Axis3Vector;
        Axis1IND;
        Axis2IND;
        Axis3IND;
        StartPhase = 0;
        EndPhae = 0;
    end
    methods (Access=private)
        function CreateParameters(obj)
            InputParameters = {...
                'NPAVGS','NREADOUTS','NPHASES','TP','REPETITIONDELAY',...
                'STARTREADOUT','ENDREADOUT','STARTPHASE','ENDPHASE','STARTSLICE','ENDSLICE',...
                'PREPULSETIME','PREPULSEDELAY','PREPULSETR',...
                };
            
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {...
                '#Points to Average','Number of Readouts','Number of Phases','Phase encode time','Repetition Delay',...
                'Start Readout','End Readout','Start Phase','End Phase','Start Slice','End Slice',...
                'Prepulse Duration','Prepulse Delay','Prepulse TR',...
                })];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                })];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {...
                1,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                })];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                })];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {...
                '','','','us','ms',...
                '','','','','',''...
                'ms','ms','s',...
                })];
            
            for k=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{k},InputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = DiagonalSPRITE(program)
            obj = obj@MRI_BlankSequence3();
            obj.SequenceName = 'Diagonal SPRITE';
            obj.ProgramName = 'MRI_BlankSequence2.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence3(obj);
            obj.SetParameter('NPAVGS',1);
            obj.SetParameter('NREADOUTS',1);
            obj.SetParameter('NPHASES',1);
            obj.SetParameter('TP',50*MyUnits.us);
            obj.SetParameter('REPETITIONDELAY',50*MyUnits.ms);
            obj.SetParameter('STARTREADOUT',-0.1);
            obj.SetParameter('ENDREADOUT',0.1);
            obj.SetParameter('STARTPHASE',-0.1);
            obj.SetParameter('ENDPHASE',0.1);
            obj.SetParameter('STARTSLICE',-0.1);
            obj.SetParameter('ENDSLICE',0.1);
            
            obj.SetParameter('PREPULSETIME',10*MyUnits.ms);
            obj.SetParameter('PREPULSEDELAY',10*MyUnits.ms);
            obj.SetParameter('PREPULSETR',1*MyUnits.s);
            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
        end
        function UpdateTETR(obj)
            obj.TE = obj.GetParameter('TRANSIENTTIME')+obj.GetParameter('TP');
            obj.TR = obj.TE + obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH') +...
             obj.GetParameter('PULSETIME') + obj.GetParameter('COILRISETIME') + ...
             obj.GetParameter('REPETITIONDELAY');
            obj.TTotal = obj.TR * obj.GetParameter('NSCANS') * ...
                    obj.GetParameter('NSLICES') * obj.GetParameter('NPHASES') * obj.GetParameter('NREADOUTS');
        end
        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence,
            %         "1", central frequency adjustment sequence,
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            
            %blanking bits
            bbit = 1; % normal blanking bit
            bbit2 = 2; % blanking bit for the Prepulse
            
            % then grab the appropriate switches for IF the
            % readout/slice/phases were selected
            [~,~,readout,slice,phase,~,~] = obj.ConfigureGradients(mode);
            
            % Calculate the number of line reads based on the length of the Axis vector
            obj.nline_reads = numel( obj.Axis1Vector );
            
            % Initialize all vectors to zero with the appropriate size
            if obj.GetParameter('PREPULSETIME') > 0
                obj.nInstructions=8*obj.nline_reads+5;
            else
                obj.nInstructions=8*obj.nline_reads+3;
            end
            
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            % set the acquisition time
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
            
            %%% Gradient amplitude calculation
               % initialize gradient vectors
            [Axis1Vector, Axis2Vector, Axis3Vector] = deal( zeros(1, obj.nline_reads) );
               % set the bounds to the vectors
            MinVector = Axis1Vector - 1;
            MaxVector = Axis1Vector + 1;
            
            % readout switch set to Axis1
            if( readout == 0 )
                Axis1Vector = Axis1Vector + shimR;
            else
                Axis1Vector = max( min(MaxVector,obj.Axis1Vector), MinVector );
            end
            
            % phase switch set to Axis2
            if( phase == 0 )
                Axis2Vector = Axis2Vector + shimP;
            else
                Axis2Vector = max( min(MaxVector,obj.Axis2Vector), MinVector );
            end
            
            % slice switch set to Axis3
            if( slice == 0 )
                Axis3Vector = Axis3Vector + shimP;
            else
                Axis3Vector = max( min(MaxVector,obj.Axis3Vector), MinVector );
            end
            
            %%% BEGIN THE PULSE PROGRAMMING
            iIns = 1;
            %------ Shimming in all gradients (3 instructions)
            nIns = 3; % set the number of instruction in this block
            vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                obj.AMPs(vIns)     = [shimP shimR shimS ];
                obj.DACs(vIns)     = [PP RR SS ];
                obj.WRITEs(vIns)   = [1 1 1];
                obj.UPDATEs(vIns)  = [1 1 1];
                obj.PhResets(vIns) = [0 0 0];
                obj.ENVELOPEs(vIns)= [7 7 7]; % (7=no shape)
                obj.FLAGs(vIns)    = [0 0 0];
                obj.DELAYs(vIns)   = [0.1 0.1 obj.GetParameter('COILRISETIME')*1e6]; %time in us
            iIns = iIns + nIns; % update the number of instructions in this block
            
            % PREPULSE IF THE PREPULSETIME IS GREATER THAN 0
            if obj.GetParameter('PREPULSETIME') > 0
                nIns  = 2; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [shimR shimR];
                    obj.DACs(vIns)     = [RR RR];
                    obj.WRITEs(vIns)   = [1 1];
                    obj.UPDATEs(vIns)  = [1 1];
                    obj.PhResets(vIns) = [0 0];
                    obj.ENVELOPEs(vIns)= [7 7]; % (7=no shape)
                    obj.FLAGs(vIns)    = [bbit2 0];
                    obj.DELAYs(vIns)   = [obj.GetParameter('PREPULSETIME')*1e6 obj.GetParameter('PREPULSEDELAY')*1e6 ]; %time in us
                iIns = iIns + nIns; % update the number of instructions
            end
            
            % --- NOW START THE RF TRAIN
            for ipulse = 1:obj.nline_reads
                % BLANKING DELAY
                nIns  = 1; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.WRITEs(vIns)   = 0;
                    obj.UPDATEs(vIns)  = 0;
                    obj.PhResets(vIns) = 0;
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.FLAGs(vIns)    = bbit;
                    obj.DELAYs(vIns)   = obj.GetParameter('BLANKINGDELAY')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
                %------ Set All Gradients to the appropriate level, include coil rise time (3 instructions)
                nIns = 3; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [Axis1Vector(ipulse) Axis2Vector(ipulse) Axis3Vector(ipulse) ];
                    obj.DACs(vIns)     = [PP RR SS];
                    obj.WRITEs(vIns)   = [1 1 1];
                    obj.UPDATEs(vIns)  = [1 1 1];
                    obj.PhResets(vIns) = [0 0 1];
                    obj.ENVELOPEs(vIns)= [7 7 7]; % (7=no shape)
                    obj.FLAGs(vIns)    = [bbit bbit bbit];
                    obj.DELAYs(vIns)   = [0.1 0.1 obj.GetParameter('COILRISETIME')*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %------ RF pulse (1 instruction)
                nIns = 1; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.TXs(vIns)      = 1;
                    if (obj.RF_Shape) % Shaped RF pulse
                        obj.ENVELOPEs(vIns)= 0;
                    else
                        obj.ENVELOPEs(vIns)= 7;
                    end
                    obj.FLAGs(vIns)    = bbit;
                    obj.OPCODEs(vIns)  = 0;
                    obj.DELAYs(vIns)   = obj.GetParameter('PULSETIME')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %----- Transient Delay and TP Delay (1 instructions)
                nIns = 1; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.WRITEs(vIns)   = 0;
                    obj.UPDATEs(vIns)  = 0;
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.DELAYs(vIns)   = (obj.GetParameter('TRANSIENTTIME')+obj.GetParameter('TP'))*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
%                 %----- Remove Gradients (3 instructions)
%                 nIns = 3; % set the number of instruction in this block
%                 vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
%                     obj.AMPs(vIns)     = [shimP shimR shimS];
%                     obj.DACs(vIns)     = [PP RR SS];
%                     obj.WRITEs(vIns)   = [1 1 1];
%                     obj.UPDATEs(vIns)  = [1 1 1];
%                     obj.PhResets(vIns) = [0 0 0];
%                     obj.ENVELOPEs(vIns)= [7 7 7]; % (7=no shape)
%                     obj.FLAGs(vIns)    = [0 0 0];
%                     obj.DELAYs(vIns)   = [0.1 0.1 obj.GetParameter('COILRISETIME')*1e6]; %time in us
%                 iIns = iIns + nIns; % update the number of instructions in this block
                
                %----- Data Acquisition (1 instruction)
                nIns = 1; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.WRITEs(vIns)   = 0;
                    obj.UPDATEs(vIns)  = 0;
                    obj.RXs(vIns)      = 1;
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.DELAYs(vIns)   = acq_time*1e6;
                iIns = iIns + nIns; % update the number of instructions in this block   
                
                %----- TR (1 instructions)
                nIns = 1; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.WRITEs(vIns)   = 0;
                    obj.UPDATEs(vIns)  = 0;
                    obj.CLEARs(vIns)   = 1;
                    obj.ENVELOPEs(vIns)= 7;
                    if (ipulse == obj.nline_reads) && (obj.GetParameter('PREPULSETIME') > 0)
                        obj.DELAYs(vIns)   = (obj.GetParameter('PREPULSETR')+obj.GetParameter('REPETITIONDELAY'))*1e6; %time in us
                    else
                        obj.DELAYs(vIns)   = obj.GetParameter('REPETITIONDELAY')*1e6; %time in us
                    end
                iIns = iIns + nIns; % update the number of instructions in this block
                
            end % end the axis instruction loop
            
                
        end % end create gradient echo pulse sequence
        function [status,result] = Run(obj,iAcq)
            global MRIDATA
            
            %%% CALCULATE THE AXISVECTORS
            % Begin and end axes
            StartReadout = obj.GetParameter('STARTREADOUT');
            EndReadout = obj.GetParameter('ENDREADOUT');
            StartPhase = obj.GetParameter('STARTPHASE');
            EndPhase = obj.GetParameter('ENDPHASE');
            StartSlice = obj.GetParameter('STARTSLICE');
            EndSlice = obj.GetParameter('ENDSLICE');
            
            % number for each axes
            nReadouts = obj.GetParameter('NREADOUTS');
            nPhases = obj.GetParameter('NPHASES');
            nSlices = obj.GetParameter('NSLICES');
            
            % do switch for if the readouts and gradients are enabled
            if ~obj.readout_en
                nReadouts = 1; StartReadout = 0; EndReadout = 0;
            end
            if ~obj.phase_en
                nPhases = 1; StartPhase = 0; EndPhase = 0;
            end
            if ~obj.slice_en
                nSlices = 1; StartSlice = 0; EndSlice = 0;
            end
            
            % Create the Axis Vectors
                % first set the individual axis
                total_instructions = nReadouts * nPhases * nSlices;
                ReadoutMags = linspace(StartReadout,EndReadout,nReadouts);
                ReadoutINDS = linspace(1,nReadouts,nReadouts);
                PhaseMags = linspace(StartPhase,EndPhase,nPhases);
                PhaseINDS = linspace(1,nPhases,nPhases);
                SliceMags = linspace(StartSlice,EndSlice,nSlices);
                SliceINDS = linspace(1,nSlices,nSlices);
                
                % set the position vector indexes on a diagonal concept
                obj.Axis1IND = repmat(ReadoutINDS,1,nPhases*nSlices);
                obj.Axis2IND = PhaseINDS;
                for ishift = 1:nReadouts-1
                    obj.Axis2IND = cat(2, obj.Axis2IND, circshift(PhaseINDS,[0 ishift]));
                end
                obj.Axis2IND = kron(obj.Axis2IND, ones(1,nSlices));
                obj.Axis3IND = kron(SliceINDS,ones(1,nReadouts*nPhases));
                
                % now set the appropriate axis vectors based on the indexs
                Axis1Vector = ReadoutMags(obj.Axis1IND);
                Axis2Vector = PhaseMags(obj.Axis2IND);
                Axis3Vector = SliceMags(obj.Axis3IND);
                
                if isfield(MRIDATA,'Data4D')
                    if obj.averaging
                        if iAcq == 1
                            MRIDATA.Data4D = [];
                        end
                    else
                        MRIDATA.Data4D = [];
                    end
                else
                    MRIDATA.Data4D = [];
                end

                if isfield(MRIDATA,'Data4DCF')
                    if obj.averaging
                        if iAcq == 1
                            MRIDATA.Data4DCF = [];
                        end
                    else
                        MRIDATA.Data4DCF = [];
                    end
                else
                    MRIDATA.Data4DCF = [];
                end
                
            % Now calculate the batches that need to be run    
            nPoints = obj.GetParameter('NPOINTS');
            InsPerBatch = min( floor(obj.MaxPoints / nPoints), obj.MaxInstructions );
            nBatches = ceil( total_instructions / InsPerBatch );
            
            % initialize storage vectors
                Data3D = zeros(nReadouts,nPhases,nSlices);
%                 INDstore = zeros(nReadouts*nPhases*nSlices,3);
            % start the counter
            cn = nBatches;
            count = 0;
            if nBatches>1
                h_waitbar = waitbar(count/cn,'Please wait (por favor espere)...');
            end
            
            % NOW run through the batches
            for iBatch = 1:nBatches
                
%                 % calculate the center frequency
%                 if nBatches >1
%                     % set the axis vectors
%                     obj.Axis1Vector = 0;
%                     obj.Axis2Vector = 0;
%                     obj.Axis3Vector = 0;
%                     % set the number of points
%                     obj.SetParameter('NPOINTS',600);
%                     obj.CreateSequence(1); % Create the gradient echo sequence for CF
%                     obj.Sequence2String;
%                     fname_CF = 'TempBatch_CF.bat';
%                     obj.WriteBatchFile(fname_CF);
%                     obj.CenterFrequency(fname_CF);
%                     % revert the number of points back
%                     obj.SetParameter('NPOINTS',nPoints);
%                 end

                if iBatch == nBatches
                    Axis1IND = obj.Axis1IND(1+InsPerBatch*(iBatch-1):end);
                    Axis2IND = obj.Axis2IND(1+InsPerBatch*(iBatch-1):end);
                    Axis3IND = obj.Axis3IND(1+InsPerBatch*(iBatch-1):end);
%                     INDstore(1+InsPerBatch*(iBatch-1):end,1:3) = cat(2,Axis1IND(:),Axis2IND(:),Axis3IND(:));
                else
                    Axis1IND = obj.Axis1IND(1+InsPerBatch*(iBatch-1):InsPerBatch*iBatch);
                    Axis2IND = obj.Axis2IND(1+InsPerBatch*(iBatch-1):InsPerBatch*iBatch);
                    Axis3IND = obj.Axis3IND(1+InsPerBatch*(iBatch-1):InsPerBatch*iBatch);
%                     INDstore(1+InsPerBatch*(iBatch-1):InsPerBatch*iBatch,1:3) = cat(2,Axis1IND(:),Axis2IND(:),Axis3IND(:));
                end
                obj.Axis1Vector = Axis1Vector(Axis1IND);
                obj.Axis2Vector = Axis2Vector(Axis2IND);
                obj.Axis3Vector = Axis3Vector(Axis3IND);

%                 obj.SetParameter('LASTFREQUENCY',obj.GetParameter('FREQUENCY'));


                % NOW RUN ACTUAL ACQUISTION
                obj.CreateSequence(0); % Create the normal gradient echo sequence
                obj.Sequence2String;


                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                [status,result] = system(fullfile(obj.path_file,fname));

                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                end

                % Load in data
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                
                % Reorder the data
                d = reshape(cDataBatch,nPoints,numel(obj.Axis1Vector));
                
                % switch the FID if needed
                if obj.readout_en
                    % mean the data
                    DataScan = mean(d(1:obj.GetParameter('NPAVGS'),:),1);
                    % place the data in the appropriate positions of the 3D matrix
                    linearIND = sub2ind([nReadouts,nPhases,nSlices],Axis1IND,Axis2IND,Axis3IND);
                    Data3D(linearIND) = DataScan(:);
                else
                    Data3D = d;
                end
                
                
                % update the counter
                if nBatches>1
                    count = count+1;
                    waitbar(count/cn,h_waitbar,sprintf('Please wait (por favor espere)..... %5.2f %%',100*count/cn));
                end
            end
                
            % check if the data is new or not
            if length(obj.cData) ~= nReadouts*nPhases*nSlices;
                obj.cData = zeros(obj.NAverages,nReadouts*nPhases*nSlices);
            end
            % check if the averaging is turned on
            if obj.averaging
                Nav = obj.NAverages;
            else
                Nav = 1;
            end
            index = mod(iAcq-1,Nav)+1;

            % auto phasing
            if obj.autoPhasing
                pt = max(Data3D(:));
                Data3D = Data3D.*exp(complex(0,-angle(pt)));
            end
            obj.cData(index,1:length(Data3D(:))) = Data3D(:);
            obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
            obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
            obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
            acq_time = obj.GetParameter('ACQUISITIONTIME');
            obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*acq_time/nPhases;
            obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time/nPhases;
        
            if nBatches>1
                delete(h_waitbar);
            end
            
%             save('TempData3D.mat','Data3D','INDstore','obj');
        end
 
    
    end
end

