classdef RaNSSE_V4 < MRI_BlankSequence
    %% 
    
    % RANSSE CODE
    
    %*******************************************************
    %** J. M. Algar�n                                     **
    %** CSIC/UPV                                          **
    %** I3M                                               **
    %** Avda. dels Tarongers, 12,46022, Valencia, (Spain) **
    %** Tel: +34 960 728 111                              **
    %** email: josalggui@i3m.upv.es                       **
    %*******************************************************
    
    %%
    properties
        PhasesPerBatch = 20;
        nPhasesBatch;
        Axis1Vector;
        Axis2Vector;
        Axis3Vector;
        StartSlice;
        lastBatch;
        EndSlice;
        StartPhase;
        EndPhase;
        Stage;              % ZTE -> 1, Single Point ->2 or Prescan -> 0
        AcquisitionTime;
        slice_ampl;
        nSteps = 10;
        readoutAmplitudeP = 0;
        phaseAmplitudeP = 0;
        sliceAmplitudeP = 0;
        repetitionDelay;
    end
    methods (Access=private)
        function CreateParameters(obj)

            % HELP %
            % I include here new inputs to discriminate between
            % acquistition and regruidding.
            % -> NPHASES, NREADOUTS and NSLICES are for regridding
            % -> NPOINTS, NCIRCUNFERENCES, NLINESPERCIRCUNFERENCES are for
            %    aqcuisition.
            CreateOneParameter(obj,'NREADOUTS','Nx','',50)
            CreateOneParameter(obj,'NPHASES','Ny','',50)
            CreateOneParameter(obj,'NSLICES','Nz','',50)
            CreateOneParameter(obj,'FOVX','FOV x','mm',20*MyUnits.mm)
            CreateOneParameter(obj,'FOVY','FOV y','mm',20*MyUnits.mm)
            CreateOneParameter(obj,'FOVZ','FOV z','mm',20*MyUnits.mm)
            CreateOneParameter(obj,'ACQTIME','Acquisition time','us',500*MyUnits.us)
            CreateOneParameter(obj,'REPETITIONTIME','TR','ms',500*MyUnits.ms)
            CreateOneParameter(obj,'UNDERSAMPLING','Undersampling','',1);
            CreateOneParameter(obj,'JUMPINGFACTOR','Jumping factor','',0);
            CreateOneParameter(obj,'REPETITIONDELAY','Repetition Delay','ms',0.1);
            CreateOneParameter(obj,'DELTAX','X-axis deviation','mm',0);
            CreateOneParameter(obj,'DELTAY','Y-axis deviation','mm',0);
            CreateOneParameter(obj,'DELTAZ','Z-axis deviation','mm',0);
            CreateOneParameter(obj,'ECHOTIME1','TE1','ms',1*MyUnits.ms);
            CreateOneParameter(obj,'PULSETIME','90� pulse time','us',10*MyUnits.us);
            CreateOneParameter(obj,'PULSETIME180','180� pulse time','us',20*MyUnits.us);
            CreateOneParameter(obj,'ECHOTIME2','TE2','ms',20*MyUnits.ms);
            
            
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
                'NPOINTS','SPECTRALWIDTH','SPOILERAMP','REPETITIONDELAY',...
                'SPOILERTIME','BLANKINGDELAY','TRANSIENTTIME'};
            
            % Reorganizes InputParameters
            obj.MoveParameter(6,20);
            obj.MoveParameter(24,12);
            obj.MoveParameter(25,13);
            obj.MoveParameter(19,15);
            obj.MoveParameter(16,17);
            obj.MoveParameter(29,26);
            obj.MoveParameter(30,27);
            obj.MoveParameter(31,28);
            obj.MoveParameter(32,14);
            obj.MoveParameter(33,10);
            obj.MoveParameter(34,16);
            
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = RaNSSE_V4(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'RaNSSE V4';
            obj.ProgramName = 'MRI_BlankSequence.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
            obj.SetParameter('BLANKINGDELAY',5*MyUnits.us);
            obj.SetParameter('COILRISETIME',100*MyUnits.us);
            obj.SetParameter('PULSETIME',10*MyUnits.us);
        end
        function UpdateTETR(obj)
            
            global rawData;
            rawData = [];
            
            obj.TE = 0;
            obj.TR = obj.GetParameter('REPETITIONTIME');
            
            % Get cartesian parameters
            obj.GetCartesianParameters;
            
            % Get sequence parameters
            obj.GetSequenceParameters;
                        
            % Set number of phases, points and slices as well as amplitudes
            obj.SetSamplingParameters;

            % Calculate the gradients list for radial sampling
            obj.GetRadialGradients;
            
            % Calculate k-points matrix for radial sampling
            obj.GetRadialKPoints
                        
            nRepetitions = rawData.aux.numberOfRadialRepetitions;
            
            obj.TTotal = obj.TR * obj.GetParameter('NSCANS')*nRepetitions;
        end
        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence
            %         "1", prescan for ring down calibration
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR] = SelectAxes(obj);
            
            % Set the number of lines to be read
            nMisses = obj.GetParameter('JUMPINGFACTOR');
            
            if(mode==1)
                [~,nPhases,~,~,~,~,~] = obj.ConfigureGradients(mode);
                obj.Axis1Vector = 0;
                obj.Axis2Vector = 0;
                obj.Axis3Vector = 0;
            elseif(mode==0)
                [~,~,~,~,~,~,~] = obj.ConfigureGradients(mode);
                nPhases = obj.nPhasesBatch+nMisses;
            end
            
            % Calculate the number of line reads
            obj.nline_reads = 2*(nPhases-nMisses);
            
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions= (3*obj.nSteps+13)*nPhases;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            bw = obj.GetParameter('SPECTRALWIDTH');
            acqTime = obj.GetParameter('NPOINTS')/bw*1e6;
            crt = obj.GetParameter('COILRISETIME')*1e6;
            blk = obj.GetParameter('BLANKINGDELAY')*1e6;
            grad_delay = 300;   %time for gradients to rise up + blk
            iIns = 1;
            pulseTime90 = obj.GetParameter('PULSETIME')*1e6;
            pulseTime180 = obj.GetParameter('PULSETIME180')*1e6;
            TE1 = obj.GetParameter('ECHOTIME1')*1e6;
            TE2 = obj.GetParameter('ECHOTIME2')*1e6;
            te2 = TE2-TE1;
            repDelayTime = obj.GetParameter('REPETITIONDELAY')*1e6;
            maxGrad = max([max(obj.Axis1Vector),max(obj.Axis2Vector),max(obj.Axis3Vector)]);
            for phasei = 1:nPhases
                if(phasei<=nMisses)
                    readoutAmplitude = 0;
                    phaseAmplitude = 0;
                    sliceAmplitude = 0;
                else
                    readoutAmplitude = obj.Axis1Vector(phasei-nMisses);
                    phaseAmplitude = obj.Axis2Vector(phasei-nMisses);
                    sliceAmplitude = obj.Axis3Vector(phasei-nMisses);
                end
                
                %% Activate the total gradient (3 instrucctions per step)
                for ii=1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimR+obj.readoutAmplitudeP+(readoutAmplitude-obj.readoutAmplitudeP)/obj.nSteps*ii,RR,0.1);
                    iIns = obj.PulseGradient(iIns,shimP+obj.phaseAmplitudeP+(phaseAmplitude-obj.phaseAmplitudeP)/obj.nSteps*ii,PP,0.1);
                    iIns = obj.PulseGradient(iIns,shimS+obj.sliceAmplitudeP+(sliceAmplitude-obj.sliceAmplitudeP)/obj.nSteps*ii,SS,crt/obj.nSteps-0.2);
                end
                obj.readoutAmplitudeP = readoutAmplitude;
                obj.phaseAmplitudeP = phaseAmplitude;
                obj.sliceAmplitudeP = sliceAmplitude;
                obj.DELAYs(iIns-1) = crt/obj.nSteps-0.2+grad_delay-blk;
                
                %% 90� RF pulse (3 instructions)
                iIns = obj.CreatePulse(iIns,0,blk,pulseTime90,TE1/2-1/2*pulseTime90-1/2*pulseTime180-blk);
                if(obj.Stage==0)
                    obj.TXs(iIns-2) = 0;
                end
                obj.PHASEs(iIns-2) = 0;
                                
                %% First 180� RF pulse (3 instruction)
                iIns = obj.CreatePulse(iIns,0,blk,pulseTime180,TE1/2-1/2*pulseTime180);
                if(obj.Stage==0)
                    obj.TXs(iIns-2) = 0;
                end
                obj.PHASEs(iIns-2) = 1;
                
                %% Acquisition and delay to second 180� RF pulse (2 instructions)
                if(phasei<=nMisses)
                    iIns = obj.RepetitionDelay(iIns,0,acqTime);
                    iIns = obj.RepetitionDelay(iIns,0,0.5*te2-blk-acqTime-0.5*pulseTime180);
                else
                    iIns = obj.Acquisition(iIns,acqTime);
                    iIns = obj.RepetitionDelay(iIns,0,0.5*te2-blk-acqTime-0.5*pulseTime180);
                end
                
                %% Second 180� RF pulse (3 instructions)
                iIns = obj.CreatePulse(iIns,0,blk,pulseTime180,te2/2-pulseTime180/2);
                if(obj.Stage==0)
                    obj.TXs(iIns-2) = 0;
                end
                obj.PHASEs(iIns-2) = 1;
                
                %% Acquisition (1 instructions)
                if(phasei<=nMisses)
                    iIns = obj.RepetitionDelay(iIns,0,acqTime);
                else
                    iIns = obj.Acquisition(iIns,acqTime);
                end
                
                %% Repetition Delay (1 instruction)    
                if(obj.lastBatch==1 && phasei==nPhases)
                    iIns = obj.RepetitionDelay(iIns,1,repDelayTime);
                    disp('Last Batch!')
                elseif(maxGrad>0.25)
                    iIns = obj.RepetitionDelay(iIns,1,repDelayTime);
                else
                    iIns = obj.RepetitionDelay(iIns,0,repDelayTime);
                end
                if(obj.Stage~=0)
                    obj.DELAYs(iIns-1)   = repDelayTime; %time in us
                else
                    obj.DELAYs(iIns-1)   = acqTime;
                end 

            end
        end
        function [status,result] = Run(obj,iAcq)
            tic;
            clc
            
            [~,~,~,~,~,spoilerEnable,~] = obj.ConfigureGradients(0);
            
            % Load amplitude calibration for 1V
            bwCal = load('rawData-BW-Calibration.mat');
            bwCalVal = bwCal.rawData.outputs.fidMean;
            bwCalVec = logspace(log10(bwCal.rawData.inputs.bwMin),...
                log10(bwCal.rawData.inputs.bwMax),...
                bwCal.rawData.inputs.steps);
                                   
            %% Constant to relate k with k'
            % HELP: k = gammabar*G*t
            % G = c*A where A is the gradient amplitude voltage from
            % radioprocessor. Then, k' = k/(gammabar*c) = A*t.
            % We still need to calibrate constant c. We also need to take
            % into account that c is different for each axis. 
            c = obj.ReorganizeCalibrationData();
            global rawData              % Raw Data contains output info
            rawData = [];
            rawData.inputs.SeqName = obj.SequenceName;
            
            % Get cartesian parameters
            obj.GetCartesianParameters;
            
            % Get sequence parameters
            obj.GetSequenceParameters;
            
            % Set number of phases, points and slices as well as amplitudes
            obj.SetSamplingParameters;

            % Calculate the gradients list for radial sampling
            obj.GetRadialGradients;
            gradientVectors = rawData.aux.gradientsRadialTeslasPerMeter;
            
            % Calculate k-points matrix for radial sampling
            obj.GetRadialKPoints;
            kSpaceValues = rawData.aux.radialKPoints;
            
            % Calculate k-points in the cartesian gridd
            obj.GetCartesianKPoints;
            
            % Normalize gradients to arbitray units.
            gradientVectors = gradientVectors/diag(c);
            if(sum(abs(gradientVectors(:))>=1)>=1 || sum(isnan(gradientVectors(:)))>=1 || sum(isinf(gradientVectors(:)))>=1)
                warndlg('At least one non valid DAC amplitude in gradients!','Gradient Amplitude Error')
                return;
            end
            
            %% Auxiliar
            DataShort = [];
            DataLong = [];
            nPPL = rawData.aux.numberOfPointsPerLine;
            nLPC = rawData.aux.linesPerCircumference;
            nMisses = rawData.inputs.jumpingFactor;
            nPoints = rawData.inputs.nPoints;
            nCir = rawData.aux.numberOfCircumferences;
            acqTime = rawData.aux.acquisitionTime;
            nRepetitions = rawData.aux.numberOfRadialRepetitions;
            bw = rawData.aux.bandwidth;
            
            %% Radial
            % Create waiting bar
            obj.Stage = 1;
            nFreq = 0;
            
            % Get value to normalize fid to Volts
            [~,bwCalNormPos] = min(abs(bwCalVec-bw));
            bwCalNormVal = bwCalVal(bwCalNormPos);
            
            % Repetitions per batch and number of repetitions
            obj.PhasesPerBatch = floor(min([2048/(3*obj.nSteps+13),16000/nPPL]))-nMisses;                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
            nPhaseBatches = ceil(nRepetitions/obj.PhasesPerBatch);
            
            % Set parameters for current block
            obj.AcquisitionTime = acqTime;
            obj.SetParameter('SPECTRALWIDTH',bw);
            obj.SetParameter('NPOINTS',nPPL);
            
            % Start batches sweep
            tRadial = 0;
            rawData.inputs.rfFrequency = zeros(nPhaseBatches,1);
            obj.readoutAmplitudeP = 0;
            obj.phaseAmplitudeP = 0;
            obj.sliceAmplitudeP = 0;
            for iBatch = 1:nPhaseBatches
                tA = clock;
                if(iBatch==1)
                    fprintf('Batch %1.0f/%1.0f, estiating remaining time \n',iBatch,nPhaseBatches);
                else
                    tC = tC*(nPhaseBatches-iBatch+1);
                    fprintf('Batch %1.0f/%1.0f, remaining time: %1.0f s \n',iBatch,nPhaseBatches,tC);
                end
                
                % Get central frequency
                if(spoilerEnable)
                    fprintf('Calibrating frequency... \n')
                    f0 = obj.GetFIDParameters;
                    obj.SetParameter('FREQUENCY',f0);
                    nFreq = nFreq+1;
                    rawData.aux.freqListRadial(nFreq) = f0;
                    fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
                end
                
                % Get the appropiate axis
                if(iBatch<nPhaseBatches)
                    obj.Axis1Vector = gradientVectors((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,1);
                    obj.Axis2Vector = gradientVectors((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,2);
                    obj.Axis3Vector = gradientVectors((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,3);
                    obj.lastBatch = 0;
                else
                    obj.Axis1Vector = gradientVectors((iBatch-1)*obj.PhasesPerBatch+1:nRepetitions,1);
                    obj.Axis2Vector = gradientVectors((iBatch-1)*obj.PhasesPerBatch+1:nRepetitions,2);
                    obj.Axis3Vector = gradientVectors((iBatch-1)*obj.PhasesPerBatch+1:nRepetitions,3);
                    obj.lastBatch = 0;
                end
                obj.nPhasesBatch = length(obj.Axis1Vector);
                
                % NOW RUN ACTUAL ACQUISTION
                obj.CreateSequence(0);
                if(sum(obj.DELAYs<0.1)~=0)
                    warndlg('At least one delay is negative!','Delay Error')
                    return;
                end
                obj.Sequence2String;
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                t1 = clock;
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                t2 = clock;
                tRadial = tRadial + etime(t2,t1);
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Radial batch aborted!','Warning')
                    return;
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = reshape(complex(DataBatch(:,1),DataBatch(:,2))/bwCalNormVal,nPPL,obj.nPhasesBatch*2);
                DataShort = cat(1,DataShort,reshape(cDataBatch(:,1:2:end-1),nPPL*obj.nPhasesBatch,1));
                DataLong = cat(1,DataLong,reshape(cDataBatch(:,2:2:end),nPPL*obj.nPhasesBatch,1));
                tB = clock;
                tC = etime(tB,tA);
            end
            kSpaceValuesShort = [kSpaceValues,DataShort(:)];
            kSpaceValuesLong = [kSpaceValues,DataLong(:)];
            rawData.kSpace.sampledShort = kSpaceValuesShort;
            rawData.kSpace.sampledLong = kSpaceValuesLong;
            
            %% Regridding
            kCartesian = rawData.aux.kCartesian;
            if(nCir>1)
                valCartesianShort = griddata(kSpaceValuesShort(:,1),kSpaceValuesShort(:,2),kSpaceValuesShort(:,3),kSpaceValuesShort(:,4),kCartesian(:,1),kCartesian(:,2),kCartesian(:,3));
                valCartesianLong = griddata(kSpaceValuesLong(:,1),kSpaceValuesLong(:,2),kSpaceValuesLong(:,3),kSpaceValuesLong(:,4),kCartesian(:,1),kCartesian(:,2),kCartesian(:,3));
            elseif(nCir==1 && nLPC>2)
                valCartesianShort = griddata(kSpaceValuesShort(:,1),kSpaceValuesShort(:,2),kSpaceValuesShort(:,4),kCartesian(:,1),kCartesian(:,2));
                valCartesianLong = griddata(kSpaceValuesLong(:,1),kSpaceValuesLong(:,2),kSpaceValuesLong(:,4),kCartesian(:,1),kCartesian(:,2));
            elseif(nCir==1 && nLPC==2)
                valCartesianShort = pchip(kSpaceValuesShort(:,1),kSpaceValuesShort(:,4),kCartesian(:,1));
                valCartesianLong = pchip(kSpaceValuesLong(:,1),kSpaceValuesLong(:,4),kCartesian(:,1));
            end
            valCartesianShort(isnan(valCartesianShort)) = 0;
            valCartesianLong(isnan(valCartesianLong)) = 0;
            
            % Add phase according to fovDeviation
            DELX = rawData.inputs.fovDeviation(1);
            DELY = rawData.inputs.fovDeviation(2);
            DELZ = rawData.inputs.fovDeviation(3);
            phase = exp(-2*pi*1i*(DELX*kCartesian(:,1)+DELY*kCartesian(:,2)+DELZ*kCartesian(:,3)));
            
            % Save interpolated data
            interpolatedShort = [kCartesian,valCartesianShort(:).*phase];
            interpolatedLong = [kCartesian,valCartesianLong(:).*phase];
            rawData.kSpace.interpolatedShort = interpolatedShort;
            rawData.kSpace.interpolatedLong = interpolatedLong;
            interpolatedShort = reshape(interpolatedShort(:,4),nPoints);
            interpolatedLong = reshape(interpolatedLong(:,4),nPoints);
            
            imagenShort = abs(ifftshift(ifftn(interpolatedShort)));
            imagenLong = abs(ifftshift(ifftn(interpolatedLong)));
            
            rawData.kSpace.imagenShort = imagenShort;
            rawData.kSpace.imagenLong = imagenLong;
            
            %% Save data
            % Short
            rawData.kSpace.sampled = kSpaceValuesShort;
            rawData.kSpace.imagen = imagenShort;
            time = clock;
            name = strcat('rawDataShort-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
            
            % Long
            rawData.kSpace.sampled = kSpaceValuesLong;
            rawData.kSpace.imagen = imagenLong;
            name = strcat('rawDataLong-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
            
        end

        
%**************************************************************************
%**************************************************************************
%**************************************************************************


        function GetCartesianParameters(obj)
            global rawData;
            
            nPoints = ...
                [obj.GetParameter('NREADOUTS'),...
                obj.GetParameter('NPHASES'),...
                obj.GetParameter('NSLICES')];
            fov     = ...
                [obj.GetParameter('FOVX'),...
                obj.GetParameter('FOVY'),...
                obj.GetParameter('FOVZ')];
            deltaK  = 1./fov;
            kMax    = nPoints./(2*fov);
            DELX    = obj.GetParameter('DELTAX');
            DELY    = obj.GetParameter('DELTAY');
            DELZ    = obj.GetParameter('DELTAZ');
            
            rawData.inputs.NEX          = obj.GetParameter('NSCANS');
            rawData.inputs.nPoints      = nPoints;
            rawData.inputs.fov          = fov;
            rawData.inputs.fovDeviation = [DELX,DELY,DELZ];
            rawData.aux.deltaK          = deltaK;
            rawData.aux.kMax            = kMax;
        end
        function GetSequenceParameters(obj)
            
            global rawData;
            
            coilRiseTime = obj.GetParameter('COILRISETIME');
            blankingTime = obj.GetParameter('BLANKINGDELAY');
            trf90 = obj.GetParameter('PULSETIME');                          % RF pulse time
            trf180 = obj.GetParameter('PULSETIME180');
            TE1 = obj.GetParameter('ECHOTIME1');                            % TE for first echo
            TE2 = obj.GetParameter('ECHOTIME2');                            % TE for second echo
            acqTime = obj.GetParameter('ACQTIME');                          % Max time for acquisition, it should be the shortest T2 to be acquired
            underSampling = obj.GetParameter('UNDERSAMPLING');              % Undersampling for azimutal acquisition.
            underSampling = sqrt(underSampling);
            TR = obj.GetParameter('REPETITIONTIME');
            gradDelay = 300*1e-6; %s for Gradients to rise up + blk
            obj.repetitionDelay = TR-coilRiseTime-blankingTime-gradDelay-0.5*trf90-TE2-acqTime;
            obj.SetParameter('REPETITIONDELAY',obj.repetitionDelay);
            nMisses = obj.GetParameter('JUMPINGFACTOR');
           
            rawData.inputs.pulseTime90 = trf90;
            rawData.inputs.pulseTime180 = trf180;
            rawData.inputs.TE1 = TE1;
            rawData.inputs.TE2 = TE2;
            rawData.inputs.acqTime = acqTime;
            rawData.inputs.coilRiseTime = coilRiseTime;
            rawData.inputs.gradDelay = gradDelay;
            rawData.inputs.blankingTime = blankingTime;
            rawData.aux.delay = obj.repetitionDelay;
            rawData.inputs.undersampling = underSampling;
            rawData.inputs.jumpingFactor = nMisses;
            rawData.inputs.rfAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.rfFrequency = obj.GetParameter('FREQUENCY');
            rawData.inputs.repetitionTime = TR;
            rawData.inputs.axes = [obj.GetParameter('READOUT'),...
                obj.GetParameter('PHASE'),obj.GetParameter('SLICE')];
        end
        function SetSamplingParameters(obj)
            % gradientAmplitude = normalized amplitude to the MRIGUI input
            % nLPC = number of lines per circunferece in k-space
            % nCir = number of circunfereces in gradient space
            % nPPL = number of acquired points per line in k-space
            
            % Load rawData
            global rawData;
            gammabar        = 42.6d6;
            acqTime         = rawData.inputs.acqTime;
            kMax            = rawData.aux.kMax;
            nPoints         = rawData.inputs.nPoints;
            underSampling   = rawData.inputs.undersampling;
            
            % Set first block parameters
            bw                          = max(nPoints)/(2*acqTime);                             % ideal acquisition bandwidth
            kLimits                     = kMax;                                             % k limits for each radial block
            gradientAmplitudes          = kLimits./(gammabar*acqTime).*~(nPoints==1);  % gradient amplitudes for each radial block (T/m)
            nPPL                        = ceil(sqrt(3)*acqTime*bw)+1;                    % number of points per line
            nLPC                        = ceil(max(nPoints(1:2))*pi/underSampling);         % max number of lines per circumference
            nLPC                        = max(nLPC-mod(nLPC,2),1);
            nCir                        = max(ceil(nPoints(3)*pi/2/underSampling)+1,1);     % number of circumferences
            if(nPoints(3)==1)
                nCir = 1;
            end
            if(nPoints(2)==1)
                nLPC = 2;
            end
            
            % Get acquisition time
            acqTime = nPPL./bw;
            
            % Save data in rawData
            rawData.aux.bandwidth               = bw;
            rawData.aux.kLimits                 = kLimits;
            rawData.aux.acquisitionTime         = acqTime;
            rawData.aux.numberOfPointsPerLine   = nPPL;
            rawData.aux.linesPerCircumference   = nLPC;
            rawData.aux.numberOfCircumferences  = nCir;
            rawData.aux.gradientAmplitudes      = gradientAmplitudes;
        end
        function GetRadialGradients(obj)
            % Gradient are defined by the sphere parametric equation. The
            % two parameteres are: theta in [0,pi] and phi [0,2pi].
            % Gradients are normalized to the amplifier amplitude.
            % First, I calculate the number of repetitions:
            
            % Load rawData
            global rawData;
            nCir                = rawData.aux.numberOfCircumferences;
            nLPC                = rawData.aux.linesPerCircumference;
            gradientAmplitudes  = rawData.aux.gradientAmplitudes;
            
            % Get number of radial repetitions
            nRepetitions = 0;
            % Get theta vector for current block
            if(nCir==1)
                theta = pi/2;
            else
                theta = linspace(0,pi,nCir);
            end
            
            % Get number of radial repetitions for current block;
            for jj = 1:nCir
                nRepetitions = nRepetitions+max(ceil(nLPC*sin(theta(jj))),1);
            end
            
            % Calculate radial gradients
            normalizedGradientsRadial = zeros(sum(nRepetitions),3);
            n = 0;
            % Get theta vector for current block
            if(nCir==1)
                theta = pi/2;
            else
                theta = linspace(0,pi,nCir);
            end
            
            % Calculate the normalized gradients:
            for jj = 1:nCir
                nLPCjj = max(ceil(nLPC*sin(theta(jj))),1);
                deltaPhi = 2*pi/nLPCjj;
                phi = linspace(0,2*pi-deltaPhi,nLPCjj);
                for kk = 1:nLPCjj
                    n = n+1;
                    normalizedGradientsRadial(n,1) = sin(theta(jj))*cos(phi(kk));
                    normalizedGradientsRadial(n,2) = sin(theta(jj))*sin(phi(kk));
                    normalizedGradientsRadial(n,3) = cos(theta(jj));
                end
            end
            
            % Set gradients to T/m
            gradientVectors1 = ...
                normalizedGradientsRadial*diag(gradientAmplitudes);
            
            % Save to rawData
            rawData.aux.numberOfRadialRepetitions = nRepetitions;
            rawData.aux.normalizedGradientsRadial = normalizedGradientsRadial;
            rawData.aux.gradientsRadialTeslasPerMeter = gradientVectors1;
        end
        function GetRadialKPoints(obj)
            
            %Load rawData
            global rawData;
            gammabar = 42.6d6;
            nPPL                        = rawData.aux.numberOfPointsPerLine;
            bw                          = rawData.aux.bandwidth;
            normalizedGradientsRadial   = rawData.aux.normalizedGradientsRadial;
            nRepetitions                = rawData.aux.numberOfRadialRepetitions;
            gradientAmplitudes          = rawData.aux.gradientAmplitudes;
            
            % Calculate the radial k points for each block
            kRadial = [];
            % Calculate k-points at t = 0.5*trf+td
            normalizedKRadial = zeros(nRepetitions,3,nPPL);
            normalizedKRadial(:,:,1) = normalizedGradientsRadial/(2*bw);
            % Calculate all k-points
            for jj=2:nPPL
                normalizedKRadial(:,:,jj) = normalizedKRadial(:,:,1)+(jj-1)*normalizedGradientsRadial/bw;
            end
            normalizedKRadial = reshape(permute(normalizedKRadial,[3,1,2]),[nRepetitions*nPPL,3]);
            kRadial = cat(1,kRadial,normalizedKRadial*diag(gammabar*gradientAmplitudes));
            % Save to rawData
            rawData.aux.radialKPoints = kRadial;
        end
        
        
        %******************************************************************
        %******************************************************************
        %******************************************************************
        
        
        function GetCartesianKPoints(obj)
            
            % Load rawData
            global rawData;
            nPoints = rawData.inputs.nPoints;
            kMax = rawData.aux.kMax;
            
            % Get the full cartesian points
            kx = linspace(-kMax(1)*~isequal(nPoints(1),1),kMax(1)*~isequal(nPoints(1),1),nPoints(1));
            ky = linspace(-kMax(2)*~isequal(nPoints(2),1),kMax(2)*~isequal(nPoints(2),1),nPoints(2));
            kz = linspace(-kMax(3)*~isequal(nPoints(3),1),kMax(3)*~isequal(nPoints(3),1),nPoints(3));
            [kx,ky,kz] = meshgrid(kx,ky,kz);
            kx = permute(kx,[2,1,3]);
            ky = permute(ky,[2,1,3]);
            kz = permute(kz,[2,1,3]);
            kCartesian(:,1) = kx(:);
            kCartesian(:,2) = ky(:);
            kCartesian(:,3) = kz(:);
            
            % Save to rawData
            rawData.aux.kCartesian = kCartesian;
        end
    end
    
end

