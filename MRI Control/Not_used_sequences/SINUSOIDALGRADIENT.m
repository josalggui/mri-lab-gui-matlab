classdef SINUSOIDALGRADIENT < MRI_BlankSequence_JM    
    properties
        maxRepetitionsPerBatch;
        nRepetitionsInBatch;
        gradient1Vector;
        lastBatch;
        stage;
        RepetitionTime;
    end
    
    methods (Access=private)
        
        function CreateParameters(obj) 
            CreateOneParameter(obj,'PERIODE','Periode gradient','us',1000*MyUnits.us)
            CreateOneParameter(obj,'NREPETITIONS','Number of periodes','',10)
            CreateOneParameter(obj,'GRADAMPLITUDE','Gradient amplitude','',0.1)
            CreateOneParameter(obj,'POINTS','Points in one periode','',20)
            
            obj.InputParHidden = {'NSCANS','SPECTRALWIDTH','FREQUENCY','RFAMPLITUDE','PULSETIME','COILRISETIME','TRANSIENTTIME','NPOINTS','BLANKINGDELAY','SHIMSLICE','SHIMPHASE','SHIMREADOUT','SPECTRALWIDTH','PHASE','SLICE','NSLICES'};
        end
    end
    
            
    methods
        function obj = SINUSOIDALGRADIENT(program)
            obj = obj@MRI_BlankSequence_JM();
            obj.SequenceName = 'SINUSOIDAL GRADIENT';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            
            if nargin > 0
                obj.ProgramName = program;
            end
            
            obj.CreateParameters();
            obj.SetDefaultParameters();        
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence_JM(obj);
        end
        
        function CreateSequence(obj,mode)

            c = [12, 12, 7.5];              % T/m/V
            
            if (obj.GetParameter('READOUT') == 'x')
                c = c(1);
                dac = 2;
            elseif (obj.GetParameter('READOUT') == 'y')
                c = c(2);
                dac = 0;
            elseif (obj.GetParameter('READOUT') == 'z')
                c = c(3);
                dac = 1;
            end
            
            nRepetitions = obj.GetParameter('NREPETITIONS');
            points = obj.GetParameter('POINTS');
            periode = obj.GetParameter('PERIODE')*1e6;
            gradamp = obj.GetParameter('GRADAMPLITUDE');
            t = linspace(0,periode,points);
            deltatT = periode/points;
            gradient = gradamp*sin(2*pi*t/periode);

            
            obj.nline_reads = nRepetitions;
            
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions = 1*(nRepetitions)*points;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            iIns = 1;          
            
            for nRep = 1:nRepetitions
                for nPoint=1:points
                %% Activate readout gradient (1 instrucction)
                igradient = gradient(nPoint);
                iIns = PulseGradient(obj,iIns,igradient,dac,deltatT);
                end

           end             
        end
             
        function [status,result] = Run(obj,iAcq)
            tic;
            clc;
            %% Constant to relate k with k'
            global rawData;             % Raw Data contains output info
            
            c = [12, 12, 7.5];              % T/m/V
            
            if (obj.GetParameter('READOUT') == 'x')
                c = c(1);
            elseif (obj.GetParameter('READOUT') == 'y')
                c = c(2);
            elseif (obj.GetParameter('READOUT') == 'z')
                c = c(3);
            end
            
            %% Set number of points as well as gradient amplitudes for radial sampling

             
       
            %% Radial
            obj.stage = 1;
            obj.maxRepetitionsPerBatch = floor(1024/1);                  
            nBatches = 1;                                             
            rawData.aux.numberOfBatches = nBatches;
            
            nRepetitions = obj.GetParameter('NREPETITIONS');
            points = obj.GetParameter('POINTS');
            periode = obj.GetParameter('PERIODE');
            gradamp = obj.GetParameter('GRADAMPLITUDE');
            
            t = linspace(0,periode,points);
            deltatT = periode/points;
            gradient = gradamp*sin(2*pi*t/periode)/c;

            % Start batches sweep
            tRadial = 0;
            rawData.inputs.rfFrequencyRadial = zeros(nBatches,1);
            
            for iBatch = 1:nBatches
                disp('New batch');
                obj.gradient1Vector = gradient;
                obj.lastBatch = 0;
                obj.nRepetitionsInBatch = length(obj.gradient1Vector);

                % NOW RUN ACTUAL ACQUISTION
                [cDataBatch,tRadial,status,result] = RunData(obj,0,tRadial);

            end

            save('rawData.mat','rawData');
            
           
            %% Save rawData
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
            
        end
            
    end
    
end