
%SUBIDA Y BAJADA DE LOS TRES GRADIENTES A LA VEZ (USADO PARA CARACTERIZACI�N T�RMICA DE DANI)
%PRECISO 10 REPETICIONES Y 5 STEPS tener ob.nInstructions=550
%PRECISO 10 REPETICIONES Y 50 STEPS tener ob.nInstructions=5950

% classdef IECO_TEST2 < MRI_BlankSequence6   
%     properties
%         maxRepetitionsPerBatch;
%         nPhasesBatch;
%         lastBatch;
%         RepetitionTime;
%         AcquisitionTime;
%         gradient;
%         time;
%         grad;
%         time1;
%         grad1;
%         time2;
%         grad2;
%         time3;
%         grad3;
%         time4;
%         grad4;
%         
%         
%     end
%     methods (Access=private)
%         function CreateParameters(obj)
%             CreateOneParameter(obj,'NREPETITIONS','Periode repetitions','',10)
%             CreateOneParameter(obj,'RISETIME','Rise time','us',500*MyUnits.us)
%             CreateOneParameter(obj,'DUTYCICLE','Duty cicle','',50)
%             CreateOneParameter(obj,'GRADMAX','+ Amplitude','A',1)
%             CreateOneParameter(obj,'GRADMIN','- Amplitude','A',1)
%             CreateOneParameter(obj,'STEPS','Number of steps','',5)
%             CreateOneParameter(obj,'FLATTOP','Flat top time','ms',100*MyUnits.ms)
%             
%             
%             obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
%                 'NPOINTS','NSLICES','FREQUENCY','SPECTRALWIDTH',...
%                 'RFAMPLITUDE','PULSETIME','BLANKINGDELAY','TRANSIENTTIME','COILRISETIME',...
%                 }; 
%         end  
%     end
%     
%     
% 
%     methods
%         function obj = IECO_TEST2(program)
%             obj = obj@MRI_BlankSequence6();
%             obj.SequenceName = 'IECO_TEST2';
%             obj.ProgramName = 'MRI_BlankSequenceJM.exe';
%             if nargin > 0
%                 obj.ProgramName = program;
%             end
%             obj.CreateParameters();
%             obj.SetDefaultParameters();
%         end
%         
%         function SetDefaultParameters(obj)
%             SetDefaultParameters@MRI_BlankSequence6(obj);
%         end
%         function UpdateTETR(obj)
%             obj.TTotal = (100/obj.GetParameter('DUTYCICLE'))*obj.GetParameter('NSCANS') * obj.GetParameter('NREPETITIONS') *( 4*obj.GetParameter('RISETIME')+2*obj.GetParameter('FLATTOP'));
%         end
%          
%         function CreateSequence(obj)
%  
%             nRepetitions = obj.GetParameter('NREPETITIONS');
%             obj.nline_reads = nRepetitions;
% 
%             steps = obj.GetParameter('STEPS')+1;
%             obj.nInstructions = ((4*(steps-2)+3)*(nRepetitions))*3-nRepetitions;
%             obj.nInstructions = 550;
%             [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
% 
%             dutycicle = obj.GetParameter('DUTYCICLE');
%             risetime = obj.GetParameter('RISETIME')*1e6;
%             flattoptime = obj.GetParameter('FLATTOP')*1e6; 
%             periode = (100*(4*risetime+2*flattoptime))/dutycicle;
%             delay = periode-(4*risetime+2*flattoptime);
%             
%             
%             switch num2str(obj.GetParameter('READOUT'))
%                 case 'x'
%                     obj.selectReadout = 2;
%                     obj.selectPhase = 0;
%                     obj.selectSlice = 1;
%                 case 'y'
%                     obj.selectReadout = 0;
%                 case'z'
%                     obj.selectReadout = 1;
%                 otherwise 
%                     obj.selectReadout = 2;
%             end
%             
%             
%             selectReadout = obj.selectReadout;
%             selectPhase = obj.selectPhase;
%             selectSlice = obj.selectSlice;
%             steeptime = (obj.time1(3) - obj.time1(2));
% 
%             obj.grad1=obj.grad1*0.0925/50;
%             obj.grad2=obj.grad2*0.0925/50;
%             obj.grad3=obj.grad3*0.0925/50;
%             obj.grad4=obj.grad4*0.0925/50;
%            
%             iIns = 1;    
%             
%             for i = 1:nRepetitions
%                 for j=2:(length(obj.grad1)-1)
%                 iIns = PulseGradient(obj,iIns,obj.grad1(j),selectReadout,0.1);
%                 iIns = PulseGradient(obj,iIns,obj.grad1(j),selectPhase,0.1);
%                 iIns = PulseGradient(obj,iIns,obj.grad1(j),selectSlice,steeptime);
%                 end
%                 
%                 iIns = PulseGradient(obj,iIns,obj.grad1(length(obj.grad1)),selectReadout,0.1);
%                 iIns = PulseGradient(obj,iIns,obj.grad1(length(obj.grad1)),selectPhase,0.1);
%                 iIns = PulseGradient(obj,iIns,obj.grad1(length(obj.grad1)),selectSlice,flattoptime);
%                 
%                 for j=2:(length(obj.grad2)-1)
%                 iIns = PulseGradient(obj,iIns,obj.grad2(j),selectReadout,0.1);
%                 iIns = PulseGradient(obj,iIns,obj.grad2(j),selectPhase,0.1);
%                 iIns = PulseGradient(obj,iIns,obj.grad2(j),selectSlice,steeptime);
%                 end
%                 
%                 for j=2:(length(obj.grad3)-1)
%                 iIns = PulseGradient(obj,iIns,obj.grad3(j),selectReadout,0.1);
%                 iIns = PulseGradient(obj,iIns,obj.grad3(j),selectPhase,0.1);
%                 iIns = PulseGradient(obj,iIns,obj.grad3(j),selectSlice,steeptime);
%                 end
%                 iIns = PulseGradient(obj,iIns,obj.grad3(length(obj.grad3)),selectReadout,0.1);
%                 iIns = PulseGradient(obj,iIns,obj.grad3(length(obj.grad3)),selectPhase,0.1);
%                 iIns = PulseGradient(obj,iIns,obj.grad3(length(obj.grad3)),selectSlice,flattoptime);
%                 
%                 for j=2:(length(obj.grad4)-1)
%                 iIns = PulseGradient(obj,iIns,obj.grad4(j),selectReadout,0.1);
%                 iIns = PulseGradient(obj,iIns,obj.grad4(j),selectPhase,0.1);
%                 iIns = PulseGradient(obj,iIns,obj.grad4(j),selectSlice,steeptime);
%                 end
% 
%                 
%                 iIns = RepetitionDelay (obj,iIns,1,delay);       
%             end 
%         end
%         
%         function [status,result] = Run(obj,iAcq)
%             clc
%             [~,~,~,~,~,~,~] = obj.ConfigureGradients(0);
% 
%             repetitions = obj.GetParameter('NREPETITIONS');
%             gradmax = obj.GetParameter('GRADMAX');
%             gradmin = obj.GetParameter('GRADMIN');
%             risetime = obj.GetParameter('RISETIME')*1e6;
%             steps = obj.GetParameter('STEPS')+1;
% 
%             
%             obj.time1 = linspace(0,risetime,steps-1);
%             obj.grad1 = linspace(0,gradmax,steps);
%             
%             obj.time2 = obj.time1+risetime;
%             obj.grad2 = fliplr(obj.grad1);
% 
%             obj.time3= obj.time1+2*risetime;
%             obj.grad3 = linspace(0,-gradmin,steps);
%             
%             obj.time4= obj.time1+3*risetime;
%             obj.grad4 = fliplr(obj.grad3);
% 
%             
%             global rawData;
%             rawData.inputs.sequence = obj.SequenceName;
%             rawData.inputs.numberrepetitions = repetitions;
%             rawData.inputs.gradamplidemax = gradmax;
%             rawData.inputs.gradamplidemin = gradmin;
%             rawData.inputs.readoutaxis = obj.selectReadout;
%            
% 
%             obj.nInstructions = 550;
%             obj.maxRepetitionsPerBatch = floor(1024/obj.nInstructions);                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
%             nBatches = 1;
%             t = 0;
%             
%             for iBatch = 1:nBatches
% 
%                 [cDataBatch,t,status,result] = RunData(obj,0,t);
% 
%                 obj.CreateSequence;
%                 obj.Sequence2String;            
%                 fname = 'TempBatch.bat';
%                 obj.WriteBatchFile(fname);
%                 t1 = clock;
%                 [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
%                 t2 = clock;
%                 t = t + etime(t2,t1);
%                 if status == 0 && ~isempty(result)
%                     C = textscan(result, '%s', 'delimiter', sprintf('\f'));
%                     res = C{:};
%                     obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
%                     obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
%                     obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
%                     obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
%                     obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
%                 else
%                     warndlg('Batch aborted!','Warning')
%                     return;
%                 end
% 
%             end
%                
%             if nBatches>1
%                 delete(h_waitbar);
%             end
% 
%         end
%     end
%     
% end








% SUBIDA Y BAJADA DE UN UNICO GRADIENTE CON DUTY CYCLE PARA TUNNING DE IECO (10/06/2019)
% 
classdef IECO_TEST2 < MRI_BlankSequence6   
    properties
        maxRepetitionsPerBatch;
        nPhasesBatch;
        lastBatch;
        RepetitionTime;
        AcquisitionTime;
        gradient;
        time;
        grad;
        time1;
        grad1;
        time2;
        grad2;
        time3;
        grad3;
        time4;
        grad4;   
    end
    
    methods (Access=private)
        function CreateParameters(obj)
            CreateOneParameter(obj,'NREPETITIONS','Periode repetitions','',10)
            CreateOneParameter(obj,'RISETIME','Rise time','us',500*MyUnits.us)
            CreateOneParameter(obj,'DUTYCICLE','Duty cicle','',50)
            CreateOneParameter(obj,'GRADAMPLITUDEMAX','+ Amplitude','',0.1)
            CreateOneParameter(obj,'STEPS','Number of steps','',5)
            CreateOneParameter(obj,'FLATTOP','Flat top time','ms',500*MyUnits.ms)
            
            
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
                'NPOINTS','NSLICES','FREQUENCY','SPECTRALWIDTH',...
                'RFAMPLITUDE','PULSETIME','BLANKINGDELAY','TRANSIENTTIME','COILRISETIME',...
                'PHASE','SLICE'}; 
        end  
    end
    
    methods
        function obj = IECO_TEST2(program)
            obj = obj@MRI_BlankSequence6();
            obj.SequenceName = 'IECO_TEST2';
            obj.ProgramName = 'MRI_BlankSequenceJM.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence6(obj);
            obj.SetParameter('NPOINTS',10);
        end
        
        function UpdateTETR(obj)
            obj.TTotal = (100/obj.GetParameter('DUTYCICLE'))*obj.GetParameter('NSCANS') * obj.GetParameter('NREPETITIONS') *( 4*obj.GetParameter('RISETIME')+obj.GetParameter('FLATTOP'));
        end
         
        function CreateSequence( obj)
            nRepetitions = obj.GetParameter('NREPETITIONS');
            obj.nline_reads = nRepetitions; 
            steps = obj.GetParameter('STEPS')+1;
            obj.nInstructions = 100;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            dutycicle = obj.GetParameter('DUTYCICLE');
            risetime = obj.GetParameter('RISETIME')*1e6;
            flattoptime = obj.GetParameter('FLATTOP')*1e6; 
            periode = (100*(2*risetime+flattoptime))/dutycicle;
            delay = periode-(2*risetime+flattoptime);
            
            switch num2str(obj.GetParameter('READOUT'))
                case 'x'
                    obj.selectReadout = 2;
                case 'y'
                    obj.selectReadout = 0;
                case'z'
                    obj.selectReadout = 1;
            end
   
            selectReadout = obj.selectReadout;
            steeptime = (obj.time1(3) - obj.time1(2));
            iIns = 1;    
   
           for i = 1:nRepetitions
                for j=2:(length(obj.grad1)-1)
                iIns = PulseGradient(obj,iIns,obj.grad1(j),selectReadout,steeptime);
                end
                iIns = PulseGradient(obj,iIns,obj.grad1(length(obj.grad1)),selectReadout,flattoptime);
                
                for j=2:(length(obj.grad2)-1)
                iIns = PulseGradient(obj,iIns,obj.grad2(j),selectReadout,steeptime);
                end
                
               iIns = RepetitionDelay (obj,iIns,1,delay);   
               obj.DACs(iIns-1) =  selectReadout;
            end 
        end
        
        function [status,result] = Run(obj,iAcq)
            clc
            [~,~,~,~,~,~,~] = obj.ConfigureGradients(0);
            gradmax = obj.GetParameter('GRADAMPLITUDEMAX');
            gradmin = obj.GetParameter('GRADAMPLITUDEMIN');
            risetime = obj.GetParameter('RISETIME')*1e6;
            steps = obj.GetParameter('STEPS')+1;

            
            obj.time1 = linspace(0,risetime,steps-1);
            obj.grad1 = linspace(0,gradmax,steps);
            
            obj.time2 = obj.time1+risetime;
            obj.grad2 = fliplr(obj.grad1);

            obj.time3= obj.time1+2*risetime;
            obj.grad3 = linspace(0,-gradmin,steps);
            
            obj.time4= obj.time1+3*risetime;
            obj.grad4 = fliplr(obj.grad3);

     
            obj.nInstructions = 100;
            obj.maxRepetitionsPerBatch = floor(2048/obj.nInstructions);                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
            nBatches = 1;
            t = 0;
            
            for iBatch = 1:nBatches
                obj.CreateSequence;
                obj.Sequence2String;            
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                t1 = clock;
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                t2 = clock;
                t = t + etime(t2,t1);
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Batch aborted!','Warning')
                    return;
                end
            end
        end
    end
    
end








% classdef IECO_TEST2 < MRI_BlankSequence_JM    
%     properties
%         maxRepetitionsPerBatch;
%         nPhasesBatch;
%         lastBatch;
%         RepetitionTime;
%         AcquisitionTime;
%         gradient;
%         time;
%         grad;
%         time1;
%         grad1;
%         time2;
%         grad2;
%         time3;
%         grad3;
%         time4;
%         grad4;
%         
%         
%     end
%     methods (Access=private)
%         function CreateParameters(obj)
%             CreateOneParameter(obj,'NREPETITIONS','Periode repetitions','',10)
%             CreateOneParameter(obj,'RISETIME','Rise time','us',500*MyUnits.us)
%             CreateOneParameter(obj,'DUTYCICLE','Duty cicle','',50)
%             CreateOneParameter(obj,'GRADMAX','+ Amplitude','',1)
%             CreateOneParameter(obj,'GRADMIN','- Amplitude','',1)
%             CreateOneParameter(obj,'STEPS','Number of steps','',5)
%             CreateOneParameter(obj,'FLATTOP','Flat top time','ms',100*MyUnits.ms)
%             
%             
%             obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
%                 'NPOINTS','NSLICES','FREQUENCY','SPECTRALWIDTH',...
%                 'RFAMPLITUDE','PULSETIME','PHASE','SLICE','BLANKINGDELAY','TRANSIENTTIME','COILRISETIME',...
%                 }; 
%         end  
%     end
%     
%     
% 
%     methods
%         function obj = IECO_TEST2(program)
%             obj = obj@MRI_BlankSequence_JM();
%             obj.SequenceName = 'IECO_TEST2';
%             obj.ProgramName = 'MRI_BlankSequenceJM.exe';
%             if nargin > 0
%                 obj.ProgramName = program;
%             end
%             obj.CreateParameters();
%             obj.SetDefaultParameters();
%         end
%         
%         function SetDefaultParameters(obj)
%             SetDefaultParameters@MRI_BlankSequence_JM(obj);
%         end
%         function UpdateTETR(obj)
%             obj.TTotal = (100/obj.GetParameter('DUTYCICLE'))*obj.GetParameter('NSCANS') * obj.GetParameter('NREPETITIONS') *( 4*obj.GetParameter('RISETIME')+2*obj.GetParameter('FLATTOP'));
%         end
%          
%         function CreateSequence(obj)
% 
%             nRepetitions = obj.GetParameter('NREPETITIONS');
%             obj.nline_reads = nRepetitions;
%             
% 
%             steps = obj.GetParameter('STEPS')+1;
% 
%             obj.nInstructions = 190;
%             [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
%            
% 
%             dutycicle = obj.GetParameter('DUTYCICLE');
%             risetime = obj.GetParameter('RISETIME')*1e6;
%             flattoptime = obj.GetParameter('FLATTOP')*1e6; 
%             periode = (100*(4*risetime+2*flattoptime))/dutycicle;
%             delay = periode-(4*risetime+2*flattoptime);
%             
%             
%             switch num2str(obj.GetParameter('READOUT'))
%                 case 'x'
%                     obj.selectReadout = 2;
%                     obj.selectPhase = 0;
%                     obj.selectSlice = 1;
%                 case 'y'
%                     obj.selectReadout = 0;
%                 case'z'
%                     obj.selectReadout = 1;
%                 otherwise 
%                     obj.selectReadout = 2;
%             end
%             
%             
%             selectReadout = obj.selectReadout;
%             steeptime = (obj.time1(3) - obj.time1(2));
% 
%            
%             iIns = 1;    
%             
%             for i = 1:nRepetitions
%                 for j=2:(length(obj.grad1)-1)
%                 iIns = PulseGradient(obj,iIns,obj.grad1(j),selectReadout,steeptime);
%                 end
%                 iIns = PulseGradient(obj,iIns,obj.grad1(length(obj.grad1)),selectReadout,flattoptime);
%                 
%                 for j=2:(length(obj.grad2)-1)
%                 iIns = PulseGradient(obj,iIns,obj.grad2(j),selectReadout,steeptime);
%                 end
%                 
%                 for j=2:(length(obj.grad3)-1)
%                 iIns = PulseGradient(obj,iIns,obj.grad3(j),selectReadout,steeptime);
%                 end
%                 iIns = PulseGradient(obj,iIns,obj.grad3(length(obj.grad3)),selectReadout,flattoptime);
%                 
%                 for j=2:(length(obj.grad4)-1)
%                 iIns = PulseGradient(obj,iIns,obj.grad4(j),selectReadout,steeptime);
%                 end
% 
%                 
%                 iIns = RepetitionDelay (obj,iIns,1,delay);       
%             end 
%         end
%         
%         function [status,result] = Run(obj,iAcq)
%             clc
%             [~,~,~,~,~,~,~] = obj.ConfigureGradients(0);
%                         
%             % Get sequence parameters
%             repetitions = obj.GetParameter('NREPETITIONS');
%             gradmax = obj.GetParameter('GRADMAX');
%             gradmin = obj.GetParameter('GRADMIN');
%             risetime = obj.GetParameter('RISETIME')*1e6;
%             steps = obj.GetParameter('STEPS')+1;
% 
%             
%             obj.time1 = linspace(0,risetime,steps-1);
%             obj.grad1 = linspace(0,gradmax,steps);
%             
%             obj.time2 = obj.time1+risetime;
%             obj.grad2 = fliplr(obj.grad1);
% 
%             obj.time3= obj.time1+2*risetime;
%             obj.grad3 = linspace(0,-gradmin,steps);
%             
%             obj.time4= obj.time1+3*risetime;
%             obj.grad4 = fliplr(obj.grad3);
% 
% 
%            
% 
%             obj.nInstructions = 190;
%             obj.maxRepetitionsPerBatch = floor(1024/obj.nInstructions);                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
%             nBatches = 1;
%             t = 0;
%             
%             for iBatch = 1:nBatches
%                  obj.CreateSequence();
%                 obj.Sequence2String;            
%                 fname = 'TempBatch.bat';
%                 obj.WriteBatchFile(fname);
%                 t1 = clock;
%                 [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
%                 t2 = clock;
%                 t = t + etime(t2,t1);
%                 if status == 0 && ~isempty(result)
%                     C = textscan(result, '%s', 'delimiter', sprintf('\f'));
%                     res = C{:};
%                     obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
%                     obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
%                     obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
%                     obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
%                     obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
%                 else
%                     warndlg('Batch aborted!','Warning')
%                     return;
%                 end
%                 
% 
%         end
%     end
%     
%     end
% end











%%CODIGO MODIFICADO EL 04/06/2019 PARA EXCITAR UN UNICO GRADIENTE INDEPENDIENTE DE LOS
%%DEM�S. nInstructions=190 para 5 steps y 10 repeticiones.
%% PROBLEMAS DE CORTE DE INSTRUCCIONES CUANDO DELAY SON MUY LARGOS

% classdef IECO_TEST2 < MRI_BlankSequence6   
%     properties
%         maxRepetitionsPerBatch;
%         nPhasesBatch;
%         lastBatch;
%         RepetitionTime;
%         AcquisitionTime;
%         gradient;
%         time;
%         grad;
%         time1;
%         grad1;
%         time2;
%         grad2;
%         time3;
%         grad3;
%         time4;
%         grad4;
%         
%         
%     end
%     methods (Access=private)
%         function CreateParameters(obj)
% 
%             CreateOneParameter(obj,'RISETIME','Rise time','us',500*MyUnits.us)
%             CreateOneParameter(obj,'GRADMAX','+ Amplitude','',1)
%             CreateOneParameter(obj,'STEPS','Number of steps','',5)
%             CreateOneParameter(obj,'FLATTOP','Flat top time','s',300*MyUnits.s)
%             
%             
%             obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
%                 'NPOINTS','NSLICES','FREQUENCY','SPECTRALWIDTH',...
%                 'RFAMPLITUDE','PULSETIME','PHASE','SLICE','BLANKINGDELAY','TRANSIENTTIME','COILRISETIME',...
%                 }; 
%         end  
%     end
%     
%     
% 
%     methods
%         function obj = IECO_TEST2(program)
%             obj = obj@MRI_BlankSequence6();
%             obj.SequenceName = 'IECO_TEST2';
%             obj.ProgramName = 'MRI_BlankSequence3.exe';
%             if nargin > 0
%                 obj.ProgramName = program;
%             end
%             obj.CreateParameters();
%             obj.SetDefaultParameters();
%         end
%         
% %         function SetDefaultParameters(obj)
% %             SetDefaultParameters@MRI_BlankSequence_JM(obj);
% %         end
% 
%          
%         function CreateSequence(obj)
% 
%             nRepetitions = 10;
%             obj.nline_reads = nRepetitions;
% 
%             obj.nInstructions = 90;
%             [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
% 
%             risetime = obj.GetParameter('RISETIME')*1e6;
%             flattoptime = obj.GetParameter('FLATTOP')*1E6; 
%             
%             switch num2str(obj.GetParameter('READOUT'))
%                 case 'x'
%                     obj.selectReadout = 2;
%                     obj.selectPhase = 0;
%                     obj.selectSlice = 1;
%                 case 'y'
%                     obj.selectReadout = 0;
%                 case'z'
%                     obj.selectReadout = 1;
%                 otherwise 
%                     obj.selectReadout = 2;
%             end
%             
%             
%             selectReadout = obj.selectReadout;
%             steeptime = (obj.time1(3) - obj.time1(2));
% 
%            
%             iIns = 1;    
%             
%             for i = 1:nRepetitions
%                 for j=2:(length(obj.grad1)-1)
%                 iIns = PulseGradient(obj,iIns,obj.grad1(j),selectReadout,steeptime);
%                 end
%                 iIns = PulseGradient(obj,iIns,obj.grad1(length(obj.grad1)),selectReadout,flattoptime); 
%                 for j=2:(length(obj.grad2)-1)
%                 iIns = PulseGradient(obj,iIns,obj.grad2(j),selectReadout,steeptime);
%                 end
%             end 
%         end
%         
%         function [status,result] = Run(obj,iAcq)
%             clc
%             [~,~,~,~,~,~,~] = obj.ConfigureGradients(0);
%                         
%             % Get sequence parameters
%             gradmax = obj.GetParameter('GRADMAX');
%             risetime = obj.GetParameter('RISETIME')*1e6;
%             steps = obj.GetParameter('STEPS')+1;
%      
%             obj.time1 = linspace(0,risetime,steps-1);
%             obj.grad1 = linspace(0,gradmax,steps);
%             
%             obj.time2 = obj.time1+risetime;
%             obj.grad2 = fliplr(obj.grad1);
% 
% 
%             obj.nInstructions = 9;
%             obj.maxRepetitionsPerBatch = floor(1024/obj.nInstructions);                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
%             nBatches = 1;
%             t = 0;
%             
%             for iBatch = 1:nBatches
%                  obj.CreateSequence();
%                 obj.Sequence2String;            
%                 fname = 'TempBatch.bat';
%                 obj.WriteBatchFile(fname);
%                 t1 = clock;
%                 [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
%                 t2 = clock;
%                 t = t + etime(t2,t1);
%                 if status == 0 && ~isempty(result)
%                     C = textscan(result, '%s', 'delimiter', sprintf('\f'));
%                     res = C{:};
%                     obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
%                     obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
%                     obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
%                     obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
%                     obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
%                 else
%                     warndlg('Batch aborted!','Warning')
%                     return;
%                 end
%             end
%         end
%     end
% end
% 
% 
% 
% 




