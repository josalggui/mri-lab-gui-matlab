classdef GradientEcho3D_cal < MRI_BlankSequence
    %   GradientEcho3D sequence based on the general MRI_BlankSequence and implementing
    %   the adjustment of the Central Frequency as well as the calibration of the k-space
    
    %  Elena Diaz Caballero
    
    properties
        StartSlice;
        EndSlice;
        PhasesPerBatch = 20;
        StartPhase;
        EndPhase;
        nPhasesBatch;
        %         RunCal = false;
        Cal1;
        Cal2;
        Cal3;
        Cal4;
        Dat1;
        deltafreq_max = 10; % the maximum frequency used for cutoff
    end
    methods (Access=private)
        function CreateParameters(obj)
            InputParameters = {...
                'NPHASES','PHASETIME','ECHODELAY','REPETITIONDELAY','READOUTAMPLITUDE','READOUTAMPLITUDE2',...
                'SLICEBANDWIDTH','STARTPHASE',...
                'ENDPHASE','STARTSLICE','ENDSLICE',...
                'GAUSSIANFILTER'...
                };
            
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {...
                'Number of Phases','Phase Time','Gradient Echo Delay','Repetition Delay','DEP Readout Amplitude','REP Readout Amplitude',...
                'Slice Bandwidth','Start Phase',...
                'End Phase','Start Slice','End Slice',...
                'Gaussian'...
                })];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,NaN,...
                NaN...
                })];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,NaN,...
                0 ...
                })];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,NaN,...
                1 ...
                })];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {...
                '','us','us','ms','','',...
                'MHz','',...
                '','','',...
                ''...
                })];
            
            for k=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{k},InputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = GradientEcho3D_cal(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'Gradient Echo 3D Cal';
            obj.ProgramName = 'MRI_BlankSequence.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
            obj.SetParameter('PHASETIME',50*MyUnits.us);
            obj.SetParameter('ECHODELAY',20*MyUnits.us);
            obj.SetParameter('REPETITIONDELAY',50*MyUnits.ms);
            obj.SetParameter('READOUTAMPLITUDE',0.1);
            obj.SetParameter('READOUTAMPLITUDE2',0.1);
            obj.SetParameter('NPHASES',1)
            obj.SetParameter('STARTPHASE',-0.1)
            obj.SetParameter('ENDPHASE',0.1)
            obj.SetParameter('STARTSLICE',-0.1)
            obj.SetParameter('ENDSLICE',0.1)
            obj.SetParameter('GAUSSIANFILTER',1);
            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
        end
        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence,
            %         "1", central frequency adjustment sequence,
            %         "2", calibration of the k-space,  RO slice + encoding gradient ON
            %         "3",                              RO slice + encoding gradient OFF
            %         "4",                              phase slice + encoding gradient ON
            %         "5",                              phase slice + encoding gradient OFF
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=obj.SelectAxes;
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            
            obj.StartSlice = obj.GetParameter('STARTSLICE');
            obj.EndSlice = obj.GetParameter('ENDSLICE');
            
            [nSlices,nPhases,readout,slice,phase,spoiler,SSaux] = obj.ConfigureGradients(mode);
            
            % Calculate the number of line reads
            obj.nline_reads=nPhases*nSlices;
            
            % Initialize all vectors to zero with the appropriate size
            nInstructionsPerGroup = 23; % this number must be changed accordingly per pulse sequence
            
            obj.nInstructions = nInstructionsPerGroup * nPhases * nSlices;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            % Calculate acquisition time
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
            
            % readout gradient amplitude calculation
            if( readout == 0 )
                dephasing_readout = shimR;
                phasing_readout = shimR;
            elseif( obj.GetParameter('PHASETIME') == 0 )
                dephasing_readout = shimR;
                phasing_readout = min(1.0, max( -1.0, shimR -1.0 * obj.GetParameter('READOUTAMPLITUDE2')));
            elseif( acq_time  > 2 * (obj.GetParameter('PHASETIME') + 1e-6)  )
                dephasing_readout = min(1.0, max( -1.0, shimR +1.0 * obj.GetParameter('READOUTAMPLITUDE')));
                phasing_readout = min( 1.0, max( -1.0, shimR -2 *obj.GetParameter('READOUTAMPLITUDE2') * (obj.GetParameter('PHASETIME') + 1e-6) / ( acq_time )));
            else
                phasing_readout = min(1.0, max( -1.0, shimR -1.0 * obj.GetParameter('READOUTAMPLITUDE2')));
                dephasing_readout = max( -1.0, min( 1.0, shimR + obj.GetParameter('READOUTAMPLITUDE') *  acq_time / (2 * (obj.GetParameter('PHASETIME') + 1e-6) )));
            end
            
            
            
            % ---- LOOP THROUGH SLICES ---- %
            
            for slicei = 1:nSlices
                
                % Calculate SLICE amplitudes
                if( slice == 0 )
                    slice_amplitude = shimS;
                    slice_rephase_amplitude = shimS;
                else
                    if (nSlices == 1)
                        slice_amplitude = obj.StartSlice;
                        slice_rephase_amplitude = - slice_amplitude;
                    else
                        slice_amplitude =  min(1.0 , max(-1.0,shimS + (obj.EndSlice - obj.StartSlice) * (slicei-1)/(nSlices-1) + obj.StartSlice));
                        slice_rephase_amplitude = - slice_amplitude;
                    end
                end
                
                % ---- LOOP THROUGH PHASES ---- %
                
                for phasei = 1:nPhases
                    
                    % phase gradient amplitude calculation
                    if( phase == 0 ) || ( obj.GetParameter('NPHASES') < 2 )
                        phase_amplitude = shimP;
                        phase_rephase_amplitude = shimP;
                    else
                        phase_amplitude =  min(1.0 , max(-1.0,shimP + (obj.EndPhase - obj.StartPhase) * (phasei-1)/(nPhases-1) + obj.StartPhase));
                        phase_rephase_amplitude = -phase_amplitude;
                    end
                    
                    % Setting the appropriate amplitude for the slice slection for
                    % calibration - this should be a constant value chosen at HALF
                    % the maximum phase or readout amplitudes. That way the ROI is
                    % somewhere in between the origin and the maximum area (Lx/4)
                    obj.deltafreq_max; % [kHz] this is the MAX frequency that we will measure for offsetting the pulses. this will dictate the following gradient amplitude calculations
                    
                    % If we are not calibrating, set the amplitude to the appropriate value
                    if (mode == 0 || mode == 1)
                        slice_amplitude2 = slice_amplitude;
                        obj.SetParameter('SLICEFREQOFF',0*MyUnits.kHz);
                    end
                    
                    % For calibration of the readout gradients, set the SLICE amplitude to the amplitude needed to achieve a point halfway from the isocenter to the edge. This requires
                    % a frequency to look at. The formula uses a frequency to calculate the gradient amplitude applied.
                    % G2 = ( 2 * fmax * G1 ) / deltaV
                    % ALSO MAKE SURE TO SET THE phase_amplitude TO SHIMP
                    if (mode == 2 || mode== 3)
                        slice_amplitude2 = ( 0.1 * ( obj.deltafreq_max * 1000) * obj.GetParameter('READOUTAMPLITUDE2') ) / obj.GetParameter('SPECTRALWIDTH');
                        obj.SetParameter('SLICEFREQOFF',obj.deltafreq_max*MyUnits.kHz);
                        phase_amplitude = shimP;
                        phase_rephase_amplitude = shimP;
                    end
                    
                    % For calibration of the phase gradients, set the SLICE amplitude to the amplitude needed to achieve a point halfway from the isocenter to the edge. This requires
                    % a frequency to look at. The formula uses a frequency to calculate the gradient amplitude applied.
                    % G2 = ( 8 * fmax * G1_max * phasetime ) / ( nPhases - 1 )
                    % ALSO MAKE SURE TO SET THE readout_amplitude TO SHIMR
                    if (mode == 4 || mode== 5)
                        slice_amplitude2 = ( 0.2 * ( obj.deltafreq_max * 1000 ) * obj.GetParameter('PHASETIME') * max( abs(obj.GetParameter('STARTPHASE')), abs(obj.GetParameter('ENDPHASE')) ) ) / ( obj.GetParameter('NPHASES') - 1 ) ;
                        obj.SetParameter('SLICEFREQOFF',obj.deltafreq_max*MyUnits.kHz);
                        dephasing_readout = shimR;
                        phasing_readout   = shimR;
                    end
                    
                    % DEFINE THE PULSE SEQUENCE NOW %
                    
                    % the specific line that you are within for the pulse
                    iline = nInstructionsPerGroup * ((slicei-1)*nPhases+(phasei-1));
                    
                    % initial instruction
                    iIns  = 1;
                    
                    %------ Blanking Delay and Shimming in all gradients (4 instructions)
                    nIns  = 4; % set number of instructions in this group
                    obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = [shimP shimR shimS slice_amplitude2];
                    obj.DACs(iline + ( iIns: (nIns+iIns-1)) )     = [PP RR SS SSaux];
                    obj.WRITEs(iline + ( iIns: (nIns+iIns-1)) )   = [1 1 1 1];
                    obj.UPDATEs(iline + ( iIns: (nIns+iIns-1)) )  = [1 1 1 1];
                    obj.PhResets(iline + ( iIns: (nIns+iIns-1)) ) = [0 0 0 1];
                    obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= [7 7 7 7]; % (7=no shape)
                    obj.FLAGs(iline + ( iIns: (nIns+iIns-1)) )    = [1 1 1 1];
                    obj.DELAYs(iline + ( iIns: (nIns+iIns-1)) )   = [0.1 0.1 obj.GetParameter('BLANKINGDELAY')*1e6 obj.GetParameter('COILRISETIME')*1e6]; %time in us
                    iIns = iIns + nIns; % update the number of instructions
                    
                    %------ RF pulse (1 instruction)
                    nIns  = 1; % set number of instructions in this group
                    obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = slice_amplitude2;
                    obj.DACs(iline + ( iIns: (nIns+iIns-1)) )     = SSaux;
                    obj.TXs(iline + ( iIns: (nIns+iIns-1)) )      = 1;
                    if mode > 1 % Switch the Frequency register to the appropriate one for modes 2, 3, 4, and 5
                        obj.FREQs(iline + ( iIns: (nIns+iIns-1)) ) = 1;
                    end
                    if (obj.RF_Shape) % Shaped RF pulse
                        obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= 0;
                    else
                        obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= 7;
                    end
                    obj.PhResets(iline + ( iIns: (nIns+iIns-1)) ) = 0;
                    obj.FLAGs(iline + ( iIns: (nIns+iIns-1)) )    = 1;
                    obj.OPCODEs(iline + ( iIns: (nIns+iIns-1)) )  = 0;
                    obj.DELAYs(iline + ( iIns: (nIns+iIns-1)) )   = obj.GetParameter('PULSETIME')*1e6; %time in us
                    iIns = iIns + nIns; % update the number of instructions
                    
                    %----- Set back to SHIMS and rephase the added slice gradient done for calibration plus transient delay ( instructions)
                    nIns  = 7; % set number of instructions in this group
                    obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = [shimS shimR shimP -slice_amplitude2 shimS shimR shimP];
                    obj.DACs(iline + ( iIns: (nIns+iIns-1)) )     = [SS RR PP SSaux SS RR PP];
                    obj.WRITEs(iline + ( iIns: (nIns+iIns-1)) )   = [1 1 1 1 1 1 1];
                    obj.UPDATEs(iline + ( iIns: (nIns+iIns-1)) )  = [1 1 1 1 1 1 1];
                    obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= [7 7 7 7 7 7 7]; % (7=no shape)
                    obj.DELAYs(iline + ( iIns: (nIns+iIns-1)) )   = [0.1 0.1 obj.GetParameter('COILRISETIME')*1e6 obj.GetParameter('COILRISETIME')*1e6+0.5*obj.GetParameter('PULSETIME')*1e6 0.1 0.1 obj.GetParameter('COILRISETIME')*1e6+obj.GetParameter('TRANSIENTTIME')*1e6]; %time in us
                    iIns = iIns + nIns; % update the number of instructions
                    
                    %----- TSetting the slice and readout dephasing gradients (2 instructions)
                    nIns  = 2; % set number of instructions in this group
                    obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = [slice_amplitude dephasing_readout];
                    obj.DACs(iline + ( iIns: (nIns+iIns-1)) )     = [SS RR];
                    obj.WRITEs(iline + ( iIns: (nIns+iIns-1)) )   = [1 1];
                    obj.UPDATEs(iline + ( iIns: (nIns+iIns-1)) )  = [1 1];
                    obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= [7 7]; % (7=no shape)
                    obj.DELAYs(iline + ( iIns: (nIns+iIns-1)) )   = [0.1 0.1]; %time in us
                    iIns = iIns + nIns; % update the number of instructions
                    
                    %----- Phase gradient, rephasing readout gradient, reset slice and phase gradients and echo delay (3 instructions)
                    nIns  = 3; % set number of instructions in this group
                    obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = [phase_amplitude shimP phasing_readout ];
                    obj.DACs(iline + ( iIns: (nIns+iIns-1)) )     = [PP PP RR];
                    obj.WRITEs(iline + ( iIns: (nIns+iIns-1)) )   = [1 1 1];
                    obj.UPDATEs(iline + ( iIns: (nIns+iIns-1)) )  = [1 1 1];
                    obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= [7 7 7]; % (7=no shape)
                    obj.DELAYs(iline + ( iIns: (nIns+iIns-1)) )   = [(obj.GetParameter('PHASETIME')+obj.GetParameter('COILRISETIME'))*1e6 obj.GetParameter('COILRISETIME')*1e6 obj.GetParameter('COILRISETIME')*1e6+obj.GetParameter('ECHODELAY')*1e6]; %time in us
                    iIns = iIns + nIns; % update the number of instructions
                    
                    %----- Rephasing Readout Gradient and Data Acquisition. Rewinding lobe for readout, slice and phase gradients (4 instructions)
                    nIns  = 4; % set number of instructions in this group
                    obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = [phasing_readout shimR slice_rephase_amplitude phase_rephase_amplitude];
                    obj.DACs(iline + ( iIns: (nIns+iIns-1)) )     = [RR RR SS PP];
                    obj.WRITEs(iline + ( iIns: (nIns+iIns-1)) )   = [1 1 1 1];
                    obj.UPDATEs(iline + ( iIns: (nIns+iIns-1)) )  = [1 1 1 1];
                    obj.RXs(iline + ( iIns: (nIns+iIns-1)) )      = [1 0 0 0];
                    obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= [7 7 7 7]; % (7=no shape)
                    obj.DELAYs(iline + ( iIns: (nIns+iIns-1)) )   = [acq_time*1e6 obj.GetParameter('COILRISETIME')*1e6 0.1 (obj.GetParameter('PHASETIME')+obj.GetParameter('COILRISETIME'))*1e6]; %time in us
                    iIns = iIns + nIns; % update the number of instructions
                    
                    %----- Spoiler for slice gradient (1 instruction)
                    nIns  = 1; % set number of instructions in this group
                    if (spoiler)
                        obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = obj.GetParameter('SPOILERAMP');
                    else
                        obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = shimS;
                    end
                    obj.DACs(iline + ( iIns: (nIns+iIns-1)) )     = SS;
                    obj.WRITEs(iline + ( iIns: (nIns+iIns-1)) )   = 1;
                    obj.UPDATEs(iline + ( iIns: (nIns+iIns-1)) )  = 1;
                    obj.CLEARs(iline + ( iIns: (nIns+iIns-1)) )   = 1;
                    obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= 7; % (7=no shape)
                    obj.DELAYs(iline + ( iIns: (nIns+iIns-1)) )   = obj.GetParameter('SPOILERTIME')*1e6; %time in us
                    iIns = iIns + nIns; % update the number of instructions
                    
                    %----- Repetition Delay (1 instruction)
                    nIns  = 1; % set number of instructions in this group
                    obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = 0.0;
                    obj.DACs(iline + ( iIns: (nIns+iIns-1)) )     = 3;
                    obj.WRITEs(iline + ( iIns: (nIns+iIns-1)) )   = 1;
                    obj.UPDATEs(iline + ( iIns: (nIns+iIns-1)) )  = 1;
                    obj.CLEARs(iline + ( iIns: (nIns+iIns-1)) )   = 1;
                    obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= 7; % (7=no shape)
                    obj.DELAYs(iline + ( iIns: (nIns+iIns-1)) )   = obj.GetParameter('REPETITIONDELAY')*1e6; %time in us
                    iIns = iIns + nIns; % update the number of instructions
                    
                end % end for nPhases
            end %end for slicei
            
        end % end create gradient echo pulse sequence
        
        function [status,result] = Run(obj,iAcq)
            
            obj.StartSlice = obj.GetParameter('STARTSLICE');
            obj.EndSlice = obj.GetParameter('ENDSLICE');
            obj.StartPhase = obj.GetParameter('STARTPHASE');
            obj.EndPhase = obj.GetParameter('ENDPHASE');
            nSlices = obj.GetParameter('NSLICES');
            if nSlices > 1
                SliceStep = (obj.EndSlice - obj.StartSlice)/(nSlices-1);
            else
                SliceStep = 0;
            end
            nPhases = obj.GetParameter('NPHASES');
            if nPhases > 1
                PhaseStep = (obj.EndPhase - obj.StartPhase)/(nPhases-1);
            else
                PhaseStep = 0;
            end
            nPhaseBatches = ceil(nPhases/obj.PhasesPerBatch);
            nPoints = obj.GetParameter('NPOINTS');
            Data3D = zeros(nPoints,nPhases,nSlices);
            cn = nSlices*nPhaseBatches;
            count = 0;
            if nPhaseBatches>1
                h_waitbar = waitbar(count/cn,'Please wait (por favor espere)...');
            end
            
            if obj.CalCheck % if the user has selected to perform the k-space calibration
                % Check if calibration needs to be done (if its the First Average and IF the points have changed)
                if ( iAcq == 1 ) || ( numel( obj.Cal3 ) ~=  nPoints*nPhases*nSlices )
                    [ obj.Cal1, obj.Cal2 ] = deal( zeros(nPoints,1) );
                    [ obj.Cal3, obj.Cal4 ] = deal( zeros(nPoints*nPhases*nSlices, 1) );
                    obj.Dat1 = zeros( nPoints * nPhases * nSlices, 1 );
                    obj.RunCal = true;
                end
                %             else
                %                  obj.Dat1 = zeros( nPoints * nPhases * nSlices, 1 );
            end
            
            
            % Initialize the data storage matrices
            [ cDataBatch_Cal1, cDataBatch_Cal2, cDataBatch_Cal3, cDataBatch_Cal4, cDataBatch_cal ] = deal( [] ) ;
            
            % -- DO READOUT CALIBRATION -- %
            % this needs to only be done once, since there is only ONE
            % readout gradient sequence, there will be nPoints of
            % calibration %
            if obj.RunCal && obj.readout_en
                % replace the number of NPHASES with 1 and the number of NSLICE with 1
                temp_nslice = obj.GetParameter('NSLICES');
                obj.nPhasesBatch = 1;
                obj.SetParameter('NSLICES',1);
                
                mode = 2;
                fname_Cal = 'TempBatch_Cal_1.bat';     
                cDataBatch_Cal1=obj.getCalibrationData(mode,fname_Cal,cDataBatch_Cal1);
                
                mode = 3;
                fname_Cal = 'TempBatch_Cal_2.bat';
                cDataBatch_Cal2=obj.getCalibrationData(mode,fname_Cal,cDataBatch_Cal2);
                
                % replace the number of NPHASES with the original values
                obj.SetParameter('NSLICES',temp_nslice);
            end % checking for calibration
            
            for iSlice = 1:nSlices
                
                obj.StartSlice = obj.GetParameter('STARTSLICE') + (iSlice-1)*SliceStep;
                obj.EndSlice = obj.StartSlice + SliceStep;
                Data2D = []; %in case we don't want to perform the calibration
                for iBatch = 1:nPhaseBatches
                    obj.StartPhase = obj.GetParameter('STARTPHASE') + (iBatch-1)*obj.PhasesPerBatch*PhaseStep;
                    if iBatch == nPhaseBatches
                        obj.nPhasesBatch = obj.GetParameter('NPHASES') - obj.PhasesPerBatch*(iBatch-1);
                        obj.EndPhase = obj.GetParameter('ENDPHASE');
                    else
                        obj.nPhasesBatch = obj.PhasesPerBatch;
                        obj.EndPhase = obj.StartPhase + (obj.PhasesPerBatch-1)*PhaseStep;
                    end
                    
                    if nPhases >1
                        obj.CreateSequence(1); % Create the gradient echo sequence for CF
                        obj.Sequence2String;
                        fname_CF = 'TempBatch_CF.bat';
                        obj.WriteBatchFile(fname_CF);
                        obj.CenterFrequency(fname_CF);
                    end % end check central frequency
                    obj.SetParameter('LASTFREQUENCY',obj.GetParameter('FREQUENCY'));
                    
                    if  obj.RunCal && obj.phase_en % check if calbiration is necesary - set to false initially, then fix per nAvgs
                        
                        % -- DO PHASE GRADIENT CALIBRATION -- %
                        % This needs to be done for each phase level %
                        % There will be nPhases * nPoints of calibration
                        mode = 4;
                        fname_Cal = 'TempBatch_Cal_3.bat';
                        cDataBatch_Cal3=obj.getCalibrationData(mode,fname_Cal,cDataBatch_Cal3);
                        
                        mode = 5;
                        fname_Cal = 'TempBatch_Cal_4.bat';
                        cDataBatch_Cal4=obj.getCalibrationData(mode,fname_Cal,cDataBatch_Cal4);
                        
                    end % end check if calibration is necessary
                    
                    % NOW RUN ACTUAL ACQUISTION
                    obj.CreateSequence(0); % Create the normal gradient echo sequence
                    obj.Sequence2String;
                    
                    
                    fname = 'TempBatch.bat';
                    obj.WriteBatchFile(fname);
                    [status,result] = system(fullfile(obj.path_file,fname));
                    
                    if status == 0 && ~isempty(result)
                        C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                        res = C{:};
                        obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                        obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                        obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                        obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                        obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                    end % read variable information from sequence
                    
                    DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                    cDataBatch_cal = [ cDataBatch_cal ; complex(DataBatch(:,1),DataBatch(:,2))];

                    cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                    nPhasesInBatch=length(cDataBatch)/nPoints;
                    d = reshape(cDataBatch,nPoints,nPhasesInBatch);
                    Data2D = cat(2,Data2D,d);

                    clear DataBatch
                    
                    
                    count = count+1;
                    if nPhaseBatches>1
                        waitbar(count/cn,h_waitbar,sprintf('Please wait / Por favor espere..... %5.2f %%',100*count/cn));
                    end
                end % run through the phases
            end % run through the slices
            
            %             if ~obj.CalCheck
            Data3D(:,:,iSlice) = Data2D;
            if length(obj.cData) ~= nPoints*nPhases*nSlices;
                obj.cData = zeros(obj.NAverages,nPoints*nPhases*nSlices);
            end
            %             end
            % Check the number of slices
            
            % Check for averages and calculate the index for where to store the data
            if obj.autoPhasing
                pt = max(Data3D(:));
                Data3D = Data3D.*exp(complex(0,-angle(pt)));
            end
            if obj.averaging
                Nav = obj.NAverages;
            else
                Nav = 1;
            end
            index = mod(iAcq-1,Nav)+1; % the index needed for which slot to store the data
            
            if obj.CalCheck
                % Store all the data appropriately
                obj.Dat1( : , index ) = cDataBatch_cal;
                if obj.RunCal
                    obj.Cal1 = cDataBatch_Cal1;
                    obj.Cal2 = cDataBatch_Cal2;
                    obj.Cal3 = cDataBatch_Cal3;
                    obj.Cal4 = cDataBatch_Cal4;
                end
            end
            
            obj.cData(index,1:length(Data3D(:))) = Data3D(:);
            obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
            
            % Reset the Calibration Switch
            obj.RunCal = false;
            
            obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
            obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
            
            acq_time = obj.GetParameter('ACQUISITIONTIME');
            obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*acq_time/nPhases;
            obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time/nPhases;
            
            if nPhaseBatches>1
                delete(h_waitbar);
            end
        end
    end
    
end

