classdef InversionRecovery2 < MRI_BlankSequence
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    %  Elena Diaz Caballero
    
    properties
%         StartSlice;
%         EndSlice;
%         PhasesPerBatch = 20;
        StartPhase;
        EndPhase;
%         slice_ampl;
    end
    methods (Access=private)
        function CreateParameters(obj)
            InputParameters = {
                'INVERSIONTIME','ACQUISITIONDELAY','REPETITIONDELAY','FREQUENCYSHIFT',...
                'INVERSIONON',...
                };
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {...
                'Inversion Time','Acquisition Delay','Repetition Delay','Frequency Shift',...
                'Inversion On?',...
                })];
             obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
               {...
                NaN,NaN,NaN,NaN...
                NaN,...
                })];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN...
                0,...
                })];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN...
                1,...
                })];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {...
                'ms','us','ms','kHz',...
                '',...
                })];
            
            
            for k=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{k},InputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = InversionRecovery2(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'Inversion Recovery';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
            obj.SetParameter('INVERSIONTIME',100*MyUnits.ms);
            obj.SetParameter('ACQUISITIONDELAY',20*MyUnits.us);
            obj.SetParameter('REPETITIONDELAY',50*MyUnits.ms);
            obj.SetParameter('FREQUENCYSHIFT',10*MyUnits.kHz);
                      
            obj.SetParameter('INVERSIONON',0);
            
            obj.TE = 0;
            obj.TR = 0;
            obj.TTotal = 0;
            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
        end
        function UpdateTETR( obj )
            
            obj.TE = 0.5 * (obj.GetParameter('PULSETIME') + obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')) + obj.GetParameter('ACQUISITIONDELAY');
            obj.TR = obj.TE*2 - obj.GetParameter('ACQUISITIONDELAY') + obj.GetParameter('INVERSIONTIME')+ 2*obj.GetParameter('PULSETIME') + obj.GetParameter('TRANSIENTTIME');
            obj.TTotal = obj.TR * obj.GetParameter('NSCANS');
        end
        
        function CreateSequence( obj, ~ )
            % mode =  "0", normal execution of the sequence,
            %         "1", central frequency adjustment sequence,
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
                       
            % Calculate the number of line reads
            obj.nline_reads=1;
            
            % check if sequence has inversion on
            InversionCheck = obj.GetParameter('INVERSIONON');
            
            % Initialize all vectors to zero with the appropriate size
            if InversionCheck
                obj.nInstructions = 10;
            else
                obj.nInstructions = 7;
            end
            
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
            
                       
            iIns = 1;
            bbit = 1;
            
            
                           
            % BLANKING DELAY and SHIMMING in all gradients AND PULSE
            % SENT TO ADC FOR ACQUISITION
            nIns  = 3; % set number of instructions in this group
            vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                obj.AMPs(vIns)     = [ shimP shimR shimS ];
                obj.DACs(vIns)     = [ PP RR SS ];
                obj.WRITEs(vIns)   = [ 1 1 1 ];
                obj.UPDATEs(vIns)  = [ 1 1 1 ];
                obj.PhResets(vIns) = [ 0 0 1 ];
                obj.PHASEs(vIns)   = [ 0 0 0 ];
                obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                obj.FLAGs(vIns)    = [ 0 0 bbit ];
                obj.DELAYs(vIns)   = [0.1 0.1 obj.GetParameter('BLANKINGDELAY')*1e6]; %time in us
            iIns = iIns + nIns; % update the number of instructions
            
            if InversionCheck
                %------ 180 RF pulse (1 instruction)
                nIns = 1; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.TXs(vIns)      = 1;
                    if (obj.RF_Shape) % Shaped RF pulse
                        obj.ENVELOPEs(vIns)= 0;
                    else
                        obj.ENVELOPEs(vIns)= 7;
                    end
                    obj.PHASEs(vIns)   = 0;
                    obj.FLAGs(vIns)    = bbit;
                    obj.OPCODEs(vIns)  = 0;
                    obj.DELAYs(vIns)   = 2*obj.GetParameter('PULSETIME')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
            
                %----- transient time and inversion time   
                nIns = 2; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [ shimS shimS ];
                    obj.DACs(vIns)     = [ SS SS ];
                    obj.WRITEs(vIns)   = [ 0 0 ];
                    obj.UPDATEs(vIns)  = [ 0 0 ];
                    obj.PhResets(vIns) = [ 0 0 ];
                    obj.PHASEs(vIns)   = [ 0 0 ];
                    obj.ENVELOPEs(vIns)= [ 7 7 ]; % (7=no shape)
                    obj.FLAGs(vIns)    = [ 0 0 ];
                    obj.DELAYs(vIns)   = [ obj.GetParameter('TRANSIENTTIME')*1e6 obj.GetParameter('INVERSIONTIME')*1e6];

                iIns = iIns + nIns; % update the number of instructions in this block
            end
            
            %------ 90 RF pulse (1 instruction)
            nIns = 1; % set the number of instruction in this block
            vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                obj.AMPs(vIns)     = 0.0;
                obj.DACs(vIns)     = 3;
                obj.TXs(vIns)      = 1;
                if (obj.RF_Shape) % Shaped RF pulse
                    obj.ENVELOPEs(vIns)= 0;
                else
                    obj.ENVELOPEs(vIns)= 7;
                end
                obj.PHASEs(vIns)   = 0;
                obj.FLAGs(vIns)    = bbit;
                obj.OPCODEs(vIns)  = 0;
                obj.DELAYs(vIns)   = obj.GetParameter('PULSETIME')*1e6; %time in us
            iIns = iIns + nIns; % update the number of instructions in this block
                   
            %----- Acquisition delay (GED) and RX
            nIns = 2;
            vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                obj.AMPs(vIns)     = [ shimR shimR ];
                obj.DACs(vIns)     = [ RR RR ];
                obj.WRITEs(vIns)   = [ 0 0 ];
                obj.UPDATEs(vIns)  = [ 0 0 ];
                obj.PHASEs(vIns)   = [ 0 0 ];
                obj.FREQs(vIns)    = [ 0 1 ];
                obj.RXs(vIns)      = [ 0 1 ];
                obj.ENVELOPEs(vIns)= [ 7 7 ]; % (7=no shape)
                obj.DELAYs(vIns)   = [( obj.GetParameter('TRANSIENTTIME')+obj.GetParameter('ACQUISITIONDELAY'))*1e6 acq_time*1e6]; %time in us
            iIns = iIns + nIns; % update the number of instructions in this block
                
            %----- Repetition Delay (1 instruction)
            nIns = 1;
            vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                obj.AMPs(vIns)     = 0.0;
                obj.DACs(vIns)     = 3;
                obj.WRITEs(vIns)   = 1;
                obj.UPDATEs(vIns)  = 1;
                obj.PHASEs(vIns)  = 0;
                obj.CLEARs(vIns)   = 1;
                obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                obj.DELAYs(vIns)   = obj.GetParameter('REPETITIONDELAY')*1e6; %time in us
            iIns = iIns + nIns; % update the number of instructions in this block
                
                
        end % end create gradient echo pulse sequence
        

        function [status,result] = Run(obj,iAcq)
            global MRIDATA
            
            
            
            obj.EndPhase = 0;
            obj.StartPhase = 0;
            
            nPhases = 1;
            nSlices = 1;
            nPoints = obj.GetParameter('NPOINTS');
            Data3D = zeros(nPoints);
            
            obj.SliceFreqOffset = obj.GetParameter('FREQUENCYSHIFT');
            
            if isfield(MRIDATA,'Data3Dnew')
                if ( isequal(size(MRIDATA.Data3Dnew),[nPoints, obj.NAverages])) || (iAcq == 1)
                    MRIDATA.Data3Dnew = [];
                end
                else
                    MRIDATA.Data3Dnew = [];
            end
            
            
            Data2D = [];

            obj.CreateSequence(0); % Create the normal gradient echo sequence
            obj.Sequence2String;

            fname = 'TempBatch.bat';
            obj.WriteBatchFile(fname);
            [status,result] = system(fullfile(obj.path_file,fname));

            if status == 0 && ~isempty(result)
                C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                res = C{:};
                obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
            end

            DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
            cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
            d = reshape(cDataBatch,nPoints,1);

            Data2D = d;
            Data3D = Data2D;
            MRIDATA.Data3Dnew(end+1).Fid3D = Data3D;

            if length(obj.cData) ~= nPoints*nPhases*nSlices;
                obj.cData = zeros(obj.NAverages,nPoints*nPhases*nSlices);
            end
            if obj.averaging
                Nav = obj.NAverages;
            else
                Nav = 1;
            end
            index = mod(iAcq-1,Nav)+1;
            if obj.autoPhasing
                pt = max(Data3D(:));
                Data3D = Data3D.*exp(complex(0,-angle(pt)));
            end
            obj.cData(index,1:length(Data3D(:))) = Data3D(:);
            obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
            obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
            obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
            acq_time = obj.GetParameter('ACQUISITIONTIME');
            obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*acq_time/nPhases;
            obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time/nPhases;
        
            MRIDATA.fid = obj.fidData;
        end
    end
    
end

