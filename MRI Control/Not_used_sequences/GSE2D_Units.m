classdef GSE2D_Units < MRI_BlankSequence
    % Gradient Echo sequence so that the input parameters are user
    % parameters: FOV in each axes, resolution, etc.
    
    %  Elena Diaz Caballero
    
    % 14.07.2019 - Mods from Jos� Miguel Algar�n.
    
    properties
        StartSlice;
        EndSlice;
        PhasesPerBatch = 20;
        PhasesinThisBatch = 1;
        StartPhase;
        EndPhase;
        slice_ampl;
        slice_ampl_re;
        acq_time;
        PhaseTime;
        RepDelay;
        RO_strength_rep;
        RO_strength_dep;
        SL_strength_dep;
        SL_strength_rep;
        NRO;
        NPH;
        NSL;
        Axis1;
        Axis2;
        Axis3;
        CAxis1;
        CAxis2;
        CAxis3;
        rdspeed_mult = 1;
        nPhasesBatch;
        NP_calib = 1;
        ROmax = 0;
        CalData;
        FOV;
        gam = 42.57E6; %gyromagnetic ratio, 42.57 MHz/T
        gradientList;
        nSteps = 5;                     % Steps for gradient ramp.
        sliceRephasingTime;
        sliceDelayTime;
        gapTime1;
        gapTime2;
        sincZeroCrossings = 1;        % Number of zero crossing for the RF sinc pulse
        maxBatchTime = 600;             % Maximun batch time in seconds
    end
    methods (Access=private)
        function CreateParameters(obj)
            
            % Input parameters
            obj.CreateOneParameter('TE','TE time','ms',1*MyUnits.ms);
            obj.CreateOneParameter('TR','TR time','ms',50*MyUnits.ms);
            obj.CreateOneParameter('FOVRO','FOV Readout','mm',10*MyUnits.mm);
            obj.CreateOneParameter('FOVPH','FOV Phase','mm',10*MyUnits.mm);
            obj.CreateOneParameter('FOVSL','FOV Slice','mm',2*MyUnits.mm);
            obj.CreateOneParameter('NPOINTS','N Readout','',10);
            obj.CreateOneParameter('NPHASES','N Phase','',10);
            obj.CreateOneParameter('ECHODELAY','Echo Delay','us',0.1*MyUnits.us);
            obj.CreateOneParameter('ACQTIME','Acquisition Time','us',400*MyUnits.us);
            obj.CreateOneParameter('GRADIENTDELAY','Gradient Delay','us',55*MyUnits.us);
            obj.CreateOneParameter('REPETITIONDELAY','Repetition Delay','ms',50*MyUnits.ms);
            obj.CreateOneParameter('SLICEPOSITION','Slice Position','mm',0*MyUnits.mm);
            obj.CreateOneParameter('SLICEDELAY','Slice Delay','us',0*MyUnits.us);
            obj.CreateOneParameter('JUMPINGFACTOR','Jumping Factor','',1);
            obj.CreateOneParameter('DELTARO','RO Deviation','mm',0);
            obj.CreateOneParameter('DELTAPH','PH Deviation','mm',0);
            obj.CreateOneParameter('DELTASL','SL Deviation','mm',0);
            
            % Output parameters
            OutputParameters = {...
                'MAXPH','RO_STRENGTH','MAXSL'...
                };
            obj.OutputParNames = [containers.Map(obj.OutputParNames.keys(),obj.OutputParNames.values());containers.Map(OutputParameters,...
                {...
                'Start Phase', 'Readout amplitude', 'Start Slice'...
                })];
            obj.OutputParValues = [containers.Map(obj.OutputParValues.keys(),obj.OutputParValues.values());containers.Map(OutputParameters,...
                {...
                NaN,NaN,NaN...
                })];
            obj.OutputParUnits = [containers.Map(obj.OutputParUnits.keys(),obj.OutputParUnits.values());containers.Map(OutputParameters,...
                {...
                '','',''...
                })];
            for k=1:length(obj.OutputParameters)
                OutputParameters(strcmp(obj.OutputParameters{k},OutputParameters)) = [];
            end
            obj.OutputParameters = [obj.OutputParameters,OutputParameters];
            
            % Hide parameters
%             obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
%                 'BLANKINGDELAY',...
%                 'TRANSIENTTIME','COILRISETIME','SPOILERAMP',...
%                 'SPOILERTIME','SPECTRALWIDTH','REPETITIONDELAY',...
%                 'NSLICES'};
            
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
                'BLANKINGDELAY',...
                'TRANSIENTTIME','SPOILERAMP',...
                'SPOILERTIME','SPECTRALWIDTH','REPETITIONDELAY',...
                'NSLICES'};


            % Rearange parameters
            obj.MoveParameter(14,15);
            obj.MoveParameter(17,25);
            obj.MoveParameter(17,25);
            obj.MoveParameter(5,21);
            obj.MoveParameter(5,22);
            obj.MoveParameter(15,12);
            obj.MoveParameter(16,13);
            obj.MoveParameter(26,9);
            obj.MoveParameter(24,12);
            obj.MoveParameter(27,12);
            obj.MoveParameter(29,23);
            obj.MoveParameter(30,13);
            obj.MoveParameter(32,25);
            obj.MoveParameter(33,26);
            obj.MoveParameter(34,27);

        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = GSE2D_Units(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'GSE2D Real units';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
            
            obj.SetParameter('RFAMPLITUDE',0.01);
            obj.SetParameter('BLANKINGDELAY',200*MyUnits.us);
            obj.SetParameter('TRANSIENTTIME',0.1*MyUnits.us);
            obj.SetParameter('COILRISETIME',100*MyUnits.us);
            obj.SetParameter('NPOINTS',10);
            obj.SetParameter('NPHASES',10);
            obj.SetParameter('NSLICES',1);
            
            % This value is empherical and picked based on not creating eddy currents
            obj.SetParameter('FREQUENCY',11*MyUnits.MHz);
            obj.SetParameterMin('FREQUENCY',10*MyUnits.MHz);
            obj.SetParameterMax('FREQUENCY',15*MyUnits.MHz);
            
            % Grab the calibration data if there
            try
                obj.CalData = GradientCalibrationData();
            catch
                obj.CalData.X_TD = 1;
                obj.CalData.Y_TD = 1;
                obj.CalData.Z_TD = 1;
                obj.CalData.X_TV = 1;
                obj.CalData.Y_TV = 1;
                obj.CalData.Z_TV = 1;
            end
            
        end
        function UpdateTETR(obj)
            obj.TE = obj.GetParameter('TE');
            obj.TR = obj.GetParameter('TR');
            [~,~,~,ENSL,ENPH,~,~] = obj.ConfigureGradients(0);
            obj.NPH = obj.GetParameter('NPHASES')*ENPH+1*~ENPH;
            obj.NSL = obj.GetParameter('NSLICES')*ENSL+1*~ENSL;

            obj.TTotal = obj.TR * obj.GetParameter('NSCANS')* obj.NSL * obj.NPH;
        end
        
        
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************



        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence,
            %         "1", slice gradient delay adjustment,
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            
            % Configure gradients and axes
            [~,~,readout,slice,~,rf180en,~] = obj.ConfigureGradients(mode);
            
            % Set the number of lines to be read
            nMisses = obj.GetParameter('JUMPINGFACTOR');
            nPhases = obj.nPhasesBatch*~mode+mode+nMisses;
            
            % Calculate the number of line reads
            obj.nline_reads=nPhases;
            
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions = (11+10*obj.nSteps)*nPhases;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            
            % Readout gradient amplitudes
            dephasing_readout = obj.RO_strength_dep*readout*~mode;
            rephasing_readout = obj.RO_strength_rep*readout*~mode;

            % Slice slection gradient amplitudes
            dephasing_slice = obj.SL_strength_dep*(slice+mode);
            rephasing_slice = obj.SL_strength_rep*(slice+mode);
            
            
            obj.ROmax = abs(rephasing_readout);
            
            % Load timing from RawData;
            blkTime         = obj.GetParameter('BLANKINGDELAY')*1e6;
            pulseTime       = obj.GetParameter('PULSETIME')*1e6;
            deadTime        = obj.GetParameter('TRANSIENTTIME')*1e6;
            crTime          = obj.GetParameter('COILRISETIME')*1e6;
            sliceRephaTime  = obj.sliceRephasingTime*1e6;
            sliceDelaTime   = obj.sliceDelayTime*1e6;
            gapTimeBefore   = obj.gapTime1*1e6;
            gapTimeAfter    = obj.gapTime2*1e6;
            phaseTime       = obj.PhaseTime*1e6;
            echoDelayTime   = obj.GetParameter('ECHODELAY')*1e6;
            gradDelayTime   = obj.GetParameter('GRADIENTDELAY')*1e6;
            acqTime         = obj.GetParameter('ACQTIME')*1e6;
            repDelayTime    = obj.RepDelay*1e6;
            
            % BEGIN THE PULSE PROGRAMMING
            iIns = 1;
            for phasei = 1:obj.nline_reads
                
                % Get the amplitude for the current line
                if(phasei>nMisses && mode==0)
                    phase_amplitude = obj.gradientList(phasei-nMisses);
                else
                    phase_amplitude = 0;
                end
                
                % SHIMMING in all gradients (3 instructions)
                iIns = obj.PulseGradient(iIns,shimR,RR,0.1);
                iIns = obj.PulseGradient(iIns,shimP,PP,0.1);
                iIns = obj.PulseGradient(iIns,shimS,SS,0.1);
                
                % Activate dephasing slice gradient (nSteps instructions)
                for ii = 1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimS+dephasing_slice*(phasei>nMisses)*ii/obj.nSteps,SS,crTime/obj.nSteps);
                end
                obj.DELAYs(iIns-1) = crTime/obj.nSteps+gradDelayTime;
                
                % 90� RF pulse (3 instruction)
                iIns = obj.CreatePulse(iIns,0,blkTime,pulseTime,deadTime);
                
                
                % Deactivate dephasing slice gradient (nSteps instructions)
                for ii = 1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimS+dephasing_slice*(phasei>nMisses)*(1-ii/obj.nSteps),SS,crTime/obj.nSteps);
                end
                
                % Activate rephasing slice gradient (nSteps instructions)
                for ii = 1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimS+rephasing_slice*(phasei>nMisses)*ii/obj.nSteps,SS,crTime/obj.nSteps);
                end
                if(pulseTime>=crTime-2*gradDelayTime)
                    obj.DELAYs(iIns-1) = crTime/obj.nSteps+sliceRephaTime+sliceDelaTime;
                else
                    obj.DELAYs(iIns-1) = crTime/obj.nSteps;
                end
                
                % Deactivate rephasing slice gradient (nSteps instructions)
                for ii = 1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimS+rephasing_slice*(phasei>nMisses)*(1-ii/obj.nSteps),SS,crTime/obj.nSteps);
                end
                obj.DELAYs(iIns-1) = crTime/obj.nSteps;
                
                % 180� RF pulse (3 instructions)
                iIns = obj.CreatePulse(iIns,0,gapTimeBefore,2*pulseTime,...
                    gapTimeAfter-echoDelayTime);
                obj.ENVELOPEs(iIns-2) = 1;
                obj.TXs(iIns-2) = rf180en;
                
                % Activate dephasing gradients - readout, phase
                % (2*nSteps instructions)
                for ii = 1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimR+dephasing_readout*(phasei>nMisses)*ii/obj.nSteps,RR,0.1);
                    iIns = obj.PulseGradient(iIns,shimP+phase_amplitude*ii/obj.nSteps,PP,crTime/obj.nSteps-0.1);
                end
                obj.DELAYs(iIns-1) = crTime/obj.nSteps-0.2+phaseTime;
                
                % Deactivate dephasing gradients - readout, phase
                % (2*nSteps instructions)
                for ii = 1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimR+dephasing_readout*(phasei>nMisses)*(1-ii/obj.nSteps),RR,0.1);
                    iIns = obj.PulseGradient(iIns,shimP+phase_amplitude*(1-ii/obj.nSteps),PP,crTime/obj.nSteps-0.1);
                end
                
                % Activate rephasing gradient - readout (nSteps instructions)
                for ii = 1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimR+rephasing_readout*(phasei>nMisses)*ii/obj.nSteps,RR,crTime/obj.nSteps);
                end
                obj.DELAYs(iIns-1) = crTime/obj.nSteps+gradDelayTime+echoDelayTime;
                %obj.PhResets(iIns-1) = 1;
            
                % Acquisition (1 instruction)
                iIns = obj.Acquisition(iIns,acqTime);
                
                % Deactivate rephasing gradient - readout (nSteps instructions)
                for ii = 1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimR+rephasing_readout*(phasei>nMisses)*(1-ii/obj.nSteps),RR,crTime/obj.nSteps);
                end
                
                % Repetition delay (1 instruction)
                iIns = obj.RepetitionDelay(iIns,0,repDelayTime);
                
            end % end for nPhases
            %             end %end for slicei
            
        end % end create gradient echo pulse sequence

        
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************


        function [status,result] = Run(obj,iAcq)
            clc
            
            global MRIDATA
            MRIDATA.ADC = [];
            
            global rawData
            rawData = [];
            
            obj.SetParameter('BLANKINGDELAY',100e-6);
            
            % Check which gradients are switched on
            [~,~,ENRO,ENSL,ENPH,~,~] = obj.ConfigureGradients(0);
            
            % Field of View
            FOVRO = obj.GetParameter('FOVRO');
            FOVPH = obj.GetParameter('FOVPH');
            FOVSL = obj.GetParameter('FOVSL');
            POSSL = obj.GetParameter('SLICEPOSITION');
            DELRO = obj.GetParameter('DELTARO');
            DELPH = obj.GetParameter('DELTAPH');
            DELSL = obj.GetParameter('DELTASL');
            nScans = obj.GetParameter('NSCANS');
            rawData.inputs.Seq = obj.SequenceName;
            rawData.inputs.NEX = nScans;
            rawData.inputs.FOV = [FOVRO,FOVPH,FOVSL];
            rawData.inputs.POS = POSSL;
            rawData.inputs.DEV = [DELRO,DELPH,DELSL];
            
            % Matrix size
            obj.NRO = obj.GetParameter('NPOINTS');
            obj.NPH = obj.GetParameter('NPHASES')*ENPH+1*~ENPH;
            obj.NSL = obj.GetParameter('NSLICES')*ENSL+1*~ENSL;
            rawData.inputs.axis = strcat(obj.GetParameter('READOUT'),...
                obj.GetParameter('PHASE'),obj.GetParameter('SLICE'));
            rawData.inputs.axisEnable = [ENRO,ENPH,ENSL];
            rawData.inputs.matrix = [obj.NRO,obj.NPH,obj.NSL];
            
            % Resolution
            rawData.auxiliar.resolution =...
                [FOVRO/(obj.NRO-1),FOVPH/(obj.NPH-1),FOVSL];
            
            % Parameters for image weighting
            TE = obj.GetParameter('TE');
            TR = obj.GetParameter('TR');
            rawData.inputs.TE = TE;
            rawData.inputs.TR = TR;
            
            % RF parameters
            RF = obj.GetParameter('PULSETIME');
            obj.acq_time = obj.GetParameter('ACQTIME');
            BW = obj.NRO/(obj.acq_time);
            obj.SetParameter('SPECTRALWIDTH',BW);
            rawData.inputs.pulseTime = RF;
            rawData.inputs.amplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.frequency = obj.GetParameter('FREQUENCY');
            rawData.inputs.acqTime = obj.acq_time;
            rawData.inputs.rfShape = obj.RF_Shape;
            rawData.auxiliar.bandwidth = BW;
            
            % Miscellaneous
            CR = obj.GetParameter('COILRISETIME'); %CR = 50E-6;
            ED = obj.GetParameter('ECHODELAY');
            GD = obj.GetParameter('GRADIENTDELAY');
            nMisses = obj.GetParameter('JUMPINGFACTOR');
            rawData.auxiliar.coilRiseTime = CR;
            rawData.inputs.echoDelay = ED;
            rawData.inputs.gradientDelay = GD;
            rawData.inputs.jumpingFactor = nMisses;
            if(RF<CR-2*GD)
                warndlg('The RF pulse time needs to be longer','RF time error')
                return
            end
            % Dephasing time (Phase and Readout)
            obj.PhaseTime = 0.5*obj.acq_time;
            rawData.auxiliar.phaseTime = obj.PhaseTime;
            
            % Rephasing time (Slice)
            obj.sliceRephasingTime = 0.5*RF+GD-0.5*CR;
            obj.sliceDelayTime = obj.GetParameter('SLICEDELAY');
            rawData.auxiliar.sliceRephasingTime = obj.sliceRephasingTime;
            rawData.auxiliar.sliceDelayTime = obj.sliceDelayTime;
            
            % Repetition delay
            tprov = RF+8*CR+obj.PhaseTime+ED+obj.acq_time;
            obj.RepDelay  = TR-tprov;
            rawData.auxiliar.repetitionDelay = obj.RepDelay;
            % check if the phase time can be possible given the selected TE, if not adjust TE as needed
            if obj.RepDelay <=0
                warndlg('The repetition delay is negative! Try making your TR longer','TR error')
                fprintf('TR should be longer than %1.3f ms \n',tprov*1e3)
                return
            end
            clear tprov
            obj.SetParameter('REPETITIONDELAY',obj.RepDelay);
            
            % Gap time
            obj.gapTime1 = TE/2-3/2*RF-3*CR-obj.sliceRephasingTime-obj.sliceDelayTime;
            obj.gapTime2 = TE/2-3*CR-RF-obj.PhaseTime-1/2*obj.acq_time;
            if(obj.gapTime1<0)
                warndlg('Gap time between slice gradient and 180� pulse is negative! Try making your TE longer','Echo error')
                fprintf('TE should be longer than %1.3f ms \n',...
                    (3*RF+6*CR+2*obj.sliceRephasingTime+2*obj.sliceDelaytime)*1e3);
                return;
            end
            if(obj.gapTime2-ED<0)
                warndlg('Gap time between 180� pulse and dephasing gradient is negative! Try making your TE longer','Echo error')
                fprintf('TE should be longer than %1.3f ms \n',...
                    (3*RF+6*CR+2*obj.sliceRephasingTime+2*obj.sliceDelaytime)*1e3);
                return;
            end
            
            % Save in rawData the sequence timing
            rawData.timing.pulseTime = RF;
            rawData.timing.coilRiseTime = obj.GetParameter('COILRISETIME');
            rawData.timing.sliceRephasingTime = obj.sliceRephasingTime;
            rawData.timing.sliceDelayTime = obj.sliceDelayTime;
            rawData.timing.gapTimeBefore = obj.gapTime1;
            rawData.timing.gapTimeAfter = obj.gapTime2;
            rawData.timing.phaseTime = obj.PhaseTime;
            rawData.timing.echoDelayTime = obj.GetParameter('ECHODELAY');
            rawData.timing.gradientDelayTime = obj.GetParameter('GRADIENTDELAY');
            rawData.timing.acquisitionTime = obj.acq_time;
            rawData.timing.repDelayTime = obj.RepDelay;
            
            % Readout gradients
            obj.RO_strength_rep = BW ./ ( obj.gam * FOVRO )*ENRO; % Eq. (8.13) big book
            obj.RO_strength_dep = -obj.RO_strength_rep*ENRO; % We need to take into account the coil rise time
            rawData.auxiliar.dephasingGradient = obj.RO_strength_dep;
            rawData.auxiliar.rephasingGradient = obj.RO_strength_rep;
            if abs(obj.RO_strength_dep) >= 0.2
                warndlg('The readout dephasing gradient is stronger than 0.2 T/m.','Readout error')
                return
            end
            if abs(obj.RO_strength_rep) >= 0.2
                warndlg('The readout rephasing gradient is stronger than 0.2 T/m.','Readout error')
                return
            end
            
            % Slice gradient
            obj.SL_strength_dep = 7.2/(42.6d6*FOVSL*RF)*ENSL;               % 7.2/RF is the SINC bandwidth (experimental factor)
            if(RF>=CR-2*GD)
                obj.SL_strength_rep = -obj.SL_strength_dep*ENSL;
            else
                obj.SL_strength_rep = -0.5*obj.SL_strength_dep*(RF+CR)/CR*ENSL;
            end
            rawData.auxiliar.sliceDephasingGradient = obj.SL_strength_dep;
            rawData.auxiliar.sliceRephasingGradient = obj.SL_strength_rep;
            if abs(obj.SL_strength_dep) >= 0.39
                warndlg('The slice dephasing gradient is stronger than 0.4 T/m. Try making the RF pulse time longer or making the slice thicker.','Slice error')
                fprintf('Warning: the required slice gradient is %1.2f T/m \n \n',obj.SL_strength_dep);
                return
            end
            if abs(obj.SL_strength_rep) >= 0.39
                warndlg('The slice rephasing gradient is stronger than 0.4 T/m. Try making the RF pulse time shorter.','Slice error')
                return
            end
            
            % Phase gradient
            PH_strength_max = 1/2*(obj.NPH-1)/(obj.gam*(obj.PhaseTime+CR)*FOVPH); % We need to take into account for gradient ramp
            phaseGradients = linspace(-PH_strength_max,PH_strength_max,obj.NPH*ENPH+1*~ENPH)'*ENPH;
            rawData.auxiliar.phaseGradient = PH_strength_max*ENPH;
            rawData.auxiliar.gradientList = phaseGradients;
            if abs(PH_strength_max) >= 0.2
                warndlg('The phase dephasing gradient is stronger than 0.2 T/m.','Phase error')
                return
            end
            
            % Set the gradient amplitudes in arbitrary units
            c = obj.ReorganizeCalibrationData();
            gradientsArray = rawData.auxiliar.gradientList/c(2);
            obj.RO_strength_rep = obj.RO_strength_rep/c(1);
            obj.RO_strength_dep = obj.RO_strength_dep/c(1);
            obj.SL_strength_rep = obj.SL_strength_rep/c(3);
            obj.SL_strength_dep = obj.SL_strength_dep/c(3);

            % Frequency offset to do slice selection
            obj.SliceFreqOffset = 42.6d6*obj.SL_strength_dep*POSSL*ENSL;
            rawData.auxiliar.freqOffsetSL = obj.SliceFreqOffset;
            
            nPoints = obj.NRO;
            nSlices = obj.NSL;
            nPhases = obj.NPH;
            
%             obj.SetParameter('MAXPH',PH_strength_max);
%             obj.SetParameter('RO_STRENGTH',obj.RO_strength_rep);
%             obj.SetParameter('MAXSL',SL_strength_max);
        
            
            Data3D = zeros(nPoints,obj.NPH,obj.NSL);
            
            
            if isfield(MRIDATA,'Data3Dnew')
                if ( isequal(size(MRIDATA.Data3Dnew),[obj.NRO, obj.NPH, obj.NSL, obj.NAverages])) || (iAcq == 1)
                    MRIDATA.Data3Dnew = [];
                end
            else
                MRIDATA.Data3Dnew = [];
            end
            
            % I will set up the phases per batch to the maximun it will
            % depend on the number of orders per phase(replace 65 by the number of
            % orders per phase)
            % I will set the number of elements in the 'for' to reduce the
            % number of batches to the minimun. I do not care at all about
            % the slice or phase direction. I have the gradientList, so we
            % just need to pass the gradientList over the Create sequence
            % as few times as possible. In this way we go out of the
            % stationary state as few times as possible.
            obj.PhasesPerBatch = floor(min([1024/(11+10*obj.nSteps),16000/nPoints]))-nMisses;
            nBatches = ceil(nPhases*nSlices/obj.PhasesPerBatch);
            cn = nBatches;
            count = 0;
            if nBatches>1
                h_waitbar = waitbar(count/cn,'Please wait (por favor espere)...');
            end
            
            Data2D = [];
            missedData = [];
            
            nSubDiv = ceil(obj.nPhasesBatch*TR*nScans/obj.maxBatchTime);
            for iBatch = 1:nBatches
                fprintf('Batch number %1.0f/%1.0f \n \n',iBatch,nBatches)
                
                % Get the appropiate axis
                if(iBatch<nBatches)
                    obj.gradientList = gradientsArray((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch);
                else
                    obj.gradientList = gradientsArray((iBatch-1)*obj.PhasesPerBatch+1:end);
                end
                obj.nPhasesBatch = size(obj.gradientList,1);
                
                cDataBatch = zeros(nPoints*obj.nPhasesBatch,nSubDiv);
                missedLines = zeros(nPoints*nMisses,nSubDiv);
                for scan=1:nSubDiv
                
                    % Get central frequency
%                     fprintf('Calibrating frequency... \n')
%                     [~,centralFrequency] = obj.GetFIDParameters;
%                     rawData.auxiliar.freqList(iBatch) = centralFrequency;
%                     fprintf('Frequency = %1.4f MHz \n \n',centralFrequency*1d-6)
%                     obj.SetParameter('FREQUENCY',centralFrequency);
                    
                    % Get the actual slice gradient delay
                    if(ENSL==1)
                        fprintf('Calibrating slice delay... \n')
%                         obj.SliceGradientDelayCalibration;
                        fprintf('Slice delay = %1.0f us \n \n',obj.GetParameter('SLICEDELAY')*1e6);
                        rawData.auxiliar.sliceDelay(iBatch) = obj.GetParameter('SLICEDELAY');
                    end
                    
                    % NOW RUN ACTUAL ACQUISTION
                    fprintf('Acquiring data \n')
                    obj.SetParameter('NSCANS',floor(nScans/nSubDiv))
                    obj.CreateSequence(0); % Create the normal gradient echo sequence
                    obj.Sequence2String;
                    fname = 'TempBatch.bat';
                    obj.WriteBatchFile(fname);
                    [status,result] = system(fullfile(obj.path_file,fname));
                    if status == 0 && ~isempty(result)
                        fprintf('Data acquired succesfully \n \n')
                        C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                        res = C{:};
                        obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                        obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                        %                     obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                        obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                        obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                    else
                        warndlg('Batch aborted!','Warning')
                        fprintf('Data fault')
                        return
                    end
                    DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                    cDataBatch(:,scan) = complex(DataBatch(nPoints*nMisses+1:end,1),DataBatch(nPoints*nMisses+1:end,2));
                    missedLines(:,scan) = complex(DataBatch(1:nPoints*nMisses,1),DataBatch(1:nPoints*nMisses,2));
                end
                cDataBatch = mean(cDataBatch,2);
                missedLines = mean(missedLines,2);
                nPhasesInBatch = length(cDataBatch)/nPoints;
                d = reshape(cDataBatch,nPoints,nPhasesInBatch);
                Data2D = cat(2,Data2D,d);
                missedData = cat(2,missedData,missedLines);
                
                count = count+1;
                if nBatches>1
                    waitbar(count/cn,h_waitbar,sprintf('Por favor espere..... %5.2f %%',100*count/cn));
                end
            end
                
            Data3D = squeeze(reshape(Data2D,nPoints,nPhases,nSlices));
            Data3D_use = Data3D(:);
                
            if obj.averaging
                Nav = obj.NAverages;
            else
                Nav = 1;
            end
            index = mod(iAcq-1,Nav)+1;
            if obj.autoPhasing
                %                    pt = max(Data3D(:,ceil(size(Data3D,2)/2),ceil(size(Data3D,3)/2)));
                pt = max(Data3D_use(:));
                Data3D_use = Data3D_use.*exp(complex(0,-angle(pt)));
            end
%             obj.cData = [];
%             obj.cData(index,1:length(Data3D_use(:))) = Data3D_use(:);
%             obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
%             obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
%             %    obj.fData1D = fftshift(ifft(fftshift(obj.cDataAv)));
%             obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
% %             obj.acq_time = obj.GetParameter('ACQUISITIONTIME');
%             obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*obj.acq_time/nPhases;
%             obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/obj.acq_time/nPhases;
            
            if nBatches>1
                delete(h_waitbar);
            end
            
            % Clean obj.fidData
            if(iAcq==1)
                obj.fidData = zeros(obj.NRO*obj.NPH*obj.NSL,1);
            end
            
            % k-space points
            kMax = 0.5./rawData.auxiliar.resolution;
            kPointsX = linspace(-kMax(1),kMax(1),nPoints);
            kPointsY = linspace(-kMax(2),kMax(2),nPhases);
            kPointsZ = linspace(-kMax(3),kMax(3),nSlices);
            [kPointsX,kPointsY,kPointsZ] = meshgrid(kPointsX,kPointsY,kPointsZ);
            kPointsX = permute(kPointsX,[2,1]);
            kPointsY = permute(kPointsY,[2,1]);
            kPointsZ = permute(kPointsZ,[2,1]);
            kPoints = [kPointsX(:),kPointsY(:),kPointsZ(:)];
            phase = exp(-2*pi*1i*(DELRO*kPointsX(:)+DELPH*kPointsY(:)+DELSL*kPointsZ(:)));
            kPointsS = Data3D(:).*phase;
            if(obj.autoPhasing)
                kPointsS = kPointsS*exp(-1i*angle(kPoints(ceil(end/2))));
            end
            obj.fidData(:,iAcq) = kPointsS;
            kPoints(:,4:3+iAcq) = obj.fidData(:,1:iAcq);
            clear kPointsX kPointsY kPointsZ kPointsS
            rawData.outputs.kSpace = kPoints;
            rawData.outputs.missed = missedData;
            kPoints = reshape(mean(kPoints(:,3:end),2),obj.NRO,obj.NPH,obj.NSL);
            imagen = abs(ifftshift(ifftn(kPoints)));
            rawData.outputs.imagen = imagen;
            
            % Save rawData
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
        

%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************


        function SliceGradientDelayCalibration(obj)
            sliceDelay = obj.GetParameter('SLICEDELAY')*1e6;
            n = 0;
            aa = sliceDelay-10;
            bb = sliceDelay+10;
            nPoints = obj.GetParameter('NPOINTS');
            nMisses = obj.GetParameter('JUMPINGFACTOR');
            nScans = obj.GetParameter('NSCANS');
            nPhases = obj.nPhasesBatch;
            rfAmp = obj.GetParameter('RFAMPLITUDE');
            repDelay = obj.RepDelay;
            pulseTime = obj.GetParameter('PULSETIME')*1e6;
            obj.SetParameter('RFAMPLITUDE',0.13*65/pulseTime);
            obj.RepDelay = 1;
            obj.SetParameter('NSCANS',1);
            m = zeros(nPoints,11);
            obj.nPhasesBatch = 1;
            for k=aa:1:bb
                n = n+1;
                obj.SetParameter('SLICEDELAY',k*1e-6)
                
                % NOW RUN ACTUAL ACQUISTION
                obj.CreateSequence(1); % Create the normal gradient echo sequence
                obj.Sequence2String;
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                [status,result] = system(fullfile(obj.path_file,fname));
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    %                     obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(nPoints*nMisses+1:end,1),DataBatch(nPoints*nMisses+1:end,2));
                m(:,n)=cDataBatch;
            end
            [~,pos] = max(sum(abs(m),1));
            obj.SetParameter('NSCANS',nScans);
            obj.RepDelay = repDelay;
            obj.SetParameter('RFAMPLITUDE',rfAmp);
            obj.SetParameter('SLICEDELAY',(aa+pos-1)*MyUnits.us);
            obj.nPhasesBatch = nPhases;
        end

        
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************
%**************************************************************************


        function RunCalibrationAxisGeneration( obj )
            % this will run the calibration on the actively stored data
            global MRIDATA
            
            obj.CAxis1 = [];
            obj.CAxis2 = [];
            obj.CAxis3 = [];
            
            % keep in mind that this is for only 2 axis at the moment
            % first axis is the readout direction, second is the phase
            % direction
            for iADC = 1:numel(MRIDATA.ADC)
                
                    Full_TriggerAxis = MRIDATA.ADC(iADC).CMatrix1(1,:)';
                    Full_DAxis1 = MRIDATA.ADC(iADC).CMatrix1(2,:)';
                    Full_DAxis2 = MRIDATA.ADC(iADC).CMatrix1(3,:)';
                    Full_DAxis3 = MRIDATA.ADC(iADC).CMatrix1(4,:)';
                    
                    % pulse parameters
                    
                    [AcqT, start_measure, end_measure, after_RF, total_measure_time] = obj.CalcTiming;
                                       
                    nP = MRIDATA.ADC(iADC).nPhases;
                    NP = obj.GetParameter('NPOINTS');
                    
                    % measurement parameters
                    RdSpeed = (40E6)/obj.rdspeed_mult;
                    dt = 1/RdSpeed;

                    % measure inds
                    start_ind = floor(start_measure/dt);
                    end_ind   = obj.NP_calib;
                    after_RF_ind = floor(after_RF/dt);
                    
                    % find the ind when the trigger is below a certain threshold that is
                    % considered as '0'
                    trigthresh_up   = 1E-1;
                    Full_TriggerAxis ( Full_TriggerAxis > trigthresh_up )  =  1;
                    Full_TriggerAxis = round(Full_TriggerAxis);
                    
                    % starting offset
                    DAxis1Sum = zeros(NP,nP);
                    DAxis2Sum = zeros(NP,nP);
                    DAxis3Sum = zeros(NP,nP);
                    
%                     dfilter = fdesign.lowpass('N,Fc',1,1,100);
%                     Hd = design(dfilter);
                    
                    for ip = 1:nP
                        % Rewrite the axes
                        TriggerAxis = Full_TriggerAxis(1+(ip-1)*obj.NP_calib:ip*obj.NP_calib);
                        DAxis1 = Full_DAxis1(1+(ip-1)*obj.NP_calib:ip*obj.NP_calib);
                        DAxis2 = Full_DAxis2(1+(ip-1)*obj.NP_calib:ip*obj.NP_calib);
                        DAxis3 = Full_DAxis3(1+(ip-1)*obj.NP_calib:ip*obj.NP_calib);
                        
                        
                        % find the ind where the start is and then where the end is
                        inds = find( TriggerAxis == 1, 1, 'first' );
                        inde = find( TriggerAxis(inds:end) == 0, 1, 'first' )+inds-1;
                        
                        % Grab the 'zero' amount - remove the shimming bias
                        d1shim = median( DAxis1(inds:inde) );
                        d2shim = median( DAxis2(inds:inde) );
                        d3shim = 0;%median( DAxis3(inds:inde) );
                        
                        
                        % grab data and smooth it
%                         SelectedData1 = filter( Hd, DAxis1(inde+after_RF_ind:inde+end_ind)-d1shim );
                        SelectedData1 = DAxis1(inde+after_RF_ind:end)-d1shim;
%                         SelectedData1 = smooth( DAxis1(inde+after_RF_ind:inde+end_ind)-d1shim,11, 'rloess' );
                        % now sum the data to determine where in KSpace you are
                        DAxis1SumFull = obj.gam * ( cumsum( SelectedData1 * dt ) );
                        DAxis1SumSelect = DAxis1SumFull(start_ind-after_RF_ind:end);
                        D1interp = interp1(linspace(1,numel(DAxis1SumSelect),numel(DAxis1SumSelect)),...
                                          DAxis1SumSelect, linspace(1,numel(DAxis1SumSelect),NP),'pchip');
                        
                        DAxis1Sum(:,ip) = D1interp;
                        
                        % grab data and smooth it
%                         SelectedData2 = filter(Hd, DAxis2(inde+after_RF_ind:inde+end_ind)-d2shim );
                        SelectedData2 = DAxis2(inde+after_RF_ind:end)-d2shim;
%                         SelectedData2 = smooth( DAxis2(inde+after_RF_ind:inde+end_ind)-d2shim,11, 'rloess' );
                        % now sum the data to determine where in KSpace you are
                        DAxis2SumFull = obj.gam * ( cumsum( SelectedData2 * dt ));
                        DAxis2SumSelect = DAxis2SumFull(start_ind-after_RF_ind:end);
                        D2interp = interp1(linspace(1,numel(DAxis2SumSelect),numel(DAxis2SumSelect)),...
                                          DAxis2SumSelect, linspace(1,numel(DAxis2SumSelect),NP),'pchip');
                        
                        DAxis2Sum(:,ip) = D2interp;
                        
                        % grab data and smooth it
% %                         SelectedData3 = filter(Hd, DAxis3(inde+after_RF_ind:inde+end_ind)-d3shim );
                        SelectedData3 = DAxis3(inde+after_RF_ind:end)-d3shim;
%                         SelectedData3 = smooth( DAxis3(inde+after_RF_ind:inde+end_ind)-d3shim,11, 'rloess' );
                        % now sum the data to determine where in KSpace you are
                        DAxis3SumFull = obj.gam * ( cumsum( SelectedData3 * dt ));
                        DAxis3SumSelect = DAxis3SumFull(start_ind-after_RF_ind:end);
                        D3interp = interp1(linspace(1,numel(DAxis3SumSelect),numel(DAxis3SumSelect)),...
                                          DAxis3SumSelect, linspace(1,numel(DAxis3SumSelect),NP),'pchip');
                        
                        DAxis3Sum(:,ip) = D3interp;
                        
                    end
                    
                        obj.CAxis1 = cat(2, obj.CAxis1, DAxis1Sum);
                        obj.CAxis2 = cat(2, obj.CAxis2, DAxis2Sum);
                        obj.CAxis3 = cat(2, obj.CAxis3, DAxis3Sum);
                
            end
            
%             reform axis values
            obj.CAxis1 = obj.CAxis1(:);
            obj.CAxis2 = obj.CAxis2(:);
            obj.CAxis3 = obj.CAxis3(:);
            
        end
        function [AcqT, start_measure, end_measure, after_RF, total_measure_time] = CalcTiming( obj )
            
            % pulse parameters
                    BD = obj.GetParameter('BLANKINGDELAY');
                    RF = obj.GetParameter('PULSETIME');
                    TT = obj.GetParameter('TRANSIENTTIME');
                    AddP = 0.1E-6;
%                     PT = obj.GetParameter('PHASETIME');
                    CR = obj.GetParameter('COILRISETIME');
                    ED = obj.GetParameter('ECHODELAY');
                    NP = obj.GetParameter('NPOINTS');
                    SW = obj.GetParameter('SPECTRALWIDTH');
                    AcqT = NP/SW;
                    

                    % specify start and end of reading
                    after_RF = 0;%1*BD+RF;
                    start_measure = after_RF+TT+2*AddP+2*obj.PhaseTime+3*CR+ED;
                    end_measure   = start_measure+AcqT;
                    total_measure_time = end_measure+BD+RF;

        end
    end
end

