classdef ARTIFACTTEST < MRI_BlankSequence6   
    properties
        maxRepetitionsPerBatch;
        nPhasesBatch;
        lastBatch;
        RepetitionTime;
        AcquisitionTime;
        nSteps=5;
        
        
    end
    methods (Access=private)
        function CreateParameters(obj)
            CreateOneParameter(obj,'GX','Gx amplitude','',0.1)
            CreateOneParameter(obj,'GY','Gy amplitude','',0.1)
            CreateOneParameter(obj,'GZ','Gz amplitude','',0.1)
            CreateOneParameter(obj,'REPETITIONDELAY','Repetition Delay','ms',1*MyUnits.ms)
            
            
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT','READOUT'...
                'NSLICES','BLANKINGDELAY','PHASE','SLICE','COILRISETIME'}; 
        end  
    end
    
    

    methods
        function obj = ARTIFACTTEST(program)
            obj = obj@MRI_BlankSequence6();
            obj.SequenceName = 'ARTIFACTTEST';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence6(obj);
        end
        
         
        function CreateSequence( obj)
            
            [shimS,shimP,shimR]=SelectAxes(obj);
            [~,~,ENRO,ENSL,ENPH,~,~] = obj.ConfigureGradients(0);
            
            nRepetitions = 1;
            obj.nline_reads = nRepetitions;
            
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;

            obj.nInstructions = 5+6*obj.nSteps;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));

            iIns = 1;    
            blkTime=300;
            pulseTime=obj.GetParameter('PULSETIME')*1e6;
            deadTime=obj.GetParameter('TRANSIENTTIME')*1e6;
            repetitiondelay = obj.GetParameter('REPETITIONDELAY')*1e6;
            crTime = 100;
            acqTime=obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6;
            gx = obj.GetParameter('GX'); 
            gy = obj.GetParameter('GY');
            gz = obj.GetParameter('GZ');           
            gradDelayTime=55;
            
                % Activate gradients
                % (3*nSteps instructions)
                for ii = 1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimR+gx*ii/obj.nSteps*ENRO,RR,0.1);
                    iIns = obj.PulseGradient(iIns,shimP+gy*ii/obj.nSteps*ENPH,PP,0.1);
                    iIns = obj.PulseGradient(iIns,shimS+gz*ii/obj.nSteps*ENSL,SS,crTime/(obj.nSteps-1)-0.2);
                end

               iIns = obj.CreatePulse(iIns,0,gradDelayTime+blkTime,pulseTime,deadTime);
               iIns = obj.Acquisition(iIns,acqTime);
               
               % Desactivate gradients
               % (3 instructions) 

                for ii = 1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimR+(gx-gx*(ii)/obj.nSteps)*ENRO,RR,0.1);
                    iIns = obj.PulseGradient(iIns,shimP+(gy-gy*(ii)/obj.nSteps)*ENPH,PP,0.1);
                    iIns = obj.PulseGradient(iIns,shimS+(gz-gz*(ii)/obj.nSteps)*ENSL,SS,crTime/(obj.nSteps-1)-0.2);
                end
               
               
               iIns = RepetitionDelay (obj,iIns,1,repetitiondelay);       
        end
        
        function [status,result] = Run(obj,iAcq)
            clc

            obj.nInstructions = (5+3*obj.nSteps);
            obj.maxRepetitionsPerBatch = floor(1024/obj.nInstructions);                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
            nBatches = 1;
            t = 0;
            
            for iBatch = 1:nBatches
                obj.CreateSequence;
                obj.Sequence2String;            
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                t1 = clock;
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                t2 = clock;
                t = t + etime(t2,t1);
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Batch aborted!','Warning')
                    return;
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
            end
            
            global rawData
            nPoints = obj.GetParameter('NPOINTS');
            BW = obj.GetParameter('SPECTRALWIDTH');
            timeVector = linspace(0,nPoints/BW,nPoints)';
            rawData = [];
            rawData.outputs.fid = [timeVector,cDataBatch];
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');

        end
    end
    
end