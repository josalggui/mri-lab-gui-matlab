classdef PrePulse_SE_noMRI < MRI_BlankSequence
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    %  Elena Diaz Caballero
    
    properties
        StartSlice;
        EndSlice;
        PhasesPerBatch = 20;
        StartPhase;
        EndPhase;
        slice_ampl;
    end
    methods (Access=private)
        function CreateParameters(obj)
            InputParameters = {...
                'NPHASES','PREPULSETIME','RFEPULSESETIME','PREPULSEAMP','PREPULSEDELAY','PREPULSEDELAY2','NPPULSES','PHASETIME','ECHODELAY','REPETITIONDELAY','READOUTAMPLITUDE','READOUTAMPLITUDE2',...
                'TAU','SLICEBANDWIDTH','STARTPHASE',...
                'ENDPHASE','STARTSLICE','ENDSLICE',...
                'GAUSSIANFILTER'...
                };
            
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {...
                'Number of Phases','PrePulse Time','RF Pulse SE Time','PrePulse Amp','PREPULSEDELAY','PREPULSEDELAY2','Number of Pulses','Phase Time','Gradient Echo Delay','Repetition Delay','DEP Readout Amplitude','REP Readout Amplitude',...
                'Tau Time','Slice Bandwidth','Start Phase',...
                'End Phase','Start Slice','End Slice',...
                'Gaussian'...
                })];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                NaN...
                })];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                0 ...
                })];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                1 ...
                })];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {...
                '','ms','us','','ms','ms','','us','us','ms','','',...
                'ms','MHz','',...
                '','','',...
                ''...
                })];
            
            for k=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{k},InputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = PrePulse_SE_noMRI(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'PrePulse SE without MRI';
            obj.ProgramName = 'MRI_BlankSequence2.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
            obj.SetParameter('PREPULSETIME',10*MyUnits.ms);
            obj.SetParameter('RFEPULSESETIME',20*MyUnits.us);
            obj.SetParameter('PREPULSEAMP',0.1);
            obj.SetParameter('PREPULSEDELAY',10*MyUnits.ms);
            obj.SetParameter('PREPULSEDELAY2',100*MyUnits.ms);
            obj.SetParameter('NPPULSES',2);
            obj.SetParameter('PHASETIME',50*MyUnits.us);
            obj.SetParameter('ECHODELAY',20*MyUnits.us);
            obj.SetParameter('REPETITIONDELAY',50*MyUnits.ms);
            obj.SetParameter('READOUTAMPLITUDE',0.1);
            obj.SetParameter('READOUTAMPLITUDE2',0.1);
            obj.SetParameter('NPHASES',1)
            %             obj.SetParameter('NSLICES',1)
            obj.SetParameter('STARTPHASE',-0.1)
            obj.SetParameter('ENDPHASE',0.1)
            obj.SetParameter('STARTSLICE',-0.1)
            obj.SetParameter('ENDSLICE',0.1)
            obj.SetParameter('GAUSSIANFILTER',1);
            obj.SetParameter('TAU',1*MyUnits.ms);
            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
        end
        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence,
            %         "1", central frequency adjustment sequence,
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            
            [~,nPhases,readout,slice,phase,spoiler,~] = obj.ConfigureGradients(mode);
            
            bbit = 0;
            
            % Calculate the number of line reads
            obj.nline_reads=nPhases;
            
            % Initialize all vectors to zero with the appropriate size
            nInstructionsPerGroup = 19+obj.GetParameter('NPPULSES')*2; % this number must be changed accordingly per pulse sequence
            
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions=nInstructionsPerGroup*nPhases;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
                        
            % readout gradient amplitude calculation
            if( readout == 0 )
                dephasing_readout = shimR;
                phasing_readout = shimR;
                prepulse_amp = 0;
            elseif( obj.GetParameter('PHASETIME') == 0 )
                dephasing_readout = shimR;
                phasing_readout = min(1.0, max( -1.0, shimR -1.0 * obj.GetParameter('READOUTAMPLITUDE2')));
                prepulse_amp = obj.GetParameter('PREPULSEAMP');
            elseif( acq_time  > 2 * (obj.GetParameter('PHASETIME') + 1e-6)  )
                dephasing_readout = min(1.0, max( -1.0, shimR +1.0 * obj.GetParameter('READOUTAMPLITUDE')));
                phasing_readout = min( 1.0, max( -1.0, shimR -2 *obj.GetParameter('READOUTAMPLITUDE2') * (obj.GetParameter('PHASETIME') + 1e-6) / ( acq_time )));
                prepulse_amp = obj.GetParameter('PREPULSEAMP');                
            else
                phasing_readout = min(1.0, max( -1.0, shimR -1.0 * obj.GetParameter('READOUTAMPLITUDE2')));
                dephasing_readout = max( -1.0, min( 1.0, shimR + obj.GetParameter('READOUTAMPLITUDE') *  acq_time / (2 * (obj.GetParameter('PHASETIME') + 1e-6) )));
                prepulse_amp = obj.GetParameter('PREPULSEAMP');                
            end
            
            % SET PREPULSE AMP TO SHIMR BECAUSE AMPLITUDE IS SET BY THE POWER SUPPLY
            prepulse_amp = shimR;
            
            if( slice == 0 )
                slice_amplitude = shimS;
            else
                slice_amplitude = obj.slice_ampl;
            end
            
            for phasei = 1:nPhases
                iline=nInstructionsPerGroup*(phasei-1);
                % initial instruction
                iIns  = 1;
                
                %------ Blanking Delay and Shimming in all gradients (3 instructions)
                nIns  = 3; % set number of instructions in this group
                obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = [shimP shimR shimS ];
                obj.DACs(iline + ( iIns: (nIns+iIns-1)) )     = [PP RR SS ];
                obj.WRITEs(iline + ( iIns: (nIns+iIns-1)) )   = [1 1 1];
                obj.UPDATEs(iline + ( iIns: (nIns+iIns-1)) )  = [1 1 1];
                obj.PhResets(iline + ( iIns: (nIns+iIns-1)) ) = [0 0 0];
                obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= [7 7 7]; % (7=no shape)
                obj.FLAGs(iline + ( iIns: (nIns+iIns-1)) )    = [bbit bbit bbit];
                obj.DELAYs(iline + ( iIns: (nIns+iIns-1)) )   = [0.1 0.1 obj.GetParameter('BLANKINGDELAY')*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
                bbit2 = 2;
                
                %------ PrePulse
                for ipulse = 1:obj.GetParameter('NPPULSES')-1;
                    nIns  = 2; % set number of instructions in this group
                    obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = [prepulse_amp shimR ];
                    obj.DACs(iline + ( iIns: (nIns+iIns-1)) )     = [RR RR];
                    obj.WRITEs(iline + ( iIns: (nIns+iIns-1)) )   = [1 1];
                    obj.UPDATEs(iline + ( iIns: (nIns+iIns-1)) )  = [1 1];
                    obj.PhResets(iline + ( iIns: (nIns+iIns-1)) ) = [0 0];
                    obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= [7 7]; % (7=no shape)
                    obj.FLAGs(iline + ( iIns: (nIns+iIns-1)) )    = [bbit2 bbit];
                    obj.DELAYs(iline + ( iIns: (nIns+iIns-1)) )   = [obj.GetParameter('PREPULSETIME')*1e6 obj.GetParameter('PREPULSEDELAY')*1e6 ]; %time in us
                    iIns = iIns + nIns; % update the number of instructions
                end
                nIns  = 2; % set number of instructions in this group
                    obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = [prepulse_amp shimR];
                    obj.DACs(iline + ( iIns: (nIns+iIns-1)) )     = [RR RR];
                    obj.WRITEs(iline + ( iIns: (nIns+iIns-1)) )   = [1 1];
                    obj.UPDATEs(iline + ( iIns: (nIns+iIns-1)) )  = [1 1];
                    obj.PhResets(iline + ( iIns: (nIns+iIns-1)) ) = [0 1];
                    obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= [7 7]; % (7=no shape)
                    obj.FLAGs(iline + ( iIns: (nIns+iIns-1)) )    = [bbit2 bbit];
                    obj.DELAYs(iline + ( iIns: (nIns+iIns-1)) )   = [obj.GetParameter('PREPULSETIME')*1e6 obj.GetParameter('PREPULSEDELAY2')*1e6]; %time in us
                    iIns = iIns + nIns; % update the number of instructions
                    
                %------ 90 RF pulse (1 instruction)
                nIns  = 1; % set number of instructions in this group
                obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = 0.0;
                obj.DACs(iline + ( iIns: (nIns+iIns-1)) )     = 3;
                obj.TXs(iline + ( iIns: (nIns+iIns-1)) )      = 1;
                if (obj.RF_Shape) % Shaped RF pulse
                    obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= 0;
                else
                    obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= 7;
                end
                obj.FLAGs(iline + ( iIns: (nIns+iIns-1)) )    = 1;
                obj.OPCODEs(iline + ( iIns: (nIns+iIns-1)) )  = 0;
                obj.DELAYs(iline + ( iIns: (nIns+iIns-1)) )   = obj.GetParameter('PULSETIME')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
                %------ TAU Time (1 instruction)
                nIns  = 1; % set number of instructions in this group
                obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = 0.0;
                obj.DACs(iline + ( iIns: (nIns+iIns-1)) )     = 3;
                obj.TXs(iline + ( iIns: (nIns+iIns-1)) )      = 0;
                if (obj.RF_Shape) % Shaped RF pulse
                    obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= 0;
                else
                    obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= 7;
                end
                obj.FLAGs(iline + ( iIns: (nIns+iIns-1)) )    = 0;
                obj.OPCODEs(iline + ( iIns: (nIns+iIns-1)) )  = 0;
                obj.DELAYs(iline + ( iIns: (nIns+iIns-1)) )   = obj.GetParameter('TAU')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
                %------ 180 RF pulse (1 instruction)
                nIns  = 1; % set number of instructions in this group
                obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = 0.0;
                obj.DACs(iline + ( iIns: (nIns+iIns-1)) )     = 3;
                obj.TXs(iline + ( iIns: (nIns+iIns-1)) )      = 1;
                if (obj.RF_Shape) % Shaped RF pulse
                    obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= 0;
                else
                    obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= 7;
                end
                obj.FLAGs(iline + ( iIns: (nIns+iIns-1)) )    = 1;
                obj.OPCODEs(iline + ( iIns: (nIns+iIns-1)) )  = 0;
                obj.DELAYs(iline + ( iIns: (nIns+iIns-1)) )   = obj.GetParameter('RFEPULSESETIME')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
                
                %----- Transient Delay and setting the slice and readout dephasing gradients (3 instructions)
                nIns  = 3; % set number of instructions in this group
                obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = [shimS slice_amplitude dephasing_readout];
                obj.DACs(iline + ( iIns: (nIns+iIns-1)) )     = [SS SS RR];
                obj.WRITEs(iline + ( iIns: (nIns+iIns-1)) )   = [1 1 1];
                obj.UPDATEs(iline + ( iIns: (nIns+iIns-1)) )  = [1 1 1];
                obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= [7 7 7]; % (7=no shape)
                obj.DELAYs(iline + ( iIns: (nIns+iIns-1)) )   = [obj.GetParameter('TRANSIENTTIME')*1e6 0.1 0.1]; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
                % phase gradient amplitude calculation
                if( phase == 0 )
                    phase_amplitude = shimP;
                else
                    phase_amplitude =  min(1.0 , max(-1.0,shimP + (obj.EndPhase - obj.StartPhase) * (phasei-1)/(nPhases-1) + obj.StartPhase));
                end
                
                %----- Phase gradient, rephasing readout gradient, reset slice and phase gradients and echo delay (4 instructions)
                nIns  = 4; % set number of instructions in this group
                obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = [phase_amplitude shimP phasing_readout shimS ];
                obj.DACs(iline + ( iIns: (nIns+iIns-1)) )     = [PP PP RR SS];
                obj.WRITEs(iline + ( iIns: (nIns+iIns-1)) )   = [1 1 1 1];
                obj.UPDATEs(iline + ( iIns: (nIns+iIns-1)) )  = [1 1 1 1];
                obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= [7 7 7 7]; % (7=no shape)
                obj.DELAYs(iline + ( iIns: (nIns+iIns-1)) )   = [(obj.GetParameter('PHASETIME')+obj.GetParameter('COILRISETIME'))*1e6 0.1 obj.GetParameter('COILRISETIME')*1e6 obj.GetParameter('ECHODELAY')*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
                %----- Rephasing Readout Gradient and Data Acquisition. Rewinding lobe for readout, slice and phase gradients (4 instructions)
                nIns  = 4; % set number of instructions in this group
                obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = [phasing_readout shimR -slice_amplitude -phase_amplitude];
                obj.DACs(iline + ( iIns: (nIns+iIns-1)) )     = [RR RR SS PP];
                obj.WRITEs(iline + ( iIns: (nIns+iIns-1)) )   = [1 1 1 1];
                obj.UPDATEs(iline + ( iIns: (nIns+iIns-1)) )  = [1 1 1 1];
                obj.RXs(iline + ( iIns: (nIns+iIns-1)) )      = [1 0 0 0];
                obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= [7 7 7 7]; % (7=no shape)
                obj.DELAYs(iline + ( iIns: (nIns+iIns-1)) )   = [acq_time*1e6 obj.GetParameter('COILRISETIME')*1e6 0.1 (obj.GetParameter('PHASETIME')+obj.GetParameter('COILRISETIME'))*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
                %----- Spoiler for slice gradient (1 instruction)
                nIns  = 1; % set number of instructions in this group
                if (spoiler)
                    obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = obj.GetParameter('SPOILERAMP');
                else
                    obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = shimS;
                end
                obj.DACs(iline + ( iIns: (nIns+iIns-1)) )     = SS;
                obj.WRITEs(iline + ( iIns: (nIns+iIns-1)) )   = 1;
                obj.UPDATEs(iline + ( iIns: (nIns+iIns-1)) )  = 1;
                obj.CLEARs(iline + ( iIns: (nIns+iIns-1)) )   = 1;
                obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= 7; % (7=no shape)
                obj.DELAYs(iline + ( iIns: (nIns+iIns-1)) )   = obj.GetParameter('SPOILERTIME')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
                %----- Repetition Delay (1 instruction)
                nIns  = 1; % set number of instructions in this group
                obj.AMPs(iline + ( iIns: (nIns+iIns-1)) )     = 0.0;
                obj.DACs(iline + ( iIns: (nIns+iIns-1)) )     = 3;
                obj.WRITEs(iline + ( iIns: (nIns+iIns-1)) )   = 1;
                obj.UPDATEs(iline + ( iIns: (nIns+iIns-1)) )  = 1;
                obj.CLEARs(iline + ( iIns: (nIns+iIns-1)) )   = 1;
                obj.ENVELOPEs(iline + ( iIns: (nIns+iIns-1)) )= 7; % (7=no shape)
                obj.DELAYs(iline + ( iIns: (nIns+iIns-1)) )   = obj.GetParameter('REPETITIONDELAY')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
            end % end for nPhases
            %             end %end for slicei
            
        end % end create gradient echo pulse sequence
        function [status,result] = Run(obj,iAcq)
            
            obj.StartSlice = obj.GetParameter('STARTSLICE');
            obj.EndSlice = obj.GetParameter('ENDSLICE');
            obj.StartPhase = obj.GetParameter('STARTPHASE');
            obj.EndPhase = obj.GetParameter('ENDPHASE');
            nSlices = obj.GetParameter('NSLICES');
            if nSlices > 1
                SliceStep = (obj.EndSlice - obj.StartSlice)/(nSlices-1);
            else
                SliceStep = 0;
            end
            nPhases = obj.GetParameter('NPHASES');
            if nPhases > 1
                PhaseStep = (obj.EndPhase - obj.StartPhase)/(nPhases-1);
            else
                PhaseStep = 0;
            end
            nPhaseBatches = ceil(nPhases/obj.PhasesPerBatch);
            nPoints = obj.GetParameter('NPOINTS');
            Data3D = zeros(nPoints,nPhases,nSlices);
            cn = nSlices*nPhaseBatches;
            count = 0;
            if nPhaseBatches>1
                h_waitbar = waitbar(count/cn,'Please wait (por favor espere)...');
            end
            for iSlice = 1:nSlices
                
                obj.StartSlice = obj.GetParameter('STARTSLICE') + (iSlice-1)*SliceStep;
                obj.EndSlice = obj.StartSlice;
                
                [shimS,~,~]=SelectAxes(obj);
                
                if (nSlices == 1)
                    obj.slice_ampl = obj.StartSlice;
                else
                    obj.slice_ampl =  min(1.0 , max(-1.0,shimS + (obj.EndSlice - obj.StartSlice) * (iSlice-1)/(nSlices-1) + obj.StartSlice));
                end
                
                Data2D = [];
                for iBatch = 1:nPhaseBatches
                    
                    if nPhases >1
                        obj.CreateSequence(1); % Create the gradient echo sequence for CF
                        obj.Sequence2String;
                        fname_CF = 'TempBatch_CF.bat';
                        obj.WriteBatchFile(fname_CF);
                        obj.CenterFrequency(fname_CF);
                    end
                    obj.SetParameter('LASTFREQUENCY',obj.GetParameter('FREQUENCY'));
                    
                    
                    % NOW RUN ACTUAL ACQUISTION
                    obj.StartPhase = obj.GetParameter('STARTPHASE') + (iBatch-1)*obj.PhasesPerBatch*PhaseStep;
                    obj.EndPhase = obj.StartPhase + (obj.PhasesPerBatch-1)*PhaseStep;
                    
                    obj.CreateSequence(0); % Create the normal gradient echo sequence
                    
                    obj.Sequence2String;
                    
                    
                    fname = 'TempBatch.bat';
                    obj.WriteBatchFile(fname);
                    [status,result] = system(fullfile(obj.path_file,fname));
                    
                    if status == 0 && ~isempty(result)
                        C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                        res = C{:};
                        obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                        obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                        obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                        obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                        obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                    end
                    
                    DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                    cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                    nPhasesInBatch=length(cDataBatch)/nPoints;
                    d = reshape(cDataBatch,nPoints,nPhasesInBatch);
                    Data2D = cat(2,Data2D,d);
                    count = count+1;
                    if nPhaseBatches>1
                        waitbar(count/cn,h_waitbar,sprintf('Por favor espere..... %5.2f %%',100*count/cn));
                    end
                end
                Data3D(:,:,iSlice) = Data2D;
                if length(obj.cData) ~= nPoints*nPhases*nSlices;
                    obj.cData = zeros(obj.NAverages,nPoints*nPhases*nSlices);
                end
                if obj.averaging
                    Nav = obj.NAverages;
                else
                    Nav = 1;
                end
                index = mod(iAcq-1,Nav)+1;
                if obj.autoPhasing
                    %                    pt = max(Data3D(:,ceil(size(Data3D,2)/2),ceil(size(Data3D,3)/2)));
                    pt = max(Data3D(:));
                    Data3D = Data3D.*exp(complex(0,-angle(pt)));
                end
                obj.cData(index,1:length(Data3D(:))) = Data3D(:);
                obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
                obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
                %    obj.fData1D = fftshift(ifft(fftshift(obj.cDataAv)));
                obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
                acq_time = obj.GetParameter('ACQUISITIONTIME');
                obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*acq_time/nPhases;
                obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time/nPhases;
            end
            if nPhaseBatches>1
                delete(h_waitbar);
            end
        end
    end
    
end

