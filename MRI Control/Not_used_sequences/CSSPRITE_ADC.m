classdef CSSPRITE_ADC < MRI_BlankSequence3
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    %  Elena Diaz Caballero
    
    properties
        MaxPoints = 4096;
        MaxInstructions = floor((1000)/8);
        Ntot;
        Axis1Vector;
        Axis2Vector;
        Axis3Vector;
        Axis1IND;
        Axis2IND;
        Axis3IND;
        StartPhase = 0;
        EndPhase = 0;
        K_ind;
        PDF_mat;
        Data3D;
        INDuse;
%         calibrationactive = 0; // already defined in the superclass
        calibrated = 0;
        Axis1;
        Axis2;
        Axis3;
        CAxis1;
        CAxis2;
        CAxis3;
        rdspeed_mult = 1;
        nLinesBatch;
        NP_calib = 1;
        Data4D;
        PhasesinThisBatch;
    end
    methods (Access=private)
        function CreateParameters(obj)
            InputParameters = {...
                'NPAVGS','NREADOUTS','NPHASES','TP','REPETITIONDELAY','CSAMOUNT',...
                'STARTREADOUT','ENDREADOUT','STARTPHASE','ENDPHASE','STARTSLICE','ENDSLICE',...
                'PREPULSETIME','PREPULSEDELAY','PREPULSETR',...
                };
            
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {...
                '#Points to Average','Number of Readouts','Number of Phases','Phase encode time','Repetition Delay','Compression',...
                'Start Readout','End Readout','Start Phase','End Phase','Start Slice','End Slice',...
                'Prepulse Duration','Prepulse Delay','Prepulse TR',...
                })];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                })];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {...
                1,NaN,NaN,NaN,NaN,0,...
                NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                })];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,100,...
                NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                })];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {...
                '','','','us','ms','',...
                '','','','','',''...
                'ms','ms','s',...
                })];
            
            for k=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{k},InputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = CSSPRITE_ADC(program)
            obj = obj@MRI_BlankSequence3();
            obj.SequenceName = 'CS SPRITE w ADC';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence3(obj);
            obj.SetParameter('NPAVGS',1);
            obj.SetParameter('NREADOUTS',1);
            obj.SetParameter('NPHASES',1);
            obj.SetParameter('TP',50*MyUnits.us);
            obj.SetParameter('REPETITIONDELAY',50*MyUnits.ms);
            obj.SetParameter('STARTREADOUT',-0.1);
            obj.SetParameter('ENDREADOUT',0.1);
            obj.SetParameter('STARTPHASE',-0.1);
            obj.SetParameter('ENDPHASE',0.1);
            obj.SetParameter('STARTSLICE',-0.1);
            obj.SetParameter('ENDSLICE',0.1);
            obj.SetParameter('CSAMOUNT',50);
            
            obj.SetParameter('PREPULSETIME',10*MyUnits.ms);
            obj.SetParameter('PREPULSEDELAY',10*MyUnits.ms);
            obj.SetParameter('PREPULSETR',1*MyUnits.s);
            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
        end
        function UpdateTETR(obj)
            obj.TE = obj.GetParameter('TRANSIENTTIME')+obj.GetParameter('TP');
            obj.TR = obj.TE + obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH') +...
             obj.GetParameter('PULSETIME') + obj.GetParameter('COILRISETIME') + ...
             obj.GetParameter('REPETITIONDELAY');
            obj.TTotal = obj.TR * obj.GetParameter('NSCANS') * obj.Ntot;
        end
        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence,
            %         "1", central frequency adjustment sequence,
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            
            %blanking bits
            bbit = 1;            
            bbit2 = 2;

            % then grab the appropriate switches for IF the
            % readout/slice/phases were selected
            [~,~,readout,slice,phase,~,~] = obj.ConfigureGradients(mode);
            
            
            % Calculate the number of line reads based on the length of the Axis vector
            if mode == 0
                obj.nline_reads = numel( obj.Axis1Vector );
            elseif mode == 1
                obj.nline_reads = 1;
            end
            % Initialize all vectors to zero with the appropriate size
            if obj.GetParameter('PREPULSETIME') > 0
                obj.nInstructions=11*obj.nline_reads+5;
            else
                obj.nInstructions=11*obj.nline_reads+3;
            end
            
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            % set the acquisition time
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
            
            %%% Gradient amplitude calculation
               % initialize gradient vectors
            [Axis1Vector, Axis2Vector, Axis3Vector] = deal( zeros( 1, obj.nline_reads) );
               % set the bounds to the vectors
            MinVector = Axis1Vector - 1;
            MaxVector = Axis1Vector + 1;
            
            % readout switch set to Axis1
            if( readout == 0 ) || (mode == 1)
                Axis1Vector = Axis1Vector + shimR;
            else
                Axis1Vector = max( min(MaxVector,obj.Axis1Vector + shimR), MinVector );
            end
            
            % phase switch set to Axis2
            if( phase == 0 ) || (mode == 1)
                Axis2Vector = Axis2Vector + shimP;
            else
                Axis2Vector = max( min(MaxVector,obj.Axis2Vector + shimP), MinVector );
            end
            
            % slice switch set to Axis3
            if( slice == 0 ) || (mode == 1)
                Axis3Vector = Axis3Vector + shimS;
            else
                Axis3Vector = max( min(MaxVector,obj.Axis3Vector + shimS), MinVector );
            end
            
            %%% BEGIN THE PULSE PROGRAMMING
            iIns = 1;
            %------ Blanking Delay and Shimming in all gradients (3 instructions)
            nIns = 3; % set the number of instruction in this block
            vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                obj.AMPs(vIns)     = [shimP shimR shimS ];
                obj.DACs(vIns)     = [PP RR SS];
                obj.WRITEs(vIns)   = [1 1 1];
                obj.UPDATEs(vIns)  = [1 1 1];
                obj.PhResets(vIns) = [0 0 0];
                obj.ENVELOPEs(vIns)= [7 7 7]; % (7=no shape)
                obj.FLAGs(vIns)    = [0 0 0];
                obj.DELAYs(vIns)   = [0.1 0.1 0.1]; %time in us
            iIns = iIns + nIns; % update the number of instructions in this block
                        
            % PREPULSE IF THE PREPULSETIME IS GREATER THAN 0
            if obj.GetParameter('PREPULSETIME') > 0
                nIns  = 2; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [shimR shimR];
                    obj.DACs(vIns)     = [RR RR];
                    obj.WRITEs(vIns)   = [1 1];
                    obj.UPDATEs(vIns)  = [1 1];
                    obj.PhResets(vIns) = [0 0];
                    obj.ENVELOPEs(vIns)= [7 7]; % (7=no shape)
                    obj.FLAGs(vIns)    = [bbit2 0];
                    obj.DELAYs(vIns)   = [obj.GetParameter('PREPULSETIME')*1e6 obj.GetParameter('PREPULSEDELAY')*1e6 ]; %time in us
                iIns = iIns + nIns; % update the number of instructions
            end
            
            % --- NOW START THE RF TRAIN
            for ipulse = 1:obj.nline_reads
                % BLANKING DELAY FOR DATA ACQ AND RF (combined together using the pulse generator)
                nIns  = 1; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.WRITEs(vIns)   = 0;
                    obj.UPDATEs(vIns)  = 0;
                    obj.PhResets(vIns) = 1;
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.FLAGs(vIns)    = bbit;
                    obj.DELAYs(vIns)   = obj.GetParameter('BLANKINGDELAY')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
%                 % BLANKING DELAY
%                 nIns  = 1; % set number of instructions in this group
%                 vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
%                     obj.AMPs(vIns)     = 0.0;
%                     obj.DACs(vIns)     = 3;
%                     obj.WRITEs(vIns)   = 0;
%                     obj.UPDATEs(vIns)  = 0;
%                     obj.PhResets(vIns) = 1;
%                     obj.ENVELOPEs(vIns)= 7; % (7=no shape)
%                     obj.FLAGs(vIns)    = bbit;
%                     obj.DELAYs(vIns)   = obj.GetParameter('BLANKINGDELAY')*1e6; %time in us
%                 iIns = iIns + nIns; % update the number of instructions
                
                %------ RF pulse (1 instruction)
                nIns = 1; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.TXs(vIns)      = 1;
                    if (obj.RF_Shape) % Shaped RF pulse
                        obj.ENVELOPEs(vIns)= 0;
                    else
                        obj.ENVELOPEs(vIns)= 7;
                    end
                    obj.FLAGs(vIns)    = bbit;
                    obj.OPCODEs(vIns)  = 0;
                    obj.DELAYs(vIns)   = obj.GetParameter('PULSETIME')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %------ Set All Gradients to the appropriate level, include coil rise time (3 instructions)
                nIns = 4; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [shimS Axis1Vector(ipulse) Axis2Vector(ipulse) Axis3Vector(ipulse) ];
                    obj.DACs(vIns)     = [SS RR PP SS];
                    obj.WRITEs(vIns)   = [0 1 1 1];
                    obj.UPDATEs(vIns)  = [0 1 1 1];
                    obj.PhResets(vIns) = [0 0 0 0];
                    obj.ENVELOPEs(vIns)= [7 7 7 7]; % (7=no shape)
                    obj.FLAGs(vIns)    = [0 0 0 0];
                    obj.DELAYs(vIns)   = [1.0 0.1 0.1 (obj.GetParameter('COILRISETIME')+obj.GetParameter('TP'))*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %------ Blanking Delay and Shimming in all gradients (3 instructions)
                nIns = 3; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [shimP shimR shimS ];
                    obj.DACs(vIns)     = [PP RR SS];
%                     obj.WRITEs(vIns)   = [1 1 1];
%                     obj.UPDATEs(vIns)  = [1 1 1];
                    obj.PhResets(vIns) = [0 0 0];
                    obj.ENVELOPEs(vIns)= [7 7 7]; % (7=no shape)
                    obj.FLAGs(vIns)    = [0 0 0];
                    obj.DELAYs(vIns)   = [0.1 0.1 obj.GetParameter('COILRISETIME')*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
%                 %----- Transient Delay and TP Delay (1 instructions)
%                 nIns = 1; % set the number of instruction in this block
%                 vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
%                     obj.AMPs(vIns)     = 0.0;
%                     obj.DACs(vIns)     = 3;
%                     obj.WRITEs(vIns)   = 0;
%                     obj.UPDATEs(vIns)  = 0;
%                     obj.ENVELOPEs(vIns)= 7; % (7=no shape)
%                     obj.DELAYs(vIns)   = (obj.GetParameter('TRANSIENTTIME')+obj.GetParameter('TP'))*1e6; %time in us
%                 iIns = iIns + nIns; % update the number of instructions in this block

                %----- Data Acquisition (1 instruction)
                nIns = 1; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.WRITEs(vIns)   = 0;
                    obj.UPDATEs(vIns)  = 0;
                    obj.RXs(vIns)      = 1;
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.DELAYs(vIns)   = acq_time*1e6;
                iIns = iIns + nIns; % update the number of instructions in this block   
                
                %----- TR (1 instructions)
                nIns = 1; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.WRITEs(vIns)   = 1;
                    obj.UPDATEs(vIns)  = 1;
                    obj.CLEARs(vIns)   = 1;
                    obj.ENVELOPEs(vIns)= 7;
                    if ipulse == obj.nline_reads
%                         obj.DELAYs(vIns)   = (obj.GetParameter('REPETITIONDELAY'))*1e6; %time in us
                        obj.DELAYs(vIns)   = (obj.GetParameter('PREPULSETR')+obj.GetParameter('REPETITIONDELAY'))*1e6; %time in us
                    else
                        obj.DELAYs(vIns)   = obj.GetParameter('REPETITIONDELAY')*1e6; %time in us
                    end
                iIns = iIns + nIns; % update the number of instructions in this block
                
            end % end the axis instruction loop
            
        end % end create gradient echo pulse sequence
        
        
        function [AcqT, start_measure, end_measure, after_RF, total_measure_time] = CalcTiming( obj )
            
            % pulse parameters
                    BD = obj.GetParameter('BLANKINGDELAY');
                    RF = obj.GetParameter('PULSETIME');
                    AddP = 0.1E-6;
                    Sync = 1E-6;
                    TP = obj.GetParameter('TP');
                    CR = obj.GetParameter('COILRISETIME');
                    NP = obj.GetParameter('NPOINTS');
                    SW = obj.GetParameter('SPECTRALWIDTH');
                    AcqT = NP/SW;
                    

                    % specify start and end of reading
                    after_RF = 0;
                    start_measure = after_RF + Sync + 4*AddP + 2*CR + TP;
                    end_measure   = start_measure + AcqT;
                    total_measure_time = end_measure + BD + RF;

        end     
        
        function [status,result] = Run(obj,iAcq)
            global MRIDATA TestData
            
            MRIDATA.ADC  = [];
            
            % Now calculate the batches that need to be run    
            nPoints = obj.GetParameter('NPOINTS');
            InsPerBatch = min( floor(obj.MaxPoints / nPoints), obj.MaxInstructions );
            
            %%% CALCULATE THE AXISVECTORS
            % Begin and end axes
            StartReadout = obj.GetParameter('STARTREADOUT');
            EndReadout = obj.GetParameter('ENDREADOUT');
            StartPhase = obj.GetParameter('STARTPHASE');
            EndPhase = obj.GetParameter('ENDPHASE');
            StartSlice = obj.GetParameter('STARTSLICE');
            EndSlice = obj.GetParameter('ENDSLICE');
            
            % number for each axes
            nReadouts = obj.GetParameter('NREADOUTS');
            nPhases = obj.GetParameter('NPHASES');
            nSlices = obj.GetParameter('NSLICES');
            
            % do switch for if the readouts and gradients are enabled
            if ~obj.readout_en
                nReadouts = 1; StartReadout = 0; EndReadout = 0;
            end
            if ~obj.phase_en
                nPhases = 1; StartPhase = 0; EndPhase = 0;
            end
            if ~obj.slice_en
                nSlices = 1; StartSlice = 0; EndSlice = 0;
            end
            
            % Create the Axis Vectors
                % FIRST create the axes for the measurements - regular
                % cartesian grid
                Readout_axis = linspace(StartReadout, EndReadout, nReadouts);
                Phase_axis = linspace(StartPhase, EndPhase, nPhases);
                Slice_axis = linspace(StartSlice, EndSlice, nSlices);
                
                naxis_en = sum( [ obj.readout_en, obj.phase_en, obj.slice_en ] );
                if naxis_en == 1
                    % Set both the compression vector and the probability density function to 1
                    obj.K_ind = 1:nReadouts*nPhases*nSlices;
                    obj.PDF_mat = ones( size( obj.K_ind) );
                    
                    % total number of samples
                    obj.Ntot = nReadouts*nPhases*nSlices;
                else
                    % Do CS compression scheme for objects with more than 1 axis
                    % Set compression precentage
                    percentage_coverage = obj.GetParameter('CSAMOUNT')/100;
                    amountsamples = round( nReadouts * nPhases * nSlices * percentage_coverage );
                    
                    % Set the standard deviation of the compression scheme
                    std_div = 6;
                    std_disx = nReadouts/std_div;
                    std_disy = nPhases/std_div;
                    std_disz = nSlices/std_div;

                    % Calculate the compression vectors
                    K_Rind = max( 1, min( nReadouts, round( (std_disx) * randn( amountsamples, 1 ) + nReadouts/2 )));
                    K_Pind = max( 1, min( nPhases, round( (std_disy) * randn( amountsamples, 1 ) + nPhases/2 )));
                    K_Sind = max( 1, min( nSlices, round( (std_disz) * randn( amountsamples, 1 ) + nSlices/2 )));
                    obj.K_ind = sub2ind([nReadouts,nPhases,nSlices], K_Rind, K_Pind, K_Sind)';
                    
                    % Now calculate the probability density function
                    pdf1 = repmat( permute( pdf('Normal',1:nReadouts,nReadouts/2,std_disx), [ 2 1 ] ), 1, nPhases, nSlices );
                    pdf2 = repmat( ( pdf('Normal',1:nPhases,nPhases/2,std_disy) ), nReadouts, 1, nSlices );
                    pdf3 = repmat( permute( pdf('Normal',1:nSlices,nSlices/2,std_disz) , [ 3 1 2 ] ), nReadouts, nPhases, 1 );
                    obj.PDF_mat = pdf1.*pdf2.*pdf3;
                    obj.PDF_mat = obj.PDF_mat/max(obj.PDF_mat(:));
                    
                    % total number of samples
                    obj.Ntot = numel(obj.K_ind);
                                
                end % end compression scheme
                
                
                % now create the matrixes for measurement
                [AX1, AX2, AX3] = meshgrid( Readout_axis, Phase_axis, Slice_axis );
                
                % convert these to vector and then sort them by distance from the center
                AxesVectors = [ AX1(obj.K_ind)', AX2(obj.K_ind)', AX3(obj.K_ind)' ];
                [~, VIND] = sort( sqrt( AxesVectors(:,1).^2 + AxesVectors(:,2).^2 + AxesVectors(:,3).^2 ), 'ascend' );
                AxesVectors = AxesVectors( VIND, : );
%                 AxesINDs = linspace(1,numel(VIND), numel(VIND));
%                 TestData.K_ind = obj.K_ind;
%                 TestData.VIND = VIND;
%                 TestData.Axesvectors = AxesVectors;
                
                obj.K_ind = obj.K_ind(VIND);
                
                % Calculate how many batches to have
                nBatches = ceil( obj.Ntot / InsPerBatch );
               
                
                if isfield(MRIDATA,'Data4D')
                    if obj.averaging
                        if iAcq == 1
                            MRIDATA.Data4D = [];
                        end
                    else
                        MRIDATA.Data4D = [];
                    end
                else
                    MRIDATA.Data4D = [];
                end

                if isfield(MRIDATA,'Data4DCF')
                    if obj.averaging
                        if iAcq == 1
                            MRIDATA.Data4DCF = [];
                        end
                    else
                        MRIDATA.Data4DCF = [];
                    end
                else
                    MRIDATA.Data4DCF = [];
                end
                
            
            
            % initialize storage vectors
            Data3D = [];%zeros(nBatches,obj.Ntot);
            Data4D = [];
            
%                 INDstore = zeros(nReadouts*nPhases*nSlices,3);
            % start the counter
            cn = nBatches;
            count = 0;
            if nBatches>1
                h_waitbar = waitbar(count/cn,'Please wait (por favor espere)...');
            end
            
            obj.Axis1IND = [];
            obj.Axis2IND = [];
            obj.Axis3IND = [];
            obj.INDuse = [];
            
            % NOW run through the batches
            for iBatch = 1:nBatches
                
                % calculate the center frequency
                
                if nBatches >1
                    % set the axis vectors
                    obj.Axis1Vector = 0;
                    obj.Axis2Vector = 0;
                    obj.Axis3Vector = 0;
                    % set the number of points
                    obj.SetParameter('NPOINTS',300);
                    ptime = obj.GetParameter('PREPULSETIME');
                    navg = obj.GetParameter('NSCANS');
                    obj.SetParameter('PREPULSETIME',0);
                    obj.SetParameter('NSCANS',3);
                    obj.CreateSequence(1); % Create the gradient echo sequence for CF
                    obj.Sequence2String;
                    fname_CF = 'TempBatch_CF.bat';
                    obj.WriteBatchFile(fname_CF);
                    
                    obj.CenterFrequency(fname_CF);
                    
                    % revert the number of points back
                    obj.SetParameter('NPOINTS',nPoints);
                    obj.SetParameter('NSCANS',navg);
                    obj.SetParameter('PREPULSETIME',ptime);
                end
                
                
                % Calculate the appropriate axes
                obj.Axis1Vector = downsample( AxesVectors(:,1), nBatches, iBatch - 1)';
                obj.Axis2Vector = downsample( AxesVectors(:,2), nBatches, iBatch - 1)';
                obj.Axis3Vector = downsample( AxesVectors(:,3), nBatches, iBatch - 1)';
                obj.PhasesinThisBatch = numel( obj.Axis1Vector );
                obj.INDuse   = [ obj.INDuse, downsample( obj.K_ind, nBatches, iBatch - 1 )];
                obj.Axis1IND = [ obj.Axis1IND, obj.Axis1Vector(:)'];
                obj.Axis2IND = [ obj.Axis2IND, obj.Axis2Vector(:)'];
                obj.Axis3IND = [ obj.Axis3IND, obj.Axis3Vector(:)'];
                
                % NOW RUN ACTUAL ACQUISTION
                
                obj.CreateSequence(0); % Create the normal gradient echo sequence
                obj.Sequence2String;
                
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                
                if obj.calibrationactive
                    % Right before we start the process of running the
                    % script, FIRST implement the ADC board to monitor for
                    % the sequence
                    obj.calibrated = 0;
                    fname2 = 'RunADC_3.bat';
                    obj.WriteCalibBatchFile( fname2 );
                    system(char(strcat(fname2,'&')));
                    pause(1);
                end
                
                [status,result] = system(fullfile(obj.path_file,fname));

                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                end

                % Load in data
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                
                % Reorder the data
                d = reshape(cDataBatch,nPoints,obj.PhasesinThisBatch);
                
                if obj.calibrationactive
                    % Now load in the data about the position of the points
                    MRIDATA.ADC(iBatch).CMatrix1 = ReadADCData;
                    MRIDATA.ADC(iBatch).nPhases = obj.PhasesinThisBatch;
                end
                
                
                Data4D = [ Data4D, d ]; % number of points down and number of axes in the horizontal direction
                     
                % update the counter
                if nBatches>1
                    count = count+1;
                    waitbar(count/cn,h_waitbar,sprintf('Please wait (por favor espere)..... %5.2f %%',100*count/cn));
                end
            end
            % save 4D data
            MRIDATA.Data4Dunfit.Fid  = Data4D;
            MRIDATA.Data4Dunfit.Axis = cat(3,obj.Axis1IND , obj.Axis2IND, obj.Axis3IND);
            MRIDATA.Data4Dunfit.Time = obj.GetParameter('TRANSIENTTIME')+obj.GetParameter('TP')+...
                                       linspace(0,obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH'),obj.GetParameter('NPOINTS'));
            
            %reorganize the data
            obj.Data4D = Data4D;
            
            % switch the FID if needed
            if obj.readout_en
                % mean the data for the appropriate choice of averages
                Data3D = mean(obj.Data4D(1:obj.GetParameter('NPAVGS'),:),1);
            else
                Data3D = obj.Data4D;
            end

            % HERE WE MUST ADD THE CALIBRATION IF IT WAS ADDED
            ndim = sum( [ obj.readout_en, obj.phase_en, obj.slice_en ] );
            
%             [ AX2, AX1, AX3 ] = meshgrid( Phase_axis, Readout_axis, Slice_axis );
            
            
            if obj.calibrationactive == 1
                obj.RunCalibrationAxisGeneration;
                MRIDATA.Calib.Ax1 = obj.CAxis1;
                MRIDATA.Calib.Ax2 = obj.CAxis2;
                MRIDATA.Calib.Ax3 = obj.CAxis3;
                MRIDATA.Calib.Dat = obj.Data4D;
                obj.calibrated = 1;
            end

            if obj.calibrated == 1
                obj.Axis1 = obj.CAxis1(1,:)./max(abs(obj.CAxis1(:)));
                obj.Axis2 = obj.CAxis2(1,:)./max(abs(obj.CAxis2(:)));
                obj.Axis3 = obj.CAxis3(1,:)./max(abs(obj.CAxis3(:)));
            else
                obj.Axis1 = obj.Axis1IND;
                obj.Axis2 = obj.Axis2IND;
                obj.Axis3 = obj.Axis3IND;
            end

            if ndim == 3
                warning off;
                Data3D_Fun = scatteredInterpolant(obj.Axis1(:),obj.Axis2(:),obj.Axis3(:),...
                                                  Data3D(:),'natural','none');
                warning on;
                Data3D_use = Data3D_Fun(AX1,AX2,AX3);
                Data3D_use(isnan(Data3D_use)) = 0;
            elseif ndim == 2
                warning off;
                Data3D_Fun = scatteredInterpolant(obj.Axis1(:),obj.Axis2(:),...
                                                  Data3D(:),'natural','none');
                warning on;
                Data3D_use = Data3D_Fun(AX1,AX2);
                Data3D_use(isnan(Data3D_use)) = 0;

            elseif ndim == 1
                if nSlices > 1
                    axis_use = obj.Axis3;
                    axis_find = AX3;
                elseif nPhases > 1
                    axis_use = obj.Axis2;
                    axis_find = AX2;
                else
                    axis_use = obj.Axis1;
                    axis_find = AX1;
                end
                Data3D_use = interp1(axis_use(:), Data3D(:), axis_find, 'spline',0);
            else
                Data3D_use = Data3D(:);
            end



            if obj.averaging
                Nav = obj.NAverages;
            else
                Nav = 1;
            end
            index = mod(iAcq-1,Nav)+1;
            if obj.autoPhasing
                %                    pt = max(Data3D(:,ceil(size(Data3D,2)/2),ceil(size(Data3D,3)/2)));
                pt = max(Data3D_use(:));
                Data3D_use = Data3D_use.*exp(complex(0,-angle(pt)));
            end
            
            if numel( obj.cData ) ~= numel( Data3D_use ) * obj.NAverages
                obj.cData ( 1:obj.NAverages, 1:length(Data3D_use(:)) ) = repmat( Data3D_use(:)', obj.NAverages, 1 );
            else
                obj.cData(index,1:length(Data3D_use(:))) = Data3D_use(:)';
            end

            obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
            obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
            %    obj.fData1D = fftshift(ifft(fftshift(obj.cDataAv)));
            obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
            acq_time = obj.GetParameter('ACQUISITIONTIME');
            obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*acq_time/nPhases;
            obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time/nPhases;
        
%             MRIDATA.Data3Dnew(:,:,:,index) = Data3D;

    
    
        end
    
        function RunCalibrationAxisGeneration( obj )
            % this will run the calibration on the actively stored data
            global MRIDATA
            
            obj.CAxis1 = [];
            obj.CAxis2 = [];
            obj.CAxis3 = [];
            
            % keep in mind that this is for only 2 axis at the moment
            % first axis is the readout direction, second is the phase
            % direction
            for iADC = 1:numel(MRIDATA.ADC)
                
                    TriggerAxis = MRIDATA.ADC(iADC).CMatrix1(1,:)';
                    DAxis1 = MRIDATA.ADC(iADC).CMatrix1(2,:)';
                    DAxis2 = MRIDATA.ADC(iADC).CMatrix1(3,:)';
                    DAxis3 = MRIDATA.ADC(iADC).CMatrix1(4,:)';
                    
                    % pulse parameters
                    
                    [AcqT, start_measure, end_measure, after_RF, total_measure_time] = obj.CalcTiming;
                                       
                    nP = MRIDATA.ADC(iADC).nPhases;
                    NP = obj.GetParameter('NPOINTS');
                    
                    % measurement parameters
                    RdSpeed = (40E6)/obj.rdspeed_mult;
                    dt = 1/RdSpeed;

                    % measure inds
                    start_ind = floor(start_measure/dt);
                    end_ind   = ceil(end_measure/dt);
                    after_RF_ind = floor(after_RF/dt);
                    
                    % find the ind when the trigger is below a certain threshold that is
                    % considered as '0'
                    trigthresh_up   = 1E-1;
                    TriggerAxis ( TriggerAxis > trigthresh_up )  =  1;
                    TriggerAxis = round(TriggerAxis);
                    
                    % starting offset
                    skipind = 0;
                    DAxis1Sum = zeros(NP,nP);
                    DAxis2Sum = zeros(NP,nP);
                    DAxis3Sum = zeros(NP,nP);
                    
%                     dfilter = fdesign.lowpass('N,Fc',1,1,100);
%                     Hd = design(dfilter);
                    
                    for ip = 1:nP
                        % Rewrite the axes
                        TriggerAxis = TriggerAxis(1+skipind:end);
                        DAxis1 = DAxis1(1+skipind:end);
                        DAxis2 = DAxis2(1+skipind:end);
                        DAxis3 = DAxis3(1+skipind:end);
                        
                        
                        % find the ind where the start is and then where the end is
                        inds = find( TriggerAxis == 1, 1, 'first' );
                        inde = find( TriggerAxis(inds:end) == 0, 1, 'first' )+inds-1;
                        
                        % Grab the 'zero' amount - remove the shimming bias
                        d1shim = 0;
                        d2shim = 0;%median( DAxis2(inds+50:inde) );
                        d3shim = 0;%median( DAxis3(inds:inde) );
                        
                        
                        % grab data and smooth it
%                         SelectedData1 = filter( Hd, DAxis1(inde+after_RF_ind:inde+end_ind)-d1shim );
                        SelectedData1 = DAxis1(inde+after_RF_ind-1:inde+end_ind-1)-d1shim;
%                         SelectedData1 = smooth( DAxis1(inde+after_RF_ind:inde+end_ind)-d1shim,11, 'rloess' );
                        % now sum the data to determine where in KSpace you are
                        DAxis1SumFull = ( cumsum( SelectedData1 ) );
                        DAxis1SumSelect = DAxis1SumFull(start_ind-after_RF_ind:end);
                        D1interp = interp1(linspace(1,numel(DAxis1SumSelect),numel(DAxis1SumSelect)),...
                                          DAxis1SumSelect, linspace(1,numel(DAxis1SumSelect),NP));
                        
                        DAxis1Sum(:,ip) = D1interp;
                        
                        % grab data and smooth it
%                         SelectedData2 = filter(Hd, DAxis2(inde+after_RF_ind:inde+end_ind)-d2shim );
                        SelectedData2 = DAxis2(inde+after_RF_ind-1:inde+end_ind-1)-d2shim;
%                         SelectedData2 = smooth( DAxis2(inde+after_RF_ind:inde+end_ind)-d2shim,11, 'rloess' );
                        % now sum the data to determine where in KSpace you are
                        DAxis2SumFull = ( cumsum( SelectedData2 ));
                        DAxis2SumSelect = DAxis2SumFull(start_ind-after_RF_ind:end);
                        D2interp = interp1(linspace(1,numel(DAxis2SumSelect),numel(DAxis2SumSelect)),...
                                          DAxis2SumSelect, linspace(1,numel(DAxis2SumSelect),NP));
                        
                        DAxis2Sum(:,ip) = D2interp;
                        
                        % grab data and smooth it
% %                         SelectedData3 = filter(Hd, DAxis3(inde+after_RF_ind:inde+end_ind)-d3shim );
                        SelectedData3 = DAxis3(inde+after_RF_ind-1:inde+end_ind-1)-d3shim;
%                         SelectedData3 = smooth( DAxis3(inde+after_RF_ind:inde+end_ind)-d3shim,11, 'rloess' );
                        % now sum the data to determine where in KSpace you are
                        DAxis3SumFull = ( cumsum( SelectedData3 ));
                        DAxis3SumSelect = DAxis3SumFull(start_ind-after_RF_ind:end);
                        D3interp = interp1(linspace(1,numel(DAxis3SumSelect),numel(DAxis3SumSelect)),...
                                          DAxis3SumSelect, linspace(1,numel(DAxis3SumSelect),NP));
                        
                        DAxis3Sum(:,ip) = D3interp;
                        
                        % update starting index
                        skipind = inde+end_ind-1;
                    end
                    
                        obj.CAxis1 = cat(2, obj.CAxis1, DAxis1Sum);
                        obj.CAxis2 = cat(2, obj.CAxis2, DAxis2Sum);
                        obj.CAxis3 = cat(2, obj.CAxis3, DAxis3Sum);
                        
            end
            
            % reform axis values
%             obj.CAxis1 = obj.CAxis1(:);
%             obj.CAxis2 = obj.CAxis2(:);
%             obj.CAxis3 = obj.CAxis3(:);
            
        end
        
        function WriteCalibBatchFile(obj,fname)
            % WRITE BATCH FILE AND RUN            
            fid = fopen(fullfile(obj.path_file,fname), 'Wt');
            
            [AcqT, start_measure, end_measure, after_RF, total_measure_time] = obj.CalcTiming;
            
            
            % READ SPEED MULTIPLE
            obj.rdspeed_mult = 10;
            RdSpeed = (40E6) / obj.rdspeed_mult;
            
            % calculate how many points should be measured per acq
            obj.NP_calib = ceil( total_measure_time * RdSpeed / 32 ) * 32;
            
            % check if number of points is too many
            maxNP = (512E6)/2;
            if maxNP < (obj.NP_calib * 4 * 20 )
               obj.rdspeed_mult = 40;
               RdSpeed = (40E6) / obj.rdspeed_mult;

               % calculate how many points should be measured per acq
               obj.NP_calib = ceil( total_measure_time * RdSpeed / 32 ) * 32;
            end
                    
            fprintf( fid, '@echo off\n');
            fprintf( fid, 'SET Debug=%d\n',0);
            fprintf( fid, char(['SET DACfilename=',strrep(obj.path_file,'\','\\\\'),'\\\\ADCdatafile\n']));
            
            fprintf( fid, 'SET nPointsCalib=%u\n', obj.NP_calib );
            fprintf( fid, 'SET rdSpeed=%u\n', obj.rdspeed_mult );
            fprintf( fid, 'SET nTrigs=%u\n', obj.PhasesinThisBatch );
            fprintf( fid, 'echo. | "%s"','C:\ADLINK\ryan\VisualStudioProjects\MultiTrigger\x64\Debug\MultiTrigger.exe'); 
            fprintf( fid, '\nexit');
            
            fclose(fid);
        end
        
    end
end
