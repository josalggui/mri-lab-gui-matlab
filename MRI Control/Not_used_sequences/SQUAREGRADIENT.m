% classdef SQUAREGRADIENT < MRI_BlankSequence_JM    
%     properties
%         maxRepetitionsPerBatch;
%         nPhasesBatch;
%         lastBatch;
%         StartPhase;
%         EndPhase;
%         RepetitionTime;
%         AcquisitionTime;
%     end
%     methods (Access=private)
%         function CreateParameters(obj)
%             CreateOneParameter(obj,'GRADTIME','Gradient Time','us',20*MyUnits.us)
%             CreateOneParameter(obj,'GRADAMPLITUDE','Gradient Amplitude','',0.1)
%             CreateOneParameter(obj,'NREPETITIONS','Number of repetitions','',20)
%             
%             obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT','NSCANS',...
%                 'NPOINTS','NSLICES','FREQUENCY','SPECTRALWIDTH',...
%                 'RFAMPLITUDE','PULSETIME','BLANKINGDELAY','TRANSIENTTIME','COILRISETIME',...
%                 'PHASE','SLICE'}; 
%         end  
%     end
%     
%     
%     %======================== Public Methods =================================
%     methods
%         function obj = SQUAREGRADIENT(program)
%             obj = obj@MRI_BlankSequence_JM();
%             obj.SequenceName = 'SQUARE GRADIENT TEST';
%             obj.ProgramName = 'MRI_BlankSequenceJM.exe';
%             if nargin > 0
%                 obj.ProgramName = program;
%             end
%             obj.CreateParameters();
%             obj.SetDefaultParameters();
%         end
%         
%         function SetDefaultParameters(obj)
%             SetDefaultParameters@MRI_BlankSequence_JM(obj);
%             obj.SetParameter('NPOINTS',1);
%         end
%         
%         function UpdateTETR(obj)
%             obj.TR = 2*obj.GetParameter('GRADTIME');
%         end
%          
%         function CreateSequence( obj, mode )
%             % Calculate the number of line reads
%             nRepetitions = obj.GetParameter('NREPETITIONS');
%             obj.nline_reads = nRepetitions;
%             
%             % Initialize all vectors to zero with the appropriate size
%             obj.nInstructions = 2*(nRepetitions);
%             [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
%            
%             % Timing
%             gt = obj.GetParameter('GRADTIME')*1e6;
%             gradamp = obj.GetParameter('GRADAMPLITUDE');
%             iIns = 1;      
%             
%             switch num2str(obj.GetParameter('READOUT'))
%                 case 'x'
%                     obj.selectReadout = 2;
%                 case 'y'
%                     obj.selectReadout = 0;
%                 case'z'
%                     obj.selectReadout = 1;
%                 otherwise 
%                     obj.selectReadout = 2;
%             end
%             selectReadout = obj.selectReadout;
%              
%             for phasei = 1:nRepetitions
%                 %% Rephasing gradient + Adquisition (1 instruction)
%                 iIns = PulseGradient(obj,iIns,-gradamp,selectReadout,gt);
%                 
%                 %% Dephasing Gradient (1 instruction)
%                 iIns = PulseGradient(obj,iIns,gradamp,selectReadout,gt);
%                 
%             end     
%         end
%         
%         function [status,result] = Run(obj,iAcq)
%             clc
%             [~,~,~,~,~,~,~] = obj.ConfigureGradients(0);
%                         
%             %% Get sequence parameters
%             global rawData;
%             rawData.inputs.sequence = obj.SequenceName;
%             gradtime = obj.GetParameter('GRADIENTTIME'); 
%             rawData.inputs.gradienttime = gradtime;
%             rawData.inputs.gradientamplitude = obj.GetParameter('GRADAMPLITUDE');
%            
%             %% EXECUTE THE SEQUENCE. Create waiting bar
%             obj.maxRepetitionsPerBatch = floor(1024/2);                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
%             nBatches = 1;
%             t = 0;
%             
%             for iBatch = 1:nBatches
%                 % NOW RUN ACTUAL ACQUISTION
%                 [cDataBatch,t,status,result] = RunData(obj,0,t);
%             end
%                
%             if nBatches>1
%                 delete(h_waitbar);
%             end
%             
%             rawData.outputs.data = cDataBatch;
%             
%             % Save rawData
%             time = clock;
%             name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
%                 num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
%             save(fullfile(obj.path_file,name),'rawData');
%         end
%     end
%     
% end





classdef SQUAREGRADIENT < MRI_BlankSequence_JM    
    properties
        maxRepetitionsPerBatch;
        nPhasesBatch;
        lastBatch;
        StartPhase;
        EndPhase;
        RepetitionTime;
        AcquisitionTime;
    end
    methods (Access=private)
        function CreateParameters(obj)
            CreateOneParameter(obj,'GRADTIME','Gradient Time','us',20*MyUnits.us)
            CreateOneParameter(obj,'GRADAMPLITUDE','Gradient Amplitude','',0.1)
            CreateOneParameter(obj,'NREPETITIONS','Number of repetitions','',20)
            CreateOneParameter(obj,'RISETIME','Rise time','us',100*MyUnits.us)
            
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT','NSCANS',...
                'NPOINTS','NSLICES','FREQUENCY','SPECTRALWIDTH',...
                'RFAMPLITUDE','PULSETIME','BLANKINGDELAY','TRANSIENTTIME','COILRISETIME',...
                'PHASE','SLICE'}; 
        end  
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = SQUAREGRADIENT(program)
            obj = obj@MRI_BlankSequence_JM();
            obj.SequenceName = 'SQUARE GRADIENT TEST';
            obj.ProgramName = 'MRI_BlankSequenceJM.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence_JM(obj);
            obj.SetParameter('NPOINTS',1);
        end

         
        function CreateSequence( obj, mode )
            % Calculate the number of line reads
            nRepetitions = obj.GetParameter('NREPETITIONS');
            risetime = obj.GetParameter('RISETIME')*1e6;
            obj.nline_reads = nRepetitions;
            
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions =21*(nRepetitions);
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
           
            % Timing
            gt = obj.GetParameter('GRADTIME')*1e6;
            gradamp = obj.GetParameter('GRADAMPLITUDE');
            iIns = 1;      
            
            switch num2str(obj.GetParameter('READOUT'))
                case 'x'
                    obj.selectReadout = 2;
                case 'y'
                    obj.selectReadout = 0;
                case'z'
                    obj.selectReadout = 1;
                otherwise 
                    obj.selectReadout = 2;
            end
            selectReadout = obj.selectReadout;
             
            for phasei = 1:nRepetitions
                
                for ii=1:10
                    iIns = PulseGradient(obj,iIns,gradamp/10*ii,selectReadout,risetime/10*ii);
                end
                
                %% Dephasing Gradient (1 instruction)
                iIns = PulseGradient(obj,iIns,gradamp,selectReadout,gt);
                
                for ii=1:10
                    iIns = PulseGradient(obj,iIns,gradamp-(gradamp/10*ii),selectReadout,risetime/10*ii);
                end
            end     
        end
        
        function [status,result] = Run(obj,iAcq)
            clc
            [~,~,~,~,~,~,~] = obj.ConfigureGradients(0);
                        
           
            %% EXECUTE THE SEQUENCE. Create waiting bar
            obj.maxRepetitionsPerBatch = floor(1024/21);                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
            nBatches = 1;
            t = 0;
            
            for iBatch = 1:nBatches
                % NOW RUN ACTUAL ACQUISTION
                [cDataBatch,t,status,result] = RunData(obj,0,t);
            end
               
            if nBatches>1
                delete(h_waitbar);
            end
            
        end
    end
    
end