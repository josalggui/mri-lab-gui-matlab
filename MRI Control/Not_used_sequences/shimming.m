classdef shimming < MRI_BlankSequence
    
    properties
        StartSlice;
        EndSlice;
        PhasesPerBatch = 20;
        StartPhase;
        EndPhase;
        slice_ampl;
        Data
    end
    methods (Access=private)
        function CreateParameters(obj)
            InputParameters = {...
                'NPHASES','PHASETIME','ECHODELAY','REPETITIONDELAY','READOUTAMPLITUDE','READOUTAMPLITUDE2',...
                'STARTPHASE',...
                'ENDPHASE','STARTSLICE','ENDSLICE',...
                'GAUSSIANFILTER'...
                };
            
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {...
                'Number of Phases','Phase Time','Gradient Echo Delay','Repetition Delay','DEP Readout Amplitude','REP Readout Amplitude',...
                'Start Phase',...
                'End Phase','Start Slice','End Slice',...
                'Gaussian'...
                })];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,...
                NaN,NaN,NaN,...
                NaN...
                })];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,...
                NaN,NaN,NaN,...
                0 ...
                })];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,...
                NaN,NaN,NaN,...
                1 ...
                })];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {...
                '','us','us','ms','','',...
                '',...
                '','','',...
                ''...
                })];
            
            for k=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{k},InputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = shimming(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'shimmming';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
            obj.SetParameter('PHASETIME',50*MyUnits.us);
            obj.SetParameter('ECHODELAY',20*MyUnits.us);
            obj.SetParameter('REPETITIONDELAY',50*MyUnits.ms);
            obj.SetParameter('READOUTAMPLITUDE',0.1);
            obj.SetParameter('READOUTAMPLITUDE2',0.1);
            obj.SetParameter('NPHASES',1)
            %             obj.SetParameter('NSLICES',1)
            obj.SetParameter('STARTPHASE',-0.1)
            obj.SetParameter('ENDPHASE',0.1)
            obj.SetParameter('STARTSLICE',-0.1)
            obj.SetParameter('ENDSLICE',0.1)
            obj.SetParameter('GAUSSIANFILTER',1);
            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
        end
        function UpdateTETR(obj)
            obj.TE = obj.GetParameter('TRANSIENTTIME') + obj.GetParameter('PHASETIME') + ...
                    obj.GetParameter('ECHODELAY') + 0.5 * obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH');
            obj.TR = obj.TE +  0.5*(obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH')) + 0.5*obj.GetParameter('PULSETIME') +...
                    obj.GetParameter('BLANKINGDELAY') + obj.GetParameter('REPETITIONDELAY');
    
            if obj.spoiler_en
                obj.TR = obj.TR + obj.GetParameter( 'SPOILERTIME' );
            end
            obj.TTotal = obj.TR * obj.GetParameter('NSCANS') * obj.GetParameter('NSLICES') * obj.GetParameter('NPHASES');
        end
        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence,
            %         "1", central frequency adjustment sequence,
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            
            [~,nPhases,readout,slice,phase,spoiler,~] = obj.ConfigureGradients(mode);
            
            % Calculate the number of line reads
            obj.nline_reads=nPhases;
            
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions=17*nPhases;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
            
            % readout gradient amplitude calculation
            if( readout == 0 )
                dephasing_readout = shimR;
                phasing_readout = shimR;
            elseif( obj.GetParameter('PHASETIME') == 0 )
                dephasing_readout = shimR;
                phasing_readout = min(1.0, max( -1.0, shimR -1.0 * obj.GetParameter('READOUTAMPLITUDE2')));
            elseif( acq_time  > 2 * (obj.GetParameter('PHASETIME') + 1e-6)  )
                dephasing_readout = min(1.0, max( -1.0, shimR +1.0 * obj.GetParameter('READOUTAMPLITUDE')));
                phasing_readout = min( 1.0, max( -1.0, shimR -2 *obj.GetParameter('READOUTAMPLITUDE2') * (obj.GetParameter('PHASETIME') + 1e-6) / ( acq_time )));
            else
                phasing_readout = min(1.0, max( -1.0, shimR -1.0 * obj.GetParameter('READOUTAMPLITUDE2')));
                dephasing_readout = max( -1.0, min( 1.0, shimR + obj.GetParameter('READOUTAMPLITUDE') *  acq_time / (2 * (obj.GetParameter('PHASETIME') + 1e-6) )));
            end
            
            %             for slicei = 1:nSlices
            
            if( slice == 0 )
                slice_amplitude = shimS;
                slice_amplitude_2 = shimS;
%                 slice_time = 0.1;
            else
                slice_amplitude_2 = min(1.0, max( -1.0, shimS -1.0 * obj.slice_ampl));
                slice_amplitude   = max(-1.0, min( 1.0, shimS +1.0 * obj.slice_ampl));
%                 slice_time = (obj.GetParameter('PHASETIME')+obj.GetParameter('COILRISETIME'))*1e6;
            end
            
            iIns = 1;
            bbit = 1;
            
            for phasei = 1:nPhases
                phaseo = 0;%mod(phasei-1,0);
                
                % BLANKING DELAY and SHIMMING in all gradients AND PULSE
                % SENT TO ADC FOR ACQUISITION
                nIns  = 3; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [ shimP shimR shimS ];
                    obj.DACs(vIns)     = [ PP RR SS ];
                    obj.WRITEs(vIns)   = [ 1 1 1 ];
                    obj.UPDATEs(vIns)  = [ 1 1 1 ];
                    obj.PhResets(vIns) = [ 0 0 1 ];
                    obj.PHASEs(vIns)   = [ phaseo phaseo phaseo ];
                    obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                    obj.FLAGs(vIns)    = [ 0 0 bbit ];
                    obj.DELAYs(vIns)   = [0.1 0.1 obj.GetParameter('BLANKINGDELAY')*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions
            
                %------ RF pulse (1 instruction)
                nIns = 1; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.TXs(vIns)      = 1;
                    if (obj.RF_Shape) % Shaped RF pulse
                        obj.ENVELOPEs(vIns)= 0;
                    else
                        obj.ENVELOPEs(vIns)= 7;
                    end
                    obj.PHASEs(vIns)   = 0;%2*mod(phasei-1,2);
                    obj.FLAGs(vIns)    = bbit;
                    obj.OPCODEs(vIns)  = 0;
                    obj.DELAYs(vIns)   = obj.GetParameter('PULSETIME')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %----- Transient Delay and setting the slice and readout dephasing gradients (3 instructions)
%                 nIns = 3; % set the number of instruction in this block
%                 vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
%                     obj.AMPs(vIns)     = [ shimS slice_amplitude dephasing_readout ];
%                     obj.DACs(vIns)     = [ SS SS RR ];
%                     obj.WRITEs(vIns)   = [1 1 1];
%                     obj.UPDATEs(vIns)  = [1 1 1];
%                     obj.PhResets(vIns) = [0 0 0];
%                     obj.PHASEs(vIns)   = [phaseo phaseo phaseo];
%                     obj.ENVELOPEs(vIns)= [7 7 7]; % (7=no shape)
%                     obj.FLAGs(vIns)    = [0 0 0];
%                     obj.DELAYs(vIns)   = [obj.GetParameter('TRANSIENTTIME')*1e6 0.1 0.1]; %time in us
%                 iIns = iIns + nIns; % update the number of instructions in this block
                % phase gradient amplitude calculation
                if( phase == 0 )
                    phase_amplitude = shimP;
                    phase_amplitude_2 = shimP;
%                     phase_time = 0.1;
                else
                    phase_amplitude =  min(1.0 , max(-1.0,shimP + (obj.EndPhase - obj.StartPhase) * (phasei-1)/(nPhases-1) + obj.StartPhase));
                    phase_amplitude_2 =  min(1.0 , max(-1.0,shimP - ( (obj.EndPhase - obj.StartPhase) * (phasei-1)/(nPhases-1) + obj.StartPhase ) ));
%                     phase_time = (obj.GetParameter('PHASETIME')+obj.GetParameter('COILRISETIME'))*1e6;
                end
                
                nIns = 3; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [ shimS slice_amplitude phase_amplitude];
                    obj.DACs(vIns)     = [ SS SS PP ];
                    obj.WRITEs(vIns)   = [1 1 1];
                    obj.UPDATEs(vIns)  = [1 1 1];
                    obj.PhResets(vIns) = [0 0 0];
                    obj.PHASEs(vIns)   = [phaseo phaseo phaseo];
                    obj.ENVELOPEs(vIns)= [7 7 7]; % (7=no shape)
                    obj.FLAGs(vIns)    = [0 0 0];
                    obj.DELAYs(vIns)   = [obj.GetParameter('TRANSIENTTIME')*1e6 0.1 0.1]; %time in us
                    
                iIns = iIns + nIns; % update the number of instructions in this block
                
                
                %----- Phase gradient, rephasing readout gradient, reset slice and phase gradients and echo delay (4 instructions)
                nIns = 4;
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [dephasing_readout shimP phasing_readout shimS ];
                    obj.DACs(vIns)     = [RR PP RR SS];
                    obj.WRITEs(vIns)   = [1 1 1 1];
                    obj.UPDATEs(vIns)  = [1 1 1 1];
                    obj.PHASEs(vIns)   = [phaseo phaseo phaseo phaseo];
                    obj.ENVELOPEs(vIns)= [7 7 7 7]; % (7=no shape)
                    obj.DELAYs(vIns)   = [(obj.GetParameter('PHASETIME')+obj.GetParameter('COILRISETIME'))*1e6 0.1 obj.GetParameter('COILRISETIME')*1e6 obj.GetParameter('ECHODELAY')*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %----- Rephasing Readout Gradient and Data Acquisition. Rewinding lobe for readout, slice and phase gradients (4 instructions)
                nIns = 4;
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [phasing_readout shimR slice_amplitude_2 phase_amplitude_2];
                    obj.DACs(vIns)     = [RR RR SS PP];
                    obj.WRITEs(vIns)   = [1 1 1 1];
                    obj.UPDATEs(vIns)  = [1 1 1 1];
                    obj.PHASEs(vIns)   = [phaseo phaseo phaseo phaseo];
                    obj.RXs(vIns)      = [1 0 0 0];
                    obj.ENVELOPEs(vIns)= [7 7 7 7]; % (7=no shape)
                    obj.DELAYs(vIns)   = [acq_time*1e6 obj.GetParameter('COILRISETIME')*1e6 0.1 (obj.GetParameter('PHASETIME')+obj.GetParameter('COILRISETIME'))*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %----- Spoiler for slice gradient (1 instruction)
                nIns = 1;
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    if (spoiler)
                        obj.AMPs(vIns)     = obj.GetParameter('SPOILERAMP');
                    else
                        obj.AMPs(vIns)     = shimS;
                    end
                    obj.DACs(vIns)     = SS;
                    obj.WRITEs(vIns)   = 1;
                    obj.UPDATEs(vIns)  = 1;
                    obj.CLEARs(vIns)   = 1;
                    obj.PHASEs(vIns)   = phaseo;
                    obj.FLAGs(vIns)    = 0;
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.DELAYs(vIns)   = obj.GetParameter('SPOILERTIME')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %----- Repetition Delay (1 instruction)
                nIns = 1;
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.WRITEs(vIns)   = 1;
                    obj.UPDATEs(vIns)  = 1;
                    obj.PHASEs(vIns)  = phaseo;
                    obj.CLEARs(vIns)   = 1;
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.DELAYs(vIns)   = obj.GetParameter('REPETITIONDELAY')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
            end % end for nPhases
            %             end %end for slicei
            
        end % end create gradient echo pulse sequence
        
        
        
        function SelectShimming(obj)
            %Definimos el salto m�s fino permitido para el shimming
%             steep = 0.0001;
%  
%             obj.shimS_en=1;
%             obj.shimP_en=1;
%             obj.shimR_en=1;
%             obj.SetParameter('SHIMSLICE',0);
%             obj.SetParameter('SHIMPHASE',0);
%             obj.SetParameter('SHIMREADOUT',0);
%             
%             shim = -10*steep:steep:10*steep;
%             I=zeros(obj.GetParameter('NPOINTS'),length(shim));
%             suma=zeros(1,length(shim));
%             for(ii=1:length(shim))
%                 obj.SetParameter('SHIMSLICE',shim(1,ii))
%                 t=0;
%                 [cDataBatch,tfinal,status,result] = RunData(obj,0,t);
%                 I(:,ii) = (abs(cDataBatch));
%                 suma(1,ii) = sum((abs(cDataBatch)));
%                
%             end

            clc
            

            %SHIMMINIG SLICE
            disp('First shimming slice')
            cond = 0;
            obj.shimS_en=1;
            steep = 0.0001;
            
            t=0;
            [cDataBatch,~,~,~] = RunData(obj,0,t);
            
            
            
            obj.SetParameter('SHIMSLICE',0);
            t=0;
            [cDataBatch,~,~,~] = RunData(obj,0,t);
            I0 = sum(abs(cDataBatch));

            obj.SetParameter('SHIMSLICE',+steep)
            t=0;
            [cDataBatch,~,~,~] = RunData(obj,0,t);
            I1 = sum(abs(cDataBatch));

            if(I1>I0)
                steep = +steep;
            else
                steep = -steep;
            end
            
            slicepath(1,:) = [0 I0];
            data(1,:)=[0 0 0 I0];
            i=2;
            k=2;
            obj.SetParameter('SHIMSLICE',0);
            while(cond == 0)
                obj.SetParameter('SHIMSLICE',obj.GetParameter('SHIMSLICE')+steep)
                t=0;
                [cDataBatch,~,~,~] = RunData(obj,0,t);
                I1 = sum(abs(cDataBatch));
                slicepath (k,:)=[obj.GetParameter('SHIMSLICE') I1];
                data(i,:)=[obj.GetParameter('SHIMSLICE') 0 0 I1];
                i=i+1;
                if(I1<I0)
                    cond = 1;
                    obj.SetParameter('SHIMSLICE',obj.GetParameter('SHIMSLICE')-steep)
                end

                k=k+1;
                if (k==25)
                    cond = 1;
                end
                I0 = I1;
                slice1 = obj.GetParameter('SHIMSLICE');
            end



            %SHIMMING PHASE
            disp('First shimming phase')
            cond = 0;
            obj.shimP_en=1;
            steep = 0.0001;
            
            obj.SetParameter('SHIMPHASE',0);
            t=0;
            [cDataBatch,~,~,~] = RunData(obj,0,t);
            I0 = sum(abs(cDataBatch));

            obj.SetParameter('SHIMPHASE',+steep)
            t=0;
            [cDataBatch,~,~,~] = RunData(obj,0,t);
            I1 = sum(abs(cDataBatch));

            if(I1>I0)
                steep = +steep;
            else
                steep = -steep;
            end
            
            phasepath(1,:) = [0 I0];
            data(i,:)=[obj.GetParameter('SHIMSLICE') 0 0 I0];
            i=i+1;
            k=2;
            obj.SetParameter('SHIMPHASE',0);
            while(cond == 0)
                obj.SetParameter('SHIMPHASE',obj.GetParameter('SHIMPHASE')+steep)
                t=0;
                [cDataBatch,~,~,~] = RunData(obj,0,t);
                I1 = sum(abs(cDataBatch));
                phasepath (k,:)=[obj.GetParameter('SHIMPHASE') I1];
                data(i,:)=[obj.GetParameter('SHIMSLICE') obj.GetParameter('SHIMPHASE') 0 I1];
                i=i+1;
                
                if(I1<I0)
                    cond = 1;
                    obj.SetParameter('SHIMPHASE',obj.GetParameter('SHIMPHASE')-steep)
                end

                k=k+1;
                
                if (k==25)
                    cond = 1;
                end
                I0 = I1;
                phase1 = obj.GetParameter('SHIMPHASE');
            end 
           


            %SHIMMING READOUT
            disp('First shimming readout')
            cond = 0;
            obj.shimR_en=1;
            steep = 0.0001;
            
            obj.SetParameter('SHIMREADOUT',0);
            t=0;
            [cDataBatch,~,~,~] = RunData(obj,0,t);
            I0 = sum(abs(cDataBatch));

            obj.SetParameter('SHIMREADOUT',+steep)
            t=0;
            [cDataBatch,~,~,~] = RunData(obj,0,t);
            I1 = sum(abs(cDataBatch));

            if(I1>I0)
                steep = +steep;
            else
                steep = -steep;
            end
            
            readoutpath(1,:) = [0 I0];
            data(i,:)=[obj.GetParameter('SHIMSLICE') obj.GetParameter('SHIMPHASE') 0 I0];
            i=i+1;
            k=2;
            obj.SetParameter('SHIMREADOUT',0);
            while(cond == 0)
                obj.SetParameter('SHIMREADOUT',obj.GetParameter('SHIMREADOUT')+steep)
                t=0;
                [cDataBatch,~,~,~] = RunData(obj,0,t);
                I1 = sum(abs(cDataBatch));
                readoutpath (k,:)=[obj.GetParameter('SHIMREADOUT') I1];
                data(i,:)=[obj.GetParameter('SHIMSLICE') obj.GetParameter('SHIMPHASE') obj.GetParameter('SHIMREADOUT') I1];
                i=i+1;
                if(I1<I0)
                    cond = 1;
                    obj.SetParameter('SHIMREADOUT',obj.GetParameter('SHIMREADOUT')-steep)
                end

                k=k+1;
                if (k==25)
                    cond = 1;
                end
                I0 = I1;
                readout1 = obj.GetParameter('SHIMREADOUT');
            end 
            
            
            %% Second iteration
            %SHIMMINIG SLICE
            disp('Second shimming slice')
            cond = 0;
            steep = 0.0001;
            
            t=0;
            [cDataBatch,~,~,~] = RunData(obj,0,t);
            I0 = sum(abs(cDataBatch));
            
            obj.SetParameter('SHIMSLICE',slice1+steep)
            t=0;
            [cDataBatch,~,~,~] = RunData(obj,0,t);
            I1 = sum(abs(cDataBatch));

            if(I1>I0)
                steep = +steep;
            else
                steep = -steep;             
            end
            
            slicepath2(1,:) = [slice1 I0];
            data(i,:)=[slice1 obj.GetParameter('SHIMPHASE') obj.GetParameter('SHIMREADOUT') I0];
            i=i+1;
            k=2;
            obj.SetParameter('SHIMSLICE',slice1)
            while(cond == 0)
                obj.SetParameter('SHIMSLICE',obj.GetParameter('SHIMSLICE')+steep)
                t=0;
                [cDataBatch,~,~,~] = RunData(obj,0,t);
                I1 = sum(abs(cDataBatch));
                slicepath2 (k,:)=[obj.GetParameter('SHIMSLICE') I1];
                data(i,:)=[obj.GetParameter('SHIMSLICE') obj.GetParameter('SHIMPHASE') obj.GetParameter('SHIMREADOUT') I1];
                i=i+1;
                if(I1<I0)
                    cond = 1;
                    obj.SetParameter('SHIMSLICE',obj.GetParameter('SHIMSLICE')-steep)
                end

                k=k+1;
                if (k==25)
                    cond = 1;
                end
                I0 = I1;
                slice2 = obj.GetParameter('SHIMSLICE');
            end
            
            
            %SHIMMINIG PHASE
            disp('Second shimming phase')
            cond = 0;
            steep = 0.0001;
            
            t=0;
            [cDataBatch,~,~,~] = RunData(obj,0,t);
            I0 = sum(abs(cDataBatch));
            
            obj.SetParameter('SHIMPHASE',phase1+steep)
            t=0;
            [cDataBatch,~,~,~] = RunData(obj,0,t);
            I1 = sum(abs(cDataBatch));
            
            data(i,:)=[obj.GetParameter('SHIMSLICE') phase1 obj.GetParameter('SHIMREADOUT') I0];
            i=i+1;
            if(I1>I0)
                steep = +steep;
            else
                steep = -steep;             
            end
            
            phasepath2(1,:) = [phase1 I0];
            k=2;
            obj.SetParameter('SHIMPHASE',phase1)
            while(cond == 0)
                obj.SetParameter('SHIMPHASE',obj.GetParameter('SHIMPHASE')+steep)
                t=0;
                [cDataBatch,~,~,~] = RunData(obj,0,t);
                I1 = sum(abs(cDataBatch));
                phasepath2 (k,:)=[obj.GetParameter('SHIMPHASE') I1];
                data(i,:)=[obj.GetParameter('SHIMSLICE') obj.GetParameter('SHIMPHASE') obj.GetParameter('SHIMREADOUT') I1];
                i=i+1;
                if(I1<I0)
                    cond = 1;
                    obj.SetParameter('SHIMPHASE',obj.GetParameter('SHIMPHASE')-steep)
                end

                k=k+1;
                if (k==25)
                    cond = 1;
                end
                I0 = I1;
                phase2 = obj.GetParameter('SHIMPHASE');
            end
            
            
            %SHIMMINIG READOUT
            disp('Second shimming readout')
            cond = 0;
            steep = 0.0001;
            
            t=0;
            [cDataBatch,~,~,~] = RunData(obj,0,t);
            I0 = sum(abs(cDataBatch));
            
            obj.SetParameter('SHIMREADOUT',readout1+steep)
            t=0;
            [cDataBatch,~,~,~] = RunData(obj,0,t);
            I1 = sum(abs(cDataBatch));

            if(I1>I0)
                steep = +steep;
            else
                steep = -steep;             
            end
            
            readoutpath2(1,:) = [readout1 I0];
            data(i,:)=[obj.GetParameter('SHIMSLICE') obj.GetParameter('SHIMPHASE') readout1 I0];
            i=i+1;
            k=2;
            obj.SetParameter('SHIMREADOUT',readout1)
            while(cond == 0)
                obj.SetParameter('SHIMREADOUT',obj.GetParameter('SHIMREADOUT')+steep)
                t=0;
                [cDataBatch,~,~,~] = RunData(obj,0,t);
                I1 = sum(abs(cDataBatch));
                readoutpath2 (k,:)=[obj.GetParameter('SHIMREADOUT') I1];
                data(i,:)=[obj.GetParameter('SHIMSLICE') obj.GetParameter('SHIMPHASE') obj.GetParameter('SHIMREADOUT') I1];
                i=i+1;
                if(I1<I0)
                    cond = 1;
                    obj.SetParameter('SHIMREADOUT',obj.GetParameter('SHIMREADOUT')-steep)
                end

                k=k+1;
                if (k==25)
                    cond = 1;
                end
                I0 = I1;
                readout2 = obj.GetParameter('SHIMREADOUT');
                
                disp(slice2)
                disp(phase2)
                disp(readout2)
            end
            
        end
        

        function [status,result] = Run(obj,iAcq)
            global MRIDATA
            
            obj.StartSlice = obj.GetParameter('STARTSLICE');
            obj.EndSlice = obj.GetParameter('ENDSLICE');
            obj.StartPhase = obj.GetParameter('STARTPHASE');
            obj.EndPhase = obj.GetParameter('ENDPHASE');
            nSlices = obj.GetParameter('NSLICES');
            if nSlices > 1
                SliceStep = (obj.EndSlice - obj.StartSlice)/(nSlices-1);
            else
                SliceStep = 0;
            end
            nPhases = obj.GetParameter('NPHASES');
            if nPhases > 1
                PhaseStep = (obj.EndPhase - obj.StartPhase)/(nPhases-1);
            else
                PhaseStep = 0;
            end
            nPhaseBatches = ceil(nPhases/obj.PhasesPerBatch);
            nPoints = obj.GetParameter('NPOINTS');
            Data3D = zeros(nPoints,nPhases,nSlices);
            
            if isfield(MRIDATA,'Data3Dnew')
                if ( isequal(size(MRIDATA.Data3Dnew),[nPoints, nPhases, nSlices, obj.NAverages])) || (iAcq == 1)
                    MRIDATA.Data3Dnew = [];
                end
                else
                    MRIDATA.Data3Dnew = [];
            end
            
            cn = nSlices*nPhaseBatches;
            count = 0;
            if nPhaseBatches>1
                h_waitbar = waitbar(count/cn,'Please wait (por favor espere)...');
            end
            
            obj.SelectShimming;
            
            
            for iSlice = 1:nSlices
                
                obj.StartSlice = obj.GetParameter('STARTSLICE') + (iSlice-1)*SliceStep;
                obj.EndSlice = obj.StartSlice;
                
                [shimS,~,~]=SelectAxes(obj);
                
                if (nSlices == 1)
                    obj.slice_ampl = obj.StartSlice;
                else
                    obj.slice_ampl =  min(1.0 , max(-1.0,shimS + (obj.EndSlice - obj.StartSlice) * (iSlice-1)/(nSlices-1) + obj.StartSlice));
                end
                
                Data2D = [];
                for iBatch = 1:nPhaseBatches
                    
                    if nPhases >1
                        obj.CreateSequence(1); % Create the gradient echo sequence for CF
                        obj.Sequence2String;
                        fname_CF = 'TempBatch_CF.bat';
                        obj.WriteBatchFile(fname_CF);
                        obj.CenterFrequency(fname_CF);
                    end
                    obj.SetParameter('LASTFREQUENCY',obj.GetParameter('FREQUENCY'));
                    
                    
                    % NOW RUN ACTUAL ACQUISTION
                    obj.StartPhase = obj.GetParameter('STARTPHASE') + (iBatch-1)*obj.PhasesPerBatch*PhaseStep;
                    obj.EndPhase = obj.StartPhase + (obj.PhasesPerBatch-1)*PhaseStep;
                    
                    obj.CreateSequence(0); % Create the normal gradient echo sequence
                    
                    obj.Sequence2String;
                    
                    
                    fname = 'TempBatch.bat';
                    obj.WriteBatchFile(fname);
                    [status,result] = system(fullfile(obj.path_file,fname));
                    
                    if status == 0 && ~isempty(result)
                        C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                        res = C{:};
                        obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                        obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                        obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                        obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                        obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                    end
                    
                    DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                    cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                    nPhasesInBatch=length(cDataBatch)/nPoints;
                    d = reshape(cDataBatch,nPoints,nPhasesInBatch);
                    
                    % reshape based on the phase angle
%                     p0 = 117*(pi/180);
%                     phase_angle_list = [p0, 2.0*p0, 4.0*p0, 7.0*p0, 11.0*p0, 16.0*p0, 22.0*p0, 29.0*p0, 37.0*p0, 46.0*p0, 56.0*p0, 67.0*p0, 79.0*p0, 92.0*p0 ];
%                     phase_angle = phase_angle_list(mod([0:nPhasesInBatch-1],0)+1);
%                     phase_angle = pi*mod((1:nPhasesInBatch)-1,2);
%                     phase_angle = repmat(phase_angle(:)',nPoints,1);
%                     d = d - exp(complex(0,phase_angle));
                    
                    Data2D = cat(2,Data2D,d);
                    count = count+1;
                    if nPhaseBatches>1
                        waitbar(count/cn,h_waitbar,sprintf('Por favor espere..... %5.2f %%',100*count/cn));
                    end
                end
                Data3D(:,:,iSlice) = Data2D;
                MRIDATA.Data3Dnew(end+1).Fid3D = Data3D;
                if length(obj.cData) ~= nPoints*nPhases*nSlices;
                    obj.cData = zeros(obj.NAverages,nPoints*nPhases*nSlices);
                end
                if obj.averaging
                    Nav = obj.NAverages;
                else
                    Nav = 1;
                end
                index = mod(iAcq-1,Nav)+1;
                if obj.autoPhasing
                    %                    pt = max(Data3D(:,ceil(size(Data3D,2)/2),ceil(size(Data3D,3)/2)));
                    pt = max(Data3D(:));
                    Data3D = Data3D.*exp(complex(0,-angle(pt)));
                end
                obj.cData(index,1:length(Data3D(:))) = Data3D(:);
                obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
                obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
                %    obj.fData1D = fftshift(ifft(fftshift(obj.cDataAv)));
                obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
                acq_time = obj.GetParameter('ACQUISITIONTIME');
                obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*acq_time/nPhases;
                obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time/nPhases;
                MRIDATA.obj = obj;
            end
%             MRIDATA.Data3Dnew(:,:,:,index) = Data3D;
            
            if nPhaseBatches>1
                delete(h_waitbar);
            end
        end
    end
    
end
