classdef SpinEchoSlice_blank< MRI_BlankSequence
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        SliceGradient;
        StartSliceFreq;
        EndSliceFreq;
        PhasesPerBatch = 20;
        StartPhase;
        EndPhase;
        nPhasesBatch;
    end
    methods (Access=private)
        function CreateParameters(obj)
            InputParameters = {...
                'NPHASES','PHASETIME','TAUTIME','ECHODELAY','NOECHODELAY','ECHODEPHASINGRATIO','REPETITIONDELAY','READOUTAMPLITUDE',...
                'STARTPHASE',...
                'ENDPHASE','SLICEGRADIENT','STARTSLICEFREQ', 'ENDSLICEFREQ', ...
                'TIMEBETWEEN' ...
                };
            
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {...
                'Number of Phases','Phase Time','Tau Time','Readout Echo Delay', 'No-Readout Echo Delay','Echo Dephasing Ratio','Repetition Delay','Readout Amplitude',...
                'Start Phase',...
                'End Phase','Slice Gradient','Start Slice Freq','End Slice Freq',...
                'Time between Phases' ...
                })];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,...
                NaN,...
                NaN,NaN,NaN,NaN,...
                NaN ...
                })];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,0.1,NaN,NaN,...
                NaN,...
                NaN,NaN,NaN,NaN,...
                NaN ...
                })];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,2,NaN,NaN,...
                NaN,...
                NaN,NaN,NaN,NaN,...
                NaN ...
                })];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {...
                '','us','us','us','us','','ms','',...
                '',...
                '','','kHz','kHz',...
                'ms' ...
                })];
            OutputParameters = {
                'DEPHASINGREADOUTAMPLITUDE','PHASINGREADOUTAMPLITUDE','ACTUALPHASETIME'...
                };
            obj.OutputParNames = [containers.Map(obj.OutputParNames.keys(),obj.OutputParNames.values());containers.Map(OutputParameters,...
                {...
                'Dephasing Readout Amplitude', 'Phasing Readout Amplitude','Actual Phase Time'...
                })];
            obj.OutputParValues = [containers.Map(obj.OutputParValues.keys(),obj.OutputParValues.values());containers.Map(OutputParameters,...
                {...
                NaN,NaN,NaN...
                })];
            obj.OutputParUnits = [containers.Map(obj.OutputParUnits.keys(),obj.OutputParUnits.values());containers.Map(OutputParameters,...
                {...
                '','','us'...
                })];
            for i=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{i},InputParameters)) = [];
            end
            for i=1:length(obj.OutputParameters)
                OutputParameters(strcmp(obj.OutputParameters{i},OutputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
            obj.OutputParameters = [obj.OutputParameters,OutputParameters];
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = SpinEchoSlice_blank(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'Spin Echo with Slice blank';
            obj.ProgramName = 'MRI_BlankSequence.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
            obj.SetParameter('NPHASES',1);
            obj.SetParameter('TAUTIME',150*MyUnits.us);
            obj.SetParameter('PHASETIME',50*MyUnits.us);
            obj.SetParameter('ECHODELAY',20*MyUnits.us);
            obj.SetParameter('NOECHODELAY',10*MyUnits.us);
            obj.SetParameter('ECHODEPHASINGRATIO',1);
            obj.SetParameter('REPETITIONDELAY',50*MyUnits.ms);
            obj.SetParameter('READOUTAMPLITUDE',0.4);
            obj.SetParameter('STARTPHASE',-0.1);
            obj.SetParameter('ENDPHASE',0.1);
            obj.SetParameter('SLICEGRADIENT',0.1);
            obj.SetParameter('STARTSLICEFREQ',-100*MyUnits.kHz);
            obj.SetParameter('ENDSLICEFREQ',100*MyUnits.kHz);
            obj.SetParameter('TIMEBETWEEN',100*MyUnits.ms);
            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
        end
        function UpdateTETR(obj)
            obj.TE = obj.GetParameter('TAUTIME')+obj.GetParameter('PULSETIME')+obj.GetParameter('NOECHODELAY')+obj.GetParameter('ECHODELAY')+...
                2*obj.GetParameter('COILRISETIME')+ 0.5*(obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH'));
            obj.TR = obj.TE +  0.5*(obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH')) + 0.5*obj.GetParameter('PULSETIME') +...
                obj.GetParameter('BLANKINGDELAY') + obj.GetParameter('REPETITIONDELAY');

            if obj.spoiler_en
                obj.TR = obj.TR + obj.GetParameter( 'SPOILERTIME' );
            end

            obj.TTotal = obj.TR * obj.GetParameter('NSCANS') * obj.GetParameter('NSLICES') * obj.GetParameter('NPHASES'); 
        end
        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence,
            %         "1", central frequency adjustment sequence,
                        
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
                        
            [~,nPhases,readout,slice,phase,spoiler,~] = obj.ConfigureGradients(mode);
            
            % Calculate the number of line reads
            obj.nline_reads=nPhases;
            
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions = 1;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
            
            rephasing_slice_time = 0.88*obj.GetParameter('PULSETIME');
            
            if( readout == 0 ) 
              dephasing_readout = shimR; 
              phasing_readout = shimR; 
            elseif( obj.GetParameter('TAUTIME') == 0 ) 
              dephasing_readout = shimR; 
              phasing_readout = min(1.0, max( -1.0, shimR +1.0 * obj.GetParameter('READOUTAMPLITUDE')));
            elseif( acq_time  > 2 * (obj.GetParameter('PHASETIME') + 1e-6) ) 
              dephasing_readout = min(1.0, max( -1.0, shimR +1.0 * obj.GetParameter('READOUTAMPLITUDE')));
              phasing_readout = min( 1.0, max( -1.0, shimR +2 *obj.GetParameter('READOUTAMPLITUDE') * (obj.GetParameter('PHASETIME') + 1e-6) / ( acq_time)));
            else 
              phasing_readout = min(1.0, max( -1.0, shimR +1.0 * obj.GetParameter('READOUTAMPLITUDE')));
              dephasing_readout = min( 1.0, max( -1.0, shimR + obj.GetParameter('READOUTAMPLITUDE') *  acq_time / (2 * (obj.GetParameter('PHASETIME') + 1e-6))));
            end

            dephasingRatio = min( max(1.0, obj.GetParameter('ECHODEPHASINGRATIO') ), 2);
	        dephasing_readout=dephasing_readout*dephasingRatio;
            
            obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',dephasing_readout);
            obj.SetParameter('PHASINGREADOUTAMPLITUDE',phasing_readout);     
        
            %phase time limitation and time filling tau calculation (modification 8)
            phase_time=obj.GetParameter('PHASETIME');
            accumul_tau_time=phase_time+obj.GetParameter('TRANSIENTTIME')+1.5*obj.GetParameter('PULSETIME')+ rephasing_slice_time + 6*obj.GetParameter('COILRISETIME');
            
            time_filling_tau=0.5*(obj.GetParameter('TAUTIME') - accumul_tau_time);
                        
            if (time_filling_tau < 0)
              phase_time=max(phase_time+ 2*time_filling_tau,0.1e-6); %us
              time_filling_tau=0.1e-6;
            end

            obj.SetParameter('ACTUALPHASETIME',phase_time);
            
            %%%%%% --- WRITING IN THE Spin Echo SEQUENCE --- %%%%%%%
            % starting instructions
            instr = 1;
            

            if( slice == 0 )
                slice_amplitude = shimS;
            else
                slice_amplitude =  min(1.0 , max(-1.0,shimS + obj.GetParameter('SLICEGRADIENT')));
            end
            for phasei = 1:nPhases
                
                %------ Blanking Delay and Shimming in all gradients
                choice_v{1} = [ shimP shimR shimS; PP RR SS ];
                choice_v{2} = 0;
                time_duration = obj.GetParameter('BLANKINGDELAY')*1e6;
                [ obj, instr, choice_v ] = ProgramInstruction( 'SETBLANKING', instr, time_duration, choice_v, obj );
                %----- Set the dephasing slice pulse
                choice_v{1} = [ slice_amplitude ; shimS ; SS ];
                choice_v{2} = 1;
                choice_v{3} = obj.GetParameter('COILRISETIME')*1e6;
                time_duration = 0.1;
                [ obj, instr, choice_v ] = ProgramInstruction( 'SETGRADSRISE', instr, time_duration, choice_v, obj );
                %------ RF pulse 90 grads
                choice_v{1} = obj.RF_Shape;
                choice_v{2} = 0;
                choice_v{3} = 0;
                choice_v{4} = slice;
                time_duration = obj.GetParameter('PULSETIME')*1e6;
                [ obj, instr, choice_v ] = ProgramInstruction( 'RFPulse', instr, time_duration, choice_v, obj );
                %----- Reset the slice amplitude
                choice_v{1} = [ slice_amplitude ; shimS ; SS ];
                choice_v{2} = 0;
                choice_v{3} = obj.GetParameter('COILRISETIME')*1e6;
                time_duration = 0.1;
                [ obj, instr, choice_v ] = ProgramInstruction( 'SETGRADSFALL', instr, time_duration, choice_v, obj );
                %------ Rephasing Slice Pulse
                choice_v{1} = [ -slice_amplitude ; shimS ; SS ];
                choice_v{2} = 0;
                choice_v{3} = obj.GetParameter('COILRISETIME')*1e6;
                time_duration = rephasing_slice_time*1e6;
                [ obj, instr, choice_v ] = ProgramInstruction( 'SETGRADSTRAPMATCH', instr, time_duration, choice_v, obj );
                %----- Transient Delay and first tau filling
                choice_v{1} = [ shimS; SS ];
                choice_v{2} = 0;
                time_duration = (obj.GetParameter('TRANSIENTTIME')+time_filling_tau)*1e6;
                [ obj, instr, choice_v ] = ProgramInstruction( 'SETGRADS', instr, time_duration, choice_v, obj );
                
                % phase gradient amplitude calculation
                if( phase == 0 )
                    phase_amplitude = shimP;
                else
                    phase_amplitude =  min(1.0 , max(-1.0,shimP + (obj.EndPhase - obj.StartPhase) * (phasei-1)/(nPhases-1) + obj.StartPhase));
                end
                
                %----- Setting the readout and phase dephasing gradients
                choice_v{1} = [ dephasing_readout phase_amplitude; shimR shimP; RR PP];
                choice_v{2} = 0;
                choice_v{3} = obj.GetParameter('COILRISETIME')*1e6;
                time_duration = phase_time*1e6;
                [ obj, instr, choice_v ] = ProgramInstruction( 'SETGRADSTRAPMATCH', instr, time_duration, choice_v, obj );
                %----- Second tau filling
                choice_v{1} = [shimP shimR shimS ; PP RR SS ];
                choice_v{2} = 0;
                time_duration = time_filling_tau*1e6;
                [ obj, instr, choice_v ] = ProgramInstruction( 'SETGRADS', instr, time_duration, choice_v, obj );
                %----- Set the dephasing slice pulse
                choice_v{1} = [ slice_amplitude ; shimS ; SS ];
                choice_v{2} = 0;
                choice_v{3} = obj.GetParameter('COILRISETIME')*1e6;
                time_duration = 0.1;
                [ obj, instr, choice_v ] = ProgramInstruction( 'SETGRADSRISE', instr, time_duration, choice_v, obj );
                %------ RF pulse 180 grads
                choice_v{1} = obj.RF_Shape;
                choice_v{2} = 0;
                choice_v{3} = 1;
                choice_v{4} = slice; 
                time_duration = 2.0*obj.GetParameter('PULSETIME')*1e6;
                [ obj, instr, choice_v ] = ProgramInstruction( 'RFPulse', instr, time_duration, choice_v, obj );
                %----- Reset the slice amplitude
                choice_v{1} = [ slice_amplitude ; shimS ; SS ];
                choice_v{2} = 0;
                choice_v{3} = obj.GetParameter('COILRISETIME')*1e6;
                time_duration = 0.1;
                [ obj, instr, choice_v ] = ProgramInstruction( 'SETGRADSFALL', instr, time_duration, choice_v, obj );
                %----- No-RO echo delay
                choice_v{1} = [ shimS; SS ];
                choice_v{2} = 0;
                time_duration = obj.GetParameter('NOECHODELAY')*1e6;
                [ obj, instr, choice_v ] = ProgramInstruction( 'SETGRADS', instr, time_duration, choice_v, obj );
                %----- Set the phasing readout amplitude
                choice_v{1} = [ phasing_readout ; shimR ; RR ];
                choice_v{2} = 0;
                choice_v{3} = obj.GetParameter('COILRISETIME')*1e6;
                time_duration = obj.GetParameter('ECHODELAY')*1e6;
                [ obj, instr, choice_v ] = ProgramInstruction( 'SETGRADSRISE', instr, time_duration, choice_v, obj );
                %----- Read-in the data
                choice_v{1} = [ phasing_readout ; RR ];
                choice_v{2} = 0;
                time_duration = acq_time*1e6;
                [ obj, instr, choice_v ] = ProgramInstruction( 'RXs', instr, time_duration, choice_v, obj );
                %----- Reset the phasing readout amplitude
                choice_v{1} = [ phasing_readout ; shimR ; RR ];
                choice_v{2} = 0;
                choice_v{3} = obj.GetParameter('COILRISETIME')*1e6;
                time_duration = 0.1;
                [ obj, instr, choice_v ] = ProgramInstruction( 'SETGRADSFALL', instr, time_duration, choice_v, obj );
                %----- Spoiler for slice gradient
                if (spoiler)
                    choice_v{1} = [ obj.GetParameter('SPOILERAMP'); SS ];
                    time_duration = obj.GetParameter('SPOILERTIME')*1e6;
                else
                    choice_v{1} = [ shimS; SS ];
                    time_duration = 0.1;
                end
                choice_v{2} = 0;
                [ obj, instr, choice_v ] = ProgramInstruction( 'SETGRADS', instr, time_duration, choice_v, obj );
                %----- Repetition Delay (1 instruction)
                time_duration = obj.GetParameter('REPETITIONDELAY')*1e6;
                [ obj, instr, choice_v ] = ProgramInstruction( 'RESET', instr, time_duration, choice_v, obj );
            end % end for nPhases
            % RECALCULATE the number of instructions based upon vector length
            obj.nInstructions = numel( obj.AMPs );
        end % end create spin echo pulse sequence

        function [status,result] = Run(obj,iAcq)
            obj.SliceGradient = obj.GetParameter('SLICEGRADIENT');
            obj.StartSliceFreq = obj.GetParameter('STARTSLICEFREQ');
            obj.EndSliceFreq = obj.GetParameter('ENDSLICEFREQ');
            obj.StartPhase = obj.GetParameter('STARTPHASE');
            obj.EndPhase = obj.GetParameter('ENDPHASE');
            nSlices = obj.GetParameter('NSLICES');
            if nSlices > 1
                SliceStep = (obj.EndSliceFreq - obj.StartSliceFreq)/(nSlices-1);
            else
                SliceStep = 0;
            end
            nPhases = obj.GetParameter('NPHASES');
            if nPhases > 1
                PhaseStep = (obj.EndPhase - obj.StartPhase)/(nPhases-1);
            else
                PhaseStep = 0;
            end
            nPhaseBatches = ceil(nPhases/obj.PhasesPerBatch);
            nPoints = obj.GetParameter('NPOINTS');
            Data3D = zeros(nPoints,nPhases,nSlices);
            
            pausingtime = obj.GetParameter('TIMEBETWEEN');
            cn = nSlices*nPhaseBatches;
            count = 0;
            if nPhaseBatches>1
                h_waitbar = waitbar(count/cn,'Please wait (por favor espere)...');
            end
            for iSlice = 1:nSlices
                obj.SliceFreqOffset = ( obj.StartSliceFreq + (iSlice-1)*SliceStep ) ;
                Data2D = [];
                for iBatch = 1:nPhaseBatches                   
                    if nPhases >1
                        obj.CreateSequence(1); % Create the spin echo sequence for CF
                        obj.Sequence2String;
                        fname_CF = 'TempBatch_CF.bat';
                        obj.WriteBatchFile(fname_CF);
                        obj.CenterFrequency(fname_CF);
                    end
                    obj.SetParameter('LASTFREQUENCY',obj.GetParameter('FREQUENCY'));
                    
                     % NOW RUN ACTUAL ACQUISTION
                    obj.StartPhase = obj.GetParameter('STARTPHASE') + (iBatch-1)*obj.PhasesPerBatch*PhaseStep;
                    obj.EndPhase = obj.StartPhase + (obj.PhasesPerBatch-1)*PhaseStep;
                    
                    obj.CreateSequence(0); % Create the normal gradient echo sequence
                    
                    obj.Sequence2String;
                    
                    fname = 'TempBatch.bat';
                    obj.WriteBatchFile(fname);
                    [status,result] = system(fullfile(obj.path_file,fname));
                    %            [status,result] = system(fname,'-echo');
                    %delete(fname);% delete the batch file
                    if status == 0 && ~isempty(result)
                        C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                        res = C{:};
                        obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                        obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                        obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                        obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                        obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                        obj.SetParameter('ACTUALPHASETIME',sscanf(res{6},'Actual Phase Time: %f us')*MyUnits.us);
                    end
                    
                    DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                    cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                    nPhasesInBatch=length(cDataBatch)/nPoints;
                    d = reshape(cDataBatch,nPoints,nPhasesInBatch);
                    Data2D = cat(2,Data2D,d);
                    
                    % add pausing step to ensure proper encoding for each line
                    pause( pausingtime );
                    count = count+1;
                    if nPhaseBatches>1
                        waitbar(count/cn,h_waitbar,sprintf('Por favor espere..... %5.2f %%',100*count/cn));
                    end
                end
                Data3D(:,:,iSlice) = Data2D;
                if length(obj.cData) ~= nPoints*nPhases*nSlices;
                    obj.cData = zeros(obj.NAverages,nPoints*nPhases*nSlices);
                end
                if obj.averaging
                    Nav = obj.NAverages;
                else
                    Nav = 1;
                end
                index = mod(iAcq-1,Nav)+1;
                if obj.autoPhasing
%                    pt = max(Data3D(:,ceil(size(Data3D,2)/2),ceil(size(Data3D,3)/2)));
                    pt = max(Data3D(:));
                    Data3D = Data3D.*exp(complex(0,-angle(pt)));
                end
                obj.cData(index,1:length(Data3D(:))) = Data3D(:);
                obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
                obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
                %    obj.fData1D = fftshift(ifft(fftshift(obj.cDataAv)));
                obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
                acq_time = obj.GetParameter('ACQUISITIONTIME');
                obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*acq_time/nPhases;
                obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time/nPhases;
            end
            if nPhaseBatches>1
                delete(h_waitbar);
            end
        end
    end
    
end

