classdef CPMG < MRISequence
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    methods (Access=private)
        function CreateParameters(obj)
            InputParameters = {
                'NECHOS','TAUDELAY','ACQUISITIONDELAY','REPETITIONDELAY','READOUTAMPLITUDE',...
                'SPOILERPHASEAMP','SPOILERREADOUTAMP','SPOILERTIME','FREQUENCYSHIFT','180PHASESHIFT'...
                };
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {...
                'Number of Echos','Tau Delay','Acquisition Delay','Repetition Delay','Readout Amplitude',...
                'Spoiler Phase Amp','Spoiler Readout Amp','Spoiler Time','Frequency Shift','180 Phase Shift'...
                })];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,NaN...
                })];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,NaN...
                })];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,NaN...
                })];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {...
                '','us','us','ms','',...
                '','','us','kHz','deg'...
                })];
            
            OutputParameters = {
                'DEPHASINGREADOUTAMPLITUDE','PHASINGREADOUTAMPLITUDE','SLICEAMPLITUDE1','SLICEAMPLITUDE2'...
                };
            obj.OutputParNames = [containers.Map(obj.OutputParNames.keys(),obj.OutputParNames.values());containers.Map(OutputParameters,...
                {...
                'Dephasing Readout Amplitude', 'Phasing Readout Amplitude','Slice Amplitude 1','Slice Amplitude 2'...
                })];
            obj.OutputParValues = [containers.Map(obj.OutputParValues.keys(),obj.OutputParValues.values());containers.Map(OutputParameters,...
                {...
                NaN,NaN,NaN,NaN...
                })];
            obj.OutputParUnits = [containers.Map(obj.OutputParUnits.keys(),obj.OutputParUnits.values());containers.Map(OutputParameters,...
                {...
                '','','',''...
                })];
            for i=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{i},InputParameters)) = [];
            end
            for i=1:length(obj.OutputParameters)
                OutputParameters(strcmp(obj.OutputParameters{i},OutputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
            obj.OutputParameters = [obj.OutputParameters,OutputParameters];
            
        end
        function WriteBatchFile(obj,fname)
            %% WRITE BATCH FILE AND RUN
            fid = fopen(fullfile(obj.path_file,fname), 'wt');
            pars = obj.InputParameters;
            values = obj.InputParValues;
            fprintf( fid, '@echo off\n');
            fprintf( fid, 'SET Debug=%d\n',0);
            fprintf( fid, 'SET outputFilename=%s\n',obj.OutputFilename);
            fprintf( fid, 'REM ======================\n');
            fprintf( fid, 'REM Acquisition Parameters\n' );
            fprintf( fid, 'REM ======================\n');
            fprintf( fid, 'SET nPoints=%u\n', values('NPOINTS') );
            fprintf( fid, 'SET nScans=%u\n', values('NSCANS') );
            fprintf( fid, 'SET nPhases=%u\n', values('NPHASES') );
            fprintf( fid, 'SET nEchos=%u\n', values('NECHOS') );
            fprintf( fid, 'SET spectrometerFrequency_MHz=%f\n', values('FREQUENCY')/MyUnits.MHz);
            fprintf( fid, 'SET readoutFrequencyShift_kHz=%f\n', values('FREQUENCYSHIFT')/MyUnits.kHz);
            fprintf( fid, 'SET spectralWidth_kHz=%f\n',values('SPECTRALWIDTH')/MyUnits.kHz);
            fprintf( fid, 'SET linebroadening_value=%f\n',values('LINEBROADENING'));
            
            fprintf( fid, 'REM ================\n' );
            fprintf( fid, 'REM Gradient Enables\n' );
            fprintf( fid, 'REM ================\n' );
            fprintf( fid, 'SET slice_en=%u\n', obj.slice_en);
            fprintf( fid, 'SET phase_en=%u\n', obj.phase_en);
            fprintf( fid, 'SET readout_en=%u\n', obj.readout_en);
            fprintf( fid, 'SET spoiler_en=%u\n', obj.spoiler_en);
            
            fprintf( fid, 'REM ===================\n');
            fprintf( fid, 'REM RF Pulse Parameters\n');
            fprintf( fid, 'REM ===================\n'  );
            fprintf( fid, 'SET RF_Shape=%u\n', obj.RF_Shape);
            fprintf( fid, 'SET amplitude=%f\n', values('RFAMPLITUDE'));
            fprintf( fid, 'SET pulseTime_us=%f\n', values('PULSETIME')/MyUnits.us);
            fprintf( fid, 'SET PhaseShift=%f\n', values('180PHASESHIFT')/MyUnits.deg);
            
            fprintf( fid, 'REM ======\n');
            fprintf( fid, 'REM Delays\n');
            fprintf( fid, 'REM ======\n');
            fprintf( fid, 'SET blankingDelay_ms=%f\n', values('BLANKINGDELAY')/MyUnits.ms);
            fprintf( fid, 'SET transTime_us=%f\n', values('TRANSIENTTIME')/MyUnits.us);
            fprintf( fid, 'SET tauTime_us=%f\n', values('TAUDELAY')/MyUnits.us);
            fprintf( fid, 'SET acquisitionDelay_us=%f\n', values('ACQUISITIONDELAY')/MyUnits.us);
            fprintf( fid, 'SET repetitionDelay_s=%f\n', values('REPETITIONDELAY')/MyUnits.s);
            
            fprintf( fid, 'SET readout_amplitude=%f\n', values('READOUTAMPLITUDE'));
            
            fprintf( fid, 'SET spoilerTime_us=%f\n', values('SPOILERTIME')/MyUnits.us);
            fprintf( fid, 'SET spoilerPhaseamp=%f\n', values('SPOILERPHASEAMP'));
            fprintf( fid, 'SET spoilerReadoutamp=%f\n', values('SPOILERREADOUTAMP'));
            
            fprintf( fid, 'SET tx_phase=%f\n', obj.tx_phase);
            fprintf( fid, 'SET blankingBit=%u\n', obj.blankingBit);
            fprintf( fid, 'SET adcOffset=%u\n', obj.adcOffset);
            
            fprintf( fid, 'echo. | "%s"',fullfile(obj.ProgramDir,obj.ProgramName)); %we need echo. to exit batch file in case of errors (it emulates typing ENTER)
            fclose(fid);
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = CPMG(program)
            obj.SequenceName = 'CPMG';
            obj.ProgramName = 'CPMG.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRISequence(obj);
            obj.SetParameter('TAUDELAY',10000*MyUnits.us);
            obj.SetParameter('ACQUISITIONDELAY',0*MyUnits.us);
            obj.SetParameter('REPETITIONDELAY',100*MyUnits.ms);
            obj.SetParameter('READOUTAMPLITUDE',1);
            obj.SetParameter('SPOILERTIME',100*MyUnits.us);
            obj.SetParameter('SPOILERPHASEAMP',1);
            obj.SetParameter('SPOILERREADOUTAMP',1);
            obj.SetParameter('SPOILERTIME',100*MyUnits.us)
            obj.SetParameter('FREQUENCYSHIFT',20*MyUnits.kHz)
            obj.SetParameter('180PHASESHIFT',90*MyUnits.deg)
            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
        end
        function [status,result] = Run(obj,iAcq)
            fname = 'TempBatch.bat';
            obj.WriteBatchFile(fname);
            [status,result] = system(fullfile(obj.path_file,fname));
            %            [status,result] = system(fname,'-echo');
            %delete(fname);% delete the batch file
            if status == 0 && ~isempty(result)
                C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                res = C{:};
                obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                obj.SetParameter('SLICEAMPLITUDE1',sscanf(res{6},'Slice amplitude 1: %f'));
                obj.SetParameter('SLICEAMPLITUDE2',sscanf(res{7},'Slice amplitude 2: %f'));
            end
            
            DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
            cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
            if obj.autoPhasing
                cDataBatch = cDataBatch.*exp(complex(0,-angle(cDataBatch(1))));
            end
            if size(obj.cData,2) ~= length(cDataBatch)
                obj.cData = zeros(obj.NAverages,size(cDataBatch,1));
            end
            if obj.averaging
                Nav = obj.NAverages;
            else
                Nav = 1;
            end
            index = mod(iAcq-1,Nav)+1;
            obj.cData(index,:) = cDataBatch;
            obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
            obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
            %    obj.fData1D = fftshift(ifft(fftshift(obj.cDataAv)));
            obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
            acq_time = obj.GetParameter('ACQUISITIONTIME');
            obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*acq_time;
            obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time;
        end
    end
    
end

