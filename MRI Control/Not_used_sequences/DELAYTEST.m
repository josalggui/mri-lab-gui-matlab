classdef DELAYTEST < MRI_BlankSequence6
    %% MASSIF CODE
    
    %*******************************************************
    %** J. M. Algar�n                                     **
    %** CSIC/UPV                                          **
    %** I3M                                               **
    %** Avda. dels Tarongers, 12,46022, Valencia, (Spain) **
    %** Tel: +34 960 728 111                              **
    %** email: josalggui@i3m.upv.es                       **
    %*******************************************************
    
    %% Properties
    properties
        gradientList;
        maxRepetitionsPerBatch;
    end
    
    
    %% Methods
    methods
        function obj = DELAYTEST(program)
            obj = obj@MRI_BlankSequence6();
            obj.SequenceName = 'DELAYTEST';
            obj.ProgramName = 'MRI_BlankSequenceJM.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function CreateParameters(obj)
            % Create parameters
            obj.CreateOneParameter('FREQUENCY','RF frequency','MHz',14*MyUnits.MHz);
            obj.CreateOneParameter('PULSETIME','RF pulse time','us',4*MyUnits.us);
            obj.CreateOneParameter('GUP','+G','',0.1);
            obj.CreateOneParameter('GDOWN','-G','',0.1);
            obj.CreateOneParameter('GUPTIME','+ Gradient time','ms',2*MyUnits.ms);
            obj.CreateOneParameter('GDOWNTIME','- Gradient time','ms',2*MyUnits.ms); 
            obj.CreateOneParameter('RISE','Rise gradient time','us',20*MyUnits.us);
            obj.CreateOneParameter('TRANSIENTTIME','Delay after RF','us',20*MyUnits.us);
            obj.CreateOneParameter('REPETITIONDELAY','Repetition delay','s',1);
            obj.CreateOneParameter('NSTEPS','N Steps','',10);
            obj.CreateOneParameter('GRADIENTDELAY','Gradient Delay','us',0.1*MyUnits.us);
            
            % Hidden parameters
            obj.InputParHidden = {...
                'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
                'SPECTRALWIDTH','NSLICES',...
                'BLANKINGDELAY','COILRISETIME'};
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence6(obj);
            obj.SetParameter('NPOINTS',1200);
            obj.SetParameter('PULSETIME',4*MyUnits.us);
            obj.SetParameter('RFAMPLITUDE',1);
            obj.SetParameter('SPECTRALWIDTH',300*MyUnits.kHz);
            obj.SetParameter('TRANSIENTTIME',20*MyUnits.us);
        end
        function CreateSequence(obj,mode)
            global rawData
            guptime = obj.GetParameter('GUPTIME')*1e6;
            gdowntime = obj.GetParameter('GDOWNTIME')*1e6;
            risetime = obj.GetParameter('RISE')*1e6;
            gup=obj.GetParameter('GUP');
            gdown=obj.GetParameter('GDOWN');
            deadTime= obj.GetParameter('TRANSIENTTIME')*1e6;
            pulseTime= obj.GetParameter('PULSETIME')*1e6;
            repDelay= obj.GetParameter('REPETITIONDELAY')*1e6;
            acqtime=1e3*obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
            nSteps = obj.GetParameter('NSTEPS');
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            % Configure gradients and axes
            [~,~,readout,~,~,~,~] = obj.ConfigureGradients(mode);
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            gup = gup*readout;
            gdown = gdown*readout;
            
            obj.nline_reads = 1;
            
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions = 4*nSteps+8;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            iIns = 1;
            % SHIMMING in all gradients (3 instructions)
            iIns = obj.PulseGradient(iIns,shimR,RR,0.1);
            iIns = obj.PulseGradient(iIns,shimP,PP,0.1);
            iIns = obj.PulseGradient(iIns,shimS,SS,0.1);
            
            % RF pulse (1 instruction)
            iIns = obj.CreatePulse(iIns,0,15,pulseTime,deadTime);

            % Acquisition
            iIns = obj.Acquisition(iIns,0.1);
            
            % Activate dephasing
            for ii = 1:nSteps
                iIns = obj.PulseGradient(iIns,shimR+gup/nSteps*ii,RR,risetime/nSteps);
            end
            obj.DELAYs(iIns-1) = risetime/nSteps+guptime;
            
            for ii = 1:nSteps
                iIns = obj.PulseGradient(iIns,shimR+gup-gup/nSteps*ii,RR,risetime/nSteps);
            end
            
            for ii = 1:nSteps
                iIns = obj.PulseGradient(iIns,shimR-gdown/nSteps*ii,RR,risetime/nSteps);
            end
            obj.DELAYs(iIns-1) = risetime/nSteps+gdowntime;
            
            for ii = 1:nSteps
                iIns = obj.PulseGradient(iIns,shimR-gdown+gdown/nSteps*ii,RR,risetime/nSteps);
            end
            
            % Repetition delay and turn all gradients off
            iIns = obj.RepetitionDelay(iIns,0,repDelay);
        end
        
        
        function [status,result] = Run(obj,iAcq)
            tic;
            clc
            
            global rawData;
            rawData=[];
            tWith = 0;
            
            guptime = obj.GetParameter('GUPTIME');
            gdowntime = obj.GetParameter('GDOWNTIME');
            risetime = obj.GetParameter('RISE');
            acqTime = 4*risetime+guptime+gdowntime+obj.GetParameter('GRADIENTDELAY');
            acqTime = 0.2e-3;
            bw = obj.GetParameter('NPOINTS')/acqTime;
            obj.SetParameter('SPECTRALWIDTH',bw);
            deadTime= obj.GetParameter('TRANSIENTTIME');
            pulseTime= obj.GetParameter('PULSETIME');
            
            [cDataBatch,~,status,result] = RunData(obj,0,tWith);
            
            bwCal = load('rawData-BW-Calibration.mat');
            bwCalVal = bwCal.rawData.outputs.fidMean;
            bwCalVec = logspace(log10(bwCal.rawData.inputs.bwMin),...
                log10(bwCal.rawData.inputs.bwMax),...
                bwCal.rawData.inputs.steps);
            % Get value to normalize fid to Volts
            [~,bwCalNormPos] = min(abs(bwCalVec-obj.GetParameter('SPECTRALWIDTH')));
            bwCalNormVal = bwCalVal(bwCalNormPos);
            
            tVector = linspace(pulseTime/2+deadTime,pulseTime/2+deadTime+acqTime,obj.GetParameter('NPOINTS'));
            rawData.outputs.measurements = [tVector',cDataBatch/bwCalNormVal];
            
            % Save rawData
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
    end
end
