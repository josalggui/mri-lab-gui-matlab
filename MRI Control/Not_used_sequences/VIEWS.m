classdef VIEWS < MRI_BlankSequence6
    %% MASSIF CODE
    
    %*******************************************************
    %** J. M. Algar�n                                     **
    %** CSIC/UPV                                          **
    %** I3M                                               **
    %** Avda. dels Tarongers, 12,46022, Valencia, (Spain) **
    %** Tel: +34 960 728 111                              **
    %** email: josalggui@i3m.upv.es                       **
    %*******************************************************
    
    %% Properties
    properties
        gradientList;
        maxRepetitionsPerBatch;
    end
    
    
    %% Methods
    methods
        function obj = VIEWS(program)
            obj = obj@MRI_BlankSequence6();
            obj.SequenceName = 'VIEWS';
            obj.ProgramName = 'MRI_BlankSequenceJM.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function CreateParameters(obj)
            % Create parameters
            obj.CreateOneParameter('FREQUENCY','RF frequency','MHz',14*MyUnits.MHz);
            obj.CreateOneParameter('PULSETIME','RF pulse time','us',4*MyUnits.us); 
            obj.CreateOneParameter('GXFREQUENCY','X Gradient frequency','kHz',1*MyUnits.kHz);
            obj.CreateOneParameter('GYFREQUENCY','Y Gradient frequency','kHz',1*MyUnits.kHz);
            obj.CreateOneParameter('GZFREQUENCY','Z gradient frequency','kHz',1*MyUnits.kHz);
            obj.CreateOneParameter('GXAMPLITUDE','X gradient amplitude','T/m',0.1);
            obj.CreateOneParameter('GYAMPLITUDE','Y gradient amplitude','T/m',0.1);
            obj.CreateOneParameter('GZAMPLITUDE','Z gradient amplitude','T/m',0);
            obj.CreateOneParameter('GTIME','Gradient time','ms',2*MyUnits.ms);
            obj.CreateOneParameter('GSTEPS','Gradient steps','',339);
            obj.CreateOneParameter('TRANSIENTTIME','Acquisition delay','us',20*MyUnits.us);
            obj.CreateOneParameter('REPETITIONDELAY','Repetition delay','s',1);
            % Hidden parameters
            obj.InputParHidden = {...
                'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
                'SPECTRALWIDTH','NSLICES',...
                'BLANKINGDELAY','COILRISETIME'};
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence6(obj);
            obj.SetParameter('NPOINTS',100);
            obj.SetParameter('PULSETIME',4*MyUnits.us);
            obj.SetParameter('RFAMPLITUDE',1);
            obj.SetParameter('TRANSIENTTIME',20*MyUnits.us);
            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
        end
        function CreateSequence(obj,mode)
            global rawData
            
            % Get the shimming and axes
            % SelectAxes save the axes at:
            % -obj.selectReadout
            % -obj.selectPhase
            % -obj.selectSlice
            [shimS,shimP,shimR] = SelectAxes(obj);
            
            % Number of times you need to read data (for batch file)
            gTime = rawData.inputs.gTime*1e6;
            gSteps = rawData.inputs.gSteps;
            obj.nline_reads = 1;
            
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions = 5+3*gSteps;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            % Get time delays for instructions
            deadTime            = rawData.inputs.acqDelay*1e6;
            pulseTime           = rawData.inputs.pulseTime*1e6;
            repDelay            = rawData.inputs.repDelay*1e6;
            deltaT              = gTime/gSteps;
            
            % Crate pulse sequence
            iIns = 1;
            % RF pulse (1 instruction)
            iIns = obj.CreatePulse(iIns,0,15,pulseTime,deadTime);
            iIns = obj.Acquisition(iIns,0.1);
            for ii = 1:gSteps
                % Gradient list
                rAmp = obj.gradientList(ii,1)-shimR;
                pAmp = obj.gradientList(ii,2)-shimP;
                sAmp = obj.gradientList(ii,3)-shimS;
                % Turn on the first gradient
                iIns = obj.PulseGradient(iIns,rAmp,...
                    obj.selectReadout,0.1);
                % Turn on the second gradient
                iIns = obj.PulseGradient(iIns,pAmp,...
                    obj.selectPhase,0.1);
                % Turn on the third gradient
                iIns = obj.PulseGradient(iIns,sAmp,...
                    obj.selectSlice,deltaT-0.2);
                % Run acquisition
            end
            % Repetition delay and turn all gradients off
            iIns = obj.RepetitionDelay(iIns,1,repDelay);
        end
        function [status,result] = Run(obj,iAcq)
            tic;
            clc
            
            %% Constant to relate k with k'
            % HELP: k = gammabar*G*t
            % G = c*A where A is the gradient amplitude voltage from
            % radioprocessor. Then, k' = k/(gammabar*c) = A*t.
            % We still need to calibrate constant c. We also need to take
            % into account that c is different for each axis. 
            gammabar = 42.6d6;          % MHz/T
            c = obj.ReorganizeCalibrationData();
            clear rawData
            global rawData;              % Raw Data contains output info
            
            %% Get sequence parameters
            pulseTime = obj.GetParameter('PULSETIME');
            gXFrequency = obj.GetParameter('GXFREQUENCY');
            gYFrequency = obj.GetParameter('GYFREQUENCY');
            gZFrequency = obj.GetParameter('GZFREQUENCY');
            gXAmplitude = obj.GetParameter('GXAMPLITUDE');
            gYAmplitude = obj.GetParameter('GYAMPLITUDE');
            gZAmplitude = obj.GetParameter('GZAMPLITUDE');
            gTime = obj.GetParameter('GTIME');
            gSteps = obj.GetParameter('GSTEPS');
            nPoints = obj.GetParameter('NPOINTS');
            
            rawData.inputs.Seq = obj.SequenceName;
            rawData.inputs.NEX = obj.GetParameter('NSCANS');
            rawData.inputs.nPoints = nPoints;
            rawData.inputs.rfAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.rfFrequency = obj.GetParameter('FREQUENCY');
            rawData.inputs.pulseTime = pulseTime;
            rawData.inputs.acqDelay = obj.GetParameter('TRANSIENTTIME');
            rawData.inputs.axis = strcat(obj.GetParameter('READOUT'),obj.GetParameter('PHASE'),obj.GetParameter('SLICE'));
            rawData.inputs.gTime = gTime;
            rawData.inputs.gSteps = gSteps;
            rawData.inputs.gXAmplitude = gXAmplitude;
            rawData.inputs.gYAmplitude = gYAmplitude;
            rawData.inputs.gZAmplitude = gZAmplitude;
            rawData.inputs.gXFrequency = gXFrequency;
            rawData.inputs.gYFrequency = gYFrequency;
            rawData.inputs.gZFrequency = gZFrequency;
            rawData.inputs.repDelay = obj.GetParameter('REPETITIONDELAY');
            
            %% Set the spectral width
            acquisitionTime = gTime;
            bw = nPoints/acquisitionTime;
            obj.SetParameter('SPECTRALWIDTH',bw);
            rawData.aux.acquisitionTime = acquisitionTime;
            rawData.aux.bandwidth = bw;
            
            %% Run sequence
            % Get number of averages
            if(obj.averaging)
                nAverages = obj.NAverages;
            else
                nAverages = 1;
            end
            measurements = zeros(nPoints,nAverages+1);
            measurements0 = measurements;
            
            % Get gradient points and acquired points
            deltaT = gTime/gSteps;
            tGradient = linspace(0,gTime-deltaT,gSteps)';
            tVector = linspace(0,gTime-1/bw,nPoints)'+0.5/bw;
            measurements(:,1) = tVector;
            measurements0(:,1) = tVector;
            
            % Open waiting bar
            if nAverages>1
                h_waitbar = waitbar(0/nAverages,'Midiendo (por favor espere)...');
            end
            
            tWithout = 0;
            tWith = 0;
            for ii = 1:nAverages
                fprintf('Repetition %1.0f/%1.0f \n',ii,nAverages)
                fprintf('Calibrating frequency... \n')
                % Get central frequency and T2
                [~,f0] = obj.GetFIDParameters;
                rawData.aux.freqList(ii) = f0;
                fprintf('Frequency = %1.4f MHz \n',f0*1d-6)
                
                % FID withoug gradients
                fprintf('FID with gradients OFF... \n')
                obj.gradientList = zeros(gSteps,3);
                clear cDataBatch status result
                [cDataBatch,tWithout,status,result] = RunData(obj,0,tWithout);
                if(obj.autoPhasing)
                    measurements0(:,ii+1) = cDataBatch*exp(-1i*angle(cDataBatch(1)));
                else
                    measurements0(:,ii+1) = cDataBatch;
                end
                
                % FID with gradients
                fprintf('FID with gradients ON... \n')
                obj.gradientList(:,1) = gXAmplitude*sin(2*pi*gXFrequency*tGradient);
                obj.gradientList(:,2) = gYAmplitude*cos(2*pi*gYFrequency*tGradient);
                obj.gradientList(:,3) = gZAmplitude*cos(2*pi*gZFrequency*tGradient);
                rawData.aux.gradientList = obj.gradientList;
                obj.gradientList = obj.gradientList/diag(c);
                clear cDataBatch status result
                [cDataBatch,tWith,status,result] = RunData(obj,0,tWith);
                if(obj.autoPhasing)
                    measurements(:,ii+1) = cDataBatch*exp(-1i*angle(cDataBatch(1)));
                else
                    measurements(:,ii+1) = cDataBatch;
                end
                if nAverages>1
                    waitbar(ii/nAverages,h_waitbar,sprintf('Midiendo (por favor espere)... %5.2f %%',100*ii/nAverages));
                end
                disp(' ')
            end
            
            % Adding gradients to the measurements
            measurements(:,ii+2) = gXAmplitude*sin(2*pi*gXFrequency*tVector);
            measurements(:,ii+3) = gYAmplitude*cos(2*pi*gYFrequency*tVector);
            measurements(:,ii+4) = gZAmplitude*cos(2*pi*gZFrequency*tVector);
            
            % Close waiting bar
            if nAverages>1
                delete(h_waitbar);
            end
            
            %% Save data to global
            rawData.outputs.measurements = measurements;
            rawData.outputs.measurements0 = measurements0;

            %% Save elapsed times to raw data
            elapsedTime = toc;
            rawData.aux.tWithout = tWithout;
            rawData.aux.tWith = tWith;
            rawData.aux.elapsedTime = elapsedTime;
            
            % Save rawData
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
    end
end

