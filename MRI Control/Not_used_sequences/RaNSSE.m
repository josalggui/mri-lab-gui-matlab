classdef RaNSSE < MRI_BlankSequence_JM    
    properties
        PhasesPerBatch = 20;
        nPhasesBatch;
        Axis1Vector;
        Axis2Vector;
        Axis3Vector;
        StartSlice;
        lastBatch;
        EndSlice;
        StartPhase;
        EndPhase;
        Stage;              % Normal ->1 or Prescan -> 0
        RepetitionTime;
        slice_ampl;
        AcquisitionTime;
    end
    methods (Access=private)
        function CreateParameters(obj)

            % HELP %
            % I include here new inputs to discriminate between
            % acquistition and regruidding.
            % -> NPHASES, NREADOUTS and NSLICES are for regridding
            % -> NPOINTS, NCIRCUNFERENCES, NLINESPERCIRCUNFERENCES are for
            %    aqcuisition.

            InputParameters = {...
                'FOVX','FOVY','FOVZ',...
                'NPHASES','NREADOUTS','NSLICES','REPETITIONDELAY',...
                'TRANSIENTTIME','ECHOTIME',...
                'OVERSAMPLING','JUMPINGFACTOR',...
                'PHASE','READOUT','SLICE',...
                'THETA','PHI','PULSETIME180'...
                };
            
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
                'NPOINTS','SPECTRALWIDTH','BLANKINGDELAY','RFAMPLITUDE'};
            
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {...
                'FOVx','FOVy','FOVz',...
                'Ny','Nx','Nz','Repetition Delay',...
                'Dead Time','Echo Time',...
                'Oversampling','Jumping Factor',...
                'Y Axis','X Axis','Z Axis',...
                'Altitude','Azimuth','180� Pulse Time'...
                
                })];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,...
                '','','',...
                NaN,NaN,NaN...
                })];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {...
                0,0,0,...
                NaN,NaN,NaN,NaN,...
                0.1d-6,NaN,...
                1,1,...
                '','','',...
                0,0,0.1d-6...
                })];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,...
                NaN,NaN,...
                '','','',...
                180,360,NaN ...
                })];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {...
                'mm','mm','mm',...
                '','','','ms',...
                'us','us',...
                '','',...
                '','','',...
                'degrees','degrees','us'...
                })];
            
            for k=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{k},InputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
            % Reorganizes InputParameters
            obj.MoveParameter(6,21);
            obj.MoveParameter(28,11);
            obj.MoveParameter(14,15);
            obj.MoveParameter(20,21);
            obj.MoveParameter(14:22,20:28);
        end
        
        function MoveParameter(obj,x,y)
            xx = obj.InputParameters(x);
            output = obj.InputParameters;
            output(x) = [];
            output(y+1:end+1) = output(y:end);
            output(y) = xx;
            obj.InputParameters = output;
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = RaNSSE(program)
            obj = obj@MRI_BlankSequence_JM();
            obj.SequenceName = 'RaNSSE';
            obj.ProgramName = 'MRI_BlankSequenceJM.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence_JM(obj);
            obj.SetParameter('FOVX',10*MyUnits.mm);
            obj.SetParameter('FOVY',10*MyUnits.mm);
            obj.SetParameter('FOVZ',10*MyUnits.mm);
            obj.SetParameter('REPETITIONDELAY',50*MyUnits.ms);
            obj.SetParameter('ECHOTIME',100*MyUnits.us);
            obj.SetParameter('NPHASES',40);
            obj.SetParameter('NREADOUTS',40);
            obj.SetParameter('OVERSAMPLING',1);
            obj.SetParameter('JUMPINGFACTOR',1);
            obj.SetParameter('TRANSIENTTIME',20*MyUnits.us);
            obj.SetParameter('COILRISETIME',500*MyUnits.us);
            obj.SetParameter('THETA',0);
            obj.SetParameter('PHI',0);
            obj.SetParameter('BLANKINGDELAY',15*MyUnits.us);
            obj.SetParameter('PULSETIME180',8*MyUnits.us);
            obj.SetParameter('RFAMPLITUDE',1);

            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
        end
        function UpdateTETR(obj)
            obj.TE = obj.GetParameter('TRANSIENTTIME') + ...
                    0.5 * obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH');
            obj.TR = obj.TE +  0.5*(obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH')) + 0.5*obj.GetParameter('PULSETIME') +...
                    obj.GetParameter('BLANKINGDELAY') + obj.GetParameter('REPETITIONDELAY');
    
            if obj.spoiler_en
                obj.TR = obj.TR + obj.GetParameter( 'SPOILERTIME' );
            end
            obj.TTotal = obj.TR * obj.GetParameter('NSCANS') * obj.GetParameter('NSLICES') * obj.GetParameter('NPHASES');
        end
        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence,
            %         "1", central frequency adjustment sequence,
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            
            
            if(mode==1)
                [~,nPhases,readout,slice,phase,~,~] = obj.ConfigureGradients(mode);
                obj.Axis1Vector = 0;
                obj.Axis2Vector = 0;
                obj.Axis3Vector = 0;
            elseif(mode==0)
                [~,~,readout,slice,phase,~,~] = obj.ConfigureGradients(mode);
                nPhases = obj.nPhasesBatch;
            end
            
            % Calculate the number of line reads
            obj.nline_reads = nPhases;
            
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions=9*(nPhases);
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            
            % Timing
            acq_time    = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6;
            crt         = max([obj.GetParameter('BLANKINGDELAY'),obj.GetParameter('COILRISETIME')])*1e6;
            trf         = obj.GetParameter('PULSETIME')*1e6;
            t180        = obj.GetParameter('PULSETIME180')*1e6;
            tau         = obj.GetParameter('ECHOTIME')/2*1e6;
            td          = obj.GetParameter('TRANSIENTTIME')*1e6;
            blk         = obj.GetParameter('BLANKINGDELAY')*1e6;
            iIns = 1;
            bbit = 1;            
            for phasei = 1:nPhases
                
                % Gradient Amplitude
                readoutAmplitude = obj.Axis1Vector(phasei)+shimR;
                phaseAmplitude = obj.Axis2Vector(phasei)+shimP;
                sliceAmplitude = obj.Axis3Vector(phasei)+shimS;
                
                %% Activate the total gradient (3 instrucctions)
                nIns  = 3; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [ readoutAmplitude phaseAmplitude sliceAmplitude ];
                    obj.DACs(vIns)     = [ RR PP SS ];
                    obj.WRITEs(vIns)   = [ 1 1 1 ];
                    obj.UPDATEs(vIns)  = [ 1 1 1 ];
                    obj.PhResets(vIns) = [ 0 0 1 ];
                    obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                    obj.FLAGs(vIns)    = [ bbit bbit bbit ];
                    if(mode==0)
                        obj.DELAYs(vIns)   = [0.1 0.1 crt]; %time in us
                    elseif(mode==1)
                        obj.DELAYs(vIns)   = [0.1 0.1 blk];
                    end
                iIns = iIns + nIns; % update the number of instructions
                
                %% RF pulse (1 instruction)
                nIns = 3; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.TXs(vIns)      = [1*obj.Stage~=0 0 1*(obj.Stage~=0)*(mode==0)];
                    obj.ENVELOPEs(vIns)= [7 7 7];
                    obj.PHASEs(vIns) = [0 0 0];
                    obj.FLAGs(vIns)    = [bbit bbit bbit];
                    if(mode==0)
                        obj.DELAYs(vIns)   = [trf tau-trf/2-t180/2 t180]; %time in us
                    elseif(mode==1)
                        obj.DELAYs(vIns)   = [trf 0.1 0.1];
                    end
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %% Dead Time
                nIns = 1; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.DELAYs(vIns)   = td; %time in us                    
                iIns = iIns + nIns; % update the number of instructions in this block
                                
                %% Acquisition
                nIns = 1;
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.RXs(vIns)  = 1;
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.DELAYs(vIns)   = acq_time;
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %% Repetition Delay (1 instruction)
                nIns = 1;
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    if(obj.lastBatch==1 && phasei==nPhases)
                        obj.AMPs(vIns)     = 0.0;
                        obj.DACs(vIns)     = 3;
                        obj.WRITEs(vIns)   = 1;
                        obj.UPDATEs(vIns)  = 1;
                        obj.PHASEs(vIns)   = 0;
                        obj.CLEARs(vIns)   = 1;
                        obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                        disp('Last Batch!')
                    end
                    % The gradients are turned off for now. Just because
                    % the gradient amplifiers has high pass filter at the
                    % output
                    %%%%%%%%%%%%%%%%%%%%%
                    obj.AMPs(vIns) = 0;
                    obj.DACs(vIns) = 3;
                    obj.WRITEs(vIns) = 1;
                    obj.UPDATEs(vIns) = 1;
                    %%%%%%%%%%%%%%%%%%%%%%
                    if(obj.Stage~=0)
                        obj.DELAYs(vIns)   = obj.GetParameter('REPETITIONDELAY')*1e6; %time in us
                    else
                        obj.DELAYs(vIns)   = 2*acq_time;
%                         obj.DELAYs(vIns)   = obj.GetParameter('REPETITIONDELAY')*1e6;
                    end
                iIns = iIns + nIns; % update the number of instructions in this block
                
            end % end for nPhases
            %             end %end for slicei
            
        end
        function [status,result] = Run(obj,iAcq)
            tic;
            clc
            [~,~,readout,slice,phase,~,~] = obj.ConfigureGradients(0);
                        
            %% Constant to relate k with k'
            % HELP: k = gammabar*G*t
            % G = c*A where A is the gradient amplitude voltage from
            % radioprocessor. Then, k' = k/(gammabar*c) = A*t.
            % We still need to calibrate constant c. We also need to take
            % into account that c is different for each axis. 
            gammabar = 42.6d6;          % MHz/T
            c = [12,12,7.5];             % T/m/V
            global rawData;               % Raw Data contains output info
            
            %% Get cartesian parameters
            nPoints = [obj.GetParameter('NREADOUTS'),obj.GetParameter('NPHASES'),obj.GetParameter('NSLICES')];
            fov = [obj.GetParameter('FOVX'),obj.GetParameter('FOVY'),obj.GetParameter('FOVZ')];
            deltaK = 1./fov;
            kMax = (nPoints-1)./(2*fov);
            
            rawData.inputs.sequence = obj.SequenceName;
            rawData.inputs.NEX = obj.GetParameter('NSCANS');
            rawData.inputs.nPoints = nPoints;
            rawData.inputs.fov = fov;
            rawData.aux.deltaK = deltaK;
            rawData.aux.kMax = kMax;
            
            %% Creates rotation matrix
            altitude = obj.GetParameter('THETA')*pi/180;
            azimuth = obj.GetParameter('PHI')*pi/180;
            ry = [cos(altitude) 0 -sin(altitude) ; 0 1 0 ; sin(altitude) 0 cos(altitude)];
            rz = [cos(azimuth) sin(azimuth) 0 ; -sin(azimuth) cos(azimuth) 0 ; 0 0 1];
            rotationMatrix = ry*rz;
            rawData.inputs.altitude = altitude;
            rawData.inputs.azimuth = azimuth;
            
            %% Reorganize calibration data
            readout = obj.GetParameter('READOUT');
            phase = obj.GetParameter('PHASE');
            slice = obj.GetParameter('SLICE');
            if(readout=='x');       readout = 1;
            elseif(readout=='y');   readout = 2;
            elseif(readout=='z');   readout = 3;
            end
            if(phase=='x');         phase = 1;
            elseif(phase=='y');     phase = 2;
            elseif(phase=='z');     phase = 3;
            end
            if(slice=='x');         slice = 1;
            elseif(slice=='y');     slice = 2;
            elseif(slice=='z');     slice = 3;
            end
            c = c([readout phase slice]);            
            
            %% Get sequence parameters
            TE = obj.GetParameter('ECHOTIME');                              % Echo time
            td = obj.GetParameter('TRANSIENTTIME');                         % Dead time
            trf = obj.GetParameter('PULSETIME');                            % RF time
            t180 = obj.GetParameter('PULSETIME180');                        % 180� rf time
            taq = (TE-2*td-2*trf);                                          % Acquisition time
            ov = obj.GetParameter('OVERSAMPLING');                          % Oversampling for radial acquisition. It helps for better regridding
            bw = max(nPoints)/(taq);                                        % Ideal acquisition bandwidth
            bwov = ov*bw;                                                   % Acquisition bandwidth taking into account the oversampling
            obj.SetParameter('SPECTRALWIDTH',bwov);                         % Set the SPECTRALWIDTH to bw
            jumpingFactor = obj.GetParameter('JUMPINGFACTOR');              % Jumping Factor
            
            rawData.inputs.coilRiseTime = obj.GetParameter('COILRISETIME');
            rawData.inputs.pulseTime = trf;
            rawData.inputs.pulseTime180 = t180;
            rawData.inputs.deadTime = td;
            rawData.inputs.echoTime = TE;
            rawData.aux.acquisitionTime = taq;
            rawData.inputs.delay = obj.GetParameter('REPETITIONDELAY');
            rawData.inputs.oversampling = ov;
            rawData.inputs.jumpingFactor = jumpingFactor;
            rawData.aux.bandwidth = bw;
            rawData.aux.bandwidthov = bwov;
            rawData.inputs.axes = [obj.GetParameter('READOUT'),...
                obj.GetParameter('PHASE'),obj.GetParameter('SLICE')];
            
            %% Set number of phases, points and slices as well as amplitudes
            % gradientAmplitude = normalized amplitude to the MRIGUI input
            % nLPC = number of lines per circunferece in k-space
            % nCir = number of circunfereces in gradient space
            % nPPL = number of acquired points per line in k-space
            gradientAmplitudes = 2*kMax./(gammabar*(taq-1/bwov)).*~(nPoints==1);
            nPPL = max(nPoints);
            nLPC = 1+ceil(max(nPoints(1:2))*pi/8);    % Ideal should be *pi instead of *pi/2 but I will use *pi/2 to save time
            nLPC = max(nLPC-mod(nLPC,2),1);
            nCir = max(ceil(nPoints(3)*pi/2)-1,1);
            obj.SetParameter('NPOINTS',nPPL);
            acq_time = nPPL/bwov;
            obj.AcquisitionTime = acq_time;
            
            rawData.aux.numberOfPointsPerLine = nPPL;
            rawData.aux.linesPerCircumference = nLPC;
            rawData.aux.circumferences = nCir;
            rawData.aux.gradientAmplitudes = gradientAmplitudes;

            %% Calculate the gradients list for radial sampling
            % Gradient are defined by the sphere parametric equation. The
            % two parameteres are: theta in [0,pi] and phi [0,2pi].
            % Gradients are normalized to the amplifier amplitude.
            deltaTheta = pi/(nCir+1);
            theta = linspace(deltaTheta,pi-deltaTheta,nCir);
            deltaPhi = pi/nLPC;
%             phi = linspace(0,pi-deltaPhi,nLPC);
            phi = linspace(0,2*pi,nLPC);
            normalizedGradientsRadial = zeros(nLPC*nCir,3);                          % gradientVectors1 has 3 columns with Gx, Gy and Gz
            n = 0;
            for ii = 1:nCir
                for jj = 1:nLPC
                    n = n+1;
                    normalizedGradientsRadial(n,1) = sin(theta(ii))*cos(phi(jj));
                    normalizedGradientsRadial(n,2) = sin(theta(ii))*sin(phi(jj));
                    normalizedGradientsRadial(n,3) = cos(theta(ii));
                end
            end
            % Reorganize the gradient values with a given jumping factor
            index = 1:jumpingFactor:size(normalizedGradientsRadial,1);
            if(jumpingFactor>1)
                for ii = 2:jumpingFactor
                    index = cat(2,index,ii:jumpingFactor:size(normalizedGradientsRadial,1));
                end
            end
            normalizedGradientsRadial = normalizedGradientsRadial(index,:);
            gradientVectors1 = normalizedGradientsRadial*diag(gradientAmplitudes);
            clear index
            
            %% Calculate k-points matrix for radial sampling
            normalizedKRadial = zeros(nCir*nLPC,3,nPPL);
            % Calculate k-points at t = 0.5*trf+td
            normalizedDkRadial = normalizedGradientsRadial/obj.GetParameter('SPECTRALWIDTH');
            normalizedKRadial(:,:,1) = (-TE/2+td+trf+1/(2*bwov))*normalizedGradientsRadial;%+normalizedDkRadial;
            % Calculate all k-points
            for ii=2:nPPL
                normalizedKRadial(:,:,ii) = normalizedKRadial(:,:,1)+(ii-1)*normalizedDkRadial;
            end
            normalizedKRadial = reshape(permute(normalizedKRadial,[3,1,2]),[nCir*nLPC*nPPL,3]);
            kRadial = normalizedKRadial*diag(gammabar*gradientAmplitudes);
            kSpaceValues = kRadial;
            
            %% k-points for Cartesian sampling
            kx = linspace(-kMax(1)*~isequal(nPoints(1),1),kMax(1)*~isequal(nPoints(1),1),nPoints(1));
            ky = linspace(-kMax(2)*~isequal(nPoints(2),1),kMax(2)*~isequal(nPoints(2),1),nPoints(2));
            kz = linspace(-kMax(3)*~isequal(nPoints(3),1),kMax(3)*~isequal(nPoints(3),1),nPoints(3));
            [kx,ky,kz] = meshgrid(kx,ky,kz);
            kx = permute(kx,[2,1,3]);
            ky = permute(ky,[2,1,3]);
            kz = permute(kz,[2,1,3]);
            kCartesian(:,1) = kx(:);
            kCartesian(:,2) = ky(:);
            kCartesian(:,3) = kz(:);
            
            %% Rotates the reference system to produce desired gradients
            gradientVectors1 = gradientVectors1*rotationMatrix;
            
            rawData.aux.gradientsRadial = gradientVectors1;
            
            gradientVectors1 = gradientVectors1/diag(c);
            
            %% Auxiliar
            Data2D = [];
            
            %% Prescan
            tBatch1 = clock;
            obj.Stage = 0;
            obj.PhasesPerBatch = floor(min([1024/9,16000/nPPL]));
            disp('New acquisition');
            disp('PRESCAN');
            NEX = obj.GetParameter('NSCANS');
            obj.SetParameter('NSCANS',1000);
            tPrescan = 0;
            for iBatch = 1:1
                obj.nPhasesBatch = 1;
                obj.Axis1Vector = 0*gradientVectors1(1,1);
                obj.Axis2Vector = 0*gradientVectors1(1,2);
                obj.Axis3Vector = 0*gradientVectors1(1,3);
                obj.lastBatch = 0;
                % NOW RUN ACTUAL ACQUISTION
                obj.CreateSequence(0);
                obj.Sequence2String;            
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                t1 = clock;
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                t2 = clock;
                tPrescan = tPrescan + etime(t2,t1);
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataCal = complex(DataBatch(:,1),DataBatch(:,2));
            end
            obj.SetParameter('NSCANS',NEX);
            rawData.kSpace.calibra = cDataCal;
            clear NEX
            
            %% Radial
            % Create waiting bar
            
            obj.Stage = 1;
            obj.PhasesPerBatch = floor(min([1024/7,16000/nPPL]));                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
            nPhaseBatches = ceil(nLPC*nCir/obj.PhasesPerBatch);
            rawData.aux.numberOfBatches = nPhaseBatches;
            cn = nPhaseBatches;
            count = 0;
            if nPhaseBatches>1
                h_waitbar = waitbar(count/cn,'Radial (por favor espere)...');
            end
            % Start batches sweep
            disp('RADIAL');
            tRadial = 0;
            rawData.inputs.rfFrequency = zeros(nPhaseBatches,1);
            for iBatch = 1:nPhaseBatches
                disp('New batch');
                
                % Get the central frequency
                obj.CreateSequence(1); % Create the gradient echo sequence for CF
                obj.Sequence2String;
                fname_CF = 'TempBatch_CF.bat';
                obj.WriteBatchFile(fname_CF);
                obj.CenterFrequency(fname_CF);
                obj.SetParameter('LASTFREQUENCY',obj.GetParameter('FREQUENCY'));
                rawData.inputs.rfFrequency(iBatch) = obj.GetParameter('FREQUENCY');
                
                % Get the appropiate axis
                if(iBatch<nPhaseBatches)
                    obj.Axis1Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,1);
                    obj.Axis2Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,2);
                    obj.Axis3Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,3);
                    obj.lastBatch = 0;
                else
                    obj.Axis1Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:nLPC*nCir,1);
                    obj.Axis2Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:nLPC*nCir,2);
                    obj.Axis3Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:nLPC*nCir,3);
                    obj.lastBatch = 1;
                end
%                 obj.Axis1Vector(:) = 0;
%                 obj.Axis2Vector(:) = 0;
%                 obj.Axis3Vector(:) = 0;
                obj.nPhasesBatch = length(obj.Axis1Vector);
                
                % NOW RUN ACTUAL ACQUISTION
                obj.CreateSequence(0);
                obj.Sequence2String;            
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                t1 = clock;
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                t2 = clock;
                tRadial = tRadial + etime(t2,t1);
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Radial batch aborted!','Warning')
                    return;
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                nPhasesInBatch=length(cDataBatch)/nPPL;
                d = reshape(cDataBatch,nPPL,nPhasesInBatch);
                Data2D = cat(2,Data2D,d);
                count = count+1;
                if nPhaseBatches>1
                    waitbar(count/cn,h_waitbar,sprintf('Radial (por favor espere)... %5.2f %%',100*count/cn));
                end
            end
            if nPhaseBatches>1
                delete(h_waitbar);
            end
            Data2D = Data2D(:);
            cDataCal = permute(ones(nLPC*nCir,nPPL)*diag(cDataCal),[2 1]);
            cDataCal = cDataCal(:);
            Data2Dcal = Data2D-cDataCal;
%             Data2Dcal = Data2D;
            kSpaceValuesCal = [kSpaceValues,Data2Dcal(:)];
            kSpaceValues = [kSpaceValues,Data2D(:)];
            tBatch2 = clock;
            tBatch = etime(tBatch2,tBatch1);
            rawData.kSpace.sampled = kSpaceValues;
            rawData.kSpace.sampledCalibrated = kSpaceValuesCal;
                        
            %% Regridding
            kSpaceValues(:,4) = kSpaceValuesCal(:,4);
            if(nCir>1)
                valCartesian = griddata(kSpaceValues(:,1),kSpaceValues(:,2),kSpaceValues(:,3),kSpaceValues(:,4),kCartesian(:,1),kCartesian(:,2),kCartesian(:,3));
            else
                valCartesian = griddata(kSpaceValues(max(nPoints)+1:end,1),kSpaceValues(max(nPoints)+1:end,2),kSpaceValues(max(nPoints)+1:end,4),kCartesian(:,1),kCartesian(:,2));
            end
            valCartesian(isnan(valCartesian)) = 0;
            rawData.kSpace.interpolated = [kx(:),ky(:),kz(:),valCartesian(:)];
            
            obj.SetParameter('TRANSIENTTIME',td);
            
            % Save elapsed times to raw data
            elapsedTime = toc;
%             rawData.aux.tPrescan = tPrescan;
            rawData.aux.tRadial = tRadial;
            rawData.aux.tBatch = tBatch;
            rawData.aux.elapsedTime = elapsedTime;
            
            % Save rawData
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(strcat('C:\Users\josalggui\Documents\repository-mri-lab-2018.11.05\MRI Control\FileDir\',name),'rawData');
        end
    end
    
end

