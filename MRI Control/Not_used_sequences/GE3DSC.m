classdef GE3DSC < MRI_BlankSequence
    % Gradient Echo sequence so that the input parameters are user
    % parameters: FOV in each axes, resolution, etc.
    
    %  Elena Diaz Caballero
    
    % 14.07.2019 - Mods from Jos� Miguel Algar�n.
    
    properties
        StartSlice;
        EndSlice;
        PhasesPerBatch = 20;
        PhasesinThisBatch = 1;
        StartPhase;
        EndPhase;
        slice_ampl;
        slice_ampl_re;
        acq_time;
        PhaseTime;
        RepDelay;
        RO_strength_rep;
        RO_strength_dep;
        NRO;
        NPH;
        NSL;
%         PH_strength_max;
%         SL_strength_max;
        calibrationactive = 0;
        calibrated = 0;
        Axis1;
        Axis2;
        Axis3;
        CAxis1;
        CAxis2;
        CAxis3;
        rdspeed_mult = 1;
        nPhasesBatch;
        NP_calib = 1;
        ROmax = 0;
        CalData;
        FOV;
        gam = 42.5775E6; %gyromagnetic ratio, 42.57 MHz/T
        gradientList;
        nSteps = 1;
    end
    methods (Access=private)
        function CreateParameters(obj)
            
            % Input parameters
            obj.CreateOneParameter('TE','TE time','ms',1*MyUnits.ms);
            obj.CreateOneParameter('TR','TR time','ms',50*MyUnits.ms);
            obj.CreateOneParameter('FOVRO','FOV Readout','mm',10*MyUnits.mm);
            obj.CreateOneParameter('FOVPH','FOV Phase','mm',10*MyUnits.mm);
            obj.CreateOneParameter('FOVSL','FOV Slice','mm',10*MyUnits.mm);
            obj.CreateOneParameter('NPOINTS','N Readout','',10);
            obj.CreateOneParameter('NPHASES','N Phase','',10);
            obj.CreateOneParameter('NSLICES','N Slice','',10);
            obj.CreateOneParameter('ECHODELAY','Gradient Echo Delay','us',0*MyUnits.us);
            obj.CreateOneParameter('ACQTIME','Acquisition Time','us',400*MyUnits.us);
            obj.CreateOneParameter('GRADIENTDELAY','Gradient Delay','us',55*MyUnits.us);
            obj.CreateOneParameter('REPETITIONDELAY','Repetition Delay','ms',50*MyUnits.ms);
            obj.CreateOneParameter('DELTARO','RO Deviation','mm',0);
            obj.CreateOneParameter('DELTAPH','PH Deviation','mm',0);
            obj.CreateOneParameter('DELTASL','SL Deviation','mm',0);
            
            % Output parameters
            OutputParameters = {...
                'MAXPH','RO_STRENGTH','MAXSL'...
                };
            obj.OutputParNames = [containers.Map(obj.OutputParNames.keys(),obj.OutputParNames.values());containers.Map(OutputParameters,...
                {...
                'Start Phase', 'Readout amplitude', 'Start Slice'...
                })];
            obj.OutputParValues = [containers.Map(obj.OutputParValues.keys(),obj.OutputParValues.values());containers.Map(OutputParameters,...
                {...
                NaN,NaN,NaN...
                })];
            obj.OutputParUnits = [containers.Map(obj.OutputParUnits.keys(),obj.OutputParUnits.values());containers.Map(OutputParameters,...
                {...
                '','',''...
                })];
            for k=1:length(obj.OutputParameters)
                OutputParameters(strcmp(obj.OutputParameters{k},OutputParameters)) = [];
            end
            obj.OutputParameters = [obj.OutputParameters,OutputParameters];
            
            % Hide parameters
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
                'BLANKINGDELAY',...
                'TRANSIENTTIME','COILRISETIME','SPOILERAMP',...
                'SPOILERTIME','SPECTRALWIDTH','REPETITIONDELAY'};
            
            % Rearange parameters
            obj.MoveParameter(14,15);
            obj.MoveParameter(17,25);
            obj.MoveParameter(17,25);
            obj.MoveParameter(5,21);
            obj.MoveParameter(5,22);
            obj.MoveParameter(15,12);
            obj.MoveParameter(16,13);
            obj.MoveParameter(26,9);
            obj.MoveParameter(24,12);
            obj.MoveParameter(27,12);

        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = GE3DSC(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'GE3DSC Real units';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
            
            obj.SetParameter('RFAMPLITUDE',0.01);
            obj.SetParameter('BLANKINGDELAY',15*MyUnits.us);
            obj.SetParameter('TRANSIENTTIME',0.1*MyUnits.us);
            obj.SetParameter('COILRISETIME',100*MyUnits.us);
            obj.SetParameter('NPOINTS',10);
            obj.SetParameter('NPHASES',10);
            obj.SetParameter('NSLICES',10);
            
            % This value is empherical and picked based on not creating eddy currents
            obj.SetParameter('FREQUENCY',11*MyUnits.MHz);
            obj.SetParameterMin('FREQUENCY',10*MyUnits.MHz);
            obj.SetParameterMax('FREQUENCY',15*MyUnits.MHz);
            
            % Grab the calibration data if there
            try
                obj.CalData = GradientCalibrationData();
            catch
                obj.CalData.X_TD = 1;
                obj.CalData.Y_TD = 1;
                obj.CalData.Z_TD = 1;
                obj.CalData.X_TV = 1;
                obj.CalData.Y_TV = 1;
                obj.CalData.Z_TV = 1;
            end
            
        end
        function UpdateTETR(obj)
            obj.TE = obj.GetParameter('TE');
            obj.TR = obj.GetParameter('TR');
            [~,~,~,ENSL,ENPH,~,~] = obj.ConfigureGradients(0);
            obj.NPH = obj.GetParameter('NPHASES')*ENPH+1*~ENPH;
            obj.NSL = obj.GetParameter('NSLICES')*ENSL+1*~ENSL;

            obj.TTotal = obj.TR * obj.GetParameter('NSCANS')* obj.NSL * obj.NPH;
        end
        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence,
            %         "1", central frequency adjustment sequence,
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            
            % Configure gradients and axes
            [~,~,readout,slice,phase,spoiler,~] = obj.ConfigureGradients(mode);
            
            % Set the number of lines to be read
            nPhases = obj.nPhasesBatch;
            
            % Calculate the number of line reads
            obj.nline_reads=nPhases;
            
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions = 3+(5+8*obj.nSteps)*nPhases;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            
            % readout gradient amplitude calculation
            % Again, I do not like to change the gradient values, if some
            % gradient amplitude is larger than 1, just stop runing and
            % give a warning, but we cannot just change and continue.
            % Actually this should be checked in the RUN instead of in the
            % CreateSequence.
            % Also, I will write shimming values explicitly. In this way it
            % is more easy to write the rise gradient.
            if( readout == 0 )
                dephasing_readout = 0;
                rephasing_readout = 0;
            else
                dephasing_readout = obj.RO_strength_dep;
                rephasing_readout = obj.RO_strength_rep;
            end
            
            obj.ROmax = abs(rephasing_readout);
            
            % Load timing from RawData;
            blkTime         = obj.GetParameter('BLANKINGDELAY')*1e6;
            pulseTime       = obj.GetParameter('PULSETIME')*1e6;
            deadTime        = obj.GetParameter('TRANSIENTTIME')*1e6;
            crTime          = obj.GetParameter('COILRISETIME')*1e6;
            phaseTime       = obj.PhaseTime*1e6;
            echoDelayTime   = obj.GetParameter('ECHODELAY')*1e6;
            gradDelayTime   = obj.GetParameter('GRADIENTDELAY')*1e6;
            acqTime         = obj.GetParameter('ACQTIME')*1e6;
            repDelayTime    = obj.RepDelay*1e6;
            
            
            % BEGIN THE PULSE PROGRAMMING
            iIns = 1;
            % SHIMMING in all gradients (3 instructions)
            iIns = obj.PulseGradient(iIns,shimR,RR,0.1);
            iIns = obj.PulseGradient(iIns,shimP,PP,0.1);
            iIns = obj.PulseGradient(iIns,shimS,SS,0.1);
            for phasei = 1:obj.nline_reads
                
                % Get the amplitude for the current line
                phase_amplitude = obj.gradientList(phasei,1);
                slice_amplitude = obj.gradientList(phasei,2);
                
                %------ RF pulse (3 instruction)
                iIns = obj.CreatePulse(iIns,0,blkTime,pulseTime,deadTime);
                
                % Activate dephasing gradients - readout, phase and slice
                % (3*nSteps instructions)
                for ii = 1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimR+dephasing_readout*ii/obj.nSteps,RR,0.1);
                    iIns = obj.PulseGradient(iIns,shimP+phase_amplitude*ii/obj.nSteps,PP,0.1);
                    iIns = obj.PulseGradient(iIns,shimS+slice_amplitude*ii/obj.nSteps,SS,crTime/obj.nSteps-0.2);
                end
                obj.DELAYs(iIns-1) = crTime/obj.nSteps-0.2+phaseTime;
                
                % Deactivate dephasing gradients - readout, phase and slice
                % (3*nSteps instructions)
                for ii = 1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimR+dephasing_readout*(1-ii/obj.nSteps),RR,0.1);
                    iIns = obj.PulseGradient(iIns,shimP+phase_amplitude*(1-ii/obj.nSteps),PP,0.1);
                    iIns = obj.PulseGradient(iIns,shimS+slice_amplitude*(1-ii/obj.nSteps),SS,crTime/obj.nSteps-0.2);
                end
                
                % Activate rephasing gradient - readout (nSteps instructions)
                for ii = 1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimR+rephasing_readout*ii/obj.nSteps,RR,crTime/obj.nSteps);
                end
                obj.DELAYs(iIns-1) = crTime/obj.nSteps+gradDelayTime+echoDelayTime;
                
                % Acquisition (1 instruction)
                iIns = obj.Acquisition(iIns,acqTime);
                
                % Deactivate rephasing gradient - readout (nSteps instructions)
                for ii = 1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimR+rephasing_readout*(1-ii/obj.nSteps),RR,crTime/obj.nSteps);
                end
                
                % Repetition delay (1 instruction)
                if phasei==obj.nline_reads
                    iIns = obj.RepetitionDelay(iIns,1,repDelayTime);
                else
                    iIns = obj.RepetitionDelay(iIns,0,repDelayTime);
                end
                
            end % end for nPhases
            %             end %end for slicei
            
        end % end create gradient echo pulse sequence
        function [status,result] = Run(obj,iAcq)
            clc
            global MRIDATA
            MRIDATA.ADC = [];
            
            global rawData
            rawData = [];

            % Check which gradients are switched on
            [~,~,ENRO,ENSL,ENPH,spoilerEnable,~] = obj.ConfigureGradients(0);
            
            % Load amplitude calibration for 1V
            bwCal = load('rawData-BW-Calibration.mat');
            bwCalVal = bwCal.rawData.outputs.fidMean;
            bwCalVec = logspace(log10(bwCal.rawData.inputs.bwMin),...
                log10(bwCal.rawData.inputs.bwMax),...
                bwCal.rawData.inputs.steps);
            
            % Field of View
            FOVRO = obj.GetParameter('FOVRO');
            FOVPH = obj.GetParameter('FOVPH');
            FOVSL = obj.GetParameter('FOVSL');
            rawData.inputs.Seq = obj.SequenceName;
            rawData.inputs.NEX = obj.GetParameter('NSCANS');
            rawData.inputs.fov = [FOVRO,FOVPH,FOVSL];
            DELRO = obj.GetParameter('DELTARO');
            DELPH = obj.GetParameter('DELTAPH');
            DELSL = obj.GetParameter('DELTASL');
            rawData.inputs.fovDeviation = [DELRO,DELPH,DELSL];
            
            % Matrix size
            obj.NRO = obj.GetParameter('NPOINTS');
            obj.NPH = obj.GetParameter('NPHASES')*ENPH+1*~ENPH;
            obj.NSL = obj.GetParameter('NSLICES')*ENSL+1*~ENSL;
            rawData.inputs.axes = strcat(obj.GetParameter('READOUT'),...
                obj.GetParameter('PHASE'),obj.GetParameter('SLICE'));
            rawData.inputs.axisEnable = [ENRO,ENPH,ENSL];
            rawData.inputs.nPoints = [obj.NRO,obj.NPH,obj.NSL];
            
            % Resolution
            ResRO = FOVRO/(obj.NRO-1);
            ResPH = FOVPH/(obj.NPH-1);
            ResSL = FOVSL/(obj.NSL-1);
            rawData.aux.resolution = [ResRO,ResPH,ResSL];
            
            % Parameters for image weighting
            TE = obj.GetParameter('TE');
            TR = obj.GetParameter('TR');
            rawData.inputs.TE = TE;
            rawData.inputs.TR = TR;
            
            % Miscellaneous
            CR = obj.GetParameter('COILRISETIME');
            ED = obj.GetParameter('ECHODELAY');
            GD = obj.GetParameter('GRADIENTDELAY');
            rawData.aux.coilRiseTime = CR;
            rawData.inputs.echoDelay = ED;
            rawData.inputs.gradientDelay = GD;
            
            % RF parameters
            RF = obj.GetParameter('PULSETIME');
            obj.acq_time = obj.GetParameter('ACQTIME');
            BW = obj.NRO/obj.acq_time;
            obj.SetParameter('SPECTRALWIDTH',BW);
            rawData.inputs.pulseTime = RF;
            rawData.inputs.amplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.frequency = obj.GetParameter('FREQUENCY');
            rawData.inputs.acqTime = obj.acq_time;
            rawData.aux.bandwidth = BW;
            
            % Dephasing time
            obj.PhaseTime = TE-0.5*obj.acq_time-3*CR-GD-0.5*RF;
            rawData.aux.phaseTime = obj.PhaseTime;
            % check if the phase time can be possible given the selected TE, if not adjust TE as needed
            if obj.PhaseTime <= 0
                warndlg('The dephasing time is negative!','Dephasing Error')
                return;
            end            
            
            % Repetition delay
            obj.RepDelay  = TR-(obj.GetParameter('BLANKINGDELAY')+RF+...
                4*CR+obj.PhaseTime+ED+obj.acq_time);
            rawData.aux.repetitionDelay = obj.RepDelay;
            % check if the phase time can be possible given the selected TE, if not adjust TE as needed
            if obj.RepDelay <=0
                warndlg('The repetition delay is negative!','TR error')
                return
            end
            obj.SetParameter('REPETITIONDELAY',obj.RepDelay);
            
            % Save in rawData the sequence timing    
            rawData.aux.blkTime = obj.GetParameter('BLANKINGDELAY');
            rawData.aux.pulseTime = RF;
            rawData.aux.deadTime = obj.GetParameter('TRANSIENTTIME');
            rawData.aux.coilRiseTime = obj.GetParameter('COILRISETIME');
            rawData.aux.phaseTime = obj.PhaseTime;
            rawData.aux.echoDelayTime = obj.GetParameter('ECHODELAY');
            rawData.aux.acquisitionTime = obj.acq_time;
            rawData.aux.repDelayTime = obj.RepDelay;
            
            % Readout gradients
            obj.RO_strength_rep = BW ./ ( obj.gam * FOVRO );
            obj.RO_strength_dep = -0.5*obj.RO_strength_rep*(obj.acq_time+CR)/(obj.PhaseTime+CR); % We need to take into account the coil rise time
            rawData.aux.dephasingGradient = obj.RO_strength_dep*ENRO;
            rawData.aux.rephasingGradient = obj.RO_strength_rep*ENRO;
            
            % Phase and slice max gradients
            PH_strength_max = (obj.NPH-1)/(2*obj.gam*FOVPH*(obj.PhaseTime+CR));
            SL_strength_max = (obj.NSL-1)/(2*obj.gam*FOVSL*(obj.PhaseTime+CR));
            rawData.aux.phaseGradient = PH_strength_max*ENPH;
            rawData.aux.sliceGradient = SL_strength_max*ENSL;
            
            % Get gradient space
            sliceGradients = linspace(-SL_strength_max,SL_strength_max,obj.NSL*ENSL+1*~ENSL)'*ENSL;
            phaseGradients = linspace(-PH_strength_max,PH_strength_max,obj.NPH*ENPH+1*~ENPH)'*ENPH;
            [phaseGradients,sliceGradients] = meshgrid(phaseGradients,sliceGradients);
            phaseGradients = phaseGradients';
            sliceGradients = sliceGradients';
            phaseGradients = phaseGradients(:);
            sliceGradients = sliceGradients(:);
            indG = (1:length(sliceGradients))';
            indGinv = indG;
%             [~,indG] = sort(rand(size(phaseGradients(:))));
            phaseGradients = phaseGradients(indG);
            sliceGradients = sliceGradients(indG);
            indGinv = indGinv(indG);
            [~,indGinv] = sort(indGinv);
            rawData.aux.gradientList = [phaseGradients,sliceGradients];
            
            % Set the gradient amplitudes in arbitrary units
            c = obj.ReorganizeCalibrationData();
            gradientsArray = rawData.aux.gradientList/diag(c(2:3));
            obj.RO_strength_rep = obj.RO_strength_rep/c(1);
            obj.RO_strength_dep = obj.RO_strength_dep/c(1);
            
            obj.SetParameter('MAXPH',PH_strength_max);
            obj.SetParameter('RO_STRENGTH',obj.RO_strength_rep);
            obj.SetParameter('MAXSL',SL_strength_max);
            
            % Get k-space points
            nPoints = obj.NRO;
            nSlices = obj.NSL;
            nPhases = obj.NPH;
            kMax = 0.5./rawData.aux.resolution;
            kx = linspace(-kMax(1),kMax(1),nPoints);
            kPointsX = zeros(nPoints*nPhases*nSlices,1);
            kPointsY = zeros(nPoints*nPhases*nSlices,1);
            kPointsZ = zeros(nPoints*nPhases*nSlices,1);
            for ii = 1:nPhases*nSlices
                kPointsX((ii-1)*nPoints+1:ii*nPoints) = kx;
                kPointsY((ii-1)*nPoints+1:ii*nPoints) = obj.gam*phaseGradients(ii)*(obj.PhaseTime+CR);
                kPointsZ((ii-1)*nPoints+1:ii*nPoints) = obj.gam*sliceGradients(ii)*(obj.PhaseTime+CR);
            end
            
            % Show time bar
            if isfield(MRIDATA,'Data3Dnew')
                if ( isequal(size(MRIDATA.Data3Dnew),[obj.NRO, obj.NPH, obj.NSL, obj.NAverages])) || (iAcq == 1)
                    MRIDATA.Data3Dnew = [];
                end
            else
                MRIDATA.Data3Dnew = [];
            end
            
            % I will set up the phases per batch to the maximun it will
            % depend on the number of orders per phase(replace 65 by the number of
            % orders per phase)
            % I will set the number of elements in the 'for' to reduce the
            % number of batches to the minimun. I do not care at all about
            % the slice or phase direction. I have the gradientList, so we
            % just need to pass the gradientList over the Create sequence
            % as few times as possible. In this way we go out of the
            % stationary state as few times as possible.
            obj.PhasesPerBatch = floor(min([(2048-3)/(5+8*obj.nSteps),16000/nPoints]));
            nBatches = ceil(nPhases*nSlices/obj.PhasesPerBatch);
            cn = nBatches;
            count = 0;
            if nBatches>1
                h_waitbar = waitbar(count/cn,'Please wait (por favor espere)...');
            end
            
            Data2D = [];
            t0 = tic;
            for iBatch = 1:nBatches
                % Get value to normalize fid to Volts
                [~,bwCalNormPos] = min(abs(bwCalVec-BW));
                bwCalNormVal = bwCalVal(bwCalNormPos);
                
                % Calculate remaining time
                tA = clock;
                if(iBatch==1)
                    clc
                    fprintf('Batch %1.0f/%1.0f, estimating remaining time \n',iBatch,nBatches);
                end
                    
%                 Get central frequency
                if(spoilerEnable)
                    if (toc(t0)>=300 || iBatch==1)
                        t0 = tic;
                        fprintf('Calibrating frequency... \n')
                        f0 = obj.GetFIDParameters;
                        rawData.aux.freqList(iBatch) = f0;
                        fprintf('Frequency = %1.4f MHz \n',f0*1d-6)
                        obj.SetParameter('FREQUENCY',f0);
                    end
                end
                
                % Get the appropiate axis
                if(iBatch<nBatches)
                    obj.gradientList = gradientsArray((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,:);
                else
                    obj.gradientList = gradientsArray((iBatch-1)*obj.PhasesPerBatch+1:end,:);
                end
                obj.nPhasesBatch = size(obj.gradientList,1);
                
                % NOW RUN ACTUAL ACQUISTION
                %obj.acq_time = 5*(4*CR+obj.PhaseTime+obj.acq_time);
                obj.CreateSequence(0); % Create the normal gradient echo sequence
                obj.Sequence2String;
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                [status,result] = system(fullfile(obj.path_file,fname));
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
%                     obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2))/bwCalNormVal;
                nPhasesInBatch=length(cDataBatch)/nPoints;
                d = reshape(cDataBatch,nPoints,nPhasesInBatch);
                Data2D = cat(2,Data2D,d);
                
                count = count+1;
                tB = clock;
                tC = etime(tB,tA);
                if nBatches>1
                    tC = tC*(nBatches-iBatch+1);
                    clc
                    fprintf('Batch %1.0f/%1.0f, remaining time: %1.0f s \n',iBatch,nBatches,tC);
                    waitbar(count/cn,h_waitbar,sprintf('%5.2f %%, estimated remaining time: %1.0f s',100*count/cn,tC));
                end
            end
            Data3D = squeeze(reshape(Data2D,nPoints,nPhases,nSlices));
            
            % Correct gradient isocenter
            phase = exp(-2*pi*1i*(DELRO*kPointsX+DELPH*kPointsY+DELSL*kPointsZ));
            
            % Save acquired k-space
            kPointsS = Data3D(:).*phase;
            for ii = 1:nPhases*nSlices
                kPoints((ii-1)*nPoints+1:ii*nPoints,1) = kPointsX((indGinv(ii)-1)*nPoints+1:indGinv(ii)*nPoints);
                kPoints((ii-1)*nPoints+1:ii*nPoints,2) = kPointsY((indGinv(ii)-1)*nPoints+1:indGinv(ii)*nPoints);
                kPoints((ii-1)*nPoints+1:ii*nPoints,3) = kPointsZ((indGinv(ii)-1)*nPoints+1:indGinv(ii)*nPoints);
                kPoints((ii-1)*nPoints+1:ii*nPoints,4) = kPointsS((indGinv(ii)-1)*nPoints+1:indGinv(ii)*nPoints);
            end
            clear kPointsX kPointsY kPointsZ
            rawData.kSpace.sampled = kPoints;
            rawData.aux.kMax = kMax;
            
            Data3D_use = Data3D(:);
                
            if obj.averaging
                Nav = obj.NAverages;
            else
                Nav = 1;
            end
            index = mod(iAcq-1,Nav)+1;
            if obj.autoPhasing
                %                    pt = max(Data3D(:,ceil(size(Data3D,2)/2),ceil(size(Data3D,3)/2)));
                pt = max(Data3D_use(:));
                Data3D_use = Data3D_use.*exp(complex(0,-angle(pt)));
            end
            obj.cData(index,1:length(Data3D_use(:))) = Data3D_use(:);
                
            obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
            obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
            %    obj.fData1D = fftshift(ifft(fftshift(obj.cDataAv)));
            obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
%             obj.acq_time = obj.GetParameter('ACQUISITIONTIME');
            obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*obj.acq_time/nPhases;
            obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/obj.acq_time/nPhases;
            
            if nBatches>1
                delete(h_waitbar);
            end

            kSpace = squeeze(reshape(rawData.kSpace.sampled(:,4),rawData.inputs.nPoints));
            imagen = abs(ifftshift(ifftn(kSpace)));
            rawData.kSpace.imagen = imagen;
            
            % Save rawData
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
        
        
        function RunCalibrationAxisGeneration( obj )
            % this will run the calibration on the actively stored data
            global MRIDATA
            
            obj.CAxis1 = [];
            obj.CAxis2 = [];
            obj.CAxis3 = [];
            
            % keep in mind that this is for only 2 axis at the moment
            % first axis is the readout direction, second is the phase
            % direction
            for iADC = 1:numel(MRIDATA.ADC)
                
                    Full_TriggerAxis = MRIDATA.ADC(iADC).CMatrix1(1,:)';
                    Full_DAxis1 = MRIDATA.ADC(iADC).CMatrix1(2,:)';
                    Full_DAxis2 = MRIDATA.ADC(iADC).CMatrix1(3,:)';
                    Full_DAxis3 = MRIDATA.ADC(iADC).CMatrix1(4,:)';
                    
                    % pulse parameters
                    
                    [AcqT, start_measure, end_measure, after_RF, total_measure_time] = obj.CalcTiming;
                                       
                    nP = MRIDATA.ADC(iADC).nPhases;
                    NP = obj.GetParameter('NPOINTS');
                    
                    % measurement parameters
                    RdSpeed = (40E6)/obj.rdspeed_mult;
                    dt = 1/RdSpeed;

                    % measure inds
                    start_ind = floor(start_measure/dt);
                    end_ind   = obj.NP_calib;
                    after_RF_ind = floor(after_RF/dt);
                    
                    % find the ind when the trigger is below a certain threshold that is
                    % considered as '0'
                    trigthresh_up   = 1E-1;
                    Full_TriggerAxis ( Full_TriggerAxis > trigthresh_up )  =  1;
                    Full_TriggerAxis = round(Full_TriggerAxis);
                    
                    % starting offset
                    DAxis1Sum = zeros(NP,nP);
                    DAxis2Sum = zeros(NP,nP);
                    DAxis3Sum = zeros(NP,nP);
                    
%                     dfilter = fdesign.lowpass('N,Fc',1,1,100);
%                     Hd = design(dfilter);
                    
                    for ip = 1:nP
                        % Rewrite the axes
                        TriggerAxis = Full_TriggerAxis(1+(ip-1)*obj.NP_calib:ip*obj.NP_calib);
                        DAxis1 = Full_DAxis1(1+(ip-1)*obj.NP_calib:ip*obj.NP_calib);
                        DAxis2 = Full_DAxis2(1+(ip-1)*obj.NP_calib:ip*obj.NP_calib);
                        DAxis3 = Full_DAxis3(1+(ip-1)*obj.NP_calib:ip*obj.NP_calib);
                        
                        
                        % find the ind where the start is and then where the end is
                        inds = find( TriggerAxis == 1, 1, 'first' );
                        inde = find( TriggerAxis(inds:end) == 0, 1, 'first' )+inds-1;
                        
                        % Grab the 'zero' amount - remove the shimming bias
                        d1shim = median( DAxis1(inds:inde) );
                        d2shim = median( DAxis2(inds:inde) );
                        d3shim = 0;%median( DAxis3(inds:inde) );
                        
                        
                        % grab data and smooth it
%                         SelectedData1 = filter( Hd, DAxis1(inde+after_RF_ind:inde+end_ind)-d1shim );
                        SelectedData1 = DAxis1(inde+after_RF_ind:end)-d1shim;
%                         SelectedData1 = smooth( DAxis1(inde+after_RF_ind:inde+end_ind)-d1shim,11, 'rloess' );
                        % now sum the data to determine where in KSpace you are
                        DAxis1SumFull = obj.gam * ( cumsum( SelectedData1 * dt ) );
                        DAxis1SumSelect = DAxis1SumFull(start_ind-after_RF_ind:end);
                        D1interp = interp1(linspace(1,numel(DAxis1SumSelect),numel(DAxis1SumSelect)),...
                                          DAxis1SumSelect, linspace(1,numel(DAxis1SumSelect),NP),'pchip');
                        
                        DAxis1Sum(:,ip) = D1interp;
                        
                        % grab data and smooth it
%                         SelectedData2 = filter(Hd, DAxis2(inde+after_RF_ind:inde+end_ind)-d2shim );
                        SelectedData2 = DAxis2(inde+after_RF_ind:end)-d2shim;
%                         SelectedData2 = smooth( DAxis2(inde+after_RF_ind:inde+end_ind)-d2shim,11, 'rloess' );
                        % now sum the data to determine where in KSpace you are
                        DAxis2SumFull = obj.gam * ( cumsum( SelectedData2 * dt ));
                        DAxis2SumSelect = DAxis2SumFull(start_ind-after_RF_ind:end);
                        D2interp = interp1(linspace(1,numel(DAxis2SumSelect),numel(DAxis2SumSelect)),...
                                          DAxis2SumSelect, linspace(1,numel(DAxis2SumSelect),NP),'pchip');
                        
                        DAxis2Sum(:,ip) = D2interp;
                        
                        % grab data and smooth it
% %                         SelectedData3 = filter(Hd, DAxis3(inde+after_RF_ind:inde+end_ind)-d3shim );
                        SelectedData3 = DAxis3(inde+after_RF_ind:end)-d3shim;
%                         SelectedData3 = smooth( DAxis3(inde+after_RF_ind:inde+end_ind)-d3shim,11, 'rloess' );
                        % now sum the data to determine where in KSpace you are
                        DAxis3SumFull = obj.gam * ( cumsum( SelectedData3 * dt ));
                        DAxis3SumSelect = DAxis3SumFull(start_ind-after_RF_ind:end);
                        D3interp = interp1(linspace(1,numel(DAxis3SumSelect),numel(DAxis3SumSelect)),...
                                          DAxis3SumSelect, linspace(1,numel(DAxis3SumSelect),NP),'pchip');
                        
                        DAxis3Sum(:,ip) = D3interp;
                        
                    end
                    
                        obj.CAxis1 = cat(2, obj.CAxis1, DAxis1Sum);
                        obj.CAxis2 = cat(2, obj.CAxis2, DAxis2Sum);
                        obj.CAxis3 = cat(2, obj.CAxis3, DAxis3Sum);
                        
            end
            
%             reform axis values
            obj.CAxis1 = obj.CAxis1(:);
            obj.CAxis2 = obj.CAxis2(:);
            obj.CAxis3 = obj.CAxis3(:);
            
        end
              
        
        function [AcqT, start_measure, end_measure, after_RF, total_measure_time] = CalcTiming( obj )
            
            % pulse parameters
                    BD = obj.GetParameter('BLANKINGDELAY');
                    RF = obj.GetParameter('PULSETIME');
                    TT = obj.GetParameter('TRANSIENTTIME');
                    AddP = 0.1E-6;
%                     PT = obj.GetParameter('PHASETIME');
                    CR = obj.GetParameter('COILRISETIME');
                    ED = obj.GetParameter('ECHODELAY');
                    NP = obj.GetParameter('NPOINTS');
                    SW = obj.GetParameter('SPECTRALWIDTH');
                    AcqT = NP/SW;
                    

                    % specify start and end of reading
                    after_RF = 0;%1*BD+RF;
                    start_measure = after_RF+TT+2*AddP+2*obj.PhaseTime+3*CR+ED;
                    end_measure   = start_measure+AcqT;
                    total_measure_time = end_measure+BD+RF;

        end
    end
end

