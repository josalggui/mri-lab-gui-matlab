classdef PETRA3 < MRI_BlankSequence
    %%
    
    % PETRA CODE
    
    %*******************************************************
    %** J. M. Algar�n                                     **
    %** CSIC/UPV                                          **
    %** I3M                                               **
    %** Avda. dels Tarongers, 12,46022, Valencia, (Spain) **
    %** Tel: +34 960 728 111                              **
    %** email: josalggui@i3m.upv.es                       **
    %*******************************************************
    
    %%
    
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    %  Elena Diaz Caballero
    properties
        PhasesPerBatch = 20;
        nPhasesBatch;
        Axis1Vector;
        Axis2Vector;
        Axis3Vector;
        StartSlice;
        lastBatch;
        EndSlice;
        StartPhase;
        EndPhase;
        Stage;              % ZTE -> 1, Single Point ->2 or Prescan -> 0
        AcquisitionTime;
        slice_ampl;
        readoutAmplitudeP = 0;
        phaseAmplitudeP = 0;
        sliceAmplitudeP = 0;
        repetitionDelay;
        gRingDownCal = [0.02 0.01];   % Gradients for ring-down calibration
    end
    methods (Access=private)
        function CreateParameters(obj)
            
            % HELP %
            % I include here new inputs to discriminate between
            % acquistition and regruidding.
            % -> NPHASES, NREADOUTS and NSLICES are for regridding
            % -> NPOINTS, NCIRCUNFERENCES, NLINESPERCIRCUNFERENCES are for
            %    aqcuisition.
            CreateOneParameter(obj,'NREADOUTS','Nx','',50)
            CreateOneParameter(obj,'NPHASES','Ny','',50)
            CreateOneParameter(obj,'NSLICES','Nz','',50)
            CreateOneParameter(obj,'FOVX','FOV x','mm',20*MyUnits.mm)
            CreateOneParameter(obj,'FOVY','FOV y','mm',20*MyUnits.mm)
            CreateOneParameter(obj,'FOVZ','FOV z','mm',20*MyUnits.mm)
            CreateOneParameter(obj,'TMAX','T max','us',70*MyUnits.us)
            CreateOneParameter(obj,'REPETITIONTIME','TR','ms',500*MyUnits.ms)
            CreateOneParameter(obj,'UNDERSAMPLING','Undersampling','',1);
            CreateOneParameter(obj,'JUMPINGFACTOR','Jumping factor','',0);
            CreateOneParameter(obj,'TRANSIENTTIME','Dead time','us',0.1e-6);
            CreateOneParameter(obj,'REPETITIONDELAY','Repetition Delay','ms',0.1);
            CreateOneParameter(obj,'DELTAX','X-axis deviation','mm',0);
            CreateOneParameter(obj,'DELTAY','Y-axis deviation','mm',0);
            CreateOneParameter(obj,'DELTAZ','Z-axis deviation','mm',0);
            CreateOneParameter(obj,'NRADIAL','N radial blocks','',1);
            
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
                'NPOINTS','SPECTRALWIDTH','SPOILERAMP','REPETITIONDELAY',...
                'SPOILERTIME','BLANKINGDELAY'};
            
            % Reorganizes InputParameters
            obj.MoveParameter(6,20);
            obj.MoveParameter(24,12);
            obj.MoveParameter(25,13);
            obj.MoveParameter(19,15);
            obj.MoveParameter(16,17);
            obj.MoveParameter(29,26);
            obj.MoveParameter(30,27);
            obj.MoveParameter(31,28);
            
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = PETRA3(program)
            obj = obj@MRI_BlankSequence();
            obj.SequenceName = 'PETRA V1';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence(obj);
        end
        function UpdateTETR(obj)
            
            global rawData;
            rawData = [];
            
            obj.TE = 0;
            obj.TR = obj.GetParameter('REPETITIONTIME');
            
            % Get cartesian parameters
            obj.GetCartesianParameters;
            
            % Get sequence parameters
            obj.GetSequenceParameters;
            
            % Set number of phases, points and slices as well as amplitudes
            obj.SetSamplingParameters;
            
            % Calculate the gradients list for radial sampling
            obj.GetRadialGradients;
            
            % Calculate k-points matrix for radial sampling
            obj.GetRadialKPoints
            
            % k-points for Cartesian sampling
            obj.GetCartesianKPoints;
            
            nRepetitions = sum(rawData.aux.numberOfRadialRepetitions)+...
                rawData.aux.numberOfCartesianRepetitions;
            
            obj.TTotal = obj.TR * obj.GetParameter('NSCANS')*nRepetitions;
        end
        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence
            %         "1", prescan for ring down calibration
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR] = SelectAxes(obj);
            
            % Set the number of lines to be read
            nMisses = obj.GetParameter('JUMPINGFACTOR');
            
            if(mode==1)
                [~,nPhases,~,~,~,~,~] = obj.ConfigureGradients(mode);
                obj.Axis1Vector = 0;
                obj.Axis2Vector = 0;
                obj.Axis3Vector = 0;
            elseif(mode==0)
                [~,~,~,~,~,~,~] = obj.ConfigureGradients(mode);
                nPhases = obj.nPhasesBatch+nMisses;
            end
            
            % Calculate the number of line reads
            obj.nline_reads=nPhases-nMisses;
            
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions= (3*obj.nSteps+5)*nPhases;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6;
            crt = obj.GetParameter('COILRISETIME')*1e6;
            blk = obj.GetParameter('BLANKINGDELAY')*1e6;
            grad_delay = 300; %time for gradients to rise up + blk
            iIns = 1;
            pulseTime = obj.GetParameter('PULSETIME')*1e6;
            deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
            repDelayTime = obj.GetParameter('REPETITIONDELAY')*1e6;
            spoilerTime =  obj.GetParameter('SPOILERTIME')*1e6;
            maxGrad = max([max(obj.Axis1Vector),max(obj.Axis2Vector),max(obj.Axis3Vector)]);
            for phasei = 1:nPhases
                if(phasei<=nMisses)
                    readoutAmplitude = 0;
                    phaseAmplitude = 0;
                    sliceAmplitude = 0;
                else
                    readoutAmplitude = obj.Axis1Vector(phasei-nMisses);
                    phaseAmplitude = obj.Axis2Vector(phasei-nMisses);
                    sliceAmplitude = obj.Axis3Vector(phasei-nMisses);
                end
                
                %% Activate the total gradient (3 instrucctions)
                for ii=1:obj.nSteps
                    iIns = obj.PulseGradient(iIns,shimR+obj.readoutAmplitudeP+(readoutAmplitude-obj.readoutAmplitudeP)/obj.nSteps*ii,RR,0.1);
                    iIns = obj.PulseGradient(iIns,shimP+obj.phaseAmplitudeP+(phaseAmplitude-obj.phaseAmplitudeP)/obj.nSteps*ii,PP,0.1);
                    iIns = obj.PulseGradient(iIns,shimS+obj.sliceAmplitudeP+(sliceAmplitude-obj.sliceAmplitudeP)/obj.nSteps*ii,SS,crt/obj.nSteps-0.2);
                end
                obj.readoutAmplitudeP = readoutAmplitude;
                obj.phaseAmplitudeP = phaseAmplitude;
                obj.sliceAmplitudeP = sliceAmplitude;
                obj.DELAYs(iIns-1) = crt/obj.nSteps-0.2+grad_delay-5;
                
                %% RF pulse (3 instructions)
                iIns = obj.CreatePulse(iIns,0,blk,pulseTime,deadTime);
                if(obj.Stage==0)
                    obj.TXs(iIns-2) = 0;
                end
                
                %% Acquisition
                if(phasei<=nMisses)
                    iIns = obj.RepetitionDelay(iIns,0,acq_time+spoilerTime);
                else
                    iIns = obj.Acquisition(iIns,acq_time+spoilerTime);
                end
                
                %% Repetition Delay (1 instruction)
                if(obj.lastBatch==1 && phasei==nPhases)
                    iIns = obj.RepetitionDelay(iIns,1,repDelayTime);
                    disp('Last Batch!')
                elseif(maxGrad>0.25)
                    iIns = obj.RepetitionDelay(iIns,1,repDelayTime);
                else
                    iIns = obj.RepetitionDelay(iIns,0,repDelayTime);
                end
                if(obj.Stage~=0)
                    obj.DELAYs(iIns-1)   = repDelayTime; %time in us
                else
                    obj.DELAYs(iIns-1)   = acq_time;
                end
                
            end
        end
        function [status,result] = Run(obj,iAcq)
            tic;
            clc
            
            nn = 4;                                                         % Number of points to calibrate
            [~,~,~,~,~,spoilerEnable,~] = obj.ConfigureGradients(0);
            
            % Load amplitude calibration for 1V
            bwCal = load('rawData-BW-Calibration.mat');
            bwCalVal = bwCal.rawData.outputs.fidMean;
            bwCalVec = logspace(log10(bwCal.rawData.inputs.bwMin),...
                log10(bwCal.rawData.inputs.bwMax),...
                bwCal.rawData.inputs.steps);
            
            %% Constant to relate k with k'
            % HELP: k = gammabar*G*t
            % G = c*A where A is the gradient amplitude voltage from
            % radioprocessor. Then, k' = k/(gammabar*c) = A*t.
            % We still need to calibrate constant c. We also need to take
            % into account that c is different for each axis.
            gammabar = 42.6d6;          % MHz/T
            c = obj.ReorganizeCalibrationData();
            global rawData              % Raw Data contains output info
            rawData = [];
            
            % Get cartesian parameters
            obj.GetCartesianParameters;
            
            % Get sequence parameters
            obj.GetSequenceParameters;
            
            % Set number of phases, points and slices as well as amplitudes
            obj.SetSamplingParameters;
            
            % Calculate the gradients list for radial sampling
            obj.GetRadialGradients;
            gradientVectors1 = rawData.aux.gradientsRadialTeslasPerMeter;
            
            % Calculate k-points matrix for radial sampling
            obj.GetRadialKPoints
            kSpaceValues = rawData.aux.radialKPoints;
            
            % k-points for Cartesian sampling
            obj.GetCartesianKPoints;
            kSinglePoint = rawData.aux.cartesianKPoints;
            kSpaceValues = cat(1,kSpaceValues,kSinglePoint);
            
            % Gradients for Cartesian sampling
            tMin = rawData.aux.tMin;
            gradientVectors2 = kSinglePoint/diag(gammabar*tMin);
            rawData.aux.gradientsCartesianTeslasPerMeter = gradientVectors2;
            
            % Normalize gradients to arbitray units.
            gradientVectors1 = gradientVectors1/diag(c);
            gradientVectors2 = gradientVectors2/diag(c);
            if(sum(abs(gradientVectors1(:))>=1)>=1 || sum(isnan(gradientVectors1(:)))>=1 || sum(isinf(gradientVectors1(:)))>=1)
                warndlg('At least one non valid DAC amplitude in radial gradients!','Delay Error')
                return;
            end
            if(sum(abs(gradientVectors2(:))>=1)>=1 || sum(isnan(gradientVectors2(:)))>=1 || sum(isinf(gradientVectors2(:)))>=1)
                warndlg('At least one non valid DAC amplitude in single point gradients!','Delay Error')
                return;
            end
            
            %% Auxiliar
            Data2D = [];
            Data2DCal = [];
            nPPL = rawData.aux.numberOfPointsPerLine;
            nMisses = rawData.inputs.jumpingFactor;
            nPoints = rawData.inputs.nPoints;
            nCir = rawData.aux.numberOfCircumferences;
            acqTime = rawData.aux.acquisitionTime;
            nRadialBlocks = rawData.inputs.nRadialBlocks;
            nRepetitions = rawData.aux.numberOfRadialRepetitions;
            bw = rawData.aux.bandwidth;
            %             acqTime = obj.AcquisitionTime;
            
            %% Radial
            tBatch1 = clock;
            
            % Get central frequency
            %             fprintf('Calibrating frequency... \n')
            %             f0 = obj.GetFIDParameters;
            %             obj.SetParameter('FREQUENCY',f0);
            %             fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
            
            % Create waiting bar
            obj.Stage = 1;
            nFreq = 0;
            tstart= tic;
            telapsed = 0;
            for iBlock = 1:nRadialBlocks
                % Get value to normalize fid to Volts
                [~,bwCalNormPos] = min(abs(bwCalVec-bw(iBlock)));
                bwCalNormVal = bwCalVal(bwCalNormPos);
                
                % Repetitions per batch and number of repetitions
                obj.PhasesPerBatch = floor(min([2048/(3*obj.nSteps+5),16000/nPPL(iBlock)]))-nMisses;                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
                nPhaseBatches = ceil(nRepetitions(iBlock)/obj.PhasesPerBatch);
                
                % Get the gradients for current block
                gradientVectors = gradientVectors1(1+sum(nRepetitions(1:iBlock-1)):sum(nRepetitions(1:iBlock)),:);
                
                % Set parameters for current block
                obj.AcquisitionTime = acqTime(iBlock);
                obj.SetParameter('SPECTRALWIDTH',bw(iBlock));
                obj.SetParameter('NPOINTS',nPPL(iBlock));
                
                % Start batches sweep
                tRadial = 0;
                rawData.inputs.rfFrequencyRadial = zeros(nPhaseBatches,1);
                obj.readoutAmplitudeP = 0;
                obj.phaseAmplitudeP = 0;
                obj.sliceAmplitudeP = 0;
                
                for iBatch = 1:nPhaseBatches
                    tA = clock;
                    if(iBatch==1)
                        fprintf('Radial %1.0f, batch %1.0f/%1.0f, estimating remaining time \n',iBlock,iBatch,nPhaseBatches);
                    else
                        tC = tC*(nPhaseBatches-iBatch+1);
                        fprintf('Radial %1.0f, batch %1.0f/%1.0f, remaining time: %1.0f s \n',iBlock,iBatch,nPhaseBatches,tC);
                    end
                    if(spoilerEnable)
                        %                     if ~mod(iBatch,10)
                        fprintf('Calibrating frequency... \n')
                        % Get central frequency
                        f0 = obj.GetFIDParameters;
                        obj.SetParameter('FREQUENCY',f0);
                        nFreq = nFreq+1;
                        rawData.aux.freqListRadial(nFreq) = f0;
                        fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
                        
                        %                         srd = CalibrateRingDown(obj);
                        
                        %                     end
                        %                     else
                        %                         srd = zeros(50,1);
                    end
                    if telapsed>(3600/4) %Additional pause so that we are able to check in the VNA if the RF coil is losing the tuning
                        load gong.mat;
                        sound(y, Fs);
                        clear Fs y
                        h_warn = warndlg(char(strcat('Please check in VNA if the coil is tuned to: [MHz]', num2str( obj.GetParameter('FREQUENCY')/MyUnits.MHz))),'!!! Waiting to Continue !!!');
                        uiwait(h_warn);
                        tstart = tic;
                    end
                    
                    % Get the appropiate axis
                    if(iBatch<nPhaseBatches)
                        obj.Axis1Vector = gradientVectors((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,1);
                        obj.Axis2Vector = gradientVectors((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,2);
                        obj.Axis3Vector = gradientVectors((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,3);
                        obj.lastBatch = 0;
                    else
                        obj.Axis1Vector = gradientVectors((iBatch-1)*obj.PhasesPerBatch+1:nRepetitions(iBlock),1);
                        obj.Axis2Vector = gradientVectors((iBatch-1)*obj.PhasesPerBatch+1:nRepetitions(iBlock),2);
                        obj.Axis3Vector = gradientVectors((iBatch-1)*obj.PhasesPerBatch+1:nRepetitions(iBlock),3);
                        obj.lastBatch = 0;
                    end
                    obj.nPhasesBatch = length(obj.Axis1Vector);
                    
                    % NOW RUN ACTUAL ACQUISTION
                    obj.CreateSequence(0);
                    if(sum(obj.DELAYs<0.1)~=0)
                        warndlg('At least one delay is negative!','Delay Error')
                        return;
                    end
                    obj.Sequence2String;
                    fname = 'TempBatch.bat';
                    obj.WriteBatchFile(fname);
                    t1 = clock;
                    [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                    t2 = clock;
                    tRadial = tRadial + etime(t2,t1);
                    if status == 0 && ~isempty(result)
                        C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                        res = C{:};
                        obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                        obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                        obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                        obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                        obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                    else
                        warndlg('Radial batch aborted!','Warning')
                        return;
                    end
                    DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                    cDataBatch = complex(DataBatch(:,1),DataBatch(:,2))/bwCalNormVal;
                    %                     cDataBatchCal = cDataBatch;
                    %                     cDataBatchCal(1:nn) = cDataBatch(1:nn)-srd(1:nn);
                    Data2D = cat(1,Data2D,cDataBatch);
                    %                     Data2DCal = cat(1,Data2DCal,cDataBatchCal);
                    tB = clock;
                    tC = etime(tB,tA);
                    telapsed = toc(tstart);
                end
            end
            
            %% Single point
            % Create waiting bar
            obj.Stage = 2;
            obj.PhasesPerBatch = floor(min([2048/(3*obj.nSteps+5),16000/1]))-nMisses;
            nAcquiredPoints = size(gradientVectors2,1);
            nPhaseBatches = ceil(nAcquiredPoints/obj.PhasesPerBatch);
            
            % Start batches sweep
            fprintf('SINGLE POINT \n');
            obj.SetParameter('NPOINTS',1);
            repDelay = obj.GetParameter('REPETITIONDELAY');
            obj.SetParameter('REPETITIONDELAY',repDelay+acqTime(1));
            tCartesian = 0;
            rawData.inputs.rfFrequencySinglePoint = zeros(nPhaseBatches,1);
            nFreq = 0;
            for iBatch = 1:nPhaseBatches
                tA = clock;
                if(iBatch==1)
                    fprintf('Single point, batch %1.0f/%1.0f, estimating remaining time \n',iBatch,nPhaseBatches);
                else
                    tC = tC*(nPhaseBatches-iBatch+1);
                    fprintf('Single point, batch %1.0f/%1.0f, remaining time: %1.0f s \n',iBatch,nPhaseBatches,tC);
                end
                if(spoilerEnable)
                    %                 if ~mod(iBatch,10)
                    fprintf('Calibrating frequency... \n')
                    % Get central frequency
                    f0 = obj.GetFIDParameters;
                    obj.SetParameter('FREQUENCY',f0);
                    nFreq = nFreq+1;
                    rawData.aux.freqListCart(nFreq) = f0;
                    fprintf('Frequency = %1.4f MHz \n \n',f0*1d-6)
                    
                    %                     srd = CalibrateRingDown(obj);
                    %                 end
                    %                 else
                    %                     srd = zeros(50,1);
                end
                if telapsed>(3600/4) %Additional pause so that we are able to check in the VNA if the RF coil is losing the tuning
                    load gong.mat;
                    sound(y, Fs);
                    clear Fs y
                    h_warn = warndlg(char(strcat('Please check in VNA if the coil is tuned to: [MHz]', num2str( obj.GetParameter('FREQUENCY')/MyUnits.MHz))),'!!! Waiting to Continue !!!');
                    uiwait(h_warn);
                    tstart = tic;
                end
                % Get the appropiate axis
                if(iBatch<nPhaseBatches)
                    obj.Axis1Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,1);
                    obj.Axis2Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,2);
                    obj.Axis3Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,3);
                    obj.lastBatch = 0;
                else
                    obj.Axis1Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:end,1);
                    obj.Axis2Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:end,2);
                    obj.Axis3Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:end,3);
                    obj.lastBatch = 1;
                end
                obj.nPhasesBatch = length(obj.Axis1Vector);
                
                % NOW RUN ACTUAL ACQUISTION
                obj.CreateSequence(0);
                if(sum(obj.DELAYs<0.1)~=0)
                    warndlg('At least one delay is negative!','Delay Error')
                    return;
                end
                obj.Sequence2String;
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                t1 = clock;
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                t2 = clock;
                tCartesian = tCartesian + etime(t2,t1);
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Single Point batch aborted!','Warning')
                    return;
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2))/bwCalNormVal;
                %                 cDataBatchCal = cDataBatch-srd(1);
                Data2D = cat(1,Data2D,cDataBatch);
                %                 Data2DCal = cat(1,Data2DCal,cDataBatchCal);
                tB = clock;
                tC = etime(tB,tA);
                telapsed = toc(tstart);
            end
            obj.SetParameter('NPOINTS',nPPL(1));
            obj.SetParameter('REPETITIONDELAY',repDelay);
            disp(' ');
            kSpaceValues = [kSpaceValues,Data2D(:)];
            %             kSpaceValuesCal = kSpaceValues;
            %             kSpaceValuesCal(:,4) = Data2DCal(:);
            
            tBatch2 = clock;
            tBatch = etime(tBatch2,tBatch1);
            rawData.kSpace.sampled = kSpaceValues;
            %             rawData.kSpace.sampledCal = kSpaceValuesCal;
            
            %% Regridding
            %             kSpaceValues = kSpaceValuesCal;
            kCartesian = rawData.aux.kCartesian;
            if(nCir>1)
                valCartesian = griddata(kSpaceValues(:,1),kSpaceValues(:,2),kSpaceValues(:,3),kSpaceValues(:,4),kCartesian(:,1),kCartesian(:,2),kCartesian(:,3));
            else
                valCartesian = griddata(kSpaceValues(:,1),kSpaceValues(:,2),kSpaceValues(:,4),kCartesian(:,1),kCartesian(:,2));
            end
            valCartesian(isnan(valCartesian)) = 0;
            
            % Add phase according to fovDeviation
            DELX = rawData.inputs.fovDeviation(1);
            DELY = rawData.inputs.fovDeviation(2);
            DELZ = rawData.inputs.fovDeviation(3);
            phase = exp(-2*pi*1i*(DELX*kCartesian(:,1)+DELY*kCartesian(:,2)+DELZ*kCartesian(:,3)));
            
            % Save interpolated data
            interpolated = [kCartesian,valCartesian(:).*phase];
            rawData.kSpace.interpolated = interpolated;
            interpolated = reshape(interpolated(:,4),nPoints);
            
            imagen = abs(ifftshift(ifftn(interpolated)));
            rawData.kSpace.imagen = imagen;
            
            %% Save data
            elapsedTime = toc;
            rawData.aux.tRadial = tRadial;
            rawData.aux.tCartesian = tCartesian;
            rawData.aux.tBatch = tBatch;
            rawData.aux.elapsedTime = elapsedTime;
            
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
        
        
        %**************************************************************************
        %**************************************************************************
        %**************************************************************************
        
        
        function GetCartesianParameters(obj)
            global rawData;
            
            nPoints = ...
                [obj.GetParameter('NREADOUTS'),...
                obj.GetParameter('NPHASES'),...
                obj.GetParameter('NSLICES')];
            fov     = ...
                [obj.GetParameter('FOVX'),...
                obj.GetParameter('FOVY'),...
                obj.GetParameter('FOVZ')];
            deltaK  = 1./fov;
            kMax    = nPoints./(2*fov);
            DELX    = obj.GetParameter('DELTAX');
            DELY    = obj.GetParameter('DELTAY');
            DELZ    = obj.GetParameter('DELTAZ');
            
            rawData.inputs.NEX          = obj.GetParameter('NSCANS');
            rawData.inputs.nPoints      = nPoints;
            rawData.inputs.fov          = fov;
            rawData.inputs.fovDeviation = [DELX,DELY,DELZ];
            rawData.aux.deltaK          = deltaK;
            rawData.aux.kMax            = kMax;
        end
        function GetSequenceParameters(obj)
            
            global rawData;
            nPoints = rawData.inputs.nPoints;
            
            coilRiseTime = obj.GetParameter('COILRISETIME');
            blankingTime = obj.GetParameter('BLANKINGDELAY');
            trf = obj.GetParameter('PULSETIME');                            % RF pulse time
            td = obj.GetParameter('TRANSIENTTIME');                         % Dead time to wait until TX/RX switching
            tMax = obj.GetParameter('TMAX');                                % Max time for acquisition, it should be the shortest T2 to be acquired
            spoilerTime = obj.GetParameter('SPOILERTIME');
            underSampling = obj.GetParameter('UNDERSAMPLING');              % Undersampling for azimutal acquisition.
            underSampling = sqrt(underSampling);
            TR = obj.GetParameter('REPETITIONTIME');
            grad_delay = 300*1e-6; %s for Gradients to rise up + blk
            obj.repetitionDelay = TR-coilRiseTime-grad_delay-0.5*trf-tMax-spoilerTime;
            obj.SetParameter('REPETITIONDELAY',obj.repetitionDelay);
            nMisses = obj.GetParameter('JUMPINGFACTOR');
            nRadialBlocks = obj.GetParameter('NRADIAL');
            
            rawData.inputs.pulseTime = trf;
            rawData.inputs.deadTime = td;
            rawData.inputs.tMax = tMax;
            rawData.inputs.coilRiseTime = coilRiseTime;
            rawData.inputs.gradDelay = grad_delay;
            rawData.inputs.blankingTime = blankingTime;
            rawData.inputs.spoilerTime = spoilerTime;
            rawData.aux.delay = obj.repetitionDelay;
            rawData.inputs.undersampling = underSampling;
            rawData.inputs.jumpingFactor = nMisses;
            rawData.inputs.rfAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.rfFrequency = obj.GetParameter('FREQUENCY');
            rawData.inputs.repetitionTime = TR;
            rawData.inputs.axes = [obj.GetParameter('READOUT'),...
                obj.GetParameter('PHASE'),obj.GetParameter('SLICE')];
            rawData.inputs.nRadialBlocks = nRadialBlocks;
        end
        function SetSamplingParameters(obj)
            % gradientAmplitude = normalized amplitude to the MRIGUI input
            % nLPC = number of lines per circunferece in k-space
            % nCir = number of circunfereces in gradient space
            % nPPL = number of acquired points per line in k-space
            
            % Load rawData
            global rawData;
            gammabar        = 42.6d6;
            tMax            = rawData.inputs.tMax;
            kMax            = rawData.aux.kMax;
            nPoints         = rawData.inputs.nPoints;
            td              = rawData.inputs.deadTime;
            trf             = rawData.inputs.pulseTime;
            underSampling   = rawData.inputs.undersampling;
            nRadialBlocks   = rawData.inputs.nRadialBlocks;
            
            % Define parameters
            kLimits             = zeros(nRadialBlocks,3);
            gradientAmplitudes  = zeros(nRadialBlocks,3);
            nPPL                = zeros(nRadialBlocks,1);
            nLPC                = zeros(nRadialBlocks,1);
            nCir                = zeros(nRadialBlocks,1);
            bw                  = zeros(nRadialBlocks,1);
            
            % Set first block parameters
            bw(1)                       = max(nPoints)/(2*tMax);                                     % ideal acquisition bandwidth
            kLimits(1,:)                = kMax;                                             % k limits for each radial block
            gradientAmplitudes(1,:)     = kLimits(1,:)./(gammabar*tMax).*~(nPoints==1);     % gradient amplitudes for each radial block (T/m)
            nPPL(1)                     = ceil((sqrt(3)*tMax-td-0.5*trf)*bw(1))+1;          % number of points per line
            nLPC(1)                     = ceil(max(nPoints(1:2))*pi/underSampling);         % max number of lines per circumference
            nLPC(1)                     = max(nLPC(1)-mod(nLPC(1),2),1);
            nCir(1)                     = max(ceil(nPoints(3)*pi/2/underSampling)+1,1);     % number of circumferences
            for ii = 2:nRadialBlocks
                bw(ii,:)                    = bw(ii-1)*(td/tMax);
                kLimits(ii,:)               = kLimits(ii-1,:)*(td/tMax);
                gradientAmplitudes(ii,:)    = gradientAmplitudes(ii-1,:)*(td/tMax);
                nPPL(ii)                    = ceil((tMax-td-0.5*trf)*bw(ii));
                nLPC(ii)                    = ceil(nLPC(ii-1)*(td/tMax));
                nLPC(ii)                    = max(nLPC(ii)-mod(nLPC(ii),2),1);
                nCir(ii)                    = max(ceil(nCir(ii-1)*(td/tMax)),1);
            end
            if(nPoints(3)==1)
                nCir = ones(nRadialBlocks,1);
            end
            if(nPoints(2)==1)
                nLPC = ones(nRadialBlocks,1)+1;
            end
            
            % Get acquisition time
            acqTime = nPPL./bw;
            %             obj.AcquisitionTime = acq_time;
            
            % Save data in rawData
            rawData.aux.bandwidth               = bw;
            rawData.aux.kLimits                 = kLimits;
            rawData.aux.acquisitionTime         = acqTime;
            rawData.aux.numberOfPointsPerLine   = nPPL;
            rawData.aux.linesPerCircumference   = nLPC;
            rawData.aux.numberOfCircumferences  = nCir;
            rawData.aux.gradientAmplitudes      = gradientAmplitudes;
        end
        function GetRadialGradients(obj)
            % Gradient are defined by the sphere parametric equation. The
            % two parameteres are: theta in [0,pi] and phi [0,2pi].
            % Gradients are normalized to the amplifier amplitude.
            % First, I calculate the number of repetitions:
            
            % Load rawData
            global rawData;
            nCir                = rawData.aux.numberOfCircumferences;
            nLPC                = rawData.aux.linesPerCircumference;
            gradientAmplitudes  = rawData.aux.gradientAmplitudes;
            nRadialBlocks       = rawData.inputs.nRadialBlocks;
            
            % Get number of radial repetitions
            nRepetitions = zeros(nRadialBlocks,1);
            for ii = 1:nRadialBlocks
                % Get theta vector for current block
                if(nCir(ii)==1)
                    theta = pi/2;
                else
                    theta = linspace(0,pi,nCir(ii));
                end
                
                % Get number of radial repetitions for current block;
                for jj = 1:nCir(ii)
                    nRepetitions(ii) = nRepetitions(ii)+max(ceil(nLPC(ii)*sin(theta(jj))),1);
                end
            end
            
            % Calculate radial gradients
            normalizedGradientsRadial = zeros(sum(nRepetitions),3);
            n = 0;
            for ii = 1:nRadialBlocks
                % Get theta vector for current block
                if(nCir(ii)==1)
                    theta = pi/2;
                else
                    theta = linspace(0,pi,nCir(ii));
                end
                
                % Calculate the normalized gradients:
                for jj = 1:nCir(ii)
                    nLPCjj = max(ceil(nLPC(ii)*sin(theta(jj))),1);
                    deltaPhi = 2*pi/nLPCjj;
                    phi = linspace(0,2*pi-deltaPhi,nLPCjj);
                    for kk = 1:nLPCjj
                        n = n+1;
                        normalizedGradientsRadial(n,1) = sin(theta(jj))*cos(phi(kk));
                        normalizedGradientsRadial(n,2) = sin(theta(jj))*sin(phi(kk));
                        normalizedGradientsRadial(n,3) = cos(theta(jj));
                    end
                end
                
                % Set gradients to T/m
                gradientVectors1(1+sum(nRepetitions(1:ii-1)):sum(nRepetitions(1:ii)),:) = ...
                    normalizedGradientsRadial(1+sum(nRepetitions(1:ii-1)):sum(nRepetitions(1:ii)),:)*diag(gradientAmplitudes(ii));
            end
            % Save to rawData
            rawData.aux.numberOfRadialRepetitions = nRepetitions;
            rawData.aux.normalizedGradientsRadial = normalizedGradientsRadial;
            rawData.aux.gradientsRadialTeslasPerMeter = gradientVectors1;
        end
        function GetRadialKPoints(obj)
            
            %Load rawData
            global rawData;
            gammabar = 42.6d6;
            nPPL                        = rawData.aux.numberOfPointsPerLine;
            trf                         = rawData.inputs.pulseTime;
            td                          = rawData.inputs.deadTime;
            bw                          = rawData.aux.bandwidth;
            normalizedGradientsRadial   = rawData.aux.normalizedGradientsRadial;
            nRepetitions                = rawData.aux.numberOfRadialRepetitions;
            gradientAmplitudes          = rawData.aux.gradientAmplitudes;
            nRadialBlocks               = rawData.inputs.nRadialBlocks;
            
            % Calculate the radial k points for each block
            kRadial = [];
            for ii = 1:nRadialBlocks
                % Calculate k-points at t = 0.5*trf+td
                normalizedKRadial = zeros(nRepetitions(ii),3,nPPL(ii));
                normalizedKRadial(:,:,1) = (0.5*trf+td+0.5/bw(ii))*normalizedGradientsRadial(1+sum(nRepetitions(1:ii-1)):sum(nRepetitions(1:ii)),:);
                % Calculate all k-points
                for jj=2:nPPL(ii)
                    normalizedKRadial(:,:,jj) = normalizedKRadial(:,:,1)+(jj-1)*normalizedGradientsRadial(1+sum(nRepetitions(1:ii-1)):sum(nRepetitions(1:ii)),:)/bw(ii);
                end
                normalizedKRadial = reshape(permute(normalizedKRadial,[3,1,2]),[nRepetitions(ii)*nPPL(ii),3]);
                kRadial = cat(1,kRadial,normalizedKRadial*diag(gammabar*gradientAmplitudes(ii)));
            end
            % Save to rawData
            rawData.aux.radialKPoints = kRadial;
        end
        function GetCartesianKPoints(obj)
            
            % Load rawData
            global rawData;
            gammabar = 42.6d6;
            trf = rawData.inputs.pulseTime;
            td = rawData.inputs.deadTime;
            nRadialBlocks = rawData.inputs.nRadialBlocks;
            bw = rawData.aux.bandwidth(nRadialBlocks);
            nPoints = rawData.inputs.nPoints;
            kMax = rawData.aux.kMax;
            gradientAmplitudes = rawData.aux.gradientAmplitudes(nRadialBlocks,:);
            % Get minimun time
            tMin = 0.5*trf+td+0.5/bw;
            
            % Get the full cartesian points
            kx = linspace(-kMax(1)*~isequal(nPoints(1),1),kMax(1)*~isequal(nPoints(1),1),nPoints(1));
            ky = linspace(-kMax(2)*~isequal(nPoints(2),1),kMax(2)*~isequal(nPoints(2),1),nPoints(2));
            kz = linspace(-kMax(3)*~isequal(nPoints(3),1),kMax(3)*~isequal(nPoints(3),1),nPoints(3));
            [kx,ky,kz] = meshgrid(kx,ky,kz);
            kx = permute(kx,[2,1,3]);
            ky = permute(ky,[2,1,3]);
            kz = permute(kz,[2,1,3]);
            kCartesian(:,1) = kx(:);
            kCartesian(:,2) = ky(:);
            kCartesian(:,3) = kz(:);
            
            % Get the points that should be acquired in a time shorter than
            % tMin
            normalizedKCartesian = kCartesian/diag(gammabar*(gradientAmplitudes+double(gradientAmplitudes==0)));
            normalizedKCartesian(:,4) = sqrt(sum(normalizedKCartesian.^2,2));
            normalizedKSinglePoint = normalizedKCartesian(normalizedKCartesian(:,4)<tMin,1:3);
            kSinglePoint = normalizedKSinglePoint*diag(gammabar*gradientAmplitudes);
            
            % Save to rawData
            rawData.aux.numberOfCartesianRepetitions = size(kSinglePoint,1);
            rawData.aux.tMin = tMin;
            rawData.aux.cartesianKPoints = kSinglePoint;
            rawData.aux.kCartesian = kCartesian;
        end
        
        
        %******************************************************************
        %******************************************************************
        %******************************************************************
        
        
        function CreateSequenceRingDownCalibration(obj)
            %% Help:
            % This function sendo two RF excitations together with two
            % different gradients.
            % Here I try to calibrate only up to 100 us. If you want to
            % calibrate longer, you need to modify properly.
            
            %% Code goes here:
            
            % Set sequence parameters
            gAmp = obj.gRingDownCal;
            acqTime = obj.AcquisitionTime*1e6;
            pulseTime = obj.GetParameter('PULSETIME')*1e6;
            deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
            repDelay = obj.GetParameter('REPETITIONDELAY')*1e6;
            blk = 5;
            
            % Select axis for shimming
            [shimS,shimP,shimR] = SelectAxes(obj);
            
            % Initialize all vectors to zero with the appropriate size
            obj.nline_reads = 2;
            obj.nInstructions = 70;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            % Axis
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            
            % Create pulse
            iIns = 1;
            for ii = 1:2
                % Turn on gradients
                for jj=1:10
                    iIns = obj.PulseGradient(iIns,(gAmp(ii)+shimR)/10*jj,RR,0.1);
                    iIns = obj.PulseGradient(iIns,(gAmp(ii)+shimP)/10*jj,PP,0.1);
                    iIns = obj.PulseGradient(iIns,(gAmp(ii)+shimS)/10*jj,SS,150/10-0.2);
                end
                obj.DELAYs(iIns-1) = 150/10-0.2+300-blk;
                % RF pulse
                iIns = obj.CreatePulse(iIns,0,blk,pulseTime,deadTime);
                % Acquisition
                iIns = obj.Acquisition(iIns,acqTime);
                % Delay
                iIns = obj.RepetitionDelay(iIns,1,repDelay);
            end
        end
        function srd = CalibrateRingDown(obj)
            %% Help:
            % This method get an estimation of the ring-down signal for the
            % provided rf pulse, bw, dead time...
            
            %% Code goes here:
            obj.gRingDownCal = [0.01 0.005];
            
            % Inputs
            trf = obj.GetParameter('PULSETIME')*1e6;                        % Pulse time
            deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;               % Dead time
            nPPL = obj.GetParameter('NPOINTS');                             % Number of points
            nPPC = 50;
            obj.SetParameter('NPOINTS',nPPC);
            bw = obj.GetParameter('SPECTRALWIDTH');                         % Acquisition time
            acqTime = nPPC/bw;                                              % Acquisition time
            obj.AcquisitionTime = acqTime;
            acqTime = acqTime*1e6;
            NEX = obj.GetParameter('NSCANS');                               % Set nscans
            repDelay = obj.GetParameter('REPETITIONDELAY');
            obj.SetParameter('NSCANS',ceil(1/repDelay));
            
            % Load bandwidth/amplitude calibration for 1V
            bwCal = load('rawData-BW-Calibration.mat');
            bwCalVal = bwCal.rawData.outputs.fidMean;
            bwCalVec = logspace(log10(bwCal.rawData.inputs.bwMin),...
                log10(bwCal.rawData.inputs.bwMax),...
                bwCal.rawData.inputs.steps);
            
            % Get value to normalize fid to Volts
            [~,bwCalNormPos] = min(abs(bwCalVec-bw));
            bwCalNormVal = bwCalVal(bwCalNormPos);
            
            % Set time vector
            dt = 1/bw*1e6;
            tmin = 0.5*trf+deadTime+0.5*dt;
            tmax = tmin+acqTime;
            tVector = linspace(tmin,tmax,nPPC)';
            sVector = zeros(nPPC,2);
            
            % Run
            obj.CreateSequenceRingDownCalibration;
            obj.Sequence2String;
            fname = 'TempBatch.bat';
            obj.WriteBatchFile(fname);
            [status,result] = system(fullfile(obj.path_file,fname));
            
            % Set back the modified parameters
            obj.SetParameter('NPOINTS',nPPL);
            obj.SetParameter('NSCANS',NEX);
            
            % Check errors
            if status == 0 && ~isempty(result)
                C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                res = C{:};
                obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
            else
                warndlg('Batch aborted!','Warning')
                return;
            end
            dataBatch = load(sprintf('%s.txt',obj.OutputFilename));
            cDataBatch = complex(dataBatch(:,1),dataBatch(:,2));
            sVector(:,1) = cDataBatch(1:nPPC);
            sVector(:,2) = cDataBatch(nPPC+1:end);
            sVector = sVector/bwCalNormVal;
            
            % Get ring-down signal
            tq = 2*tVector;
            sq = pchip(tVector,sVector(:,2),tq);
            srd = sVector(:,1)-sq;
            
            % Plot
            figure(3)
            s = sVector(:,2);
            nn = 4;
            s(1:nn) = s(1:nn)-srd(1:nn);
            subplot(1,2,1)
            plot(tVector(1:nn),real(srd(1:nn)))
            title('Ring-down signal')
            
            subplot(1,2,2)
            plot(tVector,real(sVector(:,2)),tVector,real(s))
            title('Original vs Calibrated')
        end
        
        
    end
    
end

