classdef RadialSPRITE_PP_Slice < MRI_BlankSequence3
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    %  Elena Diaz Caballero
    
    properties
        MaxPoints = 4096;
        MaxInstructions = floor((1000)/14);
        Ntot;
        Axis1Vector;
        Axis2Vector;
        Axis3Vector;
        Axis1IND;
        Axis2IND;
        Axis3IND;
        StartPhase = 0;
        EndPhase = 0;
        SliceFreqOffset;
    end
    methods (Access=private)
        function CreateParameters(obj)
            InputParameters = {...
                'NPAVGS','NREADOUTS','NPHASES','TP','REPETITIONDELAY',...
                'STARTREADOUT','ENDREADOUT','STARTPHASE','ENDPHASE',...
                'SLICEGRADIENT','STARTSLICEFREQ','ENDSLICEFREQ',...
                'PREPULSETIME','PREPULSEDELAY','PREPULSETR',...
                };
            
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {...
                '#Points to Average','Number of Readouts','Number of Phases','Phase encode time','Repetition Delay',...
                'Start Readout','End Readout','Start Phase','End Phase',...
                'Slice Gradient','Start Slice Freq','End Slice Freq',...
                'Prepulse Duration','Prepulse Delay','Prepulse TR',...
                })];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                })];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {...
                1,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                })];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                })];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {...
                '','','','us','ms',...
                '','','','',...
                '','kHz','kHz',...
                'ms','ms','s',...
                })];
            
            for k=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{k},InputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = RadialSPRITE_PP_Slice(program)
            obj = obj@MRI_BlankSequence3();
            obj.SequenceName = 'Radial SPRITE PP Slice';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence3(obj);
            obj.SetParameter('NPAVGS',1);
            obj.SetParameter('NREADOUTS',1);
            obj.SetParameter('NPHASES',1);
            obj.SetParameter('TP',50*MyUnits.us);
            obj.SetParameter('REPETITIONDELAY',50*MyUnits.ms);
            obj.SetParameter('STARTREADOUT',-0.1);
            obj.SetParameter('ENDREADOUT',0.1);
            obj.SetParameter('STARTPHASE',-0.1);
            obj.SetParameter('ENDPHASE',0.1);
            obj.SetParameter('SLICEGRADIENT',0.1);
            obj.SetParameter('STARTSLICEFREQ',-10*MyUnits.kHz);
            obj.SetParameter('ENDSLICEFREQ',10*MyUnits.kHz);
            
            obj.SetParameter('PREPULSETIME',10*MyUnits.ms);
            obj.SetParameter('PREPULSEDELAY',10*MyUnits.ms);
            obj.SetParameter('PREPULSETR',1*MyUnits.s);
            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
        end
        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence,
            %         "1", central frequency adjustment sequence,
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            
            %blanking bits
            bbit = 1;            
            bbit2 = 2;

            % then grab the appropriate switches for IF the
            % readout/slice/phases were selected
            [~,~,readout,slice,phase,~,~] = obj.ConfigureGradients(mode);
            
            
            % Calculate the number of line reads based on the length of the Axis vector
            if mode == 0
                obj.nline_reads = numel( obj.Axis1Vector );
            elseif mode == 1
                obj.nline_reads = 1;
            end
            % Initialize all vectors to zero with the appropriate size
            if obj.GetParameter('PREPULSETIME') > 0
                obj.nInstructions=14*obj.nline_reads+5;
            else
                obj.nInstructions=14*obj.nline_reads+3;
            end
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            % set the acquisition time
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
            
            %%% Gradient amplitude calculation
               % initialize gradient vectors
            [Axis1Vector, Axis2Vector, Axis3VectorDP, Axis3VectorRP] = deal( zeros( 1, obj.nline_reads) );
               % set the bounds to the vectors
            MinVector = Axis1Vector - 1;
            MaxVector = Axis1Vector + 1;
            
            % readout switch set to Axis1
            if( readout == 0 ) || (mode == 1)
                Axis1Vector = Axis1Vector + shimR;
            else
                Axis1Vector = max( min(MaxVector,obj.Axis1Vector + shimR), MinVector );
            end
            
            % phase switch set to Axis2
            if( phase == 0 ) || (mode == 1)
                Axis2Vector = Axis2Vector + shimP;
            else
                Axis2Vector = max( min(MaxVector,obj.Axis2Vector + shimP), MinVector );
            end
            
            % slice switch set to Axis3
            if( slice == 0 ) || (mode == 1)
                Axis3VectorDP = Axis3VectorDP + shimS;
                Axis3VectorRP = Axis3VectorRP + shimS;
            else
                Axis3VectorDP = max( min(MaxVector,obj.Axis3Vector + shimS), MinVector );
                Axis3VectorRP = max( min(MaxVector,-obj.Axis3Vector + shimS), MinVector );
            end
            rephasing_slice_time = 0.48*obj.GetParameter('PULSETIME');
            
            %%% BEGIN THE PULSE PROGRAMMING
            iIns = 1;
            %------ Blanking Delay and Shimming in all gradients (3 instructions)
            nIns = 3; % set the number of instruction in this block
            vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                obj.AMPs(vIns)     = [shimP shimR shimS];
                obj.DACs(vIns)     = [PP RR SS];
                obj.WRITEs(vIns)   = [1 1 1];
                obj.UPDATEs(vIns)  = [1 1 1];
                obj.PhResets(vIns) = [0 0 0];
                obj.ENVELOPEs(vIns)= [7 7 7]; % (7=no shape)
                obj.FLAGs(vIns)    = [0 0 0];
                obj.DELAYs(vIns)   = [0.1 0.1 obj.GetParameter('BLANKINGDELAY')*1e6]; %time in us
            iIns = iIns + nIns; % update the number of instructions in this block
                        
            % PREPULSE IF THE PREPULSETIME IS GREATER THAN 0
            if obj.GetParameter('PREPULSETIME') > 0
                nIns  = 2; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [shimR shimR];
                    obj.DACs(vIns)     = [RR RR];
                    obj.WRITEs(vIns)   = [1 1];
                    obj.UPDATEs(vIns)  = [1 1];
                    obj.PhResets(vIns) = [0 0];
                    obj.ENVELOPEs(vIns)= [7 7]; % (7=no shape)
                    obj.FLAGs(vIns)    = [bbit2 0];
                    obj.DELAYs(vIns)   = [obj.GetParameter('PREPULSETIME')*1e6 obj.GetParameter('PREPULSEDELAY')*1e6 ]; %time in us
                iIns = iIns + nIns; % update the number of instructions
            end
            
            % --- NOW START THE RF TRAIN
            for ipulse = 1:obj.nline_reads
                % BLANKING DELAY
                nIns  = 1; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.WRITEs(vIns)   = 0;
                    obj.UPDATEs(vIns)  = 0;
                    obj.PhResets(vIns) = 0;
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.FLAGs(vIns)    = bbit;
                    obj.DELAYs(vIns)   = obj.GetParameter('BLANKINGDELAY')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
                % Slice Pulse
                nIns  = 1; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = Axis3VectorDP(ipulse);
                    obj.DACs(vIns)     = SS;
                    obj.WRITEs(vIns)   = 1;
                    obj.UPDATEs(vIns)  = 1;
                    obj.PhResets(vIns) = 1;
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.FLAGs(vIns)    = bbit;
                    obj.DELAYs(vIns)   = obj.GetParameter('COILRISETIME')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
                %------ RF pulse (1 instruction)
                nIns = 1; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.TXs(vIns)      = 1;
                    if slice ~= 0
                        obj.FREQs(vIns)    = 1;
                    else
                        obj.FREQs(vIns)    = 0;
                    end
                    
                    if (obj.RF_Shape) % Shaped RF pulse
                        obj.ENVELOPEs(vIns)= 0;
                    else
                        obj.ENVELOPEs(vIns)= 7;
                    end
                    obj.FLAGs(vIns)    = bbit;
                    obj.OPCODEs(vIns)  = 0;
                    obj.DELAYs(vIns)   = obj.GetParameter('PULSETIME')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %------ Set All Gradients to the appropriate level, include coil rise time (3 instructions)
                nIns = 3; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [Axis1Vector(ipulse) Axis2Vector(ipulse) Axis3VectorRP(ipulse)];
                    obj.DACs(vIns)     = [RR PP SS];
                    obj.WRITEs(vIns)   = [1 1 1];
                    obj.UPDATEs(vIns)  = [1 1 1];
                    obj.PhResets(vIns) = [0 0 0];
                    obj.ENVELOPEs(vIns)= [7 7 7]; % (7=no shape)
                    obj.FLAGs(vIns)    = [0 0 0];
                    obj.DELAYs(vIns)   = [0.1 0.1 (obj.GetParameter('COILRISETIME')+rephasing_slice_time)*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %------ Set All Gradients to the appropriate level, include coil rise time (3 instructions)
                nIns = 3; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [Axis1Vector(ipulse) Axis2Vector(ipulse) shimS ];
                    obj.DACs(vIns)     = [RR PP SS];
                    obj.WRITEs(vIns)   = [1 1 1];
                    obj.UPDATEs(vIns)  = [1 1 1];
                    obj.PhResets(vIns) = [0 0 0];
                    obj.ENVELOPEs(vIns)= [7 7 7]; % (7=no shape)
                    obj.FLAGs(vIns)    = [0 0 0];
                    obj.DELAYs(vIns)   = [0.1 0.1 max((obj.GetParameter('TP')-rephasing_slice_time)*1e6,0.1)]; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %------ Blanking Delay and Shimming in all gradients (3 instructions)
                nIns = 3; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [shimP shimR shimS];
                    obj.DACs(vIns)     = [PP RR SS];
%                     obj.WRITEs(vIns)   = [1 1 1];
%                     obj.UPDATEs(vIns)  = [1 1 1];
                    obj.PhResets(vIns) = [0 0 0];
                    obj.ENVELOPEs(vIns)= [7 7 7]; % (7=no shape)
                    obj.FLAGs(vIns)    = [0 0 0];
                    obj.DELAYs(vIns)   = [0.1 0.1 obj.GetParameter('COILRISETIME')*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block

                %----- Data Acquisition (1 instruction)
                nIns = 1; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.WRITEs(vIns)   = 0;
                    obj.UPDATEs(vIns)  = 0;
                    obj.RXs(vIns)      = 1;
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.DELAYs(vIns)   = acq_time*1e6;
                iIns = iIns + nIns; % update the number of instructions in this block   
                
                %----- TR (1 instructions)
                nIns = 1; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = 0.0;
                    obj.DACs(vIns)     = 3;
                    obj.WRITEs(vIns)   = 1;
                    obj.UPDATEs(vIns)  = 1;
                    obj.CLEARs(vIns)   = 1;
                    obj.ENVELOPEs(vIns)= 7;
                    if (ipulse == obj.nline_reads) && (obj.GetParameter('PREPULSETIME') > 0)
                        obj.DELAYs(vIns)   = (obj.GetParameter('PREPULSETR')+obj.GetParameter('REPETITIONDELAY'))*1e6; %time in us
                    else
                        obj.DELAYs(vIns)   = obj.GetParameter('REPETITIONDELAY')*1e6; %time in us
                    end
                iIns = iIns + nIns; % update the number of instructions in this block
                
            end % end the axis instruction loop
            
        end % end create gradient echo pulse sequence
        function [status,result] = Run(obj,iAcq)
            global MRIDATA
            % Now calculate the batches that need to be run    
            nPoints = obj.GetParameter('NPOINTS');
            InsPerBatch = min( floor(obj.MaxPoints / nPoints), obj.MaxInstructions );
            
            %%% CALCULATE THE AXISVECTORS
            % Begin and end axes
            StartReadout = obj.GetParameter('STARTREADOUT');
            EndReadout = obj.GetParameter('ENDREADOUT');
            StartPhase = obj.GetParameter('STARTPHASE');
            EndPhase = obj.GetParameter('ENDPHASE');
            StartSliceFreq = obj.GetParameter('STARTSLICEFREQ');
            EndSliceFreq = obj.GetParameter('ENDSLICEFREQ');
            
            % number for each axes
            nReadouts = obj.GetParameter('NREADOUTS');
            nPhases = obj.GetParameter('NPHASES');
            nSlices = obj.GetParameter('NSLICES');
            
            % do switch for if the readouts and gradients are enabled
            if ~obj.readout_en
                nReadouts = 1; StartReadout = 0; EndReadout = 0;
            end
            if ~obj.phase_en
                nPhases = 1; StartPhase = 0; EndPhase = 0;
            end
            if ~obj.slice_en
                nSlices = 1; StartSliceFreq = 0; EndSliceFreq = 0;
            end
            
            % Create the Axis Vectors
                % FIRST create the axes for the measurements - regular
                % cartesian grid
                obj.Ntot = nReadouts*nPhases;
                Readout_axis = linspace(StartReadout, EndReadout, nReadouts);
                Phase_axis = linspace(StartPhase, EndPhase, nPhases);
                Slice_axis = linspace(StartSliceFreq, EndSliceFreq, nSlices);
                
                % now create the matrixes for measurement
                [AX1, AX2] = meshgrid( Readout_axis, Phase_axis );
                
                [AX11,AX22,AX33] = meshgrid( Readout_axis, Phase_axis, Slice_axis );
                
                % convert these to vector and then sort them by distance from the center
                AxesVectors = [ AX1(:), AX2(:)];
                [~, VIND] = sort( sqrt( AxesVectors(:,1).^2 + AxesVectors(:,2).^2  ), 'ascend' );
                AxesVectors = AxesVectors( VIND, : );
                
                % Calculate how many batches to have
                nBatches = ceil( obj.Ntot / InsPerBatch );
               
                
                if isfield(MRIDATA,'Data4D')
                    if obj.averaging
                        if iAcq == 1
                            MRIDATA.Data4D = [];
                        end
                    else
                        MRIDATA.Data4D = [];
                    end
                else
                    MRIDATA.Data4D = [];
                end

                if isfield(MRIDATA,'Data4DCF')
                    if obj.averaging
                        if iAcq == 1
                            MRIDATA.Data4DCF = [];
                        end
                    else
                        MRIDATA.Data4DCF = [];
                    end
                else
                    MRIDATA.Data4DCF = [];
                end
                
            
            
            % initialize storage vectors
            Data3D = [];%zeros(nBatches,obj.Ntot);
            Data4D = [];
            
%                 INDstore = zeros(nReadouts*nPhases*nSlices,3);
            % start the counter
            cn = nBatches;
            count = 0;
            if nBatches>1
                h_waitbar = waitbar(count/cn,'Please wait (por favor espere)...');
            end
            
            obj.Axis1IND = [];
            obj.Axis2IND = [];
            obj.Axis3IND = [];
            
            % run through the slices
            for iSlice = 1:nSlices
                % NOW run through the batches
                
                obj.SliceFreqOffset = Slice_axis(iSlice);
                obj.freq_offset = obj.SliceFreqOffset;
                
                for iBatch = 1:nBatches

                    % calculate the center frequency

                    if nBatches >1
                        % set the axis vectors
                        obj.Axis1Vector = 0;
                        obj.Axis2Vector = 0;
                        obj.Axis3Vector = 0;
                        % set the number of points
                        obj.SetParameter('NPOINTS',300);
                        ptime = obj.GetParameter('PREPULSETIME');
                        navg = obj.GetParameter('NSCANS');
                        obj.SetParameter('PREPULSETIME',0);
                        obj.SetParameter('NSCANS',3);
                        obj.freq_offset = 0;
                        obj.CreateSequence(1); % Create the gradient echo sequence for CF
                        obj.Sequence2String;
                        fname_CF = 'TempBatch_CF.bat';
                        obj.WriteBatchFile(fname_CF);

                        obj.CenterFrequency(fname_CF);

                        % revert the number of points back
                        obj.SetParameter('NPOINTS',nPoints);
                        obj.SetParameter('NSCANS',navg);
                        obj.SetParameter('PREPULSETIME',ptime);
                        obj.freq_offset = obj.SliceFreqOffset;

                    end


                    % Calculate the appropriate axes
                    obj.Axis1Vector = downsample( AxesVectors(:,1), nBatches, iBatch - 1)';
                    obj.Axis2Vector = downsample( AxesVectors(:,2), nBatches, iBatch - 1)';
                    obj.Axis3Vector = zeros(size(obj.Axis2Vector)) + obj.GetParameter('SLICEGRADIENT');
                    obj.Axis1IND = [ obj.Axis1IND, obj.Axis1Vector(:)'];
                    obj.Axis2IND = [ obj.Axis2IND, obj.Axis2Vector(:)'];
                    obj.Axis3IND = [ obj.Axis3IND, zeros(size(obj.Axis2Vector(:)'))+ Slice_axis(iSlice)];

                    % NOW RUN ACTUAL ACQUISTION

                    obj.CreateSequence(0); % Create the normal gradient echo sequence


                    obj.Sequence2String;


                    fname = 'TempBatch.bat';

                    obj.WriteBatchFile(fname);

                    [status,result] = system(fullfile(obj.path_file,fname));

                    if status == 0 && ~isempty(result)
                        C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                        res = C{:};
                        obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                        obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                        obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                        obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                        obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                    end

                    % Load in data
                    DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                    cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));

                    % Reorder the data
                    d = reshape(cDataBatch,nPoints,numel(obj.Axis1Vector));

                    % update the counter
                    if nBatches>1
                        count = count+1;
                        waitbar(count/cn,h_waitbar,sprintf('Please wait (por favor espere)..... %5.2f %%',100*count/cn));
                    end
                    % switch the FID if needed
                    if obj.readout_en
                        % mean the data
                        DataScan = mean(d(1:obj.GetParameter('NPAVGS'),:),1);
                        % place the data in the appropriate positions of the 3D matrix
                        Data3D = [Data3D, DataScan(:)'];
                        Data4D = [ Data4D , d ];
                    else
                        Data3D = d;
                    end
                    
                end
                
            end
            
            
                    
            % save 4D data
            MRIDATA.Data4Dunfit.Fid  = Data4D;
            MRIDATA.Data4Dunfit.Axis = cat(3,obj.Axis1IND , obj.Axis2IND, obj.Axis3IND);
            MRIDATA.Data4Dunfit.Time = obj.GetParameter('TRANSIENTTIME')+obj.GetParameter('TP')+...
                                       linspace(0,obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH'),obj.GetParameter('NPOINTS'));
            
            %reorganize the data
            Data3D = Data3D(:);
            
            % calculate the number of dimensions
            ndim = sum( [ obj.readout_en, obj.phase_en, obj.slice_en ] );
            if ndim == 3

                warning off;
                Data3D_Fun = scatteredInterpolant(obj.Axis1IND(:),obj.Axis2IND(:),obj.Axis3IND(:),...
                                                  Data3D,'natural','none');
                warning on;
                Data3D_use = Data3D_Fun(AX11,AX22,AX33);
                Data3D_use(isnan(Data3D_use)) = 0;
            elseif ndim == 2
                warning off;
                Data3D_Fun = scatteredInterpolant(obj.Axis1IND(:),obj.Axis2IND(:),...
                                                  Data3D,'natural','none');
                warning on;
                Data3D_use = Data3D_Fun(AX1,AX2);
                Data3D_use(isnan(Data3D_use)) = 0;
                
            elseif ndim == 1
                if nSlices > 1
                    axis_use = obj.Axis3IND(:);
                    axis_find = AX3;
                elseif nPhases > 1
                    axis_use = obj.Axis2IND(:);
                    axis_find = AX2;
                else
                    axis_use = obj.Axis1IND(:);
                    axis_find = AX1;
                end
                Data3D_use = interp1(axis_use, Data3D, axis_find, 'spline',0);
            else
                Data3D_use = Data3D;
            end
            
            % check if the data is new or not
            if numel(obj.cData) ~= nReadouts*nPhases*nSlices*obj.NAverages
                obj.cData = zeros(obj.NAverages,nReadouts*nPhases*nSlices);
            end
            % check if the averaging is turned on
            if obj.averaging
                Nav = obj.NAverages;
            else
                Nav = 1;
            end
            index = mod(iAcq-1,Nav)+1;

            % auto phasing
            if obj.autoPhasing
                pt = max(Data3D_use(:));
                Data3D_use = Data3D_use.*exp(complex(0,-angle(pt)));
            end
            obj.cData(index,1:numel(Data3D_use(:))) = Data3D_use(:);
            obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
%             obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
            
            
            obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
            acq_time = obj.GetParameter('ACQUISITIONTIME');
            obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*acq_time/nPhases;
            obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time/nPhases;
        
            if nBatches>1
                delete(h_waitbar);
            end
            
%             save('TempData3D.mat','Data3D','INDstore','obj');
        end
 
    
    end
end

