classdef STEADYSTATE < MRI_BlankSequence_JM 
    properties
        maxRepetitionsPerBatch;
        nRepetitionsInBatch;
        RepetitionTime;
        AcquisitionTime;
    end
    
    methods (Access=private)
        function CreateParameters(obj)
            obj.CreateOneParameter('NREPETITIONS','n Repetitions','',30);
            CreateOneParameter(obj,'REPETITIONTIME','Repetition time','ms',100*MyUnits.ms)
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT','COILRISETIME','NSLICES','NPHASES','PHASE','SLICE','READOUT'}; 
        end
    end
    

    methods
        function obj = STEADYSTATE(program)
            obj = obj@MRI_BlankSequence_JM();
            obj.SequenceName = 'STEADYSTATE';
            obj.ProgramName = 'MRI_BlankSequenceJM.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence_JM(obj);
        end
        
        function UpdateTETR(obj)
            obj.TE = obj.GetParameter('TRANSIENTTIME') + (0.5 * obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH'));
            obj.TR = obj.GetParameter('REPETITIONTIME');

            obj.TTotal = obj.TR ;
        end
        
        function CreateSequence(obj,mode)
            % Calculate the number of line reads
            nRepetitions = obj.GetParameter('NREPETITIONS');
            obj.nline_reads = nRepetitions;
        
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions = 6*(nRepetitions);
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            % Timing
            acqTime = (obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH'))*1e6;
            trf = obj.GetParameter('PULSETIME')*1e6;
            td =  obj.GetParameter('TRANSIENTTIME')*1e6;
            blk = obj.GetParameter('BLANKINGDELAY')*1e6;  
            tr = obj.GetParameter('REPETITIONTIME')*1e6;
            repetDelay = tr-acqTime-td-trf;
            dac = 1;
            spoilertime = repetDelay-400; 
            iIns = 1;
            
            %Spoiler gradient
            gradamp = 0.01;
            amp = linspace(-gradamp, gradamp, nRepetitions);
            
            for phasei = 1:nRepetitions  
                %% 90� RF pulse (3 instruction)
                iIns = CreatePulse(obj,iIns,7,0,blk,trf,td);
                
                %% Acquisition (1 instruction)
                iIns = Acquisition(obj,iIns,acqTime);
                
                %% Spoiler gradient (1 instruction)
                iIns = PulseGradient(obj,iIns,0*amp(phasei),dac,spoilertime);

                %% Repetition Delay (1 instruction)
                iIns = RepetitionDelay(obj,iIns,1,400);
                
            end 
        end
        
        function [status,result] = Run(obj,iAcq)
            clc
            
            % Check number of acquired points
            if(obj.GetParameter('NREPETITIONS')*obj.GetParameter('NPOINTS')>16000)
                warndlg('Total number of points should be smaller than 16000','Warning')
                return;
            end
            
            global rawData;
            rawData.inputs.sequence = obj.SequenceName;
            rawData.inputs.NEX = obj.GetParameter('NSCANS');
            rawData.inputs.nPoints = obj.GetParameter('NPOINTS');
            rawData.inputs.nRepetitions = obj.GetParameter('NREPETITIONS');
            rawData.inputs.repetitionTime = obj.GetParameter('REPETITIONTIME');

            %% Auxiliar
            Data2D = [];
            %% Execute the sequence
            t = 0;
            disp('PROCESSING FID');
            
            acqTime = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
            obj.AcquisitionTime = acqTime;
            rawData.aux.acquisitionTime = acqTime;
            nPPL = obj.GetParameter('NPOINTS');
            nRep = obj.GetParameter('NREPETITIONS');
            obj.RepetitionTime = obj.GetParameter('REPETITIONTIME');
            
            % NOW RUN ACTUAL ACQUISTION
            [DataBatch,~,status,result] = RunData(obj,0,t);
            DataBatch = reshape(DataBatch,nPPL,nRep);
            rawData.aux.data = DataBatch;
        
        end
    end
    
end



