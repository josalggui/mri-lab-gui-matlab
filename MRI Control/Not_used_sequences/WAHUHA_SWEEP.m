classdef WAHUHA_SWEEP < MRI_BlankSequence_JM    
    properties
        PhasesPerBatch = 20;
        nPhasesBatch;
        Axis1Vector;
        Axis2Vector;
        Axis3Vector;
        StartSlice;
        lastBatch;
        EndSlice;
        StartPhase;
        EndPhase;
        RepetitionTime;
        slice_ampl;
        AcquisitionTime;
    end
    methods (Access=private)
        function CreateParameters(obj)

            % HELP %
            % I include here new inputs to discriminate between
            % acquistition and regruidding.
            % -> NPHASES, NREADOUTS and NSLICES are for regridding
            % -> NPOINTS, NCIRCUNFERENCES, NLINESPERCIRCUNFERENCES are for
            %    aqcuisition.

            InputParameters = {...
                'REPETITIONDELAY',...
                'NREADOUTS','NPHASES','TAUTIME',...
                'MINIMUMFREQUENCY','MAXIMUMFREQUENCY','NFREQUENCIES',...
                'MINIMUMAMPLITUDE','MAXIMUMAMPLITUDE','NAMPLITUDES',...
                'MINIMUMPULSETIME','MAXIMUMPULSETIME','NPULSETIMES',...
                'MINIMUMTAU','MAXIMUMTAU','NTAUS',...
                };
            
            obj.InputParHidden = {'READOUT','PHASE','SLICE','TRANSIENTTIME',...
                'COILRISETIME','SHIMSLICE','SHIMPHASE','SHIMREADOUT',...
                'BLANKINGDELAY','NSLICES','NPHASES','FREQUENCY','NPOINTS',...
                'RFAMPLITUDE','PULSETIME','TAUTIME'};
            
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {...
                'Repetition Delay',...
                'Echoes','','',...
                'Freq min','Freq max','n Freqs',...
                'Amp min','Amp max','n Amps',...
                'Trf min','Trf max','n Trf',...
                'Tau min','Tau max','n taus',...
                })];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
                {...
                NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                })];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {...
                NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                })];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {...
                NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,...
                })];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {...
                'ms',...
                '','','',...
                'MHz','MHz','',...
                '','','',...
                'us','us','',...
                'us','us','',...
                })];
            
            for k=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{k},InputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
        end
        
        function MoveParameter(obj,x,y)
            xx = obj.InputParameters(x);
            output = obj.InputParameters;
            output(x) = [];
            output(y+1:end+1) = output(y:end);
            output(y) = xx;
            obj.InputParameters = output;
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = WAHUHA_SWEEP(program)
            obj = obj@MRI_BlankSequence_JM();
            obj.SequenceName = 'WAHUHA SWEEP';
            obj.ProgramName = 'MRI_BlankSequenceJM.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence_JM(obj);
            obj.SetParameter('REPETITIONDELAY',50*MyUnits.ms);
            obj.SetParameter('TRANSIENTTIME',20*MyUnits.us);
            obj.SetParameter('BLANKINGDELAY',15*MyUnits.us);
            obj.SetParameter('NPOINTS',1);
            obj.SetParameter('NREADOUTS',10);
            obj.SetParameter('NPHASES',1);
            obj.SetParameter('RFAMPLITUDE',1);
            obj.SetParameter('PULSETIME',4*MyUnits.us);
            
            obj.SetParameter('MINIMUMFREQUENCY',14.2*MyUnits.MHz);
            obj.SetParameter('MAXIMUMFREQUENCY',14.3*MyUnits.MHz);
            obj.SetParameter('NFREQUENCIES',1);
            
            obj.SetParameter('MINIMUMAMPLITUDE',0);
            obj.SetParameter('MAXIMUMAMPLITUDE',1);
            obj.SetParameter('NAMPLITUDES',1);
            
            obj.SetParameter('MINIMUMPULSETIME',1*MyUnits.us);
            obj.SetParameter('MAXIMUMPULSETIME',10*MyUnits.us);
            obj.SetParameter('NPULSETIMES',1);
            
            obj.SetParameter('MINIMUMTAU',25*MyUnits.us);
            obj.SetParameter('MAXIMUMTAU',50*MyUnits.us);
            obj.SetParameter('NTAUS',1);

            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.90);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.10);
        end
        function UpdateTETR(obj)
            obj.TE = obj.GetParameter('TRANSIENTTIME') + ...
                    0.5 * obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH');
            obj.TR = obj.TE +  0.5*(obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH')) + 0.5*obj.GetParameter('PULSETIME') +...
                    obj.GetParameter('BLANKINGDELAY') + obj.GetParameter('REPETITIONDELAY');
    
            if obj.spoiler_en
                obj.TR = obj.TR + obj.GetParameter( 'SPOILERTIME' );
            end
            obj.TTotal = obj.TR * obj.GetParameter('NSCANS') * obj.GetParameter('NSLICES') * obj.GetParameter('NPHASES');
        end
        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence,
            %         "1", central frequency adjustment sequence,
            
            % first set the appropriate axes as were decided
            [~,~,~]=SelectAxes(obj);
            
            %[~,~,readout,slice,phase,~,~] = obj.ConfigureGradients(mode);
            
            % Initialize all vectors to zero with the appropriate size
            nPoints = obj.GetParameter('NREADOUTS');
            obj.nInstructions = 3+13*(nPoints)+1;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            
            % Calculate the number of line reads
            obj.nline_reads=nPoints;
            
            % Timing
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6;
            blk = obj.GetParameter('BLANKINGDELAY')*1e6;
            trf = obj.GetParameter('PULSETIME')*1e6;
            tau = obj.GetParameter('TAUTIME')*1e6;
            iIns = 1;
            bbit = 1;
            
            %% Create the excitation pulse
            nIns  = 3; % set number of instructions in this group
            vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                obj.PhResets(vIns) = [ 1 0 0 ];
                obj.TXs(vIns) = [0 1 0];
                obj.PHASEs(vIns) = [0 0 0];
                obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                obj.FLAGs(vIns)    = [bbit bbit 0];
                obj.DELAYs(vIns)   = [blk trf tau-trf-blk]; %time in us
            iIns = iIns + nIns; % update the number of instructions
            
            for pointi = 1:nPoints
                %% X Pulse
                nIns  = 3; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.TXs(vIns) = [0 1 0];
                    obj.PHASEs(vIns) = [0 0 0];
                    obj.PhResets(vIns) = [ 1 0 0 ];
                    obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                    obj.FLAGs(vIns)    = [ bbit bbit 0 ];
                    obj.DELAYs(vIns)   = [blk trf tau-trf-blk]; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
                %% -Y Pulse
                nIns  = 3; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.TXs(vIns) = [0 1 0];
                    obj.PHASEs(vIns) = [0 3 0];
                    obj.PhResets(vIns) = [ 1 0 0 ];
                    obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                    obj.FLAGs(vIns)    = [ bbit bbit 0 ];
                    obj.DELAYs(vIns)   = [blk trf 2*tau-trf-blk]; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
                %% Y Pulse
                nIns  = 3; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.TXs(vIns) = [0 1 0];
                    obj.PHASEs(vIns) = [0 1 0];
                    obj.PhResets(vIns) = [ 1 0 0 ];
                    obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                    obj.FLAGs(vIns)    = [ bbit bbit 0 ];
                    obj.DELAYs(vIns)   = [blk trf tau-trf-blk]; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
                %% -X Pulse
                nIns  = 3; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.TXs(vIns) = [0 1 0];
                    obj.PHASEs(vIns) = [0 2 0];
                    obj.PhResets(vIns) = [ 1 0 0 ];
                    obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                    obj.FLAGs(vIns)    = [ bbit bbit 0 ];
                    obj.DELAYs(vIns)   = [blk trf tau-trf/2-acq_time/2]; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
                %% Acquisition
                nIns = 1;
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.RXs(vIns)  = 1;
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.DELAYs(vIns)   = acq_time+tau-trf/2-acq_time/2-blk;
                iIns = iIns + nIns; % update the number of instructions in this block

            end
            
            %% Repetition Delay
            nIns  = 1; % set number of instructions in this group
            vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                obj.PhResets(vIns) = 1;
                obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                obj.DELAYs(vIns)   = obj.GetParameter('REPETITIONDELAY')*1e6; %time in us
            iIns = iIns + nIns; % update the number of instructions
            
        end
        function [status,result] = Run(obj,iAcq)
            clc
            tic;
            global MRIDATA
            [~,~,~,~,~,~,~] = obj.ConfigureGradients(0);
            
            %% Get sequence parameters
            global rawData;               % Raw Data contains output info
            rawData = [];

            nEchoes     = obj.GetParameter('NREADOUTS');
            fmin        = obj.GetParameter('MINIMUMFREQUENCY');
            fmax        = obj.GetParameter('MAXIMUMFREQUENCY');
            nf          = obj.GetParameter('NFREQUENCIES');
            amin        = obj.GetParameter('MINIMUMAMPLITUDE');
            amax        = obj.GetParameter('MAXIMUMAMPLITUDE');
            na          = obj.GetParameter('NAMPLITUDES');
            trfmin      = obj.GetParameter('MINIMUMPULSETIME');
            trfmax      = obj.GetParameter('MAXIMUMPULSETIME');
            ntrf        = obj.GetParameter('NPULSETIMES');
            taumin      = obj.GetParameter('MINIMUMTAU');
            taumax      = obj.GetParameter('MAXIMUMTAU');
            ntau        = obj.GetParameter('NTAUS');
            bw = obj.GetParameter('SPECTRALWIDTH');
            
            nPoints = 1;
            taq = 1/bw;
            
            rawData.inputs.sequence = obj.SequenceName;
            rawData.inputs.NEX = obj.GetParameter('NSCANS');
            rawData.inputs.nEchoes = nEchoes;
            rawData.inputs.fmin = fmin;
            rawData.inputs.fmax = fmax;
            rawData.inputs.nf = nf;
            rawData.inputs.amin = amin;
            rawData.inputs.amax = amax;
            rawData.inputs.na = na;
            rawData.inputs.trfmin = trfmin;
            rawData.inputs.trfmax = trfmax;
            rawData.inputs.ntrf = ntrf;
            rawData.inputs.taumin = taumin;
            rawData.inputs.taumax = taumax;
            rawData.inputs.ntau = ntau;
            rawData.aux.acquisitionTime = taq;
            rawData.inputs.delay = obj.GetParameter('REPETITIONDELAY');
            rawData.inputs.bandwidth = obj.GetParameter('SPECTRALWIDTH');
            
            obj.AcquisitionTime = taq;            
            Data2D = [];
            
            %% Create matrix with inputs
            parsweep = zeros(na*nf*ntrf*ntau,4);
            n = 0;
            vec1 = linspace(amin,amax,na);
            vec2 = linspace(fmin,fmax,nf);
            vec3 = linspace(trfmin,trfmax,ntrf);
            vec4 = linspace(taumin,taumax,ntau);
            for ii = 1:na
                for jj = 1:nf
                    for kk = 1:ntrf
                        for ll = 1:ntau
                            n = n+1;
                            parsweep(n,1) = vec1(ii);
                            parsweep(n,2) = vec2(jj);
                            parsweep(n,3) = vec3(kk);
                            parsweep(n,4) = vec4(ll);
                        end
                    end
                end
            end
            
            %% Acquisition
            obj.PhasesPerBatch = floor(min([1024/nf*ntrf*ntau*(3+13*(nPoints)+1),16000/1]));                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
            
            % NOW RUN ACTUAL ACQUISTION
            disp('New Acquisition')
            h_waitbar = waitbar(0,'');
            for ii = 1:na*nf*ntrf*ntau
                waitbar(ii/(na*nf*ntrf*ntau),h_waitbar,sprintf('Sweep: %5.2f / %5.2f',ii,na*nf*ntrf*ntau));
                disp('New Amplitude')
                obj.SetParameter('RFAMPLITUDE',parsweep(ii,1));
                obj.SetParameter('FREQUENCY',parsweep(ii,2));
                obj.SetParameter('PULSETIME',parsweep(ii,3));
                obj.SetParameter('TAUTIME',parsweep(ii,4));
                obj.CreateSequence(0);
                obj.Sequence2String;
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Acquisition aborted!','Warning')
                    return;
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                Data2D = [Data2D;cDataBatch(:)];
            end
            delete(h_waitbar)
            
            %% Time matrix
            medidas = zeros(na*nf*ntrf*ntau*nEchoes,5);
            n = 1;
            m = 0;
            for ii = 1:na
                for jj = 1:nf
                    for kk = 1:ntrf
                        for ll = 1:ntau
                            m = m+1;
                            medidas(n:n+nEchoes-1,1) = parsweep(m,1);
                            medidas(n:n+nEchoes-1,2) = parsweep(m,2);
                            medidas(n:n+nEchoes-1,3) = parsweep(m,3);
                            medidas(n:n+nEchoes-1,4) = parsweep(m,4);
                            medidas(n:n+nEchoes-1,5) = linspace(6*parsweep(m,4),nEchoes*6*parsweep(m,4),nEchoes)';
                            n = n+nEchoes;
                        end
                    end
                end
            end
            medidas = [medidas Data2D];
            rawData.kSpace.sampled = medidas;
            rawData.kSpace.headings = ['Amplitudes, Frequencies, Pulse times, Tau times, Time vector, Data'];

            %% Save rawData
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(fullfile(obj.path_file,name),'rawData');
        end
    end
    
end

