classdef PETRA_V1 < MRI_BlankSequence_JM
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    %  Elena Diaz Caballero
    
    properties
        PhasesPerBatch = 20;
        nPhasesBatch;
        Axis1Vector;
        Axis2Vector;
        Axis3Vector;
        StartSlice;
        lastBatch;
        EndSlice;
        StartPhase;
        EndPhase;
        Stage;              % 'ZTE' or 'SP'
        slice_ampl;
    end
    methods (Access=private)
        function CreateParameters(obj)

            % HELP %
            % I include here new inputs to discriminate between
            % acquistition and regruidding.
            % -> NPHASES, NREADOUTS and NSLICES are for regridding
            % -> NPOINTS, NCIRCUNFERENCES, NLINESPERCIRCUNFERENCES are for
            %    aqcuisition.

            InputParameters = {...
                'FOVX','FOVY','FOVZ',...
                'NPHASES','NREADOUTS','NSLICES','REPETITIONDELAY',...
                'TMAX',...
                'OVERSAMPLING','JUMPINGFACTOR',...
                'PHASE','READOUT','SLICE'...
                };
            
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT','NPOINTS','SPECTRALWIDTH'};
            
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(InputParameters,...
                {...
                'FOVx','FOVy','FOVz',...
                'Ny','Nx','Nz','Repetition Delay',...
                'T max',...
                'Oversampling','Jumping Factor',...
                'Y Axis','X Axis','Z Axis'...
                })];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,...
                NaN,...
                NaN,NaN,...
                '','',''...
                })];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(InputParameters,...
                {...
                0,0,0,...
                NaN,NaN,NaN,NaN,...
                NaN,...
                1,1,...
                '','',''...
                })];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(InputParameters,...
                {...
                NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,...
                NaN,...
                NaN,NaN,...
                '','',''...
                })];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(InputParameters,...
                {...
                'mm','mm','mm',...
                '','','','ms',...
                'us',...
                '','',...
                '','',''...
                })];
            
            for k=1:length(obj.InputParameters)
                InputParameters(strcmp(obj.InputParameters{k},InputParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters,InputParameters];
            % Reorganizes InputParameters
            obj.MoveParameter(6,21);
            obj.MoveParameter(13:21,17:25);
            obj.MoveParameter(17,18);
            obj.MoveParameter(23,24);
        end
        
        function MoveParameter(obj,x,y)
            xx = obj.InputParameters(x);
            output = obj.InputParameters;
            output(x) = [];
            output(y+1:end+1) = output(y:end);
            output(y) = xx;
            obj.InputParameters = output;
        end
    end
    
    
    %======================== Public Methods =================================
    methods
        function obj = PETRA_V1(program)
            obj = obj@MRI_BlankSequence_JM();
            obj.SequenceName = 'PETRA V1';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            if nargin > 0
                obj.ProgramName = program;
            end
            obj.CreateParameters();
            obj.SetDefaultParameters();
        end
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence_JM(obj);
            obj.SetParameter('FOVX',10*MyUnits.mm);
            obj.SetParameter('FOVY',10*MyUnits.mm);
            obj.SetParameter('FOVZ',10*MyUnits.mm);
            obj.SetParameter('REPETITIONDELAY',50*MyUnits.ms);
            obj.SetParameter('TMAX',70*MyUnits.us);
            obj.SetParameter('NPHASES',40);
            obj.SetParameter('NREADOUTS',40);
            obj.SetParameter('OVERSAMPLING',1);
            obj.SetParameter('JUMPINGFACTOR',1);
            obj.SetParameter('TRANSIENTTIME',20*MyUnits.us);
            obj.SetParameter('COILRISETIME',200*MyUnits.us);

            
            obj.SetParameterMin('FREQUENCY',obj.GetParameter('FREQUENCY')*0.95);
            obj.SetParameterMax('FREQUENCY',obj.GetParameter('FREQUENCY')*1.05);
        end
        function UpdateTETR(obj)
            obj.TE = obj.GetParameter('TRANSIENTTIME') + ...
                    0.5 * obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH');
            obj.TR = obj.TE +  0.5*(obj.GetParameter('NPOINTS') / obj.GetParameter('SPECTRALWIDTH')) + 0.5*obj.GetParameter('PULSETIME') +...
                    obj.GetParameter('BLANKINGDELAY') + obj.GetParameter('REPETITIONDELAY');
    
            if obj.spoiler_en
                obj.TR = obj.TR + obj.GetParameter( 'SPOILERTIME' );
            end
            obj.TTotal = obj.TR * obj.GetParameter('NSCANS') * obj.GetParameter('NSLICES') * obj.GetParameter('NPHASES');
        end
        function CreateSequence( obj, mode )
            % mode =  "0", normal execution of the sequence,
            %         "1", central frequency adjustment sequence,
            
            % first set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            
            %[~,~,readout,slice,phase,~,~] = obj.ConfigureGradients(mode);
            nPhases = obj.nPhasesBatch;

            % Calculate the number of line reads
            obj.nline_reads=nPhases;
            
            % Initialize all vectors to zero with the appropriate size
            obj.nInstructions=7*(nPhases);
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
            iIns = 1;
            bbit = 1;            
            for phasei = 1:nPhases;
                
                readoutAmplitude = obj.Axis1Vector(phasei);
                phaseAmplitude = obj.Axis2Vector(phasei);
                sliceAmplitude = obj.Axis3Vector(phasei);
                
                %% Activate the total gradient (3 instrucctions)
                nIns  = 3; % set number of instructions in this group
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.AMPs(vIns)     = [ readoutAmplitude phaseAmplitude sliceAmplitude ];
                    obj.DACs(vIns)     = [ RR PP SS ];
                    obj.WRITEs(vIns)   = [ 1 1 1 ];
                    obj.UPDATEs(vIns)  = [ 1 1 1 ];
                    obj.PhResets(vIns) = [ 0 0 1 ];
                    obj.ENVELOPEs(vIns)= [ 7 7 7 ]; % (7=no shape)
                    obj.FLAGs(vIns)    = [ bbit bbit bbit ];
                    obj.DELAYs(vIns)   = [0.1 0.1 (obj.GetParameter('BLANKINGDELAY')+obj.GetParameter('COILRISETIME'))*1e6]; %time in us
                iIns = iIns + nIns; % update the number of instructions
                
                %% RF pulse (1 instruction)
                nIns = 1; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.TXs(vIns)      = 1;
                    obj.ENVELOPEs(vIns)= 7;
                    obj.FLAGs(vIns)    = bbit;
                    obj.DELAYs(vIns)   = obj.GetParameter('PULSETIME')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %% Transient Delay
                nIns = 1; % set the number of instruction in this block
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.DELAYs(vIns)   = obj.GetParameter('TRANSIENTTIME')*1e6; %time in us                    
                iIns = iIns + nIns; % update the number of instructions in this block
                                
                %% Acquisition
                nIns = 1;
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    obj.RXs(vIns)  = 1;
                    obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                    obj.DELAYs(vIns)   = acq_time*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
                %% Repetition Delay (1 instruction)
                
                nIns = 1;
                vIns =  iIns: (nIns+iIns-1); % calculate the instruction vector index
                    if(obj.lastBatch==1 && phasei==nPhases)
                        obj.AMPs(vIns)     = 0.0;
                        obj.DACs(vIns)     = 3;
                        obj.WRITEs(vIns)   = 1;
                        obj.UPDATEs(vIns)  = 1;
                        obj.PHASEs(vIns)   = 0;
                        obj.CLEARs(vIns)   = 1;
                        obj.ENVELOPEs(vIns)= 7; % (7=no shape)
                        disp('Last Batch!')
                    end
                    % The gradients are turned off for now. Just to prevent
                    % gradients burnings
                    %%%%%%%%%%%%%%%%%%%%%
                    obj.AMPs(vIns) = 0;
                    obj.DACs(vIns) = 3;
                    obj.WRITEs(vIns) = 1;
                    obj.UPDATEs(vIns) = 1;
                    %%%%%%%%%%%%%%%%%%%%%%
                    obj.DELAYs(vIns)   = obj.GetParameter('REPETITIONDELAY')*1e6; %time in us
                iIns = iIns + nIns; % update the number of instructions in this block
                
            end % end for nPhases
            %             end %end for slicei
            
        end % end create gradient echo pulse sequence
        function [status,result] = Run(obj,iAcq)
            tic;
            global MRIDATA
            [~,~,readout,slice,phase,~,~] = obj.ConfigureGradients(0);
            
            %% Constant to relate k with k'
            % HELP: k = gammabar*G*t
            % G = c*A where A is the gradient amplitude voltage from
            % radioprocessor. Then, k' = k/(gammabar*c) = A*t.
            % We still need to calibrate constant c. We also need to take
            % into account that c is different for each axis. 
            gammabar = 42.6d6;  % MHz/T
            c = [10,10,10];            % T/m/V
            rawData = [];               % Raw Data contains output info
            
            %% Get cartesian parameters
            nPoints = [obj.GetParameter('NREADOUTS'),obj.GetParameter('NPHASES'),obj.GetParameter('NSLICES')];
            fov = [obj.GetParameter('FOVX'),obj.GetParameter('FOVY'),obj.GetParameter('FOVZ')];
            deltaK = 1./fov;
            kMax = nPoints./(2*fov);
            
            rawData.inputs.nPoints = nPoints;
            rawData.inputs.fov = fov;
            rawData.aux.deltaK = deltaK;
            rawData.aux.kMax = kMax;
            
            %% Get sequence parameters
            tMax = obj.GetParameter('TMAX');                                % Max time for acquisition, it should be the shortest T2 to be acquired
            ov = obj.GetParameter('OVERSAMPLING');                          % Oversampling for radial acquisition. It helps for better regridding
            bw = max(nPoints-1)/(2*tMax);                                   % Ideal acquisition bandwidth
            bwov = ov*bw;                                                   % Acquisition bandwidth taking into account the oversampling
            obj.SetParameter('SPECTRALWIDTH',bwov);                         % Set the SPECTRALWIDTH to bw
            trf = obj.GetParameter('PULSETIME');                            % RF pulse time
            origin = 0.5;                                                   % Set the origing where k = 0;
            td = obj.GetParameter('TRANSIENTTIME');                         % Dead time to wait until TX/RX switching
            
            rawData.inputs.tMax = tMax;
            rawData.inputs.oversampling = ov;
            rawData.aux.bandwidth = bw;
            rawData.aux.bandwidthov = bwov;
            rawData.inputs.pulseTime = trf;
            rawData.inputs.rfAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.deadTime = td;
            
            %% Set number of phases, points and slices as well as amplitudes
            % gradientAmplitude = normalized amplitude to the MRIGUI input
            % nLPC = number of lines per circunferece in k-space
            % nCir = number of circunfereces in gradient space
            % nPPL = number of acquired points per line in k-space
            gradientAmplitudes = min(kMax./(gammabar*c*tMax).*~(nPoints==1),1);
            nPPL = ceil((tMax-td-origin*trf)*bwov)+1;
            nLPC = ceil(max(nPoints(1:2))*pi);
            nLPC = max(nLPC-mod(nLPC,2),1);
            nCir = max(ceil(nPoints(3)*pi/2)-1,1);
            obj.SetParameter('NPOINTS',nPPL);
            if(nPoints(2)==1)
                nLPC = 2;
            end
            
            rawData.aux.numberOfPointsPerLine = nPPL;
            rawData.aux.linesPerCircumference = nLPC;
            rawData.aux.circunferences = nCir;
            rawData.aux.gradientAmplitudes = gradientAmplitudes;

            %% Calculate the gradients list for radial sampling
            % Gradient are defined by the sphere parametric equation. The
            % two parameteres are: theta in [0,pi] and phi [0,2pi].
            % Gradients are normalized to the amplifier amplitude.
            deltaTheta = pi/(nCir+1);
            theta = linspace(deltaTheta,pi-deltaTheta,nCir);
            deltaPhi = 2*pi/nLPC;
            phi = linspace(0,2*pi-deltaPhi,nLPC);
            normalizedGradientsRadial = zeros(nLPC*nCir,3);                          % gradientVectors1 has 3 columns with Gx, Gy and Gz
            n = 0;
            for ii = 1:nCir
                for jj = 1:nLPC
                    n = n+1;
                    normalizedGradientsRadial(n,1) = sin(theta(ii))*cos(phi(jj));
                    normalizedGradientsRadial(n,2) = sin(theta(ii))*sin(phi(jj));
                    normalizedGradientsRadial(n,3) = cos(theta(ii));
                end
            end
            % Reorganize the gradient values with a given jumping factor
            jumpingFactor = obj.GetParameter('JUMPINGFACTOR');
            index = 1:jumpingFactor:size(normalizedGradientsRadial,1);
            if(jumpingFactor>1)
                for ii = 2:jumpingFactor
                    index = cat(2,index,ii:jumpingFactor:size(normalizedGradientsRadial,1));
                end
            end
            normalizedGradientsRadial = normalizedGradientsRadial(index,:);
            gradientVectors1 = normalizedGradientsRadial*diag(gradientAmplitudes);
            clear index
            
            rawData.aux.gradientsRadial = gradientVectors1;
            
            %% Calculate k-points matrix for radial sampling
            normalizedKRadial = zeros(nCir*nLPC,3,nPPL);
            % Calculate k-points at t = 0.5*trf+td
            normalizedKRadial(:,:,1) = (origin*trf+td+0.5/bwov)*normalizedGradientsRadial;
            % Calculate all k-points
            for ii=2:nPPL
                normalizedKRadial(:,:,ii) = normalizedKRadial(:,:,1)+(ii-1)*normalizedGradientsRadial/obj.GetParameter('SPECTRALWIDTH');
            end
            normalizedKRadial = reshape(permute(normalizedKRadial,[3,1,2]),[nCir*nLPC*nPPL,3]);
            kRadial = normalizedKRadial*diag(gammabar*c.*gradientAmplitudes);
            kSpaceValues = kRadial;
            
            %% k-points for Cartesian sampling
            acq_time = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
            tMin = origin*trf+td+0.5/bwov;
            % k points for cartesian gridd in cartesian coordinates
            kx = linspace(-kMax(1)*~isequal(nPoints(1),1),kMax(1)*~isequal(nPoints(1),1),nPoints(1));
            ky = linspace(-kMax(2)*~isequal(nPoints(2),1),kMax(2)*~isequal(nPoints(2),1),nPoints(2));
            kz = linspace(-kMax(3)*~isequal(nPoints(3),1),kMax(3)*~isequal(nPoints(3),1),nPoints(3));
            [kx,ky,kz] = meshgrid(kx,ky,kz);
            kCartesian(:,1) = kx(:);
            kCartesian(:,2) = ky(:);
            kCartesian(:,3) = kz(:);
            normalizedKCartesian = kCartesian/diag(gammabar*c.*(gradientAmplitudes+double(gradientAmplitudes==0)));
            normalizedKCartesian(:,4) = vecnorm(normalizedKCartesian,2,2);
            normalizedKSinglePoint = normalizedKCartesian(normalizedKCartesian(:,4)<tMin,1:3);
            kSinglePoint = normalizedKSinglePoint*diag(gammabar*c.*gradientAmplitudes);
            kSpaceValues = cat(1,kSpaceValues,kSinglePoint);
            
            %% Gradients for Cartesian sampling
            gradientVectors2 = kSinglePoint/diag(gammabar*tMin*c);
            
            rawData.aux.gradientsSinglePoint = gradientVectors2;
            
            %% Auxiliar
            Data2D = [];
            
            %% Radial
            % Create waiting bar
            tBatch1 = clock;
            obj.PhasesPerBatch = floor(min([1024/7,16000/nPPL]));                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
            nPhaseBatches = ceil(nLPC*nCir/obj.PhasesPerBatch);
            rawData.aux.numberOfBatches = nPhaseBatches;
            cn = nPhaseBatches;
            count = 0;
            if nPhaseBatches>1
                h_waitbar = waitbar(count/cn,'Radial (por favor espere)...');
            end
            % Start batches sweep
            disp('New acquisition');
            disp('RADIAL');
            tRadial = 0;
            for iBatch = 1:nPhaseBatches
                disp('New batch');
                
                % Get the appropiate axis
                if(iBatch<nPhaseBatches)
                    obj.Axis1Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,1);
                    obj.Axis2Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,2);
                    obj.Axis3Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,3);
                    obj.lastBatch = 0;
                else
                    obj.Axis1Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:nLPC*nCir,1);
                    obj.Axis2Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:nLPC*nCir,2);
                    obj.Axis3Vector = gradientVectors1((iBatch-1)*obj.PhasesPerBatch+1:nLPC*nCir,3);
                    obj.lastBatch = 0;
                end
                obj.nPhasesBatch = length(obj.Axis1Vector);
                
                % NOW RUN ACTUAL ACQUISTION
                obj.CreateSequence(0);
                obj.Sequence2String;            
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                t1 = clock;
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                t2 = clock;
                tRadial = tRadial + etime(t2,t1);
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                nPhasesInBatch=length(cDataBatch)/nPPL;
                d = reshape(cDataBatch,nPPL,nPhasesInBatch);
                Data2D = cat(2,Data2D,d);
                count = count+1;
                if nPhaseBatches>1
                    waitbar(count/cn,h_waitbar,sprintf('Radial (por favor espere)... %5.2f %%',100*count/cn));
                end
            end
            if nPhaseBatches>1
                delete(h_waitbar);
            end
            Data2D = Data2D(:);
            
            %% Single point
            % Create waiting bar
            obj.PhasesPerBatch = floor(min([1800/7,16000]));
            nAcquiredPoints = size(gradientVectors2,1);
            nPhaseBatches = ceil(nAcquiredPoints/obj.PhasesPerBatch);
            cn = nPhaseBatches;
            count = 0;
            if nPhaseBatches>1
                h_waitbar = waitbar(count/cn,'Cartesian (por favor espere)...');
            end
            % Start batches sweep
            disp('SINGLE POINT');
            fprintf('points = %i \n',nAcquiredPoints);
            fprintf('nPhaseBatches = %i \n',nPhaseBatches);
            obj.SetParameter('NPOINTS',1);
            repDelay = obj.GetParameter('REPETITIONDELAY');
            obj.SetParameter('REPETITIONDELAY',repDelay+acq_time);
            tCartesian = 0;
            for iBatch = 1:nPhaseBatches
                disp('New batch');
                % Get the appropiate axis
                if(iBatch<nPhaseBatches)
                    obj.Axis1Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,1);
                    obj.Axis2Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,2);
                    obj.Axis3Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,3);
                    obj.lastBatch = 0;
                else
                    obj.Axis1Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:end,1);
                    obj.Axis2Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:end,2);
                    obj.Axis3Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:end,3);
                    obj.lastBatch = 1;
                end
                obj.nPhasesBatch = length(obj.Axis1Vector);
                
                % NOW RUN ACTUAL ACQUISTION
                obj.CreateSequence(0);
                obj.Sequence2String;            
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                t1 = clock;
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                t2 = clock;
                tCartesian = tCartesian + etime(t2,t1);
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Batch aborted!','Warning')
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                d = cDataBatch;
                Data2D = cat(1,Data2D,d);
                count = count+1;
                if nPhaseBatches>1
                    waitbar(count/cn,h_waitbar,sprintf('Cartesian (por favor espere)... %5.2f %%',100*count/cn));
                end
            end
            if nPhaseBatches>1
                delete(h_waitbar);
            end
            obj.SetParameter('NPOINTS',nPPL);
            obj.SetParameter('REPETITIONDELAY',repDelay);
            disp(' ');
            kSpaceValues = [kSpaceValues,Data2D(:)];
            tBatch2 = clock;
            tBatch = etime(tBatch2,tBatch1);
            rawData.kSpace.sampled = kSpaceValues;
%             figure
%             plot(kSpaceValues(:,1),abs(kSpaceValues(:,4)),'o');
            
            
            %% Create MRIDATA.Data3Dnew
            Data3D = zeros(nPoints(1),nPoints(2),nPoints(3));
            if isfield(MRIDATA,'Data3Dnew')
                if ( isequal(size(MRIDATA.Data3Dnew),[nPPL, nLPC, nCir, obj.NAverages])) || (iAcq == 1)
                    MRIDATA.Data3Dnew = [];
                end
            else
                MRIDATA.Data3Dnew = [];
            end
            
            %% Regridding
            if(nCir>1)
                %[kx,ky,kz] = meshgrid(kCartesian(:,1),kCartesian(:,2),kCartesian(:,3));
                valCartesian = griddata(kSpaceValues(:,1),kSpaceValues(:,2),kSpaceValues(:,3),kSpaceValues(:,4),kCartesian(:,1),kCartesian(:,2),kCartesian(:,3));
            else
                %[kx,ky] = meshgrid(kCartesian(:,1),kCartesian(:,2));
                %kz = zeros(size(kx));
                valCartesian = griddata(kSpaceValues(:,1),kSpaceValues(:,2),kSpaceValues(:,4),kCartesian(:,1),kCartesian(:,2));
            end
            valCartesian(isnan(valCartesian)) = 0;
            rawData.kSpace.interpolated = [kx(:),ky(:),kz(:),valCartesian(:)];
            
            %% Save data
            Data3D = valCartesian;
            MRIDATA.Measurements = kSpaceValues;
            MRIDATA.Data3Dnew(end+1).Fid3D = Data3D;
            if length(obj.cData) ~= prod(nPoints);
                obj.cData = zeros(obj.NAverages,prod(nPoints));
            end
            if obj.averaging
                Nav = obj.NAverages;
            else
                Nav = 1;
            end
            index = mod(iAcq-1,Nav)+1;
            if obj.autoPhasing
                %                    pt = max(Data3D(:,ceil(size(Data3D,2)/2),ceil(size(Data3D,3)/2)));
                pt = max(Data3D(:));
                Data3D = Data3D.*exp(complex(0,-angle(pt)));
            end
            obj.cData(index,1:length(Data3D(:))) = Data3D(:);
            obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
            obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
            %    obj.fData1D = fftshift(ifft(fftshift(obj.cDataAv)));
            obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
            acq_time = obj.GetParameter('ACQUISITIONTIME');
            obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*acq_time/nLPC;
            obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time/nLPC;
            MRIDATA.obj = obj;
            obj.SetParameter('TRANSIENTTIME',td);
            
            % Save elapsed times to raw data
            elapsedTime = toc;
            rawData.aux.tRadial = tRadial;
            rawData.aux.tCartesian = tCartesian;
            rawData.aux.tBatch = tBatch;
            rawData.aux.elapsedTime = elapsedTime;
            
            % Save rawData
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(strcat('C:\Users\josalggui\Documents\mri-lab-gui-matlab-2018\MRI Control\FileDir\',name),'rawData');
        end
    end
    
end

