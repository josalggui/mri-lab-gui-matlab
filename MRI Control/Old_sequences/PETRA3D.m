classdef PETRA3D < MRI_BlankSequence_JM    
  
    properties
        maxRepetitionsPerBatch;
        nRepetitionsInBatch;
        gradient1Vector;
        gradient2Vector;
        gradient3Vector;
        lastBatch;
        stage;              % ZTE -> 1, Single Point ->2 or Prescan -> 0
        repetitionTime;
        acquisitionTime;
    end
    
    methods (Access=private)
        
        function CreateParameters(obj)
            CreateOneParameter(obj,'THETA','Altitude','degrees',45)
            CreateOneParameter(obj,'PHI','Azimuth','degrees',45)
            CreateOneParameter(obj,'NREADOUTS','Nx','',50)
            CreateOneParameter(obj,'NPHASES','Ny','',50)
            CreateOneParameter(obj,'NSLICE','Nz','',50)
            CreateOneParameter(obj,'FOVX','FOV x','mm',20*MyUnits.mm)
            CreateOneParameter(obj,'FOVY','FOV y','mm',20*MyUnits.mm)
            CreateOneParameter(obj,'FOVZ','FOV z','mm',20*MyUnits.mm)
            CreateOneParameter(obj,'TMAX','T max','us',70*MyUnits.us)
            CreateOneParameter(obj,'REPETITIONTIME','Repetition time','ms',600*MyUnits.ms)
            CreateOneParameter(obj,'OVERSAMPLING','Oversampling','',1);
            CreateOneParameter(obj,'JUMPINGFACTOR','Jumping factor','',1);
            
            obj.InputParHidden = {'NPOINTS','NSLICES','SHIMSLICE','SHIMPHASE','SHIMREADOUT','SPECTRALWIDTH'};
        end
    end
    
            
    methods
        function obj = PETRA3D(program)
            obj = obj@MRI_BlankSequence_JM();
            obj.SequenceName = 'PETRA3D';
            obj.ProgramName = 'MRI_BlankSequence3.exe';
            
            if nargin > 0
                obj.ProgramName = program;
            end
            
            obj.CreateParameters();
            obj.SetDefaultParameters();        
        end
        
        function SetDefaultParameters(obj)
            SetDefaultParameters@MRI_BlankSequence_JM(obj);
        end
        
        function CreateSequence(obj,mode)
            %% Set the appropriate axes as were decided
            [shimS,shimP,shimR]=SelectAxes(obj);
            [~,~,readout,slice,phase,~,~] = obj.ConfigureGradients(mode);
                
            %% Calculate the number of line reads
            nRepetitions = obj.nRepetitionsInBatch;
            obj.nline_reads = nRepetitions;
            obj.nInstructions = 7*(nRepetitions);
            
            %% Initialize all vectors to zero with the appropriate size (try dynamical size vector)
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            dacPhase=obj.selectPhase;
            dacReadout=obj.selectReadout;
            dacSlice=obj.selectSlice;
            
            %% Timing definitions
            acqTime = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6;
            blk = obj.GetParameter('BLANKINGDELAY');
            crt = obj.GetParameter('COILRISETIME')*1e6;
            trf = obj.GetParameter('PULSETIME')*1e6;
            td = obj.GetParameter('TRANSIENTTIME')*1e6;
            tr = obj.GetParameter('REPETITIONTIME')*1e6;
            obj.GetParameter('REPETITIONDELAY') = blk + crt + sqrt(3)*tMax-tr;
            obj.SetParameter('REPETITIONDELAY',repetDelay);
            bbit = 1; 
            iIns = 1;          
            
            for phasei = 1:nRepetitions
                readoutAmplitude = obj.gradient1Vector(phasei)+shimR;
                phaseAmplitude = obj.gradient2Vector(phasei)+shimP;
                sliceAmplitude = obj.gradient3Vector(phasei)+shimS;
                
                %% Activate gradients + Activate amplifier (3 instrucction)
                iIns = ActivateAmplifierGradient(obj,iIns,bbit,readoutAmplitude,dacReadout,0.1);
                iIns = ActivateAmplifierGradient(obj,iIns,bbit,phaseAmplitude,dacPhase,0.1);
                iIns = ActivateAmplifierGradient(obj,iIns,bbit,sliceAmplitude,dacSlice,crt);
     
                %% RF pulse + Transient delay (2 instruction)
                iIns = CreatePulseWithoutBlk(obj,iIns,7,0,trf,td);
                              
                %% Acquisition (1 instruction)
                iIns = Adquisition(obj,iIns,acqTime);
                
                %% Repetition Delay (1 instruction)
                if(obj.stage~=0)
                    iIns = RepetitionDelay (obj,iIns,1,repetDelay);         % Once we have the new gradients, bool input "1" should be "obj.lastBatch"
                else
                    iIns = RepetitionDelay (obj,iIns,1,acqTime);
                end
           end             
        end
             
        function [status,result] = Run(obj,iAcq)
            tic;
            global MRIDATA
            global rawData;             % Raw Data contains output info
            rawData = [];         
            [~,~,readout,slice,phase,~,~] = obj.ConfigureGradients(0);
            clc;
            %% Constant to relate k with k'
            gammabar = 42.6e6;          % MHz/T
            c = [11,11,8];              % T/m/V

            %% Define and write cartesian parameters
            nPoints = [obj.GetParameter('NREADOUTS'),obj.GetParameter('NPHASES'),obj.GetParameter('NSLICE')];
            fov = [obj.GetParameter('FOVX'),obj.GetParameter('FOVY'),obj.GetParameter('FOVZ')];
            deltaK = 1./fov;
            kMax = (nPoints-1)./(2*fov);
            
            rawData.inputs.NEX = obj.GetParameter('NSCANS');
            rawData.inputs.nPoints = nPoints;
            rawData.inputs.fov = fov;
            rawData.aux.deltaK = deltaK;
            rawData.aux.kMax = kMax;
            
            %% Creates rotation matrix
            altitude = obj.GetParameter('THETA')*pi/180;
            azimuth = obj.GetParameter('PHI')*pi/180;
            ry = [cos(altitude) 0 -sin(altitude) ; 0 1 0 ; sin(altitude) 0 cos(altitude)];
            rz = [cos(azimuth) sin(azimuth) 0 ; -sin(azimuth) cos(azimuth) 0 ; 0 0 1];
            rotationMatrix = ry*rz;
            rawData.inputs.altitude = altitude;
            rawData.inputs.azimuth = azimuth;
            
            %% Reorganize calibration data
            readout = obj.GetParameter('READOUT');
            phase = obj.GetParameter('PHASE');
            slice = obj.GetParameter('SLICE');
            if(readout=='x');       readout = 1;
            elseif(readout=='y');   readout = 2;
            elseif(readout=='z');   readout = 3;
            end
            if(phase=='x');         phase = 1;
            elseif(phase=='y');     phase = 2;
            elseif(phase=='z');     phase = 3;
            end
            if(slice=='x');         slice = 1;
            elseif(slice=='y');     slice = 2;
            elseif(slice=='z');     slice = 3;
            end
            c = c([readout phase slice]);            

            %% Define and write sequence parameters
            acqTime = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH');
            trf = obj.GetParameter('PULSETIME'); 
            blk = obj.GetParameter('BLANKINGDELAY');
            crt = obj.GetParameter('COILRISETIME'); 
            td = obj.GetParameter('TRANSIENTTIME');
            tr = obj.GetParameter('REPETITIONTIME');
            tMax = obj.GetParameter('TMAX'); 
            ov = obj.GetParameter('OVERSAMPLING');                          
            bw = max(nPoints)/(2*tMax);
            bwov = ov*bw;                                                   
            obj.SetParameter('SPECTRALWIDTH',bwov);                                               
            obj.repetitionTime = obj.GetParameter('REPETITIONTIME');
            obj.acquisitionTime = acqTime;
            repetDelay = blk + crt + sqrt(3)*tMax-tr;
            
            
            rawData.inputs.pulseTime = trf;
            rawData.inputs.coilrisetime = crt;
            rawData.inputs.deadTime = td;
            rawData.inputs.tMax = tMax;
            rawData.inputs.TR = obj.repetitionTime;
            rawData.inputs.oversampling = ov;
            rawData.inputs.rfAmplitude = obj.GetParameter('RFAMPLITUDE');
            rawData.inputs.rfFrequency = obj.GetParameter('FREQUENCY');
            rawData.aux.bandwidth = bw;
            rawData.aux.bandwidthov = bwov;
            rawData.aux.acquisitionTime = acqTime;
            rawData.aux.repetitionDelay = repetDelay;
            
            %% Set number of phases, points and slices as well as amplitudes
            gradientAmplitudes = kMax./(gammabar*tMax).*~(nPoints==1);
            nPPL = ceil((sqrt(3)*tMax-td-0.5*trf)*bwov);
            nLPC = ceil(max(nPoints(1:2))*pi/2);    % Ideal should be *pi instead of *pi/2 but I will use *pi/2 to save time
            nLPC = max(nLPC-mod(nLPC,2),1);
            nCir = max(ceil(nPoints(3)*pi/2)-1,1);
            obj.SetParameter('NPOINTS',nPPL);
            
            if(nPoints(2)==1)
                nLPC = 2;
            end
            
            obj.SetParameter('NPOINTS',nPPL);
            acqTime = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6;
            obj.acquisitionTime = acqTime;
            rawData.aux.acquisitionTime = obj.acquisitionTime;
            rawData.aux.numberOfPointsPerLine = nPPL;
            rawData.aux.linesPerCircumference = nLPC;
            rawData.aux.circunferences = nCir;
            rawData.aux.gradientAmplitudes = gradientAmplitudes;
            
            %% Calculate the gradients list for radial sampling
            deltaTheta = pi/(nCir+1);
            theta = linspace(deltaTheta,pi-deltaTheta,nCir);
            deltaPhi = 2*pi/nLPC;
            phi = linspace(0,2*pi-deltaPhi,nLPC);
            normalizedGradientsRadial = zeros(nLPC*nCir,3);
            n = 0;
            for ii = 1:nCir
                for jj = 1:nLPC
                    n = n+1;
                    normalizedGradientsRadial(n,1) = sin(theta(ii))*cos(phi(jj));
                    normalizedGradientsRadial(n,2) = sin(theta(ii))*sin(phi(jj));
                    normalizedGradientsRadial(n,3) = cos(theta(ii));
                end
            end
            % Reorganize the gradient values with a given jumping factor
            jumpingFactor = obj.GetParameter('JUMPINGFACTOR');
            index = 1:jumpingFactor:size(normalizedGradientsRadial,1);
            if(jumpingFactor>1)
                for ii = 2:jumpingFactor
                    index = cat(2,index,ii:jumpingFactor:size(normalizedGradientsRadial,1));
                end
            end
            normalizedGradientsRadial = normalizedGradientsRadial(index,:);
            gradientVectors1 = normalizedGradientsRadial*diag(gradientAmplitudes);
            clear index
             
            %% Calculate k-points matrix for radial sampling
            normalizedKRadial = zeros(nCir*nLPC,3,nPPL);
            % Calculate k-points at t = 0.5*trf+td
            normalizedKRadial(:,:,1) = (0.5*trf+td+0.5/bwov)*normalizedGradientsRadial;
            % Calculate all k-points
            for ii=2:nPPL
                normalizedKRadial(:,:,ii) = normalizedKRadial(:,:,1)+(ii-1)*normalizedGradientsRadial/obj.GetParameter('SPECTRALWIDTH');
            end
            normalizedKRadial = reshape(permute(normalizedKRadial,[3,1,2]),[nCir*nLPC*nPPL,3]);
            kRadial = normalizedKRadial*diag(gammabar*gradientAmplitudes);
            kSpaceValues = kRadial;
            
            %% k-points for Cartesian sampling
            tMin = 0.5*trf+td+0.5/bwov;
            kx = linspace(-kMax(1)*~isequal(nPoints(1),1),kMax(1)*~isequal(nPoints(1),1),nPoints(1));
            ky = linspace(-kMax(2)*~isequal(nPoints(2),1),kMax(2)*~isequal(nPoints(2),1),nPoints(2));
            kz = linspace(-kMax(3)*~isequal(nPoints(3),1),kMax(3)*~isequal(nPoints(3),1),nPoints(3));
            [kx,ky,kz] = meshgrid(kx,ky,kz);
            kx = permute(kx,[2,1,3]);
            ky = permute(ky,[2,1,3]);
            kz = permute(kz,[2,1,3]);
            kCartesian(:,1) = kx(:);
            kCartesian(:,2) = ky(:);
            kCartesian(:,3) = kz(:);
            normalizedKCartesian = kCartesian/diag(gammabar*(gradientAmplitudes+double(gradientAmplitudes==0)));
            normalizedKCartesian(:,4) = sqrt(sum(normalizedKCartesian.^2,2));
            normalizedKSinglePoint = normalizedKCartesian(normalizedKCartesian(:,4)<tMin,1:3);
            kSinglePoint = normalizedKSinglePoint*diag(gammabar*gradientAmplitudes);
            kSpaceValues = cat(1,kSpaceValues,kSinglePoint);  
            
            %% Gradients for Cartesian sampling
            gradientVectors2 = kSinglePoint/diag(gammabar*tMin);
            
            %% Rotates the reference system to produce desired gradients
            gradientVectors1 = gradientVectors1*rotationMatrix;
            gradientVectors2 = gradientVectors2*rotationMatrix;
            
            rawData.aux.gradientsRadial = gradientVectors1;
            rawData.aux.gradientsSinglePoint = gradientVectors2;
            
            gradientVectors1 = gradientVectors1/diag(c);
            gradientVectors2 = gradientVectors2/diag(c);
            
            %% Auxiliar
            Data2D = [];
           
            %% Prescan
            tBatch1 = clock;
            obj.stage = 0;
            obj.maxRepetitionsPerBatch = floor(min([1024/7,16000/nPPL]));
            disp('New acquisition');
            disp('PRESCAN');
            NEX = obj.GetParameter('NSCANS');
            obj.SetParameter('NSCANS',1000);
            tPrescan = 0;
            for iBatch = 1:1
                obj.nRepetitionsInBatch = 1;
                obj.gradient1Vector = gradientVectors1(1,1);
                obj.gradient2Vector = gradientVectors1(1,2);
                obj.gradient3Vector = gradientVectors1(1,3);
                obj.lastBatch = 0;
                % NOW RUN ACTUAL ACQUISTION
                [cDataCal,tPrescan,status,result] = DataBatch(obj,0,tPrescan);
            end

            obj.SetParameter('NSCANS',NEX);
            rawData.kSpace.calibra = cDataCal;
            clear NEX

            %% Radial
            % Create waiting bar
            
            obj.stage = 1;
            obj.maxRepetitionsPerBatch = floor(min([1024/7,16000/nPPL]));                  % obj.PhasesPerBatch = min([maxOrders,maxPoints]);
            nPhaseBatches = ceil(nLPC*nCir/obj.maxRepetitionsPerBatch);
            rawData.aux.numberOfBatches = nPhaseBatches;
            cn = nPhaseBatches;
            count = 0;
            if nPhaseBatches>1
                h_waitbar = waitbar(count/cn,'Radial (por favor espere)...');
            end
            % Start batches sweep
            disp('RADIAL');
            tRadial = 0;
            rawData.inputs.rfFrequencyRadial = zeros(nPhaseBatches,1);
            for iBatch = 1:nPhaseBatches
                disp('New batch');
                
                % Get the central frequency
                obj.CreateSequence(1); % Create the gradient echo sequence for CF
                obj.Sequence2String;
                fname_CF = 'TempBatch_CF.bat';
                obj.WriteBatchFile(fname_CF);
                obj.CenterFrequency(fname_CF);
                obj.SetParameter('LASTFREQUENCY',obj.GetParameter('FREQUENCY'));
                rawData.inputs.rfFrequencyRadial(iBatch) = obj.GetParameter('FREQUENCY');
                
                % Get the appropiate axis
                if(iBatch<nPhaseBatches)
                    obj.gradient1Vector = gradientVectors1((iBatch-1)*obj.maxRepetitionsPerBatch+1:iBatch*obj.maxRepetitionsPerBatch,1);
                    obj.gradient2Vector = gradientVectors1((iBatch-1)*obj.maxRepetitionsPerBatch+1:iBatch*obj.maxRepetitionsPerBatch,2);
                    obj.gradient3Vector = gradientVectors1((iBatch-1)*obj.maxRepetitionsPerBatch+1:iBatch*obj.maxRepetitionsPerBatch,3);
                    obj.lastBatch = 0;
                else
                    obj.gradient1Vector = gradientVectors1((iBatch-1)*obj.maxRepetitionsPerBatch+1:nLPC*nCir,1);
                    obj.gradient2Vector = gradientVectors1((iBatch-1)*obj.maxRepetitionsPerBatch+1:nLPC*nCir,2);
                    obj.gradient3Vector = gradientVectors1((iBatch-1)*obj.maxRepetitionsPerBatch+1:nLPC*nCir,3);
                    obj.lastBatch = 0;
                end
                obj.nPhasesBatch = length(obj.Axis1Vector);
                
                % NOW RUN ACTUAL ACQUISTION
                [cDataBatch,tRadial,status,result] = DataBatch(obj,0,tRadial);
                nPhasesInBatch=length(cDataBatch)/nPPL;
                d = reshape(cDataBatch,nPPL,nPhasesInBatch);
                Data2D = cat(2,Data2D,d);
                count = count+1;
                if nPhaseBatches>1
                    waitbar(count/cn,h_waitbar,sprintf('Radial (por favor espere)... %5.2f %%',100*count/cn));
                end
            end
            if nPhaseBatches>1
                delete(h_waitbar);
            end
            Data2D = Data2D(:);
            cDataCal = permute(ones(nLPC*nCir,nPPL)*diag(cDataCal),[2 1]);
            cDataCal = cDataCal(:);
            Data2Dcal = Data2D-cDataCal;
%             rawData.kSpace.sampled = Data2D;
%             rawData.kSpace.sampledCal = Data2Dcal;
%             save('rawData.mat','rawData');
            
            %% Single point
            % Create waiting bar
            obj.Stage = 2;
            obj.PhasesPerBatch = floor(min([1800/7,16000]));
            nAcquiredPoints = size(gradientVectors2,1);
            nPhaseBatches = ceil(nAcquiredPoints/obj.PhasesPerBatch);
            cn = nPhaseBatches;
            count = 0;
            if nPhaseBatches>1
                h_waitbar = waitbar(count/cn,'Cartesian (por favor espere)...');
            end
            % Start batches sweep
            disp('SINGLE POINT');
            fprintf('points = %i \n',nAcquiredPoints);
            fprintf('nPhaseBatches = %i \n',nPhaseBatches);
            obj.SetParameter('NPOINTS',1);
            repDelay = obj.GetParameter('REPETITIONDELAY');
            obj.SetParameter('REPETITIONDELAY',repDelay+acq_time);
            tCartesian = 0;
            rawData.inputs.rfFrequencySinglePoint = zeros(nPhaseBatches,1);
            for iBatch = 1:nPhaseBatches
                disp('New batch');
                
                % Get the central frequency
                obj.CreateSequence(1); % Create the gradient echo sequence for CF
                obj.Sequence2String;
                fname_CF = 'TempBatch_CF.bat';
                obj.WriteBatchFile(fname_CF);
                obj.CenterFrequency(fname_CF);
                obj.SetParameter('LASTFREQUENCY',obj.GetParameter('FREQUENCY'));
                rawData.inputs.rfFrequencySinglePoint(iBatch) = obj.GetParameter('FREQUENCY');
                
                % Get the appropiate axis
                if(iBatch<nPhaseBatches)
                    obj.Axis1Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,1);
                    obj.Axis2Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,2);
                    obj.Axis3Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:iBatch*obj.PhasesPerBatch,3);
                    obj.lastBatch = 0;
                else
                    obj.Axis1Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:end,1);
                    obj.Axis2Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:end,2);
                    obj.Axis3Vector = gradientVectors2((iBatch-1)*obj.PhasesPerBatch+1:end,3);
                    obj.lastBatch = 1;
                end
                obj.nPhasesBatch = length(obj.Axis1Vector);
                
                % NOW RUN ACTUAL ACQUISTION
                obj.CreateSequence(0);
                obj.Sequence2String;            
                fname = 'TempBatch.bat';
                obj.WriteBatchFile(fname);
                t1 = clock;
                [status,result] = system(fullfile(obj.path_file,fname));        % Run batch
                t2 = clock;
                tCartesian = tCartesian + etime(t2,t1);
                if status == 0 && ~isempty(result)
                    C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                    res = C{:};
                    obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                    obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                    obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                    obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                    obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
                else
                    warndlg('Single Point batch aborted!','Warning')
                    return;
                end
                DataBatch = load(sprintf('%s.txt',obj.OutputFilename));
                cDataBatch = complex(DataBatch(:,1),DataBatch(:,2));
                d = cDataBatch;
                dcal = cDataBatch-cDataCal(1);
                Data2D = cat(1,Data2D,d);
                Data2Dcal = cat(1,Data2Dcal,dcal);
                count = count+1;
                if nPhaseBatches>1
                    waitbar(count/cn,h_waitbar,sprintf('Cartesian (por favor espere)... %5.2f %%',100*count/cn));
                end
            end
            if nPhaseBatches>1
                delete(h_waitbar);
            end
            obj.SetParameter('NPOINTS',nPPL);
            obj.SetParameter('REPETITIONDELAY',repDelay);
            disp(' ');
            kSpaceValuesCal = [kSpaceValues,Data2Dcal(:)];
            kSpaceValues = [kSpaceValues,Data2D(:)];
            tBatch2 = clock;
            tBatch = etime(tBatch2,tBatch1);
            rawData.kSpace.sampled = kSpaceValues;
            rawData.kSpace.sampledCalibrated = kSpaceValuesCal;
            
            %% Create MRIDATA.Data3Dnew
            Data3D = zeros(nPoints(1),nPoints(2),nPoints(3));
            if isfield(MRIDATA,'Data3Dnew')
                if ( isequal(size(MRIDATA.Data3Dnew),[nPPL, nLPC, nCir, obj.NAverages])) || (iAcq == 1)
                    MRIDATA.Data3Dnew = [];
                end
            else
                MRIDATA.Data3Dnew = [];
            end
            
            %% Regridding
            kSpaceValues(:,4) = kSpaceValuesCal(:,4);
            if(nCir>1)
                %[kx,ky,kz] = meshgrid(kCartesian(:,1),kCartesian(:,2),kCartesian(:,3));
                valCartesian = griddata(kSpaceValues(:,1),kSpaceValues(:,2),kSpaceValues(:,3),kSpaceValues(:,4),kCartesian(:,1),kCartesian(:,2),kCartesian(:,3));
            else
                %[kx,ky] = meshgrid(kCartesian(:,1),kCartesian(:,2));
                %kz = zeros(size(kx));
                valCartesian = griddata(kSpaceValues(:,1),kSpaceValues(:,2),kSpaceValues(:,4),kCartesian(:,1),kCartesian(:,2));
            end
            valCartesian(isnan(valCartesian)) = 0;
            rawData.kSpace.interpolated = [kx(:),ky(:),kz(:),valCartesian(:)];
            
            %% Save data
            Data3D = valCartesian;
            MRIDATA.Measurements = kSpaceValues;
            MRIDATA.Data3Dnew(end+1).Fid3D = Data3D;
            if length(obj.cData) ~= prod(nPoints);
                obj.cData = zeros(obj.NAverages,prod(nPoints));
            end
            if obj.averaging
                Nav = obj.NAverages;
            else
                Nav = 1;
            end
            index = mod(iAcq-1,Nav)+1;
            if obj.autoPhasing
                %                    pt = max(Data3D(:,ceil(size(Data3D,2)/2),ceil(size(Data3D,3)/2)));
                pt = max(Data3D(:));
                Data3D = Data3D.*exp(complex(0,-angle(pt)));
            end
            obj.cData(index,1:length(Data3D(:))) = Data3D(:);
            obj.fidData = mean(obj.cData(1:min(iAcq,Nav),:),1);
            obj.fidData = obj.fidData.*exp(complex(0,obj.FIDPhase));
            %    obj.fData1D = fftshift(ifft(fftshift(obj.cDataAv)));
            obj.fftData1D = fliplr(fftshift(fft(obj.fidData)));
            acq_time = obj.GetParameter('ACQUISITIONTIME');
            obj.timeVector = (0:length(obj.fidData)-1)./length(obj.fidData)*acq_time/nLPC;
            obj.freqVector = (ceil(-length(obj.fftData1D)/2):ceil(length(obj.fftData1D)/2)-1)/acq_time/nLPC;
            MRIDATA.obj = obj;
            obj.SetParameter('TRANSIENTTIME',td);
            
            % Save elapsed times to raw data
            elapsedTime = toc;
            rawData.aux.tPrescan = tPrescan;
            rawData.aux.tRadial = tRadial;
            rawData.aux.tCartesian = tCartesian;
            rawData.aux.tBatch = tBatch;
            rawData.aux.elapsedTime = elapsedTime;
            
            % Save rawData
            time = clock;
            name = strcat('rawData-',num2str(time(1)),'.',num2str(time(2)),'.',num2str(time(3)),'.',...
                num2str(time(4)),'.',num2str(time(5)),'.',num2str(time(6)),'.mat');
            save(strcat('C:\Users\josalggui\Documents\repository-mri-lab-2018.11.05\MRI Control\FileDir\',name),'rawData');
        end
    end
    
end

