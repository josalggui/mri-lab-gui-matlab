classdef MRIUtils
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods(Static)
        function freq = GetCentralFrequencyAdjustment(xvectorFFT,fData,fshift)
            idx = find(fData==max(fData),1,'first');
            freq = xvectorFFT(idx)+fshift;
        end
        function str = getCurrentPopupString(hh)
            % getCurrentPopupString returns the currently selected string in the popupmenu with handle hh
            
            % could test input here
            if ~ishandle(hh) || strcmp(get(hh,'Type'),'popupmenu')
                error('getCurrentPopupString needs a handle to a popupmenu as input')
            end
            
            % get the string - do it the readable way
            list = get(hh,'String');
            val = get(hh,'Value');
            if iscell(list)
                str = list{val};
            else
                str = list(val,:);
            end
        end
    end
end