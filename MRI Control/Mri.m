classdef Mri < handle
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    
    properties (SetAccess = private)
        Temp = 310; %Kelvin, normal body temperature
        B0_ = 1*MyUnits.Gauss;
        
        Nx_=10;
        Ny_=10;
        Nz_=10;
        dT_=0;
        Ttotal_=0;
        minStepsPerRotation_=32;
        nSkip_=1; %resampling factor
        
        X_, Y_, Z_;
        rho_;
        T1_; %sec
        T2_; %sec
        Bx_, By_, Bz_, Btot_; %current magnetization (also adjusted to be aligned with Z)
        Bx1_, By1_, Bz1_, Btot1_; %saved mag field
        Mx0_, My0_, Mz0_; %original magnetization
        Mx1_, My1_, Mz1_; %current magnetization
        Mx_, My_, Mz_; %current magnetization (also adjusted for variable mag. field)
        
        FieldIsNotAligned_ = false; %magnetic field is not aligned with Z
        
        
        %For diffusion imaging
        D_;
        Dij_;
        bFactor_ = 0;
        bFij_;
        bD_;
        
        %pulse sequence
        seq_Time_;
        seq_RF_; %
        seq_Gs_; %slice select gradient
        seq_Gp_; %phase gradient
        seq_Gf_; %freq gradient
        seq_Signal_;
        
    end
    
    methods
        function obj = Mri(Nx,Ny,Nz,B0)
            if nargin > 0
                obj.Nx_ = Nx;
                obj.Ny_ = Ny;
                obj.Nz_ = Nz;
                obj.B0_ = B0;
            end
            Nx = obj.Nx_;
            Ny = obj.Ny_;
            Nz = obj.Nz_;
            
            obj.X_ = zeros(Nx,Ny,Nz,'double');
            obj.Y_ = zeros(Nx,Ny,Nz,'double');
            obj.Z_ = zeros(Nx,Ny,Nz,'double');
            
            obj.setRho(ones(size(obj.X_),'double'));
            
            obj.Bx_ = zeros(size(obj.X_),'double');
            obj.By_ = zeros(size(obj.Y_),'double');
            obj.Bz_ = obj.B0_*ones(size(obj.Z_),'double');
            obj.Btot_ = sqrt(obj.Bx_.^2+obj.By_.^2+obj.Bz_.^2);
            
            obj.Mx_ = zeros(size(obj.X_),'double');
            obj.My_ = zeros(size(obj.Y_),'double');
            obj.Mz_ = zeros(size(obj.Z_),'double');
            
            obj.setM0();
            
            obj.D_ = zeros(Nx,Ny,Nz,'double');
            obj.Dij_ = zeros(Nx,Ny,Nz,3,3,'double');
            obj.bD_ = zeros(Nx,Ny,Nz,'double');
            obj.bFij_ = zeros(3,3,'double');

            obj.Ttotal_ = 0;
        end
        function set.Temp(obj,temp)
            obj.Temp = temp;
        end
        function setUniformT1(obj,t1)
            obj.T1_ = t1*ones(obj.Nx_,obj.Ny_,obj.Nz_);
        end
        function setT1(obj,t1)
            obj.T1_ = t1;
        end
        function setUniformT2(obj,t2)
            obj.T2_ = t2*ones(obj.Nx_,obj.Ny_,obj.Nz_);
        end
        function setUniformT2LimitedByT1(obj,t2)
            obj.T2_ = min(t2*ones(obj.Nx_,obj.Ny_,obj.Nz_),obj.T1_);
        end
        function setUniformD(obj,D)
            obj.D_ = D*ones(obj.Nx_,obj.Ny_,obj.Nz_);
        end
        function setUniformDij(obj,Dij)
            obj.Dij_ = Dij*ones(obj.Nx_,obj.Ny_,obj.Nz_,3,3);
        end
        function setT2(obj,t2)
            obj.T2_ = t2;
        end
        function setD(obj,D)
            obj.D_ = D;
        end
        function setDij(obj,Dij)
            obj.Dij_ = Dij;
        end
        function setFieldIsNotAligned(obj,sw)
            obj.FieldIsNotAligned_ = sw;
        end
        function setMinStepsPerRotation(obj,n)
            obj.minStepsPerRotation_ = n;
        end
        function setdT(obj,dt)
            Nsteps = obj.minStepsPerRotation_; % Number of calculation steps per rotation
            if (dt == 0)
                Bmax = max(max(max(obj.Btot_)));
                f=Constants.gammaProtonHz*Bmax;
                obj.dT_ = 1.0/f/Nsteps;
            else
                obj.dT_ = dt;
            end
        end
        function dT = getdT(obj)
            dT = obj.dT_;
        end
        function resetTotalTime(obj)
            obj.Ttotal_=0;
        end
        function T = getTotalTime(obj)
            T = obj.Ttotal_;
        end
        function setNskip(obj,nSkip)
            obj.nSkip_ = nSkip;
        end
        function [Vx,Vy,Vz] = getImageSpaceCube(obj)
            Vx = obj.X_;
            Vy = obj.Y_;
            Vz = obj.Z_;
        end
        function [Vx,Vy,Vz] = setImageSpaceCube(obj,r1,r2)
            %            r1, r2 - coordinates of two diagonal corners of the cube (actual corners, not voxel centers)
            stepX = (r2(1)-r1(1))/obj.Nx_;
            stepY = (r2(2)-r1(2))/obj.Ny_;
            stepZ = (r2(3)-r1(3))/obj.Nz_;
            for i=1:obj.Nx_
                for j=1:obj.Ny_
                    for k=1:obj.Nz_
                        obj.X_(i,j,k) = r1(1)+stepX*(i-0.5);
                        obj.Y_(i,j,k) = r1(2)+stepY*(j-0.5);
                        obj.Z_(i,j,k) = r1(3)+stepZ*(k-0.5);
                    end
                end
            end
            Vx = obj.X_;
            Vy = obj.Y_;
            Vz = obj.Z_;
        end
        function setB(obj,Bx,By,Bz)
            obj.Bx_ = Bx;
            obj.By_ = By;
            obj.Bz_ = Bz;
            obj.Btot_ = sqrt(Bx.^2+By.^2+Bz.^2);
            obj.setM0();
            %we don't know yet how to adjust T1 and T2 so we just use the
            %old values
            obj.T1_ = obj.T1_;
            obj.T2_ = obj.T2_;
        end
        function setUniformB(obj,B)
            Bx = B(1).*ones(size(obj.Bx_));
            By = B(2).*ones(size(obj.By_));
            Bz = B(3).*ones(size(obj.Bz_));
            obj.setB(Bx,By,Bz);
        end
        function addNonUniformLocalB(obj,sigma)
            Bx = random('normal',0,sigma(1),size(obj.Bx_));
            By = random('normal',0,sigma(2),size(obj.By_));
            Bz = random('normal',0,sigma(3),size(obj.Bz_));
            obj.addB(Bx,By,Bz);
        end
        function addB(obj,Bx,By,Bz)
            obj.setB(obj.Bx_ + Bx, obj.By_ + By, obj.Bz_ + Bz);
        end
        function Bmax = getBmax(obj)
            Bmax = max(max(max(obj.Btot_)));
        end
        function B0 = getB0(obj)
            B0 = obj.B0_;
        end
        function setUniformRho(obj,rho)
            obj.rho_ = rho*ones(obj.Nx_,obj.Ny_,obj.Nz_);
        end
        function setRho(obj,rho)
            obj.rho_ = rho;
        end
        function setbFactor(obj,bFactor)
            obj.bFactor_ = bFactor;
        end
        function setbFij(obj,bFij)
            obj.bFij_ = bFij;
            Dij = zeros(3,3);
            for i=1:obj.Nx_
                for j=1:obj.Ny_
                    for k=1:obj.Nz_ 
                        Dij(:,:) = obj.Dij_(i,j,k,:,:);
                        obj.bD_(i,j,k) = -trace(obj.bFij_*Dij);
                    end
                end
            end
        end
        function setImageAtPlaneX(obj,image,pl)
            obj.setRhoImageAtPlaneX(1.0,image,pl);
        end
        function setImageAtPlaneY(obj,image,pl)
            obj.setRhoImageAtPlaneY(1.0,image,pl);
        end
        function setImageAtPlaneZ(obj,image,pl)
            obj.setRhoImageAtPlaneZ(1.0,image,pl);
        end
        function setRhoImageAtPlaneX(obj,rho,image,pl)
            obj.rho_(pl,:,:) = rho*mat2gray(imresize(image,[obj.Ny_ obj.Nz_]));
        end
        function setRhoImageAtPlaneY(obj,rho,image,pl)
            obj.rho_(:,pl,:) = rho*mat2gray(imresize(image,[obj.Nx_ obj.Nz_]));
        end
        function setRhoImageAtPlaneZ(obj,rho,image,pl)
            obj.rho_(:,:,pl) = rho*mat2gray(imresize(image,[obj.Nx_ obj.Ny_]));
        end
        function setT1ImageAtPlane(obj,t1,image,pl)
            minT1fraction = 0.01; %to prevent infinities in block equations
            obj.T1_(:,:,pl) = t1*(mat2gray(imresize(image,[obj.Nx_ obj.Ny_]))+ minT1fraction);
        end
        function setT2ImageAtPlane(obj,t2,image,pl)
            minT2fraction = 0.01; %to prevent infinities in block equations
            obj.T2_(:,:,pl) = t2*(mat2gray(imresize(image,[obj.Nx_ obj.Ny_]))+ minT2fraction);
        end
        function setDImageAtPlane(obj,D,image,pl)
            obj.D_(:,:,pl) = D*(mat2gray(imresize(image,[obj.Nx_ obj.Ny_])));
        end
        function image = getImageAtPlaneX(obj,pl)
            image = squeeze(obj.rho_(pl,:,:));
        end
        function image = getImageAtPlaneY(obj,pl)
            image = squeeze(obj.rho_(:,pl,:));
        end
        function image = getImageAtPlaneZ(obj,pl)
            image = squeeze(obj.rho_(:,:,pl));
        end
        function setRhoAtVoxel(obj,rho,vx)
            obj.rho_(vx(1),vx(2),vx(3)) = rho;
        end
        function setT1AtVoxel(obj,t1,vx)
            obj.T1_(vx(1),vx(2),vx(3)) = t1;
        end
        function setT2AtVoxel(obj,t2,vx)
            obj.T2_(vx(1),vx(2),vx(3)) = t2;
        end
        function setDAtVoxel(obj,D,vx)
            obj.D_(vx(1),vx(2),vx(3)) = D;
        end
        function setDijAtVoxel(obj,Dij,vx)
            obj.Dij_(vx(1),vx(2),vx(3),:,:) = Dij;
        end
               
        function setM0(obj)
            k = Constants.k;
            hBar = Constants.hBar;
            gamma = Constants.gammaProtonRad;
            T = obj.Temp;
            r = (1-exp(-hBar*gamma*obj.Btot_/(k*T)));
            obj.Mx0_ = obj.rho_.*r.*obj.Bx_./obj.Btot_;
            obj.My0_ = obj.rho_.*r.*obj.By_./obj.Btot_;
            obj.Mz0_ = obj.rho_.*r.*obj.Bz_./obj.Btot_;
        end
        function polarize(obj)
            obj.setM0();
            obj.Mx_ = obj.Mx0_;
            obj.My_ = obj.My0_;
            obj.Mz_ = obj.Mz0_;
        end
        function rf90x(obj,B,dB)
            obj.rfx(B,dB,pi/2);
        end
        function rf180x(obj,B,dB)
            obj.rfx(B,dB,pi);
        end
        function rfx(obj,B,dB,angle) % angle is in radians
            Mmax =  max(max(max(max([obj.Mz_ obj.My_ obj.Mz_]))))
            rotm = vrrotvec2mat([1,0,0,angle]);
            for i=1:obj.Nx_
                for j=1:obj.Ny_
                    for k=1:obj.Nz_
                        if (dB == 0)||(abs(B-obj.Btot_(i,j,k)) <= dB)
                            M=[obj.Mx_(i,j,k), obj.My_(i,j,k), obj.Mz_(i,j,k)];
                            M=M*rotm;
                            if abs(M(1)) < abs(Mmax)*1e-10 M(1) = 0; end %to get rid of rounding errors
                            if abs(M(2)) < abs(Mmax)*1e-10 M(2) = 0; end %to get rid of rounding errors
                            if abs(M(3)) < abs(Mmax)*1e-10 M(3) = 0; end %to get rid of rounding errors
                            obj.Mx_(i,j,k) = M(1);
                            obj.My_(i,j,k) = M(2);
                            obj.Mz_(i,j,k) = M(3);
                        end
                    end
                end
            end
            obj.Mx0_ = obj.Mx_;
            obj.My0_ = obj.My_;
            obj.Mz0_ = obj.Mz_;
        end

        function evolve(obj,T)
            obj.evolve_(T,false);
        end
        function Mt = evolveWithSignal(obj,T)
            Mt = obj.evolve_(T,true);
        end
        function d=calculateMinPixelSize(obj,grad)
            d = 1/(pi*obj.T2_*Constants.gammaProton*grad);
        end
        function sigOut = resample(obj,sigIn,nSkip)
            sigOut = sigIn(1,1:nSkip:end);
        end
        function [sigOut,rh] = quadDetector(obj,sigIn,refFreq) % refFreq must be in Hz
            rh = 0;
            phaseShift=2*pi*mod(refFreq*obj.Ttotal_,1);
            sigOutRe = obj.linearDetector(sigIn,refFreq,phaseShift+0);
            sigOutIm = obj.linearDetector(sigIn,refFreq,phaseShift+pi/2);
            fCut = refFreq;
            [sigOutRe,rh,fh] = winsinc(sigOutRe,obj.dT_,refFreq,1000,'welch','low');
            [sigOutIm,rh,fh] = winsinc(sigOutIm,obj.dT_,refFreq,1000,'welch','low');
            sigOutRe = sigOutRe';
            sigOutIm = sigOutIm';
            %              sigOutIm = obj.sincFilter(sigOutIm,2*pi*refFreq/4/2);
            %              sigOutRe = obj.sincFilter(sigOutRe,2*pi*refFreq/4);
            sigOut = sigOutRe + 1i*sigOutIm;
            %            sigOut = obj.sincFilter(sigOut,2*pi*refFreq/4);
        end
        function sigOut = quadDetectorFaux(obj,sig1,sig2) % refFreq must be in Hz
            sigOut = sig1 + 1i*sig2; %cos(t) + i*sin(t) - MATLAB fft likes this
        end
        function sigOut = linearDetector(obj,sigIn,refFreq,shift) % refFreq must be in Hz
            T = [0:size(sigIn,2)-1]*obj.dT_;
            refIn = cos(2*pi*refFreq*T+shift);
            sigOut = sigIn.*refIn;
        end
        function sigOut = sincFilter(obj,sigIn,fCut) %fCut must be in Hz/s
            N = size(sigIn,2);
            T = [0:N-1]*obj.dT_;
            %convolution of signal with sinc function
            sigConv = conv(sigIn,sinc(2*fCut*T)); %sinc is normalized in MATLAB
            sigOut = sigConv(1:N);
        end
    end
    methods (Access = private)
        function NormalizeMagnetization_(obj)
            obj.Bx1_ = obj.Bx_;
            obj.By1_ = obj.By_;
            obj.Bz1_ = obj.Bz_;
            obj.Mx1_ = obj.Mx_;
            obj.My1_ = obj.My_;
            obj.Mz1_ = obj.Mz_;
            for i=1:obj.Nx_
                for j=1:obj.Ny_
                    for k=1:obj.Nz_
                        r = vrrotvec([obj.Bx_(i,j,k),obj.By_(i,j,k),obj.Bz_(i,j,k)],[0,0,1]);
                        rotm = vrrotvec2mat(r);
                        M=[obj.Mx1_(i,j,k), obj.My1_(i,j,k), obj.Mz1_(i,j,k)];
                        B=[obj.Bx1_(i,j,k), obj.By1_(i,j,k), obj.Bz1_(i,j,k)];
                        M=M*rotm';
                        B=B*rotm';
                        obj.Mx_(i,j,k) = M(1);
                        obj.My_(i,j,k) = M(2);
                        obj.Mz_(i,j,k) = M(3);
                        obj.Bx_(i,j,k) = B(1);
                        obj.By_(i,j,k) = B(2);
                        obj.Bz_(i,j,k) = B(3);
                    end
                end
            end
            obj.setM0();
        end
        function [MxSum,MySum,MzSum] = GetMagnetizationSignal_(obj,FieldIsNotAligned)
            %bD = -obj.bFactor_.*obj.D_;
            bD = obj.bD_;
            Sx = obj.Mx_.*exp(bD);
            Sy = obj.My_.*exp(bD);
            Sz = obj.Mz_.*exp(bD);
            if FieldIsNotAligned
                MxSum = 0;
                MySum = 0;
                MzSum = 0;
                for i=1:obj.Nx_
                    for j=1:obj.Ny_
                        for k=1:obj.Nz_
                            r = vrrotvec([0,0,1],[obj.Bx1_(i,j,k),obj.By1_(i,j,k),obj.Bz1_(i,j,k)]);
                            rotm = vrrotvec2mat(r);
                            M=[Sx(i,j,k), Sy(i,j,k), Sz(i,j,k)];
                            M=M*rotm';
                            MxSum = MxSum + M(1);
                            MySum = MySum + M(2);
                            MzSum = MzSum + M(3);
                        end
                    end
                end
            else
                MxSum = sum(sum(sum(Sx)));
                MySum = sum(sum(sum(Sy)));
                MzSum = sum(sum(sum(Sz)));
            end
        end
        function DenormalizeMagnetization_(obj)
            for i=1:obj.Nx_
                for j=1:obj.Ny_
                    for k=1:obj.Nz_
                        r = vrrotvec([0,0,1],[obj.Bx1_(i,j,k),obj.By1_(i,j,k),obj.Bz1_(i,j,k)]);
                        rotm = vrrotvec2mat(r);
                        M=[obj.Mx_(i,j,k), obj.My_(i,j,k), obj.Mz_(i,j,k)];
                        B=[obj.Bx_(i,j,k), obj.By_(i,j,k), obj.Bz_(i,j,k)];
                        M=M*rotm';
                        B=B*rotm';
                        obj.Mx_(i,j,k) = M(1);
                        obj.My_(i,j,k) = M(2);
                        obj.Mz_(i,j,k) = M(3);
                        obj.Bx_(i,j,k) = B(1);
                        obj.By_(i,j,k) = B(2);
                        obj.Bz_(i,j,k) = B(3);
                    end
                end
            end
            obj.setM0();
        end
        function Mt = evolve_(obj,T,withSignal)
            if (T<=0); error('T in MRI.evolve_ must be positive'); end;
            obj.setdT(obj.dT_); %in case it's not yet defined (set to 0);
            N = ceil(T/obj.dT_); % number of time steps.
            Mxt = zeros(1,N);
            Myt = zeros(1,N);
            Mzt = zeros(1,N);
            if obj.FieldIsNotAligned_
                obj.NormalizeMagnetization_();
            end
            for i=1:N
                if withSignal
                    [Mxt(i),Myt(i),Mzt(i)] = obj.GetMagnetizationSignal_(obj.FieldIsNotAligned_);
                end
                obj.BlochRK4_(obj.dT_);
            end
            if obj.FieldIsNotAligned_
                obj.DenormalizeMagnetization_();
            end
            Mt = cat(1,Mxt,Myt,Mzt);
            obj.Ttotal_ = obj.Ttotal_ + T;
        end
        function BlochRK4_(obj,dT)
            gamma = Constants.gammaProtonRad;
            %===================== k1 = f(Yn)
            Mx = obj.Mx_;
            My = obj.My_;
            Mz = obj.Mz_;
            Bx = obj.Bx_;
            By = obj.By_;
            Bz = obj.Bz_;
            
            M0 = obj.Mz0_;
            T1=obj.T1_;
            T2=obj.T2_;
            
            dMx = gamma.*(My.*Bz - Mz.*By) - Mx./T2;
            dMy = -gamma.*(Mx.*Bz - Mz.*Bx) - My./T2;
            dMz = gamma.*(Mx.*By - My.*Bx) - (Mz-M0)./T1;
            
            k1 = cat(4,dMx,dMy,dMz);
            %===================== k2 = f(Yn+k1*dT/2)
            Mx = obj.Mx_+dMx*dT/2;
            My = obj.My_+dMy*dT/2;
            Mz = obj.Mz_+dMz*dT/2;
            
            dMx = gamma.*(My.*Bz - Mz.*By) - Mx./T2;
            dMy = -gamma.*(Mx.*Bz - Mz.*Bx) - My./T2;
            dMz = gamma.*(Mx.*By - My.*Bx) - (Mz-M0)./T1;
            
            k2 = cat(4,dMx,dMy,dMz);
            %===================== k3 = f(Yn+k2*dT/2)
            Mx = obj.Mx_+dMx*dT/2;
            My = obj.My_+dMy*dT/2;
            Mz = obj.Mz_+dMz*dT/2;
            
            dMx = gamma.*(My.*Bz - Mz.*By) - Mx./T2;
            dMy = -gamma.*(Mx.*Bz - Mz.*Bx) - My./T2;
            dMz = gamma.*(Mx.*By - My.*Bx) - (Mz-M0)./T1;
            
            k3 = cat(4,dMx,dMy,dMz);
            %===================== k4 = f(Yn+k3*dT)
            Mx = obj.Mx_+dMx*dT;
            My = obj.My_+dMy*dT;
            Mz = obj.Mz_+dMz*dT;
            
            dMx = gamma.*(My.*Bz - Mz.*By) - Mx./T2;
            dMy = -gamma.*(Mx.*Bz - Mz.*Bx) - My./T2;
            dMz = gamma.*(Mx.*By - My.*Bx) - (Mz-M0)./T1;
            
            k4 = cat(4,dMx,dMy,dMz);
            k = (k1+2*k2+2*k3+k4)/6;
            %k = 2.*k2;
            
            obj.Mx_ = obj.Mx_+k(:,:,:,1)*dT;
            obj.My_ = obj.My_+k(:,:,:,2)*dT;
            obj.Mz_ = obj.Mz_+k(:,:,:,3)*dT;
        end
        function BlochRK6_(obj,dT)
            gamma = Constants.gammaProtonRad;
            %===================== k1 = f(Yn)
            Mx = obj.Mx_;
            My = obj.My_;
            Mz = obj.Mz_;
            Bx = obj.Bx_;
            By = obj.By_;
            Bz = obj.Bz_;
            
            M0 = obj.Mz0_;
            T1=obj.T1_;
            T2=obj.T2_;
            
            dMx = gamma.*(My.*Bz - Mz.*By) - Mx./T2;
            dMy = -gamma.*(Mx.*Bz - Mz.*Bx) - My./T2;
            dMz = gamma.*(Mx.*By - My.*Bx) - (Mz-M0)./T1;
            
            k1 = cat(4,dMx,dMy,dMz);
            %===================== k2 = f(Yn+k1*dT/18)
            Mx = obj.Mx_+dMx*dT/18;
            My = obj.My_+dMy*dT/18;
            Mz = obj.Mz_+dMz*dT/18;
            
            dMx = gamma.*(My.*Bz - Mz.*By) - Mx./T2;
            dMy = -gamma.*(Mx.*Bz - Mz.*Bx) - My./T2;
            dMz = gamma.*(Mx.*By - My.*Bx) - (Mz-M0)./T1;
            
            k2 = cat(4,dMx,dMy,dMz);
            %===================== k3 = f(Yn+(-k1 +3*k2)*dT/12)
            Mx = obj.Mx_+(-k1(:,:,:,1) + 3*dMx)*dT/12;
            My = obj.My_+(-k1(:,:,:,2) + 3*dMy)*dT/12;
            Mz = obj.Mz_+(-k1(:,:,:,3) + 3*dMz)*dT/12;
            
            dMx = gamma.*(My.*Bz - Mz.*By) - Mx./T2;
            dMy = -gamma.*(Mx.*Bz - Mz.*Bx) - My./T2;
            dMz = gamma.*(Mx.*By - My.*Bx) - (Mz-M0)./T1;
            
            k3 = cat(4,dMx,dMy,dMz);
            %===================== k4 = f(Yn+(-2*k1 + 12*k2 + 8*k3)*dT/81)
            Mx = obj.Mx_+(-2*k1(:,:,:,1) + 12*k2(:,:,:,1) + 8*k3(:,:,:,1))*dT/81;
            My = obj.My_+(-2*k1(:,:,:,2) + 12*k2(:,:,:,2) + 8*k3(:,:,:,2))*dT/81;
            Mz = obj.Mz_+(-2*k1(:,:,:,3) + 12*k2(:,:,:,3) + 8*k3(:,:,:,3))*dT/81;
            
            dMx = gamma.*(My.*Bz - Mz.*By) - Mx./T2;
            dMy = -gamma.*(Mx.*Bz - Mz.*Bx) - My./T2;
            dMz = gamma.*(Mx.*By - My.*Bx) - (Mz-M0)./T1;
            
            k4 = cat(4,dMx,dMy,dMz);
            
            %===================== k5 = f(Yn+(40*k1 - 12*k2 - 168*k3 + 162*k4)*dT/33)
            Mx = obj.Mx_+(40*k1(:,:,:,1) - 12*k2(:,:,:,1) - 168*k3(:,:,:,1) + 162*k4(:,:,:,1))*dT/33;
            My = obj.My_+(40*k1(:,:,:,2) - 12*k2(:,:,:,2) - 168*k3(:,:,:,2) + 162*k4(:,:,:,2))*dT/33;
            Mz = obj.Mz_+(40*k1(:,:,:,3) - 12*k2(:,:,:,3) - 168*k3(:,:,:,3) + 162*k4(:,:,:,3))*dT/33;
            
            dMx = gamma.*(My.*Bz - Mz.*By) - Mx./T2;
            dMy = -gamma.*(Mx.*Bz - Mz.*Bx) - My./T2;
            dMz = gamma.*(Mx.*By - My.*Bx) - (Mz-M0)./T1;
            
            k5 = cat(4,dMx,dMy,dMz);

            %===================== k6 = f(Yn+(-8856*k1 + 1728*k2 + 43040*k3 - 36855*k4 + 2695*k5)*dT/1752)
            Mx = obj.Mx_+(-8856*k1(:,:,:,1) + 1728*k2(:,:,:,1) + 43040*k3(:,:,:,1) - 36855*k4(:,:,:,1) + 2695*k5(:,:,:,1))*dT/1752;
            My = obj.My_+(-8856*k1(:,:,:,2) + 1728*k2(:,:,:,2) + 43040*k3(:,:,:,2) - 36855*k4(:,:,:,2) + 2695*k5(:,:,:,2))*dT/1752;
            Mz = obj.Mz_+(-8856*k1(:,:,:,3) + 1728*k2(:,:,:,3) + 43040*k3(:,:,:,3) - 36855*k4(:,:,:,3) + 2695*k5(:,:,:,3))*dT/1752;
            
            dMx = gamma.*(My.*Bz - Mz.*By) - Mx./T2;
            dMy = -gamma.*(Mx.*Bz - Mz.*Bx) - My./T2;
            dMz = gamma.*(Mx.*By - My.*Bx) - (Mz-M0)./T1;
            
            k6 = cat(4,dMx,dMy,dMz);
            
            
            k = (210*k1+0*k2+896*k3+1215*k4+2695*k5+584*k6)/5600;
            %k = 2.*k2;
            
            obj.Mx_ = obj.Mx_+k(:,:,:,1)*dT;
            obj.My_ = obj.My_+k(:,:,:,2)*dT;
            obj.Mz_ = obj.Mz_+k(:,:,:,3)*dT;
        end
    end
end


