classdef MRI_BlankSequence < handle
    %MRI_BlankSequence is a class which has everything common to the rest 
    %of sequences classes.
    %   It defines everything that is present in a generic sequence and the
    %   methods useful for any sequence so that it can be the superclass 
    %   for the rest of sequence classes.
    
    %  Elena Diaz Caballero
    
    properties
        figSeq; % to store the handles for the SequenceDiagram figure
        cData;
        fidData;
        fftData1D;
        timeVector;
        freqVector;
        ProgramDir = 'C:\SpinCore\MRI_SpinAPI\examples';
        path_file;
        ProgramName;
        SequenceName;
        OutputFilename;
        OutputFilename_CF;
        AMPs_bin_file;
        DELAYs_bin_file;
        InputParameters;
        InputParNames;
        InputParValues;
        InputParValuesMin;
        InputParValuesMax;
        InputParUnits;
        InputParHidden; %this parameters are not shown in the input table
        OutputParameters;
        OutputParNames;
        OutputParValues;
        OutputParUnits;
        RF_Shape = 0;
        tx_phase = 0;
        blankingBit = 0;
        adcOffset;
        SliceFreqOffset = 0;
        slice_en = 0;
        phase_en = 0;
        readout_en = 0;
        spoiler_en = 0;
        shimS_en = 0;
        shimP_en = 0;
        shimR_en = 0;
        RunCal = false;
        calibration_en = false;
        CalCheck = false;
        FIDPhase = 0;
        freqFiltering = 200;
        filtering = 0;
        NAverages = 10;
        averaging = 0;
        autoPhasing = 0;
        AbortRun = false;
        ReferenceCF = 0;
        MaxCF_shift=20*MyUnits.kHz;  %Maximum shift in frequency without calibrating
        AMPs =[];
        AMPs_string;
        DACs=[];
        DACs_string;
        WRITEs=[];
        WRITEs_string;
        UPDATEs=[];
        UPDATEs_string;
        CLEARs=[];
        CLEARs_string;
        FREQs=[];
        FREQs_string;
        TXs=[];
        TXs_string;
        PHASEs=[];
        PHASEs_string;
        PhResets=[];
        PhResets_string;
        RXs=[];
        RXs_string;
        ENVELOPEs=[];
        ENVELOPEs_string;
        RFamps=[];
        RFamps_string;
        FLAGs=[];
        FLAGs_string;
        OPCODEs=[];
        OPCODEs_string;
        DELAYs=[];
        DELAYs_string;
        maxInstructions = 2000;
        nInstructions;
        nline_reads;
        Kspace_readout;
        KSpace_phase;
        selectReadout;
        selectPhase;
        selectSlice;
        TE = 0;
        TR = 0;
        TTotal = 0;
        autoFrequency;
        nSteps;
        %% Unused
        %kwid= 3;
        %overg= 6;
        %DCF = [];
    end
    
    methods
        function obj = MRI_BlankSequence
%             CreateOneParameter(obj,'SHIMSLICE','Slice Shimming','',0);
%             CreateOneParameter(obj,'SHIMPHASE','Phase Shimming','',0);
%             CreateOneParameter(obj,'SHIMREADOUT','Readout Shimming','',0);
%             CreateOneParameter(obj,'NSCANS','Number of Scans','',1);
%             CreateOneParameter(obj,'NPOINTS','Number of Points','',600);
%             CreateOneParameter(obj,'NSLICES','Number of Slices','',1);
%             CreateOneParameter(obj,'FREQUENCY','Frequency','MHz',14.2*MyUnits.MHz);
%             CreateOneParameter(obj,'SPECTRALWIDTH','SpectralWidth','kHz',300*MyUnits.kHz);
%             CreateOneParameter(obj,'RFAMPLITUDE','RF Amplitude','',0.3);
%             CreateOneParameter(obj,'PULSETIME','Pulse Time','us',5*MyUnits.us);
%             CreateOneParameter(obj,'BLANKINGDELAY','Blanking Delay','ms',8*MyUnits.us);
%             CreateOneParameter(obj,'TRANSIENTTIME','Transient Time','us',0.1*MyUnits.us);
%             CreateOneParameter(obj,'COILRISETIME','Coil Rise Time','us',100*MyUnits.us);
%             CreateOneParameter(obj,'PHASE','Phase Axis','','y');
%             CreateOneParameter(obj,'READOUT','Readout Axis','','x');
%             CreateOneParameter(obj,'SLICE','Slice Shimming','','z');
%             CreateOneParameter(obj,'SPOILERAMP','Spoiler Amplitude','',0);
%             CreateOneParameter(obj,'SPOILERTIME','Spoiler Time','us',0.1*MyUnits.us);
            obj.InputParameters = {...
                'SHIMSLICE','SHIMPHASE','SHIMREADOUT','NSCANS',...
                'NPOINTS','NSLICES','FREQUENCY','SPECTRALWIDTH',...
                'RFAMPLITUDE','PULSETIME','BLANKINGDELAY','TRANSIENTTIME','COILRISETIME',...
                'PHASE','READOUT','SLICE',...
                'SPOILERAMP', 'SPOILERTIME'...
                };
            obj.InputParHidden = {'SHIMSLICE','SHIMPHASE','SHIMREADOUT'}; %these pars will not be shown in a par table
            obj.InputParNames = containers.Map(obj.InputParameters,...
                {...
                'Slice Shimming','Phase Shimming','Readout Shimming','Number of Scans',...
                'Number of Points', 'Number of Slices', 'Frequency', 'SpectralWidth',...
                'RF Amplitude','Pulse Time','Blanking Delay','Transient Time', 'Coil Rise Time',...
                'Phase Axis','Readout Axis', 'Slice Axis',...
                'Spoiler Amplitude', 'Spoiler Time'...
                });
            obj.InputParValues = containers.Map(obj.InputParameters,...
                {...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,NaN,...
                '','','',...
                NaN,NaN...
                });
            obj.InputParValuesMin = containers.Map(obj.InputParameters,...
                {...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,NaN,...
                '','','',...
                NaN,NaN...
                });
            obj.InputParValuesMax = containers.Map(obj.InputParameters,...
                {...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,...
                NaN,NaN,NaN,NaN,NaN,...
                '','','',...
                NaN,NaN...
                });
            obj.InputParUnits = containers.Map(obj.InputParameters,...
                {...
                '','','','',...
                '','','MHz','kHz',...
                '','us','ms','us','us',...
                '','','',...
                '','us'...
                });
            obj.InputParHidden = {'BLANKINGDELAY'}; % since it's a specification parameter
            obj.OutputParameters = {
                'ADCDECIMATION','ACTUALSPECTRALWIDTH','ACQUISITIONTIME','USERFREQUENCY','LASTFREQUENCY','SNRMEANVALUE'
                };
            obj.OutputParNames = containers.Map(obj.OutputParameters,...
                {...
                'ADC Frequency Decimation', 'Actual Spectral Width', 'Acquisition Time','User RF frequency','Last RF Frequency','SNR Mean Value'...
                });
            obj.OutputParValues = containers.Map(obj.OutputParameters,...
                {...
                NaN,NaN,NaN,NaN,NaN,NaN...
                });
            obj.OutputParUnits = containers.Map(obj.OutputParameters,...
                {...
                '','MHz','ms','MHz','MHz',''...
                });
        end
        function SetDefaultParameters(obj)
            global scanner
            
            obj.adcOffset  = scanner.adcOffset;
            obj.nSteps = scanner.nSteps;
            [pathstr, name, ext] = fileparts(mfilename('fullpath'));
            obj.path_file = fullfile(pathstr,'FileDir'); % this is the path of the folder where all the generated files
            % will be written and read from
            obj.OutputFilename = fullfile(obj.path_file,'FID_temp');
            obj.OutputFilename_CF = fullfile(obj.path_file,'FID_temp_CF');
            obj.AMPs_bin_file = fullfile(obj.path_file,'aux_file_AMPs.bin');
            obj.DELAYs_bin_file = fullfile(obj.path_file,'aux_file_DELAYs.bin');
            obj.SetParameter('SHIMSLICE',scanner.defaultShim(3));
            obj.SetParameter('SHIMPHASE',scanner.defaultShim(2));
            obj.SetParameter('SHIMREADOUT',scanner.defaultShim(1));
            obj.SetParameter('NPOINTS',600);
            obj.SetParameter('NSCANS',1);
            obj.SetParameter('NSLICES',1);
            obj.SetParameter('FREQUENCY',scanner.freq*MyUnits.MHz);
            obj.SetParameter('SPECTRALWIDTH',500*MyUnits.kHz);
            obj.SetParameter('RFAMPLITUDE',scanner.defaultRFampl);
            obj.SetParameter('PULSETIME',3*MyUnits.us);
            obj.SetParameter('BLANKINGDELAY',scanner.BlankingDelay*MyUnits.us); 
            obj.SetParameter('TRANSIENTTIME',scanner.deadTime*MyUnits.us);
            obj.SetParameter('COILRISETIME',100*MyUnits.us);
            obj.SetParameter('PHASE','y');
            obj.SetParameter('READOUT','x');
            obj.SetParameter('SLICE','z');
            obj.SetParameter('SPOILERTIME',0.1*MyUnits.us);
            
            obj.SetParameterMax('RFAMPLITUDE',scanner.max_ampl_RF);

        end
        function SetParameter(obj,par,value)
%             if ischar(value); value = str2num(value);end
            if (isempty(value) || isnan(value)); return; end
            if any(strcmp(obj.InputParameters,par))
                %               fprintf('Parameter %s is set to %f\n',par,value);
                if value < obj.InputParValuesMin(par)
                    obj.InputParValues(par) = obj.InputParValuesMin(par);
                elseif value > obj.InputParValuesMax(par)
                    obj.InputParValues(par) = obj.InputParValuesMax(par);
                else
                    obj.InputParValues(par) = value;
                end
            elseif any(strcmp(obj.OutputParameters,par))
                obj.OutputParValues(par) = value;
            else
                fprintf('Parameter %s is not found\n',par);
            end
        end
        function SetParameterMin(obj,par,value)
            if any(strcmp(obj.InputParameters,par))
                obj.InputParValuesMin(par) = value;
            else
                fprintf('Input Parameter %s is not found\n',par);
            end
        end
        function SetScanParameterValue(obj,par,value)
            obj.SetParameter(par,value)
        end
        function SetParameterMax(obj,par,value)
            if any(strcmp(obj.InputParameters,par))
                obj.InputParValuesMax(par) = value;
            else
                fprintf('Input Parameter %s is not found\n',par);
            end
        end
        function SetParameters(obj,ParValuesMap)
            for k=1:ParValuesMap.length()
                SetParameter(ParValuesMap.keys(k),ParValuesMap.values(k));
            end
        end
        function value = GetParameter(obj,par)
            if any(strcmp(obj.InputParameters,par))
                value = obj.InputParValues(par);
            elseif any(strcmp(obj.OutputParameters,par))
                value = obj.OutputParValues(par);
            else
                value = NaN;
            end
        end
        function value = GetParameterUnit(obj,par)
            if any(strcmp(obj.InputParameters,par))
                value = obj.InputParUnits(par);
            elseif any(strcmp(obj.OutputParameters,par))
                value = obj.OutputParUnits(par);
            else
                value = '';
            end
        end
        function value = GetParameterName(obj,par)
            if any(strcmp(obj.InputParameters,par))
                value = obj.InputParNames(par);
            elseif any(strcmp(obj.OutputParameters,par))
                value = obj.OutputParNames(par);
            else
                value = '';
            end
        end
        function [pars,names,values,units] = GetInputParameters(obj)
            pars = obj.InputParameters;
            names = containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());%Matlab trick to get a new copy of container
            values = containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());
            units = containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());
            for k=1:length(obj.InputParHidden)
                par = obj.InputParHidden{k};
                pars(strcmp(pars,par)) = [];
            end
            names.remove(obj.InputParHidden);
            values.remove(obj.InputParHidden);
            units.remove(obj.InputParHidden);
            
        end
        function [pars,names,values,units] = GetOutputParameters(obj)
            pars = obj.OutputParameters;
            names = obj.OutputParNames;
            values = obj.OutputParValues;
            units = obj.OutputParUnits;
        end
        function sw = ExistsInputParameter(obj,par)
            sw = any(strcmp(obj.InputParameters,par));
        end
        function SetFIDPhase(obj,phase)
            obj.FIDPhase = phase;
        end
        function SetNAverages(obj,n)
            obj.NAverages = n;
        end
        function EnableAveraging(obj,sw)
            obj.averaging = sw;
        end
        function SetFreqFiltering(obj,n)
            obj.freqFiltering = n;
        end
        function EnableFiltering(obj,sw)
            obj.filtering = sw;
        end
        function EnableAutoPhase(obj,sw)
            obj.autoPhasing = sw;
        end
        function EnableAutoFrequency(obj,sw)
            obj.autoFrequency = sw;
        end
        function SetRFShape(obj,shape_type)
            obj.RF_Shape = shape_type;
        end
        function SetShimS(obj,v)
            obj.SetParameter('SHIMSLICE',v);
        end
        function SetShimP(obj,v)
            obj.SetParameter('SHIMPHASE',v);
        end
        function SetShimR(obj,v)
            obj.SetParameter('SHIMREADOUT',v);
        end
        function v = GetShimS(obj)
            v = obj.GetParameter('SHIMSLICE');
        end
        function v = GetShimP(obj)
            v = obj.GetParameter('SHIMPHASE');
        end
        function v = GetShimR(obj)
            v = obj.GetParameter('SHIMREADOUT');
        end
        function EnableSliceGradient(obj,sw)
            obj.slice_en = sw;
        end
        function EnablePhaseGradient(obj,sw)
            obj.phase_en = sw;
        end
        function EnableReadoutGradient(obj,sw)
            obj.readout_en = sw;
        end
        function EnableSpoilerGradient(obj,sw)
            obj.spoiler_en = sw;
        end
        function EnableShimming(obj,sw)
            obj.EnableShimmingSlice(sw);
            obj.EnableShimmingPhase(sw);
            obj.EnableShimmingReadout(sw);
        end
        function EnableShimmingSlice(obj,sw)
            obj.shimS_en = sw;
        end
        function EnableShimmingPhase(obj,sw)
            obj.shimP_en = sw;
        end
        function EnableShimmingReadout(obj,sw)
            obj.shimR_en = sw;
        end
        function EnableCalibration(obj,sw)
%             obj.RunCal = sw;
            obj.calibration_en = sw;
            obj.CalCheck = sw;
        end
        function CalibrateImage(obj,sw)
            obj.calibration_en = sw;
        end

        function [fidData,fftData] = GetData1D(obj)
            fidData = obj.fidData;
            fftData = obj.fftData1D;
        end
        function SaveFID(obj,fname)
            saveFile = fopen(fullfile(obj.path_file,fname),'w');
            for ii = 1:length(obj.timeVector)
                fprintf(saveFile,'%f %f %f\n',obj.timeVector(ii)/MyUnits.us,real(obj.fidData(ii)),imag(obj.fidData(ii)));
            end
            fclose(saveFile);
        end
        function Abort(obj)
            obj.AbortRun = true;
        end
        function [shimS,shimP,shimR]=SelectAxes(obj) %set the appropriate axes as were decided
            if (obj.GetParameter('READOUT')==obj.GetParameter('PHASE'))
              ed = errordlg('Readout and Phase gradient cannot be applied to the same axis','ConfigFileError','modal');
              uiwait(ed);
              return;
            end
            switch num2str(obj.GetParameter('READOUT'))
                case 'x'
                    obj.selectReadout = 2; shimR = obj.GetParameter('SHIMREADOUT');
                case 'y'
                    obj.selectReadout = 0; shimR = obj.GetParameter('SHIMPHASE');
                case 'z'
                    obj.selectReadout = 1; shimR = obj.GetParameter('SHIMSLICE');
                otherwise
                    obj.selectReadout = 2; shimR = obj.GetParameter('SHIMREADOUT');
            end
            switch num2str(obj.GetParameter('PHASE'))
                case 'x'
                    obj.selectPhase = 2; shimP = obj.GetParameter('SHIMREADOUT'); %obj.selectPhase = 0; shimP = obj.GetParameter('SHIMPHASE');
                case 'y'
                    obj.selectPhase = 0; shimP = obj.GetParameter('SHIMPHASE'); %obj.selectPhase = 2; shimP = obj.GetParameter('SHIMREADOUT');
                case 'z'
                    obj.selectPhase = 1; shimP = obj.GetParameter('SHIMSLICE');
                otherwise
                    obj.selectPhase = 0; shimP = obj.GetParameter('SHIMPHASE');
            end
            obj.selectSlice=3-(obj.selectPhase+obj.selectReadout); 
            switch obj.selectSlice;
                case 0
                    shimS = obj.GetParameter('SHIMPHASE');
                case 2
                    shimS = obj.GetParameter('SHIMREADOUT');
                case 1
                    shimS = obj.GetParameter('SHIMSLICE');
                otherwise
                    shimS = obj.GetParameter('SHIMSLICE');
            end
            
            if (obj.shimS_en==0)
                shimS = 0;
            end
            if (obj.shimP_en==0)
                shimP = 0;
            end
            if (obj.shimR_en==0)
                shimR = 0;
            end
        end
        function CenterFrequency(obj,fname_CF)
            % GET CENTER FREQUENCY
            [status,result] = system(fullfile(obj.path_file,fname_CF));

            if status == 0 && ~isempty(result)
                C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                res = C{:};
                obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
            end
            
            DataCF = load(sprintf('%s.txt',obj.OutputFilename_CF));
            cDataCF = complex(DataCF(:,1),DataCF(:,2));

            fftData1D = flipud(fftshift(fft(cDataCF)));
            acq_time = obj.GetParameter('ACQUISITIONTIME');
            freqVector = (ceil(-length(fftData1D)/2):ceil(length(fftData1D)/2)-1)/acq_time;
            idx = find(fftData1D==max(fftData1D),1,'first');
            deltaFreq = freqVector(idx);

            obj.SetParameter('FREQUENCY',obj.GetParameter('FREQUENCY')+deltaFreq);   
            
%             if abs(obj.ReferenceCF - obj.GetParameter('FREQUENCY')) >= obj.MaxCF_shift
%                 load gong.mat;
%                 sound(y, Fs);
%                 obj.ReferenceCF = obj.GetParameter('FREQUENCY');
%                 clear Fs y
%                 h_warn = warndlg(char(strcat('Please tune coil to: [MHz]', num2str( obj.GetParameter('FREQUENCY')/MyUnits.MHz))),'!!! Waiting to Continue !!!');
%                 uiwait(h_warn);
%             end
        end
        function WriteBatchFile(obj,fname)
            % WRITE BATCH FILE AND RUN
            fid = fopen(fullfile(obj.path_file,fname), 'wt');
            pars = obj.InputParameters;
            values = obj.InputParValues;
            fprintf( fid, '@echo off\n');
            fprintf( fid, 'SET Debug=%d\n',0);
            if strcmp(fname(end-6:end-4),'_CF')
                fprintf( fid, 'SET outputFilename=%s\n',obj.OutputFilename_CF);
            else
                fprintf( fid, 'SET outputFilename=%s\n',obj.OutputFilename);
            end
            fprintf( fid, 'SET AMPs_bin_file=%s\n',obj.AMPs_bin_file);
            fprintf( fid, 'SET DELAYs_bin_file=%s\n',obj.DELAYs_bin_file);
            fprintf( fid, 'REM ======================\n');
            fprintf( fid, 'REM Acquisition Parameters\n' );
            fprintf( fid, 'REM ======================\n');
            fprintf( fid, 'SET nPoints=%u\n', values('NPOINTS') );
            fprintf( fid, 'SET nScans=%u\n', values('NSCANS') );
            fprintf( fid, 'SET nline_reads=%u\n', obj.nline_reads);
            fprintf( fid, 'SET spectrometerFrequency_MHz=%f\n', values('FREQUENCY')/MyUnits.MHz);
            fprintf( fid, 'SET sliceFreqOffset_MHz=%f\n', obj.SliceFreqOffset / MyUnits.MHz );
            fprintf( fid, 'SET spectralWidth_kHz=%f\n',values('SPECTRALWIDTH')/MyUnits.kHz);
            
            %             fprintf( fid, 'SET linebroadening_value=%f\n',values('LINEBROADENING'));
            
            fprintf( fid, 'REM ===================\n');
            fprintf( fid, 'REM RF Pulse Parameters\n');
            fprintf( fid, 'REM ===================\n'  );
            fprintf( fid, 'SET RF_Shape=%u\n', obj.RF_Shape);
            fprintf( fid, 'SET amplitude=%f\n', values('RFAMPLITUDE'));
            if isKey(values,'RFAMPLITUDE2')
                fprintf( fid, 'SET amplitude2=%f\n', values('RFAMPLITUDE2'));
            else
                 fprintf( fid, 'SET amplitude2=%f\n', values('RFAMPLITUDE'));
            end
            if isKey(values,'RFAMPLITUDE3')
                fprintf( fid, 'SET amplitude3=%f\n', values('RFAMPLITUDE3'));
            else
                 fprintf( fid, 'SET amplitude3=%f\n', values('RFAMPLITUDE'));
            end
            if isKey(values,'RFAMPLITUDE4')
                fprintf( fid, 'SET amplitude4=%f\n', values('RFAMPLITUDE4'));
            else
                 fprintf( fid, 'SET amplitude4=%f\n', values('RFAMPLITUDE'));
            end
            if isKey(values,'PHASE1')
                fprintf( fid, 'SET phase1=%f\n', values('PHASE1'));
            else
                 fprintf( fid, 'SET phase1=%f\n', 0.0);
            end
            if isKey(values,'PHASE2')
                fprintf( fid, 'SET phase2=%f\n', values('PHASE2'));
            else
                 fprintf( fid, 'SET phase2=%f\n', 90.0);
            end
            if isKey(values,'PHASE3')
                fprintf( fid, 'SET phase3=%f\n', values('PHASE3'));
            else
                 fprintf( fid, 'SET phase3=%f\n', 180.0);
            end
            if isKey(values,'PHASE4')
                fprintf( fid, 'SET phase4=%f\n', values('PHASE4'));
            else
                 fprintf( fid, 'SET phase4=%f\n', 270.0);
            end
            
            fprintf( fid, 'SET pulseTime_us=%f\n', values('PULSETIME')/MyUnits.us);
            
            % Now put in the vectors of the amplitude selections
            fprintf( fid, 'REM ===================\n');
            fprintf( fid, 'REM Gradient Pulse Schema\n');
            fprintf( fid, 'REM ===================\n'  );
%            fprintf( fid, 'SET nPulses=%u', values('NPULSES'));
            fprintf( fid, 'SET nInstructions=%u\n', obj.nInstructions);
            fprintf( fid, 'SET AMPs=%s\n', obj.AMPs_string);
            fprintf( fid, 'SET DACs=%s\n', obj.DACs_string);
            fprintf( fid, 'SET WRITEs=%s\n', obj.WRITEs_string);
            fprintf( fid, 'SET UPDATEs=%s\n', obj.UPDATEs_string);
            fprintf( fid, 'SET CLEARs=%s\n', obj.CLEARs_string);
            fprintf( fid, 'SET FREQs=%s\n', obj.FREQs_string);
            fprintf( fid, 'SET RXs=%s\n', obj.RXs_string);
            fprintf( fid, 'SET TXs=%s\n', obj.TXs_string);
            fprintf( fid, 'SET PHASEs=%s\n', obj.PHASEs_string);
            fprintf( fid, 'SET PhResets=%s\n', obj.PhResets_string);
            fprintf( fid, 'SET DELAYs=%s\n', obj.DELAYs_string);
            fprintf( fid, 'SET ENVELOPEs=%s\n', obj.ENVELOPEs_string);
            fprintf( fid, 'SET RFamps=%s\n', obj.RFamps_string);
            
            fprintf( fid, 'REM ===================\n');
            fprintf( fid, 'REM Flags and configuration\n');
            fprintf( fid, 'REM ===================\n'  );
            fprintf( fid, 'SET FLAGs=%s\n', obj.FLAGs_string);
            fprintf( fid, 'SET OPCODEs=%s\n', obj.OPCODEs_string);
            fprintf( fid, 'SET adcOffset=%u\n', obj.adcOffset);
            
            
            
            fprintf( fid, 'echo. | "%s"',fullfile(obj.ProgramDir,obj.ProgramName)); %we need echo. to exit batch file in case of errors (it emulates typing ENTER)
            fclose(fid);
            
            fid2 = fopen(obj.AMPs_bin_file, 'wb');
            fwrite(fid2,obj.AMPs,'double');
            fclose(fid2);
            fid3 = fopen(obj.DELAYs_bin_file, 'wb');
            fwrite(fid3,obj.DELAYs,'double');
            fclose(fid3);
        end
        function [nSlices,nPhases,readout,slice,phase,spoiler,SSaux] = ConfigureGradients(obj, mode)
            % mode =  "0", normal execution of the sequence,
            %         "1", central frequency adjustment sequence,
            %         "2", calibration of the k-space,  RO slice + encoding gradient ON
            %         "3",                              RO slice + encoding gradient OFF
            %         "4",                              phase slice + encoding gradient ON
            %         "5",                              phase slice + encoding gradient OFF
            
            nSlices = obj.GetParameter('NSLICES');
            
            if (abs(obj.EndPhase - obj.GetParameter('STARTPHASE')) - abs(obj.GetParameter('ENDPHASE') - obj.GetParameter('STARTPHASE'))) > 1e-6 %last batch may be smaller
                nPhases = mod(obj.GetParameter('NPHASES'),obj.PhasesPerBatch);
                obj.EndPhase = obj.GetParameter('ENDPHASE');
            else
                nPhases = min(obj.GetParameter('NPHASES'),obj.PhasesPerBatch);
            end
            
            SSaux=obj.selectSlice;
            spoiler =obj.spoiler_en;
            
            if mode == 1  % central frequency adjustment sequence
                
                nSlices = 1;
                nPhases = 1;
                readout =0; % None of the gradients should be enabled if we are setting the central frequency (CF)
                slice =0;
                phase =0;
                spoiler =0;
                
            elseif mode ==0  % normal execution of the sequence
                
                readout =obj.readout_en;
                slice =obj.slice_en;
                phase =obj.phase_en;
                spoiler =obj.spoiler_en;
                
            elseif mode == 2 % calibration of the k-space,  RO slice + encoding gradient ON
                
                SSaux=obj.selectReadout; % switching the slice select gradient to the corresponding gradient axis
                readout = 1;
                phase   = 0; % encoding gradients in all other axes are switched off
                slice   = 0;
                
            elseif mode == 3 %  RO slice + encoding gradient OFF
                
                SSaux=obj.selectReadout; % switching the slice select gradient to the corresponding gradient axis
                readout = 0;          % all spatial encoding gradients are switched off
                phase   = 0;
                slice   = 0;
                
            elseif mode == 4 % phase slice + encoding gradient ON
                
                SSaux=obj.selectPhase; % switching the slice select gradient to the corresponding gradient axis
                phase   = 1;
                readout = 0; % encoding gradients in all other axes are switched off
                slice   = 0;
                
            elseif mode == 5 % phase slice + encoding gradient OFF
                
                SSaux=obj.selectPhase; % switching the slice select gradient to the corresponding gradient axis
                phase   = 0;        % all spatial encoding gradients are switched off
                readout = 0;
                slice   = 0;
                
            end
            
        end
        function CreateSequence( obj, mode )
            % Function rewritten inside each particular sequence
            
            obj.StartSlice = obj.GetParameter('STARTSLICE');
            obj.EndSlice = obj.GetParameter('ENDSLICE');
            
        end
        function [cDataBatch_Cal]=getCalibrationData(obj,mode,fname_Cal,cDataBatch_Cal)
            obj.CreateSequence(mode);
            obj.Sequence2String;
            obj.WriteBatchFile(fname_Cal);
            [status,result] = system(fullfile(obj.path_file,fname_Cal));
            DataBatch_Cal = load(sprintf('%s.txt',obj.OutputFilename));
            cDataBatch_Cal = [ cDataBatch_Cal ; complex(DataBatch_Cal(:,1),DataBatch_Cal(:,2))];
        end
        
        function Sequence2String(obj)
            if length(obj.AMPs)<1
                return;
            end
            obj.AMPs_string     = num2str(obj.AMPs(1));
            obj.DACs_string     = num2str(obj.DACs(1)); 
            obj.WRITEs_string   = num2str(obj.WRITEs(1));  
            obj.UPDATEs_string  = num2str(obj.UPDATEs(1));   
            obj.CLEARs_string   = num2str(obj.CLEARs(1)); 
            obj.FREQs_string    = num2str(obj.FREQs(1));   
            obj.RXs_string      = num2str(obj.RXs(1)); 
            obj.TXs_string      = num2str(obj.TXs(1));
            obj.PHASEs_string   = num2str(obj.PHASEs(1));
            obj.PhResets_string = num2str(obj.PhResets(1));
            obj.ENVELOPEs_string= num2str(obj.ENVELOPEs(1));
            if isempty (obj.RFamps)
                obj.RFamps_string= '0';
            else
                obj.RFamps_string= num2str(obj.RFamps(1));
            end
            obj.FLAGs_string    = num2str(obj.FLAGs(1));
            obj.OPCODEs_string 	= num2str(obj.OPCODEs(1));
            obj.DELAYs_string   = num2str(obj.DELAYs(1));
            for k = 2:length(obj.AMPs)
                obj.AMPs_string     = char(strcat( obj.AMPs_string, ',', num2str(obj.AMPs(k))));
                obj.DACs_string     = char(strcat( obj.DACs_string, ',', num2str(obj.DACs(k))));
                obj.WRITEs_string   = char(strcat( obj.WRITEs_string, ',', num2str(obj.WRITEs(k))));
                obj.UPDATEs_string  = char(strcat( obj.UPDATEs_string, ',', num2str(obj.UPDATEs(k))));
                obj.CLEARs_string   = char(strcat( obj.CLEARs_string, ',', num2str(obj.CLEARs(k))));
                obj.FREQs_string    = char(strcat( obj.FREQs_string, ',', num2str(obj.FREQs(k))));
                obj.RXs_string      = char(strcat( obj.RXs_string, ',', num2str(obj.RXs(k))));
                obj.TXs_string      = char(strcat( obj.TXs_string, ',', num2str(obj.TXs(k))));
                obj.PHASEs_string   = char(strcat( obj.PHASEs_string, ',', num2str(obj.PHASEs(k))));
                obj.PhResets_string = char(strcat( obj.PhResets_string, ',', num2str(obj.PhResets(k))));
                obj.ENVELOPEs_string= char(strcat( obj.ENVELOPEs_string, ',', num2str(obj.ENVELOPEs(k))));
                if isempty (obj.RFamps)
                    obj.RFamps_string= char(strcat( obj.RFamps_string, ',', '0'));
                else
                    obj.RFamps_string= char(strcat( obj.RFamps_string, ',',  num2str(obj.RFamps(k))));
                end
                obj.FLAGs_string    = char(strcat( obj.FLAGs_string, ',', num2str(obj.FLAGs(k))));
                obj.OPCODEs_string 	= char(strcat( obj.OPCODEs_string, ',', num2str(obj.OPCODEs(k))));
                obj.DELAYs_string   = char(strcat( obj.DELAYs_string, ',', num2str(obj.DELAYs(k))));
            end
        end
        
        function UpdateTETR( obj )
            obj.TE = 0;
            obj.TR = 0;
            obj.TTotal = 0;
        end
        
        function SetFigureHandle(obj,h)
            obj.figSeq = h(1);
        end
        
        function plot_diagram(obj,ninstr_ini, ninstr_fin)
            
            % To plot the diagram of the sequence from the instruction
            % ninstr_ini to ninstr_fin
            
            PP=obj.selectPhase;
            RR=obj.selectReadout;
            SS=obj.selectSlice;
            
            ninstr_repr=ninstr_fin-ninstr_ini+1; %number of represented instructions
            [blanking_bit,RF_out,slice_grad,phase_grad,readout_grad,acquisition,time]=deal(zeros(2*ninstr_repr,1));
            
            n=0; %Initializing the counter used to fill the vectors
            for ninstr=ninstr_ini:ninstr_fin
                n=n+1;
                if (obj.WRITEs(ninstr)==1 && obj.UPDATEs(ninstr)==1)
                    if (obj.DACs(ninstr)== SS)
                        slice_grad(2*n-1)= obj.AMPs(ninstr);
                        if (n>1)
                            phase_grad(2*n-1)= phase_grad(2*n-3); % The other gradients don't change
                            readout_grad(2*n-1)= readout_grad(2*n-3);
                        end
                    elseif (obj.DACs(ninstr)== PP)
                        phase_grad(2*n-1)= obj.AMPs(ninstr);
                        if (n>1)
                            slice_grad(2*n-1)= slice_grad(2*n-3); % The other gradients don't change
                            readout_grad(2*n-1)= readout_grad(2*n-3);
                        end
                    elseif (obj.DACs(ninstr)== RR)
                        readout_grad(2*n-1)= obj.AMPs(ninstr);
                        if (n>1)
                            phase_grad(2*n-1)= phase_grad(2*n-3); % The other gradients don't change
                            slice_grad(2*n-1)= slice_grad(2*n-3);
                        end
                    end
                end
                
                if (obj.WRITEs(ninstr)==0 && obj.CLEARs(ninstr)==0)
                    if (n>1)
                            phase_grad(2*n-1)= phase_grad(2*n-3); % All the gradients remain the same
                            readout_grad(2*n-1)= readout_grad(2*n-3);
                            slice_grad(2*n-1)= slice_grad(2*n-3);
                    end
                end
%                 if (obj.WRITEs(ninstr)==1)
%                 end
                
                if (obj.TXs(ninstr) == 1)
                    RF_out(2*n-1)=1;
                    slice_grad(2*n-1)= slice_grad(2*n-3);
                end
                if (obj.RXs(ninstr) == 1)
                    acquisition(2*n-1)=1;
                end
                if (obj.FLAGs(ninstr) == 1)
                    blanking_bit(2*n-1)=1;
                end
                if (obj.DACs(ninstr) == 3 && obj.CLEARs(ninstr) == 1)
                    slice_grad(2*n-1)= 0;
                    phase_grad(2*n-1)= 0;
                    readout_grad(2*n-1)= 0;
                end
                
                time(2*n)= time(2*n-1)+obj.DELAYs(ninstr);
                if (2*n+1 < 2*ninstr_repr)
                    time(2*n+1)=time(2*n)+0.1;
                end
            end
            
            blanking_bit(2:2:end) = blanking_bit(1:2:end-1);
            RF_out(2:2:end) = RF_out(1:2:end-1);
            slice_grad(2:2:end) = slice_grad(1:2:end-1);
            phase_grad(2:2:end) = phase_grad(1:2:end-1);
            readout_grad(2:2:end) = readout_grad(1:2:end-1);
            acquisition(2:2:end) = acquisition(1:2:end-1);
            
            figure(obj.figSeq);
%             set(obj.figSeq,'defaultLineLineWidth',3,'defaultAxesFontUnits','normalized','defaultAxesFontsize', 0.22, 'defaultAxesYlim',[-1 1])
            set(obj.figSeq,'defaultLineLineWidth',3,'defaultAxesFontUnits','normalized','defaultAxesFontsize', 0.22) % Removing the axes y limint, so that it is autoscaled
            set(obj.figSeq,'numbertitle','off','name','Sequence Diagram')
            subplot(6,1,1)
            plot(time,blanking_bit)
            ylabel({'Blanking'; 'bit'})
            grid
            
            subplot(6,1,2)
            plot(time,RF_out)
            ylabel('RF out')
            grid
            
            subplot(6,1,3)
            plot(time,slice_grad)
            ylabel({'Slice'; 'gradient'})
            grid
            
            subplot(6,1,4)
            plot(time,phase_grad)
            ylabel({'Phase'; 'gradient'})
            grid
            
            subplot(6,1,5)
            plot(time,readout_grad)
            ylabel({'Readout'; 'gradient'})
            grid
            
            subplot(6,1,6)
            plot(time,acquisition)
            ylabel('Acquisition')
            xlabel('Time (\mus)')
            grid
            
        end

        function PerformFiltering(obj)
            % 2018.05.10
            % Filtering ADC noise

            fs=obj.GetParameter('SPECTRALWIDTH');
            fc= obj.freqFiltering; %in kHz
            fNorm= fc*1e3/(fs/2);
            [b,a]=butter(6,fNorm,'low');
            fid_low=filtfilt(b,a,obj.fidData);
            fftData1D_low = fliplr(fftshift(fft(fid_low)));
            obj.fidData = fid_low;
            obj.fftData1D = fftData1D_low;
        end
        
        function WriteCalibBatchFile(obj,fname)
            % WRITE BATCH FILE AND RUN            
            fid = fopen(fullfile(obj.path_file,fname), 'Wt');
            
            [~, ~, ~, ~, ~, total_measure_time] = obj.CalcTiming;
            
            
            % READ SPEED MULTIPLE
            obj.rdspeed_mult = 10;
            RdSpeed = (40E6) / obj.rdspeed_mult;
            
            % calculate how many points should be measured per acq
            obj.NP_calib = ceil( total_measure_time * RdSpeed / 32 ) * 32;
            
            % check if number of points is too many
            maxNP = (512E6)/2;
            if maxNP < (obj.NP_calib * 4 * 20 )
               obj.rdspeed_mult = 20;
               RdSpeed = (40E6) / obj.rdspeed_mult;

               % calculate how many points should be measured per acq
               obj.NP_calib = ceil( total_measure_time * RdSpeed / 32 ) * 32;
            end
                
            if maxNP < (obj.NP_calib * 4 * 20 )
               obj.rdspeed_mult = 40;
               RdSpeed = (40E6) / obj.rdspeed_mult;

               % calculate how many points should be measured per acq
               obj.NP_calib = ceil( total_measure_time * RdSpeed / 32 ) * 32;
            end
            
            if maxNP < (obj.NP_calib * 4 * 20 )
               obj.rdspeed_mult = 80;
               RdSpeed = (40E6) / obj.rdspeed_mult;

               % calculate how many points should be measured per acq
               obj.NP_calib = ceil( total_measure_time * RdSpeed / 32 ) * 32;
            end
            
            fprintf( fid, '@echo off\n');
            fprintf( fid, 'SET Debug=%d\n',0);
            fprintf( fid, char(['SET DACfilename=',strrep(obj.path_file,'\','\\\\'),'\\\\ADCdatafile\n']));
            fprintf( fid, 'SET nPointsCalib=%u\n', obj.NP_calib );
            fprintf( fid, 'SET rdSpeed=%u\n', obj.rdspeed_mult );
            fprintf( fid, 'SET nTrigs=%u\n', obj.PhasesinThisBatch );
            fprintf( fid, 'echo. | "%s"','C:\ADLINK\ryan\VisualStudioProjects\MultiTrigger\x64\Debug\MultiTrigger.exe'); 
            fprintf( fid, '\nexit');
            
            fclose(fid);
        end
        
        function MoveParameter(obj,par1,par2)
            % It switches the parameter at position x to position y in the
            % GUI
            % Update: now par1 and par2 can be numeric or string
            if(isnumeric([par1 par2]))
                x = par1;
                y = par2;
            else
                x = find(strcmp(obj.InputParameters,par1));
                y = find(strcmp(obj.InputParameters,par2));
            end
            xx = obj.InputParameters(x);
            output = obj.InputParameters;
            output(x) = [];
            output(y+1:end+1) = output(y:end);
            output(y) = xx;
            obj.InputParameters = output;
        end
        
        function CreateOneParameter(obj,id,label,u,defaultvalue)
            %% Hints
            % This method creates additional input parameter
            % Inputs:
            %   - id: parameter identifier
            %   - label: parameter label
            %   - u: parameter units
            %   - defaultvalue: parameter value by default
            % Outputs:
            %   - No ouptut
            
            %% Code goes here:
            iParameters = {id};
            obj.InputParNames = [containers.Map(obj.InputParNames.keys(),obj.InputParNames.values());containers.Map(iParameters,{label})];
            obj.InputParValues = [containers.Map(obj.InputParValues.keys(),obj.InputParValues.values());containers.Map(iParameters,{NaN})];
            obj.InputParValuesMin = [containers.Map(obj.InputParValuesMin.keys(),obj.InputParValuesMin.values());containers.Map(iParameters,{NaN})];
            obj.InputParValuesMax = [containers.Map(obj.InputParValuesMax.keys(),obj.InputParValuesMax.values());containers.Map(iParameters,{NaN})];
            obj.InputParUnits = [containers.Map(obj.InputParUnits.keys(),obj.InputParUnits.values());containers.Map(iParameters,{u})];
            
            for k=1:length(obj.InputParameters)
                iParameters(strcmp(obj.InputParameters{k},iParameters)) = [];
            end
            obj.InputParameters = [obj.InputParameters, iParameters];
            SetParameter(obj,id,defaultvalue);

        end
        
        function x = CreatePulse(obj,iIns,phase,blk,trf,td)
            %% Hints
            % This method create one RF pulse.
            % Inputs:
            %   - iIns: first instruction position
            %   - phase: phase register
            %   - blk: blanking delay
            %   - trf: pulse time
            %   - td: dead time after the rf pulse
            % Outputs:
            %   - x: last instruction position
            
            %% Code goes here:
            nIns = 3;
            vIns =  iIns: (nIns+iIns-1);
            
            [~,~,~,slice,~,~,~] = obj.ConfigureGradients(0);
            if (obj.RF_Shape)  
                env = 0;
            else  
                env = 7;
            end
            
            obj.AMPs(vIns)     = [0 0 0];
            obj.DACs(vIns)     = [0 0 0];
            obj.WRITEs(vIns)   = [0 0 0];
            obj.UPDATEs(vIns)  = [0 0 0];
            obj.CLEARs(vIns)   = [0 0 0];
            obj.FREQs(vIns)    = [0 slice 0];
            obj.RXs(vIns)      = [0 0 0];
            obj.TXs(vIns)      = [0 1 0];
            obj.PHASEs(vIns)   = [0 phase 0];
            obj.PhResets(vIns) = [1 0 0]; 
            obj.DELAYs(vIns)   = [blk trf td];
            obj.ENVELOPEs(vIns)= [7 env 7];
            obj.RFamps(vIns)   = [0 0 0];
            obj.FLAGs(vIns)    = [1 1 0];
            obj.OPCODEs(vIns)  = [0 0 0];
            
            x = iIns + nIns;
        end
        
        function x = CreateRawPulse(obj,iIns,phase,trf,amp)
            %% Hints
            % This method create one RF pulse.
            % Inputs:
            %   - iIns: first instruction position
            %   - phase: phase register
            %   - trf: pulse time
            %   - amp: amplitude register
            % Outputs:
            %   - x: last instruction position
            
            %% Code goes here:
            nIns = 1;
            vIns =  iIns: (nIns+iIns-1);
            
            [~,~,~,slice,~,~,~] = obj.ConfigureGradients(0);
            if (obj.RF_Shape)  
                env = 0;
            else  
                env = 7;
            end
            
            obj.AMPs(vIns)     = 0;
            obj.DACs(vIns)     = 0;
            obj.WRITEs(vIns)   = 0;
            obj.UPDATEs(vIns)  = 0;
            obj.CLEARs(vIns)   = 0;
            obj.FREQs(vIns)    = slice;
            obj.RXs(vIns)      = 0;
            obj.TXs(vIns)      = 1;
            obj.PHASEs(vIns)   = phase;
            obj.PhResets(vIns) = 0; 
            obj.DELAYs(vIns)   = trf;
            obj.ENVELOPEs(vIns)= env;
            obj.RFamps(vIns)   = amp;
            obj.FLAGs(vIns)    = 1;
            obj.OPCODEs(vIns)  = 0;
            
            x = iIns + nIns;
        end
        
        function x = SetBlanking(obj,iIns,blk,blkTime)
            %% Hints
            % This method set the blk that we select to ON.
            
            %% Code goes here:
            nIns = 1;
            vIns =  iIns: (nIns+iIns-1);
            
            obj.AMPs(vIns)     = 0;
            obj.DACs(vIns)     = 0;
            obj.WRITEs(vIns)   = 0;
            obj.UPDATEs(vIns)  = 0;
            obj.CLEARs(vIns)   = 0;
            obj.FREQs(vIns)    = 0;
            obj.RXs(vIns)      = 0;
            obj.TXs(vIns)      = 0;
            obj.PHASEs(vIns)   = 0;
            obj.PhResets(vIns) = 1; 
            obj.DELAYs(vIns)   = blkTime;
            obj.ENVELOPEs(vIns)= 7;
            obj.RFamps(vIns)   = 0;
            obj.FLAGs(vIns)    = blk;
            obj.OPCODEs(vIns)  = 0;
            
            x = iIns + nIns;
        end
        
        function x = PulseGradient(obj,iIns,gradamp,dac,gradtime)
        %%Hints
            % This method switches on gradient
            % Inputs:
            %   - iIns: first instruction position
            %   - gradamp: Gradient amplitude in arbitrary units
            %   - dac: gradient which is modified
            %   - gradtime: gradient aplication time
            % Outputs:
            %   - x: last instruction position
        
        %%Code goes here:
                nIns = 1;
                vIns =  iIns: (nIns+iIns-1); 
                
                obj.AMPs(vIns)     = gradamp;
                obj.DACs(vIns)     = dac;
                obj.WRITEs(vIns)   = 1;
                obj.UPDATEs(vIns)  = 1;
                obj.CLEARs(vIns)   = 0;
                obj.FREQs(vIns)    = 0;
                obj.RXs(vIns)      = 0;
                obj.TXs(vIns)      = 0;
                obj.PHASEs(vIns)   = 0;
                obj.PhResets(vIns) = 0;
                obj.DELAYs(vIns)   = gradtime;
                obj.ENVELOPEs(vIns)= 7;
                obj.RFamps(vIns)   = 0;
                obj.FLAGs(vIns)    = 0;
                obj.OPCODEs(vIns)  = 0;
                    
                x = iIns + nIns;
        end
        
        function x = RampGradient(obj,iIns,g0,g1,dac,riseTime,riseSteps)
        %%Hints
            % This method switches on gradient
            % Inputs:
            %   - iIns: first instruction position
            %   - g0: initial gradient vector
            %   - g1: final gradient vector
            %   - dac: dac vector
            %   - riseTime: rise time
            %   - riseSteps: number of steps for the ramp
            % Outputs:
            %   - x: last instruction position
        
        %% Code goes here:
            if length(g0)==1
                for ii = 1:riseSteps
                    iIns = obj.PulseGradient(iIns,g0(1)+(g1(1)-g0(1))/riseSteps*ii,dac(1),riseTime/riseSteps);
                end
            elseif length(g0)==2
                for ii = 1:riseSteps
                    iIns = obj.PulseGradient(iIns,g0(1)+(g1(1)-g0(1))/riseSteps*ii,dac(1),0.1);
                    iIns = obj.PulseGradient(iIns,g0(2)+(g1(2)-g0(2))/riseSteps*ii,dac(2),riseTime/riseSteps-0.1);
                end
            elseif length(g0)==3
                for ii = 1:riseSteps
                    iIns = obj.PulseGradient(iIns,g0(1)+(g1(1)-g0(1))/riseSteps*ii,dac(1),0.1);
                    iIns = obj.PulseGradient(iIns,g0(2)+(g1(2)-g0(2))/riseSteps*ii,dac(2),0.1);
                    iIns = obj.PulseGradient(iIns,g0(3)+(g1(3)-g0(3))/riseSteps*ii,dac(3),riseTime/riseSteps-0.2);
                end
            end
            x = iIns;
        end
                
        function x = Acquisition(obj,iIns,acq_time)
            %% Hints
            % This method executes adquisition order
            % Inputs:
            %   - iIns: first instruction position
            %   - acq_time: adquisition time
            % Outputs:
            %   - x: last instruction position
            
            %% Code goes here:
            nIns = 1;
            vIns =  iIns: (nIns+iIns-1);
            obj.AMPs(vIns)     = 0;
            obj.DACs(vIns)     = 0;
            obj.WRITEs(vIns)   = 0;
            obj.UPDATEs(vIns)  = 0;
            obj.CLEARs(vIns)   = 0;
            obj.FREQs(vIns)    = 0;
            obj.RXs(vIns)  = 1;
            obj.TXs(vIns)      = 0;
            obj.PHASEs(vIns)   = 0;
            obj.PhResets(vIns) = 0;
            obj.DELAYs(vIns)   = acq_time;
            obj.ENVELOPEs(vIns)= 7;
            obj.RFamps(vIns)   = 0;
            obj.FLAGs(vIns)    = 0;
            obj.OPCODEs(vIns)  = 0;
            
            x = iIns + nIns;
        end
        
        function x = RepetitionDelay (obj,iIns,bool,delay)
            %% Hints
            % This method executes repetition delay
            % Inputs:
            %   - iIns: first instruction position
            %   - bool:
            %         - 0 -> delay while the system remains in the same state
            %         - 1 -> delay and switch the system off
            %   - delay: delay time
            % Outputs:
            %   - x: last instruction position
            
            %%Code goes here:
            nIns = 1;
            vIns =  iIns: (nIns+iIns-1);
            if (bool == 0)
                obj.AMPs(vIns)     = 0;
                obj.DACs(vIns)     = 0;
                obj.WRITEs(vIns)   = 0;
                obj.UPDATEs(vIns)  = 0;
                obj.CLEARs(vIns)   = 0;
                obj.FREQs(vIns)    = 0;
                obj.RXs(vIns)      = 0;
                obj.TXs(vIns)      = 0;
                obj.PHASEs(vIns)   = 0;
                obj.PhResets(vIns) = 0;
                obj.DELAYs(vIns)   = delay;
                obj.ENVELOPEs(vIns)= 7;
                obj.RFamps(vIns)   = 0;
                obj.FLAGs(vIns)    = 0;
                obj.OPCODEs(vIns)  = 0;
                
            elseif (bool == 1)
                obj.AMPs(vIns)     = 0;
                obj.DACs(vIns)     = 3;
                obj.WRITEs(vIns)   = 1;
                obj.UPDATEs(vIns)  = 1;
                obj.CLEARs(vIns)   = 1;
                obj.FREQs(vIns)    = 0;
                obj.RXs(vIns)      = 0;
                obj.TXs(vIns)      = 0;
                obj.PHASEs(vIns)   = 0;
                obj.PhResets(vIns) = 0;
                obj.DELAYs(vIns)   = delay;
                obj.ENVELOPEs(vIns)= 7;
                obj.RFamps(vIns)   = 0;
                obj.FLAGs(vIns)    = 0;
                obj.OPCODEs(vIns)  = 0;
                
            else
                warning('Error at RepetitionDelay method. Bool parameter only accepts 0 or 1')
            end
            x = iIns + nIns;
            
        end
        
        function c = ReorganizeCalibrationData(obj)
            global scanner
            c = scanner.GradientConstants;      
            
            readout = obj.GetParameter('READOUT');
            phase = obj.GetParameter('PHASE');
            slice = obj.GetParameter('SLICE');
            
            if(readout=='x');       readout = 1;
            elseif(readout=='y');   readout = 2;
            elseif(readout=='z');   readout = 3;
            end
            if(phase=='x');         phase = 1;
            elseif(phase=='y');     phase = 2;
            elseif(phase=='z');     phase = 3;
            end
            if(slice=='x');         slice = 1;
            elseif(slice=='y');     slice = 2;
            elseif(slice=='z');     slice = 3;
            end
            c = c([readout phase slice]); 
        end
        
        function bwCalNormVal = calculate_bwCalNormVal(obj,varargin)
            global scanner
            
            % Load amplitude calibration for 1V
            bwCalVal = scanner.bwCal.outputs.fidMean;
            bwCalVec = logspace(log10(scanner.bwCal.inputs.bwMin),...
                log10(scanner.bwCal.inputs.bwMax),...
                scanner.bwCal.inputs.steps);
            
            % Get value to normalize fid to Volts
            if size(varargin)==0
                [~,bwCalNormPos] = min(abs(bwCalVec- obj.GetParameter('SPECTRALWIDTH')));
            elseif size(varargin)==1
                [~,bwCalNormPos] = min(abs(bwCalVec- varargin{1}));
            else
                warndlg('calculate_bwCalNormVal function accepts maximum one bw as input');
            end
            bwCalNormVal = bwCalVal(bwCalNormPos);
        end
        function CreateSequenceFID(obj)
            %% Help:
            % This function creates an FID sequence with no gradients
            
            %% Code goes here:
            
            % Select axis for shimming
            [shimS,shimP,shimR] = SelectAxes(obj);
            
            % Initialize all vectors to zero with the appropriate size
            obj.nline_reads = 1;
            obj.nInstructions = 8;
            [obj.AMPs,obj.DACs,obj.WRITEs,obj.UPDATEs,obj.CLEARs,obj.FREQs,obj.TXs,obj.PHASEs,obj.PhResets,obj.RXs,obj.ENVELOPEs,obj.FLAGs,obj.OPCODEs,obj.DELAYs] = deal(zeros(obj.nInstructions,1));
            
            % Get sequence parameter
            acquisitionTime = obj.GetParameter('NPOINTS')/obj.GetParameter('SPECTRALWIDTH')*1e6;
            pulseTime = obj.GetParameter('PULSETIME')*1e6;
            repetitionDelay = obj.GetParameter('REPETITIONDELAY')*1e6;
            
            if isnan(repetitionDelay)
                repetitionDelay=50000;
            end
            
            deadTime = obj.GetParameter('TRANSIENTTIME')*1e6;
            
            % Create pulse
            rAmp = shimR;
            pAmp = shimP;
            sAmp = shimS;
            iIns = 1;
            iIns = obj.PulseGradient(iIns,rAmp,obj.selectReadout,0.1);
            iIns = obj.PulseGradient(iIns,pAmp,obj.selectPhase,0.1);
            iIns = obj.PulseGradient(iIns,sAmp,obj.selectSlice,0.1);
            iIns = obj.CreatePulse(iIns,0,8,pulseTime,deadTime);
            iIns = obj.Acquisition(iIns,acquisitionTime);
            iIns = obj.RepetitionDelay(iIns,1,repetitionDelay);
        end
        
        function freq = GetFIDParameters(obj)
            %% Help:
            % This function does an analysis of the FID withoug gradients
            % to get the central frequency as well as the T2 in microseconds
            
            %% Code goes here:
            global scanner
            
            % Get values
            nPoints = obj.GetParameter('NPOINTS');
            bw = obj.GetParameter('SPECTRALWIDTH');
            nex = obj.GetParameter('NSCANS');
            freqOffset = obj.SliceFreqOffset;
            
            % Set specific parameters           
            if strcmp(scanner.name, 'HISTO')
                obj.SetParameter('NPOINTS',10*nPoints);
                obj.SetParameter('SPECTRALWIDTH',bw*2);
% NOTE: it does not make sense to use bw = 1 kHz, this is 1 point per 1 ms.
% Most of our signals are not longer than a few ms. So 1 kHz means that
% only a few points (<10 points) have information to get the central
% frequency. Points far away from 3�T2* does not contribute to the central
% frequency acquisition.
            elseif strcmp(scanner.name, 'Dental')
                obj.SetParameter('NPOINTS',600);
                obj.SetParameter('SPECTRALWIDTH',2*MyUnits.kHz);
            end
            obj.SliceFreqOffset = 0;
            obj.SetParameter('NSCANS',1);
            
            % Reset parameters Run
            obj.CreateSequenceFID;
            obj.Sequence2String;
            fname = 'TempBatch.bat';
            obj.WriteBatchFile(fname);
            [status,result] = system(fullfile(obj.path_file,fname));
            if status == 0 && ~isempty(result)
                C = textscan(result, '%s', 'delimiter', sprintf('\f'));
                res = C{:};
                obj.SetParameter('ADCDECIMATION',sscanf(res{1},'Decimation: %d'));
                obj.SetParameter('ACTUALSPECTRALWIDTH',sscanf(res{2},'Actual Spectral Width: %f Hz')); %Actual Spectral Width
                obj.SetParameter('ACQUISITIONTIME',sscanf(res{3},'Acquisition Time: %f ms')*MyUnits.ms); %Acquisition Time
                obj.SetParameter('DEPHASINGREADOUTAMPLITUDE',sscanf(res{4},'Dephasing readout amplitude: %f'));
                obj.SetParameter('PHASINGREADOUTAMPLITUDE',sscanf(res{5},'Phasing readout amplitude: %f'));
            else
                warndlg('Batch aborted!','Warning')
                return;
            end
            dataBatch = load(sprintf('%s.txt',obj.OutputFilename));
            cDataBatch = complex(dataBatch(:,1),dataBatch(:,2));
            
            obj.SetParameter('NPOINTS',nPoints);
            obj.SetParameter('SPECTRALWIDTH',bw);
            obj.SetParameter('NSCANS',nex);
            obj.SliceFreqOffset = freqOffset;
            
            % Get central frequency
            fftData1D = flipud(fftshift(fft(cDataBatch)));
            acq_time = obj.GetParameter('ACQUISITIONTIME');
            freqVector = (ceil(-length(fftData1D)/2):ceil(length(fftData1D)/2)-1)/acq_time;
            idx = find(fftData1D==max(fftData1D),1,'first');
            deltaFreq = freqVector(idx);
            freq = obj.GetParameter('FREQUENCY')+deltaFreq;
            
            
%             if abs(obj.ReferenceCF - freq) >= obj.MaxCF_shift
%                 load gong.mat;
%                 sound(y, Fs);
%                 obj.ReferenceCF = obj.GetParameter('FREQUENCY');
%                 clear Fs y
%                 h_warn = warndlg(char(strcat('Please tune coil to: [MHz]', num2str( obj.GetParameter('FREQUENCY')/MyUnits.MHz))),'!!! Waiting to Continue !!!');
%                 uiwait(h_warn);
%             end
        end
        
        function x = SpinLockingPulse(obj,iIns,phase0,phase1,blk,trf,tspinlock)
            %% Hints
            % This method create one RF pulse.
            % Inputs:
            %   - iIns: first instruction position
            %   - phase: phase register
            %   - blk: blanking delay
            %   - trf: pulse time
            %   - td: dead time after the rf pulse
            % Outputs:
            %   - x: last instruction position
            
            %% Code goes here:
%             nIns = 3;
%             vIns =  iIns: (nIns+iIns-1);
            
            iIns = obj.SetBlanking(iIns,1,blk);
            iIns = obj.CreateRawPulse(iIns,phase0,trf,0);
            iIns = obj.CreateRawPulse(iIns,phase1,tspinlock,1);
            
%             [~,~,~,slice,~,~,~] = obj.ConfigureGradients(0);
%             obj.RFamps(vIns)        = [0 0 1];
%             obj.FREQs( vIns )       = [0 slice 0];
%             obj.FLAGs(vIns)         = [1 1 1];
%             obj.TXs(vIns)           = [0 1 1];
%             obj.ENVELOPEs(vIns)     = [7 7 7];
%             obj.PHASEs(vIns)        = [0 phase0 phase1];
%             obj.PhResets(vIns)      = [1 0 0];
%             obj.DELAYs(vIns)        = [blk trf tspinlock];
            x = iIns;
        end
        
        function x = SpinLockingPulseStoraged(obj,iIns,phase0,phase1,phase2,blk,trf,tspinlock)
            %% Hints
            % This method create one RF pulse.
            % Inputs:
            %   - iIns: first instruction position
            %   - phase0: phase register hard pulse
            %   - phase1: phase register locking pulse
            %   - phase2: phase register storaged pulse
            %   - blk: blanking delay
            %   - trf: hard pulse time
            %   - tspinlock: pulse locking time
            % Outputs:
            %   - x: last instruction position
            
            %% Code goes here:
            nIns = 4;
            vIns =  iIns: (nIns+iIns-1);
            
            [~,~,~,slice,~,~,~] = obj.ConfigureGradients(0);
            obj.RFamps(vIns)        = [0 0 1 0];
            obj.FREQs( vIns )       = [0 slice 0 0];
            obj.FLAGs(vIns)         = [1 1 1 1];
            obj.TXs(vIns)           = [0 1 1 1];
            obj.ENVELOPEs(vIns)     = [7 7 7 7];
            obj.PHASEs(vIns)        = [0 phase0 phase1 phase2];
            obj.PhResets(vIns)      = [1 0 0 0];
            obj.DELAYs(vIns)        = [blk trf tspinlock trf];
            x = iIns + nIns;
        end
        
        function [gradientinput] = Preemphasis(obj,trise,tflattop,G,th)
            rho = 16.78e-9; %Cupper resistivity (ohmio�metro)
            mu = 4*pi*1e-7;
            trise=trise*1e-6;
            tflattop=tflattop*1e-6;
            dt=5e-6; %This parameters controls the number of guide steps that the gradient output will take
            df=200; %Hz
            t=(-1/df):dt:(1/df);
            f=(-1/dt):df:(1/dt);
            omega=2*pi*f;
            
            TFSkinFDisc=exp(-th*sqrt(omega*mu/(2*rho)));
            %Uncoment this block to see the effect of attenuation due to cupper thickness
            % figure
            % hold on
            % plot(omega,real(TFSkinFDisc),'r.')
            % plot(omega,imag(TFSkinFDisc),'b.')
            % plot(omega,abs(TFSkinFDisc),'k.')
            
            grad=zeros(1,length(t));
            
            %Here we define the desired gradient as a discontinuous funcion
            for i=1:length(t)
                if t(i)<0
                    grad(i)=0;
                end
                if t(i)>=0 && t(i)<=trise
                    grad(i)=G*t(i)/trise;
                end
                if t(i)>trise && t(i)<=trise+tflattop
                    grad(i)=G;
                end
                if t(i)>trise+tflattop && t(i)<=3*trise+tflattop
                    grad(i)=-G*(t(i))/trise+G*(1+(trise+tflattop)/trise);
                end
                if t(i)>3*trise+tflattop && t(i)<=3*trise+2*tflattop
                    grad(i)=-G;
                end
                if t(i)>3*trise+2*tflattop && t(i)<=4*trise+2*tflattop
                    grad(i)=G*(t(i))/trise-G*((4*trise+2*tflattop)/trise);
                end
                if t(i)>=4*trise+2*tflattop
                    grad(i)=0;
                end
            end
            
            GzFDisc=fft(grad);
            GzOutF=GzFDisc.*TFSkinFDisc;
            GzOutDisc=ifft(GzOutF); %This is how the gradient actually is deformed due to eddy currents
            
            GzPEFDisc=GzFDisc./TFSkinFDisc;
            GzPETDisc=ifft(GzPEFDisc); %This should be the gradient pre-emphasized
            
            %Uncomment this block to see how is the desired gradient shape, the real gradient attenuated
            %by eddy currents and the preemphasized gradient         
            figure
            hold on
            plot(t(1,floor(length(t)/2):floor(length(t)/2+(6*trise+2*tflattop)/dt)),real(grad(1,floor(length(t)/2):floor(length(t)/2+(6*trise+2*tflattop)/dt))),'r','LineWidth',2)
            plot(t(1,floor(length(t)/2):floor(length(t)/2+(6*trise+2*tflattop)/dt)),real(GzOutDisc(1,floor(length(t)/2):floor(length(t)/2+(6*trise+2*tflattop)/dt))),'b','LineWidth',2)
            plot(t(1,floor(length(t)/2):floor(length(t)/2+(6*trise+2*tflattop)/dt)),real(GzPETDisc(1,floor(length(t)/2):floor(length(t)/2+(6*trise+2*tflattop)/dt))),'k','LineWidth',2)
            grid on
            legend('Desired gradient','Real gradient','Pre-emphasized gradient')
            
            
            maxPremphasisG=(max(real(GzPETDisc)));
            minPremphasisG=(min(real(GzPETDisc)));
            
            time=t(1,floor(length(t)/2):floor(length(t)/2+(4*trise+2*tflattop)/dt));
            gradient=real(GzPETDisc(1,floor(length(t)/2):floor(length(t)/2+(4*trise+2*tflattop)/dt)));
            gradient(1,1)=0; gradient(1,end)=0;
            gradientinput=[(time*1e6)' gradient'];
        end  
        
         function [time gradient] = HalfPreemphasis(obj,trise,tflattop,G,th)
            rho = 16.78e-9; %Cupper resistivity (ohmio�metro)
            mu = 4*pi*1e-7;
            trise=trise*1e-6;
            tflattop=tflattop*1e-6;
            dt=20e-6; %This parameters controls the number of guide steps that the gradient output will take
            df=200; %Hz
            t=(-1/df):dt:(1/df);
            f=(-1/dt):df:(1/dt);
            omega=2*pi*f;
            
            TFSkinFDisc=exp(-th*sqrt(omega*mu/(2*rho)));            
            grad=zeros(1,length(t));
            
            %Here we define the desired gradient as a discontinuous funcion
            for i=1:length(t)
                if t(i)<0
                    grad(i)=0;
                end
                if t(i)>=0 && t(i)<=trise
                    grad(i)=G*t(i)/trise;
                end
                if t(i)>trise && t(i)<=trise+tflattop
                    grad(i)=G;
                end
                if t(i)>trise+tflattop && t(i)<=2*trise+tflattop
                    grad(i)=-G*(t(i))/trise+G*(1+(trise+tflattop)/trise);
                end
            end
            
            GzFDisc=fft(grad);
            GzOutF=GzFDisc.*TFSkinFDisc;
            GzOutDisc=ifft(GzOutF); %This is how the gradient actually is deformed due to eddy currents
            
            GzPEFDisc=GzFDisc./TFSkinFDisc;
            GzPETDisc=ifft(GzPEFDisc); %This should be the gradient pre-emphasized
            
            %Uncomment this block to see how is the desired gradient shape, the real gradient attenuated
            %by eddy currents and the preemphasized gradient         
%             figure
%             hold on
%             plot(t(1,floor(length(t)/2):floor(length(t)/2+(2*trise+tflattop)/dt)),real(grad(1,floor(length(t)/2):floor(length(t)/2+(2*trise+tflattop)/dt))),'r','LineWidth',2)
%             plot(t(1,floor(length(t)/2):floor(length(t)/2+(2*trise+tflattop)/dt)),real(GzOutDisc(1,floor(length(t)/2):floor(length(t)/2+(2*trise+tflattop)/dt))),'b','LineWidth',2)
%             plot(t(1,floor(length(t)/2):floor(length(t)/2+(2*trise+tflattop)/dt)),real(GzPETDisc(1,floor(length(t)/2):floor(length(t)/2+(2*trise+tflattop)/dt))),'k','LineWidth',2)
%             grid on
%             legend('Desired gradient','Real gradient','Pre-emphasized gradient')

            time=t(1,floor(length(t)/2):floor(length(t)/2+(2*trise+tflattop)/dt));
            gradient=real(GzPETDisc(1,floor(length(t)/2):floor(length(t)/2+(2*trise+tflattop)/dt)));
            gradient(1,1)=0; gradient(1,end)=0;
        end  
    end
end

