function [ varargout ] = tiffwritevolume ( varargin )

name = varargin{1};

volumeD = varargin{2};

maxVD = max(max(max(volumeD)));
volumeD = volumeD/maxVD;


i = 1;
imwrite(im2uint16(volumeD(:,:,i)),name,'tiff')

for i = 2:length(volumeD(1,1,:))
    
    imwrite(im2uint16(volumeD(:,:,i)),name,'tiff','writemode','append');
    
end

end