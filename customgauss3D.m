% CUSTOMGAUSS    Generate a custom 2D gaussian 
%
%    gauss = customgauss(gsize, sigmax, sigmay, theta, offset, factor, center)
%
%          gsize     Size of the output 'gauss', should be a 1x2 vector
%          sigmax    Std. dev. in the X direction
%          sigmay    Std. dev. in the Y direction
%          theta     Rotation in degrees
%          offset    Minimum value in output
%          factor    Related to maximum value of output, should be 
%                    different from zero
%          center    The center position of the gaussian, should be a
%                    1x2 vector                     
function ret = customgauss3D(gsize, sigmax, sigmay, sigmaz, theta, offset, factor, center)
ret     = zeros(gsize);
rbegin  = -round(gsize(1) / 2);
cbegin  = -round(gsize(2) / 2);
dbegin  = -round(gsize(3) / 2);
for r=1:gsize(1)
    for c=1:gsize(2)
        for d=1:gsize(3)
            ret(r,c,d) = rotgauss(rbegin+r,cbegin+c, dbegin+d, theta, sigmax, sigmay, sigmaz, offset, factor, center);
        end
    end
end


function val = rotgauss(x, y, z, theta, sigmax, sigmay, sigmaz, offset, factor, center)
xc      = center(1);
yc      = center(2);
zc      = center(3);
theta   = (theta/180)*pi;
xm      = (x-xc)*cos(theta) - (y-yc)*sin(theta);
ym      = (x-xc)*sin(theta) + (y-yc)*cos(theta);
zm      = (z-zc);
u       = (xm/sigmax)^2 + (ym/sigmay)^2 + (zm/sigmaz)^2;
val     = offset + factor*exp(-u/2);