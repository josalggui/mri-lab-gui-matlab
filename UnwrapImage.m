close all
load 'IM.mat'                               %Load complex image
S1 = load('Data2D_50us_1.mat');                               %Load complex image
IM = fftshift(ifft2(fftshift(S1.Fid2D)));

figure; imagesc(abs(IM)), colormap(gray), axis square, axis off; title('Magnitude image'); 
figure; imagesc(angle(IM)), colorbar, colormap('default'), axis square, axis off; title('Wrapped phase'); 
[im_unwrapped,im_phase_quality] = Unwrap2D(IM);
%im_unwrapped(im_phase_quality > 1) = NaN;
%im_unwrapped(im_unwrapped < -2*pi | im_unwrapped > 2*pi) = NaN;
%im_unwrapped = im_unwrapped + pi;
%% Identify starting seed point on a phase quality map
minp=im_phase_quality(2:end-1, 2:end-1); minp=min(minp(:));
maxp=im_phase_quality(2:end-1, 2:end-1); maxp=max(maxp(:));
figure; imagesc(im_phase_quality,[minp maxp]), colormap(gray), axis square, axis off; title('Phase quality map'); 
figure; imagesc(im_unwrapped), colorbar, colormap('default'), axis square, axis off; title('Unwrapped phase'); 